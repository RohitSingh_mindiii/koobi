//
//  AppDelegate.swift
//  MualabCustomer
//
//  Created by Mac on 25/12/2017 .
//  Copyright @ 2017 Mindiii. All rights reserved.

import UIKit
import UserNotifications
import Firebase
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import Fabric
import Crashlytics
import FBSDKLoginKit
import FBSDKCoreKit
import Stripe
import SDWebImage

let appDelegate = AppDelegate.sharedObject()
let notificationDelegate = SampleNotificationDelegate()
var backgroundTask: UIBackgroundTaskIdentifier = .invalid

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate{
    
    var window: UIWindow?
    var navController: UINavigationController?
    
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    
    //MARK: - Shared object
    private static var sharedManager: AppDelegate = {
        let manager = UIApplication.shared.delegate as! AppDelegate
        return manager
    }()
     
    // MARK: - Accessors
    class func sharedObject() -> AppDelegate {
        return sharedManager
    }
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        objAppShareData.isFromDidFinish = true
        
        ////
        if WebURL.BaseUrl.contains("dev"){
            objChatShareData.kServerKey = "AAAAohAm9co:APA91bHNZDmtwzyLIdcjl4P7Vw7uoq7YwtKiuRsF5Ld5DHkXJBUZmmT4_Wcj6wXeKonhIorQEbivo3nILXfescBWH01HCmoQiOKUhwQgyP1mKByn43xpFTfXKU74E43TWkePKjaRjE7hHtsBh-FgSHTWk43xMPzFew"
            let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info-Dev", ofType: "plist")
            guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
                fatalError("Invalid Firebase configuration file.")
            }
            FirebaseApp.configure(options: options)
        }else{
            objChatShareData.kServerKey = "AAAAK1vRFPE:APA91bFDJlGE-pK5f7JarrELoglCDCZl2Bnnm495IBiYjWXte8BInV8ZSdNT9fcW-xx96LQFIQAAGiwvMXYpK8ap6uJX6qfiPXfMCEwbGbfd7KMXtSSm9MLdfpD6AhdpbHbzSQbew5wF"
            let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
            guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
                fatalError("Invalid Firebase configuration file.")
            }
            FirebaseApp.configure(options: options)
        }
        ////
        
        ////
        //FirebaseApp.configure()
        ////
        
        Messaging.messaging().delegate = self
        //Messaging.messaging().delegate = self as? MessagingDelegate
        Stripe.setDefaultPublishableKey(KUrlStripeId)
        self.configureNotification()
        //Rohit
       // ref = Database.database().reference()
        //NotificationCenter.default.addObserver(self, selector: #selector(self.redirectionFromServiceTag), name: NSNotification.Name(rawValue: "redirectionFromServiceTag"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "redirectionFromServiceTag"), object: nil, queue: nil) { [weak self](Notification) in
            self?.redirectionFromServiceTag(Notification)
        }
        
        notificationDelegate.delegate = self
        
        objLocationManager.getCurrentLocation()
        GMSPlacesClient.provideAPIKey("AIzaSyCJc8Wpt0C0qsQwrjGPudN4Ojjy3s8eUvI")
        GMSServices.provideAPIKey("AIzaSyCJc8Wpt0C0qsQwrjGPudN4Ojjy3s8eUvI")
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        
        if isLoggedIn {
            
            var strMyId = ""
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    strMyId = "\(userId)"
                }
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                if let id = userInfo["_id"] as? Int {
                    strMyId = String(id)
                }
            }
            
            //            let dict = ["count" : 50] as [String: Any]
            //            ref.child("chatBadgeCount").child(strMyId).setValue(dict) { (error, snap) in
            //            }
            ref.child("chatBadgeCount").child(strMyId).observe(.value, with: { (snapshot) in
                let dict = snapshot.value as? [String:Any]
                print(dict)
                if let count = dict?["count"] as? Int {
                    //objChatShareData.chatBadgeCount = count
                }else if let count = dict?["count"] as? String {
                    //objChatShareData.chatBadgeCount = Int(count) ?? 0
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
            })
            print(objChatShareData.chatBadgeCount)
            self.gotoTabBar(withAnitmation: false)
        }
        UIApplication.shared.statusBarStyle = .lightContent
        
        //fabric
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        //
        
        SDImageCache.shared().config.maxCacheAge = 3600 * 24 * 7 //1 Week
        SDImageCache.shared().maxMemoryCost = 1024 * 1024 * 20 //Aprox 20 images
        //SDImageCache.shared().config.shouldCacheImagesInMemory = false
        SDImageCache.shared().config.shouldDecompressImages = false
        SDWebImageDownloader.shared().shouldDecompressImages = false
        SDImageCache.shared().config.diskCacheReadingOptions = NSData.ReadingOptions.mappedIfSafe

        ////
//        let fileManager = FileManager.default
//        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        do {
//            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
//            print(fileURLs)
//            // process files
//        } catch {
//            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
//        }
        ////
        return true
    }
    
    @objc func redirectionFromServiceTag(_ objNotify: Notification){
        print(objNotify)
        //navBooking
        print(objNotify.userInfo as! [String:Any])
        print("redirectionFromServiceTag")
        let dictInfo = objNotify.userInfo as! [String:Any]
        objAppShareData.isFromServiceTag = true
        objAppShareData.selectedOtherIdForProfile = Int(dictInfo["artistId"] as? String ?? "") ?? 0
        //let strInCallPrice = dictInfo["incallPrice"] as? String ?? ""
        //let strOutCallPrice = dictInfo["outcallPrice"] as? String ?? ""
        let isOutCall = dictInfo["isOutCall"] as? String ?? ""
        var strIncallOrOutCall = ""
        if isOutCall == "0"{
           strIncallOrOutCall = "In Call"
        }else{
           strIncallOrOutCall = "Out Call"
        }
        /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        */
        let objService = SubSubService.init(dict: [:])
        objService.artistId = objAppShareData.selectedOtherIdForProfile
        objService.serviceId = dictInfo["businessTypeId"] as? Int ?? 0
        objService.subServiceId = dictInfo["categoryId"] as? Int ?? 0
        objService.subSubServiceId = dictInfo["tagId"] as? Int ?? 0
        if let id = dictInfo["businessTypeId"] as? String {
            objService.serviceId = Int(id) ?? 0
        }
        if let id = dictInfo["categoryId"] as? String {
            objService.subServiceId = Int(id) ?? 0
        }
        if let id = dictInfo["tagId"] as? String {
            objService.subSubServiceId = Int(id) ?? 0
        }
        if let id = dictInfo["staffId"] as? String {
            objService.staffIdForEdit = Int(id) ?? 0
        }
        if let id = dictInfo["staffId"] as? Int {
            objService.staffIdForEdit = id
        }
        
        objAppShareData.isFromServiceTagBook = true
        objAppShareData.isBookingFromService = true
        objAppShareData.objServiceForEditBooking = objService
        
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        //let nav = sbNew.instantiateViewController(withIdentifier: "navBooking") as? UINavigationController
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
            objVC.hidesBottomBarWhenPushed = true
            //appDelegate.window?.rootViewController = navController
            let topController = self.topViewController()
            //print(topController)
            //print(topController?.navigationController)
            topController?.navigationController!.pushViewController(objVC, animated: true)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("resign active")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        if isLoggedIn {
           
        self.manageOnlineStatus(isOnlineValue: 0)
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strMyId = "\(userId)"
                self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strMyId = String(id)
                self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                
            }
        }
            if objAppShareData.isFileIsUploding{
                self.registerBackgroundTask()
            }
        }
    }
    
    func registerBackgroundTask() {
      backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
        self?.endBackgroundTask()
      }
      assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
      print("Background task ended.")
      UIApplication.shared.endBackgroundTask(backgroundTask)
      backgroundTask = .invalid
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
       
        if isLoggedIn {
        
        self.manageOnlineStatus(isOnlineValue: 1)

        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strMyId = "\(userId)"
                self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strMyId = String(id)
                self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])

            }
        }
            let topController = self.topViewController()
            //if topController?.restorationIdentifier != "ArtistProfileVC" || topController?.restorationIdentifier != "MyMenuVC"  || topController?.restorationIdentifier != "InvitationListVC"{
            if topController?.restorationIdentifier != "ArtistProfileVC" || topController?.restorationIdentifier != "MyMenuVC" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshUserDetail"), object: nil)
            }
            if topController?.restorationIdentifier != "SearchBoardVC" && !objAppShareData.isSearchingUsingFilter{
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
   
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        objAppShareData.callWebserviceFor_deleteAllbooking()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deletePendingUploadedImage"), object: nil)
    }
    

    public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool{
        let facebookDidHandle = ApplicationDelegate.shared.application(app, open: url, options: options)
        return facebookDidHandle
    }
    
    //    public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    //        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options!)
    //    }
    
    //MARK: - Remote Notification Get Device token methods.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        if deviceTokenString.count > 0 {
            objAppShareData.deviceToken = deviceTokenString
        }
        
        //if let refreshedToken = InstanceID.instanceID().token() {
//        if let refreshedToken = InstanceID.instanceID().instanceID {
//            objAppShareData.firebaseToken = refreshedToken
//            print("objAppShareData.firebaseToken = \(objAppShareData.firebaseToken)")
//        }
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }else if let result = result {
                print("Remote instance ID token: \(result.token)")
                objAppShareData.firebaseToken = result.token
                print("objAppShareData.firebaseToken = \(result.token)")
            }
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
}

//MARK: - Register for Remote notification
extension AppDelegate{
    
    func configureNotification() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = notificationDelegate
            
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
}

//MARK: - FireBase Methods / FCM Token
extension AppDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        objAppShareData.firebaseToken = fcmToken
        print("objAppShareData.firebaseToken = \(objAppShareData.firebaseToken)")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        objAppShareData.firebaseToken = fcmToken
        print("objAppShareData.firebaseToken = \(objAppShareData.firebaseToken)")
        ConnectToFCM()
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
//        if let refreshedToken = InstanceID.instanceID().token() {
//            objAppShareData.firebaseToken = refreshedToken
//            print("objAppShareData.firebaseToken = \(objAppShareData.firebaseToken)")
//        }
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }else if let result = result {
                print("Remote instance ID token: \(result.token)")
                objAppShareData.firebaseToken = result.token
                print("objAppShareData.firebaseToken = \(result.token)")
            }
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        ConnectToFCM()
    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
    }
    
    func ConnectToFCM() {
//        if let refreshedToken = InstanceID.instanceID().token() {
//            objAppShareData.firebaseToken = refreshedToken
//            print("objAppShareData.firebaseToken = \(objAppShareData.firebaseToken)")
//        }
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }else if let result = result {
                print("Remote instance ID token: \(result.token)")
                objAppShareData.firebaseToken = result.token
                print("objAppShareData.firebaseToken = \(result.token)")
            }
        }
    }
}



//MARK: - Custom Methods
extension AppDelegate{
    
    func logout(){
        objAppShareData.selectedTab = 0
        DoneEditProfile = false
        UIApplication.shared.applicationIconBadgeNumber = 1
        UIApplication.shared.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
         UserDefaults.standard.set(false, forKey: UserDefaults.keys.isLoggedIn)
        UIApplication.shared.unregisterForRemoteNotifications()
        self.clearData()
        self.gotoLoginPage()
    }
    
    func clearData(){
        objAppShareData.arrFeedsData = [feeds]()
        objAppShareData.isSearchingUsingFilter = false
        objAppShareData.dictFilter = [String : Any]()
        objAppShareData.objRefineData = RefineData()
        objAppShareData.arrSelectedService = [BookingServices]()
        objAppShareData.isFromEditBooking = false
        objAppShareData.isFromConfirmBooking = false
    }
    
    func gotoLoginPage() {
        
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "LoginNavigationController") as? UINavigationController
        
//        let transition = CATransition()
//        transition.duration = 1.0
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType(rawValue: "cube")
//        transition.subtype = CATransitionSubtype.fromLeft
//        transition.delegate = self as? CAAnimationDelegate
//        appDelegate.window?.layer.add(transition, forKey: nil)
        
        appDelegate.window?.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func showHome() {
        let sb: UIStoryboard = UIStoryboard(name: "TabBar", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "TabBarNav") as? UINavigationController
        appDelegate.window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    func gotoTabBar(withAnitmation: Bool) {
        
        let sb: UIStoryboard = UIStoryboard(name: "TabBar", bundle: Bundle.main)
        navController = nil
        navController = sb.instantiateViewController(withIdentifier: "TabBarNav") as? UINavigationController
        
        if withAnitmation {
//            let transition = CATransition()
//            transition.duration = 1.0
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            transition.type = CATransitionType(rawValue: "cube")
//            transition.subtype = CATransitionSubtype.fromRight
//            transition.delegate = self as? CAAnimationDelegate
//            appDelegate.window?.layer.add(transition, forKey: nil)
        }
        appDelegate.window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}

//MARK: -  delegateSampleNotification
extension AppDelegate : delegateSampleNotification {
    
    func handleNotificationWithNotificationData(userInfo: [String : Any]) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        
        objAppShareData.notificationType = ""
        objAppShareData.isFromNotification =  false
        objAppShareData.notificationInfoDict = nil
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        guard isLoggedIn else { return }
        print("\n\n>> AppDelegate userInfo = \(userInfo)\n\n")
        
        var notifincationType : Int?
        if let notiType = userInfo["notifincationType"] as? Int{
            notifincationType = notiType
        }else{
            if let notiType = userInfo["notifincationType"] as? String{
                notifincationType = Int(notiType)
            }
        }
        
        guard let notiType = notifincationType else {
            return
        }
        
        switch notiType {
            
        case 1:
            //var body = 'sent a booking request.';
            //var title = 'Booking request';
            objAppShareData.notificationType = "booking" //Artist side
            break;
            
        case 2:
            //var body = 'accepted your booking request.'
            //var title = 'Booking accept';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 3:
            //var body = 'rejected your booking request.';
            //var title = 'Booking reject';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 4:
            //var body = 'cancelled your booking request.';
            //var title = 'Booking cancel';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 5:
            //var body = 'completed your booking request.';
            //var title = 'Booking complete';
            objAppShareData.notificationType = "booking"
            objAppShareData.shouldReviewPopUpOpen = true
            break;
            
        case 6:
            //var body = 'given review for booking.';
            //var title = 'Booking Review';
            objAppShareData.notificationType = "booking"
            objAppShareData.shouldReviewPopUpOpen = false
            break;
            
        case 7:
            //var body = 'added a new post.';
            //var title = 'new post';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 8:
            //var body = 'Payment has completed by';
            //var title = 'Payment';
            objAppShareData.notificationType = "booking" //Artist side //on back open payment history
            break;
            
        case 9:
            //var body = 'commented on your post.';
            //var title = 'Comment';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 10:
            //var body = 'likes your post.';
            //var title = 'Post like';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 11:
            //var body = 'likes your comment.';
            //var title = 'Comment like';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 12:
            //var body = 'started following you.';
            //var title = 'Following';
            objAppShareData.notificationType = "profile" //->Checked
            break;
            
        case 13:
            //var body = 'added to their story.';
            //var title = 'Story';
            objAppShareData.notificationType = "story" // open story //->Checked
            break;
            
        case 14:
            //var body = 'added you as a favorite.';
            //var title = 'Story';
            objAppShareData.notificationType = "profile" //Artist side
            break;
            
        case 15:
            //var body = 'chat.';
            //var title = 'chat';
            objAppShareData.notificationType = "chat"
            break;
            
        case 16:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 17:
            //var body = 'has sent you an invitation to join as a staff.';
            //var title = 'Staff invitation';
            objAppShareData.notificationType = "Staff invitation"
            break;
            
        case 20:
            //var title = 'Booking Start';
            objAppShareData.notificationType = "booking"
            break;
            
        case 21:
            //var title = 'Booking End';
            objAppShareData.notificationType = "booking"
            break;
            
        case 25:
            //var title = 'Booking Report by Admin';
            objAppShareData.notificationType = "booking"
            break;
            
        case 26:
            //var title = 'Booking Report';
            objAppShareData.notificationType = "booking"
            break;
         
        case 31:
            //var body = 'Payment has completed by';
            //var title = 'Payment';
            objAppShareData.notificationType = "booking" //Artist side //on back open payment history
            break;
            
        default:
            print("default : No condition matched")
        }
        
        //notificationType : "booking","feed","story","profile","chat"
        
        if objAppShareData.notificationType != "" {
            objAppShareData.isFromNotification =  true
            objAppShareData.notificationInfoDict = userInfo
            self.gotoTabBar(withAnitmation: false)
        }
        
        /*
         AppDelegate userInfo = [ "aps": {
         alert ={
         body = "Neha commented on your post.";
         title = Comment;
         };
         "content-available" = 1;
         "mutable-content" = 1;
         sound = default;
         },
         "notifyId": 17,
         "gcm.notification.notifincationType": 9,
         "google.c.a.e": 1,
         "title": Comment,
         "gcm.message_id": 0:1533113029672857%c6fca462c6fca462,
         "body": Neha commented on your post.,
         "notifincationType": 9,
         "urlImageString": http://koobi.co.uk:3000/uploads/profile/1532757198120.jpg,
         "gcm.notification.notifyId": 17]
         
         >> AppDelegate userInfo = ["userType": user, "notifyId": 1, "body": Pankaj added to their story., "google.c.a.e": 1, "gcm.notification.userType": user, "urlImageString": http://koobi.co.uk:3000/uploads/profile/1532755154540.jpg, "notifincationType": 13, "title": Story, "gcm.message_id": 0:1533292269949856%c6fca462c6fca462, "aps": {
         alert =     {
         body = "Pankaj added to their story.";
         title = Story;
         };
         "content-available" = 1;
         sound = default;
         }, "gcm.notification.notifyId": 1, "gcm.notification.notifincationType": 13]
         */
    }
}

extension AppDelegate{
    
    func manageOnlineStatus(isOnlineValue:Int){
        if let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId){
            let calendarDate = ServerValue.timestamp()
            let dict = ["isOnline":isOnlineValue,
                        "lastActivity":calendarDate] as [String : Any]
            Database.database().reference().child("users").child(myId).updateChildValues(dict)
        }}
    
    
    

    
}

