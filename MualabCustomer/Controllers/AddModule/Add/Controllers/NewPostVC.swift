//
//  NewPostVC.swift
//  Mualab
//
//  Created by MINDIII on 10/24/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import GooglePlaces
import Alamofire
import FacebookShare
import FBSDKCoreKit
import FBSDKLoginKit

fileprivate enum check : String {
    
    case administrative_area_level_2,
    administrative_area_level_1,
    country,
    postal_code,
    locality
}

var imageRatioDynamic = 0

class NewPostVC: UIViewController,UITextViewDelegate{
    
    @IBOutlet weak var lblTagCount: UILabel!
    @IBOutlet weak var vwAddPeopleTags: UIView!
    var documentController: UIDocumentInteractionController!
    var activityController: UIActivityViewController!

    var videoURL: URL?
    var strDescriptionText = ""
    var imgFeed: UIImage?
    var imgThumb: UIImage?
    var isVideo = false
    
    fileprivate var window = UIApplication.shared.keyWindow

    var arrFeedImages = [UIImage]()
    var arrSelectedFeedImagesWithTagInfo = [PhotoInfo]()
    
    var strLat = ""
    var strLong = ""
    var strLocation = ""
    var strDescriptionToUpload = ""
    var strShareTo = ""
    var mp4CompressedURL: URL?
    var player: AVPlayer?
    var descrip = ""
    fileprivate var currentWordIndex = 0
    fileprivate var suggesionArray = [suggessions]()
    var arrPeopleTagSuggesionArray = [suggessions]()
    
    //fileprivate let defultDescription = "Write a caption......."
    fileprivate let defultDescription = "Write a caption"
    fileprivate var suggesionType = ""
    
    //
    @IBOutlet weak var imgThumbView: UIView!
    //
    @IBOutlet weak var imgViewFeed: UIImageView!
    //
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!

    @IBOutlet weak var btnPlayVideo: UIButton!
    //
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    //
    @IBOutlet weak var suggesionView: UIView!
    //
    @IBOutlet weak var txtDescription: UITextView!
    //
    @IBOutlet weak var tblSuggesion: UITableView!
    
    @IBOutlet weak var dataScrollView: UIScrollView!

    //Mark: - Viewcontrollers methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.imgViewFeed.contentMode = .scaleAspectFit
        configureView()
        self.observeKeyboard()
        addTapGastureToFeedImage()
        self.setCurrentLocation()
        self.txtDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtDescription.tintColorDidChange()
        self.lblLocation.text = objLocationManager.strAddress
        self.lblLocation.text = objLocationManager.strCurrentPlaceName
        self.strLat = objLocationManager.strlatitude ?? ""
        self.strLong = objLocationManager.strlongitude ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        //objLocationManager.getCurrentLocation()
        if (videoURL == nil) && (imgFeed == nil) {
        if objAppShareData.arrTagePeopleNameIds.count > 0{
           self.lblTagCount.isHidden = false
        self.lblTagCount.text = String(objAppShareData.arrTagePeopleNameIds.count)
        }else{
            self.lblTagCount.text = String(objAppShareData.arrTagePeopleNameIds.count)
            self.lblTagCount.isHidden = true
            }
        }
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Local Methods
    func configureView() {
        
        imgThumbView.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
        self.lblCount.text = String(self.arrFeedImages.count)
        self.lblCount.layer.borderColor = UIColor.white.cgColor
        
        if (videoURL != nil) || (imgFeed != nil) {
            imgThumbView.isHidden = false
            imgViewFeed.image = imgThumb
            ////
            if objAppShareData.isImageCrop {
                self.imgViewFeed.contentMode = .scaleAspectFit
            }else{
                self.imgViewFeed.contentMode = .scaleToFill
            }
            ////
            let height = imgThumb?.size.height
            let wight = imgThumb?.size.width
            if self.arrFeedImages.count > 1{
                //imageRatioDynamic = 0
                imageRatioDynamic = 1
            }else{
                if float_t(height ?? 0.0) < float_t(wight ?? 0.0) {
                    if imageRatioDynamic != 1{
                      imageRatioDynamic = 0
                    }
                }else if float_t(height ?? 0.0) == float_t(wight ?? 0.0){
                    imageRatioDynamic = 1
                    UserDefaults.standard.setValue("1", forKey: "postType")
                    UserDefaults.standard.synchronize()
                }else{
                    if imageRatioDynamic != 1{
                      imageRatioDynamic = 2
                        UserDefaults.standard.setValue("1", forKey: "postType")
                        UserDefaults.standard.synchronize()
                    }
                }
                print("height,  wight",height,wight)
            }
           
            ////
            if objAppShareData.isImageCrop{
                objAppShareData.isImageCrop = false
                self.imgViewFeed.contentMode = .scaleAspectFit
            }else{
                self.imgViewFeed.contentMode = .scaleAspectFill
            }
            imgViewFeed.image = imgThumb
            ////
            
            if isVideo {
                self.vwAddPeopleTags.isHidden = true
                self.lblCount.isHidden = true
                btnPlayVideo.isHidden = false
            }else {
                self.vwAddPeopleTags.isHidden = false
                self.lblCount.isHidden = false
                btnPlayVideo.isHidden = true
            }
        }else {
            self.vwAddPeopleTags.isHidden = false
            imgThumbView.isHidden = true
        }
        
        if strDescriptionText.count > 0 {
            txtDescription.text = strDescriptionText
        }else {
            txtDescription.text = defultDescription
        }
        self.suggesionView.isHidden = true
        addAccesorryToKeyBoard()
    }
    
    func addTapGastureToFeedImage() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showImage))
        imgThumbView.addGestureRecognizer(tapRecognizer)
    }
    
    func addAccesorryToKeyBoard() {
        let keyboardDoneButtonView = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyBoard))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        txtDescription.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func resignKeyBoard() {
        if txtDescription.text.count == 0 {
            txtDescription.text = defultDescription
        }
        txtDescription.endEditing(true)
    }
    
    func addVideo(toVC url: URL) {
        // create an AVPlayer
        player = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = player
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: {() -> Void in
            controller.player?.play()
        })
    }
    
    @objc func showImage() {
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
        objShowImage.arrFeedImages = self.arrFeedImages
        objShowImage.isTypeIsUrl = false
        objShowImage.modalPresentationStyle = .fullScreen
        present(objShowImage, animated: true) //{ _ in }
    }
    
    func resetFields() {
        lblLocation.text = "Add Location"
        imgFeed = nil
        imgThumb = nil
        videoURL = nil
        mp4CompressedURL = nil
        strDescriptionToUpload = ""
        strDescriptionText = ""
        txtDescription.text = defultDescription
        imgThumbView.isHidden = true
    }
    func checkForLocationString(obj: String) -> String {
        return (obj == "Add Location") ? "" : obj
    }
    
    func setCurrentLocation(){
      
        if objLocationManager.strlatitude == "" {
            objLocationManager.strlatitude = UserDefaults.standard.value(forKey: UserDefaults.keys.myCurrentLat) as? String ?? ""
           objLocationManager.strlongitude = UserDefaults.standard.value(forKey: UserDefaults.keys.myCurrentLong) as? String ?? ""
        }
        
        if objLocationManager.strCurrentPlaceName == ""{
             objLocationManager.strCurrentPlaceName = UserDefaults.standard.value(forKey: UserDefaults.keys.myCurrentAdd) as? String ?? ""
        }
        
   
        
        self.strLat = objLocationManager.strlatitude ?? "22.71956"
        self.strLong = objLocationManager.strlongitude ?? "75.85772"
        self.strLocation = objLocationManager.strCurrentPlaceName
        
        objLocationManager.getCurrentAdd(success: { pm in
            if pm.name!.count > 0{
                self.strLocation = pm.name!
            }else{
                var addressString : String = ""
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if addressString != ""{
                   self.strLocation = addressString
                   objLocationManager.strCurrentPlaceName = addressString
                }
            }
            
        }) { error in
            
        }
    }
    
    func getLocalityWithPlace(place:GMSPlace){
        if let arrAdd = place.addressComponents {
            var city = ""
            var country = ""
            for address in arrAdd{
                switch address.type  {
                    
                case check.locality.rawValue:
                    city = address.name
                    
                case check.country.rawValue:
                    country = address.name
                    
                default:
                    break
                    //print("\(address.type) = \(address.name)")
                }
            }
            self.strLocation = city+", "+country
        }
    }
    
    // MARK: - textview delegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (txtDescription.text == defultDescription) {
            txtDescription.text = ""
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        // txtDescription.resolveHashTags()
        if txtDescription.text.count == 0 {
            txtDescription.text = defultDescription
            txtDescription.resignFirstResponder()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        var substring: String = textView.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: text)
        
        let wordsInSentence = substring.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        
        var indexInSavedArray  = 0
        
        for str in wordsInSentence{
            // Get multiple ranges of all matched strings
            let subS = substring.ranges(of: str)
            
            if let selectedRange = textView.selectedTextRange {
                // Get current index text position
                let cursorPosition = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
                
                var p = 0
                if text == ""{
                    p = 1
                }else if text == "\n" || text == " "{
                    p = -2
                }else{
                    p = 0
                }
                
                for sub in subS{
                    
                    let lowR = sub.lowerBound.encodedOffset
                    let upR = sub.upperBound.encodedOffset
                    
                    if cursorPosition >= lowR && (cursorPosition - p) <= upR{
                        
                        if str.hasPrefix("@"){
                            self.currentWordIndex = indexInSavedArray
                            self.suggesionType = "user"
                            //if (videoURL != nil) || (imgFeed != nil) {
                            self.searchEntries(withSubstring: str, andType: "user")
                            //}
                            break
                        }else if str.hasPrefix("#"){
                            self.currentWordIndex = indexInSavedArray
                            self.suggesionType = ""
                            self.searchEntries(withSubstring: str, andType: "")
                            break
                        }else{
                            self.suggesionView.isHidden = true
                            self.suggesionArray.removeAll()
                            self.tblSuggesion.reloadData()
                        }
                    }else{
                        self.suggesionView.isHidden = true
                        self.suggesionArray.removeAll()
                        self.tblSuggesion.reloadData()
                    }
                }
            }
            indexInSavedArray = indexInSavedArray + 1
        }
        
        
        print((self.txtDescription.text?.count)!+text.count)
        print(self.txtDescription.text?.count)
        print(text.count)
        
        if (self.txtDescription.text?.count)!+text.count >= 1001{
            
//            let str = text
//            let start = str.index(str.startIndex, offsetBy: 0)
//            let end = str.index(str.endIndex, offsetBy: -(str.count-1000))
//            let range = start..<end
//            print(str[range])
            
            //return false
            return true
        }else if textView == self.txtDescription && text == ""{
            return true
        }else if textView == self.txtDescription &&  ((self.txtDescription.text?.count)! >= 1001){
            //return false
            return true
        }else if text != "" &&  ((self.txtDescription.text?.count)! >= 1001){
            //return false
            return true
        }else if text == "" &&  ((self.txtDescription.text?.count)! >= 1001){
            return true
        }
        
        //let newString = text.replacingCharacters(in: 10, with: "")

        return true
    }
}

extension NewPostVC : GMSAutocompleteViewControllerDelegate{
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //lblLocation.text = place.formattedAddress
        lblLocation.text = place.name
        strLat = "\(place.coordinate.latitude)"
        strLong = "\(place.coordinate.longitude)"
        if place.name!.count>0{
            self.strLocation = place.name!
        }else{
            self.getLocalityWithPlace(place: place)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        //print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
// MARK: - IBActions

fileprivate extension NewPostVC{
    
    @IBAction func btnTagPeopleAction(_ sender: Any){
        if (videoURL != nil) || (imgFeed != nil) {

        if self.arrSelectedFeedImagesWithTagInfo.count == 0 {
            for img in arrFeedImages {
                let objPhotoInfo = PhotoInfo.init(photoInfo: ["imageFile": img,
                                                              "tags": [],
                                                              ])
                self.arrSelectedFeedImagesWithTagInfo.append(objPhotoInfo)
            }
        }
        self.gotoAddTagVC()
        }else{
            if self.txtDescription.text == "" || self.txtDescription.text == defultDescription{
                objAppShareData.shakeViewField(self.txtDescription!)
            }else{
            self.gotoAddTagTextVC()
            }}
    }
    
    @IBAction func btnTagServiceAction(_ sender: Any){
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        if objAppShareData.arrTagePeopleNameIds.count>0{
            objAppShareData.arrTagePeopleNameIds.removeAll()
        }
        if objAppShareData.arrTagePeopleNameData.count>0{
        objAppShareData.arrTagePeopleNameData.removeAll()
        }
        navigationController?.popViewController(animated: true)
    }
    
    //TODO: Select City
    @IBAction func btnSelectCityAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.tintColor = UIColor.theameColors.skyBlueNewTheam
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnFacebook(_ sender: Any) {
        btnFacebook.isSelected = !btnFacebook.isSelected
    }
    
    @IBAction func btnTwitter(_ sender: Any) {
        btnTwitter.isSelected = !btnTwitter.isSelected
    }
    @IBAction func btnInstagramAction(_ sender: Any) {
        btnInstagram.isSelected = !btnInstagram.isSelected
    }
    @IBAction func btnShareAction(_ sender: Any) {
        //shareToInstagram()
        //shareToFacebook()
        //self.uploadVideoOnFacebook()
        txtDescription.resignFirstResponder()
        //return
        
        if (videoURL != nil) || (imgFeed != nil) {
        }else{
            if self.txtDescription.text == "" || self.txtDescription.text == defultDescription{
                self.txtDescription.text = defultDescription
                objAppShareData.shakeViewField(self.txtDescription!)
                return
            }
        }
        if !(txtDescription.text == defultDescription) {
            strDescriptionToUpload = txtDescription.text!
        }else {
            strDescriptionToUpload = ""
        }
        
        var id = 0
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            id = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let idn = userInfo["_id"] as? Int {
                id = idn
            }
        }
        let strID = "\(id)"
       
        strDescriptionToUpload = strDescriptionToUpload.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let tags = self.getTagsFromString(string: strDescriptionToUpload)
        let arrPeopleTagsIDs = self.getPeopleTagsFromString(string: strDescriptionToUpload)
        var myIds = ""
        var jsonStringPeopleTagIds : String = ""
        do {
            
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: arrPeopleTagsIDs, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                jsonStringPeopleTagIds = JSONString
                myIds = JSONString
            }
        } catch {
        }
        
        
        if btnFacebook.isSelected && btnTwitter.isSelected {
            strShareTo = "facebook,twitter"
        }else if btnTwitter.isSelected {
            strShareTo = "twitter"
        }else if btnFacebook.isSelected {
            strShareTo = "facebook"
        }else if btnInstagram.isSelected {
            strShareTo = "instagram"
        }else{
            strShareTo = ""
        }
        
        self.strLocation = self.checkForLocationString(obj: self.strLocation)
        
        if (videoURL != nil) || (imgFeed != nil) {
            if isVideo {
                encodeVideo(videoURL:videoURL!, success: { firstURL in
                    
                    let dicParam  = ["type": "newsFeed",
                                     "feedType": "video",
                                     "tagData" : jsonStringPeopleTagIds,
                                     "peopleTagData":myIds,
                                     "caption": self.strDescriptionToUpload,
                                     "latitude": self.strLat,
                                     "longitude": self.strLong,
                                     "location": self.strLocation,
                                     "userId":strID,
                                     "feedImageRatio": "1",
                                     "tag":tags] as [String:AnyObject]
                    
                    self.mp4CompressedURL = firstURL
                    print("\n\n >> video dicParamTag = \(dicParam)")
                    self.callWebservice(for_AddFeed: dicParam)
                    
                }, failure: { error in
                    
                })
                
            }else {
                
                var arrPeopleTag = [[[String: Any]]]()
                var arrTempTagIDs = [String]()
                
                for objPhotoInfo in self.arrSelectedFeedImagesWithTagInfo {
                    
                    var arrTempTag = [[String: Any]]()

                    if objPhotoInfo.arrTags.count > 0 {
                        
                        for objTagInfo in objPhotoInfo.arrTags {
                            var dict = [String : Any]()
                            dict["x_axis"] = objTagInfo.normalizedPosition().x * 100
                            dict["y_axis"] = objTagInfo.normalizedPosition().y * 100
                            dict["unique_tag_id"] = objTagInfo.tagText()
                            dict["tagDetails"] = objTagInfo.metaData
                            arrTempTag.append(dict)
                            
                            var strTagID = ""
                            if let  tagID = objTagInfo.metaData["tagId"] as? String{
                                strTagID = tagID
                            }else{
                                if let  tagID = objTagInfo.metaData["tagId"] as? Int{
                                    strTagID = "\(tagID)"
                                }
                            }
 
                            if strTagID != "" && !arrTempTagIDs.contains(strTagID) {
                                arrTempTagIDs.append(strTagID)
                            }
                        }
                    }                    
                    arrPeopleTag.append(arrTempTag)
                }
                
                var jsonString : String = ""
                do {
                     //Convert to Data
                    let jsonData = try JSONSerialization.data(withJSONObject: arrPeopleTag, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    //Convert back to string. Usually only do this for debugging
                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                        jsonString = JSONString
                    }
                 } catch {
                 }
                 do {
                    //Convert to Data
                    let jsonData = try JSONSerialization.data(withJSONObject: arrTempTagIDs, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    //Convert back to string. Usually only do this for debugging
                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                        myIds = JSONString
                    }
                    
                } catch {
                }
                
                 for strID1 in arrPeopleTagsIDs {
                    if strID1 != "" && !arrTempTagIDs.contains(strID1) {
                        arrTempTagIDs.append(strID1)
                    }
                }
                
                for strID1 in arrTempTagIDs {
                    if strID1 == strID {
                        let index = arrTempTagIDs.index(of: strID1)
                        arrTempTagIDs.remove(at: index!)
                        break
                    }
                }
               
                var jsonStringtagData : String = ""
                
                do {
                     //Convert to Data
                    let jsonData = try JSONSerialization.data(withJSONObject: arrTempTagIDs, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    //Convert back to string. Usually only do this for debugging
                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                        jsonStringtagData = JSONString
                        //myIds = JSONString
                    }
                    
                } catch {
                }
                
                let dicParam  = ["feedType": "image" ,
                                 "type": "newsFeed",
                                 "peopleTag" : jsonString,
                                 "tagData" : jsonStringtagData,
                                 "peopleTagData":myIds,
                                 "caption": strDescriptionToUpload,
                                 "latitude": strLat,
                                 "longitude": strLong,
                                 "location": self.strLocation,
                                 "videoThumb":"",
                                 "userId":strID,
                                 "feedImageRatio": String(imageRatioDynamic),
                                 "tag":tags] as [String:AnyObject]
                
                print("\n\n >> Image dicParamTag = \(dicParam)")
                callWebservice(for_AddFeed: dicParam)
            }
        }else {
            if strDescriptionToUpload.count > 0 {
                
                let dicParam  = ["feed": strDescriptionToUpload,
                                 "tagData" : jsonStringPeopleTagIds,
                                 "peopleTagData":"",//myIds,
                                 "type": "newsFeed",
                                 "feedType": "text",
                                 "caption": strDescriptionToUpload,
                                 "latitude":strLat,
                                 "longitude": strLong,
                                 "location" : self.strLocation,
                                 "videoThumb" : "",
                                 
                                 "userId" : strID,
                                 "tag" : tags] as [String:AnyObject]
                
                print("\n\n >> text dicParamTag = \(dicParam)")
                callWebservice(for_AddFeed:dicParam)
            }else{
                objAppShareData.shakeViewField(self.txtDescription!)
            }
        }
    }
    @IBAction func btnPlayVideo(_ sender: Any) {
        addVideo(toVC: videoURL!)
    }
}

extension NewPostVC : UIDocumentInteractionControllerDelegate{
    // MARK: - Webservices
    func callWebservice(for_AddFeed dicParam: [String: AnyObject]) {
        //objWebserviceManager.StartIndicator()

        if !isVideo{
            objAppShareData.isFileIsUploding = true
          //objWebserviceManager.StartIndicator()
            self.uploadStartNotification()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let url = WebURL.BaseUrl+WebURL.addFeed
        var strAuth = ""
        
        if UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)==nil {
            strAuth=""
        }else{
            strAuth = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
        }
        
        print("\nstrURL = \(url)")
        print("\nparams = \(dicParam)")
        print("\nstrAuthToken = \(strAuth)")
        
        let headers = ["authtoken" : strAuth]
        Alamofire.upload(multipartFormData:{ multipartFormData in
           
            for (key, value) in dicParam {
                
                if let data = value.data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }else{
                    
                }
            }
            
            if self.isVideo{
                
                do {
                    let imageData = try Data.init(contentsOf: self.mp4CompressedURL!)
                    
                    multipartFormData.append(imageData,
                                             withName:"feed",
                                             fileName:"file.mp4",
                                             mimeType:"video/mp4")
                } catch {
                  
                }
                
                if let imageData = objAppShareData.generateImage(fromURL: self.mp4CompressedURL!, withSize: CGSize(width: 420, height: 560)).jpegData(compressionQuality: 0.5){
                    
                    multipartFormData.append(imageData,
                                             withName:"videoThumb",
                                             fileName:"image.jpg",
                                             mimeType:"image/jpeg")
                    
                }
            }else{
                
                for image in self.arrFeedImages{
                    
                    var imageData = image.jpegData(compressionQuality: 0.2)
                    
                    let size = Float((imageData?.count)!) / 1024.0 / 1024.0
                    if size < 30{
                        imageData = image.jpegData(compressionQuality: 0.5)
                    }
                    /*
                    var compression = 1.0
                    while compression >= 0.0 {
                        imageData = UIImageJPEGRepresentation(image, CGFloat(compression))!
                        let imageLength: Int = imageData!.count
                        if imageLength < 90000 {
                            break
                        }
                        compression -= 0.1
                    }*/
                   
                    if imageData != nil {
                        multipartFormData.append(imageData!,
                                                 withName:"feed",
                                                 fileName:"image.jpg",
                                                 mimeType:"image/jpeg")
                    }
                }
            }
        },
                         to:url,
                         headers:headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { responseObject in                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    if responseObject.result.isSuccess {
                                        do {
                                            let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                            self.handleResponse(response:dictionary as! [String:Any])
                                        }catch{
                                            
                                        }
                                    }
                                    if responseObject.result.isFailure {
                                        let error : Error = responseObject.result.error!
                                        objWebserviceManager.StopIndicator()
                                        if let controller = UIApplication.shared.keyWindow?.rootViewController {
                                             showAlertVC(title: kAlertTitle, message: kErrorUpload, controller: controller)
                                        }
                                        objAppShareData.isFileIsUploding = false
                                        objAppShareData.objAppdelegate.endBackgroundTask()
                                    }
                                }
                            case .failure(let encodingError):
                            
                                objWebserviceManager.StopIndicator()
                                if let controller = UIApplication.shared.keyWindow?.rootViewController {
                                    showAlertVC(title: kAlertTitle, message: kErrorUpload, controller: controller)
                                }
                          
                                objAppShareData.isFileIsUploding = false
                                objAppShareData.objAppdelegate.endBackgroundTask()
                            }
        })
    }
    
    func shareToFacebook(){
//        let photo = SharePhoto.init(image: self.imgFeed!, userGenerated: true)
//        //sharePhoto.image = self.imgFeed
//        let content = SharePhotoContent(photos: [photo])
//        //content.photos = [sharePhoto]
//        let sharer = grap(content: content)
//        //sharer.accessToken = FBSDKAccessToken.current()
//        sharer.failsOnInvalidData = true
//        sharer.completion = { result in
//            print("success")
//            print(result)
//        }
//        try? sharer.share()
    }
    
    
    func shareToInstagram() {
        
        //let instagramURL = NSURL(string: "instagram://app")
        let instagramURL = NSURL(string: "http://www.instagram.com")
        
        if (UIApplication.shared.canOpenURL(instagramURL! as URL)) {
            let captionString = "caption"
            var fileURL : URL?
            var data : Data?
            if videoURL != nil{
                //let nnn = URL(fileURLWithPath: videoURL)
                encodeVideo(videoURL:videoURL!, success: { firstURL in
                self.documentController = UIDocumentInteractionController(url: firstURL as! URL)
                self.documentController.delegate = self
                self.documentController.uti = "com.instagram.exlusivegram"
                
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                }, failure: { error in
                    print(error)
                })
                /*
                encodeVideo(videoURL:videoURL!, success: { firstURL in
                do {
                    data = try NSData(contentsOf: firstURL, options: .mappedIfSafe) as Data
                    //let fileURLnn = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("test.mp4")
                    let tempDirectory = FileManager().temporaryDirectory
                    var fileURLnn = tempDirectory.appendingPathComponent("postingVideo")
                    fileURLnn = fileURLnn.appendingPathExtension("mp4")
                    do {
                        try data?.write(to: fileURLnn, options: .atomic)
                        fileURL = fileURLnn
                        self.documentController = UIDocumentInteractionController(url: fileURL as! URL)
                        
                        self.documentController.delegate = self
                        self.documentController.uti = "com.instagram.exlusivegram"
                        
                        self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                        self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                    } catch {
                        print(error)
                    }
                } catch {
                    print(error)
                }
                }, failure: { error in
                    print(error)
                })
               */
            /*
            if videoURL != nil{
                encodeVideo(videoURL:videoURL!, success: { firstURL in
                    
                   let url = firstURL
                    do {
                        data = try NSData(contentsOf: url, options: .mappedIfSafe) as Data
                        //let fileURLnn = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("test.mp4")
                        let tempDirectory = FileManager().temporaryDirectory
                        var fileURLnn = tempDirectory.appendingPathComponent("postingVideo")
                        fileURLnn = fileURLnn.appendingPathExtension("igo")
                        do {
                            try data?.write(to: fileURLnn, options: .atomic)
                            fileURL = fileURLnn
                            self.documentController = UIDocumentInteractionController(url: fileURL as! URL)
                            
                            self.documentController.delegate = self
                            self.documentController.uti = "com.instagram.exlusivegram"
                            
                            self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                            self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                        } catch {
                            print(error)
                        }
                    } catch {
                        print(error)
                        return
                    }
                }, failure: { error in
                    print(error)
                })
                
               */
            }else{
             
                /*
            let imageData = UIImageJPEGRepresentation(imgFeed!, 100)
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
            let fileURLnn = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("test.jpg")
                do {
                    try imageData?.write(to: fileURLnn, options: .atomic)
                } catch {
                    print(error)
                }
               fileURL = fileURLnn
               self.documentController = UIDocumentInteractionController(url: fileURL as! URL)
                self.documentController.delegate = self
                self.documentController.uti = "com.instagram.exlusivegram"
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
               self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
               */
                
                let imageData = imgFeed!.jpegData(compressionQuality: 100)
                let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
                let fileURLnn = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("test.jpg")
                do {
                    try imageData?.write(to: fileURLnn, options: .atomic)
                } catch {
                    print(error)
                }
                fileURL = fileURLnn
                
                if self.arrFeedImages.count>1{
                    self.activityController = UIActivityViewController.init(activityItems: self.arrFeedImages, applicationActivities: nil)
                }else{
                    self.activityController = UIActivityViewController.init(activityItems: [fileURL as Any], applicationActivities: nil)
                }
                self.activityController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.mail, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.markupAsPDF, UIActivity.ActivityType.message, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.print, UIActivity.ActivityType.saveToCameraRoll ]
                //UIActivityType.postToFacebook
                //UIActivityType.postToTwitter
                self.present(self.activityController, animated: true, completion: nil)
            }
        } else {
            print(" Instagram isn't installed ")
        }
    }

    func callWebserviceForSearchTags(dicParam: [String: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.requestPost(strURL: WebURL.tagSearch, params: dicParam, success: { response in
            self.suggesionArray.removeAll()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
            let arr = response["allTags"] as! [[String:Any]]
            if arr.count>0{
                for dic in arr {
                    let obj = suggessions(dict: dic)
                    self.suggesionArray.append(obj!)
                }
                self.tblSuggesion.reloadData()
                self.suggesionView.isHidden = false
            }else{
                self.suggesionView.isHidden = true
            }
            
            }}){ error in
            //showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
}

//MARK: - Others
private extension NewPostVC{
    
    //MARK: - Encode Video
    func encodeVideo(videoURL: URL, success:@escaping(URL) ->Void, failure:@escaping (Error) ->Void )  {
        
        objAppShareData.isFileIsUploding = true
        //// Test
        self.uploadStartNotification()
        ////
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        //Create Export session
        let
        exportSession = AVAssetExportSession(asset:avAsset, presetName: AVAssetExportPresetPassthrough) // AVAssetExportPreset640x480
        //Creating temp path to save the converted video
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4")?.absoluteString
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath: filePath! as NSURL)
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: myDocumentPath!) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath!)
            }
            catch let error {
                //print(error)
            }
        }
        
        exportSession!.outputURL = filePath
        exportSession!.outputFileType = AVFileType.mp4
        exportSession!.shouldOptimizeForNetworkUse = true
        
        exportSession!.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession!.status {
                
            case .failed:
                failure((exportSession?.error)!)
            case .cancelled:
                //print("Export canceled")
                break
            case .completed:
                //Video conversion finished
                DispatchQueue.main.async{
                    success((exportSession?.outputURL)!)
                }
            // self.addVideo(toVC: (exportSession?.outputURL)!)
            default:
                break
            }
        })
    }
    
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    func handleResponse(response:[String:Any]) {
        let strStatus = response["status"] as? String ?? ""
        objAppShareData.isFileIsUploding = false
        if strStatus == k_success{
            objAppShareData.isFileIsUploding = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName.postUploaded), object: nil)
            objWebserviceManager.StopIndicator()
            //NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataAfterPost), name: NSNotification.Name(rawValue: notificationName.postUploaded), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideUploadLoader"), object: nil, userInfo: nil)
            //objAppShareData.isFileIsUploding = false
            /*
            let alertController = UIAlertController(title: strStatus, message: "Post uploaded successfully", preferredStyle: .alert)
            let subView = alertController.view.subviews.first!
            let alertContentView = subView.subviews.first!
            alertContentView.backgroundColor = UIColor.gray
            alertContentView.layer.cornerRadius = 20
            let btn1 = UIAlertAction(title: "Ok", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                objAppShareData.isFileIsUploding = false
            })
            alertController.addAction(btn1)
            self.window?.rootViewController?.present(alertController, animated: true, completion: {
            })*/
        }
        objAppShareData.objAppdelegate.endBackgroundTask()
    }
    
    func data(fromURL:URL) ->Data{
        let data = Data()
        do {
            let Video = try Data.init(contentsOf:fromURL)
            return Video
        } catch {
            print(error)
        }
        return data
    }
    
    func uploadStartNotification(){
        
        let alertController = UIAlertController(title:nil,message:"Uploading start...", preferredStyle: .alert)
        let subView = alertController.view.subviews.first!
        let alertContentView = subView.subviews.first!
        alertContentView.backgroundColor = UIColor.gray
        alertContentView.layer.cornerRadius = 24
        self.present(alertController, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.dismiss(animated: true, completion: {                
                objAppShareData.isSwitchToFeedTab = true
                objAppShareData.selectedTab = 3
                objAppShareData.arrTagePeopleNameIds.removeAll()
                objAppShareData.arrTagePeopleNameData.removeAll()
                objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
                //self.navigationController?.popViewController(animated: true)
            })
        }
        /*
         let notiLable = UILabel(frame: CGRect(x:0,y:0, width: 180, height:50))
         notiLable.center = view.center
         notiLable.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
         notiLable.text = "Uploading start..."
         notiLable.textAlignment = .center
         notiLable.textColor = UIColor.white
         notiLable.font = UIFont.init(name:"Raleway-Medium", size: 18.0)
         notiLable.layer.cornerRadius = 25.0 ;
         notiLable.layer.masksToBounds = true;
         notiLable.alpha = 1.0
         self.view.addSubview(notiLable)
         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
         notiLable.alpha = 0.1
         UIView.animate(withDuration:2.0, animations: {
         self.view.layoutIfNeeded()
         }, completion: { bool in
         if bool {
         notiLable.removeFromSuperview()
         self.navigationController?.popViewController(animated: true)
         }
         })
         }*/
    }
}

// MARK:- UITableView Delegate and Datasource
extension NewPostVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == self.tblSuggesion{
            return self.suggesionArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        if tableView == self.tblSuggesion{
            var cellId = ""
            if suggesionType == "user"{
                cellId = "mentions"
            }else{
                cellId = "hashTag"
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SuggessionTableCell
            
            return cell
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == self.tblSuggesion {
            self.tableView(self.tblSuggesion, willDisplay: cell as! SuggessionTableCell, forRowAt: indexPath)
        }
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
        let objSuggession = self.suggesionArray[indexPath.row]
        if suggesionType == ""{
            cell.lblTag.text = "#"+objSuggession.tag
        }else{
            cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
            cell.lblUserName.text = objSuggession.userName
            cell.imgProfile.image = UIImage.customImage.user
            if objSuggession.profileImage.count > 0 {
                //cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
                cell.imgProfile.sd_setImage(with: URL(string:objSuggession.profileImage)!, placeholderImage: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == self.tblSuggesion{
            
            if self.suggesionArray.count >= indexPath.row{
            }else{
               return
            }
            
            let objSuggession = self.suggesionArray[indexPath.row]
            var array = self.txtDescription.text.components(separatedBy:CharacterSet.whitespacesAndNewlines)
            
            let indexWord = array[currentWordIndex]
            
            // Get multiple ranges of all matched strings
            let subS = self.txtDescription.text.ranges(of: indexWord)
            
            if let selectedRange = self.txtDescription.selectedTextRange {
                // Get current index text position
                let cursorPosition = self.txtDescription.offset(from: self.txtDescription.beginningOfDocument, to: selectedRange.start)
                
                
                for sub in subS{
                    
                    let lowR = sub.lowerBound.encodedOffset
                    let upR = sub.upperBound.encodedOffset
                    
                    if cursorPosition >= lowR && cursorPosition <= upR{
                        
                        if indexWord.hasPrefix("@"){
                            var pre = ""
                            for character in indexWord {
                                if character == "@" {
                                    pre = pre+"@"
                                }else{
                                    break
                                }
                            }
                            
                            self.txtDescription.text = self.txtDescription.text.replacingCharacters(in: sub, with: pre+"\(objSuggession.userName) ")
                            self.arrPeopleTagSuggesionArray.append(objSuggession)
                            
                            break
                        }else if indexWord.hasPrefix("#"){
                            var pre = ""
                            for character in indexWord {
                                if character == "#"{
                                    pre = pre+"#"
                                }else{
                                    break
                                }
                            }
                            self.txtDescription.text = self.txtDescription.text.replacingCharacters(in: sub, with: pre+"\(objSuggession.tag) ")
                            break
                        }else{
                            
                        }
                    }
                }
            }
            self.suggesionView.isHidden = true
            self.suggesionArray.removeAll()
            self.tblSuggesion.reloadData()
        }
    }
}
// MARK: - keyboard methods
fileprivate extension NewPostVC{
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        UIView.animate(withDuration: 0.4) {
            //self.dataScrollView.contentOffset.y = self.view.frame.size.height * 0.15
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.4) {
            self.dataScrollView.contentOffset.y = 0
            self.suggesionView.isHidden = true
        }
    }
}
//MARK: - search tags and users
fileprivate extension NewPostVC{
    func searchEntries(withSubstring substring: String, andType Type:String) {
        var searchStr = ""
        if Type == ""{
            searchStr = substring.replacingOccurrences(of: "#", with: "")
        }else{
            searchStr = substring.replacingOccurrences(of: "@", with: "")
        }
        //let searchStr = substring.dropFirst()
        if !(searchStr == "") {
            let dicParam = ["search":searchStr,
                            "type":Type] as [String : Any]
            self.callWebserviceForSearchTags(dicParam: dicParam)
        }
    }
    
    func getTagsFromString(string:String) -> String{
        var result = ""
        var wordsInSentence = string.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        
        var arrTag = [String]()
        for str in wordsInSentence{
            var fStr = ""
            if str.hasPrefix("#"){
                fStr = str
                for character in str {
                    if character == "#"{
                        fStr = fStr.dropFirst().description
                    }else{
                        break
                    }
                }
                
                arrTag.append(fStr)
            }
        }
        result = arrTag.joined(separator:",")
        return result
    }
    
    func getPeopleTagsFromString(string:String) -> [String]{
        
        // var result = ""
        
        var strings = string

        
        for obj in objAppShareData.arrTagePeopleNameIds{
            let name = obj["name"] as? String ?? ""
            strings = strings+" @"+name
        }
        strDescriptionToUpload = strings
        var wordsInSentence = strings.components(separatedBy: CharacterSet.whitespacesAndNewlines)
 
        var arrTag = [String]()
        for str in wordsInSentence{
            var fStr = ""
           
            if str.hasPrefix("@"){
                fStr = str
                for character in str {
                    if character == "@"{
                        fStr = fStr.dropFirst().description
                    }else{
                        break
                    }
                }
                for obj in objAppShareData.arrTagePeopleNameIds{
                    let name = obj["id"] as? String ?? ""
                    arrTag.append(name)
                }
                if arrPeopleTagSuggesionArray.count > 0{
                    
                    for objSuggesion in arrPeopleTagSuggesionArray {
                        if objSuggesion.userName == fStr {
                            let strTagID = "\(objSuggesion._id)"
                            
                            if strTagID != "" && !arrTag.contains(strTagID) {
                                arrTag.append(strTagID)
                            }
                        }
                    }
                }

            }
        }
        //result = arrTag.joined(separator:",")
        return arrTag
    }
}

// MARK:- Custom Methods
extension NewPostVC : AddTagVCDelegate{
    
    func finishTagSelectionWithData(arrFeedImagesWithTagInfo: [PhotoInfo]) {
        navigationController?.popViewController(animated: true)
        objAppShareData.arrTagePeopleNameIds.removeAll()
        objAppShareData.arrTagePeopleNameData.removeAll()

        self.arrSelectedFeedImagesWithTagInfo = arrFeedImagesWithTagInfo
        
        guard arrFeedImagesWithTagInfo.count > 0 else {
            self.lblTagCount.isHidden = true
            return
        }
        
        var arr = [String]()
        var totalTags = 0
        for objPhotoInfo in arrFeedImagesWithTagInfo {
            for obj in objPhotoInfo.arrTags{
                if !arr.contains(obj.text){
                   totalTags = totalTags + 1
                }
                arr.append(obj.text)
            }
        }
//        var totalTags = 0
//        for objPhotoInfo in arrFeedImagesWithTagInfo {
//            totalTags = totalTags + objPhotoInfo.arrTags.count
//        }
        if totalTags > 0{
            self.lblTagCount.isHidden = false
            self.lblTagCount.text = "\(totalTags)"
        }else{
            self.lblTagCount.isHidden = true
        }
    }
    
    
    func finishTagSelectionWithData(arrSelectedTag: [String]) {
        
        if arrSelectedTag.count > 0{
            self.lblTagCount.isHidden = false
            self.lblTagCount.text = "\(arrSelectedTag.count)"
        }else{
            self.lblTagCount.isHidden = true
        }
        objAppShareData.arrTagePeopleNameIds.removeAll()
        objAppShareData.arrTagePeopleNameData.removeAll()

        navigationController?.popViewController(animated: true)
    }
    
    func gotoAddTagVC(){
        
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Tag", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"AddTagVC") as? AddTagVC {
            objVC.delegate = self
            objVC.arrFeedImages = self.arrSelectedFeedImagesWithTagInfo
            objVC.isTypeIsUrl = false
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    func gotoAddTagTextVC(){
        
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Tag", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"AddTagOnTextVC") as? AddTagOnTextVC {
           // objVC.delegate = self
            //objVC.arrFeedImages = self.arrSelectedFeedImagesWithTagInfo
            //objVC.isTypeIsUrl = false
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

