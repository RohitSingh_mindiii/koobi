//
//  presentAddFeedVC.swift
//  Mualab
//
//  Created by Mac on 30/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class presentAddFeedVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        if objAppShareData.isSwitchToFeedTab {
            objAppShareData.isSwitchToFeedTab = false
            self.tabBarController?.selectedIndex =  3
            return
        }
            
        if objAppShareData.isCloseAddFeed {
            objAppShareData.isCloseAddFeed = false
            self.tabBarController?.selectedIndex =  objAppShareData.selectedTab
            return
        }
                
        let objAddFeed = UIStoryboard(name:"Add", bundle: Bundle.main).instantiateViewController(withIdentifier: "addNav") as! UINavigationController
        objAddFeed.modalPresentationStyle = .fullScreen
        self.present(objAddFeed,animated: true,completion: nil)
        super.viewWillAppear(animated)
    }
    
}
