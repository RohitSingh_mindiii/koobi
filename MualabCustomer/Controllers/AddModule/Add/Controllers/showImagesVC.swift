//
//  showImagesVC.swift
//  Mualab
//
//  Created by Mac on 10/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

let colorGray = UIColor(red: 148.0 / 255.0, green: 148.0 / 255.0, blue: 148.0 / 255.0, alpha: 1.0)

class feedData {
    var feedPost : String = ""
    var videoThumb: String = ""
    
    init?(dict : [String:Any]){
        if let feedPost = dict["feedPost"] as? String{
            self.feedPost = feedPost
        }
        if let videoThumb = dict["videoThumb"] as? String{
            self.videoThumb = videoThumb
        }
    }
}

class showImagesVC: UIViewController , UIScrollViewDelegate {
    
    fileprivate var scrollSize: CGFloat = 300
    
    var arrFeedImages = [Any]()
    var isTypeIsUrl = false
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView : UIScrollView!

    @IBOutlet weak var pageView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            self.scrollSize = self.view.frame.size.width
            self.setPageControll()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Local Methods
    private func setPageControll(){
        self.scrollView.maximumZoomScale=4
        self.scrollView.bounces=false
        self.scrollView.zoomScale = self.scrollView.minimumZoomScale
        
        self.scrollView.delegate = self
        self.scrollView.contentSize = CGSize(width: scrollSize * CGFloat(arrFeedImages.count), height: self.scrollView.frame.size.height)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator=false
        
        pageControl.numberOfPages = arrFeedImages.count
        
        for index in  0..<arrFeedImages.count {
            let view = UIImageView(frame: CGRect(x: 0,y:0,width: scrollSize, height: scrollSize))
            let pageScrollView = UIScrollView(frame: CGRect(x: CGFloat(index) * scrollSize, y: 0, width: scrollSize, height: self.pageView.frame.size.height))
            view.tag = 1
            pageScrollView.minimumZoomScale = 1.0;
            pageScrollView.maximumZoomScale = 3.0;
            pageScrollView.zoomScale = 1.0;
           // pageScrollView.contentSize = view.bounds.size;
            pageScrollView.delegate = self;
            pageScrollView.showsHorizontalScrollIndicator = false;
            pageScrollView.showsVerticalScrollIndicator = false;
            view.backgroundColor = colorGray
            if isTypeIsUrl{
                view.af_setImage(withURL:URL.init(string:(arrFeedImages[index] as! feedData).feedPost)!)
            }else{
                view.image = arrFeedImages[index] as? UIImage
            }
            pageScrollView.addSubview(view)
            self.scrollView.addSubview(pageScrollView)
        }
    }
    
    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView{
        let pageWidth: CGFloat = scrollView.frame.size.width
            pageControl.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        if scrollView != self.scrollView {
            return scrollView.viewWithTag(1)
        }
        return nil
    }
    
    // MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
