//
//  CellFutureBooking.swift
//  MualabCustomer
//
//  Created by mac on 31/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol CellFutureBookingDelegate{
    func btnSendTapped(at index:IndexPath)
    func btnRequestTapped(at index:IndexPath)
}


class CellFutureBooking: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblBookingType: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblServiceCategory: UILabel!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var Collection: UICollectionView!
    @IBOutlet weak var collectionView: UIView!
    @IBOutlet weak var imgVwSend: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!

    var arrServices = [String]()
    weak var delegate:CellFutureBookingDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        //delegate.btnSendTapped(at: indexPath)
    }
    
    @IBAction func btnRequestAction(_ sender: Any) {
        delegate.btnRequestTapped(at: indexPath)
    }
    func setDataInCollection(obj:PastFutureBookingData){
        arrServices.removeAll()
        arrServices = obj.arr_artistService
        self.Collection.reloadData()
    }
}

//MARK:- Collection view Delegate Methods
extension CellFutureBooking:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrServices.count >= 3 {
            return 2
        }else{
            return arrServices.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        cell.lblUserName.text = obj
        let a = cell.lblUserName.layer.frame.height
        cell.lblUserName.layer.cornerRadius = 8
        cell.lblUserName.layer.masksToBounds = true
        cell.lblBorder.layer.cornerRadius = 9
        cell.lblBorder.layer.masksToBounds = true
        cell.btnService.superview?.tag = self.indexPath.row
        cell.btnService.tag = indexPath.row
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 20
        cellWidth = CGFloat(self.collectionView.frame.size.width-2)
        
        let a = arrServices[indexPath.row]
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 11)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+10
        cellHeight = 20
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
