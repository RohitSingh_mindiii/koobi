//
//  CellPastFutureReview.swift
//  MualabCustomer
//
//  Created by mac on 31/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView

@objc protocol CellPastFutureReviewDelegate{
    func btnDropDownTappedInReviewCell(at index:IndexPath)
    func btnSubmitTapped_ReviewCell(at index:IndexPath)
    func btnRebookTapped_ReviewCell(at index:IndexPath)
}

class CellPastFutureReview: UITableViewCell, UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var Collection: UICollectionView!
    @IBOutlet weak var collectionView: UIView!
    
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var vwArtistRating: HCSStarRatingView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    
    @IBOutlet weak var lblHyperLink: UILabel!
    @IBOutlet weak var lblTextLimit: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!

    @IBOutlet weak var vwCellBackground: UIView!
    @IBOutlet weak var lblServiceCategory: UILabel!

    //description view
    @IBOutlet weak var viewDescription: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnRebook: UIButton!
    @IBOutlet weak var txtViewComment: UITextView!
    var arrServices = [String]()
    weak var delegate:CellPastFutureReviewDelegate!
    var indexPath:IndexPath!
    var reviewLimit = 20
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtViewComment.delegate = self
        addAccesorryToKeyBoard()
    }
    func setDataInCollection(obj:PastFutureBookingData){
        arrServices.removeAll()
        arrServices = obj.arr_artistService
        self.Collection.reloadData()
    }
    func addAccesorryToKeyBoard(){
       
        let keyboardDoneButtonView = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: 40))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.done, target: self, action: #selector(self.resignKeyBoard))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)

        txtViewComment.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func resignKeyBoard(){
        txtViewComment.endEditing(true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnDropDownAction(_ sender: Any) {
        delegate.btnDropDownTappedInReviewCell(at: indexPath)
    }
    
    @IBAction func btnSubmitAction_ReviewCell(_ sender: Any) {
        delegate.btnSubmitTapped_ReviewCell(at: indexPath)
    }
    
    @IBAction func btnRebookAction_ReviewCell(_ sender: Any) {
        delegate.btnRebookTapped_ReviewCell(at: indexPath)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: text)
        if replacementText.contains("  "){
            return false
        }
        
        
        let str = (textView.text + text)
        if str.count <= reviewLimit {
            return true
        }
        textView.text = str.substring(to: str.index(str.startIndex, offsetBy: reviewLimit))
        
        return false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let str = "\(reviewLimit - textView.text.count)" + "/\(reviewLimit)"
       
       // self.lblTextLimit.text = "\(reviewLimit - textView.text.count)"
    }
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if arrServices.count >= 3 {
                return 2
            }else{
                return arrServices.count
            }
        }
        
        // make a cell for each cell index path
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cellIdentifier = "SubsubServicesCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
            let obj = arrServices[indexPath.row]
            //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.lblUserName.text = obj
            let a = cell.lblUserName.layer.frame.height
            cell.lblUserName.layer.cornerRadius = 8
            cell.lblUserName.layer.masksToBounds = true
            cell.lblBorder.layer.cornerRadius = 9
            cell.lblBorder.layer.masksToBounds = true
            cell.btnService.superview?.tag = self.indexPath.row
            cell.btnService.tag = indexPath.row
            return cell
        }
        
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            var cellWidth:CGFloat = 0
            var cellHeight:CGFloat = 20
            cellWidth = CGFloat(self.collectionView.frame.size.width-2)
            
            let a = arrServices[indexPath.row]
            
            var sizeOfString = CGSize()
            if let font = UIFont(name: "Nunito-Regular", size: 11)
            {
                let finalDate = a
                let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
            }
            cellWidth = sizeOfString.width+10
            cellHeight = 20
            
            return CGSize(width: cellWidth, height: cellHeight)
        }

}



//==========================================

@objc protocol CellSmallPastFutureReviewDelegate{
    func btnDropDownTappedInSmallReviewCell(at index:IndexPath)
}


class CellSmallPastFutureReview: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var vwArtistRating: HCSStarRatingView!
    @IBOutlet weak var Collection: UICollectionView!
    @IBOutlet weak var collectionView: UIView!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblHyperLink: UILabel!

    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    //@IBOutlet weak var lblAmount: UIButton
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    var arrServices = [String]()
 
    weak var delegate:CellSmallPastFutureReviewDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setDataInCollection(obj:PastFutureBookingData){
        arrServices.removeAll()
        arrServices = obj.arr_artistService
        self.Collection.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnDropDownAction(_ sender: Any) {
        delegate.btnDropDownTappedInSmallReviewCell(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrServices.count >= 3 {
            return 2
        }else{
            return arrServices.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        cell.lblUserName.text = obj
        let a = cell.lblUserName.layer.frame.height
        cell.lblUserName.layer.cornerRadius = 8
        cell.lblUserName.layer.masksToBounds = true
        cell.lblBorder.layer.cornerRadius = 9
        cell.lblBorder.layer.masksToBounds = true
        cell.btnService.superview?.tag = self.indexPath.row
        cell.btnService.tag = indexPath.row
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 20
        cellWidth = CGFloat(self.collectionView.frame.size.width-2)
        
        let a = arrServices[indexPath.row]
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 11)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+10
        cellHeight = 20
        
        return CGSize(width: cellWidth, height: cellHeight)
    }

}

