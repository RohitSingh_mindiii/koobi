//
//  cellPastFutureBooking.swift
//  MualabCustomer
//
//  Created by mac on 31/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView

@objc protocol CellPastFutureRatingDelegate{
    func btnDropDownTappedInRatingCell(at index:IndexPath)
}

class cellPastFutureRating: UITableViewCell {
    
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var viewDescription: UIView!
    
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName:  UILabel!
    @IBOutlet weak var lblDate:  UILabel!
    @IBOutlet weak var lblAmount:UILabel!
    
    @IBOutlet weak var imgVwDropDown: UIImageView!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var lblCommet1:  UILabel!
    @IBOutlet weak var lblCommet2:  UILabel!
    
    @IBOutlet weak var heightCell: NSLayoutConstraint!
    
    weak var delegate:CellPastFutureRatingDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnDropDownAction(_ sender: Any) {
        delegate.btnDropDownTappedInRatingCell(at: indexPath)
    }
    
}


@objc protocol CellSmallPastFutureRatingDelegate{
    func btnDropDownTappedInSmallRatingCell(at index:IndexPath)
}

class cellSmallPastFutureRating: UITableViewCell {
    
    @IBOutlet weak var vwRating: HCSStarRatingView!
  
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName:  UILabel!
    @IBOutlet weak var lblDate:  UILabel!
    @IBOutlet weak var lblAmount:UILabel!
    
    @IBOutlet weak var imgVwDropDown: UIImageView!
    @IBOutlet weak var btnDropDown: UIButton!
    
 
    weak var delegate:CellSmallPastFutureRatingDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnDropDownAction(_ sender: Any) {
        delegate.btnDropDownTappedInSmallRatingCell(at: indexPath)
    }
}
