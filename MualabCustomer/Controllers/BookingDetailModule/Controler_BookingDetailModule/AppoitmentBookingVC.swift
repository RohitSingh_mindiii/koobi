
//
//  AppoitmentBookingVC.swift
//  MualabBusiness
//
//  Created by mac on 24/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class AppoitmentBookingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var objPastFutureBookingData : PastFutureBookingData = PastFutureBookingData.init(dict: ["": ""])
    
    var businessNameType = ""
    var strArtistId = ""
    var strBookingId = ""
    var strUserId : String = ""
    
    var strArtistName = ""
    var strSelectedTableType = "" // "Today"
    var fromStaffSelectId = ""
    
    var isBookingCompleted  = false
    
    @IBOutlet weak var lblCashType: UILabel!
    @IBOutlet weak var lblTansactionId: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblTotolPrice: UILabel!
    
    @IBOutlet weak var btnPaymentBooking: UIButton!
    
    //top header view
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    //oppoitment date and time outlate
    @IBOutlet weak var lblOppoitmentDate: UILabel!
    @IBOutlet weak var lblOppoitmentTime: UILabel!
    
    //oppoitment Location outlate
    @IBOutlet weak var lblLocation: UILabel!
    
    //oppoitment table  outlate
    @IBOutlet weak var tblAppoitment: UITableView!
    @IBOutlet weak var heightTableview: NSLayoutConstraint!
    
    //childTableOutlate
    @IBOutlet weak var viewChildTable: UIView!
    @IBOutlet weak var tblSelectStaff: UITableView!
    
    //view AcceptReject Button  outlate
    @IBOutlet weak var viewAcceptRejectButton: UIView!
    @IBOutlet weak var viewButtonCollection: UIView!
    
    //appintment cancel outlate
    @IBOutlet weak var viewCancelButton: UIView!
    @IBOutlet weak var viewCancelButton2: UIView!
    
    @IBOutlet weak var viewAlertCancel: UIView!
    
    var BookMyId = ""
    var strTableType = ""
    var strTBookingId = ""
    
    private var indexPath = 0

    private var strSelectName = ""
    private var strSelectId = ""
    
    private var arrBookDetail = [Booking]()
    private var arrBookInfo = [BookingInfo]()
   
    var arrStaffDetail = [StaffDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DelegateCallingMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.viewCancelButton.isHidden = true
        self.viewAlertCancel.isHidden = true
        self.viewChildTable.isHidden = true
        self.viewAcceptRejectButton.isHidden = true
        self.viewButtonCollection.isHidden = false
        self.btnPaymentBooking.isHidden = true
        updateArtistInfo()
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                self.strUserId = "\(userId)"
            }
        }
        
        self.strBookingId = "\(objPastFutureBookingData._id)"
        callWebserviceForGet_BookingDetail(dict: ["bookingId":self.strBookingId])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateArtistInfo(){
        
        if objPastFutureBookingData.profileImage != "" {
            if let url = URL(string: objPastFutureBookingData.profileImage){
                //self.imgUserImage.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserImage.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objPastFutureBookingData.userName
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}

//MARK: - Custome method extension
extension AppoitmentBookingVC{
    
    func DelegateCallingMethod() {
      
        self.tblAppoitment.rowHeight = UITableView.automaticDimension
        self.tblAppoitment.estimatedRowHeight = 60
        
        self.tblAppoitment.delegate = self
        self.tblAppoitment.dataSource = self
        self.tblSelectStaff.delegate = self
        self.tblSelectStaff.dataSource = self
        self.imgUserImage.layer.masksToBounds = true
        self.imgUserImage.layer.cornerRadius = 24
    }
    
    func tableHeight(){
        
        if arrBookInfo.count > 1{
            tblAppoitment.isScrollEnabled = true
            if arrBookInfo.count >= 6{
                self.heightTableview.constant = CGFloat(85*6)
            }else{
                self.heightTableview.constant = CGFloat(85*arrBookInfo.count)
            }
        }else{
           self.heightTableview.constant = 87
            tblAppoitment.isScrollEnabled = false
        }
    }
    
    
    func clickAcceptRejectCountMethod(api:String, methodType:String){
        
        let obj1 = arrBookDetail[0]
        let obj2 = obj1.arrUserDetail
        let objUserInfo = obj2[0]
        let userId = objUserInfo._id
        
        let BookingId = obj1._id
        let serviceId = obj1.allServiceId
        let subServiceId = obj1.allSubServiceId
        let artistServiceId = obj1.allArtictServiceId
       
        let param = ["artistId":self.strArtistId,
                     "userId":userId,
                     "bookingId":BookingId,
                     "serviceId":serviceId,
                     "subserviceId":subServiceId,
                     "artistServiceId":artistServiceId,
                     "type":methodType,
                     "paymentType":obj1.paymentType,
                     ] as [String : Any]
        
        self.callWebserviceForGet_AcceptRejectCount(dict: param, webURL: api, methodType: methodType)
    }
}
//MARK: - Tableview delegate method extension
extension AppoitmentBookingVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblAppoitment{
            tableHeight()
            return arrBookInfo.count
        }else{
            return arrStaffDetail.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblAppoitment{
            
            let cellIdentifier = "AppoitmentCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AppoitmentCell
            
            let objBookingInfo = arrBookInfo[indexPath.row]
            
            cell?.lblUserName.text = objBookingInfo.staffName
            cell?.lblServiceName.text = objBookingInfo.artistServiceName
            cell?.lblAmount.text = "£"+objBookingInfo.bookingPrice
            cell?.lblTime.text = objBookingInfo.startTime
            cell?.lblDate.text =  self.converteDateIntoNewFormate(strDateFromServer: objBookingInfo.bookingDate)
            cell?.btnChangeStaff.tag = indexPath.row
            cell?.btnChangeStaff.isHidden = true
            cell?.lblUserName.text = objBookingInfo.staffName
            cell?.btnChangeStaff.isHidden = true
            
            return cell!
            
        }else{
            
            let cellIdentifier = "SelectStaffAppoitmentCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SelectStaffAppoitmentCell
            
            let objData = arrStaffDetail[indexPath.row]
            
            cell?.lblUserName.text = objData.satffName
           
            if strArtistName == objData.satffName{
                cell?.lblUserName.text = "My booking"
            }
           
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblSelectStaff {
            let objData = arrStaffDetail[indexPath.row]
             strSelectName = objData.satffName
             strSelectId = objData.staffId
        }
    }
}

//MARK: - Button extension
extension AppoitmentBookingVC{
    
    @IBAction func btnPaymentBookingTapped(_ sender: UIButton) {
        self.gotoPaymentInfoVC(obj : self.objPastFutureBookingData)
    }
    
    // Accept, Reject, counter Button  action
    @IBAction func btnAcceptAction(_ sender: UIButton) {
       // clickAcceptRejectCountMethod(api: WebURL.bookingAction, methodType: "accept")
    }
    
    @IBAction func btnRejectAction(_ sender: UIButton) {
        clickAcceptRejectCountMethod(api: WebURL.bookingAction, methodType: "reject")
    }
    
    @IBAction func btnCounter(_ sender: UIButton) {
        UnderDevelopment()
    }
    
    // chat, call, location, close Button  action
    @IBAction func btnChatAction(_ sender: UIButton) {
        UnderDevelopment()
    }
    
    @IBAction func btnCallAction(_ sender: UIButton) {
        UnderDevelopment()
    }
    
    @IBAction func btnLocationAction(_ sender: UIButton) {
        UnderDevelopment()
    }
    
    //cancle appont requiest button method
    @IBAction func btnCloseAction(_ sender: UIButton) {
         self.viewAlertCancel.isHidden = false
    }
    
    @IBAction func btnDoneCancelAppointment(_ sender: UIButton) {
        clickAcceptRejectCountMethod(api: WebURL.bookingAction, methodType: "cancel")
    }
    
    @IBAction func btnNoCancleAppointment(_ sender: UIButton) {
        self.viewAlertCancel.isHidden = true

    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        objAppShareData.clearNotificationData()
        navigationController?.popViewController(animated: true)
    }
    
    func UnderDevelopment(){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
}

//MARK:- Webservice Call

extension AppoitmentBookingVC {
    
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String,  methodType:String){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: webURL, params: dict  , success: {
            response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? ""
                if strStatus == k_success{
                    
                    if webURL != WebURL.changeStaff {
                        self.viewAcceptRejectButton.isHidden = true
                        self.viewAlertCancel.isHidden = true
                    }
                    
                    if methodType == "complete" {
                        
                        // self.callWebserviceForGet_BookingDetail(dict: ["bookingId":objAppShareData.objAppoitmentBookingVC.strTBookingId])
                        
                    }else{
                        
                        if strMsg == "Booking has been rejected"{
                            
                            self.viewCancelButton.isHidden = true
                            self.viewCancelButton2.isHidden = true
                            objAppShareData.showAlert(withMessage: "Booking has been cancelled", type: alertType.bannerDark, on: self)
                            self.removeChangeStaffButton()
                        }else{
                            objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                        }
                    }
                    
                    //self.callWebserviceForGet_BookingDetail(dict: ["bookingId":objAppShareData.objAppoitmentBookingVC.strTBookingId])
                    
                    self.callWebserviceForGet_BookingDetail(dict: ["bookingId":self.strBookingId])
                    
                    
                }else{
                   
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGet_BookingDetail(dict: [String : Any]){
       
        self.arrBookDetail.removeAll()
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.bookingDetail, params: dict  , success: { response in
            print(response)
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    self.tblAppoitment.reloadData()
                    if let msg = response["message"] as? String {
                         objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                   
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
       
        //if let arr = response["bookingDetails"] as? [[String:Any]]{
        if let arr = response["bookingInfo"] as? [[String:Any]]{
            if arr.count > 0{
                for dict in arr{
                    let objBooking = Booking.init(dict: dict)
                    
                    if let artistId = dict["artistId"] as? Int{
                        self.strArtistId = String(artistId)
                    }else if let artistId = dict["artistId"] as? String{
                        self.strArtistId = artistId
                    }
                    
                    self.arrBookInfo = objBooking.arrBookingInfo
                    self.setDataOnUI(objBooking : objBooking)
                    self.arrBookDetail.append(objBooking)
                }
            }
            
//            if businessNameType == "business"{
//
//            }else{
//                let arr1 = self.arrBookInfo
//                self.arrBookInfo.removeAll()
//                for obj1 in arr1{
//                    if obj1.staffId == strArtistId{
//                        self.arrBookInfo.append(obj1)
//                    }
//                }
//            }
            
            self.tblAppoitment.reloadData()
        }
        
        
    }
    
    
    func setDataOnUI(objBooking : Booking ) {
        
        self.lblOppoitmentDate.text = self.converteDateIntoNewFormate(strDateFromServer:objBooking.bookingDate)
        self.lblOppoitmentTime.text = objBooking.bookingTime
        self.lblLocation.text = objBooking.location
        
        // paymentType : 2 = online, 3 = Cash
        if objBooking.paymentType == "2" {
            self.lblCashType.text = "Online"
        }else if objBooking.paymentType == "3" {
            self.lblCashType.text = "Cash"
        }else{
            self.lblCashType.text = ""
        }
        
        if objBooking.transactionId == "" {
            self.lblTansactionId.text = "-"
        }else {
            self.lblTansactionId.text = objBooking.transactionId
        }
        
//        let objUserDetail = objBooking.arrUserDetail[0]
//
//        let url = URL(string: objUserDetail.profileImage)
//        if  url != nil {
//            self.imgUserImage.af_setImage(withURL: url!)
//        }else{
//            self.imgUserImage.image = #imageLiteral(resourceName: "cellBackground")
//        }
        
        let doublePrice = Double(objBooking.totalPrice) ?? 0
        let doubleStr = String(format: "%.2f", (doublePrice * 100)/100)
        
        self.objPastFutureBookingData.totalPrice = doubleStr
        
        let strTotalPrice = "£" + doubleStr
        self.lblTotolPrice.text = strTotalPrice
       
        let strTotal = "Pay £" + doubleStr
        self.btnPaymentBooking.setTitle(strTotal, for: .normal)
        
        if objBooking.bookStatus == "0" ||  objBooking.bookStatus == "1" {
            self.viewCancelButton.isHidden = false
            self.viewCancelButton2.isHidden = true
            
        }else if objBooking.bookStatus == "2" ||  objBooking.bookStatus == "3" {
            self.viewCancelButton.isHidden = true
            self.viewCancelButton2.isHidden = true
        }
        
        //bookStatus :0 = pending 1 = Confirmed, 2 = Cancelled, 3 = completed,
        
        if objBooking.bookStatus == "0"{
            self.lblBookingStatus.text = "A waiting confirmation"
            //self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
        }else if objBooking.bookStatus == "1"{
            self.lblBookingStatus.text = "Booking confirmed"
            // self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
        }else if objBooking.bookStatus == "2" {
            self.lblBookingStatus.text = "Booking cancelled"
            //self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
        }else if objBooking.bookStatus == "3" {
            
            if objBooking.paymentStatus == "1" {
                self.btnPaymentBooking.isHidden = true
                self.lblBookingStatus.text = "Booking Completed"
               
            }else{
                self.btnPaymentBooking.isHidden = false
                self.lblBookingStatus.text = "Payment pending"
            }
            
        }else{
            self.lblBookingStatus.text = ""
            self.lblBookingStatus.textColor =  UIColor.clear
        }

    }
}
//MARK:- Webservice Call for staff list

extension AppoitmentBookingVC {
    
    func parseResponceStaffList(response:[String : Any]){
        
        if let arr = response["availableStaff"] as? [[String:Any]]{
            if arr.count > 0{
                for dictStaffDetail in arr{
                    let objStaff = StaffDetail.init(dict: dictStaffDetail);
                    
                        arrStaffDetail.append(objStaff)
                }
                self.tblSelectStaff.reloadData()
                self.viewChildTable.isHidden = false
            }
        }
    }
}

//MARK: - Button extension
extension AppoitmentBookingVC{
    
    @IBAction func btnChangeStaff(_ sender: UIButton) {
    }
    
    @IBAction func btnDoneSelectStaff(_ sender: UIButton) {

    }
    
    @IBAction func btnCancleSelectStaff(_ sender: UIButton) {
        self.viewChildTable.isHidden = true
    }
    
}


extension AppoitmentBookingVC{
    
    func removeChangeStaffButton(){
        
        let objData = arrBookDetail[0]
        
        for ind in objData.arrBookingInfo{
            
            let index = objData.arrBookingInfo.index(of: ind)
            let section = 0
            let indexPath = IndexPath(row: index!, section: section)
            let cell = tblAppoitment.cellForRow(at: indexPath) as! AppoitmentCell
            cell.btnChangeStaff.isHidden = true
            cell.lblAmount.textAlignment = .right
        }
    }
}

extension AppoitmentBookingVC {
    func getDayFromSelectedDate(strDate:String)-> String{
       
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        
        switch weekDay {
            case 1:
                return "6"//"Sun"
            case 2:
                return "0"//Mon"
            case 3:
                return "1"//Tue"
            case 4:
                return "2"//"Wed"
            case 5:
                return "3"//"Thu"
            case 6:
                return "4"//"Fri"
            case 7:
                return "5"//"Sat"
            default:
                return "Day"
        }
       // return ""
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        
        // returns an integer from 1 - 7, with 1 being Sunday and 7 being Saturday
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    
    func CurrentTime() -> String{
        
       var currentTime = ""
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        
        let strTime = String(describing: TodayDate)
        
        let arr = strTime.split(separator: " ")

        let arrTime = arr[1]

        let arrTime1 = arrTime.split(separator: ":")
        var str1 = 0
        str1 = Int(arrTime1[0])!
        let str2 = arrTime1[1]
       // let str3 = arrTime1[2]

        if str1 == 12{
           currentTime = String(str1)+":"+String(str2)+" PM"
        }else if str1 > 12 {
            str1 = str1-12
            currentTime = String(str1)+":"+String(str2)+" PM"
        }else{
            currentTime = String(str1)+":"+String(str2)+" AM"
        }
        return currentTime
    }
    
    func converteDateIntoNewFormate(strDateFromServer: String) -> (String) {
        
        // "yyyy-MM-dd"  -> "dd MMM yyyy"
        // 2017-12-30  -> 30 Dec 2017
        var strConvertedDate : String = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFromServer: Date? = dateFormatter.date(from: strDateFromServer)
        if let dateFromServer = dateFromServer {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let strDate:String? = dateFormatter.string(from: dateFromServer)
            if let strDate = strDate {
                strConvertedDate = strDate
            }
        }        
        return strConvertedDate
    }
    
    func gotoPaymentInfoVC(obj : PastFutureBookingData) {
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentInfoVC") as? PaymentInfoVC {
            objVC.objPastFutureBookingData = obj
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            
            let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
}

/*
 ["status": success, "data": {
 "_id" = 15;
 artistDetail =     (
 {
 "_id" = 2;
 profileImage = "http://koobi.co.uk:3000/uploads/profile/1547529194503.jpg";
 userName = pankaj;
 }
 );
 artistId = 2;
 bookStatus = 0;
 bookingDate = "2019-01-16";
 bookingInfo =     (
 {
 "_id" = 17;
 artistServiceId = 1;
 artistServiceName = "Face makup in";
 bookingDate = "2019-01-16";
 bookingPrice = 500;
 companyId = 2;
 companyImage = "http://koobi.co.uk:3000/uploads/profile/1547529194503.jpg";
 companyName = "Patel beauty villa";
 endTime = "01:20 PM";
 serviceId = 1;
 staffId = 3;
 staffImage = "";
 staffName = anil;
 startTime = "01:00 PM";
 subServiceId = 1;
 }
 );
 bookingTime = "01:00 PM";
 customerType = online;
 discountPrice = 0;
 location = "Rave Infotech 193 \U2013 193 A, Greater Brijeshwari, Piplyahana,, Sector A";
 paymentStatus = 0;
 paymentType = 2;
 timeCount = 780;
 totalPrice = 500;
 userDetail =     (
 {
 "_id" = 10;
 profileImage = "http://koobi.co.uk:3000/uploads/profile/1547559625230.jpg";
 userName = jamesfb;
 }
 );
 voucher = "";
 }, "message": ok]

 */

