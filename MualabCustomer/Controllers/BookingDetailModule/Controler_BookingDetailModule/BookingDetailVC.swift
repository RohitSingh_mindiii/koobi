//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import HCSStarRatingView

class BookingDetailVC : UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate{
    
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderOutCallInCall: UILabel!
    @IBOutlet weak var imgArtist: UIImageView!
    @IBOutlet weak var lblOutCallAddress: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblRefundTransactionId: UILabel!
    @IBOutlet weak var tblBookedServices: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblRedTotalPrice: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var viewRedTotalPrice: UIView!
    @IBOutlet weak var viewVoucherCode: UIView!
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblVoucherText: UILabel!
    @IBOutlet weak var btnSubmitReview: UIButton!

    @IBOutlet weak var viewTrasactionId: UIView!
    @IBOutlet weak var viewPaymentStatus: UIView!
    @IBOutlet weak var lblPaymentStatusNew: UILabel!
    @IBOutlet weak var lblTrasactionId: UILabel!

    @IBOutlet weak var viewReject: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var btnPay: UIButton!
    
    //review rating view Outlet
    @IBOutlet weak var viewRatingPopup: UIView!
    @IBOutlet weak var imgCustomerRatingPopup: UIImageView!
    @IBOutlet weak var lblNameRatingPopup: UILabel!
    @IBOutlet weak var lblDetailRatingPopup: UILabel!
    @IBOutlet weak var viewRatingAssignRatingPopup: HCSStarRatingView!
    @IBOutlet weak var btnAddCommentRatingPopup: UIView!
    @IBOutlet weak var viewTxtAddCommentRatingPopup: UIView!
    @IBOutlet weak var viewReviewRatingStartButton: UIView!
    @IBOutlet weak var txtAddCommentRatingPopup: SZTextView!
    var strReviewType = "insert"
    var dictVoucher = [:] as! [String:Any]
    var totalPrice = 0.0
    var discountPrice = 0.0
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var objBookingServices = BookingServices()
    var arrBookedService = [BookingInfo]()
    var arrBookedMainService = [Booking]()
    fileprivate var timer:Timer?
    var isOutCallSelectedConfirm = false
    var strMyAddress = ""
    var strBookingId = ""
    var artistId = ""
    var myId = ""
        ///paymentTime = "Fri Mar 08 2019 11:19:08 GMT+0000 (UTC)";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshBookingDetailScreen), name: NSNotification.Name(rawValue: "refreshBookingDetailScreen"), object: nil)
        self.viewVoucherCode.isHidden = true
        self.viewPaymentStatus.isHidden = true
        self.viewTrasactionId.isHidden = true
        self.viewRatingPopup.isHidden = true
        self.viewReviewRatingStartButton.isHidden = true
        self.viewTxtAddCommentRatingPopup.isHidden = true
        self.txtAddCommentRatingPopup.delegate = self
        //self.btnCompleteService.isHidden = true
        //self.imgArtist.layer.cornerRadius = 23
        self.imgArtist.layer.masksToBounds = true
        //        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        //        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        //        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        //        if adminId == self.artistId || adminId == ""{
        //        }else{
        //            self.artistId = adminId
        //        }
        self.artistId = ""
        self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.checkPaymentTime), userInfo: nil, repeats: true)
        //self.callWebserviceForGetBookedServices()
        if objAppShareData.isFromNotification {
        }
    }
   
    @objc func refreshBookingDetailScreen(notification: Notification){
        print(notification)
        if let arr = notification.object as? [String : Any] {
            //let dict = arr[0]
            if let notiType = arr["notifyId"] as? Int{
                let notifyId = notiType
                if self.strBookingId == String(notifyId){
                    self.callWebserviceForGetBookedServices()
                }
            }
            if let notiType = arr["notifyId"] as? String{
                let notifyId = notiType
                if self.strBookingId == notifyId{
                    self.callWebserviceForGetBookedServices()
                }
            }
        }
    }
    @objc func checkPaymentTime() {
        if self.arrBookedMainService.count>0{
            let objBooking = self.arrBookedMainService[0]
        self.checkForPaymentTimeAutomaticCancel(objBooking: objBooking)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.artistId = ""
        self.callWebserviceForGetBookedServices()
    }
}

//MARK: - button extension
extension BookingDetailVC {
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under developement" , type: alertType.bannerDark,on: self)
    }
    @IBAction func btnCallAction(_ sender: UIButton) {
        let objBooking = self.arrBookedMainService[0]
        let contactNo = objBooking.arrUserDetail[0].contactNo
        let strPhoneNumber = objBooking.arrUserDetail[0].countryCode + contactNo
        if let url = NSURL(string: "tel://\(strPhoneNumber)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnTrackAction(_ sender: UIButton) {
        let sb = UIStoryboard(name:"BookingDetailModule",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"TrackMapVC") as! TrackMapVC
        let objBooking = self.arrBookedMainService[0]
        let objService = self.arrBookedService[sender.tag]
        objChooseType.hidesBottomBarWhenPushed = true
        objChooseType.objBookedMainService = objBooking
        objChooseType.objBookedService = objService
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        objAppShareData.isFromNotification = false
        if objAppShareData.isBackToSearchBoardFromBookingDetail
           {
            objAppShareData.isFromServiceTagBook = false
            objAppShareData.selectedTab = 0
            objAppShareData.isBackToSearchBoardFromBookingDetail = false
            objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnDoneAction(_ sender: UIButton) {
        objAppShareData.isFromNotification = false
        if objAppShareData.isBackToSearchBoardFromBookingDetail
        {
            objAppShareData.isFromServiceTagBook = false
            objAppShareData.selectedTab = 0
            objAppShareData.isBackToSearchBoardFromBookingDetail
                = false
            objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSubmitRatting(_ sender: Any) {
        self.view.endEditing(true)
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objMainBooking = self.arrBookedMainService[0]
        //let objUser = objMainBooking.arrUserDetail[0]
        self.txtAddCommentRatingPopup.text = self.txtAddCommentRatingPopup.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.viewRatingAssignRatingPopup.value == 0 {
            objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
//        }else if self.txtAddCommentRatingPopup.text == "" {
//            objAppShareData.showAlert(withMessage: "Please give review", type: alertType.bannerDark, on: self)
        }else{
            let date = objAppShareData.dateFormatInFormYou(forAPI: Date())
            let parameters = [
                "artistId" : objMainBooking.artistId,
                "userId" : objMainBooking.userId,//objUser._id,
                "bookingId":objMainBooking._id,
                "reviewByArtist": "",
                "ratingDate":date,
                "reviewByUser":self.txtAddCommentRatingPopup.text ?? "",
                "rating": "\(self.viewRatingAssignRatingPopup.value)",
                "type":self.strReviewType
            ]
            callWebserviceForGet_AcceptRejectCount(dict: parameters, webURL: WebURL.bookingReviewRating)
        }
    }
    @IBAction func btnCancleRatting(_ sender: Any) {
        self.view.endEditing(true)
        self.viewRatingPopup.isHidden = true
    }
    @IBAction func btnAddComment(_ sender: Any) {
        self.viewTxtAddCommentRatingPopup.isHidden = !self.viewTxtAddCommentRatingPopup.isHidden
    }
}

//MARK: - Custom methods Extension
fileprivate extension BookingDetailVC{
    
    
    func manageView(){
        //self.viewRedTotalPrice.isHidden = true
        self.manageOutCallInCallView()
        self.managePaymentMethodView()
    }
    func clearVoucherCode(){
        self.dictVoucher = [:]
        let doubleStr = String(format: "%.2f", self.totalPrice)
        //self.lblTotalPrice.text = "£" + String(self.totalPrice)
        self.lblTotalPrice.text = "£" + String(doubleStr)
        self.viewRedTotalPrice.isHidden = true
        self.txtVoucherCode.text = ""
        self.txtVoucherCode.isUserInteractionEnabled = true
    }
    func managePaymentMethodView(){
        
    }
    
    func manageOutCallInCallView(){
        
        if self.isOutCallSelectedConfirm{
            self.lblHeaderOutCallInCall.text = "Out Call"
            self.lblOutCallAddress.text = self.strMyAddress
        }else{
            self.lblHeaderOutCallInCall.text = "In Call"
        }
        if self.arrBookedMainService.count > 0{
            self.lblOutCallAddress.text = self.arrBookedMainService[0].location
            if self.arrBookedMainService[0].bookingType == "1"{
                self.lblHeaderOutCallInCall.text = "In Call"
            }else if self.arrBookedMainService[0].bookingType == "2"{
                self.lblHeaderOutCallInCall.text = "Out Call"
            }else{
                self.lblHeaderOutCallInCall.text = "Both"
            }
            let objAllBooking = self.arrBookedMainService[0]
            self.viewAddress.isHidden = true
            self.viewReject.isHidden = true
            
            //            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            //            self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
            
            //self.btnCompleteService.isHidden = true
            if objAllBooking.bookStatus == "5"{
                //if self.myId == self.artistId || self.artistId == ""   {
                var btnShow = true
                for obj in objAllBooking.arrBookingInfo{
                    if obj.status != "3" {
                        btnShow = false
                    }
                }
                if btnShow == true{
                    //self.btnCompleteService.isHidden = false
                }
                //}
            }
            
            
            if objAllBooking.bookStatus == "0" {
                self.viewReject.isHidden = false
            }
            if objAllBooking.bookStatus == "0"{
                self.lblBookingStatus.text = "Pending"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            } else if objAllBooking.bookStatus == "1"{
                self.lblBookingStatus.text = "Confirmed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                //self.viewReject.isHidden = false
            }else if objAllBooking.bookStatus == "2" {
                self.lblBookingStatus.text = "Cancelled"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
            }else if objAllBooking.bookStatus == "3" {
                self.lblBookingStatus.text = "Completed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.viewAddress.isHidden = false
                self.viewReviewRatingStartButton.isHidden = false
                self.viewAddress.isHidden = true
            }else if objAllBooking.bookStatus == "5"{
                self.lblBookingStatus.text = "In progress"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
            
        self.checkForPaymentTimeAutomaticCancel(objBooking: objAllBooking)
//            //// For Timer
//            if objAllBooking.paymentType == "1" && objAllBooking.paymentStatus == "0"{
//                self.checkForPaymentTimeAutomaticCancel(objBooking: objAllBooking)
//            }
            
            if objAllBooking.paymentType == "1"{
                self.lblPaymentStatus.text = "Card"
                self.viewTrasactionId.isHidden = false
                self.viewPaymentStatus.isHidden = false
                if objAllBooking.bookStatus == "1"{
                    if objAllBooking.paymentStatus == "0"{
                       self.btnPay.setTitle("Pay", for: .normal)
                    self.btnPay.isUserInteractionEnabled = true
                        self.lblPaymentStatusNew.text = "Pending"
                        self.lblPaymentStatusNew.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
                        self.lblTrasactionId.text = "NA"
                        self.btnPay.isHidden = false
                    }else{
                        self.btnPay.setTitle("Paid", for: .normal)
                       self.btnPay.isUserInteractionEnabled = false
                        self.lblPaymentStatusNew.text = "Completed"
                        self.lblPaymentStatusNew.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                        self.lblTrasactionId.text = objAllBooking.transactionId
                        self.btnPay.isHidden = true
                    }
                   
                }else{
                    if objAllBooking.paymentStatus == "0"{
                        self.lblPaymentStatusNew.text = "Pending"
                        self.lblPaymentStatusNew.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
                        self.lblTrasactionId.text = "NA"
                    }else{
                        self.lblPaymentStatusNew.text = "Completed"
                        self.lblPaymentStatusNew.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                        self.lblTrasactionId.text = objAllBooking.transactionId
                    }
                   self.btnPay.isHidden = true
                }
            }else{
                self.btnPay.isHidden = true
                self.lblPaymentStatus.text = "Cash"
            }
            
            //// For refund id
            self.lblRefundTransactionId.text = "Transaction Id"
            if objAllBooking.paymentStatus == "3"{
                self.lblPaymentStatusNew.text = "Refund"
                self.lblPaymentStatusNew.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
                self.lblRefundTransactionId.text = "Refund Id"
            }
            ////
        }
    }
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String) -> Date {
        let dateFormatter = DateFormatter()
        let arr = date.components(separatedBy: " ")
        var newStrDate = ""
        if arr.count >= 5{
           newStrDate = arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3] + " " + arr[4]
        }
        dateFormatter.dateFormat = "EEE MMM dd yyyy HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: newStrDate)
        //dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //return dateFormatter.string(from: dt!)
        return dt!
    }
    
    func checkForPaymentTimeAutomaticCancel(objBooking:Booking){
        //// For Timer
        if objBooking.bookStatus != "1"{
           self.timer?.invalidate()
        }
        if objBooking.paymentType == "1" && objBooking.paymentStatus == "0" && objBooking.bookStatus == "1"{
            print(objBooking.paymentTime)
            let currentDate = Date()
            if objBooking.paymentTime.count == 0{
               return
            }
            var paymentDate = UTCToLocal(date: objBooking.paymentTime)
            print(paymentDate)
            paymentDate = paymentDate.addingTimeInterval(1800)
            if currentDate > paymentDate {
               self.timer?.invalidate()
               self.rejectBookingAction()
            }
        }
    }
    
    func calculateTotalPrice(){
        var totalPrice = 0.0
        for obj in self.arrBookedService{
            totalPrice = totalPrice + Double(obj.bookingPrice)!
        }
        self.totalPrice = totalPrice
        let doubleStr = String(format: "%.2f", totalPrice)
        //self.lblTotalPrice.text = "£" + String(totalPrice)
        self.lblTotalPrice.text = "£" + String(doubleStr)
        //self.lblRedTotalPrice.text = "£" + String(totalPrice)
        self.lblRedTotalPrice.text = "£" + String(doubleStr)
    }
}

//MARK: - Webservices Extension
fileprivate extension BookingDetailVC{
    
    func manageVoucherView(dict:[String:Any]){
        self.dictVoucher = dict
        var discountType = 0
        if let discount = dict["discountType"] as? Int{
            discountType = discount
        }else if let discount = dict["discountType"] as? String{
            discountType = Int(discount)!
        }
        var amount = 0.0
        if let amnt = dict["amount"] as? Double{
            amount = amnt
        }else if let amnt = dict["amount"] as? Int{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? Float{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? String{
            amount = Double(amnt)!
        }
        
        self.lblVoucherText.text = dict["voucherCode"] as? String ?? ""
        //        if discountType == 1{
        //            self.lblVoucherText.text = ""
        //        }else if discountType == 2 {
        //            self.lblVoucherText.text = ""
        //        }
        
        if discountType == 2{
            let newPrice = self.totalPrice - (self.totalPrice*amount/100)
            if newPrice>0{
                self.discountPrice = newPrice
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-"+String(amount)+"%"
        }else{
            let newPrice = self.totalPrice-amount
            if newPrice>0{
                self.discountPrice = newPrice
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-£"+String(amount)
        }
        self.txtVoucherCode.isUserInteractionEnabled = false
        self.viewRedTotalPrice.isHidden = false
        self.txtVoucherCode.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
    }
    
    func callWebserviceForGetBookedServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let dicParam = [
            "bookingId":self.strBookingId
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            self.arrBookedService.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    print(response)
                    self.saveData(dicts:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dicts:[String:Any]){
        self.arrBookedMainService.removeAll()
        self.arrBookedService.removeAll()
        if let strBookingt = dicts["data"] as? [String : Any]{
            let objBooking = Booking.init(dict: strBookingt)
            self.arrBookedMainService.append(objBooking)
            if arrBookedMainService.count > 0{
                self.arrBookedService = self.arrBookedMainService[0].arrBookingInfo
            }
            if objBooking.arrUserDetail.count > 0{
                let objUserInfo = objBooking.arrUserDetail[0]
                self.lblArtistName.text = objUserInfo.userName
                let url = URL(string: objUserInfo.profileImage ?? "")
                if  url != nil {
                    //self.imgArtist.af_setImage(withURL: url!)
                    self.imgArtist.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }else{
                    self.imgArtist.image = #imageLiteral(resourceName: "cellBackground")
                }
            }
            self.calculateTotalPrice()
            self.viewRedTotalPrice.isHidden = true
            if let voucherCode = strBookingt["voucher"] as? [String:Any]{
                if voucherCode.count > 0{
                    self.viewVoucherCode.isHidden = false
                    self.manageVoucherView(dict: voucherCode)
                }else{
                    self.viewVoucherCode.isHidden = true
                }
            }
        }
        self.tblBookedServices.reloadData()
        self.manageTableViewHeight()
        self.manageView()
        if objAppShareData.shouldReviewPopUpOpen{
          objAppShareData.shouldReviewPopUpOpen = false
          self.popUpRatting()
        }
    }
    
    func manageTableViewHeight(){
        if self.arrBookedMainService.count==0{
            return
        }
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*285)
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*350)
                }
            }
        }else{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*285)
        }
        //self.constraintTableHeight.constant = CGFloat(1*195)
        self.view.layoutIfNeeded()
    }
}

//MARK: - UITableview delegate
extension BookingDetailVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookedService.count
        //return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        let objBookedService = self.arrBookedService[indexPath.row]
        let objMain = arrBookedMainService[0]
        
        cell.btnOnTheWay.isHidden = true
        cell.btnStartService.isHidden = true
        cell.btnEndServices.isHidden = true
        
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                cell.imgProfie.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }else{
            cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
        }
        cell.btnProfile.tag = indexPath.row
        cell.lblName.text = objBookedService.artistServiceName
        cell.lblArtistName.text = objBookedService.staffName
        let doubleStr = String(format: "%.2f", Double(objBookedService.bookingPrice)!)
        //cell.lblPrice.text = objBookedService.bookingPrice
        cell.lblPrice.text = doubleStr
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        cell.lblDuration.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            cell.viewStatus.isHidden = true
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    cell.viewStatus.isHidden = false
                }
            }
        }else{
            cell.viewStatus.isHidden = true
        }
        
        let strStartTime =  objBookedService.startTime
        let dates = getTimeFromTime(strDate: objBookedService.bookingDate+" "+strStartTime)
        let formatters  = DateFormatter()
        formatters.dateFormat = "yyyy-MM-dd hh:mm a"
        formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let dataDate = formatters.date(from: dates)!
        
        cell.lblStatusService.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
        cell.lblStatusService.text = "Pending"
        if objMain.bookStatus == "1" || objMain.bookStatus == "3" || objMain.bookStatus == "5"{
            if objBookedService.status == "0"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Confirmed"
            }else if objBookedService.status == "1"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "On the way"
            }else if objBookedService.status == "2"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
                cell.lblStatusService.text = "On going"
            }else if objBookedService.status == "3" && objMain.bookStatus == "5"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            
            ////
            }else if objBookedService.status == "4"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Reached at your location"
            ////
           
            }else{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            }
        }
        
        /*
        if (objMain.bookStatus == "1" || objMain.bookStatus == "5" ) && (objMain.paymentType == "1" && objMain.paymentStatus == "1") || (objMain.paymentType == "2" && (objMain.bookStatus == "1" || objMain.bookStatus == "5")){
            
            if objBookedService.status == "0" && objBookedService.staffId == self.myId && objMain.bookingType == "2"{
                cell.btnOnTheWay.isHidden = false
            }
            if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true{
                cell.btnOnTheWay.isHidden = true
                cell.btnStartService.isHidden = false
            }else if objBookedService.status == "2"{
                cell.btnEndServices.isHidden = false
            }
        }
        */
        //cell.btnOnTheWay.isHidden = true
        cell.btnStartService.isHidden = true
        cell.btnEndServices.isHidden = true
        if objMain.bookingType == "2"{
//            if objBookedService.status == "0" ||
//                 objBookedService.status == "2" || objBookedService.status == "3"{
            if objBookedService.status == "0" ||
                objBookedService.status == "3"{
                cell.btnOnTheWay.isHidden = true
            }else if objBookedService.status == "1"{
                cell.btnOnTheWay.isHidden = false
            }else{
                cell.btnOnTheWay.isHidden = false
            }
        }
        
        cell.btnOnTheWay.tag = indexPath.row
        cell.btnOnTheWay.addTarget(self, action:#selector(btnOnTheWayTapped(sender:)) , for: .touchUpInside)
        cell.btnStartService.tag = indexPath.row
        cell.btnStartService.addTarget(self, action:#selector(btnStartServiecTapped(sender:)) , for: .touchUpInside)
        cell.btnEndServices.tag = indexPath.row
        cell.btnEndServices.addTarget(self, action:#selector(btnEndServiceTapped(sender:)) , for: .touchUpInside)
        
        cell.btnReport.tag = indexPath.row
        cell.btnReport.addTarget(self, action:#selector(btnReportTapped(sender:)) , for: .touchUpInside)
        let isReport = matchesTwoDate(dateA: dataDate, type: "pending")
        if isReport{
           cell.btnReport.isHidden = false
        }else{
           cell.btnReport.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
   
    @objc  func btnOnTheWayTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":userId,
                           "bookingId":objBooking._id,
                           "id":objService._id,
                           "serviceName":objService.artistServiceName,
                           "paymentType":objBooking.paymentType,
                           "price":objService.bookingPrice,
                           "type":"on the way"]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
    
    
    @objc  func btnStartServiecTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":userId,//
                "bookingId":objBooking._id,//
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "type":"start" ]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
    
    
    @objc  func btnEndServiceTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":userId,
                           "bookingId":objBooking._id,
                           "id":objService._id,
                           "serviceName":objService.artistServiceName,
                           "paymentType":objBooking.paymentType,
                           "price":objService.bookingPrice,
                           "type":"end" ]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
    
    @objc func btnReportTapped(sender: UIButton!){
        let objBooking = self.arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objService.arrReportList.count > 0{
            let sb = UIStoryboard(name:"BookingDetailModule",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ReportBookingVC") as! ReportBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.objBookingInfo = objService
            objChooseType.objBooking = objBooking
            if objService.arrReportList.last?.status == "1"{
                objChooseType.reportType = 1
            }else{
                objChooseType.reportType = 2
            }
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }else{
            let sb = UIStoryboard(name:"BookingDetailModule",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ReportBookingVC") as! ReportBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.objBookingInfo = objService
            objChooseType.objBooking = objBooking
            objChooseType.reportType = 0
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
}

//MARK: - BookingPendingCellDelegate btn Action
extension BookingDetailVC {
    @IBAction func btnPayAction(_ sender: UIButton) {
        let sb = UIStoryboard(name:"PaymentModule",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"MakePaymentVC") as! MakePaymentVC
        objChooseType.strBookingId = self.arrBookedMainService[0]._id
        objChooseType.strbidAmount = self.lblTotalPrice.text!
 self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        
        let objBooking = self.arrBookedMainService[0]
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
        if objBooking.arrUserDetail.count > 0 {
            
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [ "artistId":self.artistId,
                          "userId":userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "type":"accept"
            ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
        }
    }
    
    func rejectBookingAction(){
        let obj1 = arrBookedMainService[0]
        let obj2 = obj1.arrUserDetail
        let objUserInfo = obj2[0]
        let userId = objUserInfo._id
        
        let BookingId = obj1._id
        let serviceId = obj1.allServiceId
        let subServiceId = obj1.allSubServiceId
        let artistServiceId = obj1.allArtictServiceId
        
        let param = ["artistId":self.artistId,
                     "userId":userId,
                     "bookingId":BookingId,
                     "serviceId":serviceId,
                     "subserviceId":subServiceId,
                     "artistServiceId":artistServiceId,
                     "type":"reject"]
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
    }
    
    @IBAction func btnRejectAction(_ sender: UIButton) {
        self.rejectBookingAction()
    }
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                    self.viewRatingPopup.isHidden = true
                    self.callWebserviceForGetBookedServices()
                }else{
                    self.tblBookedServices.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: compair date and time
extension BookingDetailVC{
    func matchesTwoDate(dateA:Date, type:String) -> Bool{
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        
        let dateB:Date = TodayDate
        var addIndex = false
        
        switch dateA.compare(dateB) {
            
        case .orderedAscending:
            addIndex = true
            print(type+"Date A is later than date B")
        case .orderedDescending:
            addIndex = false
            print(type+"Date A is earlier than date B")
        case .orderedSame:
            addIndex = true
            print(type+"The two dates are the same")
        }
        return addIndex
    }
    
    func getTimeFromTime(strDate:String)-> String{
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let todayDate = formatter.date(from: strDate)
        let formatedDate: String = formatter.string(from: todayDate!)
        return formatedDate
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var numberOfChars:Int = 0
        let newText = (txtAddCommentRatingPopup.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        if numberOfChars >= 150 && text != ""{
            return false
        }else{
        }
        return numberOfChars < 150
    }
}


extension BookingDetailVC{
    @IBAction func btnStarReviewAction(_ sender: UIButton) {
        self.popUpRatting()
    }
    func popUpRatting(){
        self.viewRatingPopup.isHidden = false
        var userName = ""
        var strMyId = ""
        var userInfoMy : [String : Any] = [:]
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strMyId = "\(userId)"
            }
            userName = dict["userName"] as? String ?? ""
            userInfoMy = dict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strMyId = String(id)
            }
            userName = userInfo["userName"] as? String ?? ""
            userInfoMy = userInfo
        }
        
        self.viewRatingAssignRatingPopup.value = 0.0
        self.txtAddCommentRatingPopup.text = ""
        
        let objBooking = self.arrBookedMainService[0]
        if objBooking.ratingByUser > 0.0{
            //self.viewRatingAssignRatingPopup.isUserInteractionEnabled = false
            //self.txtAddCommentRatingPopup.isUserInteractionEnabled = false
            //self.btnSubmitReview.isHidden = true
            self.strReviewType = "edit"
            self.viewRatingAssignRatingPopup.value = CGFloat(objBooking.ratingByUser)
            self.txtAddCommentRatingPopup.text = objBooking.reviewByUser
        }else{
            self.strReviewType = "insert"
            self.viewRatingAssignRatingPopup.isUserInteractionEnabled = true
            self.txtAddCommentRatingPopup.isUserInteractionEnabled = true
            self.btnSubmitReview.isHidden = false
        }
        
        if self.arrBookedMainService.count > 0{
            let objMainBooking = self.arrBookedMainService[0]
            if objMainBooking.arrUserDetail.count > 0{
//                if objMainBooking.customerType == "walking"{}else{
//                    self.viewRatingPopup.isHidden = false
//                }
                let objUser = objMainBooking.arrUserDetail[0]
                self.lblNameRatingPopup.text = userInfoMy["userName"] as? String
                let url = URL(string:  userInfoMy["profileImage"] as? String ?? "" )
                if  url != nil {
                    //self.imgCustomerRatingPopup.af_setImage(withURL: url!)
                    self.imgCustomerRatingPopup.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }else{
                    self.imgCustomerRatingPopup.image = #imageLiteral(resourceName: "cellBackground")
                }
                
                let a = "Your service has been completed. Please review how satisfied you are with "
                //let a = "Please review how satisfied you are with "
                let b = "@"+objUser.userName+"'s"
                let c = " service."
                
                let StrName = NSMutableAttributedString(string: a + "", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
                StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
                
                let StrName1 = NSMutableAttributedString(string: b + "", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
                
                StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
                
                
                let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
                
                StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
                
                
                let combination = NSMutableAttributedString()
                
                combination.append(StrName)
                combination.append(StrName1)
                combination.append(StrName2)
                self.lblDetailRatingPopup.attributedText = combination
        }}}
}

extension BookingDetailVC{
    
    @IBAction func btnProfileOwnerAction(_ sender: UIButton){
        self.goToProfileByUserName(userName: self.lblArtistName.text!)
    }
    @IBAction func btnProfileStaffAction(_ sender: UIButton){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objUser = self.arrBookedService[indexPath.row]
        self.goToProfileByUserName(userName: objUser.staffName)
    }
    func goToProfileByUserName(userName:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
//            if self.myId == tagId {
//                //isNavigate = true
//                objAppShareData.isOtherSelectedForProfile = false
//                self.gotoProfileVC()
//                return
//            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}
