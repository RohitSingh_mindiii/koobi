//
//  BookingDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 04/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class PastFutureBookingVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewPastBooking: UIView!
    @IBOutlet weak var viewFutureBooking: UIView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var tblPastBooking: UITableView!
    @IBOutlet weak var tblFutureBooking: UITableView!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearchFilter: UITextField!
    @IBOutlet weak var imgSearch: UITextField!
    var strSearchText = ""
    var objService = SubSubService.init(dict: [:])
    @IBOutlet weak var btnBookingHistory: UIButton!
    @IBOutlet weak var btnAppointments: UIButton!
    @IBOutlet weak var btnCancelled: UIButton!
    @IBOutlet weak var imgCancelled: UIImageView!
    @IBOutlet weak var imgBookingHistory: UIImageView!
    @IBOutlet weak var imgAppointments: UIImageView!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblNoRecord: UIView!
    var isBookingHistory = false
    var isLoading = false
    var isCancelled = false
    var selectedIndexPath : IndexPath?
    var arrPastBookingData : [PastFutureBookingData] = []
    var arrPastBookingDataAll : [PastFutureBookingData] = []
    var arrFutureBookingData : [PastFutureBookingData] = []
    var arrFutureBookingDataAll : [PastFutureBookingData] = []
    
    @IBOutlet weak var btnMainType: UIButton!
    @IBOutlet weak var btnAllType: UIButton!
    @IBOutlet weak var btnInCallType: UIButton!
    @IBOutlet weak var btnOutCallType: UIButton!
    @IBOutlet weak var viewAllType: UIView!
    @IBOutlet weak var viewSearchBorder: UIView!
    @IBOutlet weak var viewInCallType: UIView!
    @IBOutlet weak var viewOutCallType: UIView!
    let dropDown = DropDown()
    fileprivate var strUserId : String = ""
    fileprivate var pageNo: Int = 0
    fileprivate var pageNo3: Int = 0
    fileprivate var totalCount = 0
    fileprivate var lastLoadedPage = 0
    fileprivate var strBookingType = ""
    fileprivate var pageNo2: Int = 0
    fileprivate var totalCount2 = 0
    fileprivate var lastLoadedPage2 = 0
    
    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    fileprivate let reviewTextLimit = 150
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.skyBlueNewTheam
        return refreshControl
    }()
    
    lazy var refreshControl2: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.skyBlueNewTheam
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        self.viewSearch.isHidden = true
        self.txtSearchFilter.delegate = self
        self.txtSearchFilter.text = ""
        self.strSearchText = ""
        self.getBookingDataWith(0)
        self.addGesturesToView()
        self.tblPastBooking.allowsSelection = true
        self.tblFutureBooking.allowsSelection = true
        self.viewSearchBorder.layer.borderWidth = 1
        self.viewSearchBorder.layer.borderColor = UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0).cgColor
        self.viewSearchBorder.clipsToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        selectedIndexPath = nil
        self.setuoDropDownAppearance()
        
        if objAppShareData.isFromNotification {
            
            var notifyId : Int?
            var strUserType: String?
            var strUserName: String?
            var strProfileImage: String?
            
            if let userInfo = objAppShareData.notificationInfoDict {
                
                if let notiId  = userInfo["notifyId"] as? Int{
                    notifyId = notiId
                }else{
                    if let notiId  = userInfo["notifyId"] as? String{
                        notifyId = Int(notiId)
                    }
                }
                
                if let userType = userInfo["userType"] as? String{
                    strUserType = userType
                }
                
                if let userName = userInfo["userName"] as? String{
                    strUserName = userName
                }
                
                if let img = userInfo["urlImageString"] as? String{
                    strProfileImage  = img
                }
                
            }
            
            if let notifyId = notifyId {
                
                var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
                
                objPastFutureBookingData._id = notifyId
                objPastFutureBookingData.userName = strUserName ?? ""
                objPastFutureBookingData.profileImage = strProfileImage ?? ""
                
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
                return
                
            }
            
        }
        self.getBookingDataWith(0)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        selectedIndexPath = nil
        self.getBookingDataWith(0)
        //API Call
    }
    func addGesturesToView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginTap.delegate = self
        socialLoginTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(socialLoginTap)
    }
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        //        self.view.endEditing(true)
        //        if self.strSearchText != "" || self.txtSearchFilter.text != ""{
        //            return
        //        }
        //        self.viewSearch.isHidden = true
        //        if self.strSearchText != "" || self.txtSearchFilter.text != ""{
        //            self.txtSearchFilter.text = ""
        //            self.strSearchText = ""
        //            getBookingDataWith(0)
        //        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.tblPastBooking) == true {
            return false
        }
        if touch.view?.isDescendant(of: self.tblFutureBooking) == true {
            return false
        }
        return true
    }
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func configureView(){
        
        self.strBookingType = "All"
        self.viewAllType.isHidden = true
        self.viewInCallType.isHidden = true
        self.viewOutCallType.isHidden = true
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                self.strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        
        //        outerView.layer.cornerRadius = 10
        //        outerView.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
        //        outerView.layer.borderWidth = 1
        //        outerView.clipsToBounds = true
        
        self.tblPastBooking.delegate = self
        self.tblPastBooking.dataSource = self
        self.tblFutureBooking.delegate = self
        self.tblFutureBooking.dataSource = self
        
        self.tblPastBooking.rowHeight = UITableView.automaticDimension;
        self.tblPastBooking.estimatedRowHeight = 77.0;
        
        self.tblPastBooking.addSubview(self.refreshControl)
        self.tblFutureBooking.addSubview(self.refreshControl2)
        
        self.lblNoRecord.isHidden = true
        
        //segmentController.selectedSegmentIndex = 0
        self.lblHeader.text = "My Bookings"
        
        //        self.viewPastBooking.isHidden = false
        //        self.viewFutureBooking.isHidden = true
        
        self.viewPastBooking.isHidden = true
        self.viewFutureBooking.isHidden = false
    }
}

//MARK: - btn Actions
extension PastFutureBookingVC {
    
    @IBAction func btnServiceInListAction(_ sender: UIButton) {
        return
            print(sender.superview!.tag)
        print(sender.tag)
        var objBooking = PastFutureBookingData.init(dict: [:])
        if isBookingHistory{
            objBooking = self.arrPastBookingData[sender.superview!.tag]
        }else{
            objBooking = self.arrFutureBookingData[sender.superview!.tag]
        }
        
        let objService = objBooking.arrServices[sender.tag]
        objAppShareData.selectedOtherIdForProfile = objBooking.artistId
        var strIncallOrOutCall = objAppShareData.dictFilter["serviceType"] as? String ?? ""
        if objBooking.bookingType == 1{
            strIncallOrOutCall = "In Call"
        }else{
            strIncallOrOutCall = "Out Call"
        }
        
        self.objService.artistId = objBooking.artistId
        self.objService.serviceId = objService.serviceId
        self.objService.subServiceId = objService.subServiceIdTemp
        self.objService.subSubServiceId = objService.subSubServiceIdTemp
        objAppShareData.isBookingFromService = true
        objAppShareData.objServiceForEditBooking = self.objService
        
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        var objUser = PastFutureBookingData.init(dict: [:])
        if self.isBookingHistory{
            objUser = self.arrPastBookingData[sender.tag]
        }else{
            objUser = self.arrFutureBookingData[sender.tag]
        }
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            var myId = 0
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myId = userInfo["_id"] as? Int ?? 0
            }
            if myId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnSearchOptionFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        //        if !self.viewSearch.isHidden {
        //           self.viewSearch.isHidden = false
        //        }else{
        //           self.viewSearch.isHidden = true
        //        }
        self.viewSearch.isHidden = !self.viewSearch.isHidden
        if self.strSearchText != "" || self.txtSearchFilter.text != ""{
            self.txtSearchFilter.text = ""
            self.strSearchText = ""
            getBookingDataWith(0)
        }
    }
    @IBAction func btnDropDownFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        let arrBookingType = ["All","In Call","Out Call"]
        
        let objGetData = arrBookingType.map { $0 }
        guard objGetData.isEmpty == false else {
            return
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.direction = .bottom
        dropDown.dataSource = objGetData
        dropDown.width = self.view.frame.size.width/3
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if  item == "All Type"{
                self.chengDropDownAction(tag: 1)
            }else if item == "In Call"{
                self.chengDropDownAction(tag: 2)
            }else if item == "Out Call"{
                self.chengDropDownAction(tag: 3)
            }else{
                self.chengDropDownAction(tag: 1)
            }
            
            let index_2 = index
        }
        dropDown.show()
    }
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)
    }
    
    @IBAction func btnBookingTypeAction(_ sender: UIButton){
        self.chengDropDownAction(tag: sender.tag)
    }
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Booking Type"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        objAppShareData.arrTBottomSheetModal = ["All","In Call","Out Call"]
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if str == "0"{
                strongSelf.strBookingType = "All"
                strongSelf.arrFutureBookingData = strongSelf.arrFutureBookingDataAll
                strongSelf.arrPastBookingData = strongSelf.arrPastBookingDataAll
            }else if str == "1"{
                strongSelf.strBookingType = "In Call"
                let filteredArray = strongSelf.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                strongSelf.arrFutureBookingData = filteredArray
                let filteredArrayPast = strongSelf.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                strongSelf.arrPastBookingData = filteredArrayPast
            }else if str == "2"{
                strongSelf.strBookingType = "Out Call"
                let filteredArray = strongSelf.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                strongSelf.arrFutureBookingData = filteredArray
                let filteredArrayPast = strongSelf.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                strongSelf.arrPastBookingData = filteredArrayPast
            }
            if strongSelf.isBookingHistory{
                strongSelf.tblPastBooking.reloadData()
                if strongSelf.arrPastBookingData.count == 0 {
                    strongSelf.lblNoRecord.isHidden = false
                    strongSelf.tblPastBooking.isHidden = true
                }else{
                    strongSelf.lblNoRecord.isHidden = true
                    strongSelf.tblPastBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    strongSelf.tblPastBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }else{
                strongSelf.tblFutureBooking.reloadData()
                if strongSelf.arrFutureBookingData.count == 0 {
                    strongSelf.lblNoRecord.isHidden = false
                    strongSelf.tblFutureBooking.isHidden = true
                }else{
                    strongSelf.lblNoRecord.isHidden = true
                    strongSelf.tblFutureBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    strongSelf.tblFutureBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        strongSelf.btnMainType.setTitle(strongSelf.strBookingType, for: .normal)
            print(str)
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Booking Type"
        objAppShareData.arrTBottomSheetModal = ["All","In Call","Out Call"]
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if str == "0"{
                strongSelf.strBookingType = "All"
                strongSelf.arrFutureBookingData = strongSelf.arrFutureBookingDataAll
                strongSelf.arrPastBookingData = strongSelf.arrPastBookingDataAll
            }else if str == "1"{
                strongSelf.strBookingType = "In Call"
                let filteredArray = strongSelf.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                strongSelf.arrFutureBookingData = filteredArray
                let filteredArrayPast = strongSelf.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                strongSelf.arrPastBookingData = filteredArrayPast
            }else if str == "2"{
                strongSelf.strBookingType = "Out Call"
                let filteredArray = strongSelf.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                strongSelf.arrFutureBookingData = filteredArray
                let filteredArrayPast = strongSelf.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                strongSelf.arrPastBookingData = filteredArrayPast
            }
            if strongSelf.isBookingHistory{
                strongSelf.tblPastBooking.reloadData()
                if strongSelf.arrPastBookingData.count == 0 {
                    strongSelf.lblNoRecord.isHidden = false
                    strongSelf.tblPastBooking.isHidden = true
                }else{
                    strongSelf.lblNoRecord.isHidden = true
                    strongSelf.tblPastBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    strongSelf.tblPastBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }else{
                strongSelf.tblFutureBooking.reloadData()
                if strongSelf.arrFutureBookingData.count == 0 {
                    strongSelf.lblNoRecord.isHidden = false
                    strongSelf.tblFutureBooking.isHidden = true
                }else{
                    strongSelf.lblNoRecord.isHidden = true
                    strongSelf.tblFutureBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    strongSelf.tblFutureBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        strongSelf.btnMainType.setTitle(strongSelf.strBookingType, for: .normal)
            print(str)
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    
    /*
     func setuoDropDownAppearance()  {
     let appearance = DropDown.appearance()
     appearance.cellHeight = 40
     appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
     appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
     appearance.separatorColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
     appearance.cornerRadius = 3
     appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
     appearance.shadowOpacity = 0.9
     appearance.shadowRadius = 1
     appearance.animationduration = 0.25
     appearance.textColor = .darkGray
     }
     */
    
    func chengDropDownAction(tag:Int){
        
        if tag == 0{
            if self.strBookingType == "All"{
                self.viewAllType.isHidden = true
                self.viewInCallType.isHidden = false
                self.viewOutCallType.isHidden = false
            }else if self.strBookingType == "In Call"{
                self.viewAllType.isHidden = false
                self.viewInCallType.isHidden = true
                self.viewOutCallType.isHidden = false
            }else if self.strBookingType == "Out Call"{
                self.viewAllType.isHidden = false
                self.viewInCallType.isHidden = false
                self.viewOutCallType.isHidden = true
            }
        }else {
            if tag == 1{
                self.strBookingType = "All"
                self.arrFutureBookingData = self.arrFutureBookingDataAll
                self.arrPastBookingData = self.arrPastBookingDataAll
            }else if tag == 2{
                self.strBookingType = "In Call"
                let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                self.arrFutureBookingData = filteredArray
                let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                self.arrPastBookingData = filteredArrayPast
            }else if tag == 3{
                self.strBookingType = "Out Call"
                let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                self.arrFutureBookingData = filteredArray
                let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                self.arrPastBookingData = filteredArrayPast
            }
            if self.isBookingHistory{
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                    self.tblPastBooking.isHidden = true
                }else{
                    self.lblNoRecord.isHidden = true
                    self.tblPastBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tblPastBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                    self.tblFutureBooking.isHidden = true
                }else{
                    self.lblNoRecord.isHidden = true
                    self.tblFutureBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tblFutureBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
            self.viewAllType.isHidden = true
            self.viewInCallType.isHidden = true
            self.viewOutCallType.isHidden = true
        }
        self.btnMainType.setTitle(self.strBookingType, for: .normal)
    }
    
    
    @IBAction func btnBackAction(_ sender: Any){
        objAppShareData.clearNotificationData()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentButtonPastFuture(_ sender: UIButton) {
        if self.isLoading{
            return
        }
        
        ////
        if self.strSearchText != "" || self.txtSearchFilter.text != ""{
            self.txtSearchFilter.text = ""
            self.strSearchText = ""
        }
        self.chengDropDownAction(tag: 1)
        ////
        
        self.viewAllType.isHidden = true
        self.viewInCallType.isHidden = true
        self.viewOutCallType.isHidden = true
        
        if sender.tag == 0 {
            if isBookingHistory{
                return
            }
            
            showBookingHistoryTabSelected()
            isBookingHistory = true
            isCancelled = false
            viewPastBooking.isHidden = false
            viewFutureBooking.isHidden = true
            self.lblNoRecord.isHidden = true
            if arrPastBookingData.count == 0{
                self.getBookingDataWith(0)
            }
            
        }else if sender.tag == 1{
            
            if !isBookingHistory && !isCancelled{
                return
            }
            self.lastLoadedPage2 = 0
            self.pageNo2 = 0
            isCancelled = false
            arrFutureBookingData.removeAll()
            showApointmentTabSelected()
            isBookingHistory = false
            viewPastBooking.isHidden = true
            viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            if arrFutureBookingData.count == 0{
                self.getBookingDataWith(0)
            }
        
        }else if sender.tag == 2{
            
            if isCancelled{
                return
            }
            self.lastLoadedPage2 = 0
            self.pageNo2 = 0
            arrFutureBookingData.removeAll()
            showCancelledTabSelected()
            isBookingHistory = false
            isCancelled = true
            viewPastBooking.isHidden = true
            viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            if arrFutureBookingData.count == 0{
                self.getBookingDataWith(0)
            }
        }
    }
    
    func showBookingHistoryTabSelected() {
        self.imgBookingHistory.backgroundColor = UIColor.lightGray
        self.imgAppointments.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnAppointments.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.imgCancelled.backgroundColor = UIColor.lightGray
        self.btnCancelled.setTitleColor(UIColor.lightGray, for: .normal)
    }
    
    func showApointmentTabSelected() {
        self.imgAppointments.backgroundColor = UIColor.lightGray
        self.imgBookingHistory.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnBookingHistory.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
        self.imgCancelled.backgroundColor = UIColor.lightGray
        self.btnCancelled.setTitleColor(UIColor.lightGray, for: .normal)
    }
    func showCancelledTabSelected() {
        self.imgCancelled.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnCancelled.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.imgAppointments.backgroundColor = UIColor.lightGray
        self.imgBookingHistory.backgroundColor = UIColor.lightGray
        self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
    }
    @IBAction func segmentPastFuture(_ sender: UISegmentedControl) {
        
        switch segmentController.selectedSegmentIndex {
        case 0:
            viewPastBooking.isHidden = false
            viewFutureBooking.isHidden = true
            self.lblNoRecord.isHidden = true
            self.lblHeader.text = "Past Booking"
            if arrPastBookingData.count == 0{
                self.getBookingDataWith(0)
            }
            
        case 1:
            viewPastBooking.isHidden = true
            viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            self.lblHeader.text = "Future Booking"
            if arrFutureBookingData.count == 0{
                self.getBookingDataWith(0)
            }
            
        default:
            break;
        }
    }
}

// MARK: - table view Delegate and Datasource
extension PastFutureBookingVC : CellPastFutureRatingDelegate, CellFutureBookingDelegate, CellPastFutureReviewDelegate , CellSmallPastFutureReviewDelegate, CellSmallPastFutureRatingDelegate {
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblPastBooking{
            return arrPastBookingData.count
        }else{
            return arrFutureBookingData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.isLoading{
            return UITableViewCell()
        }
        
        if tableView == tblPastBooking {
            
            if self.arrPastBookingData.count < self.totalCount {
                
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                    self.getBookingDataWith(nextPage)
                }
            }
            
        }else{
            
            if self.arrFutureBookingData.count < self.totalCount2 {
                
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                    self.getBookingDataWith(nextPage)
                }
            }
        }
        
        if tableView == tblPastBooking {
            
            let obj  : PastFutureBookingData = arrPastBookingData[indexPath.row]
            
            /*
             if obj.isReviewAvailable {
             
             if indexPath != selectedIndexPath {
             
             let cellIdentifier = "cellSmallPastFutureRating"
             
             if let cell : cellSmallPastFutureRating = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? cellSmallPastFutureRating {
             
             cell.delegate = self
             cell.indexPath = indexPath
             obj.indexPath = indexPath
             cell.lblName.text = obj.userName
             
             cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
             cell.lblAmount.text = "£" + obj.totalPrice
             cell.vwRating.value = CGFloat(obj.rating)
             cell.vwRating.isUserInteractionEnabled = false
             
             let strImg = obj.profileImage
             if strImg != "" {
             if let url = URL(string: strImg){
             cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
             }
             }
             return cell
             
             }else{
             return UITableViewCell()
             }
             
             }else{
             
             let cellIdentifier = "cellPastFutureRating"
             
             if let cell : cellPastFutureRating = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? cellPastFutureRating {
             
             cell.delegate = self
             cell.indexPath = indexPath
             obj.indexPath = indexPath
             
             cell.lblName.text = obj.userName
             cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
             cell.lblAmount.text = "£" + obj.totalPrice
             
             cell.vwRating.value = CGFloat(obj.rating)
             cell.vwRating.isUserInteractionEnabled = false
             
             cell.lblCommet1.text = obj.reviewByUser == "" ? "NA" : obj.reviewByUser
             cell.lblCommet2.text = obj.reviewByArtist == "" ? "NA" : obj.reviewByArtist
             
             let strImg = obj.profileImage
             if strImg != "" {
             if let url = URL(string: strImg){
             cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
             }
             }
             return cell
             
             }else{
             return UITableViewCell()
             }
             }
             }else{
             */
            
            if indexPath != selectedIndexPath {
                
                let cellIdentifier = "CellSmallPastFutureReview"
                if let cell : CellSmallPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellSmallPastFutureReview {
                    
                    cell.delegate = self
                    cell.indexPath = indexPath
                    obj.indexPath = indexPath
                    cell.btnProfile.tag = indexPath.row
                    //cell.vwArtistRating.value = CGFloat(obj.artistRating)
                    cell.setDataInCollection(obj: obj)
                    cell.lblName.text = obj.userName
                    cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                    cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                    cell.lblAmount.text = "£" + obj.totalPrice
                    
                    let strImg = obj.profileImage
                    if strImg != "" {
                        if let url = URL(string: strImg){
                            //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                            cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                        }
                    }else{
                        cell.imgVwProfile.image =  UIImage(named: "cellBackground")
                    }
                    if obj.rating > 0{
                        cell.vwArtistRating.isHidden = false
                        cell.vwArtistRating.value = CGFloat(obj.rating)
                        cell.btnReview.backgroundColor = UIColor.clear
                        cell.btnReview.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.lblHyperLink.isHidden = false
                        cell.btnReview.setTitle("        Edit Review", for: .normal)
                        
                    }else{
                        cell.vwArtistRating.isHidden = true
                        cell.btnReview.setTitle("Give Review", for: .normal)
                        cell.btnReview.backgroundColor = UIColor.theameColors.skyBlueNewTheam
                        cell.btnReview.setTitleColor(UIColor.white, for: .normal)
                        cell.lblHyperLink.isHidden = true
                    }
                    return cell
                }else{
                    return UITableViewCell()
                }
                
            }else{
                
                let cellIdentifier = "CellPastFutureReview"
                if let cell : CellPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellPastFutureReview {
                    
                    cell.delegate = self
                    cell.indexPath = indexPath
                    obj.indexPath = indexPath
                    cell.btnProfile.tag = indexPath.row
                    cell.reviewLimit = reviewTextLimit
                    //cell.lblTextLimit.text = "\(cell.reviewLimit)"
                    cell.vwArtistRating.value = CGFloat(obj.artistRating)
                    cell.setDataInCollection(obj: obj)
                    //cell.lblTextLimit.isHidden = true
                    cell.lblName.text = obj.userName
                    cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                    cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                    cell.lblAmount.text = "£" + obj.totalPrice
                    
                    cell.txtViewComment.text = ""
                    cell.vwRating.value = 0
                    
                    let strImg = obj.profileImage
                    if strImg != "" {
                        if let url = URL(string: strImg){
                            //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                            cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                        }
                    }else{
                        cell.imgVwProfile.image =  UIImage(named: "cellBackground")
                    }
                    if obj.rating > 0{
                        cell.vwArtistRating.isHidden = false
                        cell.vwArtistRating.value = CGFloat(obj.rating)
                        cell.vwRating.value = CGFloat(obj.rating)
                        cell.txtViewComment.text = obj.reviewByUser
                        //cell.vwRating.isUserInteractionEnabled = false
                        //cell.txtViewComment.isUserInteractionEnabled = false
                        //cell.btnSubmit.isHidden = true
                        //cell.btnReview.setTitle("View Review", for: .normal)
                        cell.btnReview.setTitle("        Edit Review", for: .normal)
                        cell.btnReview.backgroundColor = UIColor.clear
                        cell.btnReview.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.lblHyperLink.isHidden = false
                        
                    }else{
                        cell.vwRating.value = 0
                        cell.vwArtistRating.isHidden = true
                        cell.txtViewComment.text = ""
                        cell.vwRating.isUserInteractionEnabled = true
                        cell.txtViewComment.isUserInteractionEnabled = true
                        cell.btnSubmit.isHidden = false
                        cell.btnReview.setTitle("Give Review", for: .normal)
                        cell.btnReview.backgroundColor = UIColor.theameColors.skyBlueNewTheam
                        cell.btnReview.setTitleColor(UIColor.white, for: .normal)
                        cell.lblHyperLink.isHidden = true
                    }
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
            //}
            
        }else{
            if self.isLoading{
                return UITableViewCell()
            }
            if arrFutureBookingData.count==0{
                return UITableViewCell()
            }
            let obj = arrFutureBookingData[indexPath.row]
            
            // bookStatus = 0: for pending , 1: for accept, 2: for reject or cancle, 3: for complete
            
            let cellIdentifier = "CellFutureBooking"
            if let cell : CellFutureBooking = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellFutureBooking {
                
                cell.delegate = self
                cell.indexPath = indexPath
                obj.indexPath = indexPath
                cell.btnProfile.tag = indexPath.row
                
                cell.lblName.text = obj.userName
                cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                cell.lblAmount.text = "£" + obj.totalPrice
                
                if self.strBookingType == "All"{
                    cell.lblBookingType.isHidden = false
                    if obj.bookingType == 1{
                        cell.lblBookingType.text = "In Call"
                    }else{
                        cell.lblBookingType.text = "Out Call"
                    }
                }else{
                    cell.lblBookingType.isHidden = true
                }
                cell.setDataInCollection(obj: obj)
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }else{
                    cell.imgVwProfile.image =  UIImage(named: "cellBackground")
                }
                
                if obj.arr_artistService.count > 0 {
                    cell.lblServiceCategory.text = obj.arr_artistService[0]
                }else{
                    cell.lblServiceCategory.text = "NA"
                }
                
                if obj.bookStatus == 0 {
                    cell.btnRequest.setTitle("Pending", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else if obj.bookStatus == 1 {
                    cell.btnRequest.setTitle("Confirmed", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
                    cell.btnRequest.setTitle("Payment Pending", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else if (obj.bookStatus == 5 || obj.paymentStatus == 4) {
                    cell.btnRequest.setTitle("In Progress", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else{
                    cell.btnRequest.isHidden = true
                }
                
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //return
        var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
        if tableView == tblPastBooking {
            if arrPastBookingData.count == 0{
                return
            }
            objPastFutureBookingData = arrPastBookingData[indexPath.row]
        }else{
            if arrFutureBookingData.count == 0{
                return
            }
            objPastFutureBookingData = arrFutureBookingData[indexPath.row]
        }
        self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
    }
    
}

// MARK: - table view Delegate
extension PastFutureBookingVC {
    
    func btnDropDownTappedInRatingCell(at index:IndexPath){
        self.closeBigCell(at: index)
    }
    
    func btnDropDownTappedInSmallRatingCell(at index: IndexPath) {
        self.openBigCell(at: index)
    }
    
    func btnDropDownTappedInReviewCell(at index:IndexPath){
        self.closeBigCell(at: index)
    }
    
    func btnDropDownTappedInSmallReviewCell(at index: IndexPath) {
        self.openBigCell(at: index)
    }
    
    func btnSubmitTapped_ReviewCell(at index:IndexPath){
        
        // CellPastFutureReview
        var strReviewType = ""
        let objBooking = self.arrPastBookingData[index.row]
        if objBooking.rating > 0{
            strReviewType = "edit"
        }else{
            strReviewType = "insert"
        }
        
        if let cell : CellPastFutureReview = self.tblPastBooking.cellForRow(at: index) as? CellPastFutureReview {
            
            cell.txtViewComment.text = cell.txtViewComment.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
//            if cell.txtViewComment.text == "" {
//                objAppShareData.showAlert(withMessage: "Please enter some text", type: alertType.bannerDark, on: self)
//            }else
            if cell.vwRating.value == 0 {
                objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
            }else{
                
                
                cell.txtViewComment.endEditing(true)
                let rating = "\(cell.vwRating.value)"
                
                // objAppShareData.showAlert(withMessage: "Submitted Successfully", type: alertType.bannerDark, on: self)
                
                let obj = arrPastBookingData[index.row]
                
                var parameters = [String:Any]()
                
                var strUserId : String = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                    
                    if let userId = dict["_id"] as? Int {
                        strUserId = "\(userId)"
                    }
                }
                let date = objAppShareData.dateFormatInFormYou(forAPI: Date())
                parameters = [
                    "artistId" : obj.artistId,
                    "userId" : strUserId,
                    "bookingId":obj._id,
                    "reviewByUser": (cell.txtViewComment.text)!,
                    "reviewByArtist":"",
                    "rating": rating,
                    "ratingDate":date,
                    "type":strReviewType
                ]
                print(parameters)
                self.callWebserviceFor_addCommentAndRating(parameters : parameters, obj: obj)
            }
        }
    }
    
    func btnRebookTapped_ReviewCell(at index:IndexPath){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        self.closeBigCell(at: index)
    }
    
    func btnSendTapped(at index:IndexPath){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    func btnRequestTapped(at index:IndexPath){
        
        let obj : PastFutureBookingData = arrFutureBookingData[index.row]
        
        if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
            self.gotoPaymentInfoVC(obj : obj)
        }else if obj.bookStatus == 0 {
            self.gotoAppoitmentBookingVC(obj: obj)
        }
    }
    
    func openBigCell(at index:IndexPath){
        
        // in fact, was this very row selected,
        // and the user is clicking to deselect it...
        // if you don't want "click a selected row to deselect"
        // then on't include this clause.
        if selectedIndexPath == index
        {
            
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(at: [index], with: .none)
            //tableView.deselectRowAtIndexPath(indexPath, animated:false)
            return
        }
        
        
        // in fact, was some other row selected??
        // user is changing to this row? if so, also deselect that row
        if selectedIndexPath != nil
        {
            let pleaseRedrawMe = selectedIndexPath!
            // (note that it will be drawn un-selected
            // since we're chaging the 'selectedIndexPath' global)
            selectedIndexPath = index
            self.tblPastBooking.reloadRows(
                at: [pleaseRedrawMe, index],
                with:UITableView.RowAnimation.none)
            return;
        }
        
        // no previous selection.
        // simply select that new one the user just touched.
        // note that you can not use Apple's willDeselectRowAtIndexPath
        // functions ... because they are freaky
        selectedIndexPath = index
        self.tblPastBooking.reloadRows(at: [index], with: .none)
    }
    
    func closeBigCell(at index:IndexPath){
        
        // in fact, was this very row selected,
        // and the user is clicking to deselect it...
        // if you don't want "click a selected row to deselect"
        // then on't include this clause.
        if selectedIndexPath == index
        {
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(at: [index], with: .none)
            //tableView.deselectRowAtIndexPath(indexPath, animated:false)
            return
        }
        
        // in fact, was some other row selected??
        // user is changing to this row? if so, also deselect that row
        if selectedIndexPath != nil
        {
            let pleaseRedrawMe = selectedIndexPath!
            // (note that it will be drawn un-selected
            // since we're chaging the 'selectedIndexPath' global)
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(
                at: [pleaseRedrawMe, index],
                with:UITableView.RowAnimation.none)
            return;
        }
        selectedIndexPath = nil
        self.tblPastBooking.reloadRows(at: [index], with: .none)
    }
    
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func getBookingDataWith(_ page: Int) {
        
        var parameters = [String:Any]()
        
        var strBookingType : String = ""
        //if self.segmentController.selectedSegmentIndex == 0 {
        if self.isBookingHistory {
            self.lastLoadedPage = page
            self.pageNo = page
            strBookingType = "Past"
        }else{
            if isCancelled{
                self.lastLoadedPage2 = page
                self.pageNo2 = page
                strBookingType = "Cancelled Booking"
            }else{
                self.lastLoadedPage2 = page
                self.pageNo2 = page
                strBookingType = ""
            }
            
        }
        parameters = [
            //"userId":strUserId,
            //"page":page,
            //"limit":self.pageSize,
            "search":self.strSearchText,
            "type": strBookingType
        ]
        
        self.webServiceCall_getBookingData(parameters: parameters)
    }
    
    
    func webServiceCall_getBookingData(parameters : [String:Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        isLoading = true
        if self.isBookingHistory {
            if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                self.activity.startAnimating()
            }
        }else{
            if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                self.activity.startAnimating()
            }
        }
        
        self.lblNoRecord.isHidden = true
        objWebserviceManager.requestPost(strURL:  WebURL.userBookingHistory, params: parameters as [String : AnyObject] , success: { response in
            print(response)
            self.isLoading = false
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            self.arrPastBookingDataAll.removeAll()
            self.arrFutureBookingDataAll.removeAll()
            
            if self.isBookingHistory {
                if self.pageNo == 0 {
                    self.arrPastBookingData.removeAll()
                    self.tblPastBooking.reloadData()
                }
            }else{
                if self.isCancelled{
                    if self.pageNo3 == 0 {
                      self.arrFutureBookingData.removeAll()
                        self.tblFutureBooking.reloadData()
                    }
                }else{
                    if self.pageNo2 == 0 {
                        self.arrFutureBookingData.removeAll()
                        self.tblFutureBooking.reloadData()
                    }
                }
            }
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                
                if strStatus == k_success{
                    
                    if self.isBookingHistory{
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount = totalCount
                        }
                        //if let arr = response["Booking"] as? [[String : Any]]{
                        if let arr = response["data"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = PastFutureBookingData.init(dict: dict)
                                self.arrPastBookingDataAll.append(obj)
                            }
                        }
                        
                    }else{
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount2 = totalCount
                        }
                        //if let arr = response["Booking"] as? [[String : Any]]{
                        if let arr = response["data"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = PastFutureBookingData.init(dict: dict)
                                self.arrFutureBookingDataAll.append(obj)
                                
                            }
                            //self.arrFutureBookingDataAll.reverse()
                        }
                    }
                    
                    if self.strBookingType == "All"{
                        self.arrFutureBookingData = self.arrFutureBookingDataAll
                        self.arrPastBookingData = self.arrPastBookingDataAll
                    }else if self.strBookingType == "In Call"{
                        let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                        self.arrFutureBookingData = filteredArray
                        let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                        self.arrPastBookingData = filteredArrayPast
                    }else if self.strBookingType == "Out Call"{
                        let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                        self.arrFutureBookingData = filteredArray
                        let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                        self.arrPastBookingData = filteredArrayPast
                    }
                }else{
                    
                    if strStatus == "fail"{
                        
                    }else{
                        
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }
            
            if self.isBookingHistory{
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                    self.tblPastBooking.isHidden = true
                }else{
                    self.lblNoRecord.isHidden = true
                    self.tblPastBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tblPastBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                    self.tblFutureBooking.isHidden = true
                }else{
                    self.lblNoRecord.isHidden = true
                    self.tblFutureBooking.isHidden = false
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tblFutureBooking.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
            
        }) { (error) in
            self.isLoading = false
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            
            if self.isBookingHistory {
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceFor_addCommentAndRating(parameters : [String:Any] , obj : PastFutureBookingData ){
        /*
         bookingId
         reviewByUser
         reviewByArtist
         rating
         userId
         artistId*/
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        self.activity.startAnimating()
        
        objWebserviceManager.requestPost(strURL: WebURL.bookingReviewRating, params: parameters  , success: { response in
            
            self.activity.stopAnimating()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                
                if strStatus == k_success{
                    obj.reviewStatus = 1
                    obj.isReviewAvailable = true
                    obj.reviewByUser = parameters["reviewByUser"] as? String ?? ""
                    if let rating = parameters["rating"] as? String {
                        obj.rating = Double(rating) ?? 0
                    }
                    
                    self.closeBigCell(at: obj.indexPath!)
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
            
        }){ error in
            
            self.activity.stopAnimating()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        if strDate == ""{
            return ""
        }
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            
            let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
    
    func gotoPaymentInfoVC(obj : PastFutureBookingData) {
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentInfoVC") as? PaymentInfoVC {
            objVC.objPastFutureBookingData = obj
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
        //if let objVC = sb.instantiateViewController(withIdentifier:"Booki") as? AppoitmentBookingVC {
        //if let objVC = sb.instantiateViewController(withIdentifier:"BookingDetailNewVC") as? BookingDetailNewVC {
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingDetailVC") as? BookingDetailVC {
            objVC.strBookingId = String(obj._id)
            objVC.hidesBottomBarWhenPushed = true
            if objAppShareData.isFromNotification {
                navigationController?.pushViewController(objVC, animated: false)
            }else{
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
}

// MARK: - UITextfield Delegate
extension PastFutureBookingVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            self.activity.startAnimating()
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                self.activity.startAnimating()
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        getBookingDataWith(0)
    }
}
