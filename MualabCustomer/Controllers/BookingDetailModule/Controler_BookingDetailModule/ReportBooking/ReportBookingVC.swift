import UIKit
import DropDown

class ReportBookingVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var txtReportDate: UITextField!
    @IBOutlet weak var txtReportStatus: UITextField!
    
    @IBOutlet weak var viewReportDate: UIView!
    @IBOutlet weak var viewReportStatus: UIView!
    @IBOutlet weak var viewNoteStatus: UIView!
    @IBOutlet weak var viewBtnSubmit: UIView!
    @IBOutlet weak var lblHint: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgDropdownReason: UIImageView!
    
    @IBOutlet weak var lblNoteDetail: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    
    var reportType = 0
    
    let dropDown = DropDown()
    
    fileprivate var strMyId = ""
    var objBookingInfo = BookingInfo(dict: ["":""])
    var objBooking = Booking(dict: ["":""])
    var arrReportReasonList = [ModelReportReason]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strMyId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strMyId = String(id)
            }
        }
        self.txtSelectReason.delegate = self
        self.txtViewDescription.delegate = self
        self.txtViewDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtViewDescription.tintColorDidChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setuoDropDownAppearance()
        self.manageUI()
        if reportType != 1{
            self.callWebserviceForReasonList()
        }
    }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let objGetData = self.arrReportReasonList.map { $0.title }
        if self.arrReportReasonList.count == 0{
            objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        objAppShareData.arrTBottomSheetModal = objGetData
        
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Report Reason"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            strongSelf.txtSelectReason.text = strongSelf.arrReportReasonList[Int(str) ?? 0].title
        }
        self.present(scrollingNC, animated: false, completion: nil)
        
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Report Reason"
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            strongSelf.txtSelectReason.text = strongSelf.arrReportReasonList[Int(str) ?? 0].title
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    
    func manageUI(){
        if reportType == 0{
            self.lblHeader.text = "Report"
            self.lblHint.isHidden = false
            self.imgDropdownReason.isHidden = false
            self.viewReportDate.isHidden = true
            self.viewReportStatus.isHidden = true
            self.viewNoteStatus.isHidden = true
            self.viewBtnSubmit.isHidden = false
        self.txtViewDescription.isUserInteractionEnabled = true
            self.btnSubmit.setTitle("Submit", for: .normal)
            self.txtViewDescription.text = ""
            self.txtSelectReason.text = ""
        }else if reportType == 1{
            self.lblHeader.text = "Report Details"
            self.lblHint.isHidden = true
            self.imgDropdownReason.isHidden = true
            self.viewReportDate.isHidden = false
            self.viewReportStatus.isHidden = false
            self.viewNoteStatus.isHidden = true
            self.viewBtnSubmit.isHidden = true
            self.txtViewDescription.isUserInteractionEnabled = false
            self.dataParsing()
        }else if reportType == 2{
            self.lblHeader.text = "Report Details"
            self.lblHint.isHidden = true
            self.imgDropdownReason.isHidden = true
            self.viewReportDate.isHidden = false
            self.viewReportStatus.isHidden = false
            self.viewNoteStatus.isHidden = false
            self.viewBtnSubmit.isHidden = false
            self.txtViewDescription.isUserInteractionEnabled = false
            self.btnSubmit.setTitle("Resubmit", for: .normal)
            self.dataParsing()
        }
    }
    
    func dataParsing(){
        if objBookingInfo.arrReportList.count > 0{
            self.txtSelectReason.text = objBookingInfo.arrReportList.last?.title
            self.txtViewDescription.text = objBookingInfo.arrReportList.last?.descriptions
            self.txtReportDate.text = objBookingInfo.arrReportList.last?.crd
            if objBookingInfo.arrReportList.last?.status == "1"{
                self.txtReportStatus.text = "Pending"
                self.txtReportStatus.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            }else{
                self.txtReportStatus.text = "Resolved"
                self.txtReportStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
            self.lblNoteDetail.text = objBookingInfo.arrReportList.last?.adminReason
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtViewDescription {
            if (text.count) != 0
            { searchString = self.txtViewDescription.text! + (text).uppercased()
                newLength = (self.txtViewDescription.text?.count)! + text.count - range.length
            }
            else {
            }
            if newLength <= 150{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtSelectReason {
            if (string.count ) != 0
            { searchString = self.txtSelectReason.text! + (string).uppercased()
                newLength = (self.txtSelectReason.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtSelectReason.text as NSString?)?.substring(to: (self.txtSelectReason.text?.count)! - 1).uppercased()
            }
            if newLength <= 50{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSelectReason{
            txtViewDescription.becomeFirstResponder()
        }
        return true
    }
    
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 16) ?? .systemFont(ofSize: 16)
        
    }
    @IBAction func btnSubmitReportAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if reportType == 2{
            reportType = 0
            self.manageUI()
        }else{
            self.txtSelectReason.text = self.txtSelectReason.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.txtViewDescription.text = self.txtViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if self.txtSelectReason.text?.count == 0{
                objAppShareData.showAlert(withMessage: "Please select report reason", type: alertType.bannerDark,on: self)
            }else if self.txtViewDescription.text?.count == 0{
                objAppShareData.showAlert(withMessage: "Please enter report description", type: alertType.bannerDark,on: self)
            }else{
                self.reportAction()
            }
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)    }
    
    
    func reportAction(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let myString = formatter.string(from: Date())
        let staffId = objBookingInfo.staffId
        var reportForId = "0"
        if staffId == "0" || staffId.count == 0{
           reportForId = objBooking.artistId
        }else{
           reportForId = staffId
        }
        let dict = ["bookingInfoId":objBookingInfo._id,
                    "description":self.txtViewDescription.text ?? "",
                    "bookingId":objBooking._id,
                    "staffId":staffId,
                    "businessId":objBooking.artistId,
                    "serviceName":objBookingInfo.artistServiceName,
                    "reportByUser":self.strMyId,
                    "reportForUser":reportForId,
                    "serviceId":objBookingInfo.artistServiceId,
                    "reportDate":myString,
                    "title":self.txtSelectReason.text ?? ""] as [String : Any]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.bookingReport, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                // let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.navigationController?.popViewController(animated: true)
                    objAppShareData.showAlert(withMessage: "Report Submitted successfully", type: alertType.bannerDark, on: self)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}



//MARK: - button extension
extension ReportBookingVC{
    @IBAction func btnDropReasonAction(_ sender: UIButton) {
        if reportType == 0{
            let objGetData = arrReportReasonList.map { $0.title }
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
            dropDown.anchorView = sender // UIView or UIBarButtonItem
            dropDown.direction = .bottom
            dropDown.dataSource = objGetData
            dropDown.width = self.view.frame.size.width - 50
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.txtSelectReason.text = item
                let index_2 = index
            }
            dropDown.show()
        }
    }
}
//MARK: - button extension
extension ReportBookingVC{
    func callWebserviceForReasonList(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        self.arrReportReasonList.removeAll()
        let params =  ["type":"booking"]
        objServiceManager.requestGet(strURL:WebURL.reportReason, params: params as [String : AnyObject], success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            objWebserviceManager.StopIndicator()
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]]{
                    for obj in arr{
                        let obj = ModelReportReason.init(dict:obj)
                        self.arrReportReasonList.append(obj)
                    }
                }
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            objWebserviceManager.StopIndicator()
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }}
