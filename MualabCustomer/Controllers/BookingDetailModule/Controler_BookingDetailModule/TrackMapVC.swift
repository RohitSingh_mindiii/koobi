//
//  TrackMapVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 1/24/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces
import INTULocationManager
import AlamofireImage
import CoreLocation

class TrackMapVC: UIViewController,GMSMapViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate {
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var collectionService: UICollectionView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgUserOnSlider: UIImageView!
    @IBOutlet weak var lblStaffAddress: UILabel!
    @IBOutlet weak var lblCustomerAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgDropDownDetail: UIImageView!
    @IBOutlet weak var imgCompleted: UIImageView!
    @IBOutlet weak var viewMaximumValueImage: UIView!
    @IBOutlet weak var bottumDetailView: UIView!
    @IBOutlet weak var radiusSlider:customSlider!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnEnd: UIButton!
    
    var timer: Timer?
    var strMyCurrentLatitude = ""
    var strMyCurrentLongitude = ""
    var strMyCurrentAddress = ""
    
    var strMyTrackingLatitude = ""
    var strMyTrackingLongitude = ""
    var strMyTrackingAddress = ""
    
    var strArtistTrackingLatitude = ""
    var strArtistTrackingLongitude = ""
    var strArtistTrackingAddress = ""
    
    var myId = ""
    var artistId = ""
    var objBookedMainService = Booking(dict: [:])
    var objBookedService = BookingInfo(dict: [:])
    var locationManager = CLLocationManager()
    fileprivate var arrMarker:[GMSMarker] = []
    var strLatitude = ""
    var strLongitude = ""
    
    
    var staffStartPointLatitude = ""
    var staffStartPointLongitude = ""
    
    var maximumValue = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewMaximumValueImage.isHidden = false
        
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [String:Any] ?? ["":""]
        
        self.strMyTrackingLatitude =  objBookedMainService.latitude
        self.strMyTrackingLongitude =  objBookedMainService.longitude
        
        if let imgUrl = userInfo["profileImage"] as? String {
            if imgUrl != "" {
                if let url = URL(string: imgUrl){
                    //self.imgUserOnSlider.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    self.imgUserOnSlider.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }else{
                self.imgUserOnSlider.image = #imageLiteral(resourceName: "cellBackground")
            }
        }
        
        if timer == nil {
            //timer = Timer.scheduledTimer(timeInterval: 25, target: self, selector: #selector(self.callWebserviceForGetBookedServices), userInfo: nil, repeats: true)
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.callWebserviceForGetBookedServices), userInfo: nil, repeats: true)
        }
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        objAppShareData.onTrackView = true
        
        self.strMyTrackingAddress = objBookedMainService.location
        
        self.strArtistTrackingLatitude = objBookedService.trackingLatitude
        self.strArtistTrackingLongitude = objBookedService.trackingLongitude
        
        self.staffStartPointLatitude = objBookedService.trackingLatitude
        self.staffStartPointLongitude = objBookedService.trackingLongitude
        
        
        self.lblStaffAddress.text = objBookedService.trackingAddress
        self.configureView()
        self.collectionService.delegate = self
        self.collectionService.dataSource = self
        self.collectionService.reloadData()
        viewMap.mapType = GMSMapViewType.normal
        self.viewMap.isMyLocationEnabled = true
        self.radiusSlider.setThumbImage(UIImage.init(named: "grayGubbara_ico"),for:.highlighted)
        self.radiusSlider.minimumTrackTintColor = appColor
        self.radiusSlider.value = 0.0
        let thumbImage = addUserImage(inImage: UIImage.init(named: "grayGubbara_ico")!, atPoint: CGPoint.init(x:9,y:6))
        let img = addUserImageSecond(inImage: thumbImage, atPoint:  CGPoint.init(x:12,y:8))
        self.radiusSlider.setThumbImage(img, for:.normal)
        self.radiusSlider.setThumbImage(img, for:.highlighted)
        //self.lblCustomerAddress.text = objBookedMainService.location
        PlaceAPIWork()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                self.myId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                self.myId = String(id)
            }
        }
        self.bottumDetailView.isHidden = true
        self.imgDropDownDetail.image = #imageLiteral(resourceName: "upper_ico")
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let latitude = location?.coordinate.latitude ?? 0.0
        let longitude = location?.coordinate.longitude ?? 0.0
        
        self.strMyCurrentLatitude = String(latitude)
        self.strMyCurrentLongitude = String(longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation((location ?? nil)!) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if placemarks != nil{
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    print("locality = ",placemark.locality ?? "")
                    print("administrativeArea = ",placemark.administrativeArea ?? "")
                    print("country = ",placemark.country ?? "")
                    print("name = ",placemark.name ?? "")
                    print("subLocality = ",placemark.subLocality ?? "")
                    print("subAdministrativeArea = ",placemark.subAdministrativeArea ?? "")
                    let address = String(placemark.subLocality ?? "")+" "+String(placemark.name ?? "")
                    let address2 = String(placemark.locality ?? "")+" "+String(placemark.administrativeArea ?? "")+" "+String(placemark.country ?? "")
                    self.strMyCurrentAddress  = address+address2
                }}
        }
    }
    
    func configureView(){
        let objUser = objBookedMainService.arrBookingInfo[0]
        if objUser.staffImage != "" {
            if let url = URL(string: objUser.staffImage){
                //self.imgUser.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }else{
            self.imgUser.image = #imageLiteral(resourceName: "cellBackground")
        }
        
        
        
        self.imgCompleted.isHidden = true
        if objBookedService.status == "0"{
            self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            self.lblStatus.text = "Confirmed"
        }else if objBookedService.status == "1"{
            self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            self.lblStatus.text = "On the way"
        }else if objBookedService.status == "2"{
            self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
            self.lblStatus.text = "On going"
        }else if objBookedService.status == "3" && objBookedMainService.bookStatus == "5"{
            self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            self.lblStatus.text = "Service end"
            self.lblStatus.text = "Completed"
            self.imgCompleted.isHidden = false
        }else{
            self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            self.lblStatus.text = "Service end"
        }
        
        
        self.lblName.text = objBookedMainService.arrUserDetail[0].userName
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        self.lblDateAndTime.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        
        self.btnStart.isHidden = true
        self.btnEnd.isHidden = true
        
        let strStartTime =  objBookedService.startTime
        
    }
}

//MARK : - Button method extension
extension TrackMapVC{
    
    @IBAction func btnCurrentLocation(_ sender: UIButton) {
        let mylat = (self.strMyCurrentLatitude as NSString).doubleValue
        let mylong = (self.strMyCurrentLongitude as NSString).doubleValue
        let camera = GMSCameraPosition.camera(withLatitude: mylat, longitude: mylong, zoom: 12)
        self.viewMap.camera = camera
    }
    
    @IBAction func btnUpDownDetail(_ sender: UIButton) {
        self.bottumDetailView.isHidden = !self.bottumDetailView.isHidden
        self.imgDropDownDetail.image = #imageLiteral(resourceName: "DropGray")
        if bottumDetailView.isHidden == true{
            self.imgDropDownDetail.image = #imageLiteral(resourceName: "upper_ico")
        }
    }
    
    
    @IBAction func btnCallOption(_ sender: UIButton) {
        let obj = objBookedMainService.arrUserDetail[0]
        if let url = URL(string: "tel://\(obj.contactNo)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        //    objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnMapType(_ sender: UIButton) {
        if viewMap.mapType == GMSMapViewType.normal{
            viewMap.mapType = GMSMapViewType.satellite
        }else {
            viewMap.mapType = GMSMapViewType.normal
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.timer?.invalidate()
        objAppShareData.onTrackView = false
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- CURRENT LOCATION API
extension TrackMapVC{
    func locationAccess(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self as GMSAutocompleteViewControllerDelegate
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func PlaceAPIWork(){
        let locationManager = INTULocationManager.sharedInstance()
        
        //   objWebserviceManager.StartIndicator()
        locationManager.requestLocation(withDesiredAccuracy: .city,timeout: 10.0,delayUntilAuthorized: true) { (currentLocation, achievedAccuracy, status) in
            
            if (status == INTULocationStatus.success) {
                objWebserviceManager.StopIndicator()
                let lcc = CLLocation.init(latitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)!)
                self.strLatitude = String(describing: (currentLocation?.coordinate.latitude)!)
                self.strLongitude = String(describing: (currentLocation?.coordinate.longitude)!)
                let latTemp = self.strLatitude
                let longTemp = self.strLongitude
                let mylat = (latTemp as NSString).doubleValue
                let mylong = (longTemp as NSString).doubleValue
                self.setDataOnMapWith(lat: mylat, long: mylong)
                _ = self.getAddressFromLocation(location: lcc)
                self.map_Google()
            }else if (status == INTULocationStatus.timedOut) {
                // self.call_for_webservice_NearByUserList()
            }else {
                // self.call_for_webservice_NearByUserList()
            }
        }
        // objWebserviceManager.StartIndicator()
    }
    
    func setDataOnMapWith(lat:Double, long:Double) -> Void
    {
        
        let mylat = ("22.7051" as NSString).doubleValue
        let mylong = ("75.9091" as NSString).doubleValue
        //let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12)
        let camera = GMSCameraPosition.camera(withLatitude: mylat, longitude: mylong, zoom: 12)
        
        self.viewMap.camera = camera
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ico_map_pin_f")
        
        ///////////////
        let DynamicView=UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        DynamicView.backgroundColor=UIColor.clear
        //Creating Marker Pin imageview for Custom Marker
        var imageViewForPinMarker : UIImageView
        //imageViewForPinMarker  = UIImageView(frame:CGRect(x: 0, y: 0, width: 40, height: 40));
        imageViewForPinMarker  = UIImageView(frame:CGRect(x: 0, y: 0, width: 40, height: 40));
        imageViewForPinMarker.image = UIImage(named:"ico_map_pin_f")
        
        //Creating User Profile imageview
        var imageViewForUserProfile : UIImageView
        
        imageViewForUserProfile  = UIImageView(frame:CGRect(x: 11.5, y: 5.5, width: 17, height: 17))
        imageViewForUserProfile.layer.cornerRadius = imageViewForUserProfile.frame.size.height/2
        imageViewForUserProfile.layer.masksToBounds = true
        
        
        //Adding userprofile imageview inside Marker Pin Imageview
        imageViewForPinMarker.addSubview(imageViewForUserProfile)
        //Adding Marker Pin Imageview isdie view for Custom Marker
        DynamicView.addSubview(imageViewForPinMarker)
        //Converting dynamic uiview to get the image/marker icon.
        UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        marker.icon = imageConverted
        arrMarker.append(marker)
        marker.map = self.viewMap
        /////////////////////
    }
    
    
}

//MARK: - PLACE API
extension TrackMapVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.strLatitude = String(place.coordinate.latitude)
        self.strLongitude = String(place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    }
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func getAddressFromLocation(location:CLLocation) ->String{
        var addressString = String()
        CLGeocoder().reverseGeocodeLocation(location,completionHandler: {(placemarks, error) -> Void in
            //locationManager.stopUpdatingLocation()
            if error != nil {
                return
            }
            if (placemarks?.count)! > 0 {
                let pm = placemarks?.last
                if let formattedAddress = pm?.addressDictionary?["FormattedAddressLines"] as? [String] {
                    addressString = formattedAddress.joined(separator: ", ")
                }}
            else {
            }
        })
        return addressString
    }
}

extension TrackMapVC{
    
    func map_Google(){
        viewMap.clear()
        viewMap.setMinZoom(8, maxZoom: viewMap.maxZoom)
        arrMarker.removeAll()
        
        //let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        
        for i in 0...1  {
            let objModel  = objBookedService
            let objUser  = objBookedMainService.arrUserDetail[0]
            
            var position = CLLocationCoordinate2D()
            
            if i == 0{
                position.latitude = Double(strMyTrackingLatitude) ?? 22.75
                position.longitude = Double(strMyTrackingLongitude) ?? 75.88
            }
            if i == 1{
                position.latitude = Double(self.strArtistTrackingLatitude) ?? 0.0
                position.longitude = Double(self.strArtistTrackingLongitude) ?? 0.0
            }
            
            let marker = GMSMarker(position: position)
            marker.accessibilityLabel = objBookedMainService.location
            marker.accessibilityValue = "\(i)"
            marker.groundAnchor = CGPoint(x: 0.50, y: 0.0)
            
            if i == 0{
                CATransaction.begin()
                CATransaction.setAnimationDuration(0.5)
                marker.position = CLLocationCoordinate2D(latitude: Double(strMyTrackingLatitude) ?? 0, longitude: Double(strMyTrackingLongitude) ?? 0)
                CATransaction.commit()
            }
            if i == 1{
                //position.latitude = Double(objAppShareData.objModelBusinessSetup.trackingLatitude ) ?? 0.0
                //position.longitude = Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0
                CATransaction.begin()
                CATransaction.setAnimationDuration(0.5)
                //marker.position = CLLocationCoordinate2D(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0, longitude: Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0)
                CATransaction.commit()
            }
            
            ///////////////
            let DynamicView=UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            DynamicView.backgroundColor=UIColor.clear
            var imageViewForPinMarker : UIImageView
            imageViewForPinMarker  = UIImageView(frame:CGRect(x: 0, y: 0, width: 50, height: 60));
            
            //Creating User Profile imageview
            var imageViewForUserProfile : UIImageView
            imageViewForUserProfile  = UIImageView(frame:CGRect(x: 10, y: 9, width: 30, height: 30
            ))
            imageViewForUserProfile.layer.cornerRadius = imageViewForUserProfile.frame.size.width/2
            imageViewForUserProfile.layer.masksToBounds = true
            
            var imgMy = ""
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [String:Any] ?? ["":""]
            if let imgUrl = userInfo["profileImage"] as? String {
                imgMy = imgUrl
            }
            
            if i == 0{
                imageViewForPinMarker.image = #imageLiteral(resourceName: "blue_map_ico")
                if objUser.profileImage == ""
                {  imageViewForUserProfile.image = #imageLiteral(resourceName: "cellBackground")  }else
                {  let url = URL(string:objUser.profileImage)
                    //imageViewForUserProfile.af_setImage(withURL: url!)
                    imageViewForUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }
                
            }
            
            if i == 1{
                imageViewForPinMarker.image = #imageLiteral(resourceName: "gray_ico")
                //            if objModel.staffImage == ""
                //            {  imageViewForUserProfile.image = #imageLiteral(resourceName: "cellBackground")  }else
                //            {  let url = URL(string:objModel.staffImage)
                //                imageViewForUserProfile.af_setImage(withURL: url!)
                //            }
                if imgMy == ""
                {  imageViewForUserProfile.image = #imageLiteral(resourceName: "cellBackground")  }
                else
                {
                    let url = URL(string:imgMy)
                    //imageViewForUserProfile.af_setImage(withURL: url!)
                    imageViewForUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }
                marker.accessibilityLabel = objBookedMainService.location
                
                
                //marker.accessibilityLabel =  objAppShareData.objModelBusinessSetup.trackingAddress
            }
            
            let coordinate₀ = CLLocation(latitude: Double(strMyTrackingLatitude)  ?? 0.0, longitude: Double(strMyTrackingLongitude) ?? 0.0)
            let coordinate₁ = CLLocation(latitude: Double(self.strArtistTrackingLatitude) ?? 0.0, longitude:  Double(self.strArtistTrackingLongitude) ?? 0.0)
            
            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
            
            if distanceInMeters <= 10{
                if i == 1{
                    imageViewForPinMarker.image = #imageLiteral(resourceName: "map_meet_users_ico")
                    imageViewForUserProfile.image = nil
                }else{
                    imageViewForPinMarker.image = nil
                    imageViewForUserProfile.image = nil
                }
            }
            
            //Adding userprofile imageview inside Marker Pin Imageview
            imageViewForPinMarker.addSubview(imageViewForUserProfile)
            //Adding Marker Pin Imageview isdie view for Custom Marker
            DynamicView.addSubview(imageViewForPinMarker)
            //Converting dynamic uiview to get the image/marker icon.
            UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            marker.icon = imageConverted
            arrMarker.append(marker)
            
            marker.map = self.viewMap
            
            if i == 0{
                marker.title = objUser.userName
                //marker.snippet = objAppShareData.objModelBusinessSetup.trackingAddress
            }
            if i == 1{
                marker.title = objModel.Name
                marker.snippet = objBookedMainService.location
            }
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        }
        
        
        // let startPoint = CLLocation(latitude: Double(self.strLatitude)  ?? 0.0, longitude: Double(self.strLongitude) ?? 0.0)
        let startPoint = CLLocation(latitude: Double(self.staffStartPointLatitude)  ?? 0.0, longitude: Double(self.staffStartPointLongitude) ?? 0.0)
        
        let coordinate₀ = CLLocation(latitude: Double(self.strMyTrackingLatitude)  ?? 0.0, longitude: Double(self.strMyTrackingLongitude) ?? 0.0)
        let coordinate₁ = CLLocation(latitude: Double(self.strArtistTrackingLatitude) ?? 0.0, longitude:  Double(self.strArtistTrackingLongitude) ?? 0.0)
        
        var distanceFromCurrentPointMeters = coordinate₀.distance(from: coordinate₁)
        var distanceFromStartPointMeters = startPoint.distance(from: coordinate₁)
        
        let a = distanceFromCurrentPointMeters/1609.344
        self.lblDistance.text = String(format: "%.2f", a)+" Miles"
        
        self.radiusSlider.minimumValue = 0.0
        self.radiusSlider.maximumValue = Float(distanceFromStartPointMeters)
        
        if distanceFromCurrentPointMeters >= distanceFromStartPointMeters{
            distanceFromStartPointMeters = distanceFromCurrentPointMeters
            self.radiusSlider.maximumValue = Float(distanceFromCurrentPointMeters)
            self.strLatitude = self.strArtistTrackingLatitude
            self.strLongitude = self.strArtistTrackingLongitude
            if maximumValue >= distanceFromCurrentPointMeters {
            }else{
                maximumValue = distanceFromCurrentPointMeters
            }
        }else{
            if maximumValue >= distanceFromStartPointMeters {
            }else{
                maximumValue = distanceFromStartPointMeters
            }
        }
        self.radiusSlider.maximumValue =  Float(maximumValue)
        
        print("distance = ",Float(distanceFromStartPointMeters)-Float(distanceFromCurrentPointMeters))
        
        self.radiusSlider.value = Float(maximumValue-distanceFromCurrentPointMeters)
        
        if distanceFromCurrentPointMeters <= 10{
            
            // if distanceFromCurrentPointMeters <= 10{
            self.viewMaximumValueImage.isHidden = true
            self.radiusSlider.setThumbImage(#imageLiteral(resourceName: "meet_users_ico"), for:.normal)
            self.radiusSlider.value = Float(maximumValue)
        }else{
            self.radiusSlider.setThumbImage(UIImage.init(named: "grayGubbara_ico"),for:.normal)
            let thumbImage = addUserImage(inImage: UIImage.init(named: "grayGubbara_ico")!, atPoint: CGPoint.init(x:9,y:6))
            let img = addUserImageSecond(inImage: thumbImage, atPoint:  CGPoint.init(x:12,y:8))
            self.radiusSlider.setThumbImage(img, for:.normal)
            self.viewMaximumValueImage.isHidden = false
        }
        self.lblCustomerAddress.text =  objBookedMainService.location
        if objAppShareData.onTrackView == true{
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                self.callWebserviceForGetBookedServices()
                //  self.map_Google()
            }
        }
    }
}

//MARK:- Collection view Delegate Methods
extension TrackMapVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objBookedMainService.arrBookingInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = objBookedMainService.arrBookingInfo[indexPath.row].artistServiceName
        cell.lblUserName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.lblUserName.text = obj
        cell.lblUserName.layer.cornerRadius = 10
        cell.lblUserName.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 26
        cellWidth = CGFloat(self.collectionService.frame.size.width-2)
        let a = objBookedMainService.arrBookingInfo[indexPath.row].artistServiceName
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 13)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+20
        cellHeight = 26
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    func addUserImage(inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        var smallImage = #imageLiteral(resourceName: "cellBackground")
        let img = UIImageView()
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //img.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                img.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        img.layer.cornerRadius = 11
        img.layer.masksToBounds = true
        smallImage = img.image ?? #imageLiteral(resourceName: "cellBackground")
        smallImage.draw(in: CGRect(x: 17, y: 22, width: 30, height: 30))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        self.radiusSlider.minimumTrackTintColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func addUserImageSecond(inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let smallImage = #imageLiteral(resourceName: "gary_circle_img2")
        smallImage.draw(in: CGRect(x: 11, y: 15, width: 43, height: 43))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

//MARK: - GET BOOKINGDETAIL
extension TrackMapVC{
    
    @objc func callWebserviceForGetBookedServices(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objActivity.startActivityIndicator()
        let dicParam = [
            "bookingId":objBookedMainService._id
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            self.map_Google()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    print(response)
                    self.saveData(dicts:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            self.map_Google()
            objActivity.stopActivity()
            //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dicts:[String:Any]){
        
        if let strBookingt = dicts["data"] as? [String : Any]{
            let objBooking = Booking.init(dict: strBookingt)
            objBookedMainService = objBooking
            for obj in objBookedMainService.arrBookingInfo{
                if obj._id == objBookedService._id{
                    objBookedService = obj
                    self.lblStaffAddress.text = obj.trackingAddress
                    self.strArtistTrackingLatitude =  obj.trackingLatitude
                    self.strArtistTrackingLongitude =  obj.trackingLongitude
                    
                    self.strMyTrackingLatitude =  objBooking.latitude
                    self.strMyTrackingLongitude =  objBooking.longitude
                    
                    self.configureView()
                    self.map_Google()
                    break
                }
            }
        }
        self.configureView()
    }
    
}


