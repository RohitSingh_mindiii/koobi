import Foundation

public class Booking : NSObject {
    
    var strStaff = ""
    
    var strValue = ""
    public var _id = ""
    public var artistId = ""
    public var bookingType = ""
    public var ratingByUser = 0.0
    public var reviewByUser = ""
    public var paymentTime = ""
    public var cellIndexPath:Int = 0
    public var bookingDate = ""
    public var bookingTime = ""
    public var trasactionId = ""
    public var bookStatus = ""
    public var paymentType = ""
    public var paymentStatus = ""
    public var totalPrice = ""
    public var location = ""
    public var latitude = ""
    public var longitude = ""
    public var allSubServiceName = ""
    public var allServiceId = ""
    public var allSubServiceId = ""
    public var allArtictServiceId = ""
    
    public var isFinsh = ""
    public var transactionId = ""
    public var userId = ""
    
    public var arrUserDetail :[UserDetail] =  [UserDetail]()
    public var arrBookingInfo : [BookingInfo] =  [BookingInfo]()
    public var arrStaffInfo : [StaffDetail] =  [StaffDetail]()

    init(dict: [String : Any]) {
        print(dict)
        _id = String(dict["_id"] as? Int ?? 0)
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }
        
        userId = String(dict["userId"] as? Int ?? 0)
        if let _id1 = dict["userId"] as? String{
            userId = _id1
        }
       
        ratingByUser = dict["userRating"] as? Double ?? 0.0
        if let rating = dict["userRating"] as? Int{
            ratingByUser = Double(rating)
        }
        if let rating = dict["userRating"] as? String{
            ratingByUser = Double(rating) ?? 0.0
        }
        
        if let strReview = dict["reviewByUser"] as? String{
            reviewByUser = strReview
        }
        
        if let time = dict["paymentTime"] as? String{
            paymentTime = time
        }
        
        artistId = String(dict["artistId"] as? Int ?? 0)
        if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }
        bookingDate = dict["bookingDate"] as? String ?? ""
        bookingTime = dict["bookingTime"] as? String ?? ""
        location = dict["location"] as? String ?? ""
        latitude = dict["latitude"] as? String ?? ""
        longitude = dict["longitude"] as? String ?? ""
        if let status = dict["bookStatus"] as? Int{
            bookStatus = String(status)
         }else if let status = dict["bookStatus"] as? String{
                bookStatus = status
        }
        
        if let temp = dict["transjectionId"] as? Int{
            self.transactionId = String(temp)
        }else if let temp = dict["transjectionId"] as? String{
            self.transactionId = temp
        }
        
        
        if let status = dict["isFinsh"] as? Int {
            self.isFinsh = String(status)
        }else if let status = dict["isFinsh"] as? String {
            self.isFinsh = status
        }
        
        if let prise = dict["totalPrice"] as? Int{
            totalPrice = String(prise)
        }else if let prise1 = dict["totalPrice"] as? String{
            totalPrice = prise1
        }
        
        if let status = dict["bookingType"] as? Int{
            bookingType = String(status)
        }else if let status = dict["bookingType"] as? String{
            bookingType = status
        }
        
        if let temp = dict["paymentType"] as? Int{
            self.paymentType = String(temp)
        }else if let temp = dict["paymentType"] as? String{
            self.paymentType = temp
        }

        
//        paymentType = String(dict["paymentType"] as? Int ?? 0)
//        if let paymentType1 = dict["paymentType"] as? String{
//            paymentType = paymentType1
//        }
        
        trasactionId = String(dict["transjectionId"] as? Int ?? 0)
        if let trasactionIdnn = dict["transjectionId"] as? String{
            trasactionId = trasactionIdnn
        }
        
        paymentStatus = String(dict["paymentStatus"] as? Int ?? 0)
        if let paymentStatus1 = dict["paymentStatus"] as? String{
            paymentStatus = paymentStatus1
        }
    
        if let arr = dict["bookingInfo"] as? [[String : Any]]{
            for dict in arr{
                let obj = BookingInfo.init(dict: dict)
//                if objAppShareData.objBookingCalendarVC.fromStaffSelectId != ""{
//                    if obj.staffId == objAppShareData.objBookingCalendarVC.fromStaffSelectId{
//                        self.arrBookingInfo.append(obj)
//                    }
//                }else{
                    self.arrBookingInfo.append(obj)
//                }
            }
        }

        
//        if let arr1 = dict["userDetail"] as? [[String : Any]]{
//            for dict in arr1{
//                let obj1 = UserDetail.init(dict: dict)
//                self.arrUserDetail.append(obj1)
//            }
//        }
        
        if let arr1 = dict["artistDetail"] as? [[String : Any]]{
            for dict in arr1{
                let obj1 = UserDetail.init(dict: dict)
                self.arrUserDetail.append(obj1)
            }
        }
 
        if self.arrBookingInfo.count > 0 {
           
            var arrSubServiceName:[String] = [String]()
            var arrServiceId:[String] = [String]()
            var arrSubServiceId:[String] = [String]()
            var arrArtictServiceId:[String] = [String]()
            
            for obj in arrBookingInfo{
                
                allSubServiceName = obj.artistServiceName
                allServiceId = obj.serviceId
                allSubServiceId = obj.subServiceId
                allArtictServiceId = obj.artistServiceId
                
                arrSubServiceName.append(allSubServiceName)
                arrServiceId.append(allServiceId)
                arrSubServiceId.append(allSubServiceId)
                arrArtictServiceId.append(allArtictServiceId)

            }
            arrArtictServiceId = Array(Set(arrArtictServiceId))
            arrSubServiceId = Array(Set(arrSubServiceId))
            arrServiceId = Array(Set(arrServiceId))
            
            if arrSubServiceName.count > 0{
                if arrSubServiceName.count >= 2{
                    let arrTemp = arrSubServiceName[0...arrSubServiceName.count-1]
                    allSubServiceName = arrTemp.joined(separator: ", ")
                }else{
                    allSubServiceName = arrSubServiceName[0]
                }
            }
            
            if arrServiceId.count > 0{
                if arrServiceId.count >= 2{
                    let arrTemp = arrServiceId[0...arrServiceId.count-1]
                    allServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allServiceId = arrServiceId[0]
                }
            }
            
            if arrSubServiceId.count > 0{
                if arrSubServiceId.count >= 2{
                    let arrTemp = arrSubServiceId[0...arrSubServiceId.count-1]
                    allSubServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allSubServiceId = arrSubServiceId.joined(separator: ", ")
                }
            }
            
            if arrArtictServiceId.count > 0{
                if arrArtictServiceId.count >= 2{
                    let arrTemp = arrArtictServiceId[0...arrArtictServiceId.count-1]
                    allArtictServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allArtictServiceId = arrArtictServiceId.joined(separator: ", ")
                }
            }
        }
    }
    
    public func joined(separator: String) -> String {
        return strValue
    }
  
}

public class UserDetail {
    
    public var _id = ""
    public var userName = ""
    public var profileImage = ""
    public var ratingCount = ""
    public var countryCode = ""
    public var contactNo = ""
    init(dict: [String : Any]) {
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }
        
        countryCode = dict["countryCode"] as? String ?? ""
        ratingCount = String(dict["ratingCount"] as? Int ?? 0)
        contactNo = String(dict["contactNo"] as? String ?? "0000000000")
        userName = dict["userName"] as? String ?? ""
        profileImage = dict["profileImage"] as? String ?? ""
    }
}



