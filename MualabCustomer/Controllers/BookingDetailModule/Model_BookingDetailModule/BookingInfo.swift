

import Foundation

public class BookingInfo:NSObject {
	
    public var _id : String = ""
    public var Name : String = ""

	public var bookingPrice : String = ""
	public var serviceId : String = ""
	public var subServiceId : String = ""
	public var artistServiceId : String = ""
	public var bookingDate : String = ""
	public var startTime : String = ""
	public var endTime : String = ""
	public var staffId : String = ""
	public var staffName : String = ""
	public var staffImage : String = ""
	public var artistServiceName : String = ""
    public var companyName : String = ""
    public var status : String = ""
    public var trackingLatitude : String = ""
    public var trackingLongitude : String = ""
    public var trackingAddress : String = ""
    var arrReportList : [ModelReportList] =  [ModelReportList]()
    
    
    init(dict: [String : Any]){
    
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }
        bookingPrice = String(dict["bookingPrice"] as? Int ?? 0)
        if let status = dict["bookingPrice"] as? String{
            bookingPrice = status
        }
    
        if let status1 = dict["trackingLatitude"] as? String{
            trackingLatitude = status1
        }
        if let status1 = dict["trackingLongitude"] as? String{
            trackingLongitude = status1
        }
        
        status = String(dict["status"] as? Int ?? 0)
        if let status1 = dict["status"] as? String{
            status = status1
        }
        
        serviceId = String(dict["serviceId"] as? Int ?? 0)
        if let status = dict["serviceId"] as? String{
            serviceId = status
        }
        subServiceId = String(dict["subServiceId"] as? Int ?? 0)
        if let status = dict["subServiceId"] as? String{
            subServiceId = status
        }
        artistServiceId = String(dict["artistServiceId"] as? Int ?? 0)
        if let status = dict["artistServiceId"] as? String{
            artistServiceId = status
        }
    
        staffId = String(dict["staffId"] as? Int ?? 0)
        if let status = dict["staffId"] as? String{
            staffId = status
        }
        trackingAddress = dict["trackingAddress"] as? String ?? ""

        staffName = dict["staffName"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        artistServiceName = dict["artistServiceName"] as? String ?? ""
        bookingDate = dict["bookingDate"] as? String ?? ""
        startTime = dict["startTime"] as? String ?? ""
        endTime = dict["endTime"] as? String ?? ""
        companyName = dict["companyName"] as? String ?? ""
        
        if let arr = dict["bookingReport"] as? [[String : Any]]{
            for dict in arr{
                let obj = ModelReportList.init(dict: dict)
                self.arrReportList.append(obj)
            }
        }
        
    }
}

class ModelReportList: NSObject {
    var adminReason : String = ""
    var _id : String = ""
    var bookingId : String = ""
    var bookingInfoId :String = ""
    var businessId : String = ""
    var crd : String = ""
    var descriptions : String = ""
    var reportByUser : String = ""
    var reportForUser : String = ""
    var title : String = ""
    var status : String = "0"
    
    init(dict : [String : Any]){
        
        _id = String(dict["_id"] as? Int ?? 0)
        if let id = dict["_id"] as? String{
            _id = id
        }
        status = String(dict["status"] as? Int ?? 0)
        if let sts = dict["status"] as? String{
            status = sts
        }
        if let sts = dict["adminReason"] as? String{
            adminReason = sts
        }
        bookingId = String(dict["bookingId"] as? Int ?? 0)
        if let id = dict["bookingId"] as? String{
            bookingId = id
        }
        
        self.bookingInfoId = String(dict["bookingInfoId"] as? Int ?? 0)
        if let id = dict["bookingInfoId"] as? String{
            bookingInfoId = id
        }
        
        businessId = String(dict["businessId"] as? Int ?? 0)
        if let id = dict["businessId"] as? String{
            businessId = id
        }
        
        reportByUser = String(dict["reportByUser"] as? Int ?? 0)
        if let id = dict["reportByUser"] as? String{
            reportByUser = id
        }
        
        reportForUser = String(dict["reportForUser"] as? Int ?? 0)
        if let id = dict["reportForUser"] as? String{
            reportForUser = id
        }
        
        descriptions = dict["description"] as? String ?? ""
        crd = dict["crd"] as? String ?? ""
        title = dict["title"] as? String ?? ""
        crd = objAppShareData.crtToDateString(crd: crd)
    }
}

public class ModelReportReason : NSObject {
    
    
    public var _id = ""
    public var __v = ""
    public var title = ""
    public var type = ""
    
    init(dict: [String : Any]) {
        _id = String(dict["_id"] as? Int ?? 0)
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }
        
        __v = String(dict["__v"] as? Int ?? 0)
        if let _id1 = dict["__v"] as? String{
            __v = _id1
        }
        title = dict["title"] as? String ?? ""
        type = dict["type"] as? String ?? ""
    }
}

