

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class StaffDetail {
  
    public var staffId = ""
    public var satffName = ""
    public var staffImage = ""
    public var job = ""

    init(dict: [String : Any]) {
        staffId = String(dict["staffId"] as? Int ?? 0)
        if let status = dict["staffId"] as? String{
            staffId = status
        }
        satffName = dict["staffName"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        job = dict["job"] as? String ?? ""
        
    }
}


public class CompanyInfo {
    
    public var _id = ""
    public var address = ""
    public var artistId = ""
    public var businessId = ""
    public var businessName = ""
    public var profileImage = ""
    public var userName = ""
    public var salary = ""
    public var message = ""
    public var holiday = ""
    public var jobTitle = ""
    public var mediaAccess = ""
    public var arrBusinessType = [String]()
    var staffHours : [AddStaffHours] = [AddStaffHours]()
    var staffService : [AddStaffStaffService] = [AddStaffStaffService]()

    init(dict: [String : Any]) {
        
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }else if let _id1 = dict["_id"] as? Int{
            _id = String(_id1)
        }
        
        if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }else if let artistId1 = dict["artistId"] as? Int{
            artistId = String(artistId1)
        }
        
        if let businessId1 = dict["businessId"] as? String{
            businessId = businessId1
        }else if let businessId1 = dict["businessId"] as? Int{
            businessId = String(businessId1)
        }
        
        if let holiday1 = dict["holiday"] as? String{
            holiday = holiday1
        }else if let holiday1 = dict["holiday"] as? Int{
            holiday = String(holiday1)
        }
        
        businessName = dict["businessName"] as? String ?? ""
        profileImage = dict["profileImage"] as? String ?? ""
        address = dict["address"] as? String ?? ""
        
        jobTitle = dict["job"] as? String ?? ""
        mediaAccess = dict["mediaAccess"] as? String ?? ""
        
        userName = dict["userName"] as? String ?? ""

        if let arr = dict["businessType"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = dict["serviceName"] as? String ?? ""
                self.arrBusinessType.append(objSubService)
            }
        }
        
        if let arr = dict["staffHours"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffHours.init(dict: dict)
                self.staffHours.append(objSubService)
            }
        }

        if let arr = dict["staffService"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffStaffService.init(dict: dict)
                self.staffService.append(objSubService)
            }
        }
        
        if let _id1 = dict["message"] as? String{
            message = _id1
        }
        
        if let _id1 = dict["salaries"] as? String{
            salary = _id1
        }else if let _id1 = dict["salaries"] as? Int{
            salary = String(_id1)
        }
    }
}


public class AddStaffStaffService:NSObject {
    
    var _Id:String = ""
    var artistId:String = ""
    var artistServiceId:String = ""
    var businessId:String = ""
    var completionTime : String = ""
    var inCallPrice : String = ""
    var outCallPrice : String = ""
    var serviceId:String = ""
    var subServiceId : String = ""
    
    
    var __v = "0"
    //    var _id  = 0
    //    var artistId = 0
    //    var artistServiceId = 0
    //    var businessId = 0
    //    var completionTime = "0"
    var crd = "0"
    var deleteStatus = "0"
    //    var inCallPrice = "0"
    //    var outCallPrice = "0"
    //    var serviceId = 0
    var staffId = "0"
    var status = "0"
    //   var subserviceId = 0
    var title = ""
    var upd = "0"
    var serviceType = ""
    
    init(dict: [String : Any]){
        
        if let _Id1 = dict["_id"] as? Int{
            _Id = String(_Id1)
        }else if let _Id1 = dict["_id"] as? String{
            _Id = _Id1
        }
        
        if let subserviceId1 = dict["subserviceId"] as? Int{
            subServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["subserviceId"] as? String{
            subServiceId = subserviceId1
        }
        
        if let subserviceId1 = dict["artistServiceId"] as? Int{
            artistServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["artistServiceId"] as? String{
            artistServiceId = subserviceId1
        }
        
        
        if let artistId1 = dict["artistId"] as? Int{
            artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }
        
        if let artistServiceId1 = dict["artistServiceId"] as? Int{
            artistServiceId = String(artistServiceId1)
        }else if let artistServiceId1 = dict["artistServiceId"] as? String{
            artistServiceId = artistServiceId1
        }
        
        
        
        if let businessId1 = dict["businessId"] as? Int{
            businessId = String(businessId1)
        }else if let businessId1 = dict["businessId"] as? String{
            businessId = businessId1
        }
        
        
        if let inCallPrice1 = dict["inCallPrice"] as? Int{
            inCallPrice = String(inCallPrice1)
        }else if let inCallPrice1 = dict["inCallPrice"] as? String{
            inCallPrice = inCallPrice1
        }
        
        
        if let outCallPrice1 = dict["outCallPrice"] as? Int{
            outCallPrice = String(outCallPrice1)
        }else if let outCallPrice1 = dict["outCallPrice"] as? String{
            outCallPrice = outCallPrice1
        }
        
        if let completionTime1 = dict["completionTime"] as? Int{
            completionTime = String(completionTime1)
        }else if let completionTime1 = dict["completionTime"] as? String{
            completionTime = completionTime1
        }
        
        if let serviceId1 = dict["serviceId"] as? Int{
            serviceId = String(serviceId1)
        }else if let serviceId1 = dict["serviceId"] as? String{
            serviceId = serviceId1
        }
        
        if let subserviceId1 = dict["subserviceId"] as? Int{
            subServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["subserviceId"] as? String{
            subServiceId = subserviceId1
        }
        
        if let __v1 = dict["__v"] as? Int{
            __v = String(__v1)
        }else if let __v1 = dict["__v"] as? String{
            __v = __v1
        }
        
        if let completionTime1 = dict["completionTime"] as? Int{
            completionTime = String(completionTime1)
        }else if let completionTime1 = dict["completionTime"] as? String{
            completionTime = completionTime1
        }
        
        if let inCallPrice1 = dict["inCallPrice"] as? Int{
            inCallPrice = String(inCallPrice1)
        }else if let inCallPrice1 = dict["inCallPrice"] as? String{
            inCallPrice = inCallPrice1
        }
        
        outCallPrice = dict["outCallPrice"] as? String ?? ""
        
        if let outCallPrice1 = dict["outCallPrice"] as? Int{
            outCallPrice = String(outCallPrice1)
        }else if let outCallPrice1 = dict["outCallPrice"] as? String{
            outCallPrice = outCallPrice1
        }
        
        if let staffId1 = dict["staffId"] as? Int{
            staffId = String(staffId1)
        }else if let staffId1 = dict["staffId"] as? String{
            staffId = staffId1
        }
        
        if let status1 = dict["status"] as? Int{
            status = String(status1)
        }else if let status1 = dict["status"] as? String{
            status = status1
        }
        
        if let title1 = dict["title"] as? Int{
            title = String(title1)
        }else if let title1 = dict["title"] as? String{
            title = title1
        }
        
        if let title1 = dict["bookingType"] as? Int{
            serviceType = String(title1)
        }else if let title1 = dict["bookingType"] as? String{
            serviceType = title1
        }
    }
}
class ModelEditTimeSloat:NSObject{
    var arrFinalTimes = [openingTimes]()
    var arrStoreTimes = [openingTimes]()
    var arrValidTimes = [openingTimes]()
}
