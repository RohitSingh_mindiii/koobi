//
//  BookingCompanyVC.swift
//  MualabCustomer
//
//  Created by mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit
import HCSStarRatingView

class BookingCompanyVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblNoDataFound: UILabel!
    
   //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
   
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    


    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.call_for_webservice_BookingList()
        //refreshControl.endRefreshing()
    }
}

//MARK: - view Life cycle
extension BookingCompanyVC{
    
    //MARK: - SystemMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
//MARK: - userDefine Method extension
extension BookingCompanyVC{
    func configureView(){
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.lblNoDataFound.isHidden = true
        self.tblBookingList.addSubview(self.refreshControl)
    }
}

// MARK: - table view Delegate and Datasource methods extension
extension BookingCompanyVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "CellBookingCompanyVC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CellBookingCompanyVC
        //cell.imgUserProfile.af_setImage(withURL:URL.init(string: ("http:////cara2v.com:8000/uploads/profile/1516687886329.jpg"))!)
        //cell.lblUserName.text = "Deependra "
        //cell.lblServiceCategory.text = "Make-Up"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//        let sb = UIStoryboard(name: "Notification", bundle: nil)
//        let detailVC = sb.instantiateViewController(withIdentifier: "TagServicesVC") as! TagServicesVC
//        self.navigationController!.pushViewController(detailVC, animated : true)
        
    }
}

//MARK: - webservices extension
fileprivate extension BookingCompanyVC{
    
    func call_for_webservice_BookingList() -> Void {
        if  !self.refreshControl.isRefreshing{
            objWebserviceManager.StartIndicator()
        }
        objWebserviceManager.requestGet(strURL: "", params: nil, success: { response in
            self.refreshControl.endRefreshing()
            print("response is = \(response)")
            let keyExists = response["responseCode"] != nil
            if  keyExists
            {
                objWebserviceManager.StopIndicator()
                showAlertVC(title: kAlertTitle, message: "Session expired, please Login again", controller: self)
            }
            else
            {
                let message = response["message"] as? String ?? ""
                let status = response["status"] as? String ?? ""
                if (status == "success"){
                    objWebserviceManager.StopIndicator()
                    // self.parseNotificationData(data: response )
                }else{
                    objWebserviceManager.StopIndicator()
                    if message == "No Record found"{
                        // self.viewNoNotificationFound.isHidden = false
                    }else{
                    showAlertVC(title:kAlertTitle,message:message,controller:self)
        }}}
        })
        { (Error) in
            self.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: "something went wrong", controller: self)
}}}

//MARK: - Button extension

fileprivate extension BookingCompanyVC {
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
}
