//
//  CalenderCollectionCell.swift
//  MualabCustomer
//
//  Created by Mac on 14/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CalenderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgWatch: UIImageView!
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var vwBlurr: UIView!
    @IBOutlet weak var lblTime: UILabel!
}
