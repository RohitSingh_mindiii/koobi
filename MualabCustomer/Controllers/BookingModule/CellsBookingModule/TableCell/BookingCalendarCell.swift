//
//  BookingCalendarCell.swift
//  MualabCustomer
//
//  Created by Mac on 14/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class BookingCalendarCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
