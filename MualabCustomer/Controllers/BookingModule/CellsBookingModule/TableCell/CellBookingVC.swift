//
//  CellBookingVC.swift
//  MualabCustomer
//
//  Created by mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellBookingVC: UITableViewCell {

    @IBOutlet weak var viewCellBG: UIView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblServiceCategory: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
       // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

