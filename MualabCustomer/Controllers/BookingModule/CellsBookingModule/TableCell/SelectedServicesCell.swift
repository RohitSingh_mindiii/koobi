//
//  SelectedServicesCell.swift
//  MualabCustomer
//
//  Created by Mac on 14/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit


@objc protocol SelectedServicesCellDelegate{
    func btnEditTapped(at index:IndexPath)
}

class SelectedServicesCell: UITableViewCell {
    
    
   
    @IBOutlet weak var lblSubServiceName: UILabel!
    @IBOutlet weak var lblSubSubServiceName: UILabel!
    @IBOutlet weak var lblServiceDateTime: UILabel!
    
    
    @IBOutlet weak var btnEdit: UIButton!
    
    weak var delegate:SelectedServicesCellDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        self.delegate?.btnEditTapped(at: indexPath)
    }
}

