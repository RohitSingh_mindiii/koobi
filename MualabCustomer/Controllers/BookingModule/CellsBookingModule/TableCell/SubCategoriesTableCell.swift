//
//  SubCategoriesTableCell.swift
//  MualabBusiness
//
//  Created by Mac on 20/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol SubCategoriesTableCellDelegate{
    func btnCheckBoxTapped(at index:IndexPath)
}

class SubCategoriesTableCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgVwCheck: UIImageView?
    
    weak var delegate:SubCategoriesTableCellDelegate!
    var indexPath:IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnCheckAction(_ sender: Any) {
        // self.delegate?.btnCheckBoxTapped(at: indexPath)
    }   
}
