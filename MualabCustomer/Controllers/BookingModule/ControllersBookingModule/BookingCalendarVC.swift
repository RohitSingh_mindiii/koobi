//
//  BookingCalendarVC.swift
//  MualabCustomer
//
//  Created by Mac on 14/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

/*
 
 FSCalendar Modification
 
 //Amit Modified - remove top border //line 237 FSCalendar.m
 //[self addSubview:view];
 
 //Amit Modified - remove bottom border //line-242 FSCalendar.m
 // [self addSubview:view];
 
 //Amit Modified - Hide calendarHeaderView set headerHeight to 0 //line - 337 FSCalendar.m
 headerHeight = 0;
 
 //Amit Modified - increase weekday Height //line - 339 FSCalendar.m
 weekdayHeight = weekdayHeight + 16;
 */

import UIKit
import HCSStarRatingView
import FSCalendar


class BookingCalendarVC: UIViewController,UITableViewDelegate,UITableViewDataSource,FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate {
    
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var arrToShow = [TimeSlot]()
    var arrSelectedService = [BookingServices]()
    var dateSelected : Date = Date()
    var objBookingServices = BookingServices()
    var isServiceAlreadyAdded : Bool = false
    
    var tempDict = [String : Any]()

    @IBOutlet weak var lblSelectedDete: UILabel!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    
    //Calendar
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!
    @IBOutlet weak var dataHeightConstraint: NSLayoutConstraint!
    var selectedIndex = -1
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    //Calendar
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblNoSlotFound: UILabel!
    
    //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var collectionVw: UICollectionView!
    
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!

}

//MARK: - view Life cycle
extension BookingCalendarVC{
    
    //MARK: - SystemMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)

        self.configureCalendar()
        configureView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        selectedIndex = -1
        self.lblNoSlotFound.isHidden = true
        self.dataHeightConstraint.constant = self.view.bounds.height - 30
        dateSelected = Date()
        self.updateArtistInfo()
    
        if !objAppShareData.isFromConfirmBooking {
            self.addBookingServicesObj()
            
            let date = self.setCalenderInitialDateAccordingToFirstServiceDate()
            self.calendar.select(date)
            dateSelected = date
            self.changeCalenderHeader(date: date)
            self.getlatestSlotDataFor(dateSelected: date)
        }
        
        objAppShareData.isFromConfirmBooking = false
        self.adjustTableBookingHeight()        
    }
    
    func addBookingServicesObj(){
        
        isServiceAlreadyAdded = false
        
        if objAppShareData.arrSelectedService.count > 0 {
            
            if objBookingServices.subSubServiceId != 0 {
                
                if objAppShareData.arrSelectedService.contains(where: { $0.subSubServiceId == objBookingServices.subSubServiceId }) {
                    
                    let arrTemp = objAppShareData.arrSelectedService
                    for (index, objTempBookingServices) in arrTemp.enumerated() {
                        if objTempBookingServices.subSubServiceId == self.objBookingServices.subSubServiceId{
                            
                            if self.objBookingServices.isFromEdit {
                                self.objBookingServices.isFromEdit = false
                                let element = objAppShareData.arrSelectedService.remove(at: index)
                                
                                //new
                                element.staffId = objBookingServices.staffId
                                element.completionTime = objBookingServices.completionTime
                                element.price
                                    = objBookingServices.price
                                
                                isServiceAlreadyAdded =  true
                                //new
                                
                                element.isBooked = false
                                element.bookingDate = ""
                                element.bookingTime = ""
                                
                                objAppShareData.arrSelectedService.insert(element, at: 0)
                            }
                            break
                        }
                    }
                } else {
                    
                    objAppShareData.arrSelectedService.insert(objBookingServices, at: 0)
                }
            }
        }else{
            objAppShareData.arrSelectedService.insert(objBookingServices, at: 0)
        }
        
        self.arrSelectedService = objAppShareData.arrSelectedService
        self.tblBookingList.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        adjustTableBookingHeight()
    }
    
    func updateArtistInfo(){
        
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistDetails.userName
        self.viewRating.value = CGFloat(objArtistDetails.ratingCount)
    }
    
  
}

//MARK: - userDefine Method extension
extension BookingCalendarVC{
    
    func configureView(){
        
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.lblNoDataFound.isHidden = true
        self.collectionVw.delegate = self
        self.collectionVw.dataSource = self
    }
}

// MARK: - table view Delegate and Datasource methods extension
extension BookingCalendarVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSelectedService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "BookingCalendarCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarCell{
            
            let objBookingServices : BookingServices = self.arrSelectedService[indexPath.row]
            
            cell.lblName.text = objBookingServices.subSubServiceName
            cell.lblTime.text = ""
            
            let doubleStr = String(format: "%.2f", ceil(objBookingServices.price * 100)/100)
            cell.lblPrice.text = "£" + doubleStr
            
            if objBookingServices.bookingDate != "" && objBookingServices.bookingTime != ""{
                let str = objBookingServices.bookingDate + ", \(objBookingServices.bookingTime)"
                cell.lblTime.text = str
            }else{
                cell.lblTime.text = "Select date and time"
            }
            return cell
        }else{
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let objBookingServices : BookingServices = self.arrSelectedService[indexPath.row]
        
        var swipeConfig = UISwipeActionsConfiguration(actions: [])
        swipeConfig.performsFirstActionWithFullSwipe = false
        
        if objBookingServices.bookingDate == "" && objBookingServices.bookingTime == ""{
            let deleteAction = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
        
            swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction])
        }
        
        return swipeConfig
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    @available(iOS 11.0, *)
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .normal, title: "Remove") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            
            self.removeSelectedServiceFromArray(indexPath : indexPath)
            
            completionHandler(true)
        }
        action.image = UIImage(named: "remove_ico1")
        action.backgroundColor = UIColor.init(red: 182.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        return action
    }
    
    func removeSelectedServiceFromArray(indexPath : IndexPath){
        
        let objBookingServices : BookingServices = self.arrSelectedService[indexPath.row]
        if objBookingServices.bookingId == 0 {
            self.arrSelectedService.remove(at: indexPath.row)
            objAppShareData.arrSelectedService = self.arrSelectedService
            self.tblBookingList.deleteRows(at: [indexPath], with: .left)
            self.adjustTableBookingHeight()
        }else{
            self.callWebserviceFor_deleteSelectedbookin(objBookingServices: objBookingServices, indexPath: indexPath)
        }
    }
}

//MARK: - Button extension

fileprivate extension BookingCalendarVC {
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.gotoProfileVC()
       // objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.arrSelectedService.count > 0{
             self.showAlertMessage()
        }else{
            
        objAppShareData.arrSelectedService.removeAll()
            self.objArtistDetails.isOutCallSelected = false
            objAppShareData.isFromEditBooking = false
            self.gotoBookingIndividualVC()
            objAppShareData.stopTimerForHoldBookings()
        }
       
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnRebookAction(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoOptionSelectionVC()
    }
    
    @IBAction func btnAddMoreServicesAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.arrSelectedService.count > 0{
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            
            if !objBookingServices.isBooked{
                
                if objBookingServices.bookingDate != "" && objBookingServices.bookingTime != ""{
                    
                    self.createDictForBookingSlot(objBookingServices: objBookingServices, isFromAddMoreButton: true)
                    
                }else{
                    objAppShareData.showAlert(withMessage: "Please select service date and time", type: alertType.bannerDark, on: self)
                }
                
            }else{
                self.gotoBookingIndividualVCFromAddMoreButton()
            }
        }else{
            self.gotoBookingIndividualVCFromAddMoreButton()
        }
    }
    
    @IBAction func btnConfirmBookingAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            
            if !objBookingServices.isBooked{
                
                if objBookingServices.bookingDate != "" && objBookingServices.bookingTime != ""{
                    
                    self.createDictForBookingSlot(objBookingServices: objBookingServices, isFromAddMoreButton: false)
                }else{
                    objAppShareData.showAlert(withMessage: "Please select service date and time", type: alertType.bannerDark, on: self)
                }
                
            }else{
                gotoBookingListVC()
            }
        }else{
            objAppShareData.showAlert(withMessage: "No service selected", type: alertType.bannerDark, on: self)
        }
    }
    
    func gotoBookingListVC(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingListVC") as? BookingListVC {
            objVC.objArtistDetails = objArtistDetails
            objVC.arrSelectedService = arrSelectedService
            objAppShareData.isFromEditBooking = false
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnShowWorkingHoursAction(_ sender: UIButton) {
        self.view.endEditing(true)
    
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as? BusinessHoursVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
        }
    }
}

extension BookingCalendarVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    // MARK: - collection view delegate and data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrToShow.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionCell", for:
            indexPath) as? CalenderCollectionCell)!
        
        let objTimeSlot = self.arrToShow[indexPath.row]
        cell.lblTime.text = objTimeSlot.strTimeSlot
        
        if selectedIndex == indexPath.row {
            cell.vwBg.backgroundColor = UIColor.theameColors.greenCalendar
        }else{
            cell.vwBg.backgroundColor = UIColor.lightGray
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let objTimeSlot = self.arrToShow[indexPath.row]
        
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            
            if !objBookingServices.isBooked{
                
                objBookingServices.bookingDate =  getFormattedBookingDateFrom(dateSelected:dateSelected)
                objBookingServices.bookingDateForServer =  getFormattedBookingDateForServerFrom(dateSelected:dateSelected)
                objBookingServices.bookingTime = objTimeSlot.strTimeSlot
                self.tblBookingList.reloadData()
                
            }else{
                objAppShareData.showAlert(withMessage: "Service Already Added", type: alertType.bannerDark,on: self)
            }
        }
        
        selectedIndex = indexPath.row
        self.collectionVw.reloadData()
    }
    
}

// MARK:- FSCalendar delegate

extension BookingCalendarVC{
 
    @IBAction func btnTodayClicked(_ sender: UIButton) {
        self.calendar.setCurrentPage(Date(), animated: false)
        dateSelected = Date()
        self.calendar.select(Date())
        self.getlatestSlotDataFor(dateSelected: Date())
        self.clearFirstServiceSelectedTimeDate()
    }
    
    /**
     Tells the delegate the calendar is about to change the bounding rect.
     */
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant = bounds.height
                self.view.layoutIfNeeded()
            }
        })
        
    }
    
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! == date2!){
            self.getlatestSlotDataFor(dateSelected: Date())
        }else{
            self.getlatestSlotDataFor(dateSelected: date)
        }
        dateSelected = date
        self.clearFirstServiceSelectedTimeDate()
        
       // let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    
    /**
     Close past dates in FSCalendar
     */
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at     monthPosition: FSCalendarMonthPosition) -> Bool{
        
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! < date2!){
           objAppShareData.showAlert(withMessage: "You can't select previous date for booking", type: alertType.bannerDark, on: self)
            return false
        }else{
            return true
        }
    }
}

// MARK:- Btn Action
extension BookingCalendarVC{
    
    @IBAction func toggleCalenderAction(sender: AnyObject) {
        
        if self.calendar.scope == .month {
            setCalenderScopeWeek()
        } else {
            setCalenderScopeMonth()
        }
    }
    
    @IBAction func btnPreviousAction(sender: AnyObject) {
        
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = -1 // For prev button -1, For next button 1
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = 1 // For prev button -1, For next button 1
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
}

//Notification
extension BookingCalendarVC{
    
    func configureCalendar(){
        
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        self.calendar.calendarHeaderView.isHidden = true
        self.calendar.scope = .week
        self.calendar.accessibilityIdentifier = "calendar"
    }
    
    func changeCalenderHeader(date : Date){
       
        let fmt = DateFormatter()
        fmt.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        fmt.dateFormat = "MMMM yyyy"
        let strCalenderHeader = fmt.string(from: date)
        self.lblSelectedDete.text = strCalenderHeader
    }
    
    func setCalenderScopeWeek(){
        
        self.calendar.setScope(.week, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
    
    func setCalenderScopeMonth(){
        
        self.calendar.setScope(.month, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
}

//Notification
extension BookingCalendarVC{
    
    func adjustTableBookingHeight(){
        UIView.animate(withDuration: 0.4, animations: {
            DispatchQueue.main.async {
                self.heightTableBookingConstraint.constant = self.tblBookingList.contentSize.height
                self.view.layoutIfNeeded()
            }
        })
    }
}

// MARK:- Custom Methods
extension BookingCalendarVC : OptionSelectionVCDelegate{
    
    func gotoProfileVC (){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func setCalenderInitialDateAccordingToFirstServiceDate()-> Date{
        
        var arrTemp = [BookingServices]()
        if self.arrSelectedService.count > 0{
            
            let arrFiltered = self.arrSelectedService.filter{ $0.isBooked == true }
            
            if arrFiltered.count > 0{
                
                let arrSorted = arrFiltered.sorted(by: { $0.mergedBookingDate!.compare($1.mergedBookingDate!) == .orderedAscending })
                if arrSorted.count > 0{
                    arrTemp = arrSorted
                }
            }
        }
        
        if arrTemp.count > 0{
            let objBookingServices : BookingServices = arrTemp[0]
            if let date = objBookingServices.mergedBookingDate{
                return date
            }else{
                return Date()
            }
            
        }else{
            return Date()
        }
    }
    
    func finishSectionWithData(strSelectedOption: String) {
        dismiss(animated: true, completion: nil)
        //OptionSelectionVCDelegate
        //"Weekely", "Monthly", "After Every 15 days"
    }
    
    func gotoOptionSelectionVC(){
        
         let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
            
            if let objVC = sb.instantiateViewController(withIdentifier:"OptionSelectionVC") as? OptionSelectionVC{
                objVC.modalPresentationStyle = .overCurrentContext
                objVC.delegate = self
                present(objVC, animated: true, completion: nil)
            }
    }
}


// MARK:- Webservice Call
extension BookingCalendarVC {
    
    func callWebserviceFor_deleteSelectedbookin(objBookingServices : BookingServices, indexPath : IndexPath){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let parameters : Dictionary = [
            "bookingId" : objBookingServices.bookingId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteBookService, params: parameters  , success: { response in

            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.addBoolForServiceEditFeature(objBookingServices : objBookingServices)
             
                    self.arrSelectedService.remove(at: indexPath.row)
                    objAppShareData.arrSelectedService = self.arrSelectedService
                    self.tblBookingList.deleteRows(at: [indexPath], with: .left)
                    self.adjustTableBookingHeight()
                    
                }else{
                    self.tblBookingList.reloadSections([indexPath.section], with: .none)
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblBookingList.reloadSections([indexPath.section], with: .none)
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func callWebserviceFor_deleteAllbooking(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let parameters : Dictionary = [
            "artistId" : objArtistDetails._id,
            "userId" : strUserId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteAllBookService, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.arrSelectedService.removeAll()
                    self.objArtistDetails.isOutCallSelected = false
                    objAppShareData.isFromEditBooking = false
                    self.gotoBookingIndividualVC()
                    objAppShareData.stopTimerForHoldBookings()
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceFor_book(dict: [String : Any], objBookingServices : BookingServices, isFromAddMoreButton : Bool){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.bookArtist, params: dict  , success: { response in

            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    objBookingServices.isBooked = true
                    objBookingServices.bookingId = response["bookingId"] as? Int ?? 0
                    objBookingServices.mergedBookingDate = self.getMergedBookingDateAndTime(objBookingServices: objBookingServices)
                    objAppShareData.startTimerForHoldBookings()
                    if isFromAddMoreButton{
                        self.gotoBookingIndividualVCFromAddMoreButton()
                    }else{
                        self.gotoBookingListVC()
                    }
                }else{
                    
                    let msg = response["message"] as? String ?? ""
                    if msg == "Service already added"{
                        
                        objBookingServices.isBooked = true
                        objBookingServices.bookingId = response["bookingId"] as? Int ?? 0
                        objBookingServices.mergedBookingDate = self.getMergedBookingDateAndTime(objBookingServices: objBookingServices)
                        objAppShareData.startTimerForHoldBookings()
                        if isFromAddMoreButton{
                            self.gotoBookingIndividualVCFromAddMoreButton()
                        }else{
                            self.gotoBookingListVC()
                        }
                    }else{
                        //Time Slot not available please select another time
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
           
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGet_ArtistTimeSlot(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.artistTimeSlot, params: dict  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                 
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        
        self.arrToShow.removeAll()
        if let arr = response["timeSlots"] as? [String]{
            for strTimeSlot in arr{
                let objTimeSlot = TimeSlot.init(isSelected: false, strTimeSlot: strTimeSlot)
                self.arrToShow.append(objTimeSlot!);
            }
        }
        
        self.collectionVw.reloadData()
        if self.arrToShow.count == 0 {
            self.lblNoSlotFound.isHidden = false
        }else{
            self.lblNoSlotFound.isHidden = true
        }
    }
    
    func clearFirstServiceSelectedTimeDate(){
        
        if self.arrSelectedService.count > 0{
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            
            if !objBookingServices.isBooked{
                
                objBookingServices.bookingDate = ""
                objBookingServices.bookingTime = ""
                selectedIndex = -1
                self.collectionVw.reloadData()
                self.tblBookingList.reloadData()
                
            }else{
           
            }
        }
    }
}

extension BookingCalendarVC {
    
    func getFormattedBookingDateFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "EE, d LLLL yyyy" //"EE, d LLLL yyyy hh:mm a"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    func getFormattedBookingDateForServerFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    
    func createDictForBookingSlot(objBookingServices : BookingServices, isFromAddMoreButton : Bool){
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        objBookingServices.endTime = getEndTimeForStartTime(strStartTime: objBookingServices.bookingTime)
        objBookingServices.startTime = objBookingServices.bookingTime
        
        var str_bookingId = ""
        var str_type = ""
        
        if objAppShareData.isFromEditBooking  && !isServiceAlreadyAdded {
            
            str_type = ""
            str_bookingId = ""
          
        }else if objAppShareData.isFromEditBooking {
            
            str_type = "edit"
            if self.arrSelectedService.count > 0{
                let objBookingServices : BookingServices = self.arrSelectedService[0]
                str_bookingId = "\(objBookingServices.bookingId)"
            }
            
            if str_bookingId == "0"{
                
                str_bookingId = ""
                str_type = ""
            }
        }
        
        
        let parameters : Dictionary = [
            
            "artistId" : objArtistDetails._id,
            "staff" : objBookingServices.staffId,//objArtistDetails._id,
            "userId" : strUserId,
            
            "serviceId" : objBookingServices.serviceId,
            "subServiceId" : objBookingServices.subServiceId,
            "artistServiceId" : objBookingServices.subSubServiceId,
            
            "serviceType" : objBookingServices.serviceType,
            "bookingDate" : objBookingServices.bookingDateForServer,
            "startTime" : objBookingServices.startTime,
            "endTime" : objBookingServices.endTime,
            "bookingId" : str_bookingId,
            "type" : str_type,
            "price" : objBookingServices.price,
            
            ] as [String : Any]
        
        self.callWebserviceFor_book(dict: parameters, objBookingServices: objBookingServices, isFromAddMoreButton: isFromAddMoreButton)
    }
    
    func getlatestSlotDataFor(dateSelected : Date){
        
        var str_latitude = ""
        var str_longitude = ""
        if objArtistDetails.isOutCallSelected{
            str_latitude =  objLocationManager.strlatitude ?? ""
            str_longitude = objLocationManager.strlongitude ?? ""
        }else{
            str_latitude =  objArtistDetails.latitude
            str_longitude = objArtistDetails.longitude
        }
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: dateSelected)
        
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strCurrentDate) //"0" = Mon"
        let strSelectedTime = self.getTimeFromDate(strDate: strCurrentDate) //"hh:mm a"
        let strSelectedDate = self.getFormattedDateFromDate(strDate: strCurrentDate) //"yyyy-MM-dd"
        
        let totalMin = getTotalServiceTime()
        let strTotalServiceTime = "00:\(totalMin)"
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let strStaffId  = objBookingServices.staffId == 0 ? "0" : "\(objBookingServices.staffId)"
        
        var str_bookingId = ""
        var str_type = ""
        var bookStaffId = ""
        
        if objAppShareData.isFromEditBooking  && !isServiceAlreadyAdded {
           
            str_type = ""
            str_bookingId = ""
            bookStaffId = ""
            
        }else if objAppShareData.isFromEditBooking {
            
            str_type = "edit"
            
            if self.arrSelectedService.count > 0{
                let objBookingServices : BookingServices = self.arrSelectedService[0]
                str_bookingId = "\(objBookingServices.bookingId)"
            }
            if str_bookingId == "0"{
                str_bookingId = ""
                str_type = ""
            }
            bookStaffId = objBookingServices.bookStaffId == 0 ? "" : "\(objBookingServices.bookStaffId)"
        }
        
    
        let parameters : Dictionary = [
            
            "artistId" : objArtistDetails._id,
            "day" : strSelectedDay,
            "date" : strSelectedDate,
            "currentTime" : strSelectedTime,
            "serviceTime" : strTotalServiceTime,
            "userId" : strUserId,
            "latitude" : str_latitude,
            "longitude" : str_longitude,
            "businessType" : objArtistDetails.businessType,
            "bookingTime" : "",
            "bookingDate" : "",
            "bookingCount": "",
            
            "staffId" : strStaffId ,  //>> artistId
            
            "type" : str_type, //
            "bookingId" : str_bookingId, //
            "bookStaffId": bookStaffId, //old staff id id edit
            
            ] as [String : Any]
        

        
        self.callWebserviceForGet_ArtistTimeSlot(dict: parameters)
    }
    
    func getTotalServiceTime()-> Int{
        
        var totalMin = 0
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            if objBookingServices.isOutcall{
                
                totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.outCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
            }else{
                
                totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.inCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
                
            }
        }
        return totalMin
    }
    
    func getEndTimeForStartTime(strStartTime:String)-> String{
        
        var strEndTime : String = ""
        let totalMin = getTotalServiceTime()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = dateFormatter.date(from: strStartTime){
             // Start: Optional(2000-01-01 19:00:00 +0000)
            let calendar = Calendar.current
            if let date1 = calendar.date(byAdding: .minute, value: totalMin, to: date){
                
                strEndTime = dateFormatter.string(from: date1)
               
            }
        }
        return strEndTime
    }
    
    func getTotalServiceTime(strPreprationTime:String, strComplitionTime:String)-> Int{
        
        let preprationTime :Int = getTimeInMinutFrom(strTime: strPreprationTime)
        let complitionTime :Int = getTimeInMinutFrom(strTime: strComplitionTime)
        let totalMin = preprationTime + complitionTime
        return totalMin
    }
    
    func getTimeInMinutFrom(strTime:String)-> Int{
        
        var TotalMin = 0
        var arrTime = strTime.components(separatedBy: ":")
        if arrTime.count >= 2{
            let strHours = arrTime[0]
            let strMin = arrTime[1]
            TotalMin =  Int(strHours)! * 60 + Int(strMin)!
        }
        return TotalMin
    }
    
    func getFormattedDateFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: todayDate)
      
        return strDate
    }
    
    func getTimeFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "hh:mm a"
        let strTime = formatter.string(from: todayDate)
       
        return strTime
    }
    
    func getDayFromSelectedDate(strDate:String)-> String{
        
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        switch weekDay {
        case 1:
            return "6"//"Sun"
        case 2:
            return "0"//Mon"
        case 3:
            return "1"//Tue"
        case 4:
            return "2"//"Wed"
        case 5:
            return "3"//"Thu"
        case 6:
            return "4"//"Fri"
        case 7:
            return "5"//"Sat"
        default:
 
            //"Error fetching days"
            return "Day"
        }
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        
        // returns an integer from 1 - 7, with 1 being Sunday and 7 being Saturday
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
}

extension BookingCalendarVC {
    
    func showAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to permanently remove all selected services?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.callWebserviceFor_deleteAllbooking()
        }
        
        let action2 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func showBookingSessionAlertMessage(){
        
        objWebserviceManager.StopIndicator()
       
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }        
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK:- navigation
extension BookingCalendarVC {
    
    func gotoBookingIndividualVCFromAddMoreButton(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: BookingIndividualVC.self) {
                let objVC : BookingIndividualVC = controller as! BookingIndividualVC
                objVC.isFromAddMoreService = true
                objAppShareData.isFromEditBooking = false
                self.navigationController!.popToViewController(objVC, animated: true)
                break
            }
        }
    }
    
    func gotoBookingIndividualVC(){
        
        var isSearchBoardVCFoundInNavigation = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SearchBoardVC.self) {
                isSearchBoardVCFoundInNavigation = true
                objAppShareData.isFromEditBooking = false
                objArtistDetails.isOutCallSelected = false
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        if !isSearchBoardVCFoundInNavigation{
           objAppShareData.isFromEditBooking = false
           objArtistDetails.isOutCallSelected = false
           appDelegate.gotoTabBar(withAnitmation: false)
        }
        
    }
    
    func getMergedBookingDateAndTime(objBookingServices : BookingServices ) -> Date? {
        
        var dateMerged : Date?
        
        let strDate : String = objBookingServices.bookingDateForServer //2018-03-14
        let strTime : String = objBookingServices.bookingTime //10:00 AM
        
        //strDate
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        
        if let date = formatter.date(from: strDate){
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.dateFormat = "hh:mm a"
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            
            if let timeInDateFormate = dateFormatter.date(from: strTime){
                // Start: Optional(2000-01-01 19:00:00 +0000)
                
                let dateFinal = self.combineDateWithTime(date: date, time: timeInDateFormate)
                if let dateFinal = dateFinal{
                    dateMerged = dateFinal
                }
            }
        }
        return dateMerged
    }
    
    
    func combineDateWithTime(date: Date, time: Date) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = dateComponents.day!
        mergedComponments.hour = timeComponents.hour!
        mergedComponments.minute = timeComponents.minute!
        mergedComponments.second = timeComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    func getTotalPriceOfAllServices() -> Double{
        
        var total : Double = 0
        for objBookingServices in self.arrSelectedService {
            total = total + objBookingServices.price
        }
        return total
    
    }
    
    func addBoolForServiceEditFeature(objBookingServices : BookingServices){
        /*
        for objSubSubService in  objBookingServices.objSubService.arrSubSubService{
            
            if objSubSubService.serviceId == objBookingServices.serviceId  && objSubSubService.subServiceId == objBookingServices.subServiceId &&  objSubSubService.subSubServiceId == objBookingServices.subSubServiceId{
                
                objSubSubService.isSelected = false
                
            }
        }
        */
    }
    
}

