//
//  BookingCompanyVC.swift
//  MualabCustomer
//
//  Created by mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit
import HCSStarRatingView

class BookingCompanyVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var objBookingServices = BookingServices()
    
    //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    var arrStaffInfo: [StaffInfo] = [StaffInfo]()
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
}

//MARK: - view Life cycle
extension BookingCompanyVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateArtistInfo()
        //self.arrStaffInfo = objArtistDetails.arrStaffInfo
        self.tblBookingList.reloadData()
    }
}

//MARK: - userDefine Method extension
extension BookingCompanyVC{
    
    func updateArtistInfo(){
        
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistDetails.userName
        self.viewRating.value = CGFloat(objArtistDetails.ratingCount)
    }
    
    func configureView(){
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.lblNoDataFound.isHidden = true
        self.tblBookingList.addSubview(self.refreshControl)
    }
    
    @objc func showBookingSessionAlertMessage(){
        
        objWebserviceManager.StopIndicator()
        
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func gotoProfileVC (){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

// MARK: - table view Delegate and Datasource methods extension
extension BookingCompanyVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrStaffInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "CellBookingVC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CellBookingVC
        
        let objStaffInfo = self.arrStaffInfo[indexPath.row]
        
        cell.lblUserName.text = objStaffInfo.staffName
        cell.lblServiceCategory.text = objStaffInfo.job
        
        if objStaffInfo.staffImage != "" {
            if let url = URL(string: objStaffInfo.staffImage){
                //cell.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                cell.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        
        let doubleStr = String(format: "%.2f", ceil(objStaffInfo.selectedPrice * 100)/100)
        cell.lblPrice.text = "£" + doubleStr
        
        var arrTime = objStaffInfo.selectedCompletionTime.components(separatedBy: ":")
        if arrTime.count >= 2{
            
            var strTimeToShow = ""
            
            let strHours = arrTime[0]
            let intHours = Int(strHours)
            if intHours != 0 {
                strTimeToShow = arrTime[0] + " hrs " + arrTime[1] + " min"
            }else{
                strTimeToShow = arrTime[1] + " min"
            }
            cell.lblTime.text = strTimeToShow
        }else{
            cell.lblTime.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
        let objStaffInfo = self.arrStaffInfo[indexPath.row]
       
        objBookingServices.price = objStaffInfo.selectedPrice
        objBookingServices.completionTime = objStaffInfo.selectedCompletionTime
        objBookingServices.staffId = objStaffInfo.staffId
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingCalendarVC") as? BookingCalendarVC{
            
            objVC.objArtistDetails = objArtistDetails
            objVC.objBookingServices = objBookingServices
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

//MARK: - webservices extension
fileprivate extension BookingCompanyVC{
    
    
//    func call_for_webservice_BookingList() -> Void {
//
//        if  !self.refreshControl.isRefreshing{
//            objWebserviceManager.StartIndicator()
//        }
//        objWebserviceManager.requestGet(strURL: "", params: nil, success: { response in
//
//            self.refreshControl.endRefreshing()
//            objWebserviceManager.StopIndicator()
//            let keyExists = response["responseCode"] != nil
//            if  keyExists {
//
//                objWebserviceManager.StopIndicator()
//                showAlertVC(title: kAlertTitle, message: "Session expired, please Login again", controller: self)
//            }else{
//
//                let message = response["message"] as? String ?? ""
//                let status = response["status"] as? String ?? ""
//                if (status == "success"){
//
//                }else{
//
//                    if message == "No Record found"{
//
//                    }else{
//
//                        showAlertVC(title:kAlertTitle,message:message,controller:self)
//                    }
//                }
//            }
//        })
//        { (Error) in
//
//            self.refreshControl.endRefreshing()
//            objWebserviceManager.StopIndicator()
//            showAlertVC(title: kAlertTitle, message: "something went wrong", controller: self)
//        }}
    
}

//MARK: - Button extension
fileprivate extension BookingCompanyVC {
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.gotoProfileVC()
       // objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnShowWorkingHoursAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as? BusinessHoursVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
        }
    }
}

