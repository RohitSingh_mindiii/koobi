//
//  BookingIndividualVC.swift
//  MualabCustomer
//
//  Created by mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit
import HCSStarRatingView

class BookingIndividualVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var objArtistDetails = ArtistDetails(dict: ["":""])
    var arrToShow : [Service] = [Service]()
    var isFromAddMoreService : Bool = false

    @IBOutlet weak var vwOutCallOuter: UIView!    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
   
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    var kOpenSectionTag: Int = 0
    fileprivate var arrCategories = [String]()
    fileprivate var arrSubCategories=[String]()
    fileprivate var expandedSectionHeaderNumber: Int = -1
    
    //OutCall
    @IBOutlet weak var imgVwOutCall: UIImageView!
    @IBOutlet weak var lblOutCall: UILabel!
    
    var isOutCallSelected:Bool = false
    let imgCircleSelected : UIImage = UIImage.init(named:"active_circle_ico")!
    let imgCircleUnselected : UIImage = UIImage.init(named:"inactive_circle_ico")!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        
        self.lblNoDataFound.isHidden = true
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        //objLocationManager.getCurrentLocation()
        
        let nib = UINib(nibName: "CategoriesHeader", bundle: nil)
        self.tblBookingList.register(nib, forHeaderFooterViewReuseIdentifier: "CategoriesHeader")
        self.tblBookingList.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblNoDataFound.isHidden = true
        self.updateArtistInfo()
        setOutCallUI()
        
        if isFromAddMoreService{
            self.vwOutCallOuter.isHidden = true
        }else{
            self.vwOutCallOuter.isHidden = false
        }
    
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        
        if objArtistDetails.arrServices.count == 0{
           callWebserviceForGetBusinessProfile()
        }
    }
    
    func updateArtistInfo(){
        
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistDetails.userName
        self.viewRating.value = CGFloat(objArtistDetails.ratingCount)
    }
}

//MARK: - Custom methods
fileprivate extension BookingIndividualVC {
    
    func setOutCallUI(){
        if objArtistDetails.isOutCallSelected{
            imgVwOutCall.image = imgCircleSelected
        }else{
            imgVwOutCall.image = imgCircleUnselected
        }
    }
    func gotoProfileVC (){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

//MARK: - button extension
fileprivate extension BookingIndividualVC {
    
    @IBAction func btnShowWorkingHoursAction(_ sender: UIButton) {
        self.view.endEditing(true)
      
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as? BusinessHoursVC {
            objVC.objArtistDetails = objArtistDetails
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.gotoProfileVC()
        //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if objAppShareData.arrSelectedService.count >  0{
            showAlertMessage()
        }else{
          navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnOutCallAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objArtistDetails.isOutCallSelected = !objArtistDetails.isOutCallSelected
        setOutCallUI()
        self.filterDataFor_Incall_OutCall()
    }
}


// MARK: - Expand / Collapse Methods
fileprivate extension BookingIndividualVC{
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! CategoriesHeader
        let section    = headerView.tag
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            kOpenSectionTag = section
            tableViewExpandSection(section, headerView: headerView)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                kOpenSectionTag = section
                tableViewCollapeSection(section, headerView: headerView)
            } else {
                let previousHeader = self.tblBookingList.headerView(forSection: kOpenSectionTag) as? CategoriesHeader
                tableViewCollapeSection(kOpenSectionTag,  headerView: previousHeader!)
                tableViewExpandSection(section, headerView: headerView)
                kOpenSectionTag = section
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, headerView: CategoriesHeader ) {
        
        let imageView : UIImageView = headerView.imgDropDown
        let objService : Service = self.arrToShow[section]
        let sectionData = objService.arrSubServices
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            headerView.lblName.textColor = UIColor.black
            headerView.lblBotumeLayer.backgroundColor = UIColor.clear
            
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, headerView: CategoriesHeader) {
       
        let objService : Service = self.arrToShow[section]
        let sectionData = objService.arrSubServices
        
        let imageView : UIImageView = headerView.imgDropDown
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            
            headerView.lblName.textColor = UIColor.theameColors.pinkColor
            headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
}

//MARK: - UITableview delegate
extension BookingIndividualVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.arrToShow.count > 0 {
            tableView.backgroundView = nil
            return self.arrToShow.count
        }
        return 0
    }
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let objService : Service = self.arrToShow[section]
            let arrayOfItems = objService.arrSubServices
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        // Dequeue with the reuse identifier
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CategoriesHeader") as! CategoriesHeader
        let objService : Service = self.arrToShow[section]
        header.lblName.text = objService.serviceName
        
        header.lblName.textColor = UIColor.black
        header.lblBotumeLayer.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.4, animations: {
            header.imgDropDown.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
        })
        
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        
        return header
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTableCell", for: indexPath) as? SubCategoriesTableCell{
            
            cell.indexPath = indexPath
            let objService : Service = self.arrToShow[indexPath.section]
            let objSubService: SubService = objService.arrSubServices[indexPath.row]
            cell.lblName.text =  objSubService.subServiceName
           
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objService : Service = self.arrToShow[indexPath.section]
        let objSubService: SubService = objService.arrSubServices[indexPath.row]
       
        objSubService.serviceId = objService.serviceId
        objSubService.serviceName = objService.serviceName
         
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC =
            
            sb.instantiateViewController(withIdentifier:"BookingServiceSelectionVC") as? BookingServiceSelectionVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.objSubService = objSubService
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

extension BookingIndividualVC {

    func showAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to permanently remove all selected services?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.callWebserviceFor_deleteAllbooking()
        }
        
        let action2 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func showBookingSessionAlertMessage(){
        
        objWebserviceManager.StopIndicator()
        
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}

//Webservices
fileprivate extension BookingIndividualVC{
    
    func callWebserviceFor_deleteAllbooking(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let parameters : Dictionary = [
            
            "artistId" : objArtistDetails._id,
            "userId" : strUserId,
            
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteAllBookService, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.arrSelectedService.removeAll()
                    self.navigationController?.popViewController(animated: true)
                }else{
                    
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

    func callWebserviceForGetBusinessProfile(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let parameters : Dictionary = [
            "businessType":objArtistDetails.businessType,
            "artistId":objArtistDetails._id,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.artistDetail, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                
                if strStatus == k_success{
                  self.parseResponce(response)
                }else{                    
                 
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func parseResponce(_ response:[String:Any]){
    
        if let dict = response["artistDetail"] as? [String : Any]{
            let obj = ArtistDetails.init(dict: dict)
            obj.businessType = objArtistDetails.businessType
            obj.responce = dict
            objArtistDetails = obj
            if objAppShareData.isSearchingUsingFilter{
                self.objArtistDetails.isOutCallSelected =  objAppShareData.objRefineData.isOutCallSelected
            }
        }
        updateArtistInfo()
        self.filterDataFor_Incall_OutCall()
        setOutCallUI()
    }
      
    func filterDataFor_Incall_OutCall(){
        /*
        objActivity.startActivityIndicator()
        
        //"serviceType": 1 = inCall, 2 = outCall, 3 = both
        if objArtistDetails.serviceType == "1"{
            
            if objArtistDetails.isOutCallSelected {
                self.arrToShow.removeAll()
            }else{
               self.arrToShow = objArtistDetails.arrServices
            }
            
        }else if objArtistDetails.serviceType == "2"{
            
            if objArtistDetails.isOutCallSelected {
               self.arrToShow = objArtistDetails.arrServices
            }else{
               self.arrToShow.removeAll()
            }
            
        }else if objArtistDetails.serviceType == "3" {
            
            if objArtistDetails.arrServices.count > 0{
               
                let arrTempService : [Service] = objArtistDetails.arrServices
                var arrFilterService : [Service] = [Service]() //
               
                for objService in arrTempService {
                    
                    let objTempService = Service.init(dict: [:])
                    objTempService.serviceId = objService.serviceId
                    objTempService.serviceName = objService.serviceName
                    
                    for objSubService in objService.arrSubServices{
                       
                        let objTempSubService = SubService.init(dict: [:])
                        objTempSubService.serviceId = objSubService.serviceId
                        objTempSubService.subServiceId = objSubService.subServiceId
                        objTempSubService.subServiceName = objSubService.subServiceName
                        
                        for objSubSubService  in objSubService.arrSubSubService{
                           
                            let objTempSubSubService = SubSubService.init(dict: [:])
                            
                            if objArtistDetails.isOutCallSelected {
                                
                                if objSubSubService.outCallPrice > 0{
                                    
                                    objTempSubSubService.subSubServiceId = objSubSubService.subSubServiceId
                                    objTempSubSubService.inCallPrice = objSubSubService.inCallPrice
                                    objTempSubSubService.outCallPrice = objSubSubService.outCallPrice
                                    objTempSubSubService.subSubServiceName = objSubSubService.subSubServiceName
                                    objTempSubSubService.completionTime = objSubSubService.completionTime
                                   
                                    objTempSubService.arrSubSubService.append(objTempSubSubService)                                   
                                  
                                }
                            }else{
                                
                                if objSubSubService.inCallPrice > 0{
                                    
                                    objTempSubSubService.subSubServiceId = objSubSubService.subSubServiceId
                                    objTempSubSubService.inCallPrice = objSubSubService.inCallPrice
                                    objTempSubSubService.outCallPrice = objSubSubService.outCallPrice
                                    objTempSubSubService.subSubServiceName = objSubSubService.subSubServiceName
                                    objTempSubSubService.completionTime = objSubSubService.completionTime
                                    
                                    objTempSubService.arrSubSubService.append(objTempSubSubService)
                                    
                                }
                            }
                        }
                        
                        if objTempSubService.arrSubSubService.count > 0{
                            objTempService.arrSubServices.append(objTempSubService)
                        }
                    }
                    
                    if objTempService.arrSubServices.count > 0{
                        arrFilterService.append(objTempService)
                        
                    }
                    
                }
                self.arrToShow = arrFilterService
            }
        }        
        objWebserviceManager.StopIndicator()
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
                
        if self.arrToShow.count == 0 {
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        */
    }
}

