//
//  BookingListVC.swift
//  MualabCustomer
//
//  Created by Mac on 20/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation


class BookingListVC: UIViewController, SelectedServicesCellDelegate {
    
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var arrSelectedService = [BookingServices]()
    var strLatitude : String =  ""
    var strLongitude : String = ""
    var strSelectedAddress : String = ""
    
    var strSelectedPaymentType : String = ""
    @IBOutlet var radioPaymentButton : DLRadioButton!
    @IBOutlet weak var vwPopUp: UIView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnEditWidth: NSLayoutConstraint!
    @IBOutlet weak var lblBusinessNameName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblTotalPrice: UILabel!
   
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
       
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        
        self.configureView()
        setDateOnUI()
        
        strSelectedPaymentType = ""
        self.radioPaymentButton.isSelected = true
        self.vwPopUp.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tblBookingList.reloadData()
        self.adjustTableBookingHeight()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        adjustTableBookingHeight()
        addBoolForServiceEditFeature()        
    }
    
    func configureView(){
        
        objArtistDetails.userBookingAddress = objLocationManager.strAddress
        objArtistDetails.userBookingAddressLatitude = objLocationManager.strlatitude ?? ""
        objArtistDetails.userBookingAddressLongitude = objLocationManager.strlongitude ?? ""
        
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - custommethods
extension BookingListVC{
    
    func setDateOnUI(){
        
        self.setAddressForService()
        
        if objArtistDetails.isOutCallSelected{
            self.btnEditWidth.constant = 55
            self.checkArtistAvailabilityAtSelectedAdd()
        }else{
            self.btnEditWidth.constant = 0
        }
    
        self.lblBusinessNameName.text = objArtistDetails.businessName
        self.lblName.text = objArtistDetails.userName
        self.lblServiceName.text = ""
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.setbookingServiceStartDateAndTime()
        let total : Double = self.getTotalPriceOfAllServices()
        let doubleStr = String(format: "%.2f", (total*100)/100)
        self.lblTotalPrice.text = "£\(doubleStr)"
    }
    
    func setAddressForService(){
        
        if objArtistDetails.isOutCallSelected{
            
            if objArtistDetails.userBookingSelectedAddress  != "" {
                self.lblAddress.text = objArtistDetails.userBookingSelectedAddress
            }else{
                
                if objArtistDetails.userBookingAddress != ""{
                    self.lblAddress.text = objArtistDetails.userBookingAddress
                }else{
                    
                    objArtistDetails.isAddressInArtistRange = true
                    if objArtistDetails.address != ""{
                        self.lblAddress.text = objArtistDetails.address
                    }else{
                        self.lblAddress.text = "NA"
                    }
                }
            }
        }else{
            if objArtistDetails.address != ""{
                self.lblAddress.text = objArtistDetails.address
            }else{
                self.lblAddress.text = "NA"
            }
        }
        
        let add = self.lblAddress.text!
        if add == objArtistDetails.address{
            
            strLatitude = objArtistDetails.latitude
            strLongitude = objArtistDetails.longitude
            strSelectedAddress = objArtistDetails.address
            
        }else if add == objArtistDetails.userBookingAddress {
            
            strLatitude = objArtistDetails.userBookingAddressLatitude
            strLongitude = objArtistDetails.userBookingAddressLongitude
            strSelectedAddress = objArtistDetails.userBookingAddress
            
        }else if add == objArtistDetails.userBookingSelectedAddress {
            
            strLatitude = objArtistDetails.userBookingSelectedAddressLatitude
            strLongitude = objArtistDetails.userBookingSelectedAddressLongitude
            strSelectedAddress = objArtistDetails.userBookingSelectedAddress
        }else{
            strLatitude = ""
            strLongitude = ""
            strSelectedAddress = ""
        }
    }
    
    func setbookingServiceStartDateAndTime(){
        
        if self.arrSelectedService.count > 0{
            
            let temp = self.arrSelectedService.sorted(by: { $0.mergedBookingDate!.compare($1.mergedBookingDate!) == .orderedAscending })
                
            if temp.count > 0{
                
                let objBookingServices : BookingServices = temp[0]
                self.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: objBookingServices.bookingDateForServer)
                self.lblTime.text = objBookingServices.bookingTime
                self.arrSelectedService = temp
                self.tblBookingList.reloadData()
                
            }else{
                
                let objBookingServices : BookingServices = self.arrSelectedService.last!
                self.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: objBookingServices.bookingDateForServer)
                self.lblTime.text = objBookingServices.bookingTime
            }
        }
    }
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
    
    func combineDateWithTime(date: Date, time: Date) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = dateComponents.day!
        mergedComponments.hour = timeComponents.hour!
        mergedComponments.minute = timeComponents.minute!
        mergedComponments.second = timeComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    func getTotalPriceOfAllServices() -> Double{
       
        var total : Double = 0
        for objBookingServices in self.arrSelectedService {
            total = total + objBookingServices.price
        }
        return total
    }
    @objc func showBookingSessionAlertMessage(){
        
        objWebserviceManager.StopIndicator()
        
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }

}

//MARK: - btnActions
extension BookingListVC{
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        objAppShareData.isFromConfirmBooking = true
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
        self.gotoProfileVC()
       //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }

    @IBAction func btnConfirmBookingAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if objArtistDetails.isOutCallSelected{
            if !objArtistDetails.isAddressInArtistRange {
                objAppShareData.showAlert(withMessage: "Selected artist services is not available at this location", type: alertType.bannerDark, on: self)
                return
            }
        }
        
        if objArtistDetails.bankStatus == 0 {
            strSelectedPaymentType = "Cash"
        }
        
        if strSelectedPaymentType == "" {
            self.vwPopUp.isHidden = false
            return
        }
        
        if self.arrSelectedService.count > 0{
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            self.createDictForConfirmBooking(objBookingServices: objBookingServices)
        }

    }
    
    @IBAction func btnEditTimeAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnEditLocationAction(_ sender: Any) {
        self.view.endEditing(true)
        self.gotoGMSAutocompleteVC()
    }
    
    @IBAction func btnEditServiceAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnApplyVoucherCodeAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
}

// MARK:- TableView Delegates
extension BookingListVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSelectedService.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if let cell : SelectedServicesCell = tableView.dequeueReusableCell(withIdentifier: "SelectedServicesCell")! as? SelectedServicesCell{
            
            cell.delegate = self
            cell.indexPath = indexPath
            let objBookingServices : BookingServices = self.arrSelectedService[indexPath.row]
            
            cell.lblSubServiceName.text = objBookingServices.subServiceName
            cell.lblSubSubServiceName.text = objBookingServices.subSubServiceName
            
            let date = self.getFormattedBookingDateToShowFrom(strDate: objBookingServices.bookingDateForServer)
            let time = objBookingServices.bookingTime
            
            cell.lblServiceDateTime.text = "\(date), \(time)"
            
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    

    func btnEditTapped(at index:IndexPath){
      
        let objBookingServices : BookingServices = self.arrSelectedService[index.row]
        objBookingServices.bookStaffId =  objBookingServices.staffId
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: BookingServiceSelectionVC.self) {
                let objVC : BookingServiceSelectionVC = controller as! BookingServiceSelectionVC
                
                objVC.objBookingServices_FromEdit = objBookingServices
                
                objAppShareData.isFromEditBooking = true
                self.navigationController!.popToViewController(objVC, animated: true)
                break
            }
        }
    }
    
    func addBoolForServiceEditFeature(){
        /*
        for objBookingServices in self.arrSelectedService{
            
            for objSubSubService in  objBookingServices.objSubService.arrSubSubService{
                
                if objSubSubService.serviceId == objBookingServices.serviceId  && objSubSubService.subServiceId == objBookingServices.subServiceId &&  objSubSubService.subSubServiceId == objBookingServices.subSubServiceId{
                    
                    objSubSubService.isSelected = true
                }
            }
        }
        */
    }
}

//Notification
extension BookingListVC{
    
    func adjustTableBookingHeight(){
        UIView.animate(withDuration: 0.4, animations: {
            DispatchQueue.main.async {
                self.heightTableBookingConstraint.constant = self.tblBookingList.contentSize.height
                self.view.layoutIfNeeded()
            }
        })
    }
}


// MARK:- Custom Methods
extension BookingListVC : GMSAutocompleteViewControllerDelegate{
    
    func gotoProfileVC (){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoGMSAutocompleteVC(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let add = place.formattedAddress{
            self.lblAddress.text = add
            objArtistDetails.userBookingAddress = add
            objArtistDetails.userBookingSelectedAddress = objArtistDetails.userBookingAddress
        }
        
        if place.coordinate.latitude != 0 && place.coordinate.longitude != 0 {
            objArtistDetails.userBookingAddressLatitude = "\(place.coordinate.latitude)"
            objArtistDetails.userBookingAddressLongitude = "\(place.coordinate.longitude)"
            
            objArtistDetails.userBookingSelectedAddressLatitude = objArtistDetails.userBookingAddressLatitude
            objArtistDetails.userBookingSelectedAddressLongitude = objArtistDetails.userBookingAddressLongitude
        }
        
    
        self.setAddressForService()
        dismiss(animated: true, completion: nil)
        self.checkArtistAvailabilityAtSelectedAdd()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
// MARK:- Webservice Call BookingListVC
extension BookingListVC {
    
    func getDistancBetweenTwoPoint(strlat1 : String, strLong1 : String, strlat2 : String, strLong2 : String) -> Double{
        
        
        let lat1 = Double(strlat1) ?? 0
        let long1 = Double(strLong1) ?? 0
        
        let lat2 = Double(strlat2) ?? 0
        let long2 = Double(strLong2) ?? 0
        
        let coordinate1 = CLLocation(latitude: lat1, longitude: long1)
        let coordinate2 = CLLocation(latitude: lat2, longitude: long2)
        
        let distanceInMeters = coordinate1.distance(from: coordinate2) // result is in meters
        //1 meter =  0.000621371 mile
        let distanceInMiles = 0.000621371 * distanceInMeters
        
        return distanceInMiles
    }
    
    func checkArtistAvailabilityAtSelectedAdd(){
        
        let artistRadius = objArtistDetails.radius
        let distanceInMiles = getDistancBetweenTwoPoint(strlat1: objArtistDetails.latitude, strLong1: objArtistDetails.longitude, strlat2: strLatitude, strLong2: strLongitude)
        
        if distanceInMiles > artistRadius {
            objArtistDetails.isAddressInArtistRange = false
            objAppShareData.showAlert(withMessage: "Selected artist services is not available at this location", type: alertType.bannerDark, on: self)
            
            objArtistDetails.userBookingSelectedAddress = ""
            objArtistDetails.userBookingSelectedAddressLatitude = ""
            objArtistDetails.userBookingSelectedAddressLongitude = ""
            
        }else{
            objArtistDetails.isAddressInArtistRange = true
        }
    }
    
    func createDictForConfirmBooking(objBookingServices : BookingServices){
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let total : Double = self.getTotalPriceOfAllServices()
        let strTotal = "\(total)"
        
        var paymentType : String = ""
        
        // 1 = paypal, 2 =  online, 3 = Cash
        if strSelectedPaymentType == "Cash" {
            paymentType = "3"
        }else{
            paymentType = "2"
        }
        
        let parameters : Dictionary = [
            
            "artistId" : objArtistDetails._id,
            "userId" : strUserId,
            "totalPrice" : strTotal,
            "discountPrice" : "",
            "bookingdate" : objBookingServices.bookingDateForServer,
            "bookingTime" : objBookingServices.bookingTime,
            "voucherId" : "",
            "location" : strSelectedAddress,
            "paymentType" : paymentType,
    
            ] as [String : Any]
        
        self.callWebserviceFor_book(dict: parameters)
    }
    
    func callWebserviceFor_book(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.confirmBooking, params: dict  , success: { response in

            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                       
                        _ = SweetAlert().showAlert("Congratulation!", subTitle: "Your booking request has been successfully sent to artist.", style: AlertStyle.success, buttonTitle: "OK", buttonColor: UIColor.theameColors.pinkColor, action: { (isClicked) in
                            
                            self.gotoSearchBoardVC()
                        })
                        
                    }
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func gotoSearchBoardVC(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SearchBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}

// MARK:- PopUp
extension BookingListVC {
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        
        if (radioButton.isMultipleSelectionEnabled) {
           
            for button in radioButton.selectedButtons() {
                //String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
            
        } else {
            
            let strTitle = String(format: "%@", radioButton.selected()!.titleLabel!.text!)
            
            if strTitle == "Cash"{
                self.strSelectedPaymentType = "Cash"
            }else{
                self.strSelectedPaymentType = "Online"
            }
        }
    }
    
    @IBAction func btnPopUpCancelAction(_ sender: UIButton) {
        
            self.vwPopUp.isHidden = true
            self.strSelectedPaymentType = ""
    }
    
    @IBAction func btnPopUpDoneAction(_ sender: UIButton) {
        
            self.vwPopUp.isHidden = true
            let strTitle = String(format: "%@", radioPaymentButton.selected()!.titleLabel!.text!)
            if strTitle == "Cash"{
                self.strSelectedPaymentType = "Cash"
            }else{
                self.strSelectedPaymentType = "Online"
            }
        
        if self.arrSelectedService.count > 0{
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            self.createDictForConfirmBooking(objBookingServices: objBookingServices)
        }
    }
    
}






