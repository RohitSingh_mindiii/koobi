//
//  BookingServiceSelectionVC.swift
//  MualabCustomer
//
//  Created by Mac on 08/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit
import HCSStarRatingView

class BookingServiceSelectionVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    var arrNames: [String] = []
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var objSubService: SubService = SubService(dict: ["":""])
    var arrSubSubService : [SubSubService] = [SubSubService]()
    
    var objBookingServices_FromEdit = BookingServices()
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblHeaderName: UILabel!
   
    //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
}


//MARK: - view Life cycle
extension BookingServiceSelectionVC{
    
    //MARK: - SystemMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if objAppShareData.isFromEditBooking {
            objSubService = objBookingServices_FromEdit.objSubService
        }
//        self.arrSubSubService = objSubService.arrSubSubService
//        self.tblBookingList.reloadData()
//        self.updateArtistInfo()
    }
    
    func updateArtistInfo(){
        
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistDetails.userName
        self.viewRating.value = CGFloat(objArtistDetails.ratingCount)
    }
}

//MARK: - userDefine Method extension
extension BookingServiceSelectionVC{
    
    func configureView(){
       
        self.lblHeaderName.text = objSubService.subServiceName
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.lblNoDataFound.isHidden = true
       
    }
}

// MARK: - table view Delegate and Datasource methods extension
extension BookingServiceSelectionVC{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSubSubService.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "BookingServiceSelectionCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BookingServiceSelectionCell
        let objSubSubService: SubSubService = self.arrSubSubService[indexPath.section]
        cell.lblName.text = objSubSubService.subSubServiceName
        cell.viewSelectedCellBG.isHidden = true
        cell.viewCellBG.isHidden = false
        
        var doubleStr = ""
        if objArtistDetails.isOutCallSelected{
           doubleStr = String(format: "%.2f", ceil(objSubSubService.outCallPrice*100)/100)
        }else{
           doubleStr = String(format: "%.2f", ceil(objSubSubService.inCallPrice*100)/100)
        }
    
        cell.lblPrice.text = "£" + doubleStr
        
        var arrTime = objSubSubService.completionTime.components(separatedBy: ":")
        if arrTime.count >= 2{
            
            var strTimeToShow = ""
            
            let strHours = arrTime[0]
            let intHours = Int(strHours)
            if intHours != 0 {
                strTimeToShow = arrTime[0] + " hrs " + arrTime[1] + " min"
            }else{
                strTimeToShow = arrTime[1] + " min"
            }
            cell.lblTime.text = strTimeToShow
        }else{
            cell.lblTime.text = ""
        }

        if objAppShareData.isFromEditBooking {
            if objSubSubService.isSelected{
                cell.viewSelectedCellBG.isHidden = false
                cell.viewCellBG.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        self.view.endEditing(true)
        let objSubSubService: SubSubService = self.arrSubSubService[indexPath.section]
        
        objSubSubService.serviceId = objSubService.serviceId
        objSubSubService.serviceName = objSubService.serviceName
        objSubSubService.subServiceId = objSubService.subServiceId
        objSubSubService.subServiceName = objSubService.subServiceName
     
        let objBookingServices = BookingServices()
        
        objBookingServices.objSubService = objSubService
        
        objBookingServices.serviceId = objSubSubService.serviceId
        objBookingServices.serviceName = objSubSubService.serviceName
        objBookingServices.subServiceId = objSubSubService.subServiceId
        objBookingServices.subServiceName = objSubSubService.subServiceName
        objBookingServices.subSubServiceId = objSubSubService.subSubServiceId
        objBookingServices.subSubServiceName = objSubSubService.subSubServiceName
        objBookingServices.completionTime = objSubSubService.completionTime
        
        if objArtistDetails.isOutCallSelected {
            objBookingServices.isOutcall = true
            objBookingServices.serviceType = "2"  //inCall = 1, outCall = 2
            objBookingServices.price = objSubSubService.outCallPrice
        }else{
            objBookingServices.isOutcall = false
            objBookingServices.serviceType = "1"
            objBookingServices.price = objSubSubService.inCallPrice
        }
        
        if objAppShareData.isFromEditBooking {
            objBookingServices.isFromEdit = true
            objBookingServices.bookStaffId = objBookingServices_FromEdit.bookStaffId
        }
        
        var arrTempStaffInfo : [StaffInfo] = []
        
        if objArtistDetails.businessType == "independent" || objArtistDetails.arrStaffInfo.count == 0 {
            //self.gotoBookingCalendarVCWith(objBookingServices: objBookingServices)
        }else{
            
            for objStaffInfo in objArtistDetails.arrStaffInfo {
                   
                    for objStaffService in objStaffInfo.arrStaffService{
                       
                        if objBookingServices.subSubServiceId == objStaffService.subSubServiceId {
                         
                            objStaffInfo.selectedCompletionTime = objStaffService.completionTime
                            
                            if objArtistDetails.isOutCallSelected {
                                 objStaffInfo.selectedPrice = objStaffService.outCallPrice
                            }else{
                                objStaffInfo.selectedPrice = objStaffService.inCallPrice
                            }
                            arrTempStaffInfo.append(objStaffInfo)
                            break
                        }
                    }
                }
        }
        
        //Adding artist to staff list
        var objStaffInfo = StaffInfo(dict: ["":""])
        //objStaffInfo.staffId = objArtistDetails._id
        objStaffInfo.staffImage = objArtistDetails.profileImage
        objStaffInfo.staffName = objArtistDetails.userName
        objStaffInfo.selectedCompletionTime = objSubSubService.completionTime
        
        if objArtistDetails.isOutCallSelected {
            objStaffInfo.selectedPrice = objSubSubService.outCallPrice
        }else{
            objStaffInfo.selectedPrice = objSubSubService.inCallPrice
        }
        arrTempStaffInfo.append(objStaffInfo)
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingCompanyVC") as? BookingCompanyVC {
            
            objVC.objBookingServices = objBookingServices
            objVC.objArtistDetails = objArtistDetails
            objVC.arrStaffInfo = arrTempStaffInfo
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 70
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    
        let objSubSubService: SubSubService = self.arrSubSubService[indexPath.section]
 
        var swipeConfig = UISwipeActionsConfiguration(actions: [])
        swipeConfig.performsFirstActionWithFullSwipe = false
       
        if objAppShareData.isFromEditBooking{
            if objSubSubService.isSelected{
                let deleteAction = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
    
                swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction])
            }
        }
        return swipeConfig
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
}

//MARK: - Filter staff for selected service
fileprivate extension BookingServiceSelectionVC {
    
    
}

//MARK: - webservices extension
fileprivate extension BookingServiceSelectionVC {
    
    func callWebserviceFor_deleteSelectedbookin(objBookingServices : BookingServices, indexPath : IndexPath){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let parameters : Dictionary = [
               "bookingId" : objBookingServices.bookingId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteBookService, params: parameters  , success: { response in

            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.removeSelectedServiceFromArray(indexPath : indexPath)
                }else{
                     self.tblBookingList.reloadSections([indexPath.section], with: .none)
                    
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
             self.tblBookingList.reloadSections([indexPath.section], with: .none)
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func removeSelectedServiceFromArray(indexPath : IndexPath){
       
        let objSubSubService: SubSubService = self.arrSubSubService[indexPath.section]
        objSubSubService.isSelected = false
        self.tblBookingList.reloadSections([indexPath.section], with: .none)
        
        let arrTemp = objAppShareData.arrSelectedService.filter { $0.subSubServiceId != objSubSubService.subSubServiceId }
        objAppShareData.arrSelectedService = arrTemp
        
        if objAppShareData.arrSelectedService.count > 0{
            self.gotoBookingListVC()
        }else{
            self.gotoSearchBoardVC()
        }
    }
    
    func callWebserviceFor_deleteAllbooking(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let parameters : Dictionary = [
            
            "artistId" : objArtistDetails._id,
            "userId" : strUserId,
            
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteAllBookService, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.arrSelectedService.removeAll()
                    objAppShareData.isFromEditBooking = false
                    self.objArtistDetails.isOutCallSelected = false
                    //self.gotoBookingIndividualVC()
                    self.gotoSearchBoardVC()
                }else{
               
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - Button extension

fileprivate extension BookingServiceSelectionVC {
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if objAppShareData.isFromEditBooking {
           
            if objAppShareData.arrSelectedService.count >  0{
                self.showAlertMessage()
            }else{
                self.objArtistDetails.isOutCallSelected = false
                objAppShareData.isFromEditBooking = false
                navigationController?.popViewController(animated: true)
            }
            
        }else{
           navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnShowWorkingHoursAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.gotoBusinessHoursVC()
    }
}

extension BookingServiceSelectionVC {

    func deleteSelectedServiceFrom(indexPath : IndexPath){
        
        let objSubSubService: SubSubService = self.arrSubSubService[indexPath.section]
        
        for objTempBookingServices in objAppShareData.arrSelectedService {
            if objTempBookingServices.subSubServiceId == objSubSubService.subSubServiceId {
                self.callWebserviceFor_deleteSelectedbookin(objBookingServices : objTempBookingServices, indexPath : indexPath )
            }
        }
    }
    
    func showAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to permanently remove all selected services?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.callWebserviceFor_deleteAllbooking()
        }
        
        let action2 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func showBookingSessionAlertMessage(){
        
        objWebserviceManager.StopIndicator()
        
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }

}


extension BookingServiceSelectionVC{
    
    @available(iOS 11.0, *)
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .normal, title: "Remove") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            self.deleteSelectedServiceFrom(indexPath: indexPath)
            completionHandler(true)
        }
        action.image = UIImage(named: "remove_ico1")
        action.backgroundColor = UIColor.init(red: 182.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        return action
    }

    @available(iOS 11.0, *)
    func contextualEditAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
       
        let action = UIContextualAction(style: .normal, title: "Edit") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in

            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
            completionHandler(true)
        }
        action.image = UIImage(named: "edit_icon1")
        action.backgroundColor = UIColor.init(red: 92.0/255.0, green: 131.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        return action
    }
}

//MARK: - Navigation
extension BookingServiceSelectionVC{
    
    func gotoSearchBoardVC(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SearchBoardVC.self) {
                objAppShareData.isFromEditBooking = false
                objArtistDetails.isOutCallSelected = false
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func gotoBookingListVC(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if var arrControllers = self.navigationController?.viewControllers{
            
            if let objVC = sb.instantiateViewController(withIdentifier:"BookingCalendarVC") as? BookingCalendarVC{
            
                objVC.objArtistDetails = objArtistDetails
                objVC.hidesBottomBarWhenPushed = true
                arrControllers.append(objVC)
            }
            
            if let objVC = sb.instantiateViewController(withIdentifier:"BookingListVC") as? BookingListVC {
                objVC.objArtistDetails = objArtistDetails
                objVC.arrSelectedService = objAppShareData.arrSelectedService
                objAppShareData.isFromEditBooking = false
                objVC.hidesBottomBarWhenPushed = true
                arrControllers.append(objVC)
            }
            
            self.navigationController!.setViewControllers(arrControllers, animated: true)
        }
    }
    
    func gotoBookingIndividualVC(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: BookingIndividualVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func gotoBookingCalendarVCWith(objBookingServices : BookingServices){
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingCalendarVC") as? BookingCalendarVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.objBookingServices = objBookingServices
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoBookingCompanyVCWith(objBookingServices : BookingServices){
        
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingCompanyVC") as? BookingCompanyVC{
            objVC.objBookingServices = objBookingServices
            objVC.objArtistDetails = objArtistDetails
            objVC.arrStaffInfo = objArtistDetails.arrStaffInfo
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoBusinessHoursVC(){
    
           let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as? BusinessHoursVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
             }
        
     }    
}



