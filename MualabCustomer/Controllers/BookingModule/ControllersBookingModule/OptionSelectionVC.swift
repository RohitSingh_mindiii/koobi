//
//  OptionSelectionVC.swift
//  MualabCustomer
//
//  Created by Mac on 16/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol OptionSelectionVCDelegate {
    func finishSectionWithData(strSelectedOption: String)
}

class OptionSelectionVC: UIViewController {
    
    weak var delegate: OptionSelectionVCDelegate?
    
    @IBOutlet weak var picker: UIPickerView!
    var pickerData: [String] = [String]()
    var strSelectedOption : String = ""
    @IBOutlet weak var bottomConstraintPicker : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Input data into the Array:
        pickerData = ["Weekely", "Monthly", "After Every 15 days"]
        
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        self.bottomConstraintPicker.constant = -self.view.frame.size.height/2
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.bottomConstraintPicker.constant = 0
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        pickerData = ["Weekely", "Monthly", "After Every 15 days"]
        strSelectedOption = pickerData[0]
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension OptionSelectionVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strSelectedOption = pickerData[row]
    }
}

// MARK: - IBActions
extension OptionSelectionVC{
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        
        self.bottomConstraintPicker.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }   
        delegate?.finishSectionWithData(strSelectedOption: strSelectedOption)
    }
}
