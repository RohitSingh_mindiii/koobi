//
//  BookingServices.swift
//  MualabCustomer
//
//  Created by Mac on 09/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class BookingServices {
    
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
    var bookingId : Int = 0
    
    var serviceName : String = ""
    var subServiceName : String = ""
    var subSubServiceName : String = ""
    
    var serviceType : String = "" //inCall = 1, outCall = 2
    var bookingDateForServer : String = ""
    var mergedBookingDate : Date?
    var startTime : String = ""
    var endTime : String = ""
    var strPrice : String = ""

    var price : Double = 0
    var completionTime : String = ""
    var isOutcall : Bool = false
    var bookingDate : String = ""
    var bookingTime : String = ""
    var isBooked : Bool = false
    var isFromEdit : Bool = false
    var isAddedToArray : Bool = false
    var staffName : String = ""
    var staffImage : String = ""
    var addressForBooking : String = ""
    var bookingSetting : Int = 0
    var staffId : Int = 0
    var bookStaffId : Int = 0
    var objSubService: SubService = SubService(dict: ["":""])
    var objSubSubService: SubSubService = SubSubService(dict: ["":""])
}


