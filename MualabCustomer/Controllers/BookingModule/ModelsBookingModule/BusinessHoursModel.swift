//
//  BusinessHoursModel.swift
//  MualabBusiness
//
//  Created by Mac on 04/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class openingTimes:NSObject {
    
    var isOpen = false
    var strDay : String = ""
    var arrTimeSloats = [timeSloat]()
    
    init?(open:Bool,day:String,times:[timeSloat]) {
        self.isOpen = open
        self.strDay = day
        self.arrTimeSloats = times
    }
}

class timeSloat:NSObject {
    
    var strStartTime : String = ""
    var strEndTime : String = ""
    
    init?(startTime:String,endTime:String) {
        self.strStartTime = startTime
        self.strEndTime = endTime
    }
}
