//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView
import FSCalendar
import DropDown

class BookingNewVC : UIViewController,UITableViewDelegate,UITableViewDataSource , UICollectionViewDelegate, UICollectionViewDataSource,FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance, UIGestureRecognizerDelegate {
    
    @IBAction func showScrollingNC(sender:UIButton) {
        self.view.endEditing(true)
        if sender.tag == 0{
            isBusinessType = true
        }else{
            isBusinessType = false
        }
        var objGetData : [String] = []
        if isBusinessType{
            objGetData = self.arrServices.map { $0.serviceName }
            if self.arrServices.count == 0{
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
        }else{
            objGetData = self.arrSubServices1.map { $0.subServiceName }
            if self.arrSubServices1.count == 0{
                objAppShareData.showAlert(withMessage: "No category found", type: alertType.bannerDark,on: self)
                return
            }
        }
        objAppShareData.arrTBottomSheetModal = objGetData
       
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        if isBusinessType{
            scrollingNC.strHeaderTile = "Business Types"
        }else{
            scrollingNC.strHeaderTile = "Category Types"
        }
        scrollingNC.modalPresentationStyle = .overCurrentContext
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            
            ////
            strongSelf.objBookingServices.subSubServiceId = 0
            strongSelf.objBookingServices.bookingTime = ""
            ////
            
                if strongSelf.isBusinessType{
                    strongSelf.arrSubSubServicesOutCall.removeAll()
                    strongSelf.arrSubSubServicesInCall.removeAll()
                    let obj = strongSelf.arrServices[Int(str) ?? 0]
                    strongSelf.arrSubServices1 = obj.arrSubServices
                    if strongSelf.arrSubServices1.count>0{
                        let objMyCat = strongSelf.arrSubServices1[0]
                        for obj in objMyCat.arrInCall{
                            obj.isSelected = false
                        }
                        for obj in objMyCat.arrOutCall{
                            obj.isSelected = false
                        }
                        strongSelf.arrSubSubServicesInCall = objMyCat.arrInCall
                        strongSelf.arrSubSubServicesOutCall = objMyCat.arrOutCall
                        strongSelf.lblCategory.text = objMyCat.subServiceName
                        strongSelf.objBookingServices.subServiceId =  objMyCat.subServiceId
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }else{
                        strongSelf.lblServiceTitleForHide.isHidden = true
                        strongSelf.lblCategory.text = "No category"
                        strongSelf.objBookingServices.subServiceId =  0
                    }
                    strongSelf.lblBusinessType.text = obj.serviceName
                    DispatchQueue.main.async {
                        strongSelf.tblBookingList.reloadData()
                        ////
                        if strongSelf.isOutCallSelected{
                            if strongSelf.arrSubSubServicesOutCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 250.0
                            }
                        }else{
                            if strongSelf.arrSubSubServicesInCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 150.0
                            }
                        }
                        ////
                    }
                    strongSelf.objBookingServices.serviceId = obj.serviceId
                    if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                        //self.viewTable.isHidden = true
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        //self.viewTable.isHidden = false
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                    if strongSelf.arrSubServices1.count == 1 || strongSelf.arrSubServices1.count == 0{
                        strongSelf.imgCategoryDown.isHidden = true
                        strongSelf.btnCategory.isUserInteractionEnabled = false
                    }else{
                        strongSelf.imgCategoryDown.isHidden = false
                        strongSelf.btnCategory.isUserInteractionEnabled = true
                    }
                }else{
                    strongSelf.item = Int(str) ?? 0
                    let objSub = strongSelf.arrSubServices1[strongSelf.item]
                    for obj in objSub.arrInCall{
                        obj.isSelected = false
                    }
                    for obj in objSub.arrOutCall{
                        obj.isSelected = false
                    }
                    strongSelf.arrSubSubServicesInCall = objSub.arrInCall
                    strongSelf.arrSubSubServicesOutCall = objSub.arrOutCall
                    strongSelf.lblCategory.text = objSub.subServiceName
                    DispatchQueue.main.async {
                        strongSelf.tblBookingList.reloadData()
                        ////
                        if strongSelf.isOutCallSelected{
                            if strongSelf.arrSubSubServicesOutCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 250.0
                                
                            }
                        }else{
                            if strongSelf.arrSubSubServicesInCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 150.0
                            }
                        }
                        ////
                    }
                    strongSelf.objBookingServices.subServiceId =  objSub.subServiceId
                    /*
                     if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                     //self.viewTable.isHidden = true
                     self.tblBookingList.isHidden = true
                     self.lblNoDataFound.isHidden = false
                     }else{
                     //self.viewTable.isHidden = false
                     self.tblBookingList.isHidden = false
                     self.lblNoDataFound.isHidden = true
                     }
                     */
                }
                
                strongSelf.selectedIndex = -1
                strongSelf.isAnySlotSelected = false
                strongSelf.dateSelected = Date()
                strongSelf.calendar.select(strongSelf.dateSelected)
                
                strongSelf.arrInCallStaff.removeAll()
                strongSelf.arrOutCallStaff.removeAll()
                strongSelf.arrToShow.removeAll()
                strongSelf.viewTimeSlots.isHidden = true
                strongSelf.viewStaff.isHidden = true
                strongSelf.objBookingServices.subSubServiceId = 0
                
                if strongSelf.isOutCallSelected{
                    if strongSelf.arrSubSubServicesOutCall.count == 0 {
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                }else{
                    if strongSelf.arrSubSubServicesInCall.count == 0 {
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                }
                
                strongSelf.arrInCallStaff.removeAll()
                strongSelf.arrOutCallStaff.removeAll()
                strongSelf.collectionStaff.reloadData()
                strongSelf.viewStaff.isHidden = true
                
                strongSelf.arrToShow.removeAll()
                strongSelf.collectionVw.reloadData()
                if strongSelf.arrToShow.count == 0 {
                    strongSelf.lblNoSlotFound.isHidden = false
                    strongSelf.lblSelectTime.isHidden = true
                    strongSelf.collectionVw.isHidden = true
                }else{
                    strongSelf.lblNoSlotFound.isHidden = true
                    strongSelf.lblSelectTime.isHidden = false
                    strongSelf.collectionVw.isHidden = false
                }
                strongSelf.manageTableViewHeight()
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        if isBusinessType{
            tableVc.str = "Business Types"
        }else{
            tableVc.str = "Category Types"
        }
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            
            ////
            strongSelf.objBookingServices.subSubServiceId = 0
            strongSelf.objBookingServices.bookingTime = ""
            ////
            
                if strongSelf.isBusinessType{
                    strongSelf.arrSubSubServicesOutCall.removeAll()
                    strongSelf.arrSubSubServicesInCall.removeAll()
                    let obj = strongSelf.arrServices[Int(str) ?? 0]
                    strongSelf.arrSubServices1 = obj.arrSubServices
                    if strongSelf.arrSubServices1.count>0{
                        let objMyCat = strongSelf.arrSubServices1[0]
                        for obj in objMyCat.arrInCall{
                            obj.isSelected = false
                        }
                        for obj in objMyCat.arrOutCall{
                            obj.isSelected = false
                        }
                        strongSelf.arrSubSubServicesInCall = objMyCat.arrInCall
                        strongSelf.arrSubSubServicesOutCall = objMyCat.arrOutCall
                        strongSelf.lblCategory.text = objMyCat.subServiceName
                        strongSelf.objBookingServices.subServiceId =  objMyCat.subServiceId
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }else{
                        strongSelf.lblServiceTitleForHide.isHidden = true
                        strongSelf.lblCategory.text = "No category"
                        strongSelf.objBookingServices.subServiceId =  0
                    }
                    strongSelf.lblBusinessType.text = obj.serviceName
                    DispatchQueue.main.async {
                        strongSelf.tblBookingList.reloadData()
                        ////
                        if strongSelf.isOutCallSelected{
                            if strongSelf.arrSubSubServicesOutCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 250.0
                            }
                        }else{
                            if strongSelf.arrSubSubServicesInCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 150.0
                            }
                        }
                        ////
                    }
                    strongSelf.objBookingServices.serviceId = obj.serviceId
                    if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                        //self.viewTable.isHidden = true
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        //self.viewTable.isHidden = false
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                    if strongSelf.arrSubServices1.count == 1 || strongSelf.arrSubServices1.count == 0{
                        strongSelf.imgCategoryDown.isHidden = true
                        strongSelf.btnCategory.isUserInteractionEnabled = false
                    }else{
                        strongSelf.imgCategoryDown.isHidden = false
                        strongSelf.btnCategory.isUserInteractionEnabled = true
                    }
                }else{
                    strongSelf.item = Int(str) ?? 0
                    let objSub = strongSelf.arrSubServices1[strongSelf.item]
                    for obj in objSub.arrInCall{
                        obj.isSelected = false
                    }
                    for obj in objSub.arrOutCall{
                        obj.isSelected = false
                    }
                    strongSelf.arrSubSubServicesInCall = objSub.arrInCall
                    strongSelf.arrSubSubServicesOutCall = objSub.arrOutCall
                    strongSelf.lblCategory.text = objSub.subServiceName
                    DispatchQueue.main.async {
                        strongSelf.tblBookingList.reloadData()
                        ////
                        if strongSelf.isOutCallSelected{
                            if strongSelf.arrSubSubServicesOutCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 250.0
                                
                            }
                        }else{
                            if strongSelf.arrSubSubServicesInCall.count==1 && strongSelf.calendar.scope == .week{
                                strongSelf.slotViewHeightConstraint.constant = 150.0
                            }
                        }
                        ////
                    }
                    strongSelf.objBookingServices.subServiceId =  objSub.subServiceId
                    /*
                     if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                     //self.viewTable.isHidden = true
                     self.tblBookingList.isHidden = true
                     self.lblNoDataFound.isHidden = false
                     }else{
                     //self.viewTable.isHidden = false
                     self.tblBookingList.isHidden = false
                     self.lblNoDataFound.isHidden = true
                     }
                     */
                }
                
                strongSelf.selectedIndex = -1
                strongSelf.isAnySlotSelected = false
                strongSelf.dateSelected = Date()
                strongSelf.calendar.select(strongSelf.dateSelected)
                
                strongSelf.arrInCallStaff.removeAll()
                strongSelf.arrOutCallStaff.removeAll()
                strongSelf.arrToShow.removeAll()
                strongSelf.viewTimeSlots.isHidden = true
                strongSelf.viewStaff.isHidden = true
                strongSelf.objBookingServices.subSubServiceId = 0
                
                if strongSelf.isOutCallSelected{
                    if strongSelf.arrSubSubServicesOutCall.count == 0 {
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                }else{
                    if strongSelf.arrSubSubServicesInCall.count == 0 {
                        strongSelf.tblBookingList.isHidden = true
                        strongSelf.lblNoDataFound.isHidden = false
                        strongSelf.viewBigForHide.isHidden = true
                        strongSelf.lblServiceTitleForHide.isHidden = true
                    }else{
                        strongSelf.tblBookingList.isHidden = false
                        strongSelf.lblNoDataFound.isHidden = true
                        strongSelf.viewBigForHide.isHidden = false
                        strongSelf.lblServiceTitleForHide.isHidden = false
                    }
                }
                
                strongSelf.arrInCallStaff.removeAll()
                strongSelf.arrOutCallStaff.removeAll()
                strongSelf.collectionStaff.reloadData()
                strongSelf.viewStaff.isHidden = true
                
                strongSelf.arrToShow.removeAll()
                strongSelf.collectionVw.reloadData()
                if strongSelf.arrToShow.count == 0 {
                    strongSelf.lblNoSlotFound.isHidden = false
                    strongSelf.lblSelectTime.isHidden = true
                    strongSelf.collectionVw.isHidden = true
                }else{
                    strongSelf.lblNoSlotFound.isHidden = true
                    strongSelf.lblSelectTime.isHidden = false
                    strongSelf.collectionVw.isHidden = false
                }
                strongSelf.manageTableViewHeight()
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
   
  
    var sheetHeight: CGFloat? = 250.0
    
    var managedScrollView: UIScrollView? {
        return nil
    }
    
  
    var cornerRadius: CGFloat {
        return 8.0
    }
 
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
    
    var serviceIdForCheck = 0
    var arrToShow = [TimeSlot]()
    fileprivate var item = 0
    fileprivate var isInitiallyCalendar = false
    fileprivate var isAnySlotSelected = false
    fileprivate var arrServices : [Service] = []
    fileprivate var arrSubServices1 : [SubService] = []
    fileprivate var arrSubSubServicesInCall : [SubSubService] = []
    fileprivate var arrSubSubServicesOutCall : [SubSubService] = []
    var objServiceForBooking = SubSubService.init(dict: [:])
    var arrSelectedService = [BookingServices]()
    var objBookingServices = BookingServices()
    var objArtistDetails = ArtistDetails(dict: ["":""])
    fileprivate var arrInCallStaff : [StaffServiceNew] = []
    fileprivate var arrOutCallStaff : [StaffServiceNew] = []
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var viewUnavailable: UIView!
    var strInCallOrOutCallFromService = ""
    fileprivate var arrOpeningTimes = [openingTimes]()
    //user detail outlat
    var somedays : Array = [Int]()

    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    @IBOutlet weak var viewBigForHide: UIView!
    @IBOutlet weak var lblTodayText: UILabel!
    var staffHours = [AddStaffHours]()
    fileprivate var kOpenSectionTag: Int = 0
    fileprivate var arrCategories = [String]()
    fileprivate var arrSubCategories=[String]()
    fileprivate var expandedSectionHeaderNumber: Int = -1
    fileprivate var walkingBlock = 0
    
    //// New service module
    let dropDown = DropDown()
    fileprivate var isBusinessType = true
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var lblServiceTitleForHide: UILabel!
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var btnBusinessType: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var imgBusinessTypeDown: UIImageView!
    @IBOutlet weak var imgCategoryDown: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblTodayOpeningTime: UILabel!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var imgForOutCall: UIImageView!
    @IBOutlet weak var viewForOutCall: UIView!
    fileprivate var isOutCallSelected = false
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    ////
    
    //Calendar
    var newDate = Date()
    var arrHours:[[String:Any]] = [[:]]
    @IBOutlet weak var btnForOutCall: UIButton!
    @IBOutlet weak var viewTimeSlots: UIView!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    var dateSelected : Date = Date()
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var slotViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!
    @IBOutlet weak var dataHeightConstraint: NSLayoutConstraint!
    var selectedIndex = -1
    @IBOutlet weak var lblSelectedDete: UILabel!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    //Calendar
    @IBOutlet weak var lblServiceNoLonger: UILabel!
    @IBOutlet weak var lblSelectTime: UILabel!
    @IBOutlet weak var lblNoSlotFound: UILabel!
    @IBOutlet weak var viewStaff: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var collectionVw: UICollectionView!
    @IBOutlet weak var collectionStaff: UICollectionView!
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewUnavailable.isHidden = true
        objActivity.startActivityIndicator()
        self.isInitiallyCalendar = true
        self.calendar.isHidden = true
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        self.setuoDropDownAppearance()
        self.lblNoDataFound.isHidden = true
        self.viewBigForHide.isHidden = false
        self.lblServiceTitleForHide.isHidden = false
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        //objLocationManager.getCurrentLocation()
        let nib = UINib(nibName: "CategoriesHeaderAddStaff", bundle: nil)
        self.tblBookingList.register(nib, forHeaderFooterViewReuseIdentifier: "CategoriesHeaderAddStaff")
        self.tblBookingList.reloadData()
        
        ////
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 250.0
            }
        }else{
            if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 150.0
            }
        }
        ////
        
        //callWebserviceForGetServices()
        self.configureCalendar()
        self.changeCalenderHeader(date: Date())
        dateSelected = Date()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.calendar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btnForOutCall.isUserInteractionEnabled = false
        self.scrollViewMain.contentOffset.y = 0
        self.view.layoutIfNeeded()
        
        if objAppShareData.isBookingFromService{
            self.objServiceForBooking = objAppShareData.objServiceForEditBooking
        }
        self.viewStaff.isHidden = true
        self.viewTimeSlots.isHidden = true
        
        kOpenSectionTag = 0
        item = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        ////
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 250.0
            }
        }else{
            if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 150.0
            }
        }
        ////
        self.lblNoDataFound.isHidden = true
        self.viewBigForHide.isHidden = false
        self.lblServiceTitleForHide.isHidden = false
        self.callWebserviceForGetServices()
        
        //self.dataHeightConstraint.constant = self.view.bounds.height - 30
        dateSelected = Date()
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        ////
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 250.0
            }
        }else{
            if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 150.0
            }
        }
        ////
        self.lblNoDataFound.isHidden = true
        self.viewBigForHide.isHidden = false
        self.lblServiceTitleForHide.isHidden = false
        callWebserviceForGetServices()
    }
}

//MARK: - button extension
fileprivate extension BookingNewVC {
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)
    }
    func configureView(){
        //let str =  String(self.objArtistDetails.ratingCount)
        //guard let n = NumberFormatter().number(from: str) else { return }
        //self.viewRating.value = CGFloat(truncating: n)
        self.viewRating.value = CGFloat(self.objArtistDetails.ratingCount)
        self.lblReviewCount.text = "(" + String(self.objArtistDetails.reviewCount) + ")"
        self.lblUserName.text = self.objArtistDetails.userName
        if self.objArtistDetails.profileImage.count > 0 {
            if let url = URL(string:self.objArtistDetails.profileImage){
                //self.imgProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        if self.arrSubSubServicesOutCall.count>0{
            //self.viewForOutCall.isHidden = false
        }else{
            //self.viewForOutCall.isHidden = false
            //self.viewForOutCall.isHidden = true
        }
        self.manageTableViewHeight()
    }
    
    func manageTableViewHeight(){
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count<=1{
                self.heightTableView.constant = 90
            }else if self.arrSubSubServicesOutCall.count>=4{
                //self.heightTableView.constant = 270
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesOutCall.count*60) + 30)
            }else{
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesOutCall.count*60) + 30)
            }
        }else{
            if self.arrSubSubServicesInCall.count<=1{
                self.heightTableView.constant = 90
            }else if self.arrSubSubServicesInCall.count>=4{
                //self.heightTableView.constant = 270
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesInCall.count*60) + 30)
            }else{
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesInCall.count*60) + 30)
            }
        }
        
        //        if self.tblBookingList.contentSize.height <= 90{
        //           self.heightTableView.constant = 90
        //        }else if self.tblBookingList.contentSize.height >= 270{
        //            self.heightTableView.constant = 270
        //        }else{
        //            self.heightTableView.constant = self.tblBookingList.contentSize.height + 30
        //        }
        self.view.layoutIfNeeded()
    }
    
    @objc func showBookingSessionAlertMessage(){
        objWebserviceManager.StopIndicator()
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}

fileprivate extension BookingNewVC{
    
    // MARK: - Expand / Collapse Methods
    
    /*
     @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
     
     let headerView = sender.view as!  CategoriesHeaderAddStaff
     let section    = headerView.tag
     headerView.lblName.textColor = UIColor.theameColors.pinkColor
     headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
     
     headerView.imgDropDown.isHidden = true
     
     if (self.expandedSectionHeaderNumber == -1) {
     
     self.expandedSectionHeaderNumber = section
     kOpenSectionTag = section
     tableViewExpandSection(section, headerView: headerView)
     
     } else {
     
     if (self.expandedSectionHeaderNumber == section) {
     kOpenSectionTag = section
     tableViewCollapeSection(section, headerView: headerView)
     } else {
     let previousHeader = self.tblBookingList.headerView(forSection: kOpenSectionTag) as? CategoriesHeaderAddStaff
     tableViewCollapeSection(kOpenSectionTag,  headerView: previousHeader!)
     tableViewExpandSection(section, headerView: headerView)
     kOpenSectionTag = section
     }
     }
     }
     
     func tableViewCollapeSection(_ section: Int, headerView: CategoriesHeaderAddStaff ) {
     let imageView : UIImageView = headerView.imgDropDown
     let objService  = self.arrServices[section]
     let sectionData = objService.arrSubServices
     
     headerView.lblName.textColor = UIColor.theameColors.pinkColor
     headerView.imgDropDown.isHidden = true
     
     self.expandedSectionHeaderNumber = -1;
     if (sectionData.count == 0) {
     return;
     } else {
     headerView.lblName.textColor = UIColor.black
     headerView.lblBotumeLayer.backgroundColor = UIColor.clear
     
     UIView.animate(withDuration: 0.4, animations: {
     imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
     })
     var indexesPath = [IndexPath]()
     for i in 0 ..< sectionData.count {
     let index = IndexPath(row: i, section: section)
     indexesPath.append(index)
     }
     self.tblBookingList!.beginUpdates()
     self.tblBookingList!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
     self.tblBookingList!.endUpdates()
     }
     }
     
     func tableViewExpandSection(_ section: Int, headerView: CategoriesHeaderAddStaff) {
     let objService  = self.arrSubServices1[section]
     let sectionData = objService.arrSubSubService
     
     let imageView : UIImageView = headerView.imgDropDown
     headerView.lblName.textColor = UIColor.theameColors.pinkColor
     headerView.imgDropDown.isHidden = true
     
     headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
     if (sectionData.count == 0) {
     self.expandedSectionHeaderNumber = -1;
     return;
     } else {
     
     headerView.lblName.textColor = UIColor.theameColors.pinkColor
     headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
     UIView.animate(withDuration: 0.4, animations: {
     imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
     })
     var indexesPath = [IndexPath]()
     for i in 0 ..< sectionData.count {
     let index = IndexPath(row: i, section: section)
     indexesPath.append(index)
     }
     self.expandedSectionHeaderNumber = section
     self.tblBookingList!.beginUpdates()
     self.tblBookingList!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
     self.tblBookingList!.endUpdates()
     }
     }
     */
}

//MARK: - UITableview delegate
extension BookingNewVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        if self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count>0{
            return 2
        }else if (self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count==0) || (self.arrSubSubServicesInCall.count==0 && self.arrSubSubServicesOutCall.count>0){
            return 1
        }else{
            return 0
        }
        /*
         if self.arrSubServices1.count > 0 {
         tableView.backgroundView = nil
         return arrSubServices1.count
         } else {
         let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
         messageLabel.text = " No data Found "//"Retrieving data.\nPlease wait."
         messageLabel.numberOfLines = 0;
         messageLabel.textAlignment = .center;
         messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
         self.tblBookingList.backgroundView = messageLabel;
         }
         */
        return 0
    }
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isOutCallSelected{
            return self.arrSubSubServicesOutCall.count
        }else{
            return self.arrSubSubServicesInCall.count
        }
        /*
         if section == 0 && self.arrSubSubServicesInCall.count != 0{
         return self.arrSubSubServicesInCall.count
         }else if section == 0 && self.arrSubSubServicesInCall.count == 0{
         return self.arrSubSubServicesOutCall.count
         }else{
         return self.arrSubSubServicesOutCall.count
         }
         */
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        var subviewArray = Bundle.main.loadNibNamed("CategoriesHeaderAddStaff", owner: self, options: nil)
        let header = subviewArray?[0] as! CategoriesHeaderAddStaff
        //header.lblName.textColor = UIColor.theameColors.pinkColor
        header.lblName.textColor = UIColor.black
        //header.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
        header.lblBotumeLayer.backgroundColor = UIColor.clear
        //header.imgDropDown.isHidden = true
        //let objService = arrSubServices1[section]
        //header.lblName.text = objService.subServiceName
        //        if section == 0 && self.arrSubSubServicesInCall.count != 0{
        //            header.lblName.text = "In Call"
        //        }else if section == 0 && self.arrSubSubServicesInCall.count == 0{
        //            header.lblName.text = "Out Call"
        //        }else{
        //            header.lblName.text = "Out Call"
        //        }
        return header
        //return nil
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        var objSubService = SubSubService.init(dict: [:])
        if self.isOutCallSelected{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            //let doubleStr = String(format: "%.2f", ceil(objSubService.outCallPrice))
            let twoDecimalPlaces = String(format: "%.2f", objSubService.outCallPrice)
            cell.lblPrice.text = String(twoDecimalPlaces)
        }else{
            objSubService = arrSubSubServicesInCall[indexPath.row]
            //let doubleStr = String(format: "%.2f", ceil(objSubService.inCallPrice))
            let twoDecimalPlaces = String(format: "%.2f", objSubService.inCallPrice)
            cell.lblPrice.text = String(twoDecimalPlaces)
        }
        
        /*
         if indexPath.section == 0 && self.arrSubSubServicesInCall.count != 0{
         objSubService = arrSubSubServicesInCall[indexPath.row]
         cell.lblPrice.text = String(objSubService.inCallPrice)
         }else if indexPath.section == 0 && self.arrSubSubServicesInCall.count == 0{
         objSubService = arrSubSubServicesOutCall[indexPath.row]
         cell.lblPrice.text = String(objSubService.outCallPrice)
         }else{
         objSubService = arrSubSubServicesOutCall[indexPath.row]
         cell.lblPrice.text = String(objSubService.outCallPrice)
         }
         
         
         if self.selectedIndex == indexPath.row{
         cell?.viewCardDetail.isHidden = false
         cell?.viewCardDetail.isHidden = true
         //cell?.lblNo.textColor = #colorLiteral(red: 0.1882352941, green: 0.6784313725, blue: 0.137254902, alpha: 1)
         cell?.imgSelectCard.image =  UIImage(named:"select_icon")!
         }else{
         cell?.viewCardDetail.isHidden = true
         //cell?.lblNo.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
         cell?.imgSelectCard.image =  nil
         //cell?.imgSelectCard.image =  UIImage(named:"inactive_check")!
         } */
        
        if objSubService.isSelected {
            cell.lblName.textColor = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            cell.imgProfie.image =  UIImage(named:"select_icon")!
            cell.imgProfie.isHidden = false
            ////
            if self.objBookingServices.price == 0.0{
              self.objBookingServices.price = Double(cell.lblPrice.text!) ?? 0.0
            }
            ////
        }else{
            cell.imgProfie.image =  nil
            cell.imgProfie.isHidden = true
            cell.lblName.textColor = UIColor.black
            ////
            if self.objBookingServices.price == 0.0{
              self.objBookingServices.price = Double(cell.lblPrice.text!) ?? 0.0
            }
            ////
        }
    
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        cell.lblName.text = objSubService.subSubServiceName
        cell.lblDuration.text = objSubService.completionTime
        
        if objSubService.completionTime.contains(":"){
            let arr = objSubService.completionTime.components(separatedBy: ":")
            if arr.count == 2{
                let strHr = arr[0]
                let strMin = arr[1]
                if strHr != "00"{
                    cell.lblDuration.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                }else{
                    cell.lblDuration.text = strMin + " " + "min"
                }
            }
        }
        //        if objSubService.isStaff == 0{
        //            cell.lblDuration.isHidden = false
        //            cell.lblPrice.isHidden = false
        //        }else{
        //            cell.lblDuration.isHidden = true
        //            cell.lblPrice.isHidden = true
        //        }
        if self.isOutCallSelected{
            if objSubService.isOutCallStaff == 0{
                cell.lblDuration.isHidden = false
                cell.lblPrice.isHidden = false
            }else{
                cell.lblDuration.isHidden = true
                cell.lblPrice.isHidden = true
            }
        }else{
            if objSubService.isInCallStaff == 0{
                cell.lblDuration.isHidden = false
                cell.lblPrice.isHidden = false
            }else{
                cell.lblDuration.isHidden = true
                cell.lblPrice.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isOutCallSelected {
            for obj in self.arrSubSubServicesOutCall{
                obj.isSelected = false
            }
            let obj = self.arrSubSubServicesOutCall[indexPath.row]
            obj.isSelected = true
            self.objBookingServices.price = obj.outCallPrice
            self.objBookingServices.subSubServiceId = obj.subSubServiceId
            self.serviceIdForCheck = obj.subSubServiceId
            self.objBookingServices.completionTime = obj.completionTime
            self.objBookingServices.objSubSubService = obj
            
        }else{
            for obj in self.arrSubSubServicesInCall{
                obj.isSelected = false
            }
            let obj = self.arrSubSubServicesInCall[indexPath.row]
            obj.isSelected = true
            self.objBookingServices.price = obj.inCallPrice
            self.objBookingServices.subSubServiceId = obj.subSubServiceId
            self.serviceIdForCheck = obj.subSubServiceId
            self.objBookingServices.completionTime = obj.completionTime
            self.objBookingServices.objSubSubService = obj
        }
        ////
        self.objBookingServices.staffId = 0
        ////
        //var serviceTime = ""
        if self.objBookingServices.objSubSubService.isStaff == 0{
            self.objBookingServices.staffId = 0
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.collectionStaff.reloadData()
            self.viewStaff.isHidden = true
            self.getlatestSlotDataFor(dateSelected: dateSelected)
        }else{
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.collectionStaff.reloadData()
            self.viewStaff.isHidden = false
            objActivity.startActivityIndicator()
            self.callWebserviceForGetStaff()
        }
        ////
        self.selectedIndex = -1
        isAnySlotSelected = false
        ////
        self.calendar.select(dateSelected)
        self.tblBookingList.reloadData()
        ////
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                if self.arrSubSubServicesOutCall.count == 1 && self.calendarHeightConstraint.constant<200.0{
                    self.slotViewHeightConstraint.constant = 264.0
                }else{
                   self.slotViewHeightConstraint.constant = 150.0
                }
                self.view.layoutIfNeeded()
            }
        }else{
            if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                if self.arrSubSubServicesInCall.count == 1 && self.calendarHeightConstraint.constant<200.0{
                    self.slotViewHeightConstraint.constant = 264.0
                }else{
                    self.slotViewHeightConstraint.constant = 150.0
                }
                self.view.layoutIfNeeded()
            }
        }
        ////
    }
}

//MARK: - button extension
extension BookingNewVC {
    @IBAction func btnBackUnavailableAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSerachAnotherAction(_ sender: UIButton) {
        objAppShareData.selectedTab = 0
        objAppShareData.selectedUserIdForProfile = 0
        objAppShareData.isOtherSelectedForProfile = false
        objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.objArtistDetails.isAlreadyBooked == 1{
            self.showAlertMessage()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnBusinessHoursAction(_ sender: UIButton) {
        /*
         let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
         if let objVC = sbNew.instantiateViewController(withIdentifier:"workingHoursCompanyVC") as? workingHoursCompanyVC{
         objVC.hidesBottomBarWhenPushed = true
         objVC.arrOpeningTimes = self.arrOpeningTimes
         navigationController?.pushViewController(objVC, animated: true)
         }*/
        let sb: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as? BusinessHoursVC{
            objVC.objArtistDetails = objArtistDetails
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.modalPresentationStyle = .fullScreen
            present(objVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnContinueAction(_ sender: UIButton) {
        if self.walkingBlock != 0{
           objAppShareData.showAlert(withMessage: "This artist is not available" , type: alertType.bannerDark,on: self)
            return
        }
        if self.objArtistDetails.isAlreadyBooked == 1{
            if self.objBookingServices.objSubSubService.subSubServiceId == 0{
                //objAppShareData.showAlert(withMessage: "Please select service" , type: alertType.bannerDark,on: self)
                self.goToConfirmBookingScreen()
            }else if self.objBookingServices.bookingTime.count == 0{
                //objAppShareData.showAlert(withMessage: "Please select time slot" , type: alertType.bannerDark,on: self)
                self.goToConfirmBookingScreen()
            }else{
                self.callWebserviceForBookArtist()
            }
        }else{
            //if self.objBookingServices.objSubSubService.subSubServiceId == 0{
            if self.objBookingServices.subSubServiceId == 0{
                objAppShareData.showAlert(withMessage: "Please select service" , type: alertType.bannerDark,on: self)
            }else if self.objBookingServices.bookingTime.count == 0{
                objAppShareData.showAlert(withMessage: "Please select time slot" , type: alertType.bannerDark,on: self)
            }else{
                self.callWebserviceForBookArtist()
            }
        }
    }
    @IBAction func btnOutCallAction(_ sender: UIButton) {
        return
        if self.objArtistDetails.isAlreadyBooked == 1{
            //objAppShareData.showAlert(withMessage: "Not able to change service type" , type: alertType.bannerDark,on: self)
            return
        }
        self.arrInCallStaff.removeAll()
        self.arrOutCallStaff.removeAll()
        self.arrToShow.removeAll()
        self.viewTimeSlots.isHidden = true
        self.viewStaff.isHidden = true
        self.selectedIndex = -1
        isAnySlotSelected = false
        self.dateSelected = Date()
        self.calendar.select(dateSelected)
        self.objBookingServices.subSubServiceId = 0
        
        if self.isOutCallSelected{
            self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
            self.isOutCallSelected = false
            for obj in self.arrSubSubServicesOutCall{
                obj.isSelected = false
            }
            if self.arrSubSubServicesInCall.count>0{
                self.lblNoDataFound.isHidden = true
                self.viewBigForHide.isHidden = false
                self.lblServiceTitleForHide.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = false
                self.viewBigForHide.isHidden = true
                self.lblServiceTitleForHide.isHidden = true
            }
        }else{
            self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
            self.isOutCallSelected = true
            for obj in self.arrSubSubServicesInCall{
                obj.isSelected = false
            }
            if self.arrSubSubServicesOutCall.count>0{
                self.lblNoDataFound.isHidden = true
                self.viewBigForHide.isHidden = false
                self.lblServiceTitleForHide.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = false
                self.viewBigForHide.isHidden = true
                self.lblServiceTitleForHide.isHidden = true
            }
        }
        
        self.arrInCallStaff.removeAll()
        self.arrOutCallStaff.removeAll()
        self.collectionStaff.reloadData()
        self.viewStaff.isHidden = true
        self.arrToShow.removeAll()
        self.collectionVw.reloadData()
        if self.arrToShow.count == 0 {
            self.lblNoSlotFound.isHidden = false
            self.lblSelectTime.isHidden = true
            self.collectionVw.isHidden = true
        }else{
            self.lblNoSlotFound.isHidden = true
            self.lblSelectTime.isHidden = false
            self.collectionVw.isHidden = false
        }
        
        DispatchQueue.main.async {
            self.tblBookingList.reloadData()
            ////
            if self.isOutCallSelected{
                if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                    self.slotViewHeightConstraint.constant = 250.0
                }
            }else{
                if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                    self.slotViewHeightConstraint.constant = 150.0
                }
            }
            ////
        }
        self.manageTableViewHeight()
    }
}

//MARK: - callWebserviceForGetServices extension
fileprivate extension BookingNewVC{
    func callWebserviceForGetServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objActivity.startActivityIndicator()
        objWebserviceManager.StartIndicator()
        let dicParam = [
            "artistId":objAppShareData.selectedOtherIdForProfile
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: dicParam, success: { response in
            print(response)
            self.arrServices.removeAll()
            objActivity.stopActivity()
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    self.saveData(dict:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseBusinessHourData(arrFinam: [AddStaffHours]){
        /*
         let sloatArray = arrFinam
         if sloatArray.count > 0{
         for dicSloats in sloatArray{
         var strSrtartTime = ""
         var strEndTime = ""
         var day = ""
         strSrtartTime = dicSloats.startTime
         strEndTime = dicSloats.endTime
         day = String(dicSloats.day)
         day = objAppShareData.getDayFromSelectedDate(strDate: day)
         
         if objAppShareData.objModelEditTimeSloat.arrFinalTimes.count > 0 {
         var addtrue = true
         for dayChack in objAppShareData.objModelEditTimeSloat.arrFinalTimes{
         if day == dayChack.strDay{
         addtrue = false
         let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
         dayChack.arrTimeSloats.append(objSloats!)
         }
         }
         if addtrue == true{
         let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
         let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
         objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
         }
         }else{
         let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
         let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
         objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
         
         }
         }
         }
         */
        /*
         self.arrOpeningTimes.removeAll()
         for newObj in arrFinam{
         let isOpen = newObj.isOpen
         let strDay = newObj.strDay
         let arrTimeSloats = newObj.arrTimeSloats
         let obj = openingTimes(open: isOpen, day: strDay, times: arrTimeSloats)
         self.arrOpeningTimes.append(obj!)
         }
         */
    }
    
    func saveData(dict:[String:Any]){
        
        if let dict = dict["artistDetail"] as? [String : Any]{
            self.objArtistDetails = ArtistDetails.init(dict: dict)
            print(dict)
            self.walkingBlock = dict["walkingBlock"] as? Int ?? 0
            if let str = dict["walkingBlock"] as? String{
                self.walkingBlock = Int(str) ?? 0
            }
            if self.walkingBlock != 0 {
               self.viewUnavailable.isHidden = false
            }else{
               self.viewUnavailable.isHidden = true
            }
            ////
            //self.viewUnavailable.isHidden = false
            ////
            self.objArtistDetails.responce = dict
            self.objArtistDetails.isAlreadyBooked = dict["isAlreadybooked"] as! Int
            if (dict["busineshours"] as? [[String : Any]]) != nil{
                let strDate = dateFormatter.string(from: Date())
                let strDay = self.getDayFromSelectedDate(strDate: strDate)
                var dayOld = Int(strDay)
                var strOpeningTime = ""
                let arrHours = dict["busineshours"] as? [[String : Any]]
                self.arrHours = arrHours!
                if let arr = dict["busineshours"] as? [[String : Any]]{
                    for dict in arr{
                        let objSubService = AddStaffHours.init(dict: dict)
                        self.staffHours.append(objSubService)
                    }
                }
                
                //self.parseBusinessHourData(arrFinam: staffHours)
                for dict in arrHours!{
                    let day = dict["day"] as? Int
                    let strStartTime = dict["startTime"] as? String
                    let strEndTime = dict["endTime"] as? String
                    //dayOld = 5
                    if dayOld == day{
                        if strOpeningTime.count != 0{
                            strOpeningTime = strOpeningTime + " & " + strStartTime! + " - " + strEndTime!
                        }else{
                            strOpeningTime = strStartTime! + " - " + strEndTime!
                        }
                    }
                    var newDay = day! + 2
                    if newDay == 8{
                       newDay = 1
                    }
                    self.somedays.append(newDay)
                }
                self.calendar.reloadData()
                if strOpeningTime.count>0{
                    self.lblTodayOpeningTime.text = strOpeningTime
                }else{
                    self.lblTodayOpeningTime.text = "Close"
                }
            }
        }
        if let dict = dict["artistServices"] as? [[String : Any]]{
            for dicService in dict{
                let obj = Service.init(dict: dicService)
                self.arrServices.append(obj)
            }
        }
        
        var indexBusinessType = 0
        var indexCategory = 0
        var indexSubSubService = 0
        if objAppShareData.isBookingFromService{
            
            //for objService in self.arrServices{
            for i in 0..<self.arrServices.count {
                let objService = self.arrServices[i]
                if objService.serviceId == self.objServiceForBooking.serviceId{
                    indexBusinessType = i
                }
                for j in 0..<objService.arrSubServices.count {
                    let objSubService = objService.arrSubServices[j]
                    if objSubService.subServiceId == self.objServiceForBooking.subServiceId{
                        indexCategory = j
                    }
                    var objsuSubService = SubSubService.init(dict: [:])
                    if self.strInCallOrOutCallFromService == "In Call" {
                        self.isOutCallSelected = false
                        self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
                        for k in 0..<objSubService.arrInCall.count{
                            objsuSubService = objSubService.arrInCall[k]
                            indexSubSubService = k
                            if objsuSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                
                                objsuSubService.isSelected = true
                                self.objBookingServices.subSubServiceId = objsuSubService.subSubServiceId
                                self.objBookingServices.completionTime = objsuSubService.completionTime
                                self.objBookingServices.objSubSubService = objsuSubService
                            }
                        }
                    }else{
                        self.isOutCallSelected = true
                        self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
                        for k in 0..<objSubService.arrOutCall.count{
                            objsuSubService = objSubService.arrOutCall[k]
                            indexSubSubService = k
                            if objsuSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                
                                objsuSubService.isSelected = true
                                self.objBookingServices.subSubServiceId = objsuSubService.subSubServiceId
                                self.objBookingServices.completionTime = objsuSubService.completionTime
                                self.objBookingServices.objSubSubService = objsuSubService
                            }
                        }
                    }
                }
            }
            
            var isAlert = true
            for objService in self.arrServices{
                for objSubService in objService.arrSubServices {
                    if self.strInCallOrOutCallFromService == "In Call" {
                        for objSubSubService in objSubService.arrInCall {
                            if objSubSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                isAlert = false
                                break
                            }
                        }
                    }else{
                        for objSubSubService in objSubService.arrOutCall {
                            if objSubSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                isAlert = false
                                break
                            }
                        }
                    }
                    
                }
            }
            self.lblServiceTitleForHide.isHidden = false
            if isAlert{
                if objAppShareData.isFromServiceTag{
                    self.lblServiceTitleForHide.isHidden = true
                    objAppShareData.isFromServiceTag = false
                    objAppShareData.showAlert(withMessage:"Service is no longer available. Please select another service for booking",type: alertType.bannerDark, on: self)
                }
            }else{
                self.lblServiceTitleForHide.isHidden = false
            }
        }
        if objAppShareData.isFromServiceTag{
            objAppShareData.isFromServiceTag = false
        }
        
        
        
        if arrServices.count > 0{
            let objMyData = self.arrServices[indexBusinessType]
            self.lblBusinessType.text = objMyData.serviceName
            self.arrSubServices1.removeAll()
            ////
            self.arrSubSubServicesInCall.removeAll()
            self.arrSubSubServicesOutCall.removeAll()
            ////
            if objMyData.arrSubServices.count>0{
                self.arrSubServices1 = objMyData.arrSubServices
                if self.arrSubServices1.count > indexCategory{
                    let objMyCat = self.arrSubServices1[indexCategory]
                    self.lblCategory.text = objMyCat.subServiceName
                    self.arrSubSubServicesInCall = objMyCat.arrInCall
                    self.arrSubSubServicesOutCall = objMyCat.arrOutCall
                    self.objBookingServices.serviceId = objMyData.serviceId
                    self.objBookingServices.subServiceId =  objMyCat.subServiceId
                }
            }
            self.arrSelectedService.append(self.objBookingServices)
            
            DispatchQueue.main.async {
                self.tblBookingList.reloadData()
                ////
                if self.isOutCallSelected{
                    if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                        self.slotViewHeightConstraint.constant = 250.0
                    }
                }else{
                    if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                        self.slotViewHeightConstraint.constant = 150.0
                    }
                }
                ////
            }
            /*
             if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
             //self.viewTable.isHidden = true
             self.tblBookingList.isHidden = true
             self.lblNoDataFound.isHidden = false
             }else{
             //self.viewTable.isHidden = false
             self.tblBookingList.isHidden = false
             self.lblNoDataFound.isHidden = true
             }
             */
            
            if self.arrServices.count == 1{
                self.imgBusinessTypeDown.isHidden = true
                self.btnBusinessType.isUserInteractionEnabled = false
            }else{
                self.imgBusinessTypeDown.isHidden = false
                self.btnBusinessType.isUserInteractionEnabled = true
            }
            if self.self.arrSubServices1.count == 1 || self.self.arrSubServices1.count == 0{
                if self.arrSubServices1.count == 0{
                    self.lblCategory.text = "No Category"
                    self.lblServiceTitleForHide.isHidden = true
                }else{
                   self.lblServiceTitleForHide.isHidden = false
                }
                self.imgCategoryDown.isHidden = true
                self.btnCategory.isUserInteractionEnabled = false
            }else{
                self.imgCategoryDown.isHidden = false
                self.btnCategory.isUserInteractionEnabled = true
            }
        }else{
            self.lblNoDataFound.isHidden = false
            self.viewBigForHide.isHidden = true
            self.lblServiceTitleForHide.isHidden = true
            self.lblBusinessType.text = "No Business"
            self.lblCategory.text = "No Category"
            self.imgBusinessTypeDown.isHidden = true
            self.imgCategoryDown.isHidden = true
            self.btnCategory.isUserInteractionEnabled = false
            self.btnBusinessType.isUserInteractionEnabled = false
        }
        
        self.configureView()
        
        if !objAppShareData.isBookingFromService && self.objArtistDetails.isAlreadyBooked != 1{
            if self.objArtistDetails.serviceType == "1"{
                self.viewForOutCall.isHidden = true
            }else if self.objArtistDetails.serviceType == "2"{
                self.viewForOutCall.isHidden = false
                self.isOutCallSelected = true
                self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
                self.btnForOutCall.isUserInteractionEnabled = false
            }else{
                self.viewForOutCall.isHidden = false
                self.isOutCallSelected = false
                self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
                //self.btnForOutCall.isUserInteractionEnabled = true
                self.btnForOutCall.isUserInteractionEnabled = false
            }
        }else if self.objArtistDetails.isAlreadyBooked == 1{
            if self.isOutCallSelected{
            }else{
                self.viewForOutCall.isHidden = true
            }
        }
        
        if self.isOutCallSelected{
            self.viewForOutCall.isHidden = false
            if self.arrSubSubServicesOutCall.count == 0 {
                self.lblNoDataFound.isHidden = false
                self.viewBigForHide.isHidden = true
                self.lblServiceTitleForHide.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = true
                self.viewBigForHide.isHidden = false
                self.lblServiceTitleForHide.isHidden = false
            }
        }else{
            self.viewForOutCall.isHidden = true
            if self.arrSubSubServicesInCall.count == 0 {
                self.lblNoDataFound.isHidden = false
                self.viewBigForHide.isHidden = true
                self.lblServiceTitleForHide.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = true
                self.viewBigForHide.isHidden = false
                self.lblServiceTitleForHide.isHidden = false
            }
        }
        
        if objAppShareData.isBookingFromService{
            //objAppShareData.isBookingFromService = false
            if self.objServiceForBooking.strDateForEdit.count>0{
                let formatter  = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                dateSelected = formatter.date(from: self.objServiceForBooking.strDateForEdit)!
            }
            if self.objBookingServices.objSubSubService.isStaff == 0{
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.collectionStaff.reloadData()
                self.viewStaff.isHidden = true
                self.getlatestSlotDataFor(dateSelected: dateSelected)
            }else{
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.collectionStaff.reloadData()
                self.viewStaff.isHidden = false
                self.callWebserviceForGetStaff()
            }
            ////
            //self.selectedIndex = -1
            ////
            self.calendar.select(dateSelected)
            //self.getlatestSlotDataFor(dateSelected: dateSelected)
        }
    }
}

//MARK:- Collection view Delegate Methods
extension BookingNewVC:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionStaff{
            if self.isOutCallSelected{
                return self.arrOutCallStaff.count
            }else{
                return self.arrInCallStaff.count
            }
        }else{
            return self.arrToShow.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == self.collectionStaff{
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for:
                indexPath) as? userCollectionCell)!
            let deadlineTime = DispatchTime.now() + .seconds(1/4)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime){
                cell.imgProfile.setImageFream()
            }
            var objSubService = StaffServiceNew.init(dict: [:])
            if !self.isOutCallSelected{
                objSubService = arrInCallStaff[indexPath.row]
                //let doubleStr = String(format: "%.2f", ceil(objSubService.inCallPrice))
                let doubleStr = String(format: "%.2f", objSubService.inCallPrice)
                cell.lblPrice.text = String(doubleStr)
            }else{
                objSubService = arrOutCallStaff[indexPath.row]
                let doubleStr = String(format: "%.2f", objSubService.outCallPrice)
                cell.lblPrice.text = String(doubleStr)
            }
            
            cell.lblPrice.text = "£" + cell.lblPrice.text!
            if objSubService.staffImage != "" {
                if let url = URL(string: objSubService.staffImage){
                    //cell.imgProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }else{
                cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
            }
            cell.lblUserName.text = objSubService.staffName
            cell.lblTime.text = objSubService.completionTime
            if objSubService.completionTime.contains(":"){
                let arr = objSubService.completionTime.components(separatedBy: ":")
                if arr.count == 2{
                    let strHr = arr[0]
                    let strMin = arr[1]
                    if strHr != "00"{
                        cell.lblTime.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                    }else{
                        cell.lblTime.text = strMin + " " + "min"
                    }
                }
            }
            
            if objSubService.isSelected{
                cell.lblUserName.textColor = UIColor.theameColors.skyBlueNewTheam
                cell.imgProfile.layer.borderWidth = 2.0
                cell.imgProfile.layer.borderColor = UIColor.theameColors.skyBlueNewTheam.cgColor
            }else{
                cell.lblUserName.textColor = UIColor.black
                cell.imgProfile.layer.borderWidth = 1.5
                cell.imgProfile.layer.borderColor = UIColor.lightGray.cgColor
            }
            //cell.btnProfile.accessibilityHint = objSubService.staffName
            //cell.btnProfile.tag = indexPath.row
            //cell.btnProfile.superview?.tag = indexPath.section
            return cell
            
        }else if collectionView == self.collectionVw{
            
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionCell", for:
                indexPath) as? CalenderCollectionCell)!
            
            let objTimeSlot = self.arrToShow[indexPath.row]
            cell.lblTime.text = objTimeSlot.strTimeSlot
            
            if selectedIndex == indexPath.row {
                cell.imgWatch.image = UIImage.init(named:"watch_ico")
                cell.vwBg.backgroundColor = UIColor.theameColors.skyBlueNewTheam
                cell.vwBg.layer.borderWidth = 0.0
                cell.lblTime.textColor = UIColor.white
            }else{
                cell.imgWatch.image = UIImage.init(named:"watch_ico_black")
                cell.lblTime.textColor = UIColor.black
                cell.vwBg.layer.borderWidth = 1.0
                cell.vwBg.layer.borderColor = UIColor.black.cgColor
                cell.vwBg.backgroundColor = UIColor.white
            }
            if !self.isAnySlotSelected{
                cell.vwBlurr.isHidden = true
                cell.isUserInteractionEnabled = true
            }else{
                if selectedIndex == indexPath.row{
                    cell.vwBlurr.isHidden = true
                    cell.isUserInteractionEnabled = true
                }else{
                    cell.vwBlurr.isHidden = false
                    //cell.isUserInteractionEnabled = false
                    cell.isUserInteractionEnabled = true
                }
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if collectionView == self.collectionStaff{
            var objStaff = StaffServiceNew.init(dict: [:])
            if self.isOutCallSelected{
                objStaff = self.arrOutCallStaff[indexPath.row]
                self.objBookingServices.price = objStaff.outCallPrice
            }else{
                objStaff = self.arrInCallStaff[indexPath.row]
                self.objBookingServices.price = objStaff.inCallPrice
            }
            for obj in self.arrOutCallStaff{
                obj.isSelected = false
            }
            for obj in self.arrInCallStaff{
                obj.isSelected = false
            }
            objStaff.isSelected = true
            self.collectionStaff.reloadData()
            if indexPath.row == 0{
                self.objBookingServices.staffId = 0
            }else{
                self.objBookingServices.staffId = objStaff.staffId
            }
            self.objBookingServices.completionTime = objStaff.completionTime
            selectedIndex = -1
            isAnySlotSelected = false
            if self.arrToShow.count>0{
                let index = IndexPath.init(row: 0, section: 0)
                self.collectionVw.scrollToItem(at: index, at: .top, animated: false)
            }
            objActivity.startActivityIndicator()
            self.getlatestSlotDataFor(dateSelected: dateSelected)
            //objAppShareData.showAlert(withMessage:"under developement",type: alertType.bannerDark, on: self)
        }else{
            let objTimeSlot = self.arrToShow[indexPath.row]
            if self.arrSelectedService.count > 0{
                
                let objBookingServices : BookingServices = self.arrSelectedService[0]
                
                if !objBookingServices.isBooked{
                    
                    objBookingServices.bookingDate =  getFormattedBookingDateFrom(dateSelected:dateSelected)
                    objBookingServices.bookingDateForServer =  getFormattedBookingDateForServerFrom(dateSelected:dateSelected)
                    objBookingServices.bookingTime = objTimeSlot.strTimeSlot
                    self.tblBookingList.reloadData()
                    ////
                    if self.isOutCallSelected{
                        if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 250.0
                        }
                    }else{
                        if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 150.0
                        }
                    }
                    ////
                    
                }else{
                    objAppShareData.showAlert(withMessage: "Service Already Added", type: alertType.bannerDark,on: self)
                }
            }
            if selectedIndex == indexPath.row{
                self.isAnySlotSelected = false
                selectedIndex = -1
                isAnySlotSelected = false
            }else{
                self.setCalenderScopeWeek()
                self.isAnySlotSelected = true
                selectedIndex = indexPath.row
                isAnySlotSelected = true
            }
            self.collectionVw.reloadData()
        }
    }
    
    func getFormattedBookingDateForServerFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    
    func getFormattedBookingDateFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "EE, d LLLL yyyy" //"EE, d LLLL yyyy hh:mm a"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    
    //ScrollView delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
    }
    
    @objc func btnSelectService(sender: UIButton!) {
        let objMyData  = self.arrServices[sender.tag]
        item = sender.tag
        //self.collectionView.reloadData()
        self.arrSubServices1.removeAll()
        self.arrSubServices1 = objMyData.arrSubServices
        self.tblBookingList.reloadData()
        ////
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 250.0
            }
        }else{
            if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                self.slotViewHeightConstraint.constant = 150.0
            }
        }
        ////
    }
}

//MARK:- New Service Module
extension BookingNewVC{
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        if sender.tag == 0{
            isBusinessType = true
        }else{
            isBusinessType = false
        }
        
        var objGetData : [String] = []
        if isBusinessType{
            objGetData = self.arrServices.map { $0.serviceName }
            if self.arrServices.count == 0{
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
        }else{
            objGetData = self.arrSubServices1.map { $0.subServiceName }
            if self.arrSubServices1.count == 0{
                objAppShareData.showAlert(withMessage: "No category found", type: alertType.bannerDark,on: self)
                return
            }
        }
        //self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let val = objGetData.count*40
        let yPosition = self.view.frame.size.height - CGFloat(val) - 100
        print(yPosition)
        self.dropDown.bottomOffset = CGPoint(x: 0, y: 350)
        self.dropDown.anchorView = sender // UIView or UIBarButtonItem
        self.dropDown.direction = .bottom
        self.dropDown.dataSource = objGetData
        //self.dropDown.width = self.view.frame.size.width - 50
        self.dropDown.width = self.view.frame.size.width
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if self.isBusinessType{
                self.arrSubSubServicesOutCall.removeAll()
                self.arrSubSubServicesInCall.removeAll()
                let obj = self.arrServices[index]
                self.arrSubServices1 = obj.arrSubServices
                if self.arrSubServices1.count>0{
                    let objMyCat = self.arrSubServices1[0]
                    for obj in objMyCat.arrInCall{
                        obj.isSelected = false
                    }
                    for obj in objMyCat.arrOutCall{
                        obj.isSelected = false
                    }
                    self.arrSubSubServicesInCall = objMyCat.arrInCall
                    self.arrSubSubServicesOutCall = objMyCat.arrOutCall
                    self.lblCategory.text = objMyCat.subServiceName
                    self.objBookingServices.subServiceId =  objMyCat.subServiceId
                    self.lblServiceTitleForHide.isHidden = false
                }else{
                    self.lblCategory.text = "No category"
                    self.lblServiceTitleForHide.isHidden = true
                    self.objBookingServices.subServiceId =  0
                }
                self.lblBusinessType.text = item
                DispatchQueue.main.async {
                    self.tblBookingList.reloadData()
                    ////
                    if self.isOutCallSelected{
                        if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 250.0
                        }
                    }else{
                        if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 150.0
                        }
                    }
                    ////
                }
                self.objBookingServices.serviceId = obj.serviceId
                if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                    //self.viewTable.isHidden = true
                    self.tblBookingList.isHidden = true
                    self.lblNoDataFound.isHidden = false
                    self.viewBigForHide.isHidden = true
                    self.lblServiceTitleForHide.isHidden = true
                }else{
                    //self.viewTable.isHidden = false
                    self.tblBookingList.isHidden = false
                    self.lblNoDataFound.isHidden = true
                    self.viewBigForHide.isHidden = false
                    self.lblServiceTitleForHide.isHidden = false
                }
                if self.arrSubServices1.count == 1 || self.arrSubServices1.count == 0{
                    self.imgCategoryDown.isHidden = true
                    self.btnCategory.isUserInteractionEnabled = false
                }else{
                    self.imgCategoryDown.isHidden = false
                    self.btnCategory.isUserInteractionEnabled = true
                }
            }else{
                self.item = index
                let objSub = self.arrSubServices1[self.item]
                for obj in objSub.arrInCall{
                    obj.isSelected = false
                }
                for obj in objSub.arrOutCall{
                    obj.isSelected = false
                }
                self.arrSubSubServicesInCall = objSub.arrInCall
                self.arrSubSubServicesOutCall = objSub.arrOutCall
                self.lblCategory.text = item
                DispatchQueue.main.async {
                    self.tblBookingList.reloadData()
                    ////
                    if self.isOutCallSelected{
                        if self.arrSubSubServicesOutCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 250.0
                        }
                    }else{
                        if self.arrSubSubServicesInCall.count==1 && self.calendar.scope == .week{
                            self.slotViewHeightConstraint.constant = 150.0
                        }
                    }
                    ////
                }
                self.objBookingServices.subServiceId =  objSub.subServiceId
                /*
                 if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                 //self.viewTable.isHidden = true
                 self.tblBookingList.isHidden = true
                 self.lblNoDataFound.isHidden = false
                 }else{
                 //self.viewTable.isHidden = false
                 self.tblBookingList.isHidden = false
                 self.lblNoDataFound.isHidden = true
                 }
                 */
            }
            
            self.selectedIndex = -1
            self.isAnySlotSelected = false
            self.dateSelected = Date()
            self.calendar.select(self.dateSelected)
            
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.arrToShow.removeAll()
            self.viewTimeSlots.isHidden = true
            self.viewStaff.isHidden = true
            self.objBookingServices.subSubServiceId = 0
            
            if self.isOutCallSelected{
                if self.arrSubSubServicesOutCall.count == 0 {
                    self.tblBookingList.isHidden = true
                    self.lblNoDataFound.isHidden = false
                    self.viewBigForHide.isHidden = true
                    self.lblServiceTitleForHide.isHidden = true
                }else{
                    self.tblBookingList.isHidden = false
                    self.lblNoDataFound.isHidden = true
                    self.viewBigForHide.isHidden = false
                    self.lblServiceTitleForHide.isHidden = false
                }
            }else{
                if self.arrSubSubServicesInCall.count == 0 {
                    self.tblBookingList.isHidden = true
                    self.lblNoDataFound.isHidden = false
                    self.viewBigForHide.isHidden = true
                    self.lblServiceTitleForHide.isHidden = true
                }else{
                    self.tblBookingList.isHidden = false
                    self.lblNoDataFound.isHidden = true
                    self.viewBigForHide.isHidden = false
                    self.lblServiceTitleForHide.isHidden = false
                }
            }
            
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.collectionStaff.reloadData()
            self.viewStaff.isHidden = true
            
            self.arrToShow.removeAll()
            self.collectionVw.reloadData()
            if self.arrToShow.count == 0 {
                self.lblNoSlotFound.isHidden = false
                self.lblSelectTime.isHidden = true
                self.collectionVw.isHidden = true
            }else{
                self.lblNoSlotFound.isHidden = true
                self.lblSelectTime.isHidden = false
                self.collectionVw.isHidden = false
            }
            self.manageTableViewHeight()
        }
        self.dropDown.show()
    }
    
    func configureCalendar(){
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        self.calendar.calendarHeaderView.isHidden = true
        
        /*
        //self.calendar.scope = .week
        self.calendar.scope = .month
        ////
        //self.calendar.scrollEnabled = false
        self.calendar.scrollEnabled = true
        ////
        */
        self.setCalenderScopeWeek()
        self.calendar.accessibilityIdentifier = "calendar"
    }
    
    func changeCalenderHeader(date : Date){
        /*let fmt = DateFormatter()
        fmt.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        fmt.dateFormat = "dd MMMM yyyy"
        //fmt.dateFormat = "MMMM yyyy"
        let strCalenderHeader = fmt.string(from: date)
        self.lblSelectedDete.text = strCalenderHeader
        */
        
        let fmt = DateFormatter()
        fmt.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        fmt.dateFormat = "dd MMMM yyyy"
        let strCalenderHeader = fmt.string(from: date)
        let newDateForCheck = fmt.string(from: newDate)
        
        if strCalenderHeader == newDateForCheck{
            self.lblSelectedDete.text = strCalenderHeader
        }else{
            fmt.dateFormat = "MMMM yyyy"
            let strCalenderHeader2 = fmt.string(from: date)
            self.lblSelectedDete.text = strCalenderHeader2
        }
    }
    
    private func animationReight(viewAnimation: UIView) {
        UIView.animate(withDuration: 0.0, animations: {
            viewAnimation.frame.origin.x = +viewAnimation.frame.width
        }) { (_) in
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                viewAnimation.frame.origin.x -= viewAnimation.frame.width
            })
        }
    }
    private func animationLeft(viewAnimation: UIView) {
        UIView.animate(withDuration: 0.3, animations: {
            viewAnimation.frame.origin.x = +viewAnimation.frame.width
        }) { (_) in
            UIView.animate(withDuration: 0.0, delay: 0, options: [.curveEaseIn], animations: {
                viewAnimation.frame.origin.x -= viewAnimation.frame.width
            })
        }
    }
    func setCalenderScopeWeek(){
        //        DispatchQueue.main.async{
        //            self.dataHeightConstraint.constant = self.dataHeightConstraint.constant - 200
        //            self.viewMain.frame.size.height = self.viewMain.frame.size.height - 200
        //            self.view.layoutIfNeeded()
        //        }
        self.calendar.scrollEnabled = false
        self.calendar.setScope(.week, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
        })
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count==1{
                self.slotViewHeightConstraint.constant = 264.0
            }else{
               self.slotViewHeightConstraint.constant = 150.0
            }
            self.view.layoutIfNeeded()
        }else{
            if self.arrSubSubServicesInCall.count==1{
                self.slotViewHeightConstraint.constant = 264.0
            }else{
                self.slotViewHeightConstraint.constant = 150.0
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func setCalenderScopeMonth(){
        //        DispatchQueue.main.async{
        //           self.dataHeightConstraint.constant = self.dataHeightConstraint.constant + 200
        //           self.viewMain.frame.size.height = self.viewMain.frame.size.height + 200
        //           self.view.layoutIfNeeded()
        //        }
        self.calendar.scrollEnabled = true
        self.calendar.setScope(.month, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
       
            self.slotViewHeightConstraint.constant = 150.0
            self.view.layoutIfNeeded()
    }
    
    @IBAction func toggleCalenderAction(sender: AnyObject) {
        if self.calendar.scope == .month {
            setCalenderScopeWeek()
        } else {
            setCalenderScopeMonth()
        }
    }
    
    @IBAction func btnPreviousAction(sender: AnyObject) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        if self.calendar.scope == .month{
            dateComponents.month = -1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = -1 // For prev button -1, For next button 1
        }
        self.animationLeft(viewAnimation: self.calendar)
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        if self.calendar.scope == .month{
          dateComponents.month = 1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = 1 // For prev button -1, For next button 1
        }
      self.animationReight(viewAnimation: self.calendar)
      calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
      self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
    
    @IBAction func btnTodayClicked(_ sender: UIButton) {
        self.newDate = Date()
        clickTodayAction(firstTime:true)
        clickTodayAction(firstTime:false)
        
        let strDate1 = self.dateFormatter.string(from: Date())
        let strNeeeDay = self.getDayFromSelectedDate(strDate: strDate1)
        let dayOld = Int(strNeeeDay)
        var strOpeningTime = ""
        for dict in self.arrHours{
            let day = dict["day"] as? Int
            let strStartTime = dict["startTime"] as? String
            let strEndTime = dict["endTime"] as? String
            if dayOld == day{
                if strOpeningTime.count != 0{
                    strOpeningTime = strOpeningTime + " & " + strStartTime! + " - " + strEndTime!
                }else{
                    strOpeningTime = strStartTime! + " - " + strEndTime!
                }
            }
        }
        var strDayName = ""
        if strNeeeDay == "0"{
            strDayName = "Monday"
        }else if strNeeeDay == "1"{
            strDayName = "Tuesday"
        }else if strNeeeDay == "2"{
            strDayName = "Wednesday"
        }else if strNeeeDay == "3"{
            strDayName = "Thursday"
        }else if strNeeeDay == "4"{
            strDayName = "Friday"
        }else if strNeeeDay == "5"{
            strDayName = "Saturday"
        }else if strNeeeDay == "6"{
            strDayName = "Sunday"
        }
        let strToday = self.dateFormatter.string(from: Date())
        if strDate1 == strToday{
            strDayName = "Today"
        }
        self.lblTodayText.text = strDayName + " :"
        if strOpeningTime.count>0{
            self.lblTodayOpeningTime.text = strOpeningTime
        }else{
            self.lblTodayOpeningTime.text = "Close"
        }
    }
    
    func clickTodayAction(firstTime:Bool){
        if self.objBookingServices.subSubServiceId == 0{
            objAppShareData.showAlert(withMessage: "Please select service", type: alertType.bannerDark, on: self)
            return
        }
        
        selectedIndex = -1
        isAnySlotSelected = false
        dateSelected = Date()
        //self.calendar.appearance.todaySelectionColor = UIColor.theameColors.skyBlueNewTheam
        self.calendar.select(Date())
        self.setCalenderScopeWeek()
        objActivity.startActivityIndicator()
        if firstTime == true{
        self.callWebserviceForGetStaff()
        self.getlatestSlotDataFor(dateSelected: Date())
        }
        //self.clearFirstServiceSelectedTimeDate()
    }
    
    //    func clearFirstServiceSelectedTimeDate(){
    //
    //        if self.arrSelectedService.count > 0{
    //            let objBookingServices : BookingServices = self.arrSelectedService[0]
    //
    //            if !objBookingServices.isBooked{
    //
    //                objBookingServices.bookingDate = ""
    //                objBookingServices.bookingTime = ""
    //                selectedIndex = -1
    //                self.collectionVw.reloadData()
    //                self.tblBookingList.reloadData()
    //
    //            }else{
    //
    //            }
    //        }
    //    }
    
}

// MARK:- FSCalendar delegate

extension BookingNewVC{
    
    /**
     Tells the delegate the calendar is about to change the bounding rect.
     */
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant = bounds.height
                self.view.layoutIfNeeded()
            }
        })
    }
    
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        
        /*
        //let myCalendar = Calendar(identifier: .gregorian)
        let weekDayToday = myCalendar.component(.weekday, from: Date())
        let todayString : String = self.dateFormatter.string(from:Date())
        let selectedString : String = self.dateFormatter.string(from:dateSelected)
        if !somedays.contains(weekDayToday) {
            if todayString == selectedString && self.isInitiallyCalendar{
                self.calendar.appearance.todaySelectionColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            }else{
                self.calendar.appearance.todaySelectionColor = UIColor.theameColors.skyBlueNewTheam
            }
        }else{
            self.calendar.appearance.todaySelectionColor = UIColor.theameColors.skyBlueNewTheam
        }
        */
        let todayString : String = self.dateFormatter.string(from:Date())
        let selectedString : String = self.dateFormatter.string(from:date)
        if !somedays.contains(weekDay) && todayString != selectedString
        {
            return UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {

        let dateString : String = self.dateFormatter.string(from:date)
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        let day = myCalendar.component(.day, from: date)
        //let today = Date()
        //let weekDaytoday = myCalendar.component(.weekday, from: today)
        let todayString : String = self.dateFormatter.string(from:Date())
        let selectedString : String = self.dateFormatter.string(from:date)
        if !somedays.contains(weekDay) && todayString != selectedString {
            return UIColor.white
        }else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! == date2!){
            self.getlatestSlotDataFor(dateSelected: Date())
        }else{
            self.getlatestSlotDataFor(dateSelected: date)
        }
        self.isInitiallyCalendar = false
        selectedIndex = -1
        isAnySlotSelected = false
        dateSelected = date
        objActivity.startActivityIndicator()
        self.callWebserviceForGetStaff()
        //self.clearFirstServiceSelectedTimeDate()
        
        ////// let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        calendar.setCurrentPage(date, animated: true)
        //// Himanshu
        self.changeCalenderHeader(date: date1!)
        ////
        self.setCalenderScopeWeek()
        
        //let strNeeeDate = dateFormatter.string(from: Date())
        let strNeeeDay = self.getDayFromSelectedDate(strDate: strDate1)
        let dayOld = Int(strNeeeDay)
        var strOpeningTime = ""
        for dict in self.arrHours{
            let day = dict["day"] as? Int
            let strStartTime = dict["startTime"] as? String
            let strEndTime = dict["endTime"] as? String
            if dayOld == day{
                if strOpeningTime.count != 0{
                    strOpeningTime = strOpeningTime + " & " + strStartTime! + " - " + strEndTime!
                }else{
                    strOpeningTime = strStartTime! + " - " + strEndTime!
                }
            }
        }
        var strDayName = ""
        if strNeeeDay == "0"{
            strDayName = "Monday"
        }else if strNeeeDay == "1"{
            strDayName = "Tuesday"
        }else if strNeeeDay == "2"{
            strDayName = "Wednesday"
        }else if strNeeeDay == "3"{
            strDayName = "Thursday"
        }else if strNeeeDay == "4"{
            strDayName = "Friday"
        }else if strNeeeDay == "5"{
            strDayName = "Saturday"
        }else if strNeeeDay == "6"{
            strDayName = "Sunday"
        }
        let strToday = self.dateFormatter.string(from: Date())
        if strDate1 == strToday{
            strDayName = "Today"
        }
        self.lblTodayText.text = strDayName + " :"
        if strOpeningTime.count>0{
            self.lblTodayOpeningTime.text = strOpeningTime
        }else{
            self.lblTodayOpeningTime.text = "Close"
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    
    /**
     Close past dates in FSCalendar
     */
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool{
        if self.objBookingServices.subSubServiceId == 0{
            objAppShareData.showAlert(withMessage: "Please select service", type: alertType.bannerDark, on: self)
            return false
        }
        
        let strDate1 = self.dateFormatter.string(from: date)
        self.newDate = date
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! < date2!){
            objAppShareData.showAlert(withMessage: "You can't select previous date for booking", type: alertType.bannerDark, on: self)
            return false
        }else{
            return true
        }
    }
    
    @IBAction func btnConfirmBookingAction(_ sender: UIButton) {
        /*
         self.view.endEditing(true)
         
         if self.arrSelectedService.count > 0{
         
         let objBookingServices : BookingServices = self.arrSelectedService[0]
         
         if !objBookingServices.isBooked{
         
         if objBookingServices.bookingDate != "" && objBookingServices.bookingTime != ""{
         
         self.createDictForBookingSlot(objBookingServices: objBookingServices, isFromAddMoreButton: false)
         }else{
         objAppShareData.showAlert(withMessage: "Please select service date and time", type: alertType.bannerDark, on: self)
         }
         
         }else{
         gotoBookingListVC()
         }
         }else{
         objAppShareData.showAlert(withMessage: "No service selected", type: alertType.bannerDark, on: self)
         }
         */
    }
    func getDayFromSelectedDate(strDate:String)-> String{
        
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        switch weekDay {
        case 1:
            self.calendar.firstWeekday = 6+2
            return "6"//"Sun"
        case 2:
            self.calendar.firstWeekday = 0+2
            return "0"//Mon"
        case 3:
            self.calendar.firstWeekday = 1+2
            return "1"//Tue"
        case 4:
            self.calendar.firstWeekday = 2+2
            return "2"//"Wed"
        case 5:
             self.calendar.firstWeekday = 3+2
            return "3"//"Thu"
        case 6:
            self.calendar.firstWeekday = 4+2
            return "4"//"Fri"
        case 7:
            self.calendar.firstWeekday = 5+2
            return "5"//"Sat"
        default:
            self.calendar.firstWeekday = 0
            //"Error fetching days"
            return "Day"
        }
    }
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func getlatestSlotDataFor(dateSelected : Date){
        
        var str_latitude = ""
        var str_longitude = ""
        if objArtistDetails.isOutCallSelected{
            str_latitude =  objLocationManager.strlatitude ?? ""
            str_longitude = objLocationManager.strlongitude ?? ""
        }else{
            str_latitude =  objArtistDetails.latitude
            str_longitude = objArtistDetails.longitude
        }
        
        str_latitude =  objLocationManager.strlatitude ?? ""
        str_longitude = objLocationManager.strlongitude ?? ""
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: dateSelected)
        
        ////
        //let newDate = dateFormatter.date(from: strCurrentDate)
        //let strNewDate = dateFormatter.string(from: newDate!)
        ////
        //let strSelectedDay = self.getDayFromSelectedDate(strDate: strNewDate) //"0" = Mon"
        
        let strSelectedTime = self.getTimeFromDate(strDate: strCurrentDate) //"hh:mm a"
        let strSelectedDate = self.getFormattedDateFromDate(strDate: strCurrentDate) //"yyyy-MM-dd"
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strSelectedDate) //"0" = Mon"
        
        let totalMin = getTotalServiceTime()
        let strTotalServiceTime = "00:\(totalMin)"
        
        //let totalMinBiz = getTotalServiceTimeForBiz()
        //let strTotalServiceTimeBiz = "00:\(totalMinBiz)"
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        
        let strStaffId  = objBookingServices.staffId == 0 ? "0" : "\(objBookingServices.staffId)"
        
        var str_bookingId = ""
        var str_type = ""
        var bookStaffId = ""
        
        //        if objAppShareData.isFromEditBooking  && !isServiceAlreadyAdded {
        //
        //            str_type = ""
        //            str_bookingId = ""
        //            bookStaffId = ""
        //
        //        }else if objAppShareData.isFromEditBooking {
        
        //  str_type = "edit"
        
        if self.arrSelectedService.count > 0{
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            str_bookingId = "\(objBookingServices.bookingId)"
        }
        if str_bookingId == "0"{
            str_bookingId = ""
            str_type = ""
        }
        bookStaffId = objBookingServices.bookStaffId == 0 ? "" : "\(objBookingServices.bookStaffId)"
        //  }
        
        
        let parameters : Dictionary = [
            "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId,
            "artistId" : objArtistDetails._id,
            "day" : strSelectedDay,
            "date" : strSelectedDate,
            "currentTime" : strSelectedTime,
            "serviceTime" : strTotalServiceTime,
            //"serviceTimeBiz" : strTotalServiceTimeBiz,
            //"serviceTime" : "03:30",
            "userId" : strUserId,
            "latitude" : str_latitude,
            "longitude" : str_longitude,
            "businessType" : objArtistDetails.businessType,
            "staffId" : strStaffId ,  //>> artistId
            
            "bookingTime" : "",
            "bookingDate" : "",
            "bookingCount": "",
            "type" : str_type, //
            "bookingId" : str_bookingId, //
            "bookStaffId": bookStaffId, //old staff id id edit
            ] as [String : Any]
        print(parameters)
        //objActivity.startActivityIndicator()
        self.callWebserviceForGet_ArtistTimeSlot(dict: parameters)
    }
    
    func getTotalServiceTime()-> Int{
        var totalMin = 0
        if self.arrSelectedService.count > 0{
        let objBookingServices : BookingServices = self.arrSelectedService[0]
        if self.isOutCallSelected{
            totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.outCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
        }else{
            totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.inCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
        }
        }
        return totalMin
    }
    func getTotalServiceTimeForBiz()-> Int{
        var totalMin = 0
        if self.arrSelectedService.count > 0{
        let objBookingServices : BookingServices = self.arrSelectedService[0]
        if self.isOutCallSelected{
            totalMin = getTotalServiceTime(strPreprationTime: "0", strComplitionTime: objBookingServices.completionTime)
        }else{
            totalMin = getTotalServiceTime(strPreprationTime: "0", strComplitionTime: objBookingServices.completionTime)
        }
        }
        return totalMin
    }
    func getEndTimeForStartTime(strStartTime:String)-> String{
        
        var strEndTime : String = ""
        let totalMin = getTotalServiceTime()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = dateFormatter.date(from: strStartTime){
            // Start: Optional(2000-01-01 19:00:00 +0000)
            let calendar = Calendar.current
            if let date1 = calendar.date(byAdding: .minute, value: totalMin, to: date){
                strEndTime = dateFormatter.string(from: date1)
            }
        }
        return strEndTime
    }
    func getEndTimeForStartTimeForBiz(strStartTime:String)-> String{
        
        var strEndTime : String = ""
        let totalMin = getTotalServiceTimeForBiz()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = dateFormatter.date(from: strStartTime){
            // Start: Optional(2000-01-01 19:00:00 +0000)
            let calendar = Calendar.current
            if let date1 = calendar.date(byAdding: .minute, value: totalMin, to: date){
                strEndTime = dateFormatter.string(from: date1)
            }
        }
        return strEndTime
    }
    func getTotalServiceTime(strPreprationTime:String, strComplitionTime:String)-> Int{
        let preprationTime :Int = getTimeInMinutFrom(strTime: strPreprationTime)
        let complitionTime :Int = getTimeInMinutFrom(strTime: strComplitionTime)
        let totalMin = preprationTime + complitionTime
        return totalMin
    }
    
    func getTimeInMinutFrom(strTime:String)-> Int{
        var TotalMin = 0
        var arrTime = strTime.components(separatedBy: ":")
        if arrTime.count >= 2{
            let strHours = arrTime[0]
            let strMin = arrTime[1]
            TotalMin =  Int(strHours)! * 60 + Int(strMin)!
        }
        return TotalMin
    }
    
    func callWebserviceForGet_ArtistTimeSlot(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.artistTimeSlot, params: dict  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    self.objBookingServices.bookingId = 0
                    
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.objBookingServices.bookingId = 0
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.objBookingServices.bookingId = 0
        if let bookingId = response["bookingId"] as? Int{
            self.objBookingServices.bookingId = bookingId
        }
        self.arrToShow.removeAll()
        if let arr = response["timeSlots"] as? [String]{
            for strTimeSlot in arr{
                let objTimeSlot = TimeSlot.init(isSelected: false, strTimeSlot: strTimeSlot)
                self.arrToShow.append(objTimeSlot!);
            }
        }
        
        ////
        if objAppShareData.isBookingFromService{
            for i in 0..<self.arrToShow.count{
                let objSlot = self.arrToShow[i]
                if objSlot.strTimeSlot == self.objServiceForBooking.strSlotForEdit{
                    objSlot.isSelected = true
                    selectedIndex = i
                    self.objServiceForBooking.strSlotForEdit = ""
                }
            }
            objAppShareData.isBookingFromService = false
        }else{
            selectedIndex = -1
            isAnySlotSelected = false
        }
        ////
        
        self.viewTimeSlots.isHidden = false
        self.collectionVw.reloadData()
        if self.arrToShow.count == 0 {
            self.lblNoSlotFound.isHidden = false
            self.lblSelectTime.isHidden = true
            self.collectionVw.isHidden = true
        }else{
            self.lblNoSlotFound.isHidden = true
            self.lblSelectTime.isHidden = false
            self.collectionVw.isHidden = false
        }
    }
    
    func getTimeFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "hh:mm a"
        let strTime = formatter.string(from: todayDate)
        
        return strTime
    }
    func getFormattedDateFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: todayDate)
        
        return strDate
    }
    
    func callWebserviceForGetStaff(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objActivity.startActivityIndicator()
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: dateSelected)
        let strSelectedDate = self.getFormattedDateFromDate(strDate: strCurrentDate) //"yyyy-MM-dd"
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strSelectedDate)
        
        let dicParam = [
            "day":strSelectedDay,
            "staffId":"",
            "businessId":self.objArtistDetails._id,
            "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId
            ] as [String : Any]
        print(dicParam)
        objServiceManager.requestPostForJson(strURL: WebURL.serviceStaff, params: dicParam, success: { response in
            print(response)
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objActivity.stopActivity()
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    self.saveDataStaff(dict:response)
                }else{
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveDataStaff(dict:[String:Any]){
        //        let dictService = dict["serviceInfo"] as! [String:Any]
        //        self.objService.serviceId = (dictService["serviceId"] as? Int)!
        //        self.objService.subServiceId = (dictService["subserviceId"] as? Int)!
        //        self.objService.artistId = (dictService["artistId"] as? Int)!
        
        var myUserId = 0
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                myUserId = userId
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                myUserId = id
            }
        }
        
        let arrStaff = dict["staffInfo"] as! [[String:Any]]
        for dictStaff in arrStaff{
            let objStaff = StaffServiceNew.init(dict: dictStaff)
            if myUserId != objStaff.staffId{
                let strType = dictStaff["bookingType"] as? String ?? ""
                if strType == "Incall"{
                    self.arrInCallStaff.append(objStaff)
                }else if strType == "Outcall"{
                    self.arrOutCallStaff.append(objStaff)
                }else if strType == "Both"{
                    self.arrInCallStaff.append(objStaff)
                    self.arrOutCallStaff.append(objStaff)
                }
            }
        }
        print(self.arrInCallStaff.count)
        print(self.arrOutCallStaff.count)
        if self.isOutCallSelected{
            if self.arrOutCallStaff.count == 1{
                self.arrOutCallStaff.removeAll()
                self.viewStaff.isHidden = true
            }else{
                if objAppShareData.isBookingFromService && self.objServiceForBooking.staffIdForEdit > 0{
                    for i in 0..<arrOutCallStaff.count{
                        let obj = arrOutCallStaff[i]
                        if obj.staffId == self.objServiceForBooking.staffIdForEdit{
                            obj.isSelected = true
                            self.objBookingServices.price = obj.outCallPrice
                            if i == 0{
                                self.objBookingServices.staffId = 0
                            }else{
                                self.objBookingServices.staffId = obj.staffId
                            }
                            self.objServiceForBooking.staffIdForEdit = 0
                        }else{
                            //obj.isSelected = false
                        }
                    }
                }else{
                    let obj = arrOutCallStaff[0]
                    obj.isSelected = true
                    self.objBookingServices.price = obj.outCallPrice
                }
                self.viewStaff.isHidden = false
            }
        }else{
            if self.arrInCallStaff.count == 1{
                self.arrInCallStaff.removeAll()
                self.viewStaff.isHidden = true
            }else{
                if objAppShareData.isBookingFromService && self.objServiceForBooking.staffIdForEdit > 0{
                    for i in 0..<arrInCallStaff.count{
                        let obj = arrInCallStaff[i]
                        if obj.staffId == self.objServiceForBooking.staffIdForEdit{
                            obj.isSelected = true
                            self.objBookingServices.price = obj.inCallPrice
                            if i == 0{
                                self.objBookingServices.staffId = 0
                            }else{
                                self.objBookingServices.staffId = obj.staffId
                            }
                            self.objServiceForBooking.staffIdForEdit = 0
                        }else{
                            //obj.isSelected = false
                        }
                    }
                }else{
                    let obj = arrInCallStaff[0]
                    obj.isSelected = true
                    self.objBookingServices.price = obj.inCallPrice
                }
                self.viewStaff.isHidden = false
            }
        }
        self.collectionStaff.reloadData()
        self.getlatestSlotDataFor(dateSelected: dateSelected)
    }
    
    func callWebserviceForBookArtist(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if self.isOutCallSelected{
            self.strInCallOrOutCallFromService = "Out Call"
        }else{
            self.strInCallOrOutCallFromService = "In Call"
        }
        objActivity.startActivityIndicator()
        self.objBookingServices.endTime = getEndTimeForStartTime(strStartTime: self.objBookingServices.bookingTime)
        var endTimeBiz = getEndTimeForStartTimeForBiz(strStartTime: self.objBookingServices.bookingTime)
        /*startTime:
         endTime:
         artistId:
         staff:
         userId:
         serviceId:
         subServiceId:
         artistServiceId:
         bookingDate:
         serviceType:
         price:
         bookingId:*/
        var serviceType = 0
        var price = 0.0
        price = self.objBookingServices.price
        if self.isOutCallSelected{
            serviceType = 2
            //price = self.objBookingServices.objSubSubService.outCallPrice
        }else{
            serviceType = 1
            //price = self.objBookingServices.objSubSubService.inCallPrice
        }
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"// "yyyy-MM-dd"
        let strDate = formatter.string(from: dateSelected)
        let dicParam = [
            "startTime":self.objBookingServices.bookingTime,
            "endTime":self.objBookingServices.endTime,
            "endTimeBiz":endTimeBiz,
            "artistId":self.objArtistDetails._id,
            "staff":self.objBookingServices.staffId,
            "serviceId":self.objBookingServices.objSubSubService.serviceId,
            "subServiceId":self.objBookingServices.objSubSubService.subServiceId,
            "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId,
            "bookingDate":self.objBookingServices.bookingDateForServer,
            "serviceType":serviceType,
            "price":price,
            "bookingId":self.objBookingServices.bookingId,
            ] as [String : Any]
        print(dicParam)
        objServiceManager.requestPostForJson(strURL: WebURL.bookArtist, params: dicParam, success: { response in
            print(response)
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == "success"{
                    objAppShareData.startTimerForHoldBookings()
                    objActivity.stopActivity()
                    self.objBookingServices.bookingTime = ""
                    self.goToConfirmBookingScreen()
                }else{
                    //objAppShareData.showAlert(withMessage: "Service already added" , type: alertType.bannerDark,on: self)
                    objAppShareData.showAlert(withMessage: "Time slot not available!" , type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func goToConfirmBookingScreen(){
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"ConfirmBookingVC") as? ConfirmBookingVC{
            objVC.hidesBottomBarWhenPushed = true
            objVC.isOutCallSelectedConfirm = self.isOutCallSelected
            objVC.objArtistDetails = self.objArtistDetails
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func showAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to permanently remove all selected services?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.callWebserviceFor_deleteAllbooking()
        }
        action1.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let action2 = UIAlertAction(title: "Cancel", style: .destructive) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action2)
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callWebserviceFor_deleteAllbooking() {
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        let parameters : Dictionary = [
            "userId" : strUserId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteUserBookService, params: parameters  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                //let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.stopTimerForHoldBookings()
                    self.navigationController?.popViewController(animated: false)
                }else{
                }
            }
        }){ error in
        }
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        
        let objUser = self.objArtistDetails
        
        if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
        return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
        self.view.isUserInteractionEnabled = true
        if response["status"] as? String ?? "" == "success"{
        var strId = ""
        var strType = ""
        if let dictUser = response["userDetail"] as? [String : Any]{
        let myId = dictUser["_id"] as? Int ?? 0
        strId = String(myId)
        strType = dictUser["userType"] as? String ?? ""
        }
        let dic = [
        "tabType" : "people",
        "tagId": strId,
        "userType":strType,
        "title": objUser.userName
        ] as [String : Any]
        self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
        }
        }) { error in
        self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        var dictTemp : [AnyHashable : Any]?
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            var myId = 0
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myId = userInfo["_id"] as? Int ?? 0
            }
            if myId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

