    //
//  BusinessHoursVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit
import Foundation
    
import HCSStarRatingView

fileprivate enum day:Int {
    
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    
    var string: String {
        return String(describing: self)
    }
}

class BusinessHoursVC: UIViewController {
    
    @IBOutlet weak var tblBusinessHours : UITableView!
    
    var objArtistDetails = ArtistDetails(dict: ["":""])
    
    //user detail outlat
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOpeningTime: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var lblNoRecord: UIView!
    
    fileprivate var arrOpeningTimes = [openingTimes]()
    
    fileprivate var tblTag = Int()
    fileprivate var timeTag = Int()
}
    
//MARK: - View hirarchy
extension BusinessHoursVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
        self.lblNoRecord.isHidden = true
        self.updateArtistInfo()
        makeDeafultsDay()
        self.parseResponce(objArtistDetails.responce)
    }
    
    func updateArtistInfo(){
        
        if objArtistDetails.profileImage != "" {
            if let url = URL(string: objArtistDetails.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistDetails.userName
        self.viewRating.value = CGFloat(objArtistDetails.ratingCount)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
    
//MARK: -Local Methods
fileprivate extension BusinessHoursVC{
    
    func makeDeafultsDay() {
        
        self.arrOpeningTimes.removeAll()
        let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        for day in arrDays{
            let objTimeSloat = timeSloat.init(startTime:" --:-- ",endTime:" --:-- ")
            let arr = [objTimeSloat] as! [timeSloat]
            var objOpenings:openingTimes?
            if day == "Saturday" || day == "Sunday"{
                let ar = [timeSloat]()
                objOpenings = openingTimes.init(open: false, day: day, times: ar)
            }else{
                objOpenings = openingTimes.init(open: false, day: day, times: arr)
            }
            self.arrOpeningTimes.append(objOpenings!)
        }
        self.tblBusinessHours.reloadData()
    }
    
    //MARK: - Make String for API
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        for (index,openings) in arrBusinessHours.enumerated(){
            for time in openings.arrTimeSloats{
                let dic = ["day":index,
                           "startTime":time.strStartTime,
                           "endTime":time.strEndTime,
                           "status":Int(truncating:NSNumber.init(value: openings.isOpen))] as [String : Any]
                arr.append(dic)
            }
        }
        
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!
            return dataString
        }
        return dataString
    }
    func removeTimeObjectsArray(){
        for obj in self.arrOpeningTimes{
            obj.arrTimeSloats.removeAll()
            obj.isOpen = false
        }
    }
}
    
//MARK : - Save Methods
fileprivate extension BusinessHoursVC{
    
    func parseResponce(_ dicResponse:[String:Any]){
 
        objArtistDetails.businessName = dicResponse["businessName"] as? String ?? ""
        //if let arr = dicResponse["openingTime"] as? [[String:Any]]{
        if let arr = dicResponse["busineshours"] as? [[String:Any]]{
            if arr.count>0{
                self.removeTimeObjectsArray()
                for dic in arr{
                    if let d = dic["day"] as? Int{
                        for (index,openings) in self.arrOpeningTimes.enumerated(){
                            if d == index{
                                openings.isOpen = true
                                openings.strDay = (day(rawValue:index)?.string)!
                                let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as? String ?? "",endTime:dic["endTime"] as? String ?? "")
                                openings.arrTimeSloats.append(objTimeSloat!)
                            }
                        }
                    }
                }
            }
        }
        
        //To only show working days
        var arrTemp = [openingTimes]()
        if self.arrOpeningTimes.count > 0 {
            for obj in self.arrOpeningTimes{
                //if obj.isOpen{
                    arrTemp.append(obj)
                //}
            }
            self.arrOpeningTimes.removeAll()
            self.arrOpeningTimes = arrTemp
        }
        
        self.tblBusinessHours.reloadData()
        
        if self.arrOpeningTimes.count == 0 {
            self.lblNoRecord.isHidden = false
        }else{
            self.lblNoRecord.isHidden = true
        }
    }
    
    func saveData(_ dicResponse:[String:Any]){
     
       let arr = dicResponse["businessHour"] as! [[String:Any]]
       if arr.count>0{
            self.removeTimeObjectsArray()
            for dic in arr{
                if let d = dic["day"] as? Int{
                    for (index,openings) in self.arrOpeningTimes.enumerated(){
                        if d == index{
                            openings.isOpen = true
                            openings.strDay = (day(rawValue:index)?.string)!
                            let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as? String ?? "",endTime:dic["endTime"] as? String ?? "")
                            openings.arrTimeSloats.append(objTimeSloat!)
                        }
                    }
                }
            }
            self.tblBusinessHours.reloadData()
        }
    }
}
    
//MARK : - IBAction
fileprivate extension BusinessHoursVC{
    
    @IBAction func btnIsSelecteDay(_ sender:UIButton){
       
        let section = 0
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: section)
        let cell = tblBusinessHours.cellForRow(at:indexPath) as! BusinessHoursTableCell
        let objOpening = self.arrOpeningTimes[row]
        if objOpening.isOpen{
            objOpening.isOpen = false
            cell.btnIsOpen?.isSelected = false
            objOpening.arrTimeSloats.removeAll()
        }else{
            cell.btnIsOpen?.isSelected = true
            objOpening.isOpen = true
            let objTimeSloat = timeSloat.init(startTime: "10:00 AM",endTime: "07:00 PM")
            objOpening.arrTimeSloats.append(objTimeSloat!)
        }
        self.tblBusinessHours.reloadData()
    }
    
    @IBAction func btnLogout(_ sender:UIButton){
        appDelegate.logout()
    }
}
    
//Webservices
fileprivate extension BusinessHoursVC{
    
    func callWebserviceForGetBusinessProfile(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let parameters : Dictionary = [
            "businessType":objArtistDetails.businessType,
            "artistId":objArtistDetails._id,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.artistDetail, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
               
                if strStatus == k_success{
                   
                    if let dict = response["artistDetail"] as? [String : Any]{
                        self.parseResponce(dict)
                    }
                    
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
          }
        }){ error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
    
//MARK: - UITableViewDelegate and UITableViewDataSource
extension BusinessHoursVC:UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOpeningTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblBusinessHours.dequeueReusableCell(withIdentifier: "BusinessHoursTableCell") as! BusinessHoursTableCell
        
        let objOpenings = self.arrOpeningTimes[indexPath.row]
        cell.lblDay?.text = objOpenings.strDay
        cell.btnIsOpen?.isSelected = objOpenings.isOpen
        cell.arrTime = objOpenings.arrTimeSloats
        cell.delegate = self
        if objOpenings.isOpen{
            cell.btnAdd?.isHidden = false
            cell.lblNotWorking?.isHidden = true
        }else{
            cell.btnAdd?.isHidden = true
            cell.lblNotWorking?.isHidden = false
        }
        
        cell.tblTime?.tag = indexPath.row
        cell.btnIsOpen?.tag = indexPath.row
        cell.btnAdd?.tag = indexPath.row
        //
        cell.tblTimeHeight?.constant = CGFloat((objOpenings.arrTimeSloats.count * 26)+26)
        cell.tblTime?.reloadData()
        return cell
    }
}

//MARK: - BusinessHoursTableCellDelegate
extension BusinessHoursVC : BusinessHoursTableCellDelegate{
    
    func removeTimeSloat(withSenderTable tableView: UITableView, timeArrayIndex: Int) {
    }
    
    func selectedTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int){
    }
}

fileprivate extension BusinessHoursVC {
        
    @IBAction func btnBackAction(_ sender: UIButton) {     
        dismiss(animated: true, completion: nil)
    }
}
