//
//  VoucherListVC.swift
//  MualabBusiness
//
//  Created by Mac on 31/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class VoucherListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewDeletePopup: UIView!
    @IBOutlet weak var lblNodatafound: UIView!
    @IBOutlet weak var tblVouchewList: UITableView!

    var arrVoucher = [VoucherModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DelegateCalling()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceForVoucherList()
        self.tblVouchewList.reloadData()
        self.viewDeletePopup.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - custome method
extension VoucherListVC{
    func DelegateCalling(){
        self.viewDeletePopup.isHidden = true
        self.tblVouchewList.delegate = self
        self.tblVouchewList.dataSource = self
  }
}

//MARK: - custome method
extension VoucherListVC{

@IBAction func btnBackAction(_ sender: UIButton) {
 self.navigationController?.popViewController(animated: true)
}
    
}
extension VoucherListVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrVoucher.count == 0{
            self.lblNodatafound.isHidden = false
        }else{
            self.lblNodatafound.isHidden = true
        }
        
        return arrVoucher.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objVoucher = self.arrVoucher[indexPath.row]
        objAppShareData.strVoucherCode = objVoucher.voucherCode
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellVoucherList", for: indexPath) as? CellVoucherList {
            let objStaff = arrVoucher[indexPath.row]
            cell.lblVouchewCode.text = objStaff.voucherCode.uppercased()
            cell.lblExpiryDate.text = objStaff.endDate
            if objStaff.discountType == "1"{
                cell.lblDiscount.text = "£"+objStaff.amount
            }else{
                cell.lblDiscount.text = objStaff.amount+"%"
            }
            let a = objStaff.endDate.components(separatedBy: "/")
            if a.count > 2{
                cell.lblExpiryDate.text = a[2]+"/"+a[1]+"/"+a[0]
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
  
}

extension VoucherListVC{

func callWebserviceForVoucherList(){
    if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
        return
    }
    objActivity.startActivityIndicator()
    let parameters : Dictionary = [
        "artistId" : objAppShareData.selectedOtherIdForProfile
        ] as [String : Any]
    objServiceManager.requestPost(strURL: WebURL.voucherList, params: parameters  , success: { response in
        self.arrVoucher.removeAll()
        objServiceManager.StopIndicator()
        let keyExists = response["responseCode"] != nil
        if  keyExists {
            sessionExpireAlertVC(controller: self)
        }else{
            let strStatus =  response["status"] as? String ?? ""
            if strStatus == k_success{
                self.parseResponce(response:response)
            }else{
                self.tblVouchewList.reloadData()
                // let msg = response["message"] as! String
                //objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }
    }){ error in
        self.tblVouchewList.reloadData()
        objServiceManager.StopIndicator()
        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
}
    
    
    func parseResponce(response:[String : Any]){
        if let arr = response["data"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = VoucherModel.init(dict: dictArtistData)
                    self.arrVoucher.append(objArtistList!)
                }
                self.tblVouchewList.reloadData()
            }
            self.tblVouchewList.reloadData()
        }
    }

}
