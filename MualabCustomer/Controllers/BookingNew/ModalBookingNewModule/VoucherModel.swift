//
//  CellCrearGroup.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/8/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class VoucherModel: NSObject {
    var _id : String = "0"
    var artistId: String = "0"
    var voucherCode  = ""
    
    var amount  = ""
    
    var discountType: String = ""
    
    var endDate: String = ""
    var startDate: String = ""
    var status  = ""
    
    
    init?(dict : [String:Any]){
        
        if let id = dict["_id"] as? Int{
            self._id = String(id)
        }else if let id = dict["_id"] as? String{
            self._id = id
        }
        if let certificateImage1 = dict["voucherCode"] as? String{
            self.voucherCode = certificateImage1
        }
        
        if let artistId1 = dict["artistId"] as? Int{
            self.artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            self.artistId = artistId1
        }
        
        if let artistId1 = dict["amount"] as? Int{
            self.amount = String(artistId1)
        }else if let artistId1 = dict["amount"] as? String{
            self.amount = artistId1
        }
        
        if let artistId1 = dict["discountType"] as? Int{
            self.discountType = String(artistId1)
        }else if let artistId1 = dict["discountType"] as? String{
            self.discountType = artistId1
        }
        
        if let artistId1 = dict["status"] as? Int{
            self.status = String(artistId1)
        }else if let artistId1 = dict["status"] as? String{
            self.status = artistId1
        }
        
        
        if let crd1 = dict["startDate"] as? String{
            self.startDate = crd1
        }
        if let crd1 = dict["endDate"] as? String{
            self.endDate = crd1
        }
        
        
        let a = self.endDate.components(separatedBy: "-")
        if a.count > 2{
            self.endDate = a[0]+"/"+a[1]+"/"+a[2]
        }
        
        let b = self.startDate.components(separatedBy: "-")
        if b.count > 2{
            self.startDate = b[0]+"/"+b[1]+"/"+b[2]
        }
    }
}

class AddStaffHours {
    var day : Int = 0
    var endTime:String = ""
    var startTime:String = ""
    
    init(dict: [String : Any]){
        day = dict["day"] as? Int ?? 0
        if let days = dict["day"] as? String{
            day = Int(days)!
        }
        if let days = dict["day"] as? Int{
            day = days
        }
        endTime = dict["endTime"] as? String ?? ""
        startTime = dict["startTime"] as? String ?? ""
    }
}

class BottomSheetModal: NSObject {
    var _id : Int = 0
    var serviceId: Int = 0
    var subServiceId: Int = 0
    var subSubServiceId: Int = 0
    var strItemName: String = ""
}
