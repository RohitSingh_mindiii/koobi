//
//  CellInvitationList.swift
//  MualabBusiness
//
//  Created by Mac on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellInvitationList: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource{
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var Collection: UICollectionView!
    var arrServices = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.Collection.delegate = self
        self.Collection.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataInCollection(obj:ArtistDetails){
        arrServices.removeAll()
        //arrServices = obj.arrServiceNames
        self.Collection.reloadData()
    }
    func setDataInCollectionData(obj:[String]){
        arrServices.removeAll()
            arrServices = obj
        self.Collection.reloadData()
    }
    
}

//MARK:- Collection view Delegate Methods
extension CellInvitationList:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrServices.count >= 3 {
            return 2
        }else{
            return arrServices.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        cell.lblUserName.text = obj
        let a = cell.lblUserName.layer.frame.height
        cell.lblUserName.layer.cornerRadius = 11
        cell.lblUserName.layer.masksToBounds = true
        cell.lblBorder.layer.cornerRadius = 12
        cell.lblBorder.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 20
        cellWidth = CGFloat(self.Collection.frame.size.width-2)
        
        let a = arrServices[indexPath.row]
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 12)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+20
        cellHeight = 24
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}


