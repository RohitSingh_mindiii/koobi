//
//  ServicesTableCell.swift
//  MualabBusiness
//
//  Created by Mac on 20/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ServicesTableCell: UITableViewCell {
    

    @IBOutlet weak var lblServiceName:UILabel!
    @IBOutlet weak var viewStatusManage:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            self.backgroundColor = UIColor.theameColors.pinkColor
            self.lblServiceName.textColor = UIColor.white
        }else{
            self.backgroundColor = UIColor.white
            self.lblServiceName.textColor = UIColor.black
        }
        // Configure the view for the selected state
    }

}
class BusinessTypeTableCell: UITableViewCell {
    
    
    @IBOutlet weak var lblServiceName:UILabel!
    @IBOutlet weak var viewStatusManage:UIView!
    @IBOutlet weak var imgCheck:UIImageView!
    @IBOutlet weak var btnChackUnchack:UIButton!
    @IBOutlet weak var lblUperLine:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func cellHiddenImage(){
        self.imgCheck.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
        }else{
          }
     }
}

class SubServicesTableCell: UITableViewCell {
    var arrService = [ModelServicesList]()
    @IBOutlet weak var imgDote:UIImageView!

    @IBOutlet weak var lblServiceName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblOutcallPrice:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblTop:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
     override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
        }else{
        }
    }
}


class BusinessListTypeTableCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
 
    
    @IBOutlet weak var tblServiceList:UITableView!
    @IBOutlet weak var lblCategoryName:UILabel!
    
    @IBOutlet weak var heightOfCell:NSLayoutConstraint!
    var arrService = [ModelServicesList]()
    var arrBusinessCategory = [String]()
    var serType = ""
    override func awakeFromNib() {
        super.awakeFromNib()
 
     }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("row count = \(self.arrService.count)")
        return self.arrService.count
    }
    
    
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    print("hello")
    let cell = tableView.dequeueReusableCell(withIdentifier: "SubServicesTableCell", for: indexPath) as! SubServicesTableCell
   // {
        let obj = self.arrService[indexPath.row]
    cell.lblTop.isHidden = true
    if indexPath.row == 0{
        cell.lblTop.isHidden = false
    }
        cell.lblServiceName.text = obj.serviceName.capitalized

         let arr = obj.time.split(separator: ":")
        cell.lblTime.text = obj.time+" min"

        if arr.count > 1{
            var time = "00:00"
            if arr[0] == "00"{
                cell.lblTime.text = arr[1]+" min"
            }else{
                cell.lblTime.text = arr[0]+" hr "+arr[1]+" min"
            }
    }
    
    let array = obj.price.components(separatedBy: ".")
    ////
    let doubleStr = String(format: "%.2f", Double(obj.price) ?? 00.00)
    ////
    //cell.lblPrice.text = "£"+obj.price
    cell.lblPrice.text = "£"+doubleStr
    
    if array.count == 1{
        cell.lblPrice.text = "£"+obj.price+".00"
    }
    if array.count == 2{
        if array[1].count == 1{
            "£"+obj.price+"0"
        }
    }
    
        cell.lblOutcallPrice.isHidden = true
        if obj.bookingType == "Both"{
            if serType == "I"{
                let arrayInc = obj.incallPrice.components(separatedBy: ".")
                if arrayInc.count == 1{
                    cell.lblPrice.text = "£"+obj.incallPrice+".00"
                }else{
                    cell.lblPrice.text = "£"+obj.incallPrice
                }
            }else if serType == "O"{
                let arrayOuc = obj.outcallPrice.components(separatedBy: ".")
                if arrayOuc.count == 1{
                    cell.lblPrice.text = "£"+obj.outcallPrice+".00"
                }else{
                    cell.lblPrice.text = "£"+obj.outcallPrice
                }
            }else{
                if obj.incallPrice != ""{
                    if Int(obj.incallPrice) == 0 {
                        cell.lblPrice.text = "NA"
                    }
                }
         }
        }else if obj.bookingType == "Incall"{
            let arrayInc = obj.incallPrice.components(separatedBy: ".")
            if arrayInc.count == 1{
                cell.lblPrice.text = "£"+obj.incallPrice+".00"
            }else{
            cell.lblPrice.text = "£"+obj.incallPrice
            }
        }else if obj.bookingType == "Outcall"{
            let arrayOuc = obj.outcallPrice.components(separatedBy: ".")
            if arrayOuc.count == 1{
                cell.lblPrice.text = "£"+obj.outcallPrice+".00"
            }else{
                cell.lblPrice.text = "£"+obj.outcallPrice
            }
        }
         return cell
    //}
    //return UITableViewCell()
}
    
    
    
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        tblServiceList.reloadData()
        
        // if the table view is the last UI element, you might need to adjust the height
        let size = CGSize(width: targetSize.width,
                          height: tblServiceList.frame.origin.y + tblServiceList.contentSize.height)
        return size
        
    }
    
    
    

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let obj = self.arrService[indexPath.row]
    objAppShareData.objModelServicesList = obj
    objAppShareData.objEditServiceVC.businessName = self.arrBusinessCategory[0]
    objAppShareData.objEditServiceVC.categoryName =  self.arrBusinessCategory[1]
    NotificationCenter.default.post(name: NSNotification.Name("Detail"), object: obj)
}

@available(iOS 11.0, *)
func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
    
    let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
        success(true)
        let obj = self.arrService[indexPath.row]
        objAppShareData.objModelServicesList = obj
        NotificationCenter.default.post(name: NSNotification.Name("Delete"), object: obj)
      })
    Delete.image = #imageLiteral(resourceName: "delete")
    Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
    
    let edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
        success(true)
        let obj = self.arrService[indexPath.row]
        objAppShareData.objModelServicesList = obj
        
        NotificationCenter.default.post(name: NSNotification.Name("Edit"), object: obj)
      })
    
    edit.image = #imageLiteral(resourceName: "edit")
    edit.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    
    return UISwipeActionsConfiguration(actions: [Delete,edit])
}
}

class CellAddStaffServies: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tblServiceList:UITableView!
    @IBOutlet weak var lblCategoryName:UILabel!
    
    @IBOutlet weak var heightOfCell:NSLayoutConstraint!
    var arrService = [ModelServicesList]()
    var arrBusinessCategory = [String]()
    var updateCell:Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("row count = \(self.arrService.count)")
        return self.arrService.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("hello")
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubServicesTableCell", for: indexPath) as! SubServicesTableCell
        // {
        let obj = self.arrService[indexPath.row]
        cell.lblTop.isHidden = true
        if indexPath.row == 0{
            cell.lblTop.isHidden = false
        }
        cell.lblServiceName.text = obj.serviceName.capitalized
        
        let arr = obj.time.split(separator: ":")
        cell.lblTime.text = obj.time+" min"
        
        if arr.count > 1{
            var time = "00:00"
            if arr[0] == "00"{
                cell.lblTime.text = arr[1]+" min"
            }else{
                cell.lblTime.text = arr[0]+" hr "+arr[1]+" min"
            }
        }
        
        let array = obj.price.components(separatedBy: ".")
        cell.lblPrice.text = "£"+obj.price
        
        if array.count == 1{
            cell.lblPrice.text = "£"+obj.price+".00"
        }
        if array.count == 2{
            if array[1].count == 1{
                "£"+obj.price+"0"
            }
        }
        
        cell.lblOutcallPrice.isHidden = true
        if obj.bookingType == "Both"{
            if obj.incallPrice != ""{
                if Int(obj.incallPrice) == 0 {
                    cell.lblPrice.text = "NA"
                }
            }
         }
        cell.lblServiceName.textColor = UIColor.lightGray
        cell.imgDote.image = #imageLiteral(resourceName: "Dote")
        for objMainArray in objAppShareData.arrAddedStaffServices{
            if objMainArray.id == obj.id  {
                 cell.imgDote.image = #imageLiteral(resourceName: "dotAppColor")
            }
                }
          return cell
        //}
        //return UITableViewCell()
    }
    
    
    
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        tblServiceList.reloadData()
        
        // if the table view is the last UI element, you might need to adjust the height
        let size = CGSize(width: targetSize.width,
                          height: tblServiceList.frame.origin.y + tblServiceList.contentSize.height)
        return size
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrService[indexPath.row]
        objAppShareData.objModelServicesList = obj
        objAppShareData.objEditServiceVC.businessName = self.arrBusinessCategory[0]
        objAppShareData.objEditServiceVC.categoryName =  self.arrBusinessCategory[1]
        
        let a = objAppShareData.objModelFinalSubCategory
        a.param[a.bookingName] = objAppShareData.objEditServiceVC.businessName
        a.param[a.categoryName] = objAppShareData.objEditServiceVC.categoryName

        NotificationCenter.default.post(name: NSNotification.Name("Detail"), object: obj)
    }
    
   
}

