//
//  EditServiceVC.swift
//  MualabBusiness
//
//  Created by Mac on 22/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class EditServiceVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    var businessName = ""
    var categoryName = ""
    
    var objModelClass = ModelServicesList(dict: [:])

        @IBOutlet weak var lblHeader : UILabel!
        @IBOutlet weak var timePicker : UIDatePicker!

        @IBOutlet weak var lblDescription:UILabel!
        @IBOutlet weak var txtOnlyPrice:UITextField!
        @IBOutlet weak var btnContinue:UIButton!

        @IBOutlet weak var img1:UIImageView!
        @IBOutlet weak var img2:UIImageView!
        @IBOutlet weak var img3:UIImageView!
    
        @IBOutlet weak var viewBusins:UIView!
        @IBOutlet weak var viewCategory:UIView!
        @IBOutlet weak var viewOnlyPrice: UIView!
        @IBOutlet weak var viewTimePicker : UIView!
        @IBOutlet weak var viewSupper:UIView!
        @IBOutlet weak var viewBookingType:UIView!
        @IBOutlet weak var viewPrice:UIView!
        @IBOutlet weak var viewPriceOutCall:UIView!
    
        @IBOutlet weak var txtBusinessName:UITextField!
        @IBOutlet weak var txtCategoryName:UITextField!
        @IBOutlet weak var txtServiceName:UITextField!
        @IBOutlet weak var txtBookingType:UITextField!
        @IBOutlet weak var txtPrice:UITextField!
        @IBOutlet weak var txtOutCall:UITextField!
    
        @IBOutlet weak var txtServiceDescription:UITextView!
        @IBOutlet weak var txtTime:UITextField!
   
    var serviceName = ""


        var viewType = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            self.DelegateCalling()
            self.viewTimePicker.isHidden = true
            self.timePicker.countDownDuration = 600
            
            self.timePicker.addTarget(self, action: #selector(self.datePickedValueChanged(sender:)), for: UIControl.Event.valueChanged)
            
            self.addAccesorryToKeyBoard()
        }
    
        @objc func datePickedValueChanged (sender: UIDatePicker) {
            if (self.timePicker.countDownDuration > 10800) { //5400 seconds = 1h30min
                self.timePicker.countDownDuration = 10800; //Defaults to 3 hour
            }
        }
 
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
                self.btnContinue.setTitle("Continue", for: .normal)
            //DispatchQueue.main.async {
                self.parseDataFromLocalDataBase()
            //}
            if viewType == "YES"{
                self.lblHeader.text = "Service Details"
                self.viewSupper.isHidden = false
                self.txtServiceDescription.isHidden = true
                self.lblDescription.isHidden = false
             }else{
                self.lblHeader.text = "Edit Service"
                self.viewSupper.isHidden = true
                self.txtServiceDescription.isHidden = false
                self.lblDescription.isHidden = true
             }
            self.configureView()
        }
    
    
    
    func configureView(){
        if viewType == "YES"{
            self.lblHeader.text = "Service Details"
            self.viewSupper.isHidden = false
            self.img1.isHidden = true
            self.img2.isHidden = true
            self.img3.isHidden = true
            self.viewBusins.isHidden = false
            self.viewCategory.isHidden = false
        }else{
            self.viewBusins.isHidden = true
            self.viewCategory.isHidden = true
            self.lblHeader.text = "Edit Service"
            self.viewSupper.isHidden = true
            self.img1.isHidden = false
            self.img2.isHidden = false
            self.img3.isHidden = false
        }
     }
 }

    //MARK: - Custome method Extension
    fileprivate extension EditServiceVC{
        func DelegateCalling(){
            self.txtTime.delegate = self
            self.txtPrice.delegate = self
            self.txtOutCall.delegate = self

            self.txtServiceDescription.delegate = self
            self.txtServiceName.delegate = self
            self.txtBookingType.delegate = self
        }
     }
    
    //MARK: - textfield Extension
    extension EditServiceVC{
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            var numberOfChars:Int = 0
            let newText = (txtServiceDescription.text as NSString).replacingCharacters(in: range, with: text)
            numberOfChars = newText.count
            
            if numberOfChars >= 200 && text != ""{
                return false
            }else{
                
            }
            
            return numberOfChars < 200
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            
            let text = textField.text! as NSString
            if (text.length == 1)  && (string == "") {
            }else{
                var substring: String = textField.text!
                substring = (substring as NSString).replacingCharacters(in: range, with: string)
                substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let a = substring.count
                if textField == self.txtServiceDescription {
                    if a > 200 && substring != ""{
                        return false
                    }
                }else if textField == self.txtServiceName {
                    if a > 30 && substring != ""{
                        return false
                    }
                    
 
                }else if textField == self.txtPrice || textField == self.txtOutCall {
                    let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                    let withDecimal = (
                        string == NumberFormatter().decimalSeparator &&
                            textField.text?.contains(string) == false
                    )
                    let maxLength = 7
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength && isNumber || withDecimal
                }else{
                    return true
                }
                return true
                
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()

                if textField == self.txtServiceName{
                    txtServiceName.resignFirstResponder()
                }else if textField == self.txtPrice{
                    txtPrice.resignFirstResponder()
                }else if textField == self.txtOutCall{
                    txtServiceDescription.resignFirstResponder()
                }else if textField == self.txtServiceDescription{
                    txtServiceDescription.resignFirstResponder()
                }else if textField == self.txtServiceDescription{
                    txtServiceDescription.resignFirstResponder()
            }
            return true
        }
    }
    //MARK: - Button Extension
    fileprivate extension EditServiceVC{
        @IBAction func btnContinew(_ sender:Any){
        }
        
        @IBAction func btnBack(_ sender:Any){
            objAppShareData.objModelFinalSubCategory.param = [:]
            objAppShareData.manageNavigation = false
            self.backVC()
        }
        
        func alert(str:String){
            self.backVC()
            objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
        }
        func backVC(){
            objAppShareData.manageNavigation = false
            objAppShareData.objModelFinalSubCategory.param = [:]
            self.navigationController?.popViewController(animated: true)
         }
        
//        @IBAction func btnTime(_ sender:Any){
//            self.viewTimePicker.isHidden = false
//        }
        
        func finalDict(){
            let obj = objAppShareData.objModelFinalSubCategory
            let dict = obj.param
            print(dict)
        }
 
       func parseDataFromLocalDataBase(){
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        var strType = ""
        if businessType == "1"{
            strType = "Incall"
        }else if businessType == "2"{
            strType = "Outcall"
        }else{
            strType = "Both"
        }
        if strType == "Incall"{
        }else if strType == "Outcall"{
        }else{
            strType = "Both"
            self.viewBookingType.isHidden = false
        }

        
        let a = objAppShareData.objModelFinalSubCategory
        
        objModelClass.time = a.param[a.completionTime] ?? "00:00"
        objModelClass.bookingType = a.param[a.bookingType] ?? ""
        objModelClass.incallPrice = a.param[a.incallPrice] ?? "0.0"
        objModelClass.outcallPrice = a.param[a.outCallPrice] ?? "0.0"
        
        self.txtBookingType.text = objModelClass.bookingType
        self.txtServiceName.text = objModelClass.serviceName
        self.txtServiceDescription.text = objModelClass.describe
        self.lblDescription.text = objModelClass.describe

        self.txtTime.text = objModelClass.time
        self.txtBusinessName.text = objAppShareData.objEditServiceVC.businessName
        self.txtCategoryName.text = objAppShareData.objEditServiceVC.categoryName

        if objModelClass.bookingType == "Both"{
            objModelClass.price = a.param[a.incallPrice] ?? "0.0"
        }else if objModelClass.bookingType == "Incall"{
            objModelClass.price = a.param[a.incallPrice] ?? "0.0"
        }else if objModelClass.bookingType == "Outcall"{
            objModelClass.price = a.param[a.outCallPrice] ?? "0.0"
        }
 
            let doubleStr = String(format: "%.2f", Double(objModelClass.outcallPrice)!)
            self.txtOutCall.text = doubleStr
            let doubleStrMM = String(format: "%.2f", Double(objModelClass.price)!)
//            self.txtPrice.text = objModelClass.price
//            self.txtOnlyPrice.text = objModelClass.price
        self.txtPrice.text = doubleStrMM
        self.txtOnlyPrice.text = doubleStrMM
        
        print("objModelClass.bookingType = ",objModelClass.bookingType)
        
        
        self.viewBookingType.isHidden = false
        self.viewPrice.isHidden = false
        
        
        if viewType == "YES"{
            self.viewOnlyPrice.isHidden = true
            self.viewBookingType.isHidden = false
            if objModelClass.bookingType == "Both"{
                self.viewPriceOutCall.isHidden = false
                self.viewPrice.isHidden = false
            }else if objModelClass.bookingType == "Incall"{
                self.viewPriceOutCall.isHidden = true
                 self.viewPrice.isHidden = false
            }else if objModelClass.bookingType == "Outcall"{
                self.viewPrice.isHidden = true
                self.viewPriceOutCall.isHidden = false
            }
            
//            let inCall = "£"+(self.txtPrice.text ?? "")
//            let outCall = "£"+(self.txtOutCall.text ?? "")
            
            let inCall = self.txtPrice.text ?? ""
            let outCall = self.txtOutCall.text ?? ""
            self.txtPrice.text = inCall
            self.txtOutCall.text = outCall
        }else{
            self.viewBookingType.isHidden = false

            if objModelClass.bookingType == "Both"{
                self.viewBookingType.isHidden = false
                self.txtOnlyPrice.text = objModelClass.price
            }else if objModelClass.bookingType == "Incall"{
                self.txtOnlyPrice.text = objModelClass.incallPrice
            }else if objModelClass.bookingType == "Outcall"{
                let doubleStrMM = String(format: "%.2f", Double(objModelClass.outcallPrice)!)
                //self.txtOnlyPrice.text = objModelClass.outcallPrice
                self.txtOnlyPrice.text = doubleStrMM
            }
            self.viewPriceOutCall.isHidden = true
            self.viewPrice.isHidden = true
            self.viewOnlyPrice.isHidden = false
        }
        
    }
        
        
        
       func saveDataInModel(){
        objModelClass.serviceName = self.txtServiceName.text ?? ""
        objModelClass.describe = self.txtServiceDescription.text ?? ""
        }
}

    
    //MARK: - popup and dropdown button
    fileprivate extension EditServiceVC{
        
        @IBAction func btnPrice(_ sender:Any){
            self.view.endEditing(true)
            saveDataInModel()
        }
        
  
        @IBAction func btnTime(_ sender:Any){
            self.view.endEditing(true)
            saveDataInModel()
        }
        @IBAction func btnBookingType(_ sender: UIButton) {
            self.view.endEditing(true)
            saveDataInModel()
   }
        
        
        
    }
    
    
    //MARK:- phonepad return keyboard
    extension EditServiceVC{
        
        @IBAction func btnDoneTimeSelection(_ sender:UIButton){
            if sender.tag == 0{
                let time = self.timePicker.date
                let components = Calendar.current.dateComponents([.hour, .minute], from: time)
                let hour = components.hour!
                let minute = components.minute!
                var sH = String(hour)
                var sM = String(minute)
                if sH.count == 1{
                    sH = "0" + sH
                }
                if sM.count == 1{
                    sM = "0" + sM
                }
                
                if Int(sH) ?? 00 >= 3 {
                    sH = "03"
                    sM = "00"
                }
                self.txtTime.text = "\(sH):\(sM)"
            }
            self.viewTimePicker.isHidden = true
        }
        
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            
            txtPrice.inputAccessoryView = keyboardDoneButtonView
            txtOutCall.inputAccessoryView = keyboardDoneButtonView
            txtServiceDescription.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtOutCall.endEditing(true)
            txtPrice.endEditing(true)
            txtServiceDescription.endEditing(true)
            self.view.endEditing(true)
        }
        
        
        
        func removeWS(textfield:UITextField) -> String {
            var text = textfield.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            return text
        }
        
        
}
