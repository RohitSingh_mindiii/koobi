//
//  InvitationDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class InvitationDetailVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var txtSalary: UITextField!

    var objCompanyList = CompanyInfo(dict: ["":""])
    @IBOutlet weak var Collection: UICollectionView!
    
    var arrServices = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Collection.delegate = self
        self.Collection.dataSource = self
        // Do any additional setup after loading the view.
    }

    var ids = ""
    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    
//MARK:- Custome Methods extension
extension InvitationDetailVC{
    func viewConfigure(){
        arrServices.removeAll()
        arrServices = objCompanyList.arrBusinessType
        self.imgVwProfile.image = UIImage(named: "cellBackground")
        self.lblAddress.text  = objCompanyList.address
        self.lblUsername.text  = objCompanyList.userName
        self.txtSalary.text  = objCompanyList.salary
        if objCompanyList.message == "" || objCompanyList.message == nil{
        self.changeString(userName: objCompanyList.businessName)
        }else{
            self.lblDescription.text  = objCompanyList.message
        }
         if objCompanyList.profileImage != "" {
            if let url = URL(string: objCompanyList.profileImage){
                //self.imgVwProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                self.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
    }
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get start login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }


}

//MARK:- Collection view Delegate Methods
extension InvitationDetailVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrServices.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        cell.lblUserName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.lblUserName.text = obj
        cell.lblUserName.layer.cornerRadius = 10
        cell.lblUserName.layer.masksToBounds = true
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 26
        cellWidth = CGFloat(self.Collection.frame.size.width-2)
        
        let a = arrServices[indexPath.row]
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 13)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+20
        cellHeight = 26
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAccept(_ sender:Any){
        let id = objCompanyList._id
        let param = ["type":"accept","id":id]
        objAppShareData.fromAcceptInvitation = true
        self.callWebserviceForCompanyList(paramDict:param)
    }
    @IBAction func btnReject(_ sender:Any){
        let id = objCompanyList._id
        let param = ["type":"reject","id":id]
      self.callWebserviceForCompanyList(paramDict:param)
    }
    
    @IBAction func btnServices(_ sender:Any){
        let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ServiceListCompanyVC") as? ServiceListCompanyVC{
            objVC.businessId = objCompanyList.businessId
            objVC.staffId = objCompanyList._id
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnWorkingHours(_ sender:Any){
        
        let sloatArray = objCompanyList.staffHours
        if sloatArray.count > 0{
            for dicSloats in sloatArray{
                var strSrtartTime = ""
                var strEndTime = ""
                var day = ""
                
                strSrtartTime = dicSloats.startTime
                strEndTime = dicSloats.endTime
                day = String(dicSloats.day)
                day = objAppShareData.getDayFromSelectedDate(strDate: day)
                
                let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
                objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
                
                
            }
        }
        
        let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"workingHoursCompanyVC") as? workingHoursCompanyVC{
            objVC.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
}


//MARK:- Webservice Call
extension InvitationDetailVC {
    
    func callWebserviceForCompanyList(paramDict:[String:Any]){
        
       // type:accept ("accept","reject")
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.invitationUpdate, params: paramDict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.goBackVC()
                }else{
                    objAppShareData.fromAcceptInvitation = false
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
         }){ error in
            objAppShareData.fromAcceptInvitation = false
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func goBackVC(){
        self.navigationController?.popViewController(animated: true)

    }
}
