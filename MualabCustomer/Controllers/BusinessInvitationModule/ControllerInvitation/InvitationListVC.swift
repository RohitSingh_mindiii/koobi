//
//  InvitationListVC.swift
//  MualabBusiness
//
//  Created by Mac on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class InvitationListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblInvitationList:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var lblPopUpText:UILabel!
    @IBOutlet weak var viewPopUp:UIView!
    var ref: DatabaseReference!

     private var arrCompanyList  = [CompanyInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblNoDataFound.isHidden = true
        self.tblInvitationList.delegate = self
        self.tblInvitationList.dataSource = self
        self.viewPopUp.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
           self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if objAppShareData.fromAcceptInvitation{
           objAppShareData.fromAcceptInvitation = false
           self.viewPopUp.isHidden = false
        }
        
        var myId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                myId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                myId = String(id)
            }
        }
        self.ref = Database.database().reference()
        self.ref.child("socialBookingBadgeCount").child(myId).updateChildValues(["businessInvitationCount":"0"])

            let dict = ["artistId":myId]
            self.callWebserviceForCompanyList(dict: dict)
    }
    
    @IBAction func btnBack(_ sender:Any){
        objAppShareData.isFromNotification = false
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPopUpAction(_ sender:UIButton){
        self.viewPopUp.isHidden = true
    }
    @IBAction func btnPopUpDownloadBizAppAction(_ sender:UIButton){
        if let url = URL(string: "https://apps.apple.com/us/app/koobi-biz/id1360296200?ls=1"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if arrCompanyList.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return arrCompanyList.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
          if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellInvitationList")! as? CellInvitationList {
          let objArtistDetails = arrCompanyList[indexPath.row]
                cell.imgVwProfile.image = UIImage(named: "cellBackground")
                //cell.lblName.text  = objArtistDetails.userName
                cell.lblName.text = objArtistDetails.businessName
                cell.setDataInCollectionData(obj: objArtistDetails.arrBusinessType)
                if objArtistDetails.profileImage != "" {
                    if let url = URL(string: objArtistDetails.profileImage){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var objArtistDetails = arrCompanyList[indexPath.row]
        let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
        self.changeString(userName: objArtistDetails.businessName)
        if let objVC = sb.instantiateViewController(withIdentifier:"InvitationDetailVC") as? InvitationDetailVC{
            objVC.objCompanyList = objArtistDetails
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        // self.gotoProfileVC()
    }
}


//MARK:- Webservice Call
extension InvitationListVC {
    
    func callWebserviceForCompanyList(dict: [String : Any]){
    self.arrCompanyList.removeAll()
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }

        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.ivitationList, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                    self.tblInvitationList.reloadData()

                }else{
                    self.tblInvitationList.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblInvitationList.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrCompanyList.removeAll()
        if let arr = response["businessList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CompanyInfo.init(dict: dictArtistData)
                    var statusObj = "0"
                    if let status = dictArtistData["status"] as? String{
                        statusObj = status
                    }else if let status = dictArtistData["status"] as? Int{
                        statusObj = String(status)
                    }
                    if statusObj == "0"{
                        self.arrCompanyList.append(objArtistList)
                    }
                }
            }
        }
        if self.arrCompanyList.count == 0{
           objAppShareData.strIsAnyInvitation = "0"
        }else{
           objAppShareData.strIsAnyInvitation = "1"
        }
        self.tblInvitationList.reloadData()
    }
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "You are a member of "
        let b = userName
        let c = " You can use the same credential to login in our Koobi Biz app"
        
        
        let StrName = NSMutableAttributedString(string: a , attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Light", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b , attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Medium", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c , attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Light", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:0,length:StrName2.length))
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblPopUpText.attributedText = combination
    }

}
