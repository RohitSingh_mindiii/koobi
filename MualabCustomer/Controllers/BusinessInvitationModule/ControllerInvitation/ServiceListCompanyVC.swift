//
//  ServiceListCompanyVC.swift
//  MualabBusiness
//
//  Created by Mac on 24/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class ServiceListCompanyVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
        var objArtistList = ArtistList(dict: [:])
        var edition = false
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
        var businessId = ""
        var staffId = ""

        var arrBusinessTypeList = [ModelBusinessTypeList]()
        var navigate = true
        var hitAPI = false
        override func viewDidLoad() {
            super.viewDidLoad()
            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            self.tblBusinessLIst.reloadData()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: "Detail"), object: nil)
        }
        override func viewWillAppear(_ animated: Bool) {
            navigate = true
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            //self.fetchParcelObject()
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
                //self.viewBookingType.isHidden = true
            }
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = strType
            self.callWebserviceForUpdateData()
            //self.fetchParcelObject()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    }
    extension ServiceListCompanyVC{
        
        @objc func reloadDataForDetailCell(notification: Notification){
            edition = true
            
            if objAppShareData.manageNavigation == false{
                objAppShareData.manageNavigation = true
                
                if navigate == false{
                    return
                }
                let o = objAppShareData.objModelServicesList
                let a = objAppShareData.objModelFinalSubCategory
                // a.param = [:]
                a.param[a.serviceName] = o.serviceName
                a.param[a.strBusinessId] = o.mainBookingId
                a.param[a.strCategoryId] = o.mainCategoryId
                a.param[a.bookingType] = o.bookingType
                a.param[a.incallPrice] = o.price
                a.param[a.outCallPrice] = o.outcallPrice
                a.param[a.descriptions] = o.describe
                a.param[a.completionTime] = o.time
                a.param[a.strSubSubServiceId] = o.id
                a.param[a.bookingTypeOld] = o.bookingTypeOld
                let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"EditServiceVC") as! EditServiceVC
                objChooseType.objModelClass = objAppShareData.objModelServicesList
                objChooseType.viewType = "YES"
                if navigate == true{
                    navigate = false
                    self.navigationController?.pushViewController(objChooseType, animated: true)
                }
                
            }
        }
 
        
    }
    
    //MARK:- extension for tableview delegate
    extension ServiceListCompanyVC {
        func numberOfSections(in tableView: UITableView) -> Int
        {
            if self.arrBusinessTypeList.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            return self.arrBusinessTypeList.count
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.white
            
            let headerLabel = UILabel(frame: CGRect(x: 20, y: 2, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Nunito-Medium", size: 17)
            headerLabel.textColor = UIColor.black
            headerLabel.text = self.tableView(self.tblBusinessLIst, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            
            return headerView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 30
        }
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
        {
            return self.arrBusinessTypeList[section].businessName.capitalized
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let arrDevelopers = self.arrBusinessTypeList[section].arrBusinessList
            return arrDevelopers.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellAddStaffServies", for: indexPath) as? CellAddStaffServies{
                let obj  = arrBusinessTypeList[indexPath.section].arrBusinessList[indexPath.row]
                cell.lblCategoryName.text = obj.categoryName
                cell.arrService = obj.arrServices
                print("height = ",CGFloat(30+(obj.arrServices.count*45)))
                cell.setNeedsLayout()
            cell.arrBusinessCategory.append(arrBusinessTypeList[indexPath.section].businessName)
                cell.arrBusinessCategory.append(obj.categoryName)
                cell.updateCell = false
                objAppShareData.arrAddedStaffServices.removeAll()
                cell.tblServiceList.reloadData()
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    
    //MARK:- extension for tableview delegate
    extension ServiceListCompanyVC {
        @IBAction func btnBack(_ sender:Any){
            self.navigationController?.popViewController(animated: true)
        }
    }


    
    //MARK: - call api for get service list from backecd server and set on local server
    
    extension ServiceListCompanyVC{
        func callWebserviceForUpdateData(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.arrBusinessTypeList.removeAll()
            self.tblBusinessLIst.reloadData()
            
            objActivity.startActivityIndicator()
            let param = ["staffId":staffId,
                         "businessId":businessId]
            objServiceManager.requestPostForJson(strURL: WebURL.staffServices, params: param, success: { response in
                print(response)
                objActivity.stopActivity()
                let status = response["status"] as? String ?? ""
                if status == "success"{
                    
                    
                    if let arrArtistServices = response["artistServices"] as? [[String:Any]]{
                        for dict1 in arrArtistServices {
                            var MainServiceId = ""
                            if let id = dict1["serviceId"] as? Int { MainServiceId = String(id)
                            }else if let id = dict1["serviceId"] as? String{ MainServiceId = id
                            }
                            let businessName = dict1["serviceName"] as? String ?? ""
                            var arrServices = [[String:Any]]()
                            var arrayCate = [[String:Any]]()
                            
                            if let arrSer = dict1["subServies"] as? [[String:Any]]{
                                for dict11 in arrSer{
                                    var arrayServices = [[String:Any]]()
                                    let catId = dict11["subServiceId"] as? Int ?? 0
                                    
                                    if let arrArtistServices = dict11["artistservices"] as? [[String:Any]]{
                                        for dict111 in arrArtistServices{
                                            
                                            
                                            var outCallPrice = "0.00"
                                            if let id = dict111["outCallPrice"] as? Int {
                                                let a = float_t(id)
                                                outCallPrice = String(a)
                                            }else if let id = dict111["outCallPrice"] as? String{
                                                outCallPrice = id
                                                if id != ""{
                                                    let a = float_t(id) ?? 0.00
                                                    outCallPrice = String(a)
                                                }
                                            }else if let id = dict111["outCallPrice"] as? float_t{
                                                outCallPrice = String(id)
                                            }
                                            
                                            var inCallPrice = "0.00"
                                            if let id = dict111["inCallPrice"] as? Int {
                                                let a = float_t(id)
                                                inCallPrice = String(a)
                                            }else if let id = dict111["inCallPrice"] as? String{
                                                inCallPrice = id
                                                if id != ""{
                                                    let a = float_t(id) ?? 0.00
                                                    inCallPrice = String(a)
                                                }
                                            }else if let id = dict111["inCallPrice"] as? float_t{
                                                inCallPrice = String(id)
                                            }else if let id = dict111["inCallPrice"] as? double_t{
                                                inCallPrice = String(id)
                                            }
                                            
                                            var idService = ""
                                            if let id = dict111["_id"] as? Int { idService = String(id)
                                            }else if let id = dict111["_id"] as? String{ idService = id
                                            }
                                            var bukTyp = dict111["bookingType"] as? String ?? ""
                                            var price = inCallPrice
                                            if bukTyp == "Outcall"{
                                                price = outCallPrice
                                            }
                                            
                                            if inCallPrice == ""{
                                                inCallPrice = "0.00"
                                            }
                                            if outCallPrice == ""{
                                                outCallPrice = "0.00"
                                            }
                                            if float_t(inCallPrice) == 0{
                                                bukTyp = "Outcall"
                                                price = outCallPrice
                                            }else if float_t(outCallPrice) == 0{
                                                bukTyp = "Incall"
                                                price = inCallPrice
                                            }else{
                                                price = inCallPrice
                                                bukTyp = "Both"
                                            }
                                            
                                    let dictA = ["price":price,
                                                         "incallPrice":inCallPrice,
                                                         "outcallPrice":outCallPrice,
                                                         "_id":idService,
                                                         "mainBookingId":MainServiceId,
                                                         "mainCategoryId":catId,
                                                         "serviceName":dict111["title"] as? String ?? "",
                                                         "bookingType":bukTyp,
                                                         "bookingTypeOld":bukTyp,
                                                         "describe":dict111["description"] as? String ?? "",
                                                         "time":dict111["completionTime"] as? String ?? ""] as [String : Any]
                                            
                                            let lastServiceName = dict111["title"] as? String ?? ""
                                            
                                            arrayServices.append(dictA)
                                            arrServices.append(dictA)
                                        }
                                    }
                                    let catName = dict11["subServiceName"] as? String ?? ""
                                    let c = dict1["serviceName"] ?? ""
                                    let dict1 = ["categoryId":catId,
                                                 "categoryName":catName,
                                                 "businessName":businessName,
                                                 "arrServices":arrayServices] as [String : Any]
                                    if arrayServices.count > 0{
                                        arrayCate.append(dict1)
                                    }
                                }
                            }
                            let dict2 = ["businessName":businessName,
                                         "businessId":MainServiceId,
                                         "arrServices":arrServices,
                                         "arrCategory":arrayCate] as [String : Any]
                            
                            let objModelBusinessTypeList = ModelBusinessTypeList(dict: dict2)
                            if objModelBusinessTypeList.arrBusinessList.count > 0{
                                self.arrBusinessTypeList.append(objModelBusinessTypeList)
                            }
                        }
                            self.tblBusinessLIst.reloadData()
                        
                    }
                    self.tblBusinessLIst.reloadData()
                }else{
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                objActivity.stopActivity()
                self.tblBusinessLIst.reloadData()
            }
        }
    }

