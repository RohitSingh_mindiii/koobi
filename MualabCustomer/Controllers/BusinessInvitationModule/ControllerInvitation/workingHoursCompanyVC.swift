//
//  workingHoursCompanyVC.swift
//  MualabBusiness
//
//  Created by Mac on 25/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//


    
import UIKit
    
    fileprivate enum day:Int {
        case Monday
        case Tuesday
        case Wednesday
        case Thursday
        case Friday
        case Saturday
        case Sunday
        
        var string: String {
            return String(describing: self)
        }
    }
    
    class workingHoursCompanyVC: UIViewController {
 
        @IBOutlet weak var tblBusinessHours : UITableView!
 
        fileprivate var arrOpeningTimes = [openingTimes]()
        
        fileprivate var fromNextButton = false
        
        fileprivate var tblTag = Int()
        fileprivate var timeTag = Int()
    }
    
    //MARK: - View hirarchy
    extension workingHoursCompanyVC{
        override func viewDidLoad() {
            super.viewDidLoad()
            self.tblBusinessHours.isUserInteractionEnabled = false
           // if objAppShareData.editStaffTimeSloat == true{
                dataParsingMethod()
//            }else{
//                makeDeafultsDay()
//                //callWebserviceForGetBusinessProfile()
//            }
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            //makeDeafultsDay()
           // customTimePickers()
            dataParsingMethod()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              //  self.configureView()
            }
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
    }
//    //MARK: -Local Methods
//    fileprivate extension workingHoursCompanyVC{
//        func configureView() {
////            self.timePickerBottem.constant = -self.view.frame.size.height/1.5
////            self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2
//        }
//
//        func customTimePickers()  {
////            startTimePicker.date = self.setDateFrom("10:00 AM")
////            endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(600.0)
////            endTimePicker.date = self.setDateFrom("07:00 PM")
////            startTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
////            endTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
//        }
//
//        //UIDatePicker methods
//        @objc func dateChanged(_ sender: UIDatePicker) {
////            endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(600.0)
////            self.lblTimes.text = "From: \(self.timeFormat(forAPI: self.startTimePicker.date)) To: \(self.timeFormat(forAPI: self.endTimePicker.date))"
//        }
//
//
//
//        func removeTimeObjectsArray(){
//            for obj in self.arrOpeningTimes{
//                obj.arrTimeSloats.removeAll()
//                obj.isOpen = false
//            }
//
//
//        }
//    }
//    //MARK : - Save Methods
//    fileprivate extension workingHoursCompanyVC{
//
//        func saveData(_ dicResponse:[String:Any]){
//            print(dicResponse)
//            let arr = dicResponse["businessHour"] as! [[String:Any]]
//            let bankStatus = dicResponse["bankStatus"] as! Bool
//            UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
//            UserDefaults.standard.synchronize()
//            if arr.count>0{
//                self.removeTimeObjectsArray()
//                for dic in arr{
//                    if let d = dic["day"] as? Int{
//                        for (index,openings) in self.arrOpeningTimes.enumerated(){
//                            if d == index{
//                                openings.isOpen = true
//                                openings.strDay = (day(rawValue:index)?.string)!
//                                let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as! String,endTime:dic["endTime"] as! String)
//                                openings.arrTimeSloats.append(objTimeSloat!)
//                            }
//                        }
//                    }
//                }
//
//                if self.arrOpeningTimes.count > 0{
//                    for dic in self.arrOpeningTimes{
//                        let selectIndex = self.arrOpeningTimes.index(of: dic)
//                        if dic.arrTimeSloats.count == 0{
//                            self.arrOpeningTimes.remove(at: selectIndex!)
//                        }
//                    }
//                }
//                self.tblBusinessHours.reloadData()
//            }
//        }
//    }

    //MARK : - IBAction
    fileprivate extension workingHoursCompanyVC{
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            fromNextButton = false
            objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
            self.navigationController?.popViewController(animated: true)
        }
    }
    

    //MARK: - UITableViewDelegate and UITableViewDataSource
    extension workingHoursCompanyVC:UITableViewDelegate,UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrOpeningTimes.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tblBusinessHours.dequeueReusableCell(withIdentifier: "BusinessHoursTableCell") as! BusinessHoursTableCell
            //objAppShareData.imageShow = false
            
            let objOpenings = self.arrOpeningTimes[indexPath.row]
            let index = indexPath.row
            if index == self.arrOpeningTimes.count-1 || index == self.arrOpeningTimes.count{
                //objAppShareData.imageShow = true
            }
            cell.lblDay?.text = objOpenings.strDay
            cell.btnIsOpen?.isSelected = objOpenings.isOpen
            cell.arrTime = objOpenings.arrTimeSloats
            //cell.delegate = self
            if objOpenings.isOpen{
                cell.btnAdd?.isHidden = false
                cell.lblNotWorking?.isHidden = true
            }else{
                cell.btnAdd?.isHidden = true
                cell.lblNotWorking?.isHidden = false
            }
            //
            cell.tblTime?.tag = indexPath.row
            cell.btnIsOpen?.tag = indexPath.row
            cell.btnAdd?.tag = indexPath.row
            //
            cell.tblTimeHeight?.constant = CGFloat((objOpenings.arrTimeSloats.count * 26)+26)+8
            
            if objOpenings.arrTimeSloats.count == 0 || objOpenings.arrTimeSloats.count == 1{
                cell.tblTimeHeight?.constant = 35
            }
            if objOpenings.arrTimeSloats.count == 2 {
                cell.tblTimeHeight?.constant = 62
            }
            if objOpenings.arrTimeSloats.count == 0{
                cell.lblDay?.textColor = UIColor.lightGray
            }else{
                cell.lblDay?.textColor = UIColor.black
            }
            cell.tblTime?.reloadData()
            return cell
        }
    }
    
//    //MARK: - BusinessHoursTableCellDelegate
//
//    extension workingHoursCompanyVC : BusinessHoursTableCellDelegate{
//
//        func removeTimeSloat(withSenderTable tableView: UITableView, timeArrayIndex: Int) {
//            self.deleteTimeSloat(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
//        }
//
//        func selectedTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int){
//            openTimePicker(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
//        }
//    }
//
//
//    //MARK: - Date formates
//
//    fileprivate extension workingHoursCompanyVC {
//
//        //MARK: - Date time format
//        func timeFormat(forAPI date: Date) -> String {
//            let formatter = DateFormatter()
//            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
//            formatter.dateFormat = "hh:mm a"
//            let formattedTime: String = formatter.string(from: date)
//            return formattedTime.uppercased()
//        }
//
//        // Set deafult Times from string
//        func setDateFrom(_ time: String) -> Date {
//            let dateFormatter = DateFormatter()
//            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
//            dateFormatter.dateFormat = "hh:mm a"
//            let date = dateFormatter.date(from: time)
//            return date!
//        }
//    }
//
    //MARK: - Date ParsingMethod
    fileprivate extension workingHoursCompanyVC {
        func dataParsingMethod(){
            self.arrOpeningTimes.removeAll()
           // if objAppShareData.addTimeSloat == true {
                if objAppShareData.objModelEditTimeSloat.arrFinalTimes.count > 0{
                    let arrFinam = objAppShareData.objModelEditTimeSloat.arrFinalTimes
                    
                    for newObj in arrFinam{
                        let isOpen = newObj.isOpen
                        let strDay = newObj.strDay
                        let arrTimeSloats = newObj.arrTimeSloats
                        
                        let obj = openingTimes(open: isOpen, day: strDay, times: arrTimeSloats)
                        self.arrOpeningTimes.append(obj!)
                    }
                }
                
//                
//                if objAppShareData.objModelEditTimeSloat.arrValidTimes.count > 0{
//                    for objValid in objAppShareData.objModelEditTimeSloat.arrValidTimes{
//                        var index = 0
//                        let selectIndex = objAppShareData.objModelEditTimeSloat.arrValidTimes.index(of: objValid)
//                        index = selectIndex!
//                        let dayValid = objValid.strDay
//                        var addIndex = false
//                        for objLocal in self.arrOpeningTimes{
//                            if objLocal.strDay == dayValid{
//                                addIndex = true
//                            }
//                        }
//                        if addIndex == false{
//                            let obj = openingTimes(open: false, day: dayValid, times: [])
//                            self.arrOpeningTimes.insert(obj!, at: index)
//                        }
//                    }
//                }
                
//
//            }else if objAppShareData.objModelEditTimeSloat.arrStoreTimes.count > 0{
//                let arrFinam = objAppShareData.objModelEditTimeSloat.arrStoreTimes
//
//                for newObj in arrFinam{
//                    let isOpen = newObj.isOpen
//                    let strDay = newObj.strDay
//                    let arrTimeSloats = newObj.arrTimeSloats
//
//                    let obj = openingTimes(open: isOpen, day: strDay, times: arrTimeSloats)
//                    self.arrOpeningTimes.append(obj!)
//                }
//            }
            self.tblBusinessHours.reloadData()
        }
        
        
        func makeDeafultsDay() {
            self.arrOpeningTimes.removeAll()
            
            //objAppShareData.objModelEditTimeSloat.arrFinalTimes1.removeAll()
            let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
            for day in arrDays{
                let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
                let arr = [objTimeSloat] as! [timeSloat]
                var objOpenings:openingTimes?
                objOpenings = openingTimes.init(open: true, day: day, times: arr)
                if objOpenings?.arrTimeSloats.count != 0{
                    self.arrOpeningTimes.append(objOpenings!)
                    //objAppShareData.objModelEditTimeSloat.arrFinalTimes1.append(objOpenings!)
                }
            }
                        self.tblBusinessHours.reloadData()
        }
}
