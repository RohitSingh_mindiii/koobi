//
//  CellChatHistory.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/20/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SDWebImage

class CellChatHistory: UITableViewCell {
    @IBOutlet weak var imgMuteStatus: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMsgCount: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var lblDayDate: UILabel!
    @IBOutlet weak var viewTopLine: UIView!
    @IBOutlet weak var viewBottomLine: UIView!
    
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    var strUserId = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(objChatList : ChatHistoryData){
        
        self.lblMsgCount.isHidden = true
        self.lblDayDate.text = objChatList.strDateTime//objAppShareData.getMsgDay(timeStamp:objChatList.strDate)
        if objChatList.strUserType == "user"{
            self.lblUserType.text = ""
            self.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
        }else if objChatList.strUserType == "broadcast"{
            self.lblUserType.text = "Broadcast"
            self.imgUserProfile.image = #imageLiteral(resourceName: "logo1")
        }else if objChatList.strUserType == "group"{
            self.imgUserProfile.image = #imageLiteral(resourceName: "user_img_ico")
            self.lblUserType.text = "Group"
        }else{
            self.lblUserType.text = ""
        }
        
        
        self.lblMsg.text = objChatList.strMessage
        self.lblTime.text = objChatList.strDate//objChatList.strDateTime
        self.lblUserName.text = objChatList.strOpponentName
        self.lblMsg.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        if (objChatList.strUnreadMessage) >= 1 {
            self.lblMsg.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)

            self.lblMsgCount.text = String(objChatList.strUnreadMessage)
            if (objChatList.strUnreadMessage) >= 100{
                self.lblMsgCount.text = "99+"
            }
            self.lblMsgCount.isHidden = false
        }
        
        if objChatList.strUserType == "group" {
            if objChatList.strOpponentProfileImage != ""{
                if let url = URL(string: objChatList.strOpponentProfileImage){
                    //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "group_img_ico"))
                    self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
            
        }else if objChatList.strUserType == "user"{
            if objChatList.strOpponentProfileImage != ""{
                if let url = URL(string: objChatList.strOpponentProfileImage){
                    //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
        }else{
            self.imgUserProfile.image = #imageLiteral(resourceName: "logo1")
        }
 
        if objChatList.strMessageType == "1"{
            self.lblMsg.text = "Image"
        }else{
            self.lblMsg.text = objChatList.strMessage
        }
    
    }
}
