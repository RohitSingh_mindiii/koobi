//
//  CellCrearGroup.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/8/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellCreatGroup: UITableViewCell {
    @IBOutlet weak var viewUserType: UIView!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnChackUnchack: UIButton!
    @IBOutlet weak var imgChackUnchack: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
