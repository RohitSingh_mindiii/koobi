//
//  MyTextCell.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit

class MyTextCell: UITableViewCell {
    @IBOutlet weak var heightDayDate: NSLayoutConstraint!

    @IBOutlet weak var viewMainBack: UIView!
    @IBOutlet weak var imgReadStatus: UIImageView!
    @IBOutlet weak var viewBlur: UIView!
    
    @IBOutlet weak var viewMyText: UIView!
    @IBOutlet weak var lblMyMessage: UILabel!
    @IBOutlet weak var lblMyMsgTime: UILabel!
    
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

 
    func cellDataParsing(objChat:ChatData, strDate: String, msgtime: String, resultDate: String,yesterDayDate: String){
        
        self.lblMyMessage.text = objChat.strMessage
        if objChat.strReadStatus == "0"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_single_tick_gray")
        }else if objChat.strReadStatus == "1"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
        }else if objChat.strReadStatus == "2"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
        }
 
        if resultDate == strDate {
            self.lblMsgDay.text = "Today"
        }else if yesterDayDate == strDate{
            self.lblMsgDay.text = "Yesterday"
        }else{
            self.lblMsgDay.text = strDate
        }
        self.lblMyMsgTime.text = self.lblMsgDay.text! + " " + msgtime

    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
