//
//  OpponentImageself.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit
import Kingfisher

class OpponentImageCell: UITableViewCell {
    @IBOutlet weak var heightDayDate: NSLayoutConstraint!

    @IBOutlet weak var viewMainBack: UIView!
    @IBOutlet weak var lblOppName: UILabel!

    @IBOutlet weak var imgOppSide: UIImageView!
    @IBOutlet weak var viewOppImage: UIView!
    @IBOutlet weak var lblOppMsgTime: UILabel!
    @IBOutlet weak var btnZoomImage: UIButton!
    
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    @IBOutlet weak var OppImageIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
   
    class MediaFetcher {
        func downloadGifFile(_ url: URL, completion: @escaping (_ imageURL: URL) -> () = { (imageURL) in} ) {
            KingfisherManager.shared.retrieveImage(with: url, options: .none, progressBlock: nil) { (image, error, cacheType, imageUrl) in
                guard error == nil else {
                    return
                }
                guard let imageUrl = imageUrl else {
                    return
                }
                completion(imageUrl)
            }
        }
    }
    
    func cellDataParsing(objChat:ChatData, strDate: String, msgtime: String, resultDate: String,yesterDayDate: String,opponentData:ChatHistoryData){
        self.lblOppName.text = objChat.strOppName
        self.OppImageIndicator.isHidden = false
        self.OppImageIndicator.startAnimating()
        let urlImg = URL(string: objChat.strMessage)
        let urlReq = URLRequest(url:urlImg! , cachePolicy:.returnCacheDataElseLoad,timeoutInterval:0.0)
        if (objChat.strMessage.contains("content://media/external")
            || objChat.strMessage.contains("content://com.google.android")) {
            self.imgOppSide.image = #imageLiteral(resourceName: "gallery_placeholder")
            self.OppImageIndicator.isHidden = false
            self.OppImageIndicator.startAnimating()
        }else{
            if objChat.strMessage.contains(".gif"){
//                MediaFetcher.downloadGifFile(urlImg) { imageUrl in
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                       //self.imgOppSide.image = imageURL
//                    }
//                }
                //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.imgOppSide.image = #imageLiteral(resourceName: "gallery_placeholder")
                DispatchQueue.main.async {
                //let imageURL = UIImage.gif(url: objChat.strMessage)
                self.imgOppSide.image = objChat.imageLocal
                self.OppImageIndicator.stopAnimating()
                self.OppImageIndicator.isHidden = true
                }
            }else{
                self.OppImageIndicator.stopAnimating()
                self.OppImageIndicator.isHidden = true
                
                let processor = DownsamplingImageProcessor(size: self.imgOppSide.frame.size)
                    >> RoundCornerImageProcessor(cornerRadius: 0)
                self.imgOppSide.kf.indicatorType = .activity
                self.imgOppSide.kf.setImage(
                    with: urlImg,
                    placeholder: UIImage(named: "gallery_placeholder"),
                    options: [
                        .processor(processor),
                        .scaleFactor(UIScreen.main.scale),
                        .transition(.fade(1)),
                        .cacheOriginalImage
                    ])
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                }
            //self.imgOppSide.kf.setImage(with: urlImg!, placeholder: #imageLiteral(resourceName: "gallery_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                /*
            self.imgOppSide.af_setImage(withURLRequest: urlReq, placeholderImage:#imageLiteral(resourceName: "gallery_placeholder"), completion: { response in
                self.OppImageIndicator.stopAnimating()
                self.OppImageIndicator.isHidden = true
                if let image = response.result.value {
                    self.imgOppSide.image = image
                }else{
                    self.imgOppSide.image = #imageLiteral(resourceName: "gallery_placeholder")
                }
            })*/
            }
        }
        
        if resultDate == strDate {
            self.lblMsgDay.text = "Today"
        }else if yesterDayDate == strDate{
            self.lblMsgDay.text = "Yesterday"
        }else{
            self.lblMsgDay.text = strDate
        }
        self.lblOppMsgTime.text = self.lblMsgDay.text! + " " + msgtime
        //self.viewMsgDay.isHidden = true
        self.lblMsgDay.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
