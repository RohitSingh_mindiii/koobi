//
//  OpponentTextCell.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit

class OpponentTextCell: UITableViewCell {
    @IBOutlet weak var heightDayDate: NSLayoutConstraint!

    @IBOutlet weak var viewMainBack: UIView!
    @IBOutlet weak var lblOppName: UILabel!

    @IBOutlet weak var viewOppText: UIView!
    @IBOutlet weak var lblOppMessage: UILabel!
    @IBOutlet weak var lblOppMsgTime: UILabel!
    
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
