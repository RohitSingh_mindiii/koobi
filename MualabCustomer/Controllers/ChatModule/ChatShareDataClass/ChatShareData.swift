//
//  ChatShareData.swift
//  MualabCustomer
//  Created by Mindiii on 7/28/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation
let objChatShareData = ChatShareData.sharedObject()

class ChatShareData {
    //MARK: - Shared object
    var fromChatToHistory:Bool = false
    var fromReportVC:Bool = false
    var fromChatVC:Bool = false
    var fromGroupChatDelete:Bool = false
    var isOnChatScreen:Bool = false
    var currentOpponantIDForNotification : String = ""
    var chatBadgeCount:Int = 0
    let objChatVC = ChatVC()
    var kServerKey =  "AAAAK1vRFPE:APA91bFDJlGE-pK5f7JarrELoglCDCZl2Bnnm495IBiYjWXte8BInV8ZSdNT9fcW-xx96LQFIQAAGiwvMXYpK8ap6uJX6qfiPXfMCEwbGbfd7KMXtSSm9MLdfpD6AhdpbHbzSQbew5wF"
    
    
    private static var ChatSharedManager: ChatShareData = {
        let manager = ChatShareData()
        return manager
    }()
    // MARK: - Accessors
    class func sharedObject() -> ChatShareData {
        return ChatSharedManager
    }
}


extension ChatShareData{
    func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        
        let unitsSet : Set<Calendar.Component> = [.year,.month,.weekOfYear,.day, .hour, .minute, .second, .nanosecond]
        
        let components:NSDateComponents =  calendar.dateComponents(unitsSet, from: earliest, to: latest as Date) as NSDateComponents
        
        if (components.year >= 2) {
            return "\(components.year) years ago"
        } else if (components.year >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month >= 2) {
            return "\(components.month) months ago"
        } else if (components.month >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear >= 2) {
            return "\(components.weekOfYear) weeks ago"
        } else if (components.weekOfYear >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day >= 2) {
            return "\(components.day) days ago"
        } else if (components.day >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour >= 2) {
            return "\(components.hour) hours ago"
        } else if (components.hour >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute >= 2) {
            return "\(components.minute) minutes ago"
        } else if (components.minute >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second >= 3) {
            return "\(components.second) seconds ago"
        } else {
            return "Just now"
        }
    }
    
    func timeAgoSinceDateForNotificationList(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        
        let unitsSet : Set<Calendar.Component> = [.year,.month,.weekOfYear,.day, .hour, .minute, .second, .nanosecond]
        
        let components:NSDateComponents =  calendar.dateComponents(unitsSet, from: earliest, to: latest as Date) as NSDateComponents
        
        
        let yesterdayDate  = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        // let oneDayOldDate  = Calendar.current.date(byAdding: .day, value: -0, to: date as Date)
        
        var dateOfOneDayOld = ""
        let yesterday = "\(yesterdayDate)"
        let yesterDay = yesterday.components(separatedBy: " ")
        if yesterDay.count > 0{
            dateOfOneDayOld = String(yesterDay[0])
        }
        
        var returnDate = ""
        let newDate = "\(date)"
        let bbDate = newDate.components(separatedBy: " ")
        if bbDate.count > 0{
            returnDate = String(bbDate[0])
        }
        
        if (components.year >= 1){
            return "Before This Year"
        } else if (components.month >= 2) {
            return "This Year"
        } else if (components.month >= 1){
            return "This Year"
        } else if (components.weekOfYear >= 2) {
            return "This Month"
        } else if (components.weekOfYear >= 1){
            return "This Month"
        } else if (components.day >= 2) {
            return "This Week"
        } else if (components.day >= 1){
            if (components.day >= 1) && (components.day < 2){
                print(components.hour)
                if returnDate == dateOfOneDayOld{
                    return "Yesterday"
                }else{
                    return "This Week"
                }
            }else{
                return "New"
            }
        } else{
            var c = ""
            let a = "\(Date())"
            let b = a.components(separatedBy: " ")
            if b.count > 0{
                c = String(b[0])
            }
            var d = ""
            let e = "\(date)"
            let f = e.components(separatedBy: " ")
            if f.count > 0{
                d = String(f[0])
            }
            if c == d{
                return "New"
            }else{
                return "Yesterday"
            }
            
        }
    }
    
    func getToDay()->String{
        let dateToday = Date()
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MMMM-yyyy"
        let resultDate = formatter.string(from: dateToday)
        return resultDate
    }
    
    func getDateMunthYear(timeStamp:String)->String{
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let arr = timeStamp.split(separator: "/")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let msgDate:String = arr[2]+"-"+arr[1]+"-"+arr[0]+" 00:00:00"
        let date = formatter.date(from: msgDate)
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        return strDate
    }
    
    //MARK:- Alert for chat Validation
    func showAlertChatValidation(titles:String,messages1:String, controller: UIViewController) {
        let alertController = UIAlertController(title: titles, message: messages1, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        OKAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - Compess image resize
extension ChatShareData{
    
    func compressImage(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 1600.0
        let maxWidth : CGFloat = 1000.0
        let minHeight : CGFloat = 150.0
        let minWidth : CGFloat = 150.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        let minRatio : CGFloat = minWidth/minHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        if (actualHeight < minHeight || actualWidth < minWidth){
            if(imgRatio > minRatio){
                //adjust width according to maxHeight
                imgRatio = minHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = minHeight
            }
            else if(imgRatio < minRatio){
                //adjust height according to maxWidth
                imgRatio = minWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = minWidth
            }
            else{
                actualHeight = minHeight
                actualWidth = minWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }
    
}

//MARK: - get ChatVC Method
extension ChatShareData {
    func getMsgDay(timeStamp:String) ->String {
        var day = ""
        let dateToday = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy"
        let resultDate = formatter.string(from: dateToday)
        
        let c = timeStamp.split(separator: " ")
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let date = formatter.date(from: String(c[0]))
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDayDate = formatter.string(from: yesterday!)
        
        if resultDate == strDate {
            day = "Today"
        }else if yesterDayDate == strDate{
            day = "Yesterday"
        }else{
            day = strDate
        }
        return day
    }
    
    
    func getMsgDate(timeStamp:String)->String{
        var date = ""
        let formatter = DateFormatter()
        
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let dates = formatter.date(from: timeStamp)
        formatter.dateFormat = "dd-MMMM-yyyy"
        date = formatter.string(from: dates!)
        return date
    }
    
    
}
