//
//  AddBroadCastMemberVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 9/5/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

class AddBroadCastMemberVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource  {
        
        @IBOutlet weak var viewCollection:UIView!
        @IBOutlet weak var tblMemberList:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
        @IBOutlet weak var txtEnterMemberName: UITextField!
        @IBOutlet weak var collectionView: UICollectionView!
        
        var objChatHistoryModel = ChatHistoryData()
        var arrSelectedMember = [ChatHistoryData]()
        var ref: DatabaseReference!
        var messages: [DataSnapshot]! = []
        var strName = ""
        fileprivate var _refHandle: DatabaseHandle?
        fileprivate var storageRef: StorageReference!
        fileprivate var strMyChatId:String = ""
        fileprivate var arrChatHistoryTotal = [ChatHistoryData]()
        fileprivate var arrChatHistory = [ChatHistoryData]()
        fileprivate var arrChatHistoryMyData = [ChatHistoryData]()
        fileprivate var arrChatHistoryTextFilter = [ChatHistoryData]()
        
        fileprivate var strText = ""
        
        fileprivate var newGroupName = ""
    }
    
    //MARK: - System Method extension
    extension AddBroadCastMemberVC{
        override func viewDidLoad() {
            super.viewDidLoad()
            self.txtEnterMemberName.delegate = self
            ref =  Database.database().reference()
            storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
            
            self.strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
            self.strName = UserDefaults.standard.string(forKey: UserDefaults.keys.myName) ?? ""
            
            self.viewConfigure()
            self.GetUserListFromFirebase()
            
            self.viewConfigure()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.viewCollection.isHidden = true
            self.GetUserListFromFirebase()
        }
    }
    
    //MARK: - Custome method extension
    fileprivate extension AddBroadCastMemberVC{
        func viewConfigure(){
            self.tblMemberList.delegate = self
            self.tblMemberList.dataSource = self
            self.txtEnterMemberName.delegate = self
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            ref = Database.database().reference()
        }
        
        internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            arrChatHistory.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            
            let filteredArray = self.arrChatHistoryTotal.filter(){ $0.strOpponentName.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            strText = substring
            arrChatHistory = filteredArray
            if arrChatHistory.count == 0 && strText.count != 0{
                self.lblNoDataFound.isHidden = false
                self.tblMemberList.isHidden = true
            }else{
                if strText.count == 0{
                    arrChatHistory = self.arrChatHistoryTotal
                    self.lblNoDataFound.isHidden = true
                    self.tblMemberList.isHidden = false
                }else{
                    self.lblNoDataFound.isHidden = true
                    self.tblMemberList.isHidden = false
                }
            }
            self.tblMemberList.reloadData()
            ////
            self.tblMemberList.contentSize.height = (self.tblMemberList.contentSize.height) + 60.0
            ////
            // }
            return  true
        }
        
        
        internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == self.txtEnterMemberName{
                txtEnterMemberName.resignFirstResponder()
            }
            return true
        }
    }
    
    //MARK: - collection view delegate method
    extension AddBroadCastMemberVC:UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.arrSelectedMember.count
        }
        
        // make a cell for each cell index path
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            //self.viewCollection.isHidden = false
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellAddRemoveGroupMember", for:indexPath as IndexPath) as? CellAddRemoveGroupMember{
                
                let objStaff = arrSelectedMember[indexPath.row]
                
                let url = URL(string: objStaff.strOpponentProfileImage)
                if url != nil{
                    //cell.imgMember.af_setImage(withURL: url!)
                    cell.imgMember.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }else{
                    cell.imgMember.image =  #imageLiteral(resourceName: "cellBackground")
                }
                cell.lblMemberName.text = objStaff.strOpponentName
                cell.imgMember.layer.cornerRadius = 30
                cell.imgMember.layer.masksToBounds = true
                
                cell.btnDeleteMember.tag = indexPath.row
                //cell.btnDeleteMember.addTarget(self, action:#selector(deleteCertificate(sender:)) , for: .touchUpInside)
                return cell
            }else{
                return UICollectionViewCell()
            }
        }
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            var cellWidth:CGFloat = 70
            var cellHeight:CGFloat = 80
            //cellWidth = CGFloat((self.collectionView.frame.size.width-10) / 2.0)
            //cellHeight = cellWidth*1.20
            return CGSize(width: cellWidth, height: cellHeight)
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let objFeed = arrSelectedMember[indexPath.row]
        }
    }
    
    
    //MARK: - Button extension
    fileprivate extension AddBroadCastMemberVC{
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        }
        
        @IBAction func btnCrossAction(_ sender: UIButton) {
            let obj = self.arrSelectedMember[sender.tag]
            
            if self.strText.count == 0{
                let index = self.arrChatHistory.index(of: obj)
                obj.strCreatUserStatus = false
                self.arrChatHistory.remove(at: index!)
                self.arrChatHistory.insert(obj, at: index!)
            }else{
                let index = self.arrChatHistoryTotal.index(of: obj)
                obj.strCreatUserStatus = false
                self.arrChatHistoryTotal.remove(at: index!)
                self.arrChatHistoryTotal.insert(obj, at: index!)
            }
            self.arrSelectedMember.remove(at: sender.tag)
            self.collectionView.reloadData()
            self.tblMemberList.reloadData()
            ////
            self.tblMemberList.contentSize.height = (self.tblMemberList.contentSize.height) + 60.0
            ////
            if arrSelectedMember.count == 0{
                self.viewCollection.isHidden = true
            }else{
                self.viewCollection.isHidden = false
            }
        }
        @IBAction func btnAddMemberAction(_ sender: UIButton){
            self.view.endEditing(true)
            var dictOfDict = [String:[String:Any]]()
            
            if self.arrSelectedMember.count == 0  {
                objAppShareData.showAlert(withMessage: "Please select any member", type: alertType.bannerDark,on: self)
            }else{
                for objUser in self.arrSelectedMember{
                    let Key = objUser.strOpponentId
                    let img = objUser.strOpponentProfileImage
                    let name = objUser.strOpponentName
                    let FT = objUser.strOpponentFireBaseToken
                    let time = ServerValue.timestamp()
                    
                    let dictOfDicts = ["createdDate":time,
                                       "firebaseToken":FT,
                                       "memberId":Key,
                                       "mute":"0",
                                       "profilePic":img,
                                       "type":"member",
                                       "userName":name] as [String : Any]
                    ref.child("broadcast").child(self.objChatHistoryModel.strOpponentId).child("member").child(Key).setValue(dictOfDicts)
                }
                
    let dictHistory = ["memberCount":self.arrSelectedMember.count+self.objChatHistoryModel.arrMemberId.count,"userName":String(self.arrSelectedMember.count+self.objChatHistoryModel.arrMemberId.count)+" Members"] as [String : Any]
                    ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(dictHistory)
                ref.child("broadcast").child(self.objChatHistoryModel.strOpponentId).updateChildValues(["groupName":String(self.arrSelectedMember.count+self.objChatHistoryModel.arrMemberId.count)+" Members"])
                        self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - tableview method extension
    extension AddBroadCastMemberVC{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrChatHistory.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCreatGroup", for: indexPath) as? CellCreatGroup{
                var objChatList = ChatHistoryData()
                if self.strText.count != 0{
                    objChatList = arrChatHistory[indexPath.row]
                }else{
                    objChatList = arrChatHistory[indexPath.row]
                }
                cell.btnProfile.tag = indexPath.row
                if objChatList.strOpponentProfileImage == ""{
                    cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                }else{
                    let url = URL(string: objChatList.strOpponentProfileImage)
                    //cell.imgUserProfile.af_setImage(withURL: url!)
                    cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }
                cell.lblUserName.text = objChatList.strOpponentName
                cell.btnChackUnchack.tag = indexPath.row
                // cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
                if objChatList.strCreatUserStatus == true{
                    cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")
                }else{
                    cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
                }
                cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
                
                return cell
            }else{
                return UITableViewCell()
            }
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
        
        @objc func btnChackUnchackAction(sender: UIButton!){
            self.view.endEditing(true)
            
            let objMyEventsData = self.arrChatHistory[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblMemberList?.cellForRow(at: indexPath) as? CellCreatGroup)!
            
            if objMyEventsData.strCreatUserStatus == false {
                objMyEventsData.strCreatUserStatus = true
                cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")
                //self.viewCollection.isHidden = false
            }else {
                objMyEventsData.strCreatUserStatus = false
                cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
            }
            
            self.arrSelectedMember.removeAll()
            self.collectionView.reloadData()
            
            for newCollection in self.arrChatHistoryTotal{
                if newCollection.strCreatUserStatus == true{
                    if !self.arrSelectedMember.contains(newCollection){
                        self.arrSelectedMember.append(newCollection)
                    }
                }
            }
            for newCollection in self.arrChatHistory{
                if newCollection.strCreatUserStatus == true{
                    if !self.arrSelectedMember.contains(newCollection){
                        self.arrSelectedMember.append(newCollection)
                    }
                }
            }
            
            if self.arrSelectedMember.count == 0{
                DispatchQueue.main.async {
                    self.viewCollection.isHidden = true
                    self.view.layoutIfNeeded()
                    self.collectionView.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                    self.viewCollection.isHidden = false
                    self.view.layoutIfNeeded()
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    //MARK: - FireBase Method calls
    extension AddBroadCastMemberVC{
        func GetUserListFromFirebase() {
            objWebserviceManager.StartIndicator()
            self.ref.child("users").observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
                guard let strongSelf = self else { return }
                print(snapshot)
                self?.arrChatHistory.removeAll()
                self?.arrChatHistoryMyData.removeAll()
                self?.arrChatHistoryTotal.removeAll()
                
                if let arr = snapshot.value as? [Any]{
                    
                    for  dicttt in arr {
                        
                        if let dict = dicttt as? [String:Any]{
                            print("\n\ndicttt = \(dict)")
                            
                            let objChatList = ChatHistoryData()
                            objChatList.strOpponentName = dict["userName"] as? String ?? ""
                            objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                            objChatList.strUserType = "member"
                            objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                            objChatList.strMuteStatus = "0"
                            objChatList.strCreatUserStatus = false
                            if let id = dict["uId"] as? Int {
                                objChatList.strOpponentId = String(id)
                            }else if let id = dict["uId"] as? String {
                                objChatList.strOpponentId = id
                            }
                            
                            var addOrNot = true
                            for id in (self?.objChatHistoryModel.arrMemberId)!{
                                print(id,objChatList.strOpponentId)
                                if id == objChatList.strOpponentId{
                                    addOrNot = false
                                }
                            }
                            if addOrNot == true{
                                if objChatList.strOpponentId == self?.strMyChatId{
                                    if objChatList.strOpponentName.count>0{
                                    self?.arrChatHistoryMyData.append(objChatList)
                                    }
                                }else{
                                    if objChatList.strOpponentName.count>0{
                                        
                                    self?.arrChatHistory.append(objChatList)
                                    self?.arrChatHistoryTotal.append(objChatList)
                                    }
                                }
                            }
                            
                        }
                        
                        objWebserviceManager.StopIndicator()
                        
                        if (self?.arrChatHistory.count)! > 0 {
                            self?.lblNoDataFound.isHidden = true
                        }else{
                            self?.lblNoDataFound.isHidden = false
                        }
                    }
                    
                    self?.tblMemberList.reloadData()
                    ////
                    self?.tblMemberList.contentSize.height = ((self?.tblMemberList.contentSize.height)!) + 60.0
                    ////
                    objWebserviceManager.StopIndicator()
                    
                    if (self?.arrChatHistory.count == 0) {
                        self?.lblNoDataFound.isHidden = false;
                        self?.tblMemberList.isHidden = true;
                    } else {
                        self?.lblNoDataFound.isHidden = true;
                        self?.tblMemberList.isHidden = false;
                    }
                    objWebserviceManager.StopIndicator()
                }else if let dict = snapshot.value as? NSDictionary {
                    strongSelf.parseDataChatHistory(dictHistory: dict)
                    objWebserviceManager.StopIndicator()
                } else {
                    objWebserviceManager.StopIndicator()
                    strongSelf.arrChatHistory.removeAll()
                    strongSelf.arrChatHistoryMyData.removeAll()
                    
                    strongSelf.tblMemberList.reloadData()
                    ////
                    strongSelf.tblMemberList.contentSize.height = (strongSelf.tblMemberList.contentSize.height) + 60.0
                    ////
                }
            })
        }
        
        func parseDataChatHistory(dictHistory:NSDictionary){
            self.arrChatHistory.removeAll()
            self.arrChatHistoryMyData.removeAll()
            
            for (key, element) in dictHistory {
                print("key = \(key)")
                print("element = \(element)")
                if let dict = element as? [String:Any]{
                    
                    let objChatList = ChatHistoryData()
                    objChatList.strOpponentName = dict["userName"] as? String ?? ""
                    objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                    objChatList.strOpponentId = String(dict["uId"] as? Int ?? 0)
                    objChatList.strUserType = "member"
                    objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                    objChatList.strMuteStatus = "0"
                    
                    
                    objWebserviceManager.StopIndicator()
                    var addOrNot = true
                    for id in self.objChatHistoryModel.arrMemberId{
                        print(id,objChatList.strOpponentId)
                        
                        if id == objChatList.strOpponentId{
                            addOrNot = false
                        }
                    }
                    if addOrNot == true{
                        if objChatList.strOpponentId == self.strMyChatId{
                            if objChatList.strOpponentName.count>0{
                            self.arrChatHistoryMyData.append(objChatList)
                            }
                        }else{
                            if objChatList.strOpponentName.count>0{
                            self.arrChatHistory.append(objChatList)
                            self.arrChatHistoryTotal.append(objChatList)
                            }
                        }
                    }
                }
                
                
                if arrChatHistory.count > 0 {
                    self.lblNoDataFound.isHidden = true
                }else{
                    self.lblNoDataFound.isHidden = false
                }
            }
            
            print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
            self.tblMemberList.reloadData()
            ////
            self.tblMemberList.contentSize.height = (self.tblMemberList.contentSize.height) + 60.0
            ////
            objWebserviceManager.StopIndicator()
            
            if (self.arrChatHistory.count == 0) {
                self.lblNoDataFound.isHidden = false;
                self.tblMemberList.isHidden = true;
            } else {
                self.lblNoDataFound.isHidden = true;
                self.tblMemberList.isHidden = false;
            }
        }
}


extension AddBroadCastMemberVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}
