//
//  ChatHistoryVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/20/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

class ChatHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    func didDismiss() {
       print("GTHalfSheet dismiss")
    }
    
    @IBOutlet weak var viewPluseMenu: UIView!
    @IBOutlet weak var viewFiltereMenu: UIView!
    @IBOutlet weak var imgFaviourite: UIImageView!
    @IBOutlet weak var viewRequestOption: UIView!
    
    @IBOutlet weak var tblChatHistory: UITableView!
    @IBOutlet weak var txtSearchUser: UITextField!
    @IBOutlet weak var lblNoDataFound: UIView!
    var strText = ""
    var isAnyGroupRequest = false
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var arrRequests = [GroupModal]()
    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [ChatHistoryData]()
    fileprivate var arrChatHistoryTextFilter = [ChatHistoryData]()
    
    fileprivate var isFaviourite:Bool = false
    fileprivate var isFirstTime:Bool = false
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
}

//MARK: - System Method extension
extension ChatHistoryVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.viewConfigure()
        self.imgFaviourite.image = #imageLiteral(resourceName: "inactive_yellow_star_ico")
        self.isFaviourite = false
        self.lblNoDataFound.isHidden = true
        self.GetRequestFromFirebase()
        //self.GetChatHistoryFromFirebase(strFilterType: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if objChatShareData.fromChatToHistory{
            objChatShareData.fromChatToHistory = false
        }
        isFirstTime = true
        objChatShareData.isOnChatScreen = false
        objChatShareData.fromChatVC = false
        
        if objAppShareData.isFromNotification {
            
            self.viewFiltereMenu.isHidden = true
            self.viewPluseMenu.isHidden = true
            let obj = ChatHistoryData.init()
            
            if let dict = objAppShareData.notificationInfoDict,let opponentChatId = dict["opponentChatId"] as? String {
                obj.strOpponentId = opponentChatId
                obj.strReceiverId = opponentChatId
                if let type = dict["type"] as? String,  type == "groupChat" {
                    obj.strUserType = "group"
                }else{
                    obj.strUserType = "user"
                }
                objChatShareData.fromChatVC = true
                self.goToChatScreenWithUser(objChatList: obj)
                return
            }
        }
        
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        objWebserviceManager.StartIndicator()
        //self.lblNoDataFound.isHidden = false
        self.GetChatHistoryFromFirebase(strFilterType: "")
        strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.ref.removeAllObservers()
        //self.clearUnreadChatHistory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: - Custome method extension
fileprivate extension ChatHistoryVC{
    func viewConfigure(){
        self.txtSearchUser.delegate = self
        self.tblChatHistory.delegate = self
        self.tblChatHistory.dataSource = self
        ref = Database.database().reference()
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
    }
}

//MARK: - Button extension
fileprivate extension ChatHistoryVC{
    @IBAction func btnBackAction(_ sender: UIButton) { self.view.endEditing(true)
        if objAppShareData.isFromNotification {
            objAppShareData.clearNotificationData()
        }
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGroupAction(_ sender: UIButton) { self.view.endEditing(true)
        
        //        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        //        if let objVC = sb.instantiateViewController(withIdentifier:"MemberRequestList") as? MemberRequestList{
        //            self.present(objVC, animated: true, completion: nil)
        //        }
    }
    
    @IBAction func btnFavouriteList(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.isFaviourite == false {
            self.isFaviourite = true
            self.imgFaviourite.image = #imageLiteral(resourceName: "starFilter_ico")
            self.GetChatHistoryFromFirebase(strFilterType: "")
        }else{
            self.isFaviourite = false
            self.imgFaviourite.image = #imageLiteral(resourceName: "inactive_yellow_star_ico")
            self.GetChatHistoryFromFirebase(strFilterType: "")
        }
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewPluseMenu.isHidden = true
        if self.viewFiltereMenu.isHidden == true{
            self.viewFiltereMenu.isHidden = false
        }else {
            self.viewFiltereMenu.isHidden = true
        }
    }
    
    @IBAction func btnPluseAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFiltereMenu.isHidden = true
        if self.viewPluseMenu.isHidden == true{
            self.viewPluseMenu.isHidden = false
        }else {
            self.viewPluseMenu.isHidden = true
        }
    }
    @IBAction func btnGroupRequestsAction(_ sender: UIButton) {
        //return
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"RequestVC") as? RequestVC{
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnCreatNewGroupAction(_ sender: UIButton) {
        //return
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CreateGroupVC") as? CreateGroupVC{
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnCreatNewBroadcastAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CreatBroadCastVC") as? CreatBroadCastVC{
            //self.present(objVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnCreatNewChatAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CreatNewChatVC") as? CreatNewChatVC{
            //self.present(objVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnJoinNewGroupAction(_ sender: UIButton) {
        //return
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"JoinGroupVC") as? JoinGroupVC{
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnAllHistoryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.GetChatHistoryFromFirebase(strFilterType: "")
    }
    @IBAction func btnAllGroupAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.GetChatHistoryFromFirebase(strFilterType: "AllGroup")
    }
    @IBAction func btnMyGroupAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.GetChatHistoryFromFirebase(strFilterType: "MyGroup")
    }
    @IBAction func btnReadAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.GetChatHistoryFromFirebase(strFilterType: "Read")
    }
    @IBAction func btnUnreadAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.GetChatHistoryFromFirebase(strFilterType: "Unread")
    }
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        if objUser.strUserType == "user"{
        }else if objUser.strUserType == "group"{
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            let detailVC = sb.instantiateViewController(withIdentifier: "GroupMemberListVC") as! GroupMemberListVC
            detailVC.objChatHistoryModel = objUser
            detailVC.modalPresentationStyle = .fullScreen
            self.present(detailVC, animated: true, completion: nil)
            return
        }else if objUser.strUserType == "broadcast"{
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            let detailVC = sb.instantiateViewController(withIdentifier: "BroadCastDetailsVC") as! BroadCastDetailsVC
            detailVC.objChatHistoryModel = objUser
            detailVC.modalPresentationStyle = .fullScreen
            self.present(detailVC, animated: true, completion: nil)
        }
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                ] as [String : Any]
            self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
                self.view.isUserInteractionEnabled = true
            }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
       /* if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}

//MARK: - tableview method extension
extension ChatHistoryVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.strText.count != 0{
            
            if arrChatHistoryTextFilter.count == 0{
                return arrChatHistoryTextFilter.count
                self.lblNoDataFound.isHidden = false
            }else{
                return arrChatHistoryTextFilter.count + 1
                self.lblNoDataFound.isHidden = true
            }
            
        }else{
            
            if arrChatHistory.count == 0{
                return arrChatHistory.count
                self.lblNoDataFound.isHidden = false
            }else{
                return arrChatHistory.count + 1
                self.lblNoDataFound.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.strText.count == 0{
            if arrChatHistory.count > indexPath.row {
            }else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CellLine", for: indexPath) as? UITableViewCell{
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
        }else{
            if arrChatHistoryTextFilter.count > indexPath.row {
            }else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CellLine", for: indexPath) as? UITableViewCell{
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatHistory", for: indexPath) as? CellChatHistory{
            
            var objChatList = ChatHistoryData()
            
            if self.strText.count == 0{
                objChatList = arrChatHistory[indexPath.row]
            }else{
                objChatList = arrChatHistoryTextFilter[indexPath.row]
            }
            
            cell.configureCellWith(objChatList : objChatList)
            cell.imgMuteStatus.isHidden = true
            
            cell.viewTopLine.isHidden = true
            //cell.viewBottomLine.isHidden = true
            
            if objChatList.strMuteStatus == "1" {
                cell.imgMuteStatus.isHidden = false
            }
            
            if indexPath.row == self.arrChatHistory.count-1{
                //cell.viewBottomLine.isHidden = false
            }else{
                //cell.viewBottomLine.isHidden = true
            }
            if indexPath.row == 0 {
                cell.viewTopLine.isHidden = true
            }else{
                cell.viewTopLine.isHidden = false
            }
            
            if indexPath.row == 0 && cell.lblDayDate.text != "Today" {
                cell.lblDayDate.isHidden = false
                cell.viewTopLine.isHidden = false
            } else if indexPath.row == 0{
                cell.lblDayDate.isHidden = true
                //cell.viewTopLine.isHidden = true
            }else{
                let objChatDay = arrChatHistory[indexPath.row - 1]
                let a = objChatDay.strDateTime
                let b = objChatList.strDateTime
                if a == b {
                    cell.lblDayDate.isHidden = true
                    //cell.viewTopLine.isHidden = true
                } else {
                    cell.lblDayDate.isHidden = false
                    cell.viewTopLine.isHidden = false
                }
            }
            
            if objChatList.strOpponentId != ""{
                if objChatList.strOpponentProfileImage == ""{
                    if objChatList.strUserType == "group" {
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "user_img_ico")
                    }else if objChatList.strUserType == "user"{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }else{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "logo1")
                    }
                }
                
                //_refHandle = self.ref.child("users").child(objChatList.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
                _refHandle = self.ref.child("users").child(objChatList.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
                    print("snapshot = \(snapshot)")
                    //guard let strongSelf = self else { return }
                    guard let dict = snapshot.value as? [String:Any] else { return }
                    
                    if self?.arrChatHistory[indexPath.row].strOpponentId != objChatList.strOpponentId {
                        return
                    }
                    
                    if objChatList.strUserType == "user" {
                        objChatList.strOpponentName = dict["userName"] as? String ?? ""
                        objChatList.strOpponentProfileImage = dict["profilePic"] as? String ?? ""
                    }
                    if objChatList.strOpponentProfileImage == ""{
                        if objChatList.strUserType == "group" {
                            cell.imgUserProfile.image = #imageLiteral(resourceName: "user_img_ico")
                        }else if objChatList.strUserType == "user"{
                            cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                        }else{
                            cell.imgUserProfile.image = #imageLiteral(resourceName: "logo1")
                        }
                    }else{
                        let url = URL(string: objChatList.strOpponentProfileImage)
                        //cell.imgUserProfile.af_setImage(withURL: url!)
                        cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                    }
                    if objChatList.strUserType == "broadcast"{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "logo1")
                    }else{
                        cell.lblUserName.text = objChatList.strOpponentName
                    }
                })
            }
            
            if objChatList.strMessageType != "1" {
                cell.lblMsg.text = objChatList.strMessage
            }else{
                cell.lblMsg.text = "Image"
            }
            if (objChatList.strUnreadMessage) >= 1 {
                cell.lblMsg.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            }else{
                cell.lblMsg.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
            self.ref.child("isTyping").child(objChatList.strOpponentId+"_"+self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
                // guard let strongSelf = self else { return }
                if self?.arrChatHistory[indexPath.row].strOpponentId != objChatList.strOpponentId || self?.arrChatHistory[indexPath.row].strUserType == "group"{
                    return
                }
                if objChatList.strMessageType != "1" {
                    cell.lblMsg.text = objChatList.strMessage
                }else{
                    cell.lblMsg.text = "Image"
                }
                if (objChatList.strUnreadMessage) >= 1 {
                    cell.lblMsg.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                }else{
                    cell.lblMsg.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                }
                
                if snapshot.exists(){
                    cell.lblMsg.text = "typing..."
                    cell.lblMsg.textColor = #colorLiteral(red: 0.1961900294, green: 0.8260042071, blue: 0.7877198458, alpha: 1)
                }else{
                    
                    if objChatList.strMessageType != "1" {
                        cell.lblMsg.text = objChatList.strMessage
                    }else{
                        cell.lblMsg.text = "Image"
                    }
                    if (objChatList.strUnreadMessage) >= 1 {
                        cell.lblMsg.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                    }else{
                        cell.lblMsg.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                    }
                }
            })
            
            
            if objChatList.strUserType == "group" {
                self.ref.child("group").child(objChatList.strReceiverId).child("member").child(self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
                    guard let strongSelf = self else { return }
                    print(snapshot.value)
                    if self?.arrChatHistory[indexPath.row].strOpponentId != objChatList.strOpponentId {
                        return
                    }
                    if let dict = snapshot.value as? NSDictionary {
                        var id = "0"
                        if let muteId = dict["mute"] as? Int{
                            id = String(muteId)
                        }else if let muteId = dict["mute"] as? String{
                            id = muteId
                        }
                        if id == "1"{
                            cell.imgMuteStatus.isHidden = false
                            //Messaging.messaging().unsubscribe(fromTopic: objChatList.strReceiverId)
                        }else{
                            cell.imgMuteStatus.isHidden = true
                            //Messaging.messaging().subscribe(toTopic: objChatList.strReceiverId)
                        }
                        
                    }
                })}else{
                self.ref.child("mute_user").child(self.strMyChatId).child(objChatList.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
                    guard let strongSelf = self else { return }
                    if let dict = snapshot.value as? [String:Any] {
                        if self?.arrChatHistory[indexPath.row].strOpponentId != objChatList.strOpponentId {
                            return
                        }
                        var id = "0"
                        if let muteId = dict["mute"] as? Int{
                            id = String(muteId)
                        }else if let muteId = dict["mute"] as? String{
                            id = muteId
                        }
                        if id == "1"{ cell.imgMuteStatus.isHidden = false }else{
                            cell.imgMuteStatus.isHidden = true
                        }
                    }
                })
            }
            cell.btnProfile.tag = indexPath.row
            if objChatList.strUserType == "broadcast"{
                cell.lblUserName.text = objChatList.strBroadCastMemberCount + " Recipients"
            }else if objChatList.strUserType == "group"{
                self.ref.child("group").child(objChatList.strReceiverId).observe(.value, with: { [weak self] (snapshot) -> Void in
                    guard let strongSelf = self else { return }
                    print(snapshot.value)
                    let dict = snapshot.value as? [String:Any]
                    cell.lblUserName.text = dict!["groupName"] as? String ?? ""
                    let strImage = dict!["groupImg"] as? String ?? ""
                    let url = URL(string: strImage)
                    //cell.imgUserProfile.af_setImage(withURL: url!)
                    cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                })
            }else{
                
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrChatHistory.count > indexPath.row {
            
        }else{
            return
        }
        
        self.viewFiltereMenu.isHidden = true
        self.viewPluseMenu.isHidden = true
        let obj = arrChatHistory[indexPath.row]  //ModelChatList
        objChatShareData.fromChatVC = true
        self.goToChatScreenWithUser(objChatList: obj)
    }
}

//MARK: - FireBase Method calls
extension ChatHistoryVC{
    
    func goToChatScreenWithUser(objChatList :ChatHistoryData ){
        
        let count = objChatShareData.chatBadgeCount - Int(objChatList.strUnreadMessage)
        let newDict = ["count" : count]
        self.ref.child("chatBadgeCount").child(self.strMyChatId).setValue(newDict)
        
        if objChatList.strUserType == "group"{
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            let detailVC = sb.instantiateViewController(withIdentifier: "GroupChatVC") as! GroupChatVC
            objChatList.strOpponentId = objChatList.strReceiverId
            detailVC.objChatHistoryModel = objChatList
            detailVC.modalPresentationStyle = .fullScreen
            self.present(detailVC, animated: true, completion: nil)
        }else  if objChatList.strUserType == "user"{
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            let detailVC = sb.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            detailVC.objChatHistoryModel = objChatList
            if objAppShareData.isFromNotification {
                detailVC.modalPresentationStyle = .fullScreen
                self.present(detailVC, animated: false, completion: nil)
            }else{
                detailVC.modalPresentationStyle = .fullScreen
                self.present(detailVC, animated: true, completion: nil)
            }
        }else if objChatList.strUserType == "broadcast"{
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            let detailVC = sb.instantiateViewController(withIdentifier: "BroadcastChatVC") as! BroadcastChatVC
            objChatList.strOpponentId = objChatList.strReceiverId
            detailVC.objChatHistoryModel = objChatList
            detailVC.modalPresentationStyle = .fullScreen
            self.present(detailVC, animated: true, completion: nil)
        }
    }
    
    func GetChatHistoryFromFirebase(strFilterType:String) {
        self.viewFiltereMenu.isHidden = true
        //objWebserviceManager.StartIndicator()
        _refHandle = self.ref.child("chat_history").child(self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
            
            guard let strongSelf = self else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                return
            }
            print(snapshot)
            
            if objChatShareData.fromChatVC == true{
                objWebserviceManager.StopIndicator()
                return
            }
            
            print("\n\n\(String(describing: snapshot.key))")
            print("\n\n\(snapshot.value)")
            self?.arrChatHistory.removeAll()
            if let arr = snapshot.value as? [Any]{
                
                for dicttt in arr {
                    
                    if let dict = dicttt as? NSDictionary{
                        print("\n\ndicttt = \(dict)")
                        
                        // objChatList.strOpponentId = String(describing: key)
                        
                        //let objChatList = ChatHistoryData.init(dict: dict,key: key)
                        let objChatList = ChatHistoryData()
                        objChatList.strMessage = dict["message"] as? String ?? ""
                        objChatList.strOpponentName = dict["userName"] as? String ?? ""
                        objChatList.strUserType = dict["type"] as? String ?? ""
                        objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                        if let count = dict["memberCount"] as? Int {
                            objChatList.strBroadCastMemberCount = String(count)
                        }
                        if let count = dict["memberCount"] as? String {
                            objChatList.strBroadCastMemberCount = count
                        }
                        
                        if let msgType = dict["messageType"] as? String {
                            objChatList.strMessageType = msgType
                        }else if let msgType = dict["messageType"] as? Int{
                            objChatList.strMessageType = String(msgType)
                        }
                        
                        if let reciverId = dict["reciverId"] as? String {
                            objChatList.strReceiverId = reciverId
                            
                        }else if let reciverId = dict["reciverId"] as? Int{
                            objChatList.strReceiverId = String(reciverId)
                            
                        }
                        
                        if let favourite = dict["favourite"] as? String {
                            objChatList.strFavouriteStatus = favourite
                        }else if let favourite = dict["favourite"] as? Int{
                            objChatList.strFavouriteStatus = String(favourite)
                        }
                        
                        if let senderId = dict["senderId"] as? String {
                            objChatList.strSenderId = senderId
                        }else if let senderId = dict["senderId"] as? Int{
                            objChatList.strSenderId = String(senderId)
                        }
                        if self?.strMyChatId == objChatList.strSenderId{
                            objChatList.strOpponentId = objChatList.strReceiverId
                        }else{
                            objChatList.strOpponentId = objChatList.strSenderId
                        }
                        if let unreadMessage = dict["unreadMessage"] as? String {
                            var a = unreadMessage
                            if a == "" {
                                a = "0"
                            }
                            objChatList.strUnreadMessage = Int(a)!
                        }else if let unreadMessage = dict["unreadMessage"] as? Int{
                            objChatList.strUnreadMessage = unreadMessage
                        }
                        
                        objChatList.TimeStamp = dict["timestamp"] as? Double ?? 0
                        
                        let date = Date(timeIntervalSince1970: objChatList.TimeStamp / 1000.0)
                        let date_1 = self?.dateFormatter.string(from: date)
                        let timeFormatter = DateFormatter()
                        timeFormatter.dateFormat = "hh:mm a"
                        let Time = timeFormatter.string(from: date)
                        objChatList.strDate = Time
                        
                        objChatList.strDateTime = objChatShareData.getMsgDay(timeStamp:date_1!)
                        
                        self?.FilterValueInArray(filterType: strFilterType, objChatData: objChatList)
                        //self?.arrChatHistory.append(objChatList)
                    }
                    objWebserviceManager.StopIndicator()
                    
                    if (self?.arrChatHistory.count)! > 0 {
                        self?.lblNoDataFound.isHidden = true
                    }else{
                        self?.lblNoDataFound.isHidden = false
                    }
                }
                
                if (self?.arrChatHistory.count)! > 0{
                    ((self?.arrChatHistory = (self?.arrChatHistory.sorted {
                        (lhs, rhs) in lhs.TimeStamp > rhs.TimeStamp
                        })!) != nil)
                }
                print("self.arrChatHistory.count = \(self?.arrChatHistory.count)")
                self?.filterDataFromMainArrayToLocalArray()
                self?.tblChatHistory.reloadData()
                
                //let indexPath = IndexPath(row: 0, section: 0)
                //self?.tblChatHistory.scrollToRow(at: indexPath, at: .bottom, animated: false)
                self?.tableViewScrollToBottom(animated: true)
                objWebserviceManager.StopIndicator()
                
                UIView.animate(withDuration: 0.4, animations: {
                    if (self?.arrChatHistory.count == 0) {
                        self?.lblNoDataFound.isHidden = false;
                        self?.tblChatHistory.isHidden = true;
                    } else {
                        self?.lblNoDataFound.isHidden = true;
                        self?.tblChatHistory.isHidden = false;
                    }
                })
                
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(filterType: strFilterType, dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                strongSelf.arrChatHistory.removeAll()
                strongSelf.tblChatHistory.reloadData()
            }
        })
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(0)) {
            let numberOfSections = self.tblChatHistory.numberOfSections
            let numberOfRows = self.tblChatHistory.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                //let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                let indexPath = IndexPath(row: 0, section: (0))
                self.tblChatHistory.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                self.tblChatHistory.isHidden = false
            }
        }
    }
    func parseDataChatHistory(filterType:String,dictHistory:NSDictionary){
        
        self.arrChatHistory.removeAll()
        
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                
                let objChatList = ChatHistoryData()
                objChatList.strMessage = dict["message"] as? String ?? ""
                objChatList.strOpponentName = dict["userName"] as? String ?? ""
                objChatList.strOpponentId = String(describing: key)
                objChatList.strUserType = dict["type"] as? String ?? ""
                objChatList.strOpponentFireBaseToken  = dict["firebaseToken"] as? String ?? ""
                objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                if let count = dict["memberCount"] as? Int {
                    objChatList.strBroadCastMemberCount = String(count)
                }
                if let count = dict["memberCount"] as? String {
                    objChatList.strBroadCastMemberCount = count
                }
                if let msgType = dict["messageType"] as? String {
                    objChatList.strMessageType = msgType
                }else if let msgType = dict["messageType"] as? Int{
                    objChatList.strMessageType = String(msgType)
                }
                
                if let reciverId = dict["reciverId"] as? String {
                    objChatList.strReceiverId = reciverId
                }else if let reciverId = dict["reciverId"] as? Int{
                    objChatList.strReceiverId = String(reciverId)
                }
                objChatList.strMemberType = dict["memberType"] as? String ?? ""
                
                if let senderId = dict["senderId"] as? String {
                    objChatList.strSenderId = senderId
                }else if let senderId = dict["senderId"] as? Int{
                    objChatList.strSenderId = String(senderId)
                }
                if self.strMyChatId == objChatList.strSenderId{
                    objChatList.strOpponentId = objChatList.strReceiverId
                }else{
                    objChatList.strOpponentId = objChatList.strSenderId
                }
                
                if let favourite = dict["favourite"] as? String {
                    objChatList.strFavouriteStatus = favourite
                }else if let favourite = dict["favourite"] as? Int{
                    objChatList.strFavouriteStatus = String(favourite)
                }
                if let unreadMessage = dict["unreadMessage"] as? String {
                    var a = unreadMessage
                    if a == "" {
                        a = "0"
                    }
                    objChatList.strUnreadMessage = Int(a)!
                }else if let unreadMessage = dict["unreadMessage"] as? Int{
                    objChatList.strUnreadMessage = unreadMessage
                }
                
                objChatList.TimeStamp = dict["timestamp"] as? Double ?? 0
                
                let date = Date(timeIntervalSince1970: objChatList.TimeStamp / 1000.0)
                let date_1 = dateFormatter.string(from: date)
                let timeFormatter = DateFormatter()
                timeFormatter.dateFormat = "hh:mm a"
                let Time = timeFormatter.string(from: date)
                objChatList.strDate = Time
                
                objChatList.strDateTime = objChatShareData.getMsgDay(timeStamp:date_1)
                
                self.FilterValueInArray(filterType: filterType, objChatData: objChatList)
            }
            objWebserviceManager.StopIndicator()
            
            if arrChatHistory.count > 0 {
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
        }
        
        self.arrChatHistory = self.arrChatHistory.sorted {
            (lhs, rhs) in lhs.TimeStamp > rhs.TimeStamp
        }
        
        print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
        self.filterDataFromMainArrayToLocalArray()
        
        self.tblChatHistory.reloadData()
        self.tableViewScrollToBottom(animated: true)
        objWebserviceManager.StopIndicator()
        
        UIView.animate(withDuration: 0.4, animations: {
            if (self.arrChatHistory.count == 0) {
                self.lblNoDataFound.isHidden = false;
                self.tblChatHistory.isHidden = true;
            } else {
                self.lblNoDataFound.isHidden = true;
                self.tblChatHistory.isHidden = false;
            }
        })
    }
    
    func FilterValueInArray(filterType:String,objChatData:ChatHistoryData){
        if self.isFaviourite == true {
            let favStatus = objChatData.strFavouriteStatus
            if favStatus != "1"{
                return
            }
        }
        if filterType == "" || filterType == "All"{
            self.arrChatHistory.append(objChatData)
        }else if filterType == "AllGroup"{
            if objChatData.strUserType == "group"{
                self.arrChatHistory.append(objChatData)
            }
        }else if filterType == "MyGroup"{
            if objChatData.strUserType == "group"{
                if objChatData.strMemberType != "member"{
                    self.arrChatHistory.append(objChatData)
                }}
        }else if filterType == "Read"{
            if objChatData.strUnreadMessage == 0{
                self.arrChatHistory.append(objChatData)
            }
        }else if filterType == "Unread"{
            if objChatData.strUnreadMessage != 0{
                self.arrChatHistory.append(objChatData)
            }
        }else if filterType == "Favourite"{
            if objChatData.strFavouriteStatus == "1"{
                self.arrChatHistory.append(objChatData)
            }
        }
    }
}



extension ChatHistoryVC{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSearchUser{
            txtSearchUser.resignFirstResponder()
        }
        return true
    }
    
    internal func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.viewPluseMenu.isHidden = true
        self.viewFiltereMenu.isHidden = true
        return true
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let arrAny:[Any] = self.arrChatHistory
        arrChatHistoryTextFilter.removeAll()
        let text = textField.text! as NSString
        
        //    if (text.length == 1)  && (string == "") {
        //    strText = ""
        //    self.tblChatHistory.reloadData()
        //    }else{
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        //var bPredicate: NSPredicate = NSPredicate(format: "SELF.strOpponentName contains[cd] %@", substring)
        let filteredArray = self.arrChatHistory.filter(){ $0.strOpponentName.localizedCaseInsensitiveContains(substring) }
        NSLog("HERE %@", filteredArray)
        strText = substring
        arrChatHistoryTextFilter = filteredArray
        if arrChatHistoryTextFilter.count == 0 && strText.count != 0{
            self.lblNoDataFound.isHidden = false
            self.tblChatHistory.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = true
            self.tblChatHistory.isHidden = false
        }
        self.tblChatHistory.reloadData()
        // }
        return  true
    }
    
    
    
    
    func filterDataFromMainArrayToLocalArray(){
        if strText.count != 0{
            let filteredArray = self.arrChatHistory.filter(){ $0.strOpponentName.localizedCaseInsensitiveContains(self.txtSearchUser.text!) }
            NSLog("HERE %@", filteredArray)
            strText = self.txtSearchUser.text!
            arrChatHistoryTextFilter = filteredArray
            if arrChatHistoryTextFilter.count == 0 && strText.count != 0{
                self.lblNoDataFound.isHidden = false
                self.tblChatHistory.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = true
                self.tblChatHistory.isHidden = false
            }
        }
    }
    
    func GetRequestFromFirebase() {
        strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.ref.child("group_request").child(self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else {
                self?.viewRequestOption.isHidden = true;
                return
            }
            self?.arrRequests.removeAll()
            if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseRequest(dictHistory: dict)
            } else {
                strongSelf.isAnyGroupRequest = false
                strongSelf.arrRequests.removeAll()
                self?.viewRequestOption.isHidden = true;
            }
        })
    }
    
    func parseRequest(dictHistory:NSDictionary){
        //// group_request
        self.arrRequests.removeAll()
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                let objChatList = GroupModal()
                let lastActivity = dict["timestamp"] as? Double ?? 0
                objChatList.timestamp = lastActivity
                self.arrRequests.append(objChatList)
                if (self.arrRequests.count == 0) {
                    self.viewRequestOption.isHidden = true;
                    self.isAnyGroupRequest = false
                } else {
                    self.viewRequestOption.isHidden = false;
                    self.isAnyGroupRequest = true
                }
            }
        }
    }
    
    @IBAction func showScrollingNCPlusButton() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Select Option"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        if self.isAnyGroupRequest{
            objAppShareData.arrTBottomSheetModal = ["Create New Chat","Create New Broadcast","Create New Group","Join New Group","Group Requests"]
        }else{
            objAppShareData.arrTBottomSheetModal = ["Create New Chat","Create New Broadcast","Create New Group","Join New Group"]
        }
        scrollingNC.callback = { [weak self] (str) in
                guard let strongSelf = self else {
                    return
                }
                let btn = UIButton()
                print(str)
                if str == "0"{
                    strongSelf.btnCreatNewChatAction(btn)
                }else if str == "1"{
                    strongSelf.btnCreatNewBroadcastAction(btn)
                }else if str == "2"{
                    strongSelf.btnCreatNewGroupAction(btn)
                }else if str == "3"{
                    strongSelf.btnJoinNewGroupAction(btn)
                }else if str == "4"{
                    strongSelf.btnGroupRequestsAction(btn)
                }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as! UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Select Option"
        if self.isAnyGroupRequest{
            objAppShareData.arrTBottomSheetModal = ["Create New Chat","Create New Broadcast","Create New Group","Join New Group","Group Requests"]
        }else{
            objAppShareData.arrTBottomSheetModal = ["Create New Chat","Create New Broadcast","Create New Group","Join New Group"]
        }
        
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            let btn = UIButton()
            print(str)
            if str == "0"{
                strongSelf.btnCreatNewChatAction(btn)
            }else if str == "1"{
                strongSelf.btnCreatNewBroadcastAction(btn)
            }else if str == "2"{
                strongSelf.btnCreatNewGroupAction(btn)
            }else if str == "3"{
                strongSelf.btnJoinNewGroupAction(btn)
            }else if str == "4"{
                strongSelf.btnGroupRequestsAction(btn)
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }

    @IBAction func showScrollingNCFilterButton() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Select Option"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        objAppShareData.arrTBottomSheetModal = ["All","All Group","My Group","Read","Unread"]
        scrollingNC.callback = { [weak self] (str) in
                guard let strongSelf = self else {
                    return
                }
                let btn = UIButton()
                if str == "0"{
                    strongSelf.btnAllHistoryAction(btn)
                }else if str == "1"{
                    strongSelf.btnAllGroupAction(btn)
                }else if str == "2"{
                    strongSelf.btnMyGroupAction(btn)
                }else if str == "3"{
                    strongSelf.btnReadAction(btn)
                }else if str == "4"{
                    strongSelf.btnUnreadAction(btn)
                }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as! UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Select Option"
        objAppShareData.arrTBottomSheetModal = ["All","All Group","My Group","Read","Unread"]
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            let btn = UIButton()
            if str == "0"{
                strongSelf.btnAllHistoryAction(btn)
            }else if str == "1"{
                strongSelf.btnAllGroupAction(btn)
            }else if str == "2"{
                strongSelf.btnMyGroupAction(btn)
            }else if str == "3"{
                strongSelf.btnReadAction(btn)
            }else if str == "4"{
                strongSelf.btnUnreadAction(btn)
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    
}
