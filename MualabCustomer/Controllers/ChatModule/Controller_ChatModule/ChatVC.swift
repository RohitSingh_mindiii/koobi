//
//  ChatVC.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage
import Kingfisher

//BlockUser
enum BlockStatus {
    case kBlockedByMe
    case kBlockedByOpponent
    case kBlockedByBoth
    case kBlockedByNone
}

//BlockUser

var imageData: Data?

class ChatVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate{
    
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache:NSCache<AnyObject, AnyObject>!
    
    var objChatHistoryModel = ChatHistoryData()
    var indexOfArray = 0
    var objHoldIndex = ChatData.init(dict: [:], key: "")
    @IBOutlet weak var txtViewContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var txtViewHeight: NSLayoutConstraint!
    
    //MARK: - Outlat declaration
    @IBOutlet weak var lblFavouriteStatus: UILabel!
    @IBOutlet weak var lblMuteStatus: UILabel!
    @IBOutlet weak var viewZoomImg: UIView!
    @IBOutlet weak var imgZoomImage: UIImageView!
    @IBOutlet weak var scrollViewImg: UIScrollView!
    @IBOutlet weak var lblOpponentName: UILabel!
    @IBOutlet weak var viewNoChatFound: UIView!
    
    @IBOutlet weak var lblDeleteUserName: UILabel!
    
    @IBOutlet weak var lblBlock_UnblockMsg: UILabel!
    @IBOutlet weak var lblBlock_UnblockUserName: UILabel!
    @IBOutlet weak var imgOpponent: UIImageView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var tableChat: UITableView!
    @IBOutlet weak var viewCommentAction: UIView!
    @IBOutlet weak var viewEditCommentAction: UIView!
    @IBOutlet weak var viewFor_Block_Delete: UIView!
    @IBOutlet weak var viewForBlock: UIView!
    
    @IBOutlet weak var lblBlock_Unblock: UILabel!
    
    @IBOutlet weak var viewForDelete: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    // @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtViewChat: UITextView!
    @IBOutlet weak var viewBottomChat: UIView!
    //@IBOutlet weak var bottem: NSLayoutConstraint!
    @IBOutlet weak var lblOnlineStatus: UILabel!
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    fileprivate let dateToday = Date()
    fileprivate let formatter = DateFormatter()
    fileprivate var resultDate = ""
    fileprivate var strMuteStatusForOtherNotification = ""
    
    //MARK: - Variable declaration
    // var isFromDeleteSingleMsg:Bool = false
    fileprivate var updateReadStatus = false
    fileprivate var indexToHighlight:Int = -1
    fileprivate var arrIndexToHighlight:[Int] = []
    
    fileprivate var imgType:Int = 0
    
    fileprivate var isPullToRefresh = false
    fileprivate var isImagePicked:Bool = false
    fileprivate var isImageLocallyUploading:Bool = false
    fileprivate var isSendButtonClicked:Bool = false
    var isFirstTimeScrollToBottom = false
    fileprivate var dictionaryMessages =  [String: [String:Any]]()
    
    fileprivate var storageRef: StorageReference!
    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    //BlockUser
    fileprivate var blockedBy: BlockStatus = BlockStatus.kBlockedByNone
    //BlockUser
    var isLastMsg = false
    var isLoading = false
    //Video call
    fileprivate var isOnChatScreen : Bool = false
    
    fileprivate var isFromBlock = false
    
    @IBOutlet weak var txtViewContainer: UIView!
    
    fileprivate var TimeStamp:Double = 0
    fileprivate var strKeyValueForHold:String = ""
    fileprivate var arrKeyValueForHold = [String]()

    fileprivate var strTextNotification:String = ""
    fileprivate var strName:String = ""
    fileprivate var profilePic:String = ""
    fileprivate var strMyChatId:String = ""
    
    fileprivate var isChatAppearFirst:Bool = false
    
    fileprivate let txtViewMsgMaxHeight: CGFloat = 100
    fileprivate let txtViewMsgMinHeight: CGFloat = 39
    
    fileprivate let imagePicker = UIImagePickerController()
    private var tap: UITapGestureRecognizer!
    
    fileprivate var arrMessages = [ChatData]()
    fileprivate var message = ""
    fileprivate var imgUrl = ""
    
    fileprivate var isTableScrollToTop:Bool = false
    fileprivate var lastContentOffset: CGFloat = 0
    fileprivate var strPaginationKey:String = ""
    
    fileprivate let myChatRoom =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    
    lazy var messagesRef: DatabaseReference = Database.database().reference().child("chat").child(myChatRoom)
    var newMessageRefHandle: DatabaseHandle?
    
    //MARK: - System Method Extension
    override func viewDidLoad() {
        super.viewDidLoad()
        
        session = URLSession.shared
        self.cache = NSCache()

        self.view.endEditing(true)
        self.viewCommentAction.isHidden = true
        self.viewEditCommentAction.isHidden = true
        self.registerForKeyboardNotifications()
        self.txtViewContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.txtViewContainer.layer.borderWidth = 1
        
        objChatShareData.isOnChatScreen = true
        ref = Database.database().reference()
        self.delegateColling()
        self.dataParseFromDidload()
        self.observeMessagesOpponentChat()
        self.txtViewChat.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtViewChat.tintColorDidChange()
        
        //        self.viewConfigure()
        //        self.observeKeyboard()
        //        self.configureStorage()
        //        self.configureView()
        //        self.clearUnreadChatHistory()
        //        self.getOpponentInfo()
        //        self.checkBlockStatusForThisChatRoom()
        //        self.muteStatus()
        //        self.observeMessages(lastMessageKey: nil)
        objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
        ////
        NotificationCenter.default.addObserver(self, selector: #selector(self.deletePendingUploadedImage), name: NSNotification.Name(rawValue: "deletePendingUploadedImage"), object: nil)
        ////
    }
    @objc func deletePendingUploadedImage(_ objNotify: Notification) {
        print("123456789")
        if self.arrKeyValueForHold.count>0{
        print("987654321")
        self.strKeyValueForHold = self.arrKeyValueForHold[0]
            let ref = Database.database().reference()
            ref.child("chat").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(self.strKeyValueForHold).removeValue()
            ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).child(self.strKeyValueForHold).removeValue()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        
        if !self.isImagePicked{
            self.viewConfigure()
            self.observeKeyboard()
            self.configureStorage()
            self.configureView()
            self.clearUnreadChatHistory()
            self.getOpponentInfo()
            self.checkBlockStatusForThisChatRoom()
            self.muteStatus()
            self.observeMessages(lastMessageKey: nil)
            self.getOpponentDetailsFromFirebase()
        }else{
            objWebserviceManager.StopIndicator()
        }
        
        self.isFirstTimeScrollToBottom = true
        objChatShareData.currentOpponantIDForNotification = objChatHistoryModel.strOpponentId
        self.isOnChatScreen = true
        self.isChatAppearFirst = true
        
        self.view.endEditing(true)
        //getOpponentDetailsFromFirebase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        objAppShareData.setView(view: self.viewMsgDay, hidden: true)
        self.IsTypingStatus()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.isOnChatScreen = false
        self.ref.removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        self.deregisterFromKeyboardNotifications()
    }

    //MARK: - refreshControl
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ChatVC.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.isFromDeleteSingleMsg = false
        
         if self.arrMessages.count > 0{
         self.refreshControl.beginRefreshing()
         let obj = self.arrMessages[0]
         if self.strPaginationKey != obj.str_Key {
         self.observeMessages(lastMessageKey: obj.str_Key)
         }else{
         self.refreshControl.endRefreshing()
         }
         }
         self.refreshControl.endRefreshing()
         isPullToRefresh = true
        
    }
    
}
//MARK: - Custome Method Extension
extension ChatVC {
    
    func viewConfigure(){
        //self.tableChat.addSubview(self.refreshControl)
        self.viewMsgDay.isHidden = true
        self.isChatAppearFirst = true
        //self.lblPlaceholder.isHidden = true
        self.viewNoChatFound.isHidden = true
        self.viewFor_Block_Delete.isHidden = true
        self.viewMenu.isHidden = true
        
        self.txtViewChat.layer.cornerRadius = 18
        //self.txtViewChat.layer.borderWidth = 1
        //self.txtViewChat.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    func dataParseFromDidload(){
        
        formatter.dateFormat = "dd-MMMM-yyyy"
        self.resultDate = formatter.string(from: dateToday)
        
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.lblOpponentName.text = objChatHistoryModel.strOpponentName
        self.lblBlock_UnblockUserName.text = objChatHistoryModel.strOpponentName+"?"
        self.lblDeleteUserName.text = objChatHistoryModel.strOpponentName+"?"
        
        if self.objChatHistoryModel.strOpponentProfileImage == ""{
            self.imgOpponent.image = #imageLiteral(resourceName: "cellBackground")
        }else{
            let url = URL(string: self.objChatHistoryModel.strOpponentProfileImage)
            //self.imgOpponent.af_setImage(withURL: url!)
            self.imgOpponent.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
        }
        self.strName = UserDefaults.standard.string(forKey: UserDefaults.keys.myName) ?? ""
        self.profilePic = UserDefaults.standard.string(forKey: UserDefaults.keys.myImage) ?? ""
    }
    
    func delegateColling(){
        
        self.tableChat.delegate = self
        self.tableChat.dataSource = self
        self.scrollViewImg.minimumZoomScale = 1.0
        self.scrollViewImg.maximumZoomScale = 5.0
        self.scrollViewImg.delegate = self
        self.txtViewChat.delegate = self
        self.txtViewChat.isScrollEnabled = true
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgZoomImage
    }
}

//MARK: - Block User Methods
extension ChatVC {
    
    func checkBlockStatusForThisChatRoom() {
        
        var BlockRoom = ""
        BlockRoom = self.objChatHistoryModel.strOpponentId+"_"+self.strMyChatId
        var miId = 0
        miId = Int(self.strMyChatId)!
        var oopId = 0
        oopId = Int(objChatHistoryModel.strOpponentId ) ?? 0
        if miId <= oopId{
            BlockRoom = self.strMyChatId+"_"+self.objChatHistoryModel.strOpponentId
        }
        
        _refHandle = self.ref.child("block_users").child(BlockRoom).observe(.value, with: {
            [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.setBlockStatusWithData(snapshot: snapshot)
        })
    }
    
    func setBlockStatusWithData(snapshot:DataSnapshot){
        var strBlockedBy = ""
        self.lblOnlineStatus.isHidden = false
        if let dict = snapshot.value as? [String:Any]{
            strBlockedBy =  dict["blockedBy"] as? String ?? ""
            switch strBlockedBy{
            case strMyChatId:
                self.blockedBy = BlockStatus.kBlockedByMe
                self.lblBlock_Unblock.text = "Unblock User"
                objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Unblock User"]
                self.lblBlock_UnblockMsg.text = "Are you want to unblock?"
                self.lblOnlineStatus.isHidden = false
                self.imgOpponent.isHidden = false
                
            case self.objChatHistoryModel.strOpponentId:
                self.blockedBy =
                    BlockStatus.kBlockedByOpponent
                self.lblBlock_Unblock.text = "Block User"
                objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
                self.lblBlock_UnblockMsg.text = "Are you want to block?"
                self.lblOnlineStatus.isHidden = true
                self.imgOpponent.isHidden = true
                
            case "Both":
                self.blockedBy = BlockStatus.kBlockedByBoth
                self.lblBlock_Unblock.text = "Unblock User"
                objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Unblock User"]
                self.lblBlock_UnblockMsg.text = "Are you want to unblock?"
                self.lblOnlineStatus.isHidden = true
                self.imgOpponent.isHidden = true
                
            default:
                self.blockedBy = BlockStatus.kBlockedByNone
                objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
                self.lblBlock_Unblock.text = "Block User"
                self.lblBlock_UnblockMsg.text = "Are you want to block?"
                self.lblOnlineStatus.isHidden = false
                self.imgOpponent.isHidden = false
                
            }
        }
        else {
            self.blockedBy = BlockStatus.kBlockedByNone
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
            self.lblBlock_Unblock.text = "Block"
            self.lblOnlineStatus.isHidden = false
            self.imgOpponent.isHidden = false
        }
        self.updateBlockUnblockButtonUI()
    }
    
    func updateBlockUnblockButtonUI(){
        if self.blockedBy == BlockStatus.kBlockedByMe || self.blockedBy == BlockStatus.kBlockedByBoth {
            //unblock image
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Unblock User"]
            self.lblBlock_Unblock.text = "Unblock User"
            self.lblBlock_UnblockMsg.text = "Are you want to unblock?"
        }else{
            //block image
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
            self.lblBlock_Unblock.text = "Block User"
            self.lblBlock_UnblockMsg.text = "Are you want to block?"
        }
    }
    
    func changeBlockStatusOfOpponent(){
        if self.blockedBy == BlockStatus.kBlockedByNone || self.blockedBy == BlockStatus.kBlockedByOpponent {
            self.lblBlock_Unblock.text = "Block User"
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
            self.lblBlock_UnblockMsg.text = "Are you want to block?"
            self.blockOpponent()
        }else if self.blockedBy == BlockStatus.kBlockedByMe || self.blockedBy == BlockStatus.kBlockedByBoth {
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Unblock User"]
            self.lblBlock_Unblock.text = "Unblock User"
            self.lblBlock_UnblockMsg.text = "Are you want to unblock?"
            self.unblockOpponent()
        }
    }
    
    func blockOpponent(){
        var dict = [NSString : Any]()
        if self.blockedBy == BlockStatus.kBlockedByOpponent  {
            dict = [ "blockedBy":checkForNULL(obj: "Both") as! String ]
        }else{
            dict = [ "blockedBy":checkForNULL(obj: strMyChatId) as! String  ]
        }
        
        var BlockRoom = ""
        BlockRoom = self.objChatHistoryModel.strOpponentId+"_"+self.strMyChatId
        var miId = 0
        miId = Int(self.strMyChatId)!
        var oopId = 0
        oopId = Int(objChatHistoryModel.strOpponentId)!
        if miId <= oopId{
            BlockRoom = self.strMyChatId+"_"+self.objChatHistoryModel.strOpponentId
        }
        self.ref.child("block_users").child(BlockRoom).setValue(dict)
    }
    
    func unblockOpponent(){
        var dict = [NSString : Any]()
        
        var BlockRoom = ""
        BlockRoom = self.objChatHistoryModel.strOpponentId+"_"+self.strMyChatId
        var miId = 0
        miId = Int(self.strMyChatId)!
        var oopId = 0
        oopId = Int(objChatHistoryModel.strOpponentId)!
        if miId <= oopId{
            BlockRoom = self.strMyChatId+"_"+self.objChatHistoryModel.strOpponentId
        }
        
        if self.blockedBy == BlockStatus.kBlockedByBoth  {
            dict = [
                "blockedBy":checkForNULL(obj: self.objChatHistoryModel.strOpponentId) as! String
            ]
            self.ref.child("block_users").child(BlockRoom).setValue(dict)
            
        }else{
            self.ref.child("block_users").child(BlockRoom).setValue(nil)
            
        }
        self.ref.child("block_users").child(BlockRoom).setValue(dict)
    }
}

//MARK:- Get Opponent details
extension ChatVC{
    
    func getOpponentDetailsFromFirebase() {
        objWebserviceManager.StartIndicator()
        
        // Listen for new messages in the Firebase database
        _refHandle = self.ref.child("users").child(self.objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
            self?.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            guard let strongSelf = self else { return }
            if let dict = snapshot.value as? [String:Any]{
                strongSelf.parseDataOpponentDetail(dictResponce: dict)
            } else {
                if !(self?.isImagePicked)! {
                    objWebserviceManager.StopIndicator()
                }
            }
        })
    }
    
    
    func parseDataOpponentDetail(dictResponce:[String:Any]){
        //print("dictResponce = \(dictResponce)")
        if let dict = dictResponce as? [String:Any]{
            self.lblOpponentName.text = dict["userName"] as? String ?? ""
            
            objChatHistoryModel.strOpponentName = dict["userName"] as? String ?? ""
            objChatHistoryModel.strOpponentProfileImage = dict["profilePic"] as? String ?? ""
            objChatHistoryModel.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
            objChatHistoryModel.strOpponentIsLogin = dict["isOnline"] as? Int ?? 0
            self.lblOnlineStatus.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            if let status = dict["isOnline"] as? String {
                self.lblOnlineStatus.text = status
                objChatHistoryModel.strOpponentOnlineStatus = status
            }else if let status1 = dict["isOnline"] as? Int {
                self.lblOnlineStatus.text = String(status1)
                objChatHistoryModel.strOpponentOnlineStatus = String(status1)
            }
            if self.lblOnlineStatus.text == "1"{
                self.lblOnlineStatus.text = "Online"
                objChatHistoryModel.strOpponentOnlineStatus = "Online"
            }else{
                let lastActivity = dict["lastActivity"] as? Double ?? 0
                let date = Date(timeIntervalSince1970: lastActivity / 1000.0)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
                dateFormatter.timeZone = NSTimeZone.local
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                
                self.lblOnlineStatus.text = objChatShareData.timeAgoSinceDate(date: date as NSDate, numericDates: true)
                objChatHistoryModel.strOpponentOnlineStatus = objChatShareData.timeAgoSinceDate(date: date as NSDate, numericDates: true)
            }
            if !isImagePicked {
                //objWebserviceManager.StopIndicator()
            }
        }
        
        if isImagePicked == false {
            //objWebserviceManager.StopIndicator()
        }
    }
    
    func configureStorage() {
        ref =  Database.database().reference()
        if WebURL.BaseUrl.contains("dev"){
            storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
        }else{
           storageRef = Storage.storage().reference().child("gs://koobi-89a2d.appspot.com")
        }
    }
    
    func configureView() {
        imagePicker.delegate = self
        self.tableChat.delegate = self
        self.tableChat.dataSource = self
        tableChat.rowHeight = UITableView.automaticDimension
        self.viewZoomImg.isHidden = true
        
        tableChat.estimatedRowHeight = 50.0
        tableChat.rowHeight = UITableView.automaticDimension
    }
    
    
    func getOpponentInfo(){
        _refHandle = self.ref.child("users").child(objChatHistoryModel.strOpponentId).observe(.value, with:{ [weak self] (snapshot) -> Void in
            ////print("snapshot = \(snapshot)")
            guard let strongSelf = self else { return }
            guard let dict = snapshot.value as? [String:Any] else { return }
            
            var strImage:String = ""
            strImage = dict["profilePic"] as? String ?? ""
            self?.lblOpponentName.text = dict["userName"] as? String ?? ""
            
            if strImage == ""{
                self?.imgOpponent.image = UIImage(named:"ico_user_placeholder") ?? nil
            }else{
                let url = URL(string: strImage)
                //self?.imgOpponent.af_setImage(withURL: url!)
                self?.imgOpponent.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }
        })
    }
}

//MARK:- Get MyChat data methods
extension ChatVC{
   
    func observeMessages(lastMessageKey:String?) {
//        let messageQuery = self.ref.child("chat").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).queryEnding(atValue: nil, childKey: lastMessageKey).queryLimited(toLast: 50)
//        newMessageRefHandle = messageQuery.observe(.value, with: { (snapshot) -> Void in
        
  self.ref.child("chat").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self]  (snapshot)  -> Void in
    
            if objChatShareData.fromChatVC != true{
                objWebserviceManager.StopIndicator()
                return
            }
            
            self?.updateReadStatus = true
            // self.observeMessagesOpponentChat()
            ////print("snapshot = \(snapshot)")
            self?.strPaginationKey = lastMessageKey ?? ""
            let dictKey = snapshot.key
            if let dict = snapshot.value as? [String:Any]{
                //print(dict)
                self?.parseChatData(dictN: dict)
            } else {
                objAppShareData.isFromDidFinish = false
                self?.arrMessages.removeAll()
                self?.tableChat.reloadData()
                self?.refreshControl.endRefreshing()
                //if !((self?.isImagePicked)!) {
                objWebserviceManager.StopIndicator()
                //}
            }
        })
    }
    
    func parseChatData(dictN:[String:Any]){
        //// Old without pagination
        if self.arrMessages.count>0{
            let obj = self.arrMessages[self.arrMessages.count-1]
            if obj.strMessage == "https://local" && obj.imageLocal == #imageLiteral(resourceName: "gallery_placeholder"){
            }else{
                if self.arrMessages.count == dictN.count{
                    if obj.strMessage.hasPrefix("content://com.google.android"){
                    }else{
                      return
                    }
                }
            }
        }
        
        self.arrMessages.removeAll()
        for dictxxx in dictN{
            let strKey = dictxxx.key
            let dict = dictxxx.value as! [String:Any]
            
            let objChatList = ChatData.init(dict: dict,key:strKey)
            //print(dict)
            //self.arrMessages.append(objChatList!)
            
            if self.arrMessages.contains(objChatList!){
                //print("hi")
                let index = self.arrMessages.index(of: objChatList!)
                let objN = self.arrMessages[index!]
                self.arrMessages.remove(at: index!)
                self.arrMessages.insert(objChatList!, at: index!)
                //objN.strReadStatus = "2"
            }else{
                self.arrMessages.append(objChatList!)
            }
            
        }
        
        //self.getOpponentInfo()
        self.arrMessages = self.arrMessages.sorted(by: { $0.TimeStamp < $1.TimeStamp})
        for obj in self.arrMessages{
            
        }
        tableChat.reloadData()
        
        /*if isPullToRefresh && self.arrMessages.count > 15
         {
         let indexPath = IndexPath(row: 10, section: 0)
         self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)*/
        if isPullToRefresh && self.arrMessages.count > 50
        {
            //if self.arrMessages.count > 50{
            let remain = (self.arrMessages.count+1)%50
            if remain == 0{
                let indexPath = IndexPath(row: 49, section: 0)
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
            }else{
                let indexPath = IndexPath(row: remain, section: 0)
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
            }
        }else{
            /*
             let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
             self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
             //self.tableViewScrollToBottom(animated: false)
             */
            if arrMessages.count > 0{
                if !(self.strPaginationKey.count>0 && arrMessages.count <= 50){
                    
                    let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
                    self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
                    //self.tableViewScrollToBottom(animated: false)
                }
            }
        }
        
        if objAppShareData.isFromNotification && self.isFirstTimeScrollToBottom{
            self.isFirstTimeScrollToBottom = false
            self.tableViewScrollToBottom(animated: false)
        }
        if isImagePicked == false {
            //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5000)) {
            //objWebserviceManager.StopIndicator()
            //}
        }
        self.refreshControl.endRefreshing()
        isPullToRefresh = false
        isLoading = false
        
        ////
        if objAppShareData.isFromDidFinish{
            let obj = self.arrMessages[self.arrMessages.count-1]
            if obj.strMessage == "https://local"{
                let ref = Database.database().reference()
                ref.child("chat").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(obj.str_Key).removeValue()
                ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).child(obj.str_Key).removeValue()
            }
        }
        objAppShareData.isFromDidFinish = false
        ////
    }
    
    //send message
    func sendMessageNew(){
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        //self.txtViewChat.isScrollEnabled = false
        isChatAppearFirst = true
        isSendButtonClicked = false
        
        if self.blockedBy == BlockStatus.kBlockedByMe || self.blockedBy == BlockStatus.kBlockedByBoth {
            self.txtViewChat.text = ""
            self.view.endEditing(true)
            
            isFromBlock = true
            objChatShareData.showAlertChatValidation(titles: "Alert", messages1: "You have blocked "+objChatHistoryModel.strOpponentName+", you can't send any message.", controller: self)
            return
            
        }else if self.blockedBy == BlockStatus.kBlockedByOpponent  {
            self.txtViewChat.text = ""
            self.view.endEditing(true)
            //self.
            
            isFromBlock = true
            objChatShareData.showAlertChatValidation(titles: "Alert", messages1: "You are blocked by "+objChatHistoryModel.strOpponentName+", you can't send message.", controller: self)
            
            return
        }
        
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        //self.txtViewChat.isScrollEnabled = false
        
        if self.txtViewChat.text == "" {
            self.txtViewChat.text = "."
            self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
            //self.txtViewChat.frame.size.height = self.txtViewMsgMinHeight
            self.txtViewHeight.constant = self.txtViewMsgMinHeight
            self.txtViewChat.text = ""
            return
        }
        
        if self.txtViewChat.text == "" {
            return
        }else{
            message = self.txtViewChat.text
            self.writeDataOnFirebaseForChatNew()
        }
        self.viewZoomImg.isHidden = true
        self.imgZoomImage.image = nil
        self.txtViewChat.text = ""
    }
    
}

//MARK: - Opponent Chat update
extension ChatVC{
    
    func observeMessagesOpponentChat() {
        self.ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).observe(.childAdded, with: { [weak self]  (snapshot)  -> Void in
            if !objChatShareData.isOnChatScreen{
                return
            }
            guard let strongSelf = self else { return }
            let dictKey = snapshot.key
            if let dict = snapshot.value as? [String:Any]{
                //print(dict)
                strongSelf.ref.child("chat").child(strongSelf.objChatHistoryModel.strOpponentId).child(strongSelf.strMyChatId).child(dictKey).updateChildValues(["readStatus":2])
                
            } else {
                strongSelf.refreshControl.endRefreshing()
                if !(self?.isImagePicked)! {
                    //objWebserviceManager.StopIndicator()
                }
            }
        })
    }
    
    func parseOpponentChatData(dict:[String:Any]){
        var tempDict = dict
        ////print("\n>>dict =  \(dict)")
        
        for (key, element) in tempDict {
            if var dictMsg  = element as? [String : Any]{
                ////print("\n>>dictMsg =  \(dictMsg)")
                dictMsg.updateValue(2, forKey: "readStatus")
                ////print("\n>>dictMsg update=  \(dictMsg)")
                tempDict.updateValue(dictMsg, forKey: key)
                ////print("\n>>tempDict update=  \(tempDict)")
                self.ref.child("chat").child(objChatHistoryModel.strOpponentId).child(strMyChatId).child(key).updateChildValues(["readStatus":2])
            }
        }
        
        
    }
}

//MARK: - Notification method
extension ChatVC{
    
    func ChatNotification(badgeCount:Int){
        //self.strMyChatId = "1  "//UserDefaults.standard.string(forKey: UserDefaults.keys.isLoggedIn) ?? ""
        
        //Android
        if self.objChatHistoryModel.strOpponentFireBaseToken == ""{
            
            let webNotification = [ "title" : self.strName,
                                    "body" : self.strTextNotification,
                                    "url":"/chat?uId="+self.strMyChatId ]
            self.ref.child("webnotification").child(self.objChatHistoryModel.strOpponentId).childByAutoId().setValue(webNotification)
            
        } else {
            
            let messageDict = ["body": checkForNULL(obj: self.strTextNotification),
                               "title": checkForNULL(obj: self.strName ),
                               "icon": "icon",
                               "sound": "default",
                               "badge": String(badgeCount),
                               "message": self.strTextNotification,
                               "notifincationType": "15",
                               "type": "chat",
                               "click_action": "ChatActivity",
                               "opponentChatId": checkForNULL(obj: self.strMyChatId)]
            
            //IOS
            let notificationDict = ["body": checkForNULL(obj: self.strTextNotification),
                                    "title": checkForNULL(obj: strName ),
                                    "icon": "icon",
                                    "sound": "default",
                                    "badge":String(badgeCount),
                                    "message": self.strTextNotification,
                                    "notifincationType": "15",
                                    "type": "chat",
                                    "click_action": "ChatActivity",
                                    "opponentChatId": checkForNULL(obj: self.strMyChatId)]
            
            let finalDict = ["to":checkForNULL(obj:self.objChatHistoryModel.strOpponentFireBaseToken),
                             "data": checkForNULL(obj:messageDict),
                             "priority" : "high",
                             "notification": checkForNULL(obj:notificationDict),
                             "sound": "default"] as [String : Any]
            
            self.sendNotificationWithDict(dictNotification:finalDict)
        }
    }
    
    func sendNotificationWithDict(dictNotification:Dictionary<String, Any>){
        
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StartIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        let strUrl = "https://fcm.googleapis.com/fcm/send"
        var request = URLRequest.init(url: URL.init(string: strUrl)!)
        
        request.setValue( "key=" + objChatShareData.kServerKey, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dictNotification, options: .prettyPrinted)
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                //print("error calling POST on /todos/1")
                //print(error!)
                return
            }
            guard data != nil else {
                //print("Error: did not receive data")
                return
            }
            
            }.resume()
    }
}

// MARK: - Textview delegate methods
extension ChatVC:UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.createTypingStatus()
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.perform(#selector(self.reload), with: nil, afterDelay: 2)
        
        self.viewMenu.isHidden = true
        if self.txtViewChat.text == "\n"{
            self.txtViewChat.resignFirstResponder()
        }else{
            //self.lblPlaceholder.isHidden = true
        }
        return true
    }
    
    func createTypingStatus(){
        
        let dict = ["isTyping":1,
                    "reciverId":self.objChatHistoryModel.strOpponentId,
                    "senderId":self.strMyChatId] as [String : Any]
        self.ref.child("isTyping").child(self.strMyChatId+"_"+self.objChatHistoryModel.strOpponentId).setValue(dict)
    }
    func textViewDidChange(_ textView: UITextView)
    {
        
        self.viewMenu.isHidden = true
        //self.lblPlaceholder.isHidden = true
        
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize.init(width: fixedWidth, height:CGFloat(MAXFLOAT)))
        
        if newSize.height <= self.txtViewMsgMaxHeight && newSize.height >= self.txtViewMsgMinHeight {
            self.txtViewHeight.constant = newSize.height;
        }
        
        //        if self.txtViewChat.contentSize.height >= self.txtViewMsgMaxHeight
        //        {
        //            //self.lblPlaceholder.isHidden = true
        //            //self.txtViewChat.isScrollEnabled = true
        //        }
        //        else
        //        {
        //            //self.lblPlaceholder.isHidden = true
        //            self.txtViewChat.frame.size.height = self.txtViewChat.contentSize.height
        //            //self.txtViewChat.isScrollEnabled = false
        //        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let indexPath = IndexPath(row: self.arrMessages.count - 1, section: 0)
        
        if self.arrMessages.count > 0{
            
            if isTableScrollToTop == false {
                
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: true)
            } else {
                //self.tableChat.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        removeTypingStatus()
    }
    
    func removeTypingStatus(){
        self.ref.child("isTyping").child(self.strMyChatId+"_"+self.objChatHistoryModel.strOpponentId).setValue(nil)
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.createTypingStatus()
        
        let text = self.txtViewChat.text! as NSString
        var substring: String = txtViewChat.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: text as String)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.searchAutocompleteEntries(withSubstring: substring)
        
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 3)
        }
    }
    
    @objc func reload() {
        self.removeTypingStatus()
        
    }
}

//MARK:- Tableview delegate methods
extension ChatVC:UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMessages.count
    }
    
    func setTextOnPastTimeLabel(indexPath:IndexPath){
        let objChat = arrMessages[indexPath.row]
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let arr = objChat.str_timestamp.components(separatedBy: " ")
        let date = formatter.date(from: arr[0])
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDayDate = formatter.string(from: yesterday!)
        
        if resultDate == strDate {
            self.lblMsgDay.text = "Today"
        }else if yesterDayDate == strDate{
            self.lblMsgDay.text = "Yesterday"
        }else{
            self.lblMsgDay.text = strDate
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let objChat = arrMessages[indexPath.row]
        let MyChatId = self.strMyChatId
        self.setTextOnPastTimeLabel(indexPath: indexPath)
        
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let arr = objChat.str_timestamp.components(separatedBy: " ")
        let date = formatter.date(from: arr[0])
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDayDate = formatter.string(from: yesterday!)
        
        let arrTime = objChat.str_timestamp.components(separatedBy: " ")
        let msgtimeOnly = arrTime[1]
        let msgAM_PM = arrTime[2]
        let msgtime = msgtimeOnly+" "+msgAM_PM
        
        let strSenderChatId = objChat.str_UId
        
        if strSenderChatId == MyChatId {
            
            if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") || objChat.strMessage.hasPrefix("content://media/external") || objChat.strMessage.hasPrefix("content://com.google.android"){
                
                let cellIdentifier = "MyImageCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyImageCell
                cell.viewBlur.isHidden = true
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate)
                
                if self.arrMessages.last?.strReadStatus == "2"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
                }
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if indexPath.row == 0 {
                    cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                cell.btnZoomImage.tag = indexPath.row
                cell.btnZoomImage.addTarget(self, action:#selector(btnZoomImage(sender:)) , for: .touchUpInside)
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    objWebserviceManager.StopIndicator()
                }
                self.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                cell.lblMsgDay.isHidden = true
                
                let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
                lpgr.minimumPressDuration = 0.5
                lpgr.delaysTouchesBegan = true
                lpgr.delegate = self
                cell.addGestureRecognizer(lpgr)
                
                /*
                //// Test
                if (self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
                    // 2
                    // Use cache
                    print("Cached image used, no need to download it")
                    cell.imgMySide.image = self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
                }else{
                    // 3
                    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
                    activityIndicator.hidesWhenStopped = true
                    DispatchQueue.main.async(execute: {() -> Void in
                        activityIndicator.center = CGPoint(x: (cell.imgMySide.frame.size.width) / 2, y: (cell.imgMySide.frame.size.height) / 2)
                        cell.imgMySide.addSubview(activityIndicator)
                    })
                    activityIndicator.startAnimating()
                    
                    let url = URL(string: objChat.strMessage)
                    task = session.downloadTask(with: url!, completionHandler: { (location, response, error) -> Void in
                        if let data = try? Data(contentsOf: url!){
                            // 4
                            DispatchQueue.main.async(execute: { () -> Void in
                                // 5
                                // Before we assign the image, check whether the current cell is visible
                                if let cell = tableView.cellForRow(at: indexPath) {
                                    
                                    let updateCell = tableView.cellForRow(at: indexPath) as!MyImageCell
//                                    let img:UIImage! = UIImage(data: data)
//                                                                    updateCell.imgMySide.image = img
//                                                                    self.cache.setObject(img, forKey: (indexPath as NSIndexPath).row as AnyObject)
                                    
            updateCell.imgMySide.af_setImage(withURL: url!, placeholderImage: #imageLiteral(resourceName: "gallery_placeholder"), progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { response in
                    
                        if let image = response.result.value {
                           DispatchQueue.main.async {
                           activityIndicator.stopAnimating()
                            updateCell.imgMySide.image = image
                            self.cache.setObject(image, forKey: (indexPath as NSIndexPath).row as AnyObject)
                            print("sdvdsvhsdsddsdsb")
                            }
                        }else{
                           updateCell.imgMySide.image = #imageLiteral(resourceName: "gallery_placeholder")
                        }
                    }
                    }
                    })
                    }
                    })
                    task.resume()
                }
                //// Test
                */
                
                return cell
                
            }else{
                let cellIdentifier = "MyTextCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyTextCell
                cell.viewBlur.isHidden = true
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate)
                
                if self.arrMessages.last?.strReadStatus == "2"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
                }
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if indexPath.row == 0 {
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    ////////////
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    ////////////
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    objWebserviceManager.StopIndicator()
                }
                self.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                cell.lblMsgDay.isHidden = true
                
                let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
                lpgr.minimumPressDuration = 0.5
                lpgr.delaysTouchesBegan = true
                lpgr.delegate = self
                cell.addGestureRecognizer(lpgr)
                
                return cell
            }
            
        }else{
            
            if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") || objChat.strMessage.hasPrefix("content://media/external") || objChat.strMessage.hasPrefix("content://com.google.android"){
                
                let cellIdentifier = "OpponentImageCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OpponentImageCell
                
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate,opponentData:self.objChatHistoryModel)
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                cell.btnZoomImage.tag = indexPath.row
                cell.btnZoomImage.addTarget(self, action:#selector(btnZoomImage(sender:)) , for: .touchUpInside)
                
                if indexPath.row == 0 {
                    //cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    objWebserviceManager.StopIndicator()
                }
                self.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                cell.lblMsgDay.isHidden = true
                return cell
            } else{
                
                let cellIdentifier = "OpponentTextCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OpponentTextCell
                cell.lblOppMessage.text = objChat.strMessage
                cell.lblOppName.text = objChatHistoryModel.strOpponentName
                
                
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if resultDate == strDate {
                    cell.lblMsgDay.text = "Today"
                    cell.lblOppMsgTime.text = "Today " + msgtime
                }else if yesterDayDate == strDate{
                    cell.lblMsgDay.text = "Yesterday"
                    cell.lblOppMsgTime.text = "Yesterday " + msgtime
                }else{
                    cell.lblMsgDay.text = strDate
                    cell.lblOppMsgTime.text = strDate + " " + msgtime
                }
                
                
                if indexPath.row == 0 {
                    //cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    objWebserviceManager.StopIndicator()
                }
                self.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                cell.lblMsgDay.isHidden = true
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.viewMenu.isHidden = true
        if self.txtViewChat.text.count > 0{
            
            //self.txtViewChat.isScrollEnabled = false
            //self.lblPlaceholder.isHidden = true
        }else{
            
            //self.txtViewChat.isScrollEnabled = false
            //self.lblPlaceholder.isHidden = true
        }
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
        
    }
    
}

//MARK: tableView Button extension
extension ChatVC{
    
    @objc func btnZoomImage(sender: UIButton!) {
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
        self.imgZoomImage.image = UIImage.init(named: "gallery_placeholder")
        let objChat = arrMessages[sender.tag]
        if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") {
            self.viewZoomImg.isHidden = false
            if objChat.strMessage == "https://local" || objChat.strMessage.contains(".gif") {
               self.imgZoomImage.image = objChat.imageLocal
               return
            }
            let url = URL(string: (objChat.strMessage))
            //self.imgZoomImage.af_setImage(withURL: url!)
            let processor = DownsamplingImageProcessor(size: self.imgZoomImage.frame.size)
                >> RoundCornerImageProcessor(cornerRadius: 0)
            self.imgZoomImage.kf.indicatorType = .activity
            self.imgZoomImage.kf.setImage(
                with: url,
                placeholder: UIImage(named: "gallery_placeholder"),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }else{
            self.viewZoomImg.isHidden = true
        }
    }
}



//MARK: - scrollView manage t extension
extension ChatVC{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.arrMessages.count > 0{
            //self.viewMsgDay.isHidden = false
            self.viewMsgDay.isHidden = true
            //            let arrNNN = self.tableChat.indexPathsForVisibleRows
            //            let index = arrNNN?.last
            //            self.setTextOnPastTimeLabel(indexPath: index!)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //objAppShareData.setView(view: self.viewMsgDay, hidden: true)
        /*
        //// Himanshu
        if (self.tableChat.indexPathsForVisibleRows)!.count>0{
            let indexPath = self.tableChat.indexPathsForVisibleRows![0]
            if indexPath.row<=25 && !self.isLoading{
                if self.arrMessages.count < 50 {
                    self.isLastMsg = true
                    self.isLoading = true
                    return
                }
                let obj = self.arrMessages[0]
                if self.strPaginationKey != obj.str_Key{
                    self.isLoading = true
                    self.isPullToRefresh = true
                    self.observeMessages(lastMessageKey: obj.str_Key)
                }else{
                    self.isLastMsg = true
                    self.refreshControl.endRefreshing()
                }
            }
        }*/
        ////
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //self.view.endEditing(true)
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            // moved to bottom
            isTableScrollToTop = false
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            // moved to top
            isTableScrollToTop = true
        } else {
            // didn't move
        }
        if self.arrMessages.count > 0{
            let arrNNN = self.tableChat.indexPathsForVisibleRows
            if arrNNN?.count ?? 0 > 0{
                let index = arrNNN?[0]
                self.setTextOnPastTimeLabel(indexPath: index!)
            }
        }
        //// Himanshu
        /*
         self.viewOptionsMenu.isHidden = true
         if (self.tblChat.indexPathsForVisibleRows)!.count>0{
         let indexPath = self.tblChat.indexPathsForVisibleRows![0]
         let objChat = self.arrMessages[indexPath.row]
         self.lblHeaderDate.text = objChat.strRelativePastTime
         }
         */
        ////
    }
    
}

//MARK: - send message on firebase chat extension
extension ChatVC{
    
    func timeInMiliSeconds() -> String {
        let date = Date()
        let timeInMS = "\(Int(date.timeIntervalSince1970 * 1000))"
        return timeInMS
    }
    
    
    func writeDataOnFirebaseForChatNew(){
        
        txtViewChat.contentSize.height = 39
        let calendarDate = ServerValue.timestamp()//timeInMiliSeconds()
        var a = 1
        if String(objChatHistoryModel.strOpponentIsLogin) == String(a){
            a = 1
        } else {
            a = 0
        }
        let ref = Database.database().reference()

        let myIds = ref.childByAutoId()
        let dict = [
            "message": checkForNULL(obj:message),
            "messageType":checkForNULL(obj:0),
            "reciverId":checkForNULL(obj:self.objChatHistoryModel.strOpponentId ),
            "readStatus":a,
            "senderId":checkForNULL(obj:self.strMyChatId),
            "timestamp":calendarDate]
        
        self.isChatAppearFirst = true
        print(myIds)
        let keyValue = ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).childByAutoId().key
        ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(keyValue ?? "").setValue(dict)
        ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(strMyChatId).child(keyValue ?? "").setValue(dict)

        self.imgType = 0
        self.strTextNotification = message
        //DispatchQueue.main.async {
        self.UpdateHistory()
        //}
    }
    
    func UpdateHistory(){
        
        let calendarDate = ServerValue.timestamp()
        var unreadCount = 0
        var favouriteStatus = 0
        self.ref.child("chat_history").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            //print("snapshot = \(snapshot)")
            // guard let strongSelf = self else { return }
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    
                    if let count = dict["unreadMessage"] as? String {
                        unreadCount = Int(count)!
                    }else if let count1 = dict["unreadMessage"] as? Int {
                        unreadCount = count1
                    }
                    if let count = dict["favourite"] as? String {
                        favouriteStatus = Int(count)!
                    }else if let count1 = dict["favourite"] as? Int {
                        favouriteStatus =  count1
                    }
                }
            }
            unreadCount = unreadCount+1
            //For My Side
            let dict1 = [
                "memberCount":0,
                "favourite":Int(self.objChatHistoryModel.strFavouriteStatus),
                "message": checkForNULL(obj:self.message),
                "messageType":checkForNULL(obj:self.imgType),
                "profilePic":checkForNULL(obj:self.objChatHistoryModel.strOpponentProfileImage ),//opponent image
                "reciverId":checkForNULL(obj: self.objChatHistoryModel.strOpponentId),//
                "senderId":checkForNULL(obj:self.strMyChatId ),
                "timestamp":calendarDate,
                "type":checkForNULL(obj:"user"),
                "unreadMessage":checkForNULL(obj: 0),
                "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName )//opponent name
            ]
            let ref = Database.database().reference()
            ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(dict1)
            
            //For Opponent Side
            let dict_2 = [
                "memberCount":0,
                "favourite":favouriteStatus,
                "message": checkForNULL(obj:self.message),
                "messageType":checkForNULL(obj:self.imgType),
                "profilePic":checkForNULL(obj:self.profilePic ),
                "reciverId":checkForNULL(obj: self.objChatHistoryModel.strOpponentId),
                "senderId":checkForNULL(obj:self.strMyChatId ),
                "timestamp":calendarDate,
                "unreadMessage":checkForNULL(obj: unreadCount),
                "type":checkForNULL(obj:"user"),
                "userName":checkForNULL(obj:self.strName)
            ]
            ref.child("chat_history").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).setValue(dict_2)
            
            self.isImagePicked = false
            if !self.isImagePicked {
                //objWebserviceManager.StopIndicator()
            }
            
            var count = 0
            self.ref.child("chatBadgeCount").child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value) { (snap) in
                if !snap.exists(){
                    count = 1
                }else{
                    print(snap.value)
                    let dict = snap.value as! [String:Any]
                    count = dict["count"] as! Int + 1
                }
                
                let newDict = ["count" : count]
                let strBadgeCount = String(count)
                self.ref.child("chatBadgeCount").child(self.objChatHistoryModel.strOpponentId).setValue(newDict)
            }
            
            //if self.objChatHistoryModel.strMuteStatus == "1"{
            if self.strMuteStatusForOtherNotification == "1"{
            }else{
                self.badgeCount()
              //  self.ChatNotification()
            }
        })
    }
}

//MARK:- TypingStatus manage method extension
extension ChatVC {
    func IsTypingStatus(){
        
        self._refHandle = self.ref.child("isTyping").child(objChatHistoryModel.strOpponentId+"_"+self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
            self?.lblOnlineStatus.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self?.lblOnlineStatus.text = self?.objChatHistoryModel.strOpponentOnlineStatus
            
            guard let strongSelf = self else { return }
            
            if snapshot.exists(){
                
                if (snapshot.value as? [String:Any]) != nil{
                    strongSelf.lblOnlineStatus.text = "typing..."
                    strongSelf.lblOnlineStatus.textColor = #colorLiteral(red: 0.1961900294, green: 0.8260042071, blue: 0.7877198458, alpha: 1)
                    
                    //print("\n\n Typing")
                } else {
                    //print("\n\n Not Typing")
                    strongSelf.lblOnlineStatus.text = strongSelf.objChatHistoryModel.strOpponentOnlineStatus
                }
            }
            else{
                //print("\n\n Not Typing2")
                strongSelf.lblOnlineStatus.text = strongSelf.objChatHistoryModel.strOpponentOnlineStatus
            }
        })
    }
}


//MARK:- Clear UnreadChatHistory method
extension ChatVC {
    func clearUnreadChatHistory(){
        self._refHandle = self.ref.child("chat_history").child(self.strMyChatId).child(objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            if let dict = snapshot.value as? [String:Any]{
                
                var favourites = "0"
                if let favourite = dict["favourite"] as? String{
                    favourites = favourite
                }else if let favourite = dict["favourite"] as? Int{
                    favourites = String(favourite)
                }
                
                if favourites == "1"{
                    self?.objChatHistoryModel.strFavouriteStatus = "1"
                    self?.lblFavouriteStatus.text = "Unfavourite"
                }else{
                    self?.objChatHistoryModel.strFavouriteStatus = "0"
                    self?.lblFavouriteStatus.text = "Add To Favourite"
                }
            }
            if !(self?.isOnChatScreen)!{
                return
            }
            
            if snapshot.exists(){
                
                //                    if let dict = snapshot.value as? [String:Any]{
                //                        let favourite = dict["favourite"] as? String ?? ""
                //                        if favourite == "1"{
                //                            self?.objChatHistoryModel.strFavouriteStatus = "1"
                //                            self?.lblFavouriteStatus.text = "Unfavourite"
                //                        }else{
                //                            self?.objChatHistoryModel.strFavouriteStatus = "0"
                //                            self?.lblFavouriteStatus.text = "Add To Favourite"
                //                        }
                //                    }
                strongSelf.ref = Database.database().reference()
                /*
                 self?.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).observeSingleEvent(of:.value, with: { (DataSnapshot) in
                 let dict = DataSnapshot.value as? [String:Any]
                 var unreadCount = 0
                 if let unread = dict?["unreadMessage"] as? Int{
                 unreadCount = unread
                 }else if let unread = dict?["unreadMessage"] as? String{
                 unreadCount = Int(unread) ?? 0
                 }
                 if unreadCount>0{
                 let count = objChatShareData.chatBadgeCount - 1
                 let newDict = ["count" : count]
                 strongSelf.ref.child("chatBadgeCount").child((self?.strMyChatId)!).setValue(newDict)
                 }
                 })
                 */
                strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).updateChildValues(["unreadMessage": 0])
                
                if let refHandle = strongSelf._refHandle{
                    strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).removeObserver(withHandle: refHandle)
                }
            }
            else{
                self?.lblFavouriteStatus.text = "Add To Favourite"
                self?.objChatHistoryModel.strFavouriteStatus = ""
            }
        })
        //////////////
    }
}

//MARK: - Button methods extension
extension ChatVC{
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if objAppShareData.isFromNotification {
            objAppShareData.clearNotificationData()
        }
        self.viewMenu.isHidden = true
        objChatShareData.isOnChatScreen = false
        
        isOnChatScreen = false
        self.view.endEditing(true)
        
        self.ref.child("chat_history").child(self.strMyChatId).child(objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.ref = Database.database().reference()
            if snapshot.exists(){
                strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).updateChildValues(["unreadMessage": 0])
            } })
        
        if objChatShareData.fromChatToHistory{
            self.dismiss(animated: false, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.viewMenu.isHidden == false{
            self.viewMenu.isHidden = true
        }else{
            self.viewMenu.isHidden = false
        }
    }
    
    @IBAction func btnBlockAction(_ sender: UIButton) {
        self.viewFor_Block_Delete.isHidden = false
        self.viewForBlock.isHidden = false
        self.viewForDelete.isHidden = true
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
    }
    
    @IBAction func btnBlockYesAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFor_Block_Delete.isHidden = true
        self.changeBlockStatusOfOpponent()
    }
    
    @IBAction func btnBlockCancelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFor_Block_Delete.isHidden = true
    }
    @IBAction func btnMuteAction(_ sender: UIButton) {
        self.viewForDelete.isHidden = true
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
        self.changeMuteStatus()
    }
    
    @IBAction func btnDeleteChatCancelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.viewFor_Block_Delete.isHidden = true
    }
    
    @IBAction func btnDeleteChatYesAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        self.viewFor_Block_Delete.isHidden = true
        self.viewMenu.isHidden = true
        
        if self.arrMessages.count > 0{
            self.arrMessages.removeAll()
            self.tableChat.reloadData()
            self.ref.child("chat").child(strMyChatId).child(objChatHistoryModel.strOpponentId).setValue(nil)
            self.ref.child("chat_history").child(strMyChatId).child(objChatHistoryModel.strOpponentId).setValue(nil)
        }
    }
    @IBAction func btnAddToFavouriteAction(_ sender: UIButton) {
        self.manageFavouriteStatus()
    }
    
    @IBAction func btnDeleteChatAction(_ sender: UIButton) {
        self.viewFor_Block_Delete.isHidden = false
        self.viewForDelete.isHidden = false
        self.viewForBlock.isHidden = true
        self.view.endEditing(true)
        
    }
    
    @IBAction func btnGalaryImageUpload(_ sender: UIButton) {
        self.GallaryCameraAction(imageFrom: "Gallary")
    }
    
    @IBAction func btnImageUpload(_ sender: UIButton) {
        self.GallaryCameraAction(imageFrom: "Camera")
    }
    
    @IBAction func btnCancelViewZoom(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.scrollViewImg.zoomScale = 1.0
        self.viewZoomImg.isHidden = true
    }
    
    @IBAction func btnSendMessage(_ sender: UIButton) {
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: "No Internet Connection ", on: self)
            return
        }else{
            if self.txtViewChat.text.count == 0 {
                self.txtViewHeight.constant = self.txtViewMsgMinHeight
                return;
            }
                
                
                //            if self.txtViewChat.text == " " || self.txtViewChat.text == ""{
                //                if self.txtViewChat.text == "" {
                //                    self.txtViewChat.text = "."
                //                    self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
                //                    self.txtViewChat.frame.size.height = self.txtViewMsgMinHeight
                //                    self.txtViewChat.text = ""
                //                    return
                //                }
                //            }
                
            else{
                isSendButtonClicked = true
                //self.txtViewChat.frame.size.height = self.txtViewMsgMinHeight
                self.txtViewHeight.constant = self.txtViewMsgMinHeight
                //self.view.endEditing(true)
                self.sendMessageNew()
                //self.lblPlaceholder.isHidden = true
            }
        }
    }
}


//MARK: manage camera galary action extension
extension ChatVC{
    
    func GallaryCameraAction(imageFrom:String){
        self.viewMenu.isHidden = true
        if self.txtViewChat.text.count > 0{
            //self.txtViewChat.isScrollEnabled = false
            //self.lblPlaceholder.isHidden = true
        }else{
            //self.txtViewChat.isScrollEnabled = false
            //self.lblPlaceholder.isHidden = true
        }
        self.view.endEditing(true)
        
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: "No Internet Connection ", on: self)
            return
        }
        
        
        if self.blockedBy == BlockStatus.kBlockedByMe || self.blockedBy == BlockStatus.kBlockedByBoth {
            self.txtViewChat.text = ""
            self.view.endEditing(true)
            
            isFromBlock = true
            objChatShareData.showAlertChatValidation(titles: "Alert", messages1: "You have blocked "+objChatHistoryModel.strOpponentName+", you can't send any message.", controller: self)
            return
            
        }else if self.blockedBy == BlockStatus.kBlockedByOpponent  {
            self.txtViewChat.text = ""
            self.view.endEditing(true)
            //self.bottem.constant = 10
            
            isFromBlock = true
            objChatShareData.showAlertChatValidation(titles: "Alert", messages1: "You are blocked by "+objChatHistoryModel.strOpponentName+", you can't send message.", controller: self)
            
            return
        }
        
        self.view.endEditing(true)
        
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {   UIAlertAction in self.openCamera() }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {   UIAlertAction in self.openGallary() }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {   UIAlertAction in }
        
        if imageFrom == "Gallary"{
            //            alert.addAction(gallaryAction)
            //            alert.addAction(cancelAction)
            self.openGallary()
        }else if imageFrom == "Camera"{
            //            alert.addAction(cameraAction)
            //            alert.addAction(cancelAction)
            self.openCamera()
        }
        //self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: manage favouriteStatus extension
extension ChatVC{
    
    func manageFavouriteStatus(){
        let myId = self.strMyChatId
        let oppId = self.objChatHistoryModel.strOpponentId
        //objWebserviceManager.StartIndicator()
        self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            
            if !(self?.isImagePicked)! {
                objWebserviceManager.StopIndicator()
            }
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    var favourites = "0"
                    if let favourite = dict["favourite"] as? String{
                        favourites = favourite
                    }else if let favourite = dict["favourite"] as? Int{
                        favourites = String(favourite)
                    }
                    
                    if favourites == "1"{
                        self?.objChatHistoryModel.strFavouriteStatus = "1"
                    }else{
                        self?.objChatHistoryModel.strFavouriteStatus = "0"
                    }
                }
                
                if self?.objChatHistoryModel.strFavouriteStatus == "1"{
                    self?.lblFavouriteStatus.text = "Add To Favourite"
                    objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
                    self?.ref.child("chat_history").child(myId).child(oppId).updateChildValues(["favourite": 0])
                }else if self?.objChatHistoryModel.strFavouriteStatus == "0"{
                    self?.lblFavouriteStatus.text = "Unfavourite"
                    objAppShareData.arrTBottomSheetModal = ["Unfavourite","Clear Chat","Mute Chat","Block User"]
                    self?.ref.child("chat_history").child(myId).child(oppId).updateChildValues(["favourite": 1])
                }
                
            }else{
                let calendarDate = ServerValue.timestamp()
                self?.lblFavouriteStatus.text = "Unfavourite"
                objAppShareData.arrTBottomSheetModal = ["Unfavourite","Clear Chat","Mute Chat","Block User"]
                let dict1 = [
                    "memberCount":0,
                    "message": checkForNULL(obj:""),
                    "messageType":checkForNULL(obj:0),
                    "profilePic":checkForNULL(obj:self?.objChatHistoryModel.strOpponentProfileImage),//opponent image
                    "reciverId":checkForNULL(obj: self?.objChatHistoryModel.strOpponentId),//
                    "senderId":checkForNULL(obj:self?.strMyChatId ),
                    "timestamp":calendarDate,
                    "type":checkForNULL(obj:"user"),
                    "unreadMessage":checkForNULL(obj: 0),
                    "favourite":checkForNULL(obj: 1),
                    "userName":checkForNULL(obj:self?.objChatHistoryModel.strOpponentName )//opponent name
                ]
                self?.ref.child("chat_history").child(myId).child(oppId).setValue(dict1)
            }
        })
        self.viewMenu.isHidden = true
    }
}



//MARK: - get badge count
extension ChatVC{
    
    func badgeCount(){
        var TotalBadgeCount = 0
        var chatCounts = "0"
        self.ref.child("socialBookingBadgeCount").child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            
            if !(self?.isImagePicked)! {
                objWebserviceManager.StopIndicator()
            }
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                 
                    if let chatCount1 = dict["chatCount"] as? String{
                        chatCounts = chatCount1
                    }else if let chatCount1 = dict["chatCount"] as? Int{
                        chatCounts = String(chatCount1)
                    }
                    
                    chatCounts = String((Int(chatCounts) ?? 0)+1)
                    var totalCounsts = "0"
                    if let totalCount1 = dict["totalCount"] as? String{
                        totalCounsts = totalCount1
                    }else if let totalCount1 = dict["totalCount"] as? Int{
                        totalCounsts = String(totalCount1)
                    }
                    
                    TotalBadgeCount = (Int(totalCounsts) ?? 0)+1
                    self?.ref.child("socialBookingBadgeCount").child(self?.objChatHistoryModel.strOpponentId ?? "0").updateChildValues(["chatCount":chatCounts,"totalCount":TotalBadgeCount])

                    self?.ChatNotification(badgeCount: TotalBadgeCount)
                }
            }else{
                self?.ref.child("socialBookingBadgeCount").child(self?.objChatHistoryModel.strOpponentId ?? "0").updateChildValues(["chatCount":chatCounts,"totalCount":TotalBadgeCount])
                self?.ChatNotification(badgeCount: TotalBadgeCount)
            }
        })
        
    }
}




// MARK: - keyboard methods
extension ChatVC{
    
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo
        let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
        let keyboardFrame: CGRect? = kbFrame?.cgRectValue
        let height: CGFloat? = keyboardFrame?.size.height
        //bottem.constant = height!+5
        UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
        if isFromBlock{
            isFromBlock = false
            
        }
        UIView.animate(withDuration: animationDuration , animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
}

//MARK: - imagePicker extension method
extension ChatVC{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        isImagePicked = true
        if isImageLocallyUploading{
           return
        }else{
           isImageLocallyUploading = true
        }
        
        var img : UIImage?
        var url : URL?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            img = image
            self.strTextNotification = "Image"
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            img = image
            self.strTextNotification = "Image"
            //// Himanshu
            url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            ////
        }
        if let Image = img {
            self.strTextNotification = "Image"
            imgZoomImage.image = Image
            self.setImageLocallyBeforeSend()
            self.sendImageToFirebaseWithImage(image: Image, url: url)
        }
        self.dismiss(animated: true, completion: nil)
        //objWebserviceManager.StartIndicator()
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.viewZoomImg.isHidden = true
        dismiss(animated: true, completion: nil)
        //print("picker cancel.")
    }
    
    func openCamera() //to Access Camera
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.modalPresentationStyle = .fullScreen
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {}
    }
    
    func openGallary() //to Access Gallery
    {   //imagePicker.allowsEditing = true
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
        })
    }
}


//MARK: - send image on firebase
extension ChatVC{
    
    func sendImageToFirebaseWithImage(image : UIImage, url : URL?){
        
        //objWebserviceManager.StartIndicator()
        //// Himanshu
        //        let localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        //        let metadata = StorageMetadata()
        //        metadata.contentType = "image/jpeg"
        ////
        var localFilePath = ""
        let metadata = StorageMetadata()
        let str = url?.absoluteString ?? ""
        if str.contains(".gif"){
            imageData = try! Data.init(contentsOf: url!)
            metadata.contentType = "image/gif"
            localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).gif"
        }else{
            imageData = objChatShareData.compressImage(image: image) as Data?
            metadata.contentType = "image/jpeg"
            localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpeg"
            
            var compression = 1.0
            while compression >= 0.0 {
                imageData = image.jpegData(compressionQuality: CGFloat(compression))!
                let imageLength: Int = imageData!.count
                if imageLength < 60000 {
                    break
                }
                compression -= 0.1
            }
        }
        self.storageRef.child(localFilePath).putData(imageData!, metadata: metadata) { [weak self] (metadata, error) in
            if let error = error {
                //print("Error uploading: \(error)")
                return
            }
            guard let strongSelf = self else { return }
            self?.storageRef.child(localFilePath).downloadURL { (url, error) in
              guard let downloadURL = url else {
                return
              }
                strongSelf.imgUrl = downloadURL.absoluteString
                strongSelf.sendImageInStringForm()
            }
            print(">> imgUrl =  \(strongSelf.imgUrl)")
        }
    }
    
    func setImageLocallyBeforeSend(){
        let calendarDate = ServerValue.timestamp()
        let currentTimeStamp = Date().toMillis()

        let dict = [
            "message": "https://local",
            "messageType":checkForNULL(obj:1),
            "reciverId":checkForNULL(obj:self.objChatHistoryModel.strOpponentId),
            "readStatus":"0",
            "senderId":checkForNULL(obj:self.strMyChatId ),
            "timestamp":currentTimeStamp ]
        let ref = Database.database().reference()
        let keyValue = ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).childByAutoId().key
        self.strKeyValueForHold = keyValue ?? ""
        self.arrKeyValueForHold.append(keyValue ?? "")
        let objChat = ChatData.init(dict: dict, key: self.strKeyValueForHold)
        objChat!.imageLocal = imgZoomImage.image!
        let date = Date(timeIntervalSince1970: Double(currentTimeStamp!) / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        objChat!.str_timestamp = dateFormatter.string(from: date)
        self.arrMessages.append(objChat!)
        self.tableChat.reloadData()
        self.tableViewScrollToBottom(animated: false)
        ref.child("chat").child(strMyChatId).child(objChatHistoryModel.strOpponentId).child(keyValue ?? "").setValue(dict)
        ref.child("chat").child(objChatHistoryModel.strOpponentId).child(strMyChatId).child(keyValue ?? "").setValue(dict)
        
    }
    
    func sendImageInStringForm() {
        
        let calendarDate = ServerValue.timestamp()
        var a = 1
        if String(objChatHistoryModel.strOpponentIsLogin) == String(a){  a = 1
        } else { a = 0 }
        
        let dict = [
            "message": checkForNULL(obj:imgUrl),
            "messageType":checkForNULL(obj:1),
            "reciverId":checkForNULL(obj:objChatHistoryModel.strOpponentId),
            "readStatus":a,
            "senderId":checkForNULL(obj:self.strMyChatId),
            "timestamp":calendarDate,
            //"imageLocal":imgZoomImage.image!
        ]
        
        let ref = Database.database().reference()
        let keyValue = ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).childByAutoId().key
        self.strKeyValueForHold = self.arrKeyValueForHold[0]
        self.arrKeyValueForHold.remove(at: 0)
        ref.child("chat").child(strMyChatId).child(objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(self.strKeyValueForHold){
                print("true rooms exist")
                ref.child("chat").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(self.strKeyValueForHold).setValue(dict)
                ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).child(self.strKeyValueForHold).setValue(dict)
                self.message = "Image"
                self.imgType = 1
                self.UpdateHistory()
                self.isImageLocallyUploading = false
            }else{
                print("false room doesn't exist")
            }
        })
    }
}

//MARK: - manage message mute status extension

//MARK: - manage message mute status extension
extension ChatVC{
    func muteStatus(){
        self.ref.child("mute_user").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            //self.ref.child("mute_user").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).observe(.value, with: { (snapshot) -> Void in
            
            //print("snapshot = \(snapshot)")
            // guard let strongSelf = self else { return }
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    if let muteId = dict["mute"] as? Int{
                        self.objChatHistoryModel.strMuteStatus = String(muteId)
                    }else if let muteIds = dict["mute"] as? String{
                        self.objChatHistoryModel.strMuteStatus = muteIds
                    }
                }
            } else{
                self.objChatHistoryModel.strMuteStatus = "0"
            }
            self.lblMuteStatus.text = "Unmute Chat"
            
            if self.objChatHistoryModel.strMuteStatus == "0"{
                self.lblMuteStatus.text = "Mute Chat"
            }
        })
        self.ref.child("mute_user").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).observe(.value, with: { (snapshot) -> Void in
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    if let muteId = dict["mute"] as? Int{
                        self.strMuteStatusForOtherNotification = String(muteId)
                    }else if let muteIds = dict["mute"] as? String{
                        self.strMuteStatusForOtherNotification = muteIds
                    }
                }
            } else{
                self.strMuteStatusForOtherNotification = "0"
            }
        })
        
    }
    
    func changeMuteStatus(){
        var muteId:Int = 0
        if objChatHistoryModel.strMuteStatus == "0"{
            muteId = 1
            self.lblMuteStatus.text = "Unmute Chat"
            self.ref.child("mute_user").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(["mute":muteId])
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Mute Chat","Block User"]
            
        }else{
            muteId = 0
            self.lblMuteStatus.text = "Mute Chat"
            self.ref.child("mute_user").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue([nil])
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat","Unmute Chat","Block User"]
            
        }
        
        
        if objChatHistoryModel.strMuteStatus == "0"{
            objChatHistoryModel.strMuteStatus = "1"
        }else{
            objChatHistoryModel.strMuteStatus = "0"
        }
    }
    
    
}

//MARK: - scroll table to bottom
extension ChatVC{
    
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(0)) {
            
            let numberOfSections = self.tableChat.numberOfSections
            let numberOfRows = self.tableChat.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tableChat.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                self.tableChat.isHidden = false
            }
        }
    }
}

//MARK: - scroll table to bottom
extension ChatVC{
    
    // MARK:- Kyeboard hide/show methods
    
    @objc func keyboardWasShown(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5, animations: {
                self.txtViewContainerBottom.constant = keyboardSize.size.height + 3
                //print("\n\n\n self.txtViewContainerBottom.constant = \( self.txtViewContainerBottom.constant)")
                if self.arrMessages.count>0{
                    let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
                    self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
                }
                //self.tableViewScrollToBottom(animated: false)
                self.view.layoutIfNeeded()
            }, completion: nil)
            //        if self.view.frame.origin.y == 0{
            //            self.view.frame.origin.y -= keyboardSize.height
            //        }
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5, animations: {
                self.txtViewContainerBottom.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            //        if self.view.frame.origin.y != 0{
            //            self.view.frame.origin.y += keyboardSize.height
            //        }
        }
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //func textFieldDidBeginEditing(_ textField: UITextField) {
    //    if textField == mEnterPasswordTextField || textField == mEnterConfirmPassword  {
    //        animateViewMoving(up: true, moveValue: 120)
    //    }
    //}
    //
    //func textFieldDidEndEditing(_ textField: UITextField) {
    //    if textField == mEnterPasswordTextField || textField == mEnterConfirmPassword  {
    //        animateViewMoving(up: false, moveValue: 120)
    //    }
    //}
    //
    //func animateViewMoving (up:Bool, moveValue :CGFloat){
    //    let movementDuration:TimeInterval = 0.3
    //    let movement:CGFloat = ( up ? -moveValue : moveValue)
    //    UIView.beginAnimations( "animateView", context: nil)
    //    UIView.setAnimationBeginsFromCurrentState(true)
    //    UIView.setAnimationDuration(movementDuration )
    //    self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
    //    UIView.commitAnimations()
    //}
}

//MARK: - Edit & Delete Section
extension ChatVC{
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            let touchPoint = gestureReconizer.location(in: self.tableChat)
            self.view.endEditing(true)
            if let indexPaths = tableChat.indexPathForRow(at: touchPoint) {
                let obj = self.arrMessages[indexPaths.row]
                if obj.strMessage == "https://local"{
                   //return
                }
                print("obj = ",obj.strMessage)
                objHoldIndex = obj
                
                indexOfArray = indexPaths.row// (self.arrMessages.count - 1)-indexPaths.row
                if self.strMyChatId == String(obj.str_UId){
                    if let cell = tableChat.cellForRow(at: indexPaths) as? MyTextCell{
                        cell.viewBlur.isHidden = false
                    }
                    if let cell = tableChat.cellForRow(at: indexPaths) as? MyImageCell{
                        cell.viewBlur.isHidden = false
                    }
                    self.viewEditCommentAction.isHidden = true
                    self.viewCommentAction.isHidden = false
                }
            }
        }
    }
    
    @IBAction func btnEditCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        //let objReminder = self.objHoldIndex
        //let param = ["id":String(objReminder?._id ?? 0),"feedId":objReminder?.feedId]
        //objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
        //self.tableChat.reloadData()
    }
    
    @IBAction func btnDeleteCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        self.deleteCommentAlert(Index: sender.tag)
    }
    
    
    func deleteCommentAlert(Index:Int){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this message?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
             self.deleteSingleMessage(index: Index)
        }
        okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            self.tableChat.reloadData()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteSingleMessage(index:Int){
      let obj = self.arrMessages[self.indexOfArray].str_Key
        print(obj)
        let ref = Database.database().reference()
        ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(obj).removeValue()
        ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(strMyChatId).child(obj).removeValue()

       // ref.child("chat").child(self.objChatHistoryModel.strOpponentId).child(strMyChatId).child(obj).setValue([:])
        if self.arrMessages.count == 1{
            ref.child("chat_history").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).removeValue()
            ref.child("chat_history").child(self.objChatHistoryModel.strOpponentId).child(strMyChatId).removeValue()
        }else if indexOfArray+1 == self.arrMessages.count{
            let objChatHistoryModel = self.arrMessages[indexOfArray-1]
           
            var strRecvId = ""
            if self.strMyChatId == objChatHistoryModel.str_lastSenderID{
               strRecvId = self.objChatHistoryModel.strOpponentId
            }else{
                strRecvId = self.strMyChatId
            }
            let dict1 = [
                "memberCount":0,
                "favourite":0,
                "message": checkForNULL(obj:objChatHistoryModel.strMessage),
                "messageType":checkForNULL(obj:objChatHistoryModel.str_isImage),
                "profilePic":checkForNULL(obj:objChatHistoryModel.str_ProfileImage ),//opponent image
                "reciverId":checkForNULL(obj: strRecvId),//
                "senderId":checkForNULL(obj:objChatHistoryModel.str_lastSenderID ),
                "timestamp":objChatHistoryModel.TimeStamp,
                "type":checkForNULL(obj:"user"),
                "unreadMessage":checkForNULL(obj: 0),
                "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName )//opponent name
            ]
            let ref = Database.database().reference()
            ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(dict1)
            ref.child("chat_history").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).setValue(dict1)
        } else{
        }
    }
    
    @IBAction func btnCommentActionPopUpHiddden(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        self.tableChat.reloadData()
    }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Select Option"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        if self.lblFavouriteStatus.text == "Add To Favourite"{
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat"]
        }else{
            objAppShareData.arrTBottomSheetModal = ["Unfavourite","Clear Chat"]
        }
        
        if self.lblMuteStatus.text == "Mute Chat"{
            objAppShareData.arrTBottomSheetModal.append("Mute Chat")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Unmute Chat")
        }
        if self.lblBlock_Unblock.text == "Block User"{
            objAppShareData.arrTBottomSheetModal.append("Block User")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Unblock User")
        }
        scrollingNC.callback = { [weak self] (str) in
                guard let strongSelf = self else {
                    return
                }
                let btn = UIButton()
                if str == "0"{
                    strongSelf.btnAddToFavouriteAction(btn)
                }else if str == "1"{
                    strongSelf.btnDeleteChatAction(btn)
                }else if str == "2"{
                    strongSelf.btnMuteAction(btn)
                }else if str == "3"{
                    strongSelf.btnBlockAction(btn)
                }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Select Option"
        if self.lblFavouriteStatus.text == "Add To Favourite"{
            objAppShareData.arrTBottomSheetModal = ["Add To Favourite","Clear Chat"]
        }else{
            objAppShareData.arrTBottomSheetModal = ["Unfavourite","Clear Chat"]
        }
        
        if self.lblMuteStatus.text == "Mute Chat"{
            objAppShareData.arrTBottomSheetModal.append("Mute Chat")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Unmute Chat")
        }
        if self.lblBlock_Unblock.text == "Block User"{
            objAppShareData.arrTBottomSheetModal.append("Block User")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Unblock User")
        }
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            let btn = UIButton()
            print(str)
            if str == "0"{
                strongSelf.btnAddToFavouriteAction(btn)
            }else if str == "1"{
                strongSelf.btnDeleteChatAction(btn)
            }else if str == "2"{
                strongSelf.btnMuteAction(btn)
            }else if str == "3"{
                strongSelf.btnBlockAction(btn)
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
}


extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

