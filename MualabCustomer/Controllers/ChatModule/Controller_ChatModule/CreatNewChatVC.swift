//
//  CreatBroadCastVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/17/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//


import UIKit
import Firebase
import Alamofire
import AlamofireImage

class CreatNewChatVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    
    @IBOutlet weak var tblCreateGroupList:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var txtUserName: UITextField!
    
    var strText = ""
    
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var storageRef: StorageReference!
    
    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [ChatHistoryData]()
    fileprivate var arrChatHistoryMyData = [ChatHistoryData]()
    fileprivate var arrChatHistoryTotal = [ChatHistoryData]()

    
    fileprivate var newBroadCastID = ""
    fileprivate var imgUrl = ""
    
    fileprivate var fromImage = false
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
    
}
//MARK: - System Method extension
extension CreatNewChatVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtUserName.delegate = self
        
        ref =  Database.database().reference()
        storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
        
        self.strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.viewConfigure()
        self.GetUserListFromFirebase()
        self.viewConfigure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if objChatShareData.fromChatToHistory{
            objChatShareData.fromChatToHistory = false
        self.navigationController?.popViewController(animated: false)
        }
        if fromImage == false{
            self.GetUserListFromFirebase()
        }
        fromImage = false
        self.getGroupName()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.clearUnreadChatHistory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getGroupName(){
        self._refHandle = self.ref.child("broadcast") .observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
            var groupName = 0
            
            if snapshot.exists(){
                let keys  = snapshot.key
                if let dict = snapshot.value as? NSDictionary{
                    
                    if let dictNew = dict as? [String:Any] {
                        for obj in dictNew{
                            let a = obj.key
                            print(a)
                            let arrName = a.split(separator: "_")
                            if arrName.count >= 2 {
                                var newName = 0
                                newName = Int(arrName[1])!
                                if newName >= groupName {
                                    groupName = newName
                                    print(groupName)
                                }
                            }
                        }
                    }
                } else {
                    
                }
            }
            else{
                
            }
            let newGroup = groupName+1
            self?.newBroadCastID = "broadcast_"+String(newGroup)
        })
    }
}


//MARK: - Custome method extension
fileprivate extension CreatNewChatVC{
    func viewConfigure(){
        self.tblCreateGroupList.delegate = self
        self.tblCreateGroupList.dataSource = self
        self.txtUserName.delegate = self
        ref = Database.database().reference()
    }
    
    
    
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        arrChatHistory.removeAll()
        let text = textField.text! as NSString
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        
        let filteredArray = self.arrChatHistoryTotal.filter(){ $0.strOpponentName.localizedCaseInsensitiveContains(substring) }
        NSLog("HERE %@", filteredArray)
        strText = substring
        arrChatHistory = filteredArray
        if arrChatHistory.count == 0 && strText.count != 0{
            self.lblNoDataFound.isHidden = false
            self.tblCreateGroupList.isHidden = true
        }else{
            if strText.count == 0{
                arrChatHistory = self.arrChatHistoryTotal
                self.lblNoDataFound.isHidden = true
                self.tblCreateGroupList.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
                self.tblCreateGroupList.isHidden = false
            }
        }
        self.tblCreateGroupList.reloadData()
        return  true
    }
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtUserName{
            txtUserName.resignFirstResponder()
        }
        return true
    }
}

//MARK: - Button extension
fileprivate extension CreatNewChatVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        //self.dismiss(animated: true, completion: nil)
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreateBroadCastAction(_ sender: UIButton){
        self.view.endEditing(true)
        
        var arrSelectedUser = [ChatHistoryData]()
        var dictOfDict = [String:[String:Any]]()
        
//        for obj in self.arrChatHistory {
//            if obj.strCreatUserStatus == true{
//                arrSelectedUser.append(obj)
//            }
//        }
        
        for obj in self.arrChatHistoryTotal {
            if obj.strCreatUserStatus == true{
                arrSelectedUser.append(obj)
            }
        }
        if self.imgUrl == ""{
            self.imgUrl = "http://koobi.co.uk:3000/front/img/loader.png"//self.imgGroupProfile.image = #imageLiteral(resourceName: "cellBackground")
        }
        if arrSelectedUser.count <= 1  {
            objAppShareData.showAlert(withMessage: "Please select atleast 2 members.", type: alertType.bannerDark,on: self)
        }else{
            
            let newObj = self.arrChatHistoryMyData[0]
            let Key = newObj.strOpponentId
            let img = newObj.strOpponentProfileImage
            let name = newObj.strOpponentName
            let FT = newObj.strOpponentFireBaseToken
            
            let time = ServerValue.timestamp()
            
            //dictOfDict.updateValue(["createdDate":time, "firebaseToken":FT, "memberId":Key, "mute":"0", "profilePic":img, "type":"admin", "userName":name], forKey: Key)
            
            for objUser in arrSelectedUser{
                let Key = objUser.strOpponentId
                let img = objUser.strOpponentProfileImage
                let name = objUser.strOpponentName
                let FT = objUser.strOpponentFireBaseToken
                let time = ServerValue.timestamp()
                
                dictOfDict.updateValue(["createdDate":time,
                                        "firebaseToken":FT,
                                        "memberId":Key,
                                        "mute":"0",
                                        "profilePic":img,
                                        "type":"member",
                                        "userName":name], forKey: Key)
            }
            
            
            let dict = ["adminId":self.strMyChatId,
                        "groupImg":self.imgUrl,
                        "groupName":String(arrSelectedUser.count)+" Members",
                        "member":dictOfDict] as [String : Any]
            ref.child("broadcast").child(self.newBroadCastID).setValue(dict)
            
            
            let objMyData = arrSelectedUser[0]
            let dictHistory = ["favourite":0,
                               "memberCount":arrSelectedUser.count,
                               "memberType":"",
                               "message":"",
                               "messageType":0,
                               "profilePic":self.imgUrl,
                               "reciverId":self.newBroadCastID,
                               "senderId":self.strMyChatId,
                               "timestamp":ServerValue.timestamp(),
                               "type":"broadcast",
                               "unreadMessage":0,
                               "userName":String(arrSelectedUser.count)+" Members"] as [String : Any]
            ref.child("chat_history").child(self.strMyChatId).child(self.newBroadCastID).setValue(dictHistory)
            
            /*
             
             for i in 0...arrSelectedUser.count-1{
             let obj = arrSelectedUser[i]
             let oppId = obj.strOpponentId
             let time = ServerValue.timestamp()
             
             let dictHistory = ["favourite":0,
             "memberCount":arrSelectedUser.count+1,
             "memberType":"member",
             "message":"",
             "messageType":0,
             "profilePic":obj.strOpponentProfileImage,
             "reciverId":self.newBroadCastID,
             "senderId":self.strMyChatId,
             "timestamp":time,
             "type":"user",
             "unreadMessage":0,
             "userName":self.txtUserName.text ?? ""] as [String : Any]
             
             ref.child("chat_history").child(oppId).child(self.newBroadCastID).setValue(dictHistory)
             */
            //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            // }
        }
    }
}
//MARK: - tableview method extension
extension CreatNewChatVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCreatGroup", for: indexPath) as? CellCreatGroup{
            let objChatList = arrChatHistory[indexPath.row]
            if objChatList.strOpponentProfileImage == ""{
                cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
            }else{
                let url = URL(string: objChatList.strOpponentProfileImage)
                //cell.imgUserProfile.af_setImage(withURL: url!)
                cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }
            cell.btnProfile.tag = indexPath.row
            cell.lblUserName.text = objChatList.strOpponentName
            cell.btnChackUnchack.tag = indexPath.row
            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
            if objChatList.strCreatUserStatus == true { cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")  }
            cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
            cell.imgChackUnchack.isHidden = true
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        objChatShareData.fromChatToHistory = true
        let objMyEventsData = self.arrChatHistory[sender.tag]
        let objChatList = ChatHistoryData()
        objChatList.strOpponentId = objMyEventsData.strOpponentId
        objChatList.strOpponentName = objMyEventsData.strOpponentName
        objChatList.strOpponentProfileImage = objMyEventsData.strOpponentProfileImage
        objChatShareData.fromChatVC = true
            
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        if let detailVC = sb.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC {
        detailVC.objChatHistoryModel = objChatList
        detailVC.modalPresentationStyle = .fullScreen
        self.present(detailVC, animated: true, completion: nil)
        }
    }
}


//MARK: - FireBase Method calls
extension CreatNewChatVC{
    func GetUserListFromFirebase() {
        objWebserviceManager.StartIndicator()
        self.ref.child("users").observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
            self?.arrChatHistory.removeAll()
            self?.arrChatHistoryMyData.removeAll()
            self?.arrChatHistoryTotal.removeAll()

            
            if let arr = snapshot.value as? [Any]{
                
                for  dicttt in arr {
                    if let dict = dicttt as? [String:Any]{
                        print("\n\ndicttt = \(dict)")
                        
                        let objChatList = ChatHistoryData()
                        objChatList.strOpponentName = dict["userName"] as? String ?? ""
                        objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                        objChatList.strUserType = "member"
                        objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                        objChatList.strMuteStatus = "0"
                        objChatList.strCreatUserStatus = false
                        if let id = dict["uId"] as? Int {
                            objChatList.strOpponentId = String(id)
                        }else if let id = dict["uId"] as? String {
                            objChatList.strOpponentId = id
                        }
                        
                        if objChatList.strOpponentId == self?.strMyChatId{
                            if objChatList.strOpponentName.count>0{
                            self?.arrChatHistoryMyData.append(objChatList)
                            }
                        }else{
                            if objChatList.strOpponentName.count>0{
                            self?.arrChatHistory.append(objChatList)
                            self?.arrChatHistoryTotal.append(objChatList)
                            }
                        }
                        
                    }
                    objWebserviceManager.StopIndicator()
                    if (self?.arrChatHistory.count)! > 0 {
                        self?.lblNoDataFound.isHidden = true
                    }else{
                        self?.lblNoDataFound.isHidden = false
                    }
                }
                self?.tblCreateGroupList.reloadData()
                objWebserviceManager.StopIndicator()
                
                if (self?.arrChatHistory.count == 0) {
                    self?.lblNoDataFound.isHidden = false;
                    self?.tblCreateGroupList.isHidden = true;
                } else {
                    self?.lblNoDataFound.isHidden = true;
                    self?.tblCreateGroupList.isHidden = false;
                }
                objWebserviceManager.StopIndicator()
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                strongSelf.arrChatHistory.removeAll()
                strongSelf.arrChatHistoryMyData.removeAll()
                strongSelf.tblCreateGroupList.reloadData()
            }
        })
    }
    
    func parseDataChatHistory(dictHistory:NSDictionary){
        self.arrChatHistory.removeAll()
        self.arrChatHistoryMyData.removeAll()
        
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                
                let objChatList = ChatHistoryData()
                objChatList.strOpponentName = dict["userName"] as? String ?? ""
                objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                
                if let id = dict["uId"] as? Int {
                    objChatList.strOpponentId = String(id)
                }else if let id = dict["uId"] as? String {
                    objChatList.strOpponentId = id
                }
                
                objChatList.strUserType = "member"
                objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                objChatList.strMuteStatus = "0"
                if objChatList.strOpponentId == self.strMyChatId{
                    if objChatList.strOpponentName.count>0{
                    self.arrChatHistoryMyData.append(objChatList)
                    }
                }else{
                    if objChatList.strOpponentName.count>0{
                    self.arrChatHistory.append(objChatList)
                    self.arrChatHistoryTotal.append(objChatList)
                    }

                }
                
            }
            objWebserviceManager.StopIndicator()
            
            if arrChatHistory.count > 0 {
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
        }
        
        print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
        self.tblCreateGroupList.reloadData()
        objWebserviceManager.StopIndicator()
        
        if (self.arrChatHistory.count == 0) {
            self.lblNoDataFound.isHidden = false;
            self.tblCreateGroupList.isHidden = true;
        } else {
            self.lblNoDataFound.isHidden = true;
            self.tblCreateGroupList.isHidden = false;
        }
    }
}

extension CreatNewChatVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}
