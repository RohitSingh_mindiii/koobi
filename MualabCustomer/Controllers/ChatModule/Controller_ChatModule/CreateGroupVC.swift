//
//  CreateGroupVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/22/18.
//  Copyright © 2018 Mindiii. All rights reserved.
 
import UIKit
import Firebase
import Alamofire
import AlamofireImage


class CreateGroupVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate{
   
    @IBOutlet weak var tblCreateGroupList:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var txtEnterGroupName: UITextField!
    @IBOutlet weak var txtEnterGroupDescription: UITextView!

    @IBOutlet weak var imgGroupProfile:UIImageView!
    
    var imagePicker = UIImagePickerController()
    var strName = ""
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var storageRef: StorageReference!

    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [ChatHistoryData]()
    fileprivate var arrChatHistoryMyData = [ChatHistoryData]()
    fileprivate var newGroupName = ""
    fileprivate var imgUrl = ""

    fileprivate var fromImage = false
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
    
}
//MARK: - System Method extension
extension CreateGroupVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.strName = UserDefaults.standard.string(forKey: UserDefaults.keys.myName) ?? ""
        self.txtEnterGroupName.delegate = self
        self.txtEnterGroupDescription.delegate = self

        ref =  Database.database().reference()
        storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
        
        self.strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.viewConfigure()
        self.GetUserListFromFirebase()
        
        self.viewConfigure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if fromImage == false{
        self.GetUserListFromFirebase()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5)) {
            self.imgGroupProfile.layer.cornerRadius = 40.0
            self.imgGroupProfile.clipsToBounds = true
            }
        }
        fromImage = false
        self.getGroupName()

    }
    override func viewDidAppear(_ animated: Bool) {
        self.imgGroupProfile.layer.cornerRadius = 40.0
        self.imgGroupProfile.layer.masksToBounds = true
    }
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getGroupName(){
        self._refHandle = self.ref.child("group") .observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
            var groupName = 0

            if snapshot.exists(){
                let keys  = snapshot.key
                if let dict = snapshot.value as? NSDictionary{
                    
                    if let dictNew = dict as? [String:Any] {
                        for obj in dictNew{
                           let a = obj.key
                            print(a)
                            let arrName = a.split(separator: "_")
                            if arrName.count >= 2 {
                                var newName = 0
                                newName = Int(arrName[1])!
                                if newName >= groupName {
                                    groupName = newName
                                    print(groupName)
                                }
                            }
                        }
                    }
                    
                } else {
                    
                }
            }
            else{
                
            }
             let newGroup = groupName+1
             self?.newGroupName = "group_"+String(newGroup)
        })
    }

}


//MARK: - Custome method extension
fileprivate extension CreateGroupVC{
    func viewConfigure(){
        self.txtEnterGroupDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtEnterGroupDescription.tintColorDidChange()
        self.tblCreateGroupList.delegate = self
        self.tblCreateGroupList.dataSource = self
        self.txtEnterGroupName.delegate = self
        self.imagePicker.delegate = self
        ref = Database.database().reference()
        self.txtEnterGroupName.attributedPlaceholder = NSAttributedString(string: "Type group name",
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtEnterGroupDescription {
            if (text.count ) != 0
            { searchString = self.txtEnterGroupDescription.text! + (text).uppercased()
                newLength = (self.txtEnterGroupDescription.text?.count)! + text.count - range.length
            }
            else {
                //searchString = (self.txtEnterGroupDescription.text as NSString?)?.substring(to: (self.txtEnterGroupDescription.text?.count)! - 1).uppercased()
            }
            if newLength <= 200{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
 
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            var edit = true
            var newLength = 0
            var searchString: String? = nil
            if textField == self.txtEnterGroupName {
                 if (string.count ) != 0
                { searchString = self.txtEnterGroupName.text! + (string).uppercased()
                    newLength = (self.txtEnterGroupName.text?.count)! + string.count - range.length
                }
                else { searchString = (self.txtEnterGroupName.text as NSString?)?.substring(to: (self.txtEnterGroupName.text?.count)! - 1).uppercased()
                }
                 if newLength <= 25{  edit = true
                }else{   edit = false  }
                 return edit
            }
            return edit
        }
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEnterGroupName{
            txtEnterGroupName.resignFirstResponder()
        }
        return true
    }
}

//MARK: - Button extension
fileprivate extension CreateGroupVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnCreateGroupAction(_ sender: UIButton){
        self.view.endEditing(true)
    self.txtEnterGroupName.text = self.txtEnterGroupName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    self.txtEnterGroupDescription.text = self.txtEnterGroupDescription.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        var arrSelectedUser = [ChatHistoryData]()
        var dictOfDict = [String:[String:Any]]()
        
       // for i in 0...self.arrChatHistory.count-1{
        for obj in self.arrChatHistory {
            //let obj = self.arrChatHistory[index]
          //  let indexPath = IndexPath(row:i, section: (sender as AnyObject).superview!!.tag)
           // let cell = (tblCreateGroupList?.cellForRow(at: indexPath) as? CellCreatGroup)!
            if obj.strCreatUserStatus == true{
                arrSelectedUser.append(obj)
            }
        }
        
        
        if self.imgUrl == ""{
            self.imgUrl = "http://koobi.co.uk:3000/uploads/default_group.png"
            //self.imgUrl = "http://koobi.co.uk:3000/uploads/default_user.png"
            //self.imgGroupProfile.image = #imageLiteral(resourceName: "cellBackground")
        }
        if self.txtEnterGroupName.text?.count == 0 {
            objAppShareData.showAlert(withMessage: "Please enter group name", type: alertType.bannerDark,on: self)
        }else if self.txtEnterGroupDescription.text?.count == 0 {
            objAppShareData.showAlert(withMessage: "Please enter group description", type: alertType.bannerDark,on: self)
        }else if arrSelectedUser.count <= 0  {
            objAppShareData.showAlert(withMessage: "Please select any member", type: alertType.bannerDark,on: self)
        }else{
            
            let newObj = self.arrChatHistoryMyData[0]
            let Key = newObj.strOpponentId
            let img = newObj.strOpponentProfileImage
            let name = newObj.strOpponentName
            let FT = newObj.strOpponentFireBaseToken
            
            let time = ServerValue.timestamp()
           
            dictOfDict.updateValue(["createdDate":time,
                                    "firebaseToken":FT,
                                    "memberId":Int(Key),
                                    "mute":0,
                                    "profilePic":img,
                                    "type":"admin",
                                    "userName":name], forKey: Key)
            
            for objUser in arrSelectedUser{
                let Key = objUser.strOpponentId
                let img = objUser.strOpponentProfileImage
                let name = objUser.strOpponentName
                let FT = objUser.strOpponentFireBaseToken
                let time = ServerValue.timestamp()
               
                dictOfDict.updateValue(["createdDate":time,
                                        "firebaseToken":FT,
                                        "memberId":Int(Key),
                                        "mute":0,
                                        "profilePic":img,
                                        "type":"member",
                                        "userName":name], forKey: Key)
                let dictNew = [self.newGroupName:self.newGroupName]
                ref.child("myGroup").child(Key).updateChildValues(dictNew)
             }
        
        
         let dict = ["adminId":Int(self.strMyChatId),
                     "groupId":self.newGroupName,
                    "groupImg":self.imgUrl,
                    "groupDescription":self.txtEnterGroupDescription.text,
                    "groupName":self.txtEnterGroupName.text ?? "",
                    "member":dictOfDict,
                    "request":""] as [String : Any]
        ref.child("group").child(self.newGroupName).setValue(dict)
       
            let objMyData = arrSelectedUser[0]
             let dictHistory = ["favourite":0,
                           "memberCount":arrSelectedUser.count+1,
                               "memberType":"admin",
                               "message":"",
                               "messageType":0,
                               "profilePic":self.imgUrl,//objMyData.strOpponentProfileImage,
                               "reciverId":self.newGroupName,
                               "senderId":self.strMyChatId,
                               "timestamp":ServerValue.timestamp(),
                               "type":"group",
                               "unreadMessage":0,
                               "userName":self.txtEnterGroupName.text ?? ""] as [String : Any]
              ref.child("chat_history").child(self.strMyChatId).child(self.newGroupName).setValue(dictHistory)
            
            for i in 0...arrSelectedUser.count-1{
                let obj = arrSelectedUser[i]
                let oppId = obj.strOpponentId
                let time = ServerValue.timestamp()

                let dictHistory = ["favourite":0,
                                   "memberCount":arrSelectedUser.count+1,
                                   "memberType":"member",
                                   "message":"",
                                   "messageType":0,
                                   "profilePic":self.imgUrl,
                                   "reciverId":self.newGroupName,
                                   "senderId":self.strMyChatId,
                                   "timestamp":time,
                                   "type":"group",
                                   "unreadMessage":0,
                                   "userName":self.txtEnterGroupName.text ?? ""] as [String : Any]
              
  ref.child("chat_history").child(oppId).child(self.newGroupName).setValue(dictHistory)
        let dictNew = [self.newGroupName:self.newGroupName]
    ref.child("myGroup").child(self.strMyChatId).updateChildValues(dictNew)
        }
        self.createDataSetForNotification(arrSelecteduser:arrSelectedUser)
        self.navigationController?.popViewController(animated: true)
      }
    }
    
    func createDataSetForNotification(arrSelecteduser:[ChatHistoryData]){
        
        var arrFirebaseToken : [String] = []
        for objUser in arrSelecteduser {
            
            let firebaseToken = objUser.strOpponentFireBaseToken
            
            if firebaseToken == "" {
                let webNotification = [ "body": checkForNULL(obj:self.strName + " added you"),
                                        "title": checkForNULL(obj: self.strName + " @ " + self.txtEnterGroupName.text!),
                                        "url":""]
                self.ref.child("webnotification").child(objUser.strOpponentId).childByAutoId().setValue(webNotification)
                
            } else {
                if firebaseToken != objAppShareData.firebaseToken{
                    arrFirebaseToken.append(firebaseToken)
                }
            }
        }
        
        if arrFirebaseToken.count > 0 {
            
            let messageDict = ["body": checkForNULL(obj:self.strName + " added you"),
                               "title": checkForNULL(obj: self.strName + " @ " + self.txtEnterGroupName.text!),
                               "icon": "icon",
                               "sound": "default",
                               "badge": "1",
                               "adminId": self.strMyChatId,
                               "message": self.strName + " added you",
                               "type": "groupChat",
                               "notifincationType": "15",
                               "click_action": "ChatActivity",
                               "opponentChatId": checkForNULL(obj: self.newGroupName)]
            
            //IOS
            let notificationDict = ["body": checkForNULL(obj:self.strName + " added you"),
                                    "title": checkForNULL(obj: self.strName + " @ " + self.txtEnterGroupName.text!),
                                    "icon": "icon",
                                    "sound": "default",
                                    "badge": "1",
                                    "adminId": self.strMyChatId,
                                    "message": self.strName + " added you",
                                    "notifincationType": "15",
                                    "type": "groupChat",
                                    "click_action": "ChatActivity",
                                    "opponentChatId": checkForNULL(obj: self.newGroupName)]
            
            let finalDict = ["registration_ids": arrFirebaseToken,
                             "data": checkForNULL(obj:messageDict),
                             "notification": checkForNULL(obj:notificationDict),
                             "sound": "default"] as [String : Any]
            print(finalDict)
            self.sendNotificationWithDict(dictNotification:finalDict)
        }
    }
    
    func sendNotificationWithDict(dictNotification:Dictionary<String, Any>){
        
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StartIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let strUrl = "https://fcm.googleapis.com/fcm/send"
        var request = URLRequest.init(url: URL.init(string: strUrl)!)
        request.setValue("key=" + objChatShareData.kServerKey, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dictNotification, options: .prettyPrinted)
        request.httpBody = jsonData
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                ////print("error calling POST on /todos/1")
                ////print(error!)
                return
            }
            guard data != nil else {
                ////print("Error: did not receive data")
                return
            }
            }.resume()
    }
    @IBAction func btnCameraAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.view .endEditing(true)
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnEmoziAction(_ sender: UIButton){
        self.view.endEditing(true)
    }
}

//MARK: - tableview method extension
extension CreateGroupVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCreatGroup", for: indexPath) as? CellCreatGroup{
             let objChatList = arrChatHistory[indexPath.row]
             if objChatList.strOpponentProfileImage == ""{
                cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
            }else{
                let url = URL(string: objChatList.strOpponentProfileImage)
                //cell.imgUserProfile.af_setImage(withURL: url!)
                cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }
            cell.btnProfile.tag = indexPath.row
            cell.lblUserName.text = objChatList.strOpponentName
            cell.btnChackUnchack.tag = indexPath.row
            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
            if objChatList.strCreatUserStatus == true { cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")  }
            cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)

            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        let objMyEventsData = self.arrChatHistory[sender.tag]
 
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblCreateGroupList?.cellForRow(at: indexPath) as? CellCreatGroup)!
        
        
        if objMyEventsData.strCreatUserStatus == false {
            objMyEventsData.strCreatUserStatus = true
            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")

        }else {
            objMyEventsData.strCreatUserStatus = false
            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
        }
        
        
//        if cell.imgChackUnchack.image == #imageLiteral(resourceName: "ico_uncheck") {
//            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_check")
//        } else {
//            cell.imgChackUnchack.image = #imageLiteral(resourceName: "ico_uncheck")
//        }
  }
}


//MARK: - UIImagePicker extension Methods
extension CreateGroupVC{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //imagePicker.allowsEditing = true
        var img : UIImage?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            img = image
         }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            img = image
         }
//        if let Image = img {
//             self.sendImageToFirebaseWithImage(image: Image)
//            self.imgGroupProfile.layer.cornerRadius = 40.0
//            self.imgGroupProfile.layer.masksToBounds = true
//             imgGroupProfile.image = Image
//        }
       // self.dismiss(animated: true, completion: nil)
        
            let imageCropVC = RSKImageCropViewController(image: img!, cropMode: .custom)
            imageCropVC.delegate = self
            imageCropVC.dataSource = self
            
            imageCropVC.view.setNeedsUpdateConstraints()
            imageCropVC.view.layoutSubviews()
            
            var cons = imageCropVC.view.constraints
            var newConstraints = [AnyHashable]()
            for object: NSLayoutConstraint in cons {
                if object.identifier == nil || ((object.identifier as NSString?)?.range(of: "Layout-Height"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Width"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Left"))?.location != NSNotFound {
                    newConstraints.append(object)
                }
            }
            imageCropVC.view.removeConstraints(imageCropVC.view.constraints)
            if let aConstraints = newConstraints as? [NSLayoutConstraint] {
                imageCropVC.view.addConstraints(aConstraints)
            }
            picker.dismiss(animated: false) {() -> Void in
            }
            navigationController?.pushViewController(imageCropVC, animated: false)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }
    
    func openCamera() //to Access Camera
    {
       self.fromImage = true
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.modalPresentationStyle = .fullScreen
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {}
    }
    
    func openGallary() //to Access Gallery
    {    self.fromImage = true
        //imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
        })
    }
    
    
    func sendImageToFirebaseWithImage(image : UIImage){
         imageData = objChatShareData.compressImage(image: image) as Data?
        let localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        objWebserviceManager.StartIndicator()
        self.storageRef.child(localFilePath).putData(imageData!, metadata: metadata) { [weak self] (metadata, error) in
            objWebserviceManager.StopIndicator()
            if let error = error {
                print("Error uploading: \(error)")
                return
            }
            guard let strongSelf = self else { return }
            self?.storageRef.child(localFilePath).downloadURL { (url, error) in
              guard let downloadURL = url else {
                return
              }
                strongSelf.imgUrl = downloadURL.absoluteString
            }
             print(">> imgUrl =  \(self?.imgUrl)")
         }
     }
 }


//MARK: - FireBase Method calls
extension CreateGroupVC{
     func GetUserListFromFirebase() {
         objWebserviceManager.StartIndicator()
        self.ref.child("users").observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
             self?.arrChatHistory.removeAll()
            self?.arrChatHistoryMyData.removeAll()

            if let arr = snapshot.value as? [Any]{
                
                for  dicttt in arr {
                    
                    if let dict = dicttt as? [String:Any]{
                        print("\n\ndicttt = \(dict)")
                        
                        let objChatList = ChatHistoryData()
                        objChatList.strOpponentName = dict["userName"] as? String ?? ""
                        objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                        objChatList.strUserType = "member"
                        objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                        objChatList.strMuteStatus = "0"
                        objChatList.strCreatUserStatus = false
                        if let id = dict["uId"] as? Int {
                            objChatList.strOpponentId = String(id)
                        }else if let id = dict["uId"] as? String {
                            objChatList.strOpponentId = id
                        }
                        
                        if objChatList.strOpponentId == self?.strMyChatId{
                            if objChatList.strOpponentName.count>0{
                            self?.arrChatHistoryMyData.append(objChatList)
                            }
                        }else{
                            if objChatList.strOpponentName.count>0{
                            self?.arrChatHistory.append(objChatList)
                            }
                        }

                    }
                    
                    objWebserviceManager.StopIndicator()
                    
                    if (self?.arrChatHistory.count)! > 0 {
                        self?.lblNoDataFound.isHidden = true
                    }else{
                        self?.lblNoDataFound.isHidden = false
                    }
                }
                
                self?.tblCreateGroupList.reloadData()
                ////
                self?.tblCreateGroupList.contentSize.height = ((self?.tblCreateGroupList.contentSize.height)!) + 60.0
                ////
                objWebserviceManager.StopIndicator()
                
                if (self?.arrChatHistory.count == 0) {
                    self?.lblNoDataFound.isHidden = false;
                    self?.tblCreateGroupList.isHidden = true;
                } else {
                    self?.lblNoDataFound.isHidden = true;
                    self?.tblCreateGroupList.isHidden = false;
                }
                objWebserviceManager.StopIndicator()
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                strongSelf.arrChatHistory.removeAll()
                strongSelf.arrChatHistoryMyData.removeAll()
                strongSelf.tblCreateGroupList.reloadData()
                ////
                strongSelf.tblCreateGroupList.contentSize.height = (strongSelf.tblCreateGroupList.contentSize.height) + 60.0
                ////
            }
        })
    }

    func parseDataChatHistory(dictHistory:NSDictionary){
        self.arrChatHistory.removeAll()
        self.arrChatHistoryMyData.removeAll()

        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                
                let objChatList = ChatHistoryData()
                objChatList.strOpponentName = dict["userName"] as? String ?? ""
                objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                objChatList.strOpponentId = String(dict["uId"] as? Int ?? 0)
                objChatList.strUserType = "member"
                objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                objChatList.strMuteStatus = "0"

                if objChatList.strOpponentId == self.strMyChatId{
                    if objChatList.strOpponentName.count>0{
                    self.arrChatHistoryMyData.append(objChatList)
                    }
                }else{
                    if objChatList.strOpponentName.count>0{
                      self.arrChatHistory.append(objChatList)
                    }
                }

            }
            objWebserviceManager.StopIndicator()
            
            if arrChatHistory.count > 0 {
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
        }
        
        print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
        self.tblCreateGroupList.reloadData()
        ////
        self.tblCreateGroupList.contentSize.height = (self.tblCreateGroupList.contentSize.height) + 60.0
        ////
        objWebserviceManager.StopIndicator()
        
        if (self.arrChatHistory.count == 0) {
            self.lblNoDataFound.isHidden = false;
            self.tblCreateGroupList.isHidden = true;
        } else {
            self.lblNoDataFound.isHidden = true;
            self.tblCreateGroupList.isHidden = false;
        }
    }
}

// MARK: - RSKImageCropViewControllerDataSource methods
extension CreateGroupVC : RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource{
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        navigationController?.popViewController(animated: true)
        let imgPosted = croppedImage
        self.processSelectedImage(imgPosted: imgPosted)
    }
    
    // Returns a custom rect for the mask.
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
        var maskSize: CGSize
        // calculate the width based on the screen size - 30px
        let screenRect: CGRect = UIScreen.main.bounds
        let width: CGFloat = screenRect.size.width - 20
        // 4:3, height will be 75% of the width
        let height: CGFloat = width * 0.75
        if controller.isPortraitInterfaceOrientation() {
            maskSize = CGSize(width: width, height: height)
        }
        else {
            maskSize = CGSize(width: 355, height: 266.25)
        }
        let viewWidth: CGFloat = controller.view.frame.width
        let viewHeight: CGFloat = controller.view.frame.height
        let maskRect = CGRect(x: (viewWidth - maskSize.width) * 0.5, y: (viewHeight - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
        return maskRect
    }
    // Returns a custom rect in which the image can be moved.
    
    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
        // If the image is not rotated, then the movement rect coincides with the mask rect.
        return controller.maskRect
    }
    
    // Returns a custom path for the mask.
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
        return UIBezierPath(rect: controller.maskRect)
    }
    
    // MARK: - RSKImageCropViewControllerDelegate
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        //isImageCanceled = true
        navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        navigationController?.popViewController(animated: true)
        let imgPosted = croppedImage
        self.processSelectedImage(imgPosted: imgPosted)
    }
    func processSelectedImage(imgPosted : UIImage){
            self.sendImageToFirebaseWithImage(image: imgPosted)
            self.imgGroupProfile.layer.cornerRadius = 40.0
            self.imgGroupProfile.layer.masksToBounds = true
            imgGroupProfile.image = imgPosted
    }
}



extension CreateGroupVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
    
}
