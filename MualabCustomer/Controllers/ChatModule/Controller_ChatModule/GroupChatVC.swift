//
//  GroupChatVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/4/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage
import Kingfisher

class GroupChatVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate{
    var objChatHistoryModel = ChatHistoryData()
    var isLoading = false
    var isLastMsg = false
    var indexOfArray = 0
    var objHoldIndex = ChatData.init(dict: [:], key: "")
    var isFirstTimeScrollToBottom = false
    @IBOutlet weak var lblAfterLeaveGroup: UILabel!
    
    @IBOutlet weak var txtViewContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var txtViewHeight: NSLayoutConstraint!
    //MARK: - Outlat declaration
    @IBOutlet weak var lblFavouriteStatus: UILabel!
    @IBOutlet weak var txtViewContainer: UIView!
    @IBOutlet weak var viewSeparateReportLine: UIView!
    fileprivate var isImageLocallyUploading:Bool = false
    @IBOutlet weak var viewZoomImg: UIView!
    @IBOutlet weak var imgZoomImage: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var scrollViewImg: UIScrollView!
    
    @IBOutlet weak var imgBgPopUpBig: UIImageView!
    @IBOutlet weak var imgBgPopUpSmall: UIImageView!
    @IBOutlet weak var lblOpponentName: UILabel!
    @IBOutlet weak var viewNoChatFound: UIView!
    @IBOutlet weak var viewCommentAction: UIView!
    @IBOutlet weak var viewEditCommentAction: UIView!
    @IBOutlet weak var lblDeleteUserName: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgOpponent: UIImageView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var tableChat: UITableView!
    
    @IBOutlet weak var viewFor_Block_Delete: UIView!
    @IBOutlet weak var viewReportButton: UIView!
    
    @IBOutlet weak var viewForDelete: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var txtViewChat: UITextView!
    @IBOutlet weak var lblOnlineStatus: UILabel!
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    @IBOutlet weak var viewAddMember: UIView!
    @IBOutlet weak var viewRemoveMember: UIView!
    @IBOutlet weak var viewGroupDelete: UIView!
    @IBOutlet weak var viewGroupRequest: UIView!
    
    @IBAction func btnSeeAllRequest(_ sender: UIButton) {
    }
    @IBAction func btnDeleteGroup(_ sender: UIButton) {
    }
    
    fileprivate var strKeyValueForHold:String = ""
    fileprivate var arrKeyValueForHold = [String]()
    fileprivate let dateToday = Date()
    fileprivate let formatter = DateFormatter()
    fileprivate var resultDate = ""
    
    //MARK: - Variable declaration
    // var isFromDeleteSingleMsg:Bool = false
    fileprivate var updateReadStatus = false
    fileprivate var indexToHighlight:Int = -1
    fileprivate var arrIndexToHighlight:[Int] = []
    
    fileprivate var imgType:Int = 0
    
    fileprivate var isPullToRefresh = false
    fileprivate var isOppNotification:String = "1"
    fileprivate var isImagePicked:Bool = false
    fileprivate var isSendButtonClicked:Bool = false
    fileprivate var dictionaryMessages =  [String: [String:Any]]()
    fileprivate var arrNewFirebaseToken : [String] = []
    fileprivate var storageRef: StorageReference!
    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    //BlockUser
    fileprivate var blockedBy: BlockStatus = BlockStatus.kBlockedByNone
    //BlockUser
    
    //Video call
    fileprivate var isOnChatScreen : Bool = false
    
    fileprivate var isFromBlock = false
    
    
    fileprivate var TimeStamp:Double = 0
    
    fileprivate var strTextNotification:String = ""
    fileprivate var strName:String = ""
    fileprivate var profilePic:String = ""
    fileprivate var strMyChatId:String = ""
    
    fileprivate var isChatAppearFirst:Bool = false
    
    fileprivate let txtViewMsgMaxHeight: CGFloat = 100
    fileprivate let txtViewMsgMinHeight: CGFloat = 42
    
    fileprivate let imagePicker = UIImagePickerController()
    private var tap: UITapGestureRecognizer!
    
    fileprivate var arrMessages = [ChatData]()
    fileprivate var message = ""
    fileprivate var imgUrl = ""
    
    fileprivate var isTableScrollToTop:Bool = false
    fileprivate var lastContentOffset: CGFloat = 0
    fileprivate var strPaginationKey:String = ""
    
    fileprivate let myChatRoom =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    
    lazy var messagesRef: DatabaseReference = Database.database().reference().child("groupChat").child(myChatRoom)
    var newMessageRefHandle: DatabaseHandle?
    
    
    //MARK: - System Method Extension
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCommentAction.isHidden = true
        self.viewEditCommentAction.isHidden = true
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.lblAfterLeaveGroup.isHidden = true
        self.isOnChatScreen = true
        self.view.endEditing(true)
        self.registerForKeyboardNotifications()
        self.txtViewContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.txtViewContainer.layer.borderWidth = 1
        objWebserviceManager.StartIndicator()
        ref = Database.database().reference()
        self.memberEntryTime()
        self.delegateColling()
        self.dataParseFromDidload()
        self.viewConfigure()
        self.observeKeyboard()
        self.configureStorage()
        self.deleteMsgHistory()
        self.clearUnreadChatHistory()
        self.txtViewChat.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtViewChat.tintColorDidChange()
        self.getArrayForAllMembersToken()
        objAppShareData.isFromDidFinish = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if objChatShareData.fromGroupChatDelete{
            objChatShareData.fromGroupChatDelete = false
            self.dismiss(animated: false, completion: nil)
        }
        
        if objChatShareData.fromReportVC{
            objChatShareData.fromReportVC = false
            objAppShareData.showAlert(withMessage: "Report submitted successfully", type: alertType.bannerDark,on: self)
        }
        
        self.isFirstTimeScrollToBottom = true
        self.isOnChatScreen = true
        self.isChatAppearFirst = true
        objChatShareData.isOnChatScreen =  true
        objChatShareData.currentOpponantIDForNotification = objChatHistoryModel.strOpponentId
        self.view.endEditing(true)
        self.adminStatus()
        
        if !self.isImagePicked{
            getOpponentDetailsFromFirebase()
            self.deleteMsgHistory()
        }
    }
    
    func adminStatus(){
        
        //if self.objChatHistoryModel.strMemberType == "admin" {
        if self.objChatHistoryModel.strAdminId == self.strMyChatId{
            self.viewSeparateReportLine.isHidden = true
            self.viewReportButton.isHidden = true
            self.imgBgPopUpBig.isHidden = false
            self.imgBgPopUpSmall.isHidden = true
            
            self.viewAddMember.isHidden = false
            self.viewRemoveMember.isHidden = false
            self.viewGroupDelete.isHidden = false
            self.viewGroupRequest.isHidden = true
            self.viewGroupDelete.isHidden = true
            //self.viewGroupRequest.isHidden = false
        }else{
            self.viewSeparateReportLine.isHidden = false
            self.viewReportButton.isHidden = false
            self.imgBgPopUpBig.isHidden = true
            self.imgBgPopUpSmall.isHidden = false
            
            self.viewAddMember.isHidden = true
            self.viewRemoveMember.isHidden = true
            self.viewGroupDelete.isHidden = true
            self.viewGroupRequest.isHidden = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        objAppShareData.setView(view: self.viewMsgDay, hidden: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.isOnChatScreen = false
        //self.ref.removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        self.deregisterFromKeyboardNotifications()
    }
   
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ChatVC.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
    }
}

//MARK: - Custom Method Extension
extension GroupChatVC {
    
    func viewConfigure(){
        //self.tableChat.addSubview(self.refreshControl)
        self.viewMsgDay.isHidden = true
        self.isChatAppearFirst = true
        self.viewNoChatFound.isHidden = true
        self.viewFor_Block_Delete.isHidden = true
        self.viewMenu.isHidden = true
        
        self.txtViewChat.layer.cornerRadius = 18
        
        imagePicker.delegate = self
        self.tableChat.delegate = self
        self.tableChat.dataSource = self
        tableChat.rowHeight = UITableView.automaticDimension
        self.viewZoomImg.isHidden = true
        
        tableChat.estimatedRowHeight = 50.0
        tableChat.rowHeight = UITableView.automaticDimension
    }
    
    func dataParseFromDidload(){
        
        formatter.dateFormat = "dd-MMMM-yyyy"
        self.resultDate = formatter.string(from: dateToday)
        
        self.lblOpponentName.text = objChatHistoryModel.strOpponentName
        self.lblDeleteUserName.text = objChatHistoryModel.strOpponentName+"?"
        
        // DispatchQueue.main.async {
        if self.objChatHistoryModel.strOpponentProfileImage == ""{
            self.imgOpponent.image = UIImage.init(named: "user_img_ico")
        }else{
            let url = URL(string: self.objChatHistoryModel.strOpponentProfileImage)
            //self.imgOpponent.af_setImage(withURL: url!)
            self.imgOpponent.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
        }
        //   }
        self.strName = UserDefaults.standard.string(forKey: UserDefaults.keys.myName) ?? ""
        self.profilePic = UserDefaults.standard.string(forKey: UserDefaults.keys.myImage) ?? ""
    }
    
    func delegateColling(){
        self.tableChat.delegate = self
        self.tableChat.dataSource = self
        self.scrollViewImg.minimumZoomScale = 1.0
        self.scrollViewImg.maximumZoomScale = 5.0
        self.scrollViewImg.delegate = self
        self.txtViewChat.delegate = self
        self.txtViewChat.isScrollEnabled = true
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgZoomImage
    }
}

//MARK:- Get Opponent details
extension GroupChatVC{
    
    func getOpponentDetailsFromFirebase() {
        objWebserviceManager.StartIndicator()
        
        // Listen for new messages in the Firebase database
        _refHandle = self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
            //////print("snapshot = \(snapshot)")
            guard let strongSelf = self else { return }
            if let dict = snapshot.value as? [String:Any]{
                strongSelf.parseDataOpponentDetail(dict: dict)
            } else {
                if self?.isImagePicked == false {
                    objWebserviceManager.StopIndicator()
                }
            }
        })
    }
    
    func parseDataOpponentDetail(dict:[String:Any]){
        objChatHistoryModel.strMemberCount = dict.values.count
        ////print("dictResponce = \(dict)")
        self.lblOpponentName.text = dict["groupName"] as? String ?? ""
        objChatHistoryModel.strOpponentName = dict["groupName"] as? String ?? ""
        objChatHistoryModel.strOpponentProfileImage = dict["groupImg"] as? String ?? ""
        
        if let adminId = dict["adminId"] as? String{
            objChatHistoryModel.strAdminId =  adminId
        }else if let adminId = dict["adminId"] as? Int{
            objChatHistoryModel.strAdminId =  String(adminId)
        }
        objChatHistoryModel.strOpponentProfileImage = dict["groupImg"] as? String ?? ""
        
        
        objChatHistoryModel.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
        objChatHistoryModel.strOpponentIsLogin = 0
        
        self.lblOpponentName.text = objChatHistoryModel.strOpponentName
        if self.objChatHistoryModel.strOpponentProfileImage == ""{
            self.imgOpponent.image = UIImage.init(named: "user_img_ico")
        }else{
            let url = URL(string: self.objChatHistoryModel.strOpponentProfileImage)
            //self.imgOpponent.af_setImage(withURL: url!)
            self.imgOpponent.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
        }
        ////print(dict)
        if let dictt = dict["member"] as? Dictionary<String, Any> {
            objChatHistoryModel.strMemberCount = dictt.keys.count
            self.lblOnlineStatus.text = "\(objChatHistoryModel.strMemberCount)" + " Members"
            self.objChatHistoryModel.arrMemberId.removeAll()
            for obj in dictt{
                let a = obj.key
                self.objChatHistoryModel.arrMemberId.append(a)
            }
            if !self.objChatHistoryModel.arrMemberId.contains(self.strMyChatId){
                self.lblAfterLeaveGroup.isHidden = false
                self.btnMenu.isHidden = true
                self.imgMenu.isHidden = true
                self.txtViewChat.isUserInteractionEnabled = false
                //self.dismiss(animated: false, completion: nil)
            }else{
                self.btnMenu.isHidden = false
                self.imgMenu.isHidden = false
                self.lblAfterLeaveGroup.isHidden = true
                self.txtViewChat.isUserInteractionEnabled = true
            }
            self.lblOnlineStatus.text = "\(self.objChatHistoryModel.arrMemberId.count)" + " Members"
            
        }else if let arr = dict["member"] as? NSArray {
            self.objChatHistoryModel.arrMemberId.removeAll()
            
            for newObj in arr{
                if let newDict = newObj as? [String:Any] {
                    print("dictionary",newDict)
                    var memberId = ""
                    if let a1 = newDict["memberId"] as? Int{
                        memberId = String(a1)
                    }else if let a2 = newDict["memberId"] as? String{
                        memberId = a2
                    }
                    self.objChatHistoryModel.arrMemberId.append(memberId)
                }
            }
            if !self.objChatHistoryModel.arrMemberId.contains(self.strMyChatId){
                self.lblAfterLeaveGroup.isHidden = false
                self.btnMenu.isHidden = true
                self.imgMenu.isHidden = true
                self.txtViewChat.isUserInteractionEnabled = false
                //self.dismiss(animated: false, completion: nil)
            }
            objChatHistoryModel.strMemberCount = self.objChatHistoryModel.arrMemberId.count
            self.lblOnlineStatus.text = "\(objChatHistoryModel.strMemberCount)" + " Members"
        }
        
        if objAppShareData.isFromNotification{
            if self.objChatHistoryModel.strAdminId == self.strMyChatId{
                self.objChatHistoryModel.strMemberType = "admin"
                self.adminStatus()
            }
        }
        
        if isImagePicked == false {
           //objWebserviceManager.StopIndicator()
        }
    }
    
    func configureStorage() {
        ref =  Database.database().reference()
        if WebURL.BaseUrl.contains("dev"){
            storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
        }else{
           storageRef = Storage.storage().reference().child("gs://koobi-89a2d.appspot.com")
        }
    }
}

//MARK:- Get MyChat data methods
extension GroupChatVC{
    
    func getArrayForAllMembersToken(){
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            var arrTemp : [[String : Any]] = []
            if let arr = snapshot.value as? [Any]{
                for newObj in arr {
                    if let dict = newObj as? [String:Any] {
                        var memberId = ""
                        if let count = dict["memberId"] as? String {
                            memberId = count
                        }else if let count1 = dict["memberId"] as? Int {
                            memberId = String(count1)
                        }
                        self!.ref.child("users").child(memberId).observe(.value, with: { [weak self] (snapshot) -> Void in
                            
                            if snapshot.exists(){
                                let dictNew = snapshot.value as? [String:Any]
                                let firebaseToken = dictNew!["firebaseToken"] as? String ?? ""
                                if firebaseToken != objAppShareData.firebaseToken{
                                    self?.arrNewFirebaseToken.append(firebaseToken)
                                }
                            }
                        })
                    }
                }
            } else if let arr = snapshot.value as? [String:Any]{
                for newObj in arr {
                    if let dict = newObj.value as? [String:Any] {
                        var memberId = ""
                        if let count = dict["memberId"] as? String {
                            memberId = count
                        }else if let count1 = dict["memberId"] as? Int {
                            memberId = String(count1)
                        }
                        self!.ref.child("users").child(memberId).observe(.value, with: { [weak self] (snapshot) -> Void in
                            
                            if snapshot.exists(){
                                let dictNew = snapshot.value as? [String:Any]
                                let firebaseToken = dictNew!["firebaseToken"] as? String ?? ""
                                if firebaseToken != objAppShareData.firebaseToken{
                                    self?.arrNewFirebaseToken.append(firebaseToken)
                                }
                            }
                        })
                    }
                }
            }
        })
    }
    
    func observeMessages(lastMessageKey:String?) {

//        let messageQuery = self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).queryEnding(atValue: nil, childKey: lastMessageKey).queryLimited(toLast: 50)
//        newMessageRefHandle = messageQuery.observe(.value, with: { (snapshot) -> Void in
    self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self]  (snapshot)  -> Void in
            
            if objChatShareData.fromChatVC != true{
                objWebserviceManager.StopIndicator()
                return
            }
            if !objChatShareData.isOnChatScreen{
                return
            }
            self?.updateReadStatus = true
            
            self?.strPaginationKey = lastMessageKey ?? ""
            let dictKey = snapshot.key
            if let dict = snapshot.value as? [String:Any]{
                self?.parseChatData(dictN: dict)
            } else {
                objAppShareData.isFromDidFinish = false
                self?.arrMessages.removeAll()
                self?.tableChat.reloadData()
                self?.refreshControl.endRefreshing()
                //if !(self?.isImagePicked)!{
                objWebserviceManager.StopIndicator()
                //}
            }
        })
    }
    
    func parseChatData(dictN:[String:Any]){
        print(dictN)
        let arrTemp = self.arrMessages
        
        self.arrMessages.removeAll()
        for dictxxx in dictN{
            let strKey = dictxxx.key
            let dict = dictxxx.value as! [String:Any]
            
            //// Hang Up
            // DispatchQueue.main.async {
            if !objChatShareData.isOnChatScreen{
                return
            }
            
            /*
             if let memberCount = dict["readMember"] as? [String:Any]{
             if (dict["readStatus"] as? Int != 2){
             if (memberCount.count == self.objChatHistoryModel.strMemberCount){
             self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(strKey).updateChildValues(["readStatus":2])
             }else{
             if (dict["senderId"] as! String != self.strMyChatId) {
             self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(strKey).child("readMember").updateChildValues([self.strMyChatId: self.strMyChatId])
             }
             }
             }
             }
             */
            //          }
            ////
            /*
             if let memberCount = dict["readMember"] as? [String:Any]{
             if memberCount.count == self.objChatHistoryModel.strMemberCount{
             self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(strKey).updateChildValues(["readStatus":"2"])
             }
             }
             */
            let objChatList = ChatData.init(dict: dict,key:strKey)
            ////print(dict)
            
            if objChatList?.str_UId == self.strMyChatId{
                if let memberCount = dict["readMember"] as? [String:Any]{
                    if memberCount.count >= self.objChatHistoryModel.strMemberCount{
                        objChatList?.strReadStatus = "2"
                    }
                }
            }else{
                objChatList?.strReadStatus = "0"
            }
            
           //////////
            if (objChatList?.TimeStamp)! > self.objChatHistoryModel.strCreatTime{
                if self.objChatHistoryModel.strDeleteTime < (objChatList?.TimeStamp)!{
                    self.arrMessages.append(objChatList!)
                }
            }
            //////////
            /*
            if self.arrMessages.contains(objChatList!){
                ////print("hi")
                let index = self.arrMessages.index(of: objChatList!)
                self.arrMessages.remove(at: index!)
                if (objChatList?.TimeStamp)! > self.objChatHistoryModel.strCreatTime{
                    if self.objChatHistoryModel.strDeleteTime < (objChatList?.TimeStamp)!{
                        self.arrMessages.insert(objChatList!, at: index!)
                    }
                }
            }else{
                if (objChatList?.TimeStamp)! > self.objChatHistoryModel.strCreatTime{
                    if self.objChatHistoryModel.strDeleteTime < (objChatList?.TimeStamp)!{
                        self.arrMessages.append(objChatList!)
                    }
                }
            }*/
        }
        
        self.arrMessages =  self.arrMessages.sorted(by: { $0.TimeStamp < $1.TimeStamp})
        tableChat.reloadData()
        
        if isPullToRefresh && self.arrMessages.count > 50
        {
            //if self.arrMessages.count > 50{
            let remain = (self.arrMessages.count+1)%50
            if remain == 0{
                let indexPath = IndexPath(row: 49, section: 0)
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
            }else{
                let indexPath = IndexPath(row: remain, section: 0)
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
            }
        }else{
             /*
             let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
             self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
             */
            if arrMessages.count > 0{
                if !(self.strPaginationKey.count>0 && arrMessages.count <= 50){
                    let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
                    self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        }
        
        ////////
        if arrTemp.count>0{
            let obj = arrTemp[arrTemp.count-1]
            if obj.strMessage == "https://local" && obj.imageLocal == #imageLiteral(resourceName: "gallery_placeholder") {
            }else{
                if arrTemp.count == self.arrMessages.count{
                    if obj.strMessage.hasPrefix("content://com.google.android"){
                    }else{
                       self.arrMessages = arrTemp
                    }
                }else{
                }
            }
        }
        ////////
        
        if objAppShareData.isFromNotification && self.isFirstTimeScrollToBottom{
            self.isFirstTimeScrollToBottom = false
            self.tableViewScrollToBottom(animated: false)
        }
        if isImagePicked == false {
            objWebserviceManager.StopIndicator()
        }
        self.refreshControl.endRefreshing()
        isPullToRefresh = false
        isLoading = false
        
        ////
        if objAppShareData.isFromDidFinish{
            if self.arrMessages.count>0{
                let obj = self.arrMessages[self.arrMessages.count-1]
                if obj.strMessage == "https://local"{
                    let ref = Database.database().reference()
                    ref.child("groupChat").child(objChatHistoryModel.strOpponentId).child(obj.str_Key).removeValue()
                }
            }
        }
        objAppShareData.isFromDidFinish = false
        ////
    }
    
    //send message
    func sendMessageNew(){
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        //self.txtViewChat.isScrollEnabled = false
        isChatAppearFirst = true
        isSendButtonClicked = false
        
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        //self.txtViewChat.isScrollEnabled = false
        
        if self.txtViewChat.text == "" {
            ////print("Please enter some text")
            return
        }else{
            message = self.txtViewChat.text
            self.writeDataOnFirebaseForChatNew()
        }
        self.viewZoomImg.isHidden = true
        self.imgZoomImage.image = nil
        self.txtViewHeight.constant = self.txtViewMsgMinHeight
        self.txtViewChat.text = ""
    }
}

//MARK: - Compess image resize
extension GroupChatVC{
    
    func compressImage(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 1600.0
        let maxWidth : CGFloat = 1000.0
        let minHeight : CGFloat = 150.0
        let minWidth : CGFloat = 150.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        let minRatio : CGFloat = minWidth/minHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        if (actualHeight < minHeight || actualWidth < minWidth){
            if(imgRatio > minRatio){
                //adjust width according to maxHeight
                imgRatio = minHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = minHeight
            }
            else if(imgRatio < minRatio){
                //adjust height according to maxWidth
                imgRatio = minWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = minWidth
            }
            else{
                actualHeight = minHeight
                actualWidth = minWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }
}

//MARK: - Notification method
extension GroupChatVC{
    
    func ChatNotification(firebaseToken:String){
        
        if firebaseToken == "" {
            
            let webNotification = [ "title" : self.strName,
                                    "body" : self.strTextNotification,
                                    "url":"/chat?uId="+self.strMyChatId ]
            self.ref.child("webnotification").child(self.objChatHistoryModel.strOpponentId).childByAutoId().setValue(webNotification)
            
        } else {
            
            let messageDict = ["body": checkForNULL(obj: self.strName + " : " + self.strTextNotification),
                               "title": checkForNULL(obj: self.objChatHistoryModel.strOpponentName),
                               "icon": "icon",
                               "sound": "default",
                               "badge": "1",
                               "message": self.strTextNotification,
                               "type": "groupChat",
                               "notifincationType": "15",
                               "click_action": "ChatActivity",
                               "opponentChatId": checkForNULL(obj: self.objChatHistoryModel.strOpponentId)]
            
            //IOS
            let notificationDict = ["body": checkForNULL(obj: self.strName + " : " + self.strTextNotification),
                                    "title": checkForNULL(obj: self.objChatHistoryModel.strOpponentName ),
                                    "icon": "icon",
                                    "sound": "default",
                                    "badge": "1",
                                    "message": self.strTextNotification,
                                    "notifincationType": "15",
                                    "type": "groupChat",
                                    "click_action": "ChatActivity",
                                    "opponentChatId": checkForNULL(obj: self.objChatHistoryModel.strOpponentId)]
            
            let finalDict = ["to":"/topics/"+self.objChatHistoryModel.strOpponentId,
                             "data": checkForNULL(obj:messageDict),
                             "notification": checkForNULL(obj:notificationDict),
                             "sound": "default"] as [String : Any]
            
            self.sendNotificationWithDict(dictNotification:finalDict)
        }
    }
    
    func sendNotificationWithDict(dictNotification:Dictionary<String, Any>){
        
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StartIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        let strUrl = "https://fcm.googleapis.com/fcm/send"
        var request = URLRequest.init(url: URL.init(string: strUrl)!)
        request.setValue("key=" + objChatShareData.kServerKey, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dictNotification, options: .prettyPrinted)
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                ////print("error calling POST on /todos/1")
                ////print(error!)
                return
            }
            guard data != nil else {
                ////print("Error: did not receive data")
                return
            }
            
            }.resume()
    }
}

// MARK: - Textview delegate methods
extension GroupChatVC:UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //self.createTypingStatus()
        self.viewMenu.isHidden = true
        if self.txtViewChat.text == "\n"{
            self.txtViewChat.resignFirstResponder()
        }else{
        }
        return true
    }
    
    func createTypingStatus(){
        let dict = ["isTyping":1,
                    "reciverId":self.objChatHistoryModel.strOpponentId,
                    "senderId":self.strMyChatId] as [String : Any]
        self.ref.child("isTyping").child(self.objChatHistoryModel.strOpponentId).setValue(dict)
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        self.viewMenu.isHidden = true
        
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize.init(width: fixedWidth, height:CGFloat(MAXFLOAT)))
        
        if newSize.height <= self.txtViewMsgMaxHeight && newSize.height >= self.txtViewMsgMinHeight {
            self.txtViewHeight.constant = newSize.height;
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let indexPath = IndexPath(row: self.arrMessages.count - 1, section: 0)
        if self.arrMessages.count > 0{
            if isTableScrollToTop == false {
                self.tableChat.scrollToRow(at: indexPath, at: .top, animated: true)
            } else {
                //self.tableChat.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        removeTypingStatus()
    }
    
    func removeTypingStatus(){
        self.ref.child("isTyping").child(self.objChatHistoryModel.strOpponentId).setValue(nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //self.createTypingStatus()
        
        let text = self.txtViewChat.text! as NSString
        var substring: String = txtViewChat.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: text as String)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 3)
        }
    }
    
    @objc func reload() {
        self.removeTypingStatus()
    }
}

//MARK:- Tableview delegate methods
extension GroupChatVC:UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMessages.count
    }
    func setTextOnPastTimeLabel(indexPath:IndexPath){
        let objChat = arrMessages[indexPath.row]
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let arr = objChat.str_timestamp.components(separatedBy: " ")
        let date = formatter.date(from: arr[0])
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDayDate = formatter.string(from: yesterday!)
        
        if resultDate == strDate {
            self.lblMsgDay.text = "Today"
        }else if yesterDayDate == strDate{
            self.lblMsgDay.text = "Yesterday"
        }else{
            self.lblMsgDay.text = strDate
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let objChat = arrMessages[indexPath.row]
        let MyChatId = self.strMyChatId//UserDefaults.standard.string(forKey: UserDefaults.Keys.UserId)
        
        self.setTextOnPastTimeLabel(indexPath: indexPath)
        
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "YYYY-MM-dd"
        let arr = objChat.str_timestamp.components(separatedBy: " ")
        let date = formatter.date(from: arr[0])
        formatter.dateFormat = "dd-MMMM-yyyy"
        let strDate = formatter.string(from: date!)
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDayDate = formatter.string(from: yesterday!)
        
        let arrTime = objChat.str_timestamp.components(separatedBy: " ")
        let msgtimeOnly = arrTime[1]
        let msgAM_PM = arrTime[2]
        let msgtime = msgtimeOnly+" "+msgAM_PM
        
        let strSenderChatId = objChat.str_UId
        if strSenderChatId == MyChatId {
            
            if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") || objChat.strMessage.hasPrefix("content://media/external") || objChat.strMessage.hasPrefix("content://com.google.android"){
                
                let cellIdentifier = "MyImageCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyImageCell
                cell.viewBlur.isHidden = true
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate)
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if indexPath.row == 0 {
                    cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    ////////////
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    ////////////
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                
                if objChat.strReadStatus == "0"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
                }else if objChat.strReadStatus == "1"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
                }else if objChat.strReadStatus == "2"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
                }
                
                
                cell.btnZoomImage.tag = indexPath.row
                cell.btnZoomImage.addTarget(self, action:#selector(btnZoomImage(sender:)) , for: .touchUpInside)
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    //objWebserviceManager.StopIndicator()
                }
                cell.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                self.viewMsgDay.isHidden = true
                
                let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
                lpgr.minimumPressDuration = 0.5
                lpgr.delaysTouchesBegan = true
                lpgr.delegate = self
                cell.addGestureRecognizer(lpgr)
                
                return cell
            }else{
                let cellIdentifier = "MyTextCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyTextCell
                cell.viewBlur.isHidden = true
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate)
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if indexPath.row == 0 {
                    //cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    ////////////
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    ////////////
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                
                if objChat.strReadStatus == "0"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
                }else if objChat.strReadStatus == "1"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
                }else if objChat.strReadStatus == "2"{
                    cell.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    // objWebserviceManager.StopIndicator()
                }
                cell.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                self.viewMsgDay.isHidden = true
                
                let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
                lpgr.minimumPressDuration = 0.5
                lpgr.delaysTouchesBegan = true
                lpgr.delegate = self
                cell.addGestureRecognizer(lpgr)
                
                return cell
            }
        }else{
            
            //// Remove HangUp
            //            DispatchQueue.main.async {
            //        self.ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(objChat.str_Key).child("readMember").updateChildValues([self.strMyChatId: self.strMyChatId])
            //            }
            ////
            
            if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") || objChat.strMessage.hasPrefix("content://media/external") || objChat.strMessage.hasPrefix("content://com.google.android"){
                
                let cellIdentifier = "OpponentImageCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OpponentImageCell
                
                //  DispatchQueue.main.async {
                
                cell.cellDataParsing(objChat:objChat, strDate: strDate, msgtime: msgtime, resultDate: self.resultDate,yesterDayDate: yesterDayDate,opponentData:self.objChatHistoryModel)
                
                //   }
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                cell.btnZoomImage.tag = indexPath.row
                cell.btnZoomImage.addTarget(self, action:#selector(btnZoomImage(sender:)) , for: .touchUpInside)
                
                if indexPath.row == 0 {
                    //cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    ////////////
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    ////////////
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    //objWebserviceManager.StopIndicator()
                }
                cell.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                self.viewMsgDay.isHidden = true
                return cell
            }
            else{
                let cellIdentifier = "OpponentTextCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OpponentTextCell
                cell.lblOppMessage.text = objChat.strMessage
                cell.lblOppName.text = objChat.strOppName
                
                
                
                if self.arrIndexToHighlight.contains(indexPath.row){
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                }else{
                    cell.viewMainBack.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
                if resultDate == strDate {
                    cell.lblMsgDay.text = "Today"
                    cell.lblOppMsgTime.text = "Today " + msgtime
                }else if yesterDayDate == strDate{
                    cell.lblMsgDay.text = "Yesterday"
                    cell.lblOppMsgTime.text = "Yesterday " + msgtime
                }else{
                    cell.lblMsgDay.text = strDate
                    cell.lblOppMsgTime.text = strDate + " " + msgtime
                }
                
                if indexPath.row == 0 {
                    //cell.heightDayDate.constant = 18
                } else {
                    let objChatDay = arrMessages[indexPath.row - 1]
                    
                    ////////////
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                    formatter.dateFormat = "YYYY-MM-dd"
                    let arr = objChatDay.str_timestamp.components(separatedBy: " ")
                    let date = formatter.date(from: arr[0])
                    formatter.dateFormat = "dd-MMMM-yyyy"
                    let strDateDay = formatter.string(from: date!)
                    ////////////
                    
                    if !(strDateDay == strDate) {
                        cell.heightDayDate.constant = 18
                    } else {
                        cell.heightDayDate.constant = 0
                    }
                }
                if isImagePicked == false && (indexPath.row == self.arrMessages.count-1){
                    // objWebserviceManager.StopIndicator()
                }
                cell.lblMsgDay.isHidden = true
                //cell.viewMsgDay.isHidden = true
                self.lblMsgDay.isHidden = true
                self.viewMsgDay.isHidden = true
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewMenu.isHidden = true
        if self.txtViewChat.text.count > 0{
        }else{
        }
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
    }
}

//MARK: tableView Button extension
extension GroupChatVC{
    
    @objc func btnZoomImage(sender: UIButton!) {
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
        let objChat = arrMessages[sender.tag]
        if objChat.strMessage.hasPrefix("gs://") || objChat.strMessage.hasPrefix("http://") || objChat.strMessage.hasPrefix("https://") {
            let url = URL(string: (objChat.strMessage))
            //self.imgZoomImage.af_setImage(withURL: url!)
            self.viewZoomImg.isHidden = false
            if objChat.strMessage == "https://local" || objChat.strMessage.contains(".gif") {
                self.imgZoomImage.image = objChat.imageLocal
                return
            }
            let processor = DownsamplingImageProcessor(size: self.imgZoomImage.frame.size)
                >> RoundCornerImageProcessor(cornerRadius: 0)
            self.imgZoomImage.kf.indicatorType = .activity
            self.imgZoomImage.kf.setImage(
                with: url,
                placeholder: UIImage(named: "gallery_placeholder"),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }else{
            self.viewZoomImg.isHidden = true
        }
    }
}
//MARK: - deleteSingleMessage firebase chat extension
extension GroupChatVC{
    func deleteSingleMessage()  {
        // self.isFromDeleteSingleMsg = true
        guard self.arrIndexToHighlight.count > 0 else { return }
        
        for index in self.arrIndexToHighlight{
            
            let obj = arrMessages[index]
            let strKey = obj.str_Key
            
            if !self.isOnChatScreen{
                return
            }
            self.ref.child("groupChat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).child(strKey).setValue(nil)
            
            self.arrMessages.remove(at: index)
            
            if index == self.arrMessages.count && self.arrMessages.count > 0{
                
                let calendarDate = ServerValue.timestamp()
                let obj_1 = self.arrMessages[index-1]
                
                let dict = [
                    "message":checkForNULL(obj:obj_1.str_message),
                    "messageType":checkForNULL(obj: self.imgType),
                    "profilePic":checkForNULL(obj:self.objChatHistoryModel.strOpponentProfileImage),
                    "reciverId": checkForNULL(obj:self.strMyChatId),
                    "senderId": checkForNULL(obj:self.objChatHistoryModel.strOpponentId),
                    "timestamp":calendarDate,
                    "unreadMessage":checkForNULL(obj:0),
                    "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName ) ]
                self.ref.child("chat_history").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(dict)
            }else if self.arrMessages.count == 0{
                self.ref.child("chat_history").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(nil)
            }else{
                
            }
            
        }
        self.arrIndexToHighlight.removeAll()
        //self.indexToHighlight = -1
        self.tableChat.reloadData()
    }
}

//MARK: - scrollView manage t extension
extension GroupChatVC{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.arrMessages.count > 0{
            //self.viewMsgDay.isHidden = false
            self.viewMsgDay.isHidden = true
            //            let arrNNN = self.tableChat.indexPathsForVisibleRows
            //            let index = arrNNN?.last
            //            self.setTextOnPastTimeLabel(indexPath: index!)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        objAppShareData.setView(view: self.viewMsgDay, hidden: true)
        /*//// Himanshu
        if (self.tableChat.indexPathsForVisibleRows)!.count>0{
            let indexPath = self.tableChat.indexPathsForVisibleRows![0]
            if indexPath.row<=25 && !self.isLoading{
                if self.arrMessages.count < 50 {
                    self.isLastMsg = true
                    self.isLoading = true
                    return
                }
                let obj = self.arrMessages[0]
                if self.strPaginationKey != obj.str_Key{
                    self.isLoading = true
                    self.isPullToRefresh = true
                    self.observeMessages(lastMessageKey: obj.str_Key)
                }else{
                    self.isLastMsg = true
                    self.refreshControl.endRefreshing()
                }
            }
        }
        ////
        */
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //self.view.endEditing(true)
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            // moved to bottom
            isTableScrollToTop = false
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            // moved to top
            isTableScrollToTop = true
        } else {
            // didn't move
        }
//        if self.arrMessages.count > 0{
//            let arrNNN = self.tableChat.indexPathsForVisibleRows
//            if arrNNN?.count ?? 0 > 0{
//                let index = arrNNN?[0]
//                self.setTextOnPastTimeLabel(indexPath: index!)
//            }
//        }
    }
}

//MARK: - send message on firebase chat extension
extension GroupChatVC{
    
    func timeInMiliSeconds() -> String {
        let date = Date()
        let timeInMS = "\(Int(date.timeIntervalSince1970 * 1000))"
        return timeInMS
    }
    
    func writeDataOnFirebaseForChatNew(){
        
        txtViewChat.contentSize.height = 42
        let calendarDate = ServerValue.timestamp()//timeInMiliSeconds()
        
        let dict = ["memberCount":self.objChatHistoryModel.strMemberCount,
                    "message": checkForNULL(obj:message),
                    "messageType":checkForNULL(obj:0),
                    "reciverId":checkForNULL(obj:self.objChatHistoryModel.strOpponentId ),
                    "readMember":[self.strMyChatId: self.strMyChatId],
                    "readStatus":0,
                    "senderId":checkForNULL(obj:self.strMyChatId),
                    "timestamp":calendarDate,
                    "userName":self.strName]
        
        self.isChatAppearFirst = true
        
        let ref = Database.database().reference()
        //let keyValue = ref.child("chat").child(strMyChatId).child(self.objChatHistoryModel.strOpponentId).childByAutoId().key
        ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).childByAutoId().setValue(dict)
        { (error, snap) in
            if error == nil{
                //self.imgType = 0
                //self.strTextNotification = self.message
                //self.UpdateHistory()
            }
        }
        self.imgType = 0
        self.strTextNotification = self.message
        self.UpdateHistory()
    }
    
    func UpdateHistory(){
        
        let calendarDate = ServerValue.timestamp()
        var unreadCount = 0
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            ////print("snapshot = \(snapshot)")
            
            guard let strongSelf = self else { return }
            if snapshot.exists(){
                
                var arrTemp : [[String : Any]] = []
                if let arr = snapshot.value as? [Any]{
                    
                    for newObj in arr {
                        if let dict = newObj as? [String:Any] {
                            ////print("dict = \(dict)")
                            strongSelf.historyManage(dict: dict)
                            arrTemp.append(dict)
                        }
                    }
                } else if let arr = snapshot.value as? [String:Any]{
                    for newObj in arr {
                        if let dict = newObj.value as? [String:Any] {
                            strongSelf.historyManage(dict: dict)
                            arrTemp.append(dict)
                        }}}
                
                if arrTemp.count > 0 {
                    strongSelf.createDataSetForNotification(arr: arrTemp)
                    self?.badgeCount(arrTemp:arrTemp)
                }
            }
        })
    }
    
    func createDataSetForNotification(arr : [[String : Any]]){
        
        var arrFirebaseToken : [String] = []
        
        for dict in arr {
            
            var muteId = ""
            if let count = dict["mute"] as? String {
                muteId = count
            }else if let count1 = dict["mute"] as? Int {
                muteId = String(count1)
            }
            var firebaseToken = dict["firebaseToken"]  as? String ?? ""
            var id = ""
            if let userId = dict["memberId"]  as? String{
                id = userId
            }else if let userId = dict["memberId"]  as? Int{
                id = String(userId)
            }
            if muteId == "0"{
                self.ref.child("users").child(id).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
                    guard let strongSelf = self else { return }
                    if snapshot.exists(){
                        let dictNew = snapshot.value as? [String:Any]
                        firebaseToken = dictNew!["firebaseToken"] as? String ?? ""
                        if firebaseToken == "" {
                            
                            let webNotification = [ "body": checkForNULL(obj: self!.strTextNotification),
                                                    "title": checkForNULL(obj: self!.strName + " @ " + self!.objChatHistoryModel.strOpponentName),
                                                    "url":"/chat?uId="+self!.strMyChatId ]
                            self!.ref.child("webnotification").child(id).childByAutoId().setValue(webNotification)
                            
                        } else {
                            if firebaseToken != objAppShareData.firebaseToken{
                                arrFirebaseToken.append(firebaseToken)
                                self?.getBadgeCount(strAuth: firebaseToken,id:id)
                            }
                        }
                    }
                })
            }
        }
    }
    
    func getBadgeCount(strAuth:String,id:String){
        
        var totalCounsts = "0"
         var chatCounts = "0"
        self.ref.child("socialBookingBadgeCount").child(id).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            
            if !(self?.isImagePicked)! {
                objWebserviceManager.StopIndicator()
            }
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    
                    if let chatCount1 = dict["chatCount"] as? String{
                        chatCounts = chatCount1
                    }else if let chatCount1 = dict["chatCount"] as? Int{
                        chatCounts = String(chatCount1)
                    }
                   
                    if let totalCount1 = dict["totalCount"] as? String{
                        totalCounsts = totalCount1
                    }else if let totalCount1 = dict["totalCount"] as? Int{
                        totalCounsts = String(totalCount1)
                    }
                }
                totalCounsts = String((Int(totalCounsts ) ?? 0)+1)
                self?.sendNotificationBadgeCount(strAuth: strAuth, badgeCount: totalCounsts)

            }else{
                totalCounsts = String((Int(totalCounsts ) ?? 0)+1)
                self?.sendNotificationBadgeCount(strAuth: strAuth, badgeCount: totalCounsts)
            }
        })
    }
    
func sendNotificationBadgeCount(strAuth:String,badgeCount:String){

        if self.arrNewFirebaseToken.count > 0 {
            
            let messageDict = ["body": checkForNULL(obj: self.strTextNotification),
                               "title": checkForNULL(obj: self.strName + " @ " + self.objChatHistoryModel.strOpponentName),
                               "icon": "icon",
                               "sound": "default",
                               "badge":badgeCount,
                               "adminId": self.objChatHistoryModel.strAdminId,
                               "message": self.strTextNotification,
                               "type": "groupChat",
                               "notifincationType": "15",
                               "click_action": "ChatActivity",
                               "opponentChatId": checkForNULL(obj: self.objChatHistoryModel.strOpponentId)]
            
            //IOS
            let notificationDict = ["body": checkForNULL(obj: self.strTextNotification),
                                    "title": checkForNULL(obj: self.strName + " @ " + self.objChatHistoryModel.strOpponentName),
                                    "icon": "icon",
                                    "sound": "default",
                                    "badge":badgeCount,
                                    "adminId": self.objChatHistoryModel.strAdminId,
                                    "message": self.strTextNotification,
                                    "notifincationType": "15",
                                    "type": "groupChat",
                                    "click_action": "ChatActivity",
                                    "opponentChatId": checkForNULL(obj: self.objChatHistoryModel.strOpponentId)]
            
            let finalDict = ["to": strAuth,
                             "data": checkForNULL(obj:messageDict),
                             "notification": checkForNULL(obj:notificationDict),
                             "sound": "default"] as [String : Any]
            print(finalDict)
            self.sendNotificationWithDict(dictNotification:finalDict)
        }
    }
    
    func historyManage(dict:[String:Any]){
        
        var unreadCount = 0
        
        let calendarDate = ServerValue.timestamp()
        
        var memberId = ""
        var muteId = ""
        
        if let count = dict["memberId"] as? String {
            memberId = count
        }else if let count1 = dict["memberId"] as? Int {
            memberId = String(count1)
        }
        if let count = dict["mute"] as? String {
            muteId = count
        }else if let count1 = dict["mute"] as? Int {
            muteId = String(count1)
        }
        var firebaseToken = dict["firebaseToken"]  as? String ?? ""
        var memberCount = dict["memberCount"]  as? Int ?? 0
        var memberType = dict["memberType"]  as? String ?? ""
        var favouriteStatus = 0
        self.ref.child("chat_history").child(memberId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            ////print("snapshot = \(snapshot)")
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    if let count = dict["unreadMessage"] as? String {
                        unreadCount = Int(count)!
                    }else if let count1 = dict["unreadMessage"] as? Int {
                        unreadCount = count1
                    }
                    if let favourite = dict["favourite"] as? String {
                        favouriteStatus = Int(favourite)!
                    }else if let favourite = dict["favourite"] as? Int {
                        favouriteStatus = favourite
                    }
                    
                    firebaseToken = dict["firebaseToken"]  as? String ?? ""
                    memberType = dict["memberType"]  as? String ?? ""
                    if let memberCounts = dict["memberCount"]  as? String{
                        memberCount = Int(memberCounts)!
                    }else if let memberCounts = dict["memberCount"]  as? Int{
                        memberCount = memberCounts
                    }
                    
                    if let favourites = dict["favourite"]  as? String{
                        favouriteStatus = Int(favourites)!
                    }else if let favourites = dict["favourite"]  as? Int{
                        favouriteStatus = favourites
                    }
                }
            }
            
            unreadCount = unreadCount + 1
            //For My Side
            let dict1 = [
                "favourite":favouriteStatus,
                "memberCount":self.objChatHistoryModel.strMemberCount,
                "memberType":memberType,
                "message": checkForNULL(obj:self.message),
                "messageType":checkForNULL(obj:self.imgType),
                "profilePic":checkForNULL(obj:self.objChatHistoryModel.strOpponentProfileImage),
                "reciverId":checkForNULL(obj: self.objChatHistoryModel.strOpponentId),
                "senderId":checkForNULL(obj:self.strMyChatId ),
                "timestamp":calendarDate,
                "type":checkForNULL(obj:"group"),
                "unreadMessage":checkForNULL(obj: unreadCount),
                "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName )//opponent name
            ]
            let ref = Database.database().reference()
            ref.child("chat_history").child(memberId).child(self.objChatHistoryModel.strOpponentId).setValue(dict1)
            
            var count = 0
            self.ref.child("chatBadgeCount").child(memberId).observeSingleEvent(of: .value) { (snap) in
                if !snap.exists(){
                    count = 1
                }else{
                    print(snap.value)
                    let dict = snap.value as! [String:Any]
                    count = dict["count"] as! Int + 1
                }
                
                let newDict = ["count" : count]
                self.ref.child("chatBadgeCount").child(memberId).setValue(newDict)
            }
            
            self.isImagePicked = false
            if !self.isImagePicked{
                objWebserviceManager.StopIndicator()
            }
            if muteId == "0"{
            }
        })
    }
    
    func UpdateHistoryForDelete(index:Int){
        
        let calendarDate = ServerValue.timestamp()
        var unreadCount = 0
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            ////print("snapshot = \(snapshot)")
            
            guard let strongSelf = self else { return }
            if snapshot.exists(){
                
                var arrTemp : [[String : Any]] = []
                if let arr = snapshot.value as? [Any]{
                    
                    for newObj in arr {
                        if let dict = newObj as? [String:Any] {
                            ////print("dict = \(dict)")
                            strongSelf.historyManageForDelete(dict: dict,index:index)
                            arrTemp.append(dict)
                        }
                    }
                } else if let arr = snapshot.value as? [String:Any]{
                    for newObj in arr {
                        if let dict = newObj.value as? [String:Any] {
                            strongSelf.historyManageForDelete(dict: dict,index:index)
                            arrTemp.append(dict)
                        }}}
            }
        })
    }
    
    
    func historyManageForDelete(dict:[String:Any],index:Int){
        
        var unreadCount = 0
        
        let calendarDate = ServerValue.timestamp()
        
        var memberId = ""
        var muteId = ""
        
        if let count = dict["memberId"] as? String {
            memberId = count
        }else if let count1 = dict["memberId"] as? Int {
            memberId = String(count1)
        }
        if let count = dict["mute"] as? String {
            muteId = count
        }else if let count1 = dict["mute"] as? Int {
            muteId = String(count1)
        }
        var firebaseToken = dict["firebaseToken"]  as? String ?? ""
        var memberCount = dict["memberCount"]  as? Int ?? 0
        var memberType = dict["memberType"]  as? String ?? ""
        var favouriteStatus = 0
        self.ref.child("chat_history").child(memberId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            ////print("snapshot = \(snapshot)")
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    if let count = dict["unreadMessage"] as? String {
                        unreadCount = Int(count)!
                    }else if let count1 = dict["unreadMessage"] as? Int {
                        unreadCount = count1
                    }
                    if let favourite = dict["favourite"] as? String {
                        favouriteStatus = Int(favourite)!
                    }else if let favourite = dict["favourite"] as? Int {
                        favouriteStatus = favourite
                    }
                    
                    firebaseToken = dict["firebaseToken"]  as? String ?? ""
                    memberType = dict["memberType"]  as? String ?? ""
                    if let memberCounts = dict["memberCount"]  as? String{
                        memberCount = Int(memberCounts)!
                    }else if let memberCounts = dict["memberCount"]  as? Int{
                        memberCount = memberCounts
                    }
                    
                    if let favourites = dict["favourite"]  as? String{
                        favouriteStatus = Int(favourites)!
                    }else if let favourites = dict["favourite"]  as? Int{
                        favouriteStatus = favourites
                    }
                }
            }
            var obj=self.objHoldIndex
            if self.arrMessages.count>0{
                 obj = self.arrMessages[index]
            }
            var strMessage = ""
            if index == self.indexOfArray{
                strMessage = ""
            }else{
                strMessage = obj?.strMessage ?? ""
            }
            //For My Side
            let dict1 = [
                "favourite":favouriteStatus,
                "memberCount":self.objChatHistoryModel.strMemberCount,
                "memberType":memberType,
                "message": checkForNULL(obj:strMessage),
                "messageType":checkForNULL(obj:obj?.str_isImage),
                "profilePic":checkForNULL(obj:obj?.str_ProfileImage),
                "reciverId":checkForNULL(obj: self.objChatHistoryModel.strOpponentId),
                "senderId":checkForNULL(obj:obj?.str_lastSenderID),
                "timestamp":obj?.TimeStamp,
                "type":checkForNULL(obj:"group"),
                "unreadMessage":checkForNULL(obj: unreadCount),
                "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName )//opponent name
            ]
            let ref = Database.database().reference()
            ref.child("chat_history").child(memberId).child(self.objChatHistoryModel.strOpponentId).setValue(dict1)
        })
    }
}

//MARK:- Clear UnreadChatHistory method
extension GroupChatVC {
    
    func memberEntryTime(){
        self.ref.child("group").child(objChatHistoryModel.strOpponentId).child("member").child(self.strMyChatId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    self?.objChatHistoryModel.strCreatTime = dict["createdDate"] as? Double ?? 0.0
                }
            }else{
                self?.objChatHistoryModel.strCreatTime = 0.0
            }
        })
    }
    
    
    func deleteMsgHistory(){
        //        if self.strMyChatId != self.objChatHistoryModel.strSenderId {
        //            self.objChatHistoryModel.strOpponentId = self.objChatHistoryModel.strSenderId
        //        }else if self.strMyChatId == self.objChatHistoryModel.strSenderId{            self.objChatHistoryModel.strOpponentId = self.objChatHistoryModel.strReceiverId
        //        }
        self.ref.child("group_msg_delete").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            
            //            if !(self?.isOnChatScreen)!{
            //                return
            //            }
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    self?.objChatHistoryModel.strDeleteTime = dict["deleteBy"] as? Double ?? 0.0
                }
            }else{
                self?.objChatHistoryModel.strDeleteTime = 0.0
            }
            self?.observeMessages(lastMessageKey: nil)
        })
    }
}

//MARK:- Clear UnreadChatHistory method
extension GroupChatVC {
    func clearUnreadChatHistory(){
        //////////////
        self._refHandle = self.ref.child("chat_history").child(self.strMyChatId).child(objChatHistoryModel.strOpponentId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            
            if snapshot.exists(){
                
                if let dict = snapshot.value as? [String:Any]{
                    var favourite = dict["favourite"] as? String ?? ""
                    if let ab = dict["favourite"] as? String{
                        favourite = ab
                    }else if let ab = dict["favourite"] as? Int {
                        favourite = String(ab)
                    }
                    
                    self?.objChatHistoryModel.strMemberType = dict["memberType"] as? String ?? ""
                    if favourite == "1"{
                        self?.objChatHistoryModel.strFavouriteStatus = "1"
                        self?.lblFavouriteStatus.text = "Unfavourite"
                    }else{
                        self?.objChatHistoryModel.strFavouriteStatus = "0"
                        self?.lblFavouriteStatus.text = "Add To Favourite"
                    }
                }
                
                if !objChatShareData.isOnChatScreen{
                    return
                }
                //if !(self?.isOnChatScreen)!{
                //    return
                //}
                
                strongSelf.ref = Database.database().reference()
                strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).updateChildValues(["unreadMessage": 0])
                
                if let refHandle = strongSelf._refHandle{
                    strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).removeObserver(withHandle: refHandle)
                }
            }
            else{
                self?.lblFavouriteStatus.text = "Add To Favourite"
                self?.objChatHistoryModel.strFavouriteStatus = ""
            }
        })
        //////////////
    }
}

//MARK: - Button methods extension
extension GroupChatVC{
    @IBAction func btnReportGroupAction(_ sender: UIButton) {
        self.viewMenu.isHidden = true
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
        detailVC.objModel = self.objChatHistoryModel
        detailVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(detailVC, animated: true, completion: nil)
    }
    @IBAction func btnGroupAdmin(_ sender: UIButton) {
    }
    @IBAction func btnAddMemberAction(_ sender: UIButton) {
        self.viewMenu.isHidden = true
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        if let detailVC = sb.instantiateViewController(withIdentifier: "AddGroupMemberVC") as? AddGroupMemberVC{
            detailVC.objChatHistoryModel = self.objChatHistoryModel
            detailVC.modalPresentationStyle = .fullScreen
            self.present(detailVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnRemoveMemberddAction(_ sender: UIButton) {
        self.viewMenu.isHidden = true
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "RemoveGroupMemberVC") as! RemoveGroupMemberVC
        detailVC.objChatHistoryModel = self.objChatHistoryModel
        detailVC.modalPresentationStyle = .fullScreen
        self.present(detailVC, animated: true, completion: nil)
    }
    @IBAction func btnAllMember(_ sender: UIButton) {
        self.viewMenu.isHidden = true
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "GroupMemberListVC") as! GroupMemberListVC
        detailVC.objChatHistoryModel = self.objChatHistoryModel
        detailVC.modalPresentationStyle = .fullScreen
        self.present(detailVC, animated: true, completion: nil)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if objAppShareData.isFromNotification {
            objAppShareData.clearNotificationData()
        }
        objChatShareData.isOnChatScreen =  false
        self.viewMenu.isHidden = true
        isOnChatScreen = false
        self.view.endEditing(true)
        self.ref.child("chat_history").child(self.strMyChatId).child(objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.ref = Database.database().reference()
            if snapshot.exists(){
                strongSelf.ref.child("chat_history").child((self?.strMyChatId)!).child((self?.objChatHistoryModel.strOpponentId)!).updateChildValues(["unreadMessage": 0])
            } })
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDetailHeaderAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewMenu.isHidden = true
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "GroupMemberListVC") as! GroupMemberListVC
        detailVC.objChatHistoryModel = self.objChatHistoryModel
        detailVC.modalPresentationStyle = .fullScreen
        self.present(detailVC, animated: true, completion: nil)
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.viewMenu.isHidden == false{
            self.viewMenu.isHidden = true
        }else{
            self.adminStatus()
            self.viewMenu.isHidden = false
        }
    }
    
    @IBAction func btnDeleteChatCancelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFor_Block_Delete.isHidden = true
    }
    
    @IBAction func btnDeleteChatYesAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.viewFor_Block_Delete.isHidden = true
        self.viewMenu.isHidden = true
        
        let currentTime = ServerValue.timestamp()
        if self.arrMessages.count > 0{
            self.arrMessages.removeAll()
            self.tableChat.reloadData()
            let dict = ["deleteBy":currentTime]
            self.ref.child("group_msg_delete").child(strMyChatId).child(objChatHistoryModel.strOpponentId).updateChildValues(dict)
            self.deleteMsgHistory()
        }
        
    }
    @IBAction func btnAddToFavouriteAction(_ sender: UIButton) {
        self.manageFavouriteStatus()
    }
    
    @IBAction func btnDeleteChatAction(_ sender: UIButton) {
        self.viewFor_Block_Delete.isHidden = false
        self.viewForDelete.isHidden = false
        self.view.endEditing(true)
    }
    
    @IBAction func btnGalaryImageUpload(_ sender: UIButton) {
        self.GallaryCameraAction(imageFrom: "Gallary")
    }
    
    @IBAction func btnImageUpload(_ sender: UIButton) {
        self.GallaryCameraAction(imageFrom: "Camera")
    }
    
    
    @IBAction func btnCancelViewZoom(_ sender: UIButton) {
        self.view.endEditing(true)
        self.scrollViewImg.zoomScale = 1.0
        self.viewZoomImg.isHidden = true
    }
    
    @IBAction func btnSendMessage(_ sender: UIButton) {
        self.txtViewChat.text = self.txtViewChat.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: "No Internet Connection ", on: self)
            return
        }else{
            if self.txtViewChat.text.count == 0 {
                self.txtViewHeight.constant = self.txtViewMsgMinHeight
                return;
            }else{
                isSendButtonClicked = true
                //self.txtViewChat.frame.size.height = self.txtViewMsgMinHeight
                self.txtViewHeight.constant = self.txtViewMsgMinHeight
                //self.view.endEditing(true)
                //DispatchQueue.main.async {
                self.sendMessageNew()
                //}
                //self.lblPlaceholder.isHidden = true
            }
        }
    }
}


//MARK: manage camera galary action extension
extension GroupChatVC{
    
    func GallaryCameraAction(imageFrom:String){
        self.viewMenu.isHidden = true
        if self.txtViewChat.text.count > 0{
        }else{
        }
        self.view.endEditing(true)
        if !NetworkReachabilityManager()!.isReachable{
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: "No Internet Connection ", on: self)
            return
        }
        
        self.view.endEditing(true)
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {   UIAlertAction in self.openCamera() }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {   UIAlertAction in self.openGallary() }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {   UIAlertAction in }
        
        if imageFrom == "Gallary"{
            //alert.addAction(gallaryAction)
            //alert.addAction(cancelAction)
            self.openGallary()
        } else if imageFrom == "Camera"{
            self.openCamera()
            //alert.addAction(cameraAction)
            //alert.addAction(cancelAction)
        }
        //self.present(alert, animated: true, completion: nil)
    }
}

//MARK: manage favouriteStatus extension
extension GroupChatVC{
    
    func manageFavouriteStatus(){
        
        let myId = self.strMyChatId
        let oppId = self.objChatHistoryModel.strOpponentId
        //objWebserviceManager.StartIndicator()
        self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            
            if !(self?.isImagePicked)!{
                objWebserviceManager.StopIndicator()
            }
            
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    var favourites = "0"
                    if let favourite = dict["favourite"] as? String {
                        favourites = favourite
                    }else if let favourite = dict["favourite"] as? Int {
                        favourites = String(favourite)
                    }
                    if favourites == "1"{
                        self?.objChatHistoryModel.strFavouriteStatus = "1"
                    }else{
                        self?.objChatHistoryModel.strFavouriteStatus = "0"
                    }
                }
                
                if self?.objChatHistoryModel.strFavouriteStatus == "1"{
                    self?.lblFavouriteStatus.text = "Add To Favourite"
                    self?.ref.child("chat_history").child(myId).child(oppId).updateChildValues(["favourite": 0])
                }else if self?.objChatHistoryModel.strFavouriteStatus == "0"{
                    self?.lblFavouriteStatus.text = "Unfavourite"
                    self?.ref.child("chat_history").child(myId).child(oppId).updateChildValues(["favourite": 1])
                }
                
            }else{
                let calendarDate = ServerValue.timestamp()
                self?.lblFavouriteStatus.text = "Unfavourite"
                
                let dict1 = [
                    "memberCount":0,
                    "message": checkForNULL(obj:""),
                    "messageType":checkForNULL(obj:0),
                    "profilePic":checkForNULL(obj:self?.objChatHistoryModel.strOpponentProfileImage),//opponent image
                    "reciverId":checkForNULL(obj: self?.objChatHistoryModel.strOpponentId),//
                    "senderId":checkForNULL(obj:self?.strMyChatId ),
                    "timestamp":calendarDate,
                    "type":checkForNULL(obj:"user"),
                    "unreadMessage":checkForNULL(obj: 0),
                    "favourite":checkForNULL(obj: 1),
                    "userName":checkForNULL(obj:self?.objChatHistoryModel.strOpponentName )//opponent name
                ]
                self?.ref.child("chat_history").child(myId).child(oppId).setValue(dict1)
            }
        })
        self.viewMenu.isHidden = true
    }
}
// MARK: - keyboard methods
extension GroupChatVC{
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo
        let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
        let keyboardFrame: CGRect? = kbFrame?.cgRectValue
        let height: CGFloat? = keyboardFrame?.size.height
        UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
        if isFromBlock{
            isFromBlock = false
        }
        UIView.animate(withDuration: animationDuration , animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
}

//MARK: - imagePicker extension method
extension GroupChatVC{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        isImagePicked = true
        if isImageLocallyUploading{
            return
        }else{
            isImageLocallyUploading = true
        }
        var img : UIImage?
        var url : URL?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            img = image
            self.strTextNotification = "Image"
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            img = image
            self.strTextNotification = "Image"
            //// Himanshu
            url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            ////
        }
        if let Image = img {
            imgZoomImage.image = Image
            self.strTextNotification = "Image"
            self.setImageLocallyBeforeSend()
            self.sendImageToFirebaseWithImage(image: Image, url: url)
            
        }
        self.dismiss(animated: true, completion: nil)
        //objWebserviceManager.StartIndicator()
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.viewZoomImg.isHidden = true
        dismiss(animated: true, completion: nil)
        ////print("picker cancel.")
    }
    
    func openCamera() //to Access Camera
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {}
    }
    
    func openGallary() //to Access Gallery
    {   //imagePicker.allowsEditing = true
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
    
        //DispatchQueue.main.async {
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: {
                self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
            })
        //}
    }
}


//MARK: - send image on firebase
extension GroupChatVC{
    func sendImageToFirebaseWithImage(image: Image, url: URL?){
        //objWebserviceManager.StartIndicator()
        //let imageData = UIImageJPEGRepresentation(image, 0.6)
        //        imageData = self.compressImage(image: image) as Data?
        //        let localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        var localFilePath = ""
        let metadata = StorageMetadata()
        let str = url?.absoluteString ?? ""
        if str.contains(".gif"){
            imageData = try! Data.init(contentsOf: url!)
            metadata.contentType = "image/gif"
            localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).gif"
        }else{
            imageData = objChatShareData.compressImage(image: image) as Data?
            metadata.contentType = "image/jpeg"
            localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpeg"
            var compression = 1.0
            while compression >= 0.0 {
                imageData = image.jpegData(compressionQuality: CGFloat(compression))!
                let imageLength: Int = imageData!.count
                if imageLength < 60000 {
                    break
                }
                compression -= 0.1
            }
        }
        self.storageRef.child(localFilePath).putData(imageData!, metadata: metadata) { [weak self] (newMetadata, error) in
            if let error = error {
                objWebserviceManager.StopIndicator()
                print("Error uploading: \(error)")
                return
            }
            guard let strongSelf = self else { return }
            strongSelf.storageRef.child(localFilePath).downloadURL { (url, error) in
              guard let downloadURL = url else {
                return
              }
                strongSelf.imgUrl = downloadURL.absoluteString
                strongSelf.sendImageInStringForm()
            }
            objWebserviceManager.StopIndicator()
        }
    }
    
    func setImageLocallyBeforeSend(){
        let calendarDate = ServerValue.timestamp()
        let currentTimeStamp = Date().toMillis()
       
        let dict = [
            "memberCount":self.objChatHistoryModel.strMemberCount,
            "message": "https://local",
            "messageType":checkForNULL(obj:1),
            "reciverId":checkForNULL(obj:self.objChatHistoryModel.strOpponentId),
            "readMember":[self.strMyChatId: self.strMyChatId],
            "readStatus":"0",
            "senderId":checkForNULL(obj:self.strMyChatId ),
            "timestamp":currentTimeStamp,
            "userName":self.strName]
        let ref = Database.database().reference()
        let keyValue = ref.child("groupChat").child(objChatHistoryModel.strOpponentId).childByAutoId().key
        self.strKeyValueForHold = keyValue ?? ""
        self.arrKeyValueForHold.append(keyValue!)
        let objChat = ChatData.init(dict: dict, key: self.strKeyValueForHold)
        objChat!.imageLocal = imgZoomImage.image!
        let date = Date(timeIntervalSince1970: Double(currentTimeStamp!) / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        objChat!.str_timestamp = dateFormatter.string(from: date)
        self.arrMessages.append(objChat!)
        self.tableChat.reloadData()
        self.tableViewScrollToBottom(animated: false)
        ref.child("groupChat").child(objChatHistoryModel.strOpponentId).child(self.strKeyValueForHold).setValue(dict)
    }
    
    func sendImageInStringForm() {
        
        let calendarDate = ServerValue.timestamp()
        
        let dict = ["memberCount":self.objChatHistoryModel.strMemberCount,
                    "message": checkForNULL(obj:imgUrl),
                    "messageType":checkForNULL(obj:1),
                "reciverId":checkForNULL(obj:self.objChatHistoryModel.strOpponentId ),
                    "readMember":[self.strMyChatId: self.strMyChatId],
                    "readStatus":0,
                    "senderId":checkForNULL(obj:self.strMyChatId),
                    "timestamp":calendarDate,
                    "userName":self.strName]
        
        
        let ref = Database.database().reference()
        
        self.strKeyValueForHold = self.arrKeyValueForHold[0]
        self.arrKeyValueForHold.remove(at: 0)
        ref.child("groupChat").child(objChatHistoryModel.strOpponentId).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(self.strKeyValueForHold){
                print("true rooms exist")
            ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(self.strKeyValueForHold).setValue(dict)
                self.message = "Image"
                self.imgType = 1
                self.UpdateHistory()
                self.isImageLocallyUploading = false
            }else{
                print("false room doesn't exist")
            }
        })
    }
}


extension GroupChatVC{
    func alertViewController(titleAlert:String,methodType:String){
        let alert:UIAlertController=UIAlertController(title: "Alert", message: titleAlert, preferredStyle: UIAlertController.Style.alert)
        let YesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            if methodType == "Delete"{
                
            }else if methodType == "Block"{
                
            }else if methodType == "Mute"{
                
            }else if methodType == "Favourite"{
                
            }else {
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(YesAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

//MARK: - scroll table to bottom
extension GroupChatVC{
    
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            let numberOfSections = self.tableChat.numberOfSections
            let numberOfRows = self.tableChat.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tableChat.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                self.tableChat.isHidden = false
            }
        }
    }
    
}

//MARK: - scroll table to bottom
extension GroupChatVC{
    
    // MARK:- Kyeboard hide/show methods
    
    @objc func keyboardWasShown(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5, animations: {
                self.txtViewContainerBottom.constant = keyboardSize.size.height + 3
                ////print("\n\n\n self.txtViewContainerBottom.constant = \( self.txtViewContainerBottom.constant)")
                if self.arrMessages.count>0{
                    let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
                    self.tableChat.scrollToRow(at: indexPath, at: .top, animated: false)
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            UIView.animate(withDuration: 0.5, animations: {
                self.txtViewContainerBottom.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}


//MARK: - Edit & Delete Section
extension GroupChatVC{
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            self.view.endEditing(true)
            let touchPoint = gestureReconizer.location(in: self.tableChat)
            if let indexPaths = tableChat.indexPathForRow(at: touchPoint) {
                let obj = self.arrMessages[indexPaths.row]
                print("obj = ",obj.strMessage)
                objHoldIndex = obj
                indexOfArray = indexPaths.row
                if self.strMyChatId == String(obj.str_UId){
                    if let cell = tableChat.cellForRow(at: indexPaths) as? MyTextCell{
                        cell.viewBlur.isHidden = false
                    }
                    if let cell = tableChat.cellForRow(at: indexPaths) as? MyImageCell{
                        cell.viewBlur.isHidden = false
                    }
                    self.viewEditCommentAction.isHidden = true
                    self.viewCommentAction.isHidden = false
                }
            }
        }
    }
    
    @IBAction func btnEditCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        //let objReminder = self.objHoldIndex
        //let param = ["id":String(objReminder?._id ?? 0),"feedId":objReminder?.feedId]
        //objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
        //self.tableChat.reloadData()
    }
    
    @IBAction func btnDeleteCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        deleteCommentAlert(Index: sender.tag)
    }
    
    
    func deleteCommentAlert(Index:Int){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this message?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
           self.deleteSingleMessage(index: Index)
        }
        okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            self.tableChat.reloadData()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func deleteSingleMessage(index:Int){
        let obj = self.arrMessages[self.indexOfArray].str_Key
        print(obj)
        let ref = Database.database().reference()
        ref.child("groupChat").child(self.objChatHistoryModel.strOpponentId).child(obj).removeValue()
        
        if self.arrMessages.count == 1{
            self.UpdateHistoryForDelete(index: self.indexOfArray)
        }else if indexOfArray+1 == self.arrMessages.count{
            self.UpdateHistoryForDelete(index: self.indexOfArray-1)
            /*let objChatHistoryModel = self.arrMessages[indexOfArray-1]
            var strRecvId = ""
            if self.strMyChatId == objChatHistoryModel.str_lastSenderID{
                strRecvId = self.objChatHistoryModel.strOpponentId
            }else{
                strRecvId = self.strMyChatId
            }
            let dict1 = [
                "memberCount":0,
                "favourite":0,
                "message": checkForNULL(obj:objChatHistoryModel.strMessage),
                "messageType":checkForNULL(obj:objChatHistoryModel.str_isImage),
                "profilePic":checkForNULL(obj:objChatHistoryModel.str_ProfileImage ),//opponent image
                "reciverId":checkForNULL(obj: strRecvId),//
                "senderId":checkForNULL(obj:objChatHistoryModel.str_lastSenderID ),
                "timestamp":objChatHistoryModel.TimeStamp,
                "type":checkForNULL(obj:"user"),
                "unreadMessage":checkForNULL(obj: 0),
                "userName":checkForNULL(obj:self.objChatHistoryModel.strOpponentName )//opponent name
            ]
            let ref = Database.database().reference()
            ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(dict1)
            ref.child("chat_history").child(self.objChatHistoryModel.strOpponentId).child(self.strMyChatId).setValue(dict1)*/
        } else{
        }
        
    }
    @IBAction func btnCommentActionPopUpHiddden(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        self.tableChat.reloadData()
    }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Select Option"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        objAppShareData.arrTBottomSheetModal = ["Group Details"]
        if self.lblFavouriteStatus.text == "Unfavourite"{
            objAppShareData.arrTBottomSheetModal.append("Unfavourite")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Add To Favourite")
        }
        if self.objChatHistoryModel.strAdminId == self.strMyChatId{
            objAppShareData.arrTBottomSheetModal.append("Add New Member")
            objAppShareData.arrTBottomSheetModal.append("Remove Member")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Report Group")
        }
        scrollingNC.callback = { [weak self] (str) in
                guard let strongSelf = self else {
                    return
                }
                let btn = UIButton()
                if str == "0"{
                    strongSelf.btnAllMember(btn)
                }else if str == "1"{
                    strongSelf.btnAddToFavouriteAction(btn)
                }
                if strongSelf.objChatHistoryModel.strAdminId == strongSelf.strMyChatId{
                    if str == "2"{
                        strongSelf.btnAddMemberAction(btn)
                    }else if str == "3"{
                        strongSelf.btnRemoveMemberddAction(btn)
                    }
                }else{
                    if str == "2"{
                        strongSelf.btnReportGroupAction(btn)
                    }
                }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Select Option"
        objAppShareData.arrTBottomSheetModal = ["Group Details"]
        if self.lblFavouriteStatus.text == "Unfavourite"{
            objAppShareData.arrTBottomSheetModal.append("Unfavourite")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Add To Favourite")
        }
        if self.objChatHistoryModel.strAdminId == self.strMyChatId{
            objAppShareData.arrTBottomSheetModal.append("Add New Member")
            objAppShareData.arrTBottomSheetModal.append("Remove Member")
        }else{
            objAppShareData.arrTBottomSheetModal.append("Report Group")
        }
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            let btn = UIButton()
            print(str)
            if str == "0"{
                strongSelf.btnAllMember(btn)
            }else if str == "1"{
                strongSelf.btnAddToFavouriteAction(btn)
            }
            if strongSelf.objChatHistoryModel.strAdminId == strongSelf.strMyChatId{
                if str == "2"{
                    strongSelf.btnAddMemberAction(btn)
                }else if str == "3"{
                    strongSelf.btnRemoveMemberddAction(btn)
                }
            }else{
                if str == "2"{
                    strongSelf.btnReportGroupAction(btn)
                }
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
}


//MARK: - get badge count
extension GroupChatVC{
    func badgeCount(arrTemp:[[String:Any]]){
        
        for obj in arrTemp{
            var opponentId = ""
            if  let UserId = obj["memberId"] as? String{
                opponentId = UserId
            }else if let UserId = obj["memberId"] as? Int{
                opponentId = String(UserId)
            }
        var TotalBadgeCount = 0
        var chatCounts = "0"
        
        self.ref.child("socialBookingBadgeCount").child(opponentId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) -> Void in
            if !(self?.isImagePicked)! {
                objWebserviceManager.StopIndicator()
            }
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    
                    if let chatCount1 = dict["chatCount"] as? String{
                        chatCounts = chatCount1
                    }else if let chatCount1 = dict["chatCount"] as? Int{
                        chatCounts = String(chatCount1)
                    }
                    
                    chatCounts = String((Int(chatCounts) ?? 0)+1)
                    var totalCounsts = "0"
                    if let totalCount1 = dict["totalCount"] as? String{
                        totalCounsts = totalCount1
                    }else if let totalCount1 = dict["totalCount"] as? Int{
                        totalCounsts = String(totalCount1)
                    }
                    
                    TotalBadgeCount = (Int(totalCounsts) ?? 0)+1
                    self?.ref.child("socialBookingBadgeCount").child(opponentId).updateChildValues(["chatCount":chatCounts,"totalCount":TotalBadgeCount])
                    
                //    self?.ChatNotification(badgeCount: TotalBadgeCount)
                }
            }else{
                self?.ref.child("socialBookingBadgeCount").child(opponentId).updateChildValues(["chatCount":chatCounts,"totalCount":TotalBadgeCount])
               // self?.ChatNotification(badgeCount: TotalBadgeCount)
            }
        })
    }
        
}
}
