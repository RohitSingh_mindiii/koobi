//
//  GroupMemberListVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/16/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

class GroupMemberListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate{
    
    var objChatHistoryModel = ChatHistoryData()

    @IBOutlet weak var tblCreateGroupList:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var txtEnterGroupName: UILabel!
    @IBOutlet weak var txtEnterGroupDescription: UILabel!
    @IBOutlet weak var imgGroupProfile:UIImageView!
    @IBOutlet weak var imgGroupProfileForHide:UIImageView!
    @IBOutlet weak var lblPopUpTitle:UILabel!
    @IBOutlet weak var btnMuteUnmute: UIButton!
    @IBOutlet weak var lblPopUpDescription:UILabel!

    @IBOutlet weak var lblMuteGroup: UILabel!
    @IBOutlet weak var lblClearChatHistory: UILabel!
    @IBOutlet weak var lblMemberCount: UILabel!

    @IBOutlet weak var viewDeletePopup: UIView!
    
    //Edit Group Outlet
    
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var viewEditGroup: UIView!
    @IBOutlet weak var viewForMute: UIView!
    @IBOutlet weak var txtEditGName: UITextField!
    @IBOutlet weak var lblLeaveGroup: UILabel!
    @IBOutlet weak var txtEditGDescription: UITextView!
    @IBOutlet weak var imgEditImage: UIImageView!
    @IBOutlet weak var btnEditIcon: UIButton!
    ///////
    var imagePicker = UIImagePickerController()
    
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var storageRef: StorageReference!
    
    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [ChatHistoryData]()
    fileprivate var arrChatHistoryMyData = [ChatHistoryData]()
    fileprivate var newGroupName = ""
    fileprivate var imgUrl = ""
    fileprivate var fromImage = false
    fileprivate var strAdminId = ""
}


//MARK: - edit viewCode
extension GroupMemberListVC{
    @IBAction func btnEditGroupPermission(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtEditGDescription.text = self.txtEnterGroupDescription.text
        self.txtEditGName.text = self.txtEnterGroupName.text
        self.imgEditImage.image = self.imgGroupProfile.image
        if self.imgEditImage.image == #imageLiteral(resourceName: "bg_group_placeholder"){
           self.imgGroupProfileForHide.isHidden = false
        }else{
           self.imgGroupProfileForHide.isHidden = true
        }
        self.viewEditGroup.isHidden = false
    }
    @IBAction func btnEditCancel(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewEditGroup.isHidden = true
    }
    
    @IBAction func btnEditDone(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtEditGName.text = self.txtEditGName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtEditGDescription.text = self.txtEditGDescription.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (self.txtEditGName.text == self.txtEnterGroupName.text) && (self.txtEditGDescription.text == self.txtEnterGroupDescription.text) && (self.imgEditImage.image == self.imgGroupProfile.image){
            self.viewEditGroup.isHidden = true
        }else{

        if self.imgEditImage.image != self.imgGroupProfile.image{
        }else{
            self.imgUrl = self.objChatHistoryModel.strOpponentProfileImage
        }
            if self.txtEditGName.text?.count == 0{
               
                objAppShareData.showAlert(withMessage: "Please enter group name", type: alertType.bannerDark,on: self)
            }else if self.txtEditGDescription.text?.count == 0{
 
                objAppShareData.showAlert(withMessage: "Please enter group description", type: alertType.bannerDark,on: self)
            }else{
                self.editGroupMethod()
            }
        }}
    
    func editGroupMethod(){
        
            let dict = ["groupImg":self.imgUrl,
                        "groupDescription":self.txtEditGDescription.text ?? "",
                        "groupName":self.txtEditGName.text ?? "",
                        ] as [String : Any]
            ref.child("group").child(self.objChatHistoryModel.strOpponentId).updateChildValues(dict)
 
            for obj in self.arrChatHistory{
                let oppId = obj.strOpponentId
                let time = ServerValue.timestamp()
                let dictHistory = ["profilePic":self.imgUrl,
                                   "userName":self.txtEditGName.text ?? ""] as [String : Any]
                ref.child("chat_history").child(oppId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(dictHistory)
                self.txtEnterGroupDescription.text = self.txtEditGDescription.text
                self.txtEnterGroupName.text = self.txtEditGName.text
                self.imgGroupProfile.image = self.imgEditImage.image
                self.viewEditGroup.isHidden = true
            }
        }
}
    

//MARK: - System Method extension
extension GroupMemberListVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDeletePopup.isHidden = true

        ref =  Database.database().reference()
        storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
        
        self.strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.viewEditGroup.isHidden = true
        self.viewConfigure()
        //self.GetUserListFromFirebase()
        self.muteStatus()
        self.viewConfigure()
        self.txtEditGDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtEditGDescription.tintColorDidChange()
       
        if !self.objChatHistoryModel.arrMemberId.contains(self.strMyChatId){
            self.lblLeaveGroup.text = "Delete Group"
            self.viewForMute.isHidden = true
        }else{
            self.lblLeaveGroup.text = "Leave Group"
            self.viewForMute.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDeletePopup.isHidden = true
        
        ///// New
        self.objChatHistoryModel.strOpponentId = self.objChatHistoryModel.strReceiverId
        /////
        
        if fromImage == false{
           self.GetUserListFromFirebase()
        }else{
            objWebserviceManager.StartIndicator()
        }
        fromImage = false
        if self.strAdminId == self.strMyChatId {
            self.btnEditIcon.isHidden = false
        }else{
            self.btnEditIcon.isHidden = true
        }
     }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - Custome method extension
fileprivate extension GroupMemberListVC{
    func viewConfigure(){
        self.tblCreateGroupList.delegate = self
        self.tblCreateGroupList.dataSource = self
        self.txtEditGDescription.delegate = self
        self.txtEditGName.delegate = self
         self.imagePicker.delegate = self
        ref = Database.database().reference()
        self.addAccessoryView()
    }
    
    func addAccessoryView() {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.7098039216, alpha: 1)
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        self.txtEditGDescription.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
    }
}

//MARK: - Button extension
fileprivate extension GroupMemberListVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnMuteAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.changeMuteStatus()
    }
    
    @IBAction func btnClearChathistoryAction(_ sender: UIButton){
         self.view.endEditing(true)
        self.lblPopUpTitle.text = "Delete Conversation?"
        self.lblPopUpDescription.text = "You will not be able to recover this conversation."
         self.viewDeletePopup.isHidden = false
    }
    
    @IBAction func btnLeaveGroupAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.lblPopUpDescription.text = "You will not be able to recover this conversation."
        if self.lblLeaveGroup.text == "Delete Group"{
            self.lblPopUpTitle.text = "Delete Group?"
            self.viewDeletePopup.isHidden = false
        }else{
            self.lblPopUpTitle.text = "Leave Group?"
            self.viewDeletePopup.isHidden = false
            if self.strAdminId == self.strMyChatId{
               self.lblPopUpDescription.text = "Are you sure you want to leave this group? Since you're the admin, before leaving now make admin of the group."
            }else{
                self.lblPopUpDescription.text = "You will not be able to recover this conversation."
            }
        }
        /*
        if self.strAdminId == self.strMyChatId{
            self.viewDeletePopup.isHidden = false
            //self.goToMakeNewAdminVC()
            //objAppShareData.showAlert(withMessage: "Under developement" , type: alertType.bannerDark,on: self)
        }else{
            if self.lblLeaveGroup.text == "Delete Group"{
                self.lblPopUpTitle.text = "Delete Group?"
                self.viewDeletePopup.isHidden = false
            }else{
                self.lblPopUpTitle.text = "Leave Group?"
                self.viewDeletePopup.isHidden = false
            }
        }
        */
    }
    func goToMakeNewAdminVC(){
        let sb = UIStoryboard(name: "Chat", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "MakeNewAdminVC") as! MakeNewAdminVC
        detailVC.objChatHistoryModel = self.objChatHistoryModel
        detailVC.modalPresentationStyle = .fullScreen
        self.present(detailVC, animated: true, completion: nil)
    }
    @IBAction func btnEditImageAction(_ sender: UIButton){
        self.view.endEditing(true)
        
        self.view .endEditing(true)
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
       
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}


//MARK: - textfield method extension
extension GroupMemberListVC{
 
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtEditGDescription {
            if (text.count ) != 0
            { searchString = self.txtEditGDescription.text! + (text).uppercased()
                newLength = (self.txtEditGDescription.text?.count)! + text.count - range.length
            }
            else {
                //searchString = (self.txtEditGDescription.text as NSString?)?.substring(to: (self.txtEditGDescription.text?.count)! - 1).uppercased()
            }
            if newLength <= 200{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtEditGName {
            if (string.count ) != 0
            { searchString = self.txtEditGName.text! + (string).uppercased()
                newLength = (self.txtEditGName.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtEditGName.text as NSString?)?.substring(to: (self.txtEditGName.text?.count)! - 1).uppercased()
            }
            if newLength <= 25{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEditGName{
            txtEditGDescription.becomeFirstResponder()
        }
        return true
    }
}


//MARK: - tableview method extension
extension GroupMemberListVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.lblMemberCount.text = String(self.arrChatHistory.count)+" Members"
        tableHeight()
        return arrChatHistory.count
    }
    
    
    func tableHeight(){
        if arrChatHistory.count <= 3{
           self.height.constant = 200
        }else{
            self.height.constant = (CGFloat(self.arrChatHistory.count*55))
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCreatGroup", for: indexPath) as? CellCreatGroup{
            let objChatList = arrChatHistory[indexPath.row]
            if objChatList.strOpponentProfileImage == ""{
                cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
            }else{
                let url = URL(string: objChatList.strOpponentProfileImage)
                //cell.imgUserProfile.af_setImage(withURL: url!)
                cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }
            cell.btnProfile.tag = indexPath.row
            cell.lblUserName.text = objChatList.strOpponentName
            if self.strAdminId == objChatList.strOpponentId{
                cell.viewUserType.layer.cornerRadius = 4
                cell.viewUserType.layer.borderWidth = 1
                cell.viewUserType.layer.borderColor = UIColor.theameColors.skyBlueNewTheam.cgColor
                cell.viewUserType.isHidden = false
                cell.lblUserType.isHidden = false
            }else{
                cell.viewUserType.isHidden = true
                cell.lblUserType.isHidden = true
            }
            if objChatList.strOpponentId == self.strMyChatId{
                cell.lblUserName.text = "You"
            } 
           return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


//MARK: - UIImagePicker extension Methods
extension GroupMemberListVC{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //imagePicker.allowsEditing = true
        var img : UIImage?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            img = image
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            img = image
        }
       
        let imageCropVC = RSKImageCropViewController(image: img!, cropMode: .custom)
        imageCropVC.delegate = self
        imageCropVC.dataSource = self
        
        imageCropVC.view.setNeedsUpdateConstraints()
        imageCropVC.view.layoutSubviews()
        
        var cons = imageCropVC.view.constraints
        var newConstraints = [AnyHashable]()
        for object: NSLayoutConstraint in cons {
            if object.identifier == nil || ((object.identifier as NSString?)?.range(of: "Layout-Height"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Width"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Left"))?.location != NSNotFound {
                newConstraints.append(object)
            }
        }
        imageCropVC.view.removeConstraints(imageCropVC.view.constraints)
        if let aConstraints = newConstraints as? [NSLayoutConstraint] {
            imageCropVC.view.addConstraints(aConstraints)
        }
        picker.dismiss(animated: false) {() -> Void in
        }
        imageCropVC.modalPresentationStyle = .fullScreen
        self.present(imageCropVC, animated: false, completion: nil)
        //navigationController?.pushViewController(imageCropVC, animated: false)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }
    
    func openCamera() //to Access Camera
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {}
    }
    
    func openGallary() //to Access Gallery
    {
       // imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
        })
    }
    
    
    func sendImageToFirebaseWithImage(image : UIImage){
        imageData = objChatShareData.compressImage(image: image) as Data?
        let localFilePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        objWebserviceManager.StartIndicator()
        self.storageRef.child(localFilePath).putData(imageData!, metadata: metadata) { [weak self] (metadata, error) in
            objWebserviceManager.StopIndicator()
            if let error = error {
                print("Error uploading: \(error)")
                return
            }
            guard let strongSelf = self else { return }
            self?.storageRef.child(localFilePath).downloadURL { (url, error) in
              guard let downloadURL = url else {
                return
              }
                strongSelf.imgUrl = downloadURL.absoluteString
            }
            print(">> imgUrl =  \(self?.imgUrl)")
        }
    }
}


//MARK: - FireBase Method calls
extension GroupMemberListVC{
    func GetUserListFromFirebase() {
        objWebserviceManager.StartIndicator()
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
            self?.arrChatHistory.removeAll()
            self?.arrChatHistoryMyData.removeAll()
            
            if let dictData = snapshot.value as? NSDictionary{
                if let aId = dictData["adminId"] as? Int{
                    self?.strAdminId = String(aId)
                }else if let aId = dictData["adminId"] as? String{
                    self?.strAdminId = aId
                }
                if self?.strAdminId == self?.strMyChatId {
                    self?.btnEditIcon.isHidden = false
                }else{
                    self?.btnEditIcon.isHidden = true
                }
                let groupImage = dictData["groupImg"] as? String ?? ""
                self?.txtEnterGroupName.text = dictData["groupName"] as? String ?? ""
                self?.txtEnterGroupDescription.text = dictData["groupDescription"] as? String ?? ""
                
                if groupImage == "" || groupImage == "http://koobi.co.uk:3000/uploads/default_group.png"{
                    self?.imgGroupProfile.image = #imageLiteral(resourceName: "bg_group_placeholder")
                }else{
                    let url = URL(string: groupImage)
                    //self?.imgGroupProfile.af_setImage(withURL: url!)
                    self?.imgGroupProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }
                
                if let dictNNNN = dictData["member"] as? NSDictionary {
                let arr = dictNNNN.allValues as NSArray
                var isNotLeave = false
                for  dicttt in arr {
                    if let dict = dicttt as? NSDictionary {
                        print("\n\ndicttt = \(dict)")
                        
                        let objChatList = ChatHistoryData()
                        objChatList.strOpponentName = dict["userName"] as? String ?? ""
                        objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                        objChatList.strMemberType  = dict["type"] as? String ?? ""
                        objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                        objChatList.strMuteStatus = String(dict["mute"] as? Int ?? 0)
                        objChatList.strCreatUserStatus = false
                        if let id = dict["memberId"] as? Int {
                            objChatList.strOpponentId = String(id)
                        }else if let id = dict["memberId"] as? String {
                            objChatList.strOpponentId = id
                        }
                        
                        if objChatList.strOpponentId == self?.strMyChatId{
                            isNotLeave = true
                            self?.arrChatHistoryMyData.append(objChatList)
                        }else{
                            self?.arrChatHistory.append(objChatList)
                        }
                    }
                    objWebserviceManager.StopIndicator()
                    if (self?.arrChatHistory.count)! > 0 {
                        self?.lblNoDataFound.isHidden = true
                    }else{
                        self?.lblNoDataFound.isHidden = false
                    }
                }
                    //////
                    if !isNotLeave{
                        self?.lblLeaveGroup.text = "Delete Group"
                        self?.viewForMute.isHidden = true
                    }else{
                        self?.lblLeaveGroup.text = "Leave Group"
                        self?.viewForMute.isHidden = false
                    }
                    //////
                    self?.addMyObj()
                    
                }else if let dict = snapshot.value as? NSDictionary {
                    strongSelf.parseDataChatHistory(dictHistory: dict)
                    objWebserviceManager.StopIndicator()
                }
                self?.tblCreateGroupList.reloadData()
                objWebserviceManager.StopIndicator()
                
                if (self?.arrChatHistory.count == 0) {
                    self?.lblNoDataFound.isHidden = false;
                    self?.tblCreateGroupList.isHidden = true;
                } else {
                    self?.lblNoDataFound.isHidden = true;
                    self?.tblCreateGroupList.isHidden = false;
                }
                objWebserviceManager.StopIndicator()
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                strongSelf.arrChatHistory.removeAll()
                strongSelf.arrChatHistoryMyData.removeAll()
                strongSelf.tblCreateGroupList.reloadData()
            }
        })
    }
    
    func addMyObj(){
        if self.arrChatHistoryMyData.count >= 1{
        self.arrChatHistory.append(self.arrChatHistoryMyData[0])
        }
        for obj in self.arrChatHistory{
            if (obj.strOpponentId == strAdminId) && obj.strOpponentId != self.strMyChatId{
                let index = self.arrChatHistory.index(of: obj)
                self.arrChatHistory.remove(at: index!)
                self.arrChatHistory.insert(obj, at: 0)
            }
        }
    }
    
    func parseDataChatHistory(dictHistory:NSDictionary){
        self.arrChatHistory.removeAll()
        self.arrChatHistoryMyData.removeAll()
        if let aId = dictHistory["adminId"] as? Int{
            self.strAdminId = String(aId)
        }else if let aId = dictHistory["adminId"] as? String{
            self.strAdminId = aId
        }
        if self.strAdminId == self.strMyChatId {
            self.btnEditIcon.isHidden = false
        }else{
            self.btnEditIcon.isHidden = true
        }
        
        if let newDict = dictHistory as? [String:Any]{
                if let dictmember = newDict["member"] as? NSArray{
                var isNotLeave = false
                for obj in dictmember{
                    
                if let dict = obj as? [String:Any]{
                    
                let objChatList = ChatHistoryData()
                objChatList.strOpponentName = dict["userName"] as? String ?? ""
                objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                        if let id = dict["memberId"] as? Int {
                           objChatList.strOpponentId = String(id)
                        }else if let ids = dict["memberId"] as? String{
                            objChatList.strOpponentId = ids
                        }
                        if strAdminId == objChatList.strOpponentId{
                            objChatList.strUserType = "Admin"
                        }else{
                            objChatList.strUserType = "member"
                        }
                objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                objChatList.strMuteStatus = "0"
                
                if objChatList.strOpponentId == self.strMyChatId{
                    isNotLeave = true
                self.arrChatHistoryMyData.append(objChatList)
                }else{
                    self.arrChatHistory.append(objChatList)
                }
            }
            objWebserviceManager.StopIndicator()
            
            if arrChatHistory.count > 0 {
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
        }
                    //////
                    if !isNotLeave{
                        self.lblLeaveGroup.text = "Delete Group"
                        self.viewForMute.isHidden = true
                    }else{
                        self.lblLeaveGroup.text = "Leave Group"
                        self.viewForMute.isHidden = false
                    }
                    //////
    }
}

        self.addMyObj()

        print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
        self.tblCreateGroupList.reloadData()
        objWebserviceManager.StopIndicator()
        
        if (self.arrChatHistory.count == 0) {
            self.lblNoDataFound.isHidden = false;
            self.tblCreateGroupList.isHidden = true;
        } else {
            self.lblNoDataFound.isHidden = true;
            self.tblCreateGroupList.isHidden = false;
        }
    }
}

//MARK: - manage message mute status extension
extension GroupMemberListVC{
    func muteStatus(){
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").child(self.strMyChatId).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            print("snapshot = \(snapshot)")
            // guard let strongSelf = self else { return }
            if snapshot.exists(){
                if let dict = snapshot.value as? [String:Any]{
                    if let muteId = dict["mute"] as? Int{
                        self.objChatHistoryModel.strMuteStatus = String(muteId)
                    }else if let muteIds = dict["mute"] as? String{
                        self.objChatHistoryModel.strMuteStatus = muteIds
                    }
                }
            } else{
                self.objChatHistoryModel.strMuteStatus = "0"
            }
            self.lblMuteGroup.text = "Mute Group"
            
            if self.objChatHistoryModel.strMuteStatus == "0"{
                self.lblMuteGroup.text = "Mute Group"
                self.btnMuteUnmute.setImage(#imageLiteral(resourceName: "ico_toggle_off"), for: .normal)
            }else{
                self.btnMuteUnmute.setImage(#imageLiteral(resourceName: "ico_toggle_on"), for: .normal)
            }
        })
    }
    
    func changeMuteStatus(){
        var muteId = 0
        self.lblMuteGroup.text = "Mute Group"
        if objChatHistoryModel.strMuteStatus == "0"{
            muteId = 1
            self.lblMuteGroup.text = "Mute Group"
        }
        self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").child(self.strMyChatId).updateChildValues(["mute":muteId])
        if objChatHistoryModel.strMuteStatus == "0"{
            objChatHistoryModel.strMuteStatus = "1"
            self.btnMuteUnmute.setImage(#imageLiteral(resourceName: "ico_toggle_on"), for: .normal)
        }else{
            objChatHistoryModel.strMuteStatus = "0"
            self.btnMuteUnmute.setImage(#imageLiteral(resourceName: "ico_toggle_off"), for: .normal)
        }
    }
}

//MARK: - Leave Group extension
extension GroupMemberListVC{
     func leaveGroup(){
        if self.lblPopUpTitle.text == "Delete Group?"{
            objChatShareData.fromGroupChatDelete = true
        self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue([nil])
            self.dismiss(animated: false, completion: nil)
        }else{
            if self.strAdminId == self.strMyChatId{
                self.goToMakeNewAdminVC()
            }else{
                self.lblLeaveGroup.text = "Delete Group"
                self.ref.child("group").child(self.objChatHistoryModel.strOpponentId).child("member").child(self.strMyChatId).setValue([:])
                self.ref.child("myGroup").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).removeValue()
                self.dismiss(animated: false, completion: nil)
            }
        }
     }
   
    @IBAction func btnClearChatYes(_ sender: UIButton) {
        if self.lblPopUpTitle.text == "Leave Group?" || self.lblPopUpTitle.text == "Delete Group?"{
            self.leaveGroup()
        }else{
        let currentTime = ServerValue.timestamp()
        let dict = ["deleteBy":currentTime]
        let dictNew = ["message":""]
        self.ref.child("group_msg_delete").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(dict)
        self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(dictNew)
        }
        self.viewDeletePopup.isHidden = true
    }
    @IBAction func btnClearChatNo(_ sender: UIButton) {
        self.viewDeletePopup.isHidden = true
    }
}

// MARK: - RSKImageCropViewControllerDataSource methods
extension GroupMemberListVC : RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource{
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.fromImage = true
        self.dismiss(animated: false, completion: nil)
        //navigationController?.popViewController(animated: true)
        let imgPosted = croppedImage
        objWebserviceManager.StartIndicator()
        self.processSelectedImage(imgPosted: imgPosted)
    }
    
    // Returns a custom rect for the mask.
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
        var maskSize: CGSize
        // calculate the width based on the screen size - 30px
        let screenRect: CGRect = UIScreen.main.bounds
        let width: CGFloat = screenRect.size.width - 20
        // 4:3, height will be 75% of the width
        let height: CGFloat = width * 0.75
        if controller.isPortraitInterfaceOrientation() {
            maskSize = CGSize(width: width, height: height)
        }
        else {
            maskSize = CGSize(width: 355, height: 266.25)
        }
        let viewWidth: CGFloat = controller.view.frame.width
        let viewHeight: CGFloat = controller.view.frame.height
        let maskRect = CGRect(x: (viewWidth - maskSize.width) * 0.5, y: (viewHeight - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
        return maskRect
    }
    // Returns a custom rect in which the image can be moved.
    
    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
        // If the image is not rotated, then the movement rect coincides with the mask rect.
        return controller.maskRect
    }
    
    // Returns a custom path for the mask.
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
        return UIBezierPath(rect: controller.maskRect)
    }
    
    // MARK: - RSKImageCropViewControllerDelegate
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        //isImageCanceled = true
        self.dismiss(animated: false, completion: nil)
        //navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        navigationController?.popViewController(animated: true)
        let imgPosted = croppedImage
        self.processSelectedImage(imgPosted: imgPosted)
    }
    func processSelectedImage(imgPosted : UIImage){
         self.sendImageToFirebaseWithImage(image: imgPosted)
         imgEditImage.image = imgPosted
         self.imgGroupProfileForHide.isHidden = true
    }
}


extension GroupMemberListVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}
