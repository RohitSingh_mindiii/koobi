//
//  NewMessageListVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/22/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class NewMessageListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var tblNewMessage:UITableView!
    @IBOutlet weak var txtSearchUser: UITextField!

}
//MARK: - System Method extension
extension NewMessageListVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewConfigure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.clearUnreadChatHistory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: - Custome method extension
fileprivate extension NewMessageListVC{
    func viewConfigure(){
        self.tblNewMessage.delegate = self
        self.tblNewMessage.dataSource = self
        self.txtSearchUser.delegate = self
        
        self.tblNewMessage.reloadData()
    }
}

//MARK: - Button extension
fileprivate extension NewMessageListVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnSendMessageAction(_ sender: UIButton){
        self.view.endEditing(true)
    }
}
//MARK: - tableview method extension
extension NewMessageListVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatHistory", for: indexPath) as? CellChatHistory{
              return cell
         }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
