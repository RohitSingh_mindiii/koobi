//
//  MemberRequestList.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/21/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class RequestVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblAddMember:UITableView!
    @IBOutlet weak var btnChatRequest:UIButton!
    @IBOutlet weak var btnGroupRequest:UIButton!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var lblBotumeChatRequest:UILabel!
    @IBOutlet weak var lblBotumeGroupRequest:UILabel!
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [GroupModal]()
 }

//MARK: - System Method extension
extension RequestVC{
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewConfigure()
            self.GetRequestFromFirebase()
        }
        override func viewWillAppear(_ animated: Bool) {
        }
        override func viewWillDisappear(_ animated: Bool) {
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
    }
    
    
//MARK: - Custome method extension
fileprivate extension RequestVC{
    func viewConfigure(){
            self.tblAddMember.delegate = self
            self.tblAddMember.dataSource = self
            self.tblAddMember.reloadData()
            ref = Database.database().reference()
            strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    }
    
    func selectChatRequest(){
        self.lblBotumeChatRequest.backgroundColor = #colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1)
        self.lblBotumeGroupRequest.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.btnChatRequest.setTitleColor(#colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1), for: .normal)
        self.btnGroupRequest.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        self.tblAddMember.reloadData()
    }
    
    func selectGroupRequest(){
        self.lblBotumeChatRequest.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.lblBotumeGroupRequest.backgroundColor = #colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1)
        self.btnChatRequest.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        self.btnGroupRequest.setTitleColor(#colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1), for: .normal)
        self.tblAddMember.reloadData()
    }
    
    func GetRequestFromFirebase() {
        objWebserviceManager.StartIndicator()
        strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        //_refHandle = self.ref.child("group_request").child(self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
    self.ref.child("group_request").child(self.strMyChatId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                return
            }
            print(snapshot)
            print("\n\n\(String(describing: snapshot.key))")
            print("\n\n\(snapshot.value)")
            self?.arrChatHistory.removeAll()
            
            if let dict = snapshot.value as? NSDictionary {
              strongSelf.parseDataChatHistory(dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                strongSelf.arrChatHistory.removeAll()
                strongSelf.tblAddMember.reloadData()
            }
        })
    }
    
    func parseDataChatHistory(dictHistory:NSDictionary){
        //// group_request
        self.arrChatHistory.removeAll()
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                let objChatList = GroupModal()
                let lastActivity = dict["timestamp"] as? Double ?? 0
                objChatList.timestamp = lastActivity
                let date = Date(timeIntervalSince1970: lastActivity / 1000.0)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
                dateFormatter.timeZone = NSTimeZone.local
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                objChatList.strTimeAgo = objChatShareData.timeAgoSinceDate(date: date as NSDate, numericDates: true)
                objChatList.strAutoIdFirebaseNode = String(describing: key)
                let strUserId = dict["senderId"] as? String ?? ""
                objChatList.strUserOrSenderId = strUserId
            self.ref.child("users").child(strUserId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
                    guard let strongSelf = self else {
                        return
                    }
                    print(snapshot)
                    print("\n\n\(String(describing: snapshot.key))")
                    print("\n\n\(snapshot.value)")
                    if let dict = snapshot.value as? NSDictionary {
                        objChatList.strUserName = dict["userName"] as? String ?? ""
                        objChatList.strUserImage = dict["profilePic"] as? String ?? ""
                        objChatList.strUserFirebaseToken = dict["firebaseToken"] as? String ?? ""
                    }
                    
                    let strGroupId = dict["groupId"] as? String ?? ""
            self?.ref.child("group").child(strGroupId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
                        guard let strongSelf = self else {
                            return
                        }
                        print(snapshot)
                        print("\n\n\(String(describing: snapshot.key))")
                        print("\n\n\(snapshot.value)")
                        if let dict = snapshot.value as? NSDictionary {
                            objChatList.strGroupName = dict["groupName"] as? String ?? ""
                            objChatList.strGroupId = String(describing: snapshot.key)
                            let dictMember = dict["member"] as? [String:Any] ?? [:]
                            objChatList.strMemberCount = String(dictMember.count)
                        }
                        self?.arrChatHistory.append(objChatList)
                        
                    objWebserviceManager.StopIndicator()
                        if self?.arrChatHistory.count ?? 0 > 0 {
                            self?.lblNoDataFound.isHidden = true
                        }else{
                            self?.lblNoDataFound.isHidden = false
                        }
                
                if self?.arrChatHistory.count ?? 0 > 0 {
                   self?.arrChatHistory =  (self?.arrChatHistory.sorted(by: { $0.timestamp > $1.timestamp}))!
                }
                
                        self?.tblAddMember.reloadData()
                        objWebserviceManager.StopIndicator()
                        
                        if (self?.arrChatHistory.count == 0) {
                            self?.lblNoDataFound.isHidden = false;
                            self?.tblAddMember.isHidden = true;
                        } else {
                            self?.lblNoDataFound.isHidden = true;
                            self?.tblAddMember.isHidden = false;
                        }
                    })
                })
            }
        }
    }
}
    
//MARK: - Button extension
fileprivate extension RequestVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        self.view.endEditing(true)
        objWebserviceManager.StartIndicator()
        let objRequest = self.arrChatHistory[sender.tag]
        ref.child("group_request").child(self.strMyChatId).child(objRequest.strAutoIdFirebaseNode).removeValue(){ (error, snap) in
            if error == nil{
                self.ref.child("my_group_request").child(objRequest.strUserOrSenderId).child(objRequest.strGroupId).removeValue(){ (error, snap) in
                    if error == nil{
                        let Key = objRequest.strUserOrSenderId
                        let img = objRequest.strUserImage
                        let name = objRequest.strUserName
                        let FT = objRequest.strUserFirebaseToken
                        let time = ServerValue.timestamp()
                        
                        let dictOfDicts = ["createdDate":time,
                                           "firebaseToken":FT,
                                           "memberId":Int(Key)!,
                                           "mute":0,
                                           "profilePic":img,
                                           "type":"member",
                                           "userName":name] as [String : Any]
                        
                        self.ref.child("group").child(objRequest.strGroupId).child("member").child(Key).setValue(dictOfDicts){ (error, snap) in
                            if error == nil{
                                let dictNew = [objRequest.strGroupId:objRequest.strGroupId]
                                self.ref.child("myGroup").child(Key).updateChildValues(dictNew){ (error, snap) in
                                    if error == nil{
                                        let oppId = objRequest.strUserOrSenderId
                                        let timenn = ServerValue.timestamp()
                                        let count = Int(objRequest.strMemberCount) ?? 0 + 1
                                        let dictHistory = ["favourite":0,
                                                           "memberCount":count,
                                                           "memberType":"member",
                                                           "message":"",
                                                           "messageType":0,
                                                           "profilePic":objRequest.strGroupImage,
                                                           "reciverId":objRequest.strGroupId,
                                                           "senderId":objRequest.strUserOrSenderId,
                                                           "timestamp":timenn,
                                                           "type":"group",
                                                           "unreadMessage":0,
                                                           "userName":objRequest.strGroupName] as [String : Any]
                                        self.ref.child("chat_history").child(oppId).child(objRequest.strGroupId).setValue(dictHistory)
                                        self.ref.child("group_msg_delete").child(oppId).child(objRequest.strUserOrSenderId).updateChildValues(["deleteBy":ServerValue.timestamp()]){ (error, snap) in
                                            if error == nil{
                                                self.GetRequestFromFirebase()
                                            }
                                        }
                                        }
                                    }
                                }
                            }
                    }
            }
            }
            }
    }
   
    @IBAction func btnRejectAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let objRequest = self.arrChatHistory[sender.tag]
        ref.child("group_request").child(self.strMyChatId).child(objRequest.strAutoIdFirebaseNode).removeValue(){ (error, snap) in
            if error == nil{
                self.ref.child("my_group_request").child(objRequest.strUserOrSenderId).child(objRequest.strGroupId).removeValue(){ (error, snap) in
                    if error == nil{
                        self.GetRequestFromFirebase()
                    }
                }
            }
        }
    }
    @IBAction func btnGroupRequestAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.selectGroupRequest()
    }
    @IBAction func btnChatRequestAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.selectChatRequest()
    }
}
    
//MARK: - tableview method extension
extension RequestVC{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.arrChatHistory.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatHistory", for: indexPath) as? CellChatHistory{
                let objRequest = self.arrChatHistory[indexPath.row]
                cell.lblTime.text = objRequest.strTimeAgo
                cell.btnProfile.tag = indexPath.row
                
                let url = URL(string: objRequest.strUserImage)
                //cell.imgUserProfile.af_setImage(withURL: url!)
                cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                cell.lblMsg.text = "Wants to join "  + objRequest.strGroupName + " group"
                cell.lblUserName.text = objRequest.strUserName
                cell.btnAccept.tag = indexPath.row
                cell.btnReject.tag = indexPath.row
                return cell
             }
            return UITableViewCell()
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         }
}


extension RequestVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strUserName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strUserName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}
