//
//  ChatHistoryData.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit

class ChatHistoryData: NSObject {

    var strMessage = ""
    var strMessageType = ""
    var strOpponentProfileImage = ""
    var strReceiverId = ""
    var strAdminId = ""
    var strBroadCastMemberCount = ""
    var strSenderId = ""
    var TimeStamp: Double = 0.0
    var strDateTime = ""
    var strUnreadMessage = 0
    var strOpponentName = ""
    var strOpponentFireBaseToken = ""
    var strMemberType = ""
 
    var strOpponentIsLogin = 0
    var strOpponentOnlineStatus = ""
    var strDeleteTime:Double = 0.0
    var strCreatTime:Double = 0.0

    var strMuteStatus = ""
    var strCreatUserStatus = false


    var strOpponentId = ""
    var strDate = ""
    var strUserType = ""
    var strFavouriteStatus = "0"

//group chat more required variable
    var strMemberCount = 0
    var arrMemberId = [String]()
}

