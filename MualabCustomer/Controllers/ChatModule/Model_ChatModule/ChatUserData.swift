//
//  ChatUserData.swift
//  Appointment
//
//  Created by Apple on 12/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit

class ChatUserData: NSObject {

    var strEmail = ""
    var strFirebaseToken = ""
    var strFirebaseId = ""
    var strName = ""
    var imgProfileImage = ""
    var strCategory = ""
    var strUid = ""
    var strUserType = ""
    
}
