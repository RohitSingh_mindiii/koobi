//
//  showImagesVC.swift
//  Mualab
//
//  Created by Mac on 10/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class showImagesVC: UIViewController, UIScrollViewDelegate, EBPhotoViewDelegate {
    
    //amit
    private(set) var tagsHidden = true
    var objFeeds:feeds?
    //amit
    
    fileprivate var scrollSize: CGFloat = 300
    var strHeaderName = ""
    var arrFeedImages = [Any]()
    var isTypeIsUrl = false
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var lblHeader : UILabel!

    @IBOutlet weak var pageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.setValue("no", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
        if strHeaderName != "" {
           self.lblHeader.text = strHeaderName
        }
        
        self.scrollView.backgroundColor = UIColor.white
        
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            self.scrollSize = self.view.frame.size.width
            self.scrollSize = self.view.frame.size.height
            //self.setPageControll()
            self.setPageControllMethod()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    /*
    //MARK: - Local Methods
    private func setPageControll(){
        
        self.scrollView.maximumZoomScale=4
        self.scrollView.zoomScale = self.scrollView.minimumZoomScale
        self.scrollView.delegate = self
        
        self.scrollView.contentSize = CGSize(width: scrollSize * CGFloat(arrFeedImages.count), height: self.scrollView.frame.size.height)
        self.scrollView.isPagingEnabled = true
        self.scrollView.bounces=false
        self.scrollView.showsHorizontalScrollIndicator=false
        
        pageControl.numberOfPages = arrFeedImages.count
        
        for index in  0..<arrFeedImages.count {
           
            let view = UIImageView(frame: CGRect(x: 0,y:0,width: scrollSize, height: scrollSize))
            let pageScrollView = UIScrollView(frame: CGRect(x: CGFloat(index) * scrollSize, y: 0, width: scrollSize, height: self.pageView.frame.size.height))
            view.tag = 1
          
            //amit
            view.contentMode = .scaleAspectFit
            
            pageScrollView.minimumZoomScale = 1.0;
            pageScrollView.maximumZoomScale = 3.0;
            pageScrollView.zoomScale = 1.0;
            //pageScrollView.contentSize = view.bounds.size;
            pageScrollView.delegate = self;
            pageScrollView.showsHorizontalScrollIndicator = false;
            pageScrollView.showsVerticalScrollIndicator = false;
            view.backgroundColor = UIColor.black
            
            if isTypeIsUrl{
                view.af_setImage(withURL:URL.init(string:(arrFeedImages[index] as! feedData).feedPost)!)
            }else{
                view.image = arrFeedImages[index] as? UIImage
            }
            pageScrollView.addSubview(view)
            self.scrollView.addSubview(pageScrollView)
        }
    }
    */
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.setValue("yes", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
    }
    func setPageControllMethod(){
        
        //let arrFeed = objFeeds.arrFeed
        let scrollWidth = self.pageView.frame.size.width
        //let scrollHeight = self.pageView.frame.size.height
        let scrollHeight = self.view.frame.size.height-self.view.frame.size.height*0.11
        
        //self.scrollView.frame = CGRect(x: 0, y:0, width: scrollWidth, height: scrollHeight)
        self.scrollView.contentSize = CGSize(width:scrollWidth * CGFloat(arrFeedImages.count), height:scrollHeight)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator=false
        self.scrollView.bounces = false
        
        ////
        self.scrollView.minimumZoomScale = 1
        ////
        self.scrollView.maximumZoomScale = 4
        self.scrollView.zoomScale = 0.7
        //self.scrollView.zoomScale = self.scrollView.minimumZoomScale
        self.scrollView.delegate = self
        
        self.pageControl.hidesForSinglePage = true
        self.pageControl.numberOfPages = arrFeedImages.count
        self.pageControl.currentPage = 0
        self.pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
    
        for index in  0..<arrFeedImages.count {
            
            //amit
            let viewOuter = UIView.init(frame: CGRect(x: CGFloat(index) * scrollWidth, y: 0, width: scrollWidth, height: scrollHeight))
            viewOuter.backgroundColor = UIColor.white
            
            let scrollVwEBPhotoView = EBPhotoView.init(frame: CGRect(x: 0, y:(viewOuter.frame.size.height/2)-(self.view.frame.size.width*0.75)/2, width: scrollWidth, height: self.view.frame.size.width*0.75))
            
            scrollVwEBPhotoView.tag = index
            scrollVwEBPhotoView.myDelegate = self
            
            //scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFill
            scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
            scrollVwEBPhotoView.imageView.clipsToBounds = true
            scrollVwEBPhotoView.adjustsContentModeForImageSize = false
            
            scrollVwEBPhotoView.imageView.image = UIImage(named:"gallery_placeholder")
           
            if isTypeIsUrl{
                
                if let objFeedData = arrFeedImages[index] as? feedData {
                    if objFeedData.feedPost != "" {
                        //scrollVwEBPhotoView.imageView.af_setImage(withURL:URL.init(string:objFeedData.feedPost)!)
                        scrollVwEBPhotoView.imageView.sd_setImage(with: URL.init(string:objFeedData.feedPost)!, placeholderImage: nil)
                    }
                }else{
                    //scrollVwEBPhotoView.imageView.af_setImage(withURL:URL.init(string:arrFeedImages[0] as! String)!)
                    scrollVwEBPhotoView.imageView.sd_setImage(with: URL.init(string:arrFeedImages[0] as! String)!, placeholderImage: nil)
                }
                
                if let objFeeds = self.objFeeds, objFeeds.arrPhotoInfo.count > index{
                    let objPhotoInfo = objFeeds.arrPhotoInfo[index]
                    objPhotoInfo.objEBPhotoView = scrollVwEBPhotoView
                }

            }else{
                
                if let img = arrFeedImages[index] as? UIImage{
                    scrollVwEBPhotoView.imageView.image = img
                }
            }
            viewOuter.tag = index
            viewOuter.addSubview(scrollVwEBPhotoView)
            self.scrollView.addSubview(viewOuter)
        }
        self.pageControl.layoutIfNeeded()
        self.scrollView.clipsToBounds = true
        self.setTagOnImages()
    }
    
    func setTagOnImages(){
        
        guard let objFeeds = self.objFeeds, objFeeds.arrPhotoInfo.count > 0 else{return}
        
        for objPhotoInfo in objFeeds.arrPhotoInfo {
            
            if objPhotoInfo.arrTags.count > 0  {
                
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrTags.enumerated() {
                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                    }
                }
                
                self.tagsHidden = true
                objPhotoInfo.arrTagPopovers = arrTagPopovers
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                    }
                }
            }
        }
    }
    
    //MARK: - ScrollView Delegate
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == self.scrollView{
        let pageWidth: CGFloat = scrollView.frame.size.width
            pageControl.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
        }
    }
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
//        if scrollView == self.scrollView {
//         return scrollView.subviews[pageControl.currentPage]
//        }
//        return nil
//    }
    
    // MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        //dismiss(animated: true, completion: nil)
    }
}

// MARK: - EBPhotoView and EBTagPopover Delegate
extension showImagesVC {
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
       // delegate.didReceiveSingleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveDoubleTap doubleTap: UITapGestureRecognizer!) {
        //delegate.didReceiveDoubleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveLongPress longPress: UILongPressGestureRecognizer!) {
        self.setTagsHidden(!tagsHidden)
        //delegate.didReceiveLondPressAt(indexPath: indexPath, feedsTableCell: self)
    }
}


// MARK: - EBTagPopover Delegate
extension showImagesVC : EBTagPopoverDelegate {
    
    func tagPopoverDidEndEditing(_ tagPopover: EBTagPopover!) {
        
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        
        guard let objFeeds = self.objFeeds, pageControl.currentPage < objFeeds.arrPhotoInfo.count else {
            return
        }
        
        let objPhotoInfo = objFeeds.arrPhotoInfo[pageControl.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            
            if tagPopover.text() == objTagPopover.text() {
                let objTagInfo = objPhotoInfo.arrTags[index]
                let dict = objTagInfo.metaData
                print("tagPopover dict = \(dict)")
                //NO redirection from ShowImage
                //self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
            }
        }
    }
    
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = 0
            //Get my Id
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }
            
            if myId == tagId {
      
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                self.gotoProfileVC()
            }
        }
    }
    
    func gotoProfileVC (){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
}


// MARK: - Tag Hide/Show
extension showImagesVC {
    
    func setTagsHidden(_ tagsHidden: Bool) {
        tagsHidden ? hideTags() : showTags()
    }
    
    func showTags() {
        setTagsAsHidden(false)
    }
    
    func hideTags() {
        setTagsAsHidden(true)
    }
    
    func setTagsAsHidden(_ hidden: Bool) {
        
        guard let objFeeds = self.objFeeds,  objFeeds.arrPhotoInfo.count > 0   else{return}
        
        tagsHidden = hidden
        let alpha: CGFloat = hidden ? 0 : 1
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            
            for objPhotoInfo in objFeeds.arrPhotoInfo {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
            
        }) { finished in
            
            for objPhotoInfo in objFeeds.arrPhotoInfo{
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
        }
    }
}





