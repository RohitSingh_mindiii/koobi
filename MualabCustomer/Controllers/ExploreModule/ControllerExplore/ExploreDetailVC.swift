//
//  ExploreDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 03/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SDWebImage
import Kingfisher

class ExploreDetailVC : UIViewController {
    
    @IBOutlet weak var lblHeaderName: UILabel!
    @IBOutlet weak var lblNoResults: UIView!
    
    var sharedDict : [String : Any]?
    var dictParam:[String:Any] = [:]
    @IBOutlet weak var vwDataContainer: UIView!
    @IBOutlet weak var postCollection: UICollectionView!
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var arrFeedsData = [feeds]()
    
    let pageSize = 20 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.skyBlueNewTheam
        return refreshControl
    }()
}


extension ExploreDetailVC {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        loadFeedsWithPageCount(page: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
//    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        URLCache.shared.removeAllCachedResponses()
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadFeedsWithPageCount(page: 0)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("ExploreDetailVC")
//        print(SDImageCache.shared().getSize())
//    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        URLCache.shared.removeAllCachedResponses()
//        print(SDImageCache.shared().getSize())
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK: - IBActions
extension ExploreDetailVC {
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
}

// MARK:- Custom Methods
extension ExploreDetailVC {
    
    func configureView(){
        
        self.postCollection.delegate = self
        self.postCollection.dataSource = self
        self.postCollection.addSubview(self.refreshControl)
        self.lblHeaderName.text = "Detail"
        
        if let dict = sharedDict {
            if let strTabType = dict["tabType"] as? String, strTabType == "hasTag" {
                if let str = dict["title"] as? String {
                    self.lblHeaderName.text = "#" + str
                }
            }else{
                if let str = dict["title"] as? String {
                    self.lblHeaderName.text = str
                }
            }
        }
    }
}

// MARK: - collection view delegate and data source
extension ExploreDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeedsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if self.arrFeedsData.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                loadFeedsWithPageCount(page: nextPage)
            }
        }
        
        if let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
            indexPath) as? ExploreCollectionCell){
            
            cell.imgVwPost.layer.cornerRadius = 0
            cell.imgVwPost.layer.masksToBounds = true
            
//            cell.imgVwPost.layer.borderWidth = 0.5
//            cell.imgVwPost.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
            
            
            cell.imgVwPlay?.isHidden = true
            //cell.imgVwPost.image = UIImage(named: "gallery_placeholder")
            
            let objFeeds = self.arrFeedsData[indexPath.row]
            
            if objFeeds.feedType == "video"{
                if objFeeds.arrFeed.count > 0 {
                    if objFeeds.arrFeed[0].videoThumb != "" {
                        
                        if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                            //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                        }
                    }
                    cell.imgVwPlay?.isHidden = false
                }
            }else if objFeeds.feedType == "image"{
                
                if objFeeds.arrFeed.count > 0 {
                    
                    if objFeeds.arrFeed[0].feedPost != "" {
                        
                        if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                            
                            //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            /*let processor = DownsamplingImageProcessor(size: cell.imgVwPost.frame.size)
                                >> RoundCornerImageProcessor(cornerRadius: 0)
                            cell.imgVwPost.kf.indicatorType = .activity
                            cell.imgVwPost.kf.setImage(
                                with: url,
                                placeholder: UIImage(named: "gallery_placeholder"),
                                options: [
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.7)),
                                    .cacheOriginalImage
                                ])
                            {
                                result in
                                switch result {
                                case .success(let value):
                                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                    
                                case .failure(let error):
                                    //cell.imgVwPost.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                                    //cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                    let processor = DownsamplingImageProcessor(size: cell.imgVwPost.frame.size)
                                    >> RoundCornerImageProcessor(cornerRadius: 0)
                                    cell.imgVwPost.kf.indicatorType = .activity
                                    cell.imgVwPost.kf.setImage(
                                    with: url,
                                    placeholder: UIImage(named: "gallery_placeholder"),
                                    options: [
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.7)),
                                    .cacheOriginalImage
                                    ])
                                    {
                                    result in
                                    switch result {
                                    case .success(let value):
                                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                    
                                    case .failure(let error):
                                       cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                       print("Job failed: \(error.localizedDescription)")
                                    }
                                    }
                                       print("Job failed: \(error.localizedDescription)")
                                }
                            }
                            */
                        }
                    }
                }
            }
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let objFeeds = self.arrFeedsData[indexPath.row]
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            
            objVC.objFeeds = objFeeds
            objVC.dictParam = self.dictParam
            objAppShareData.isExploreGridToAllFeed = true
           objAppShareData.arrFeedsForArtistData.removeAll()
            objAppShareData.arrFeedsForArtistData = self.arrFeedsData
            objVC.arrFeedsLocal = self.arrFeedsData
            objVC.indexToScroll = indexPath.item

            navigationController?.pushViewController(objVC, animated: true)
        }
        /*
        let sb = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objDetail = sb.instantiateViewController(withIdentifier:"NewFeedDetailVC") as? NewFeedDetailVC{
            objAppShareData.arrFeedsForArtistData.removeAll()
            objAppShareData.arrFeedsForArtistData.append(objFeeds)
            self.navigationController?.pushViewController(objDetail, animated: true)
        }*/
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellDimension = CGFloat((collectionView.frame.size.width - 3)/3.0)
        return CGSize(width: cellDimension, height: cellDimension)
    }
}

// MARK: - Webservices call

extension ExploreDetailVC {
    
    func loadFeedsWithPageCount(page: Int) {
        
        lastLoadedPage = page
        self.pageNo = page
        
        /*
         type - "user" for TOP and PEOPLE tab
              - "hasTag" for HASHTAG tab
              - "" for SERVICETAG tab is under development
              - "place" for LOCATION tab
        */
        
        /*
         place - placeName for LOCATION tab
         - "" for all other Tab
        */
        
        /*
         tagId - id for HASHTAG tab
         - "" for all other Tab
         */
        
        var strSearchUserData : String = ""
        var strType : String = ""
     
        
        if let dict = sharedDict {
            
            if let strTabType = dict["tabType"] as? String, strTabType == "place" {
                 strSearchUserData = dict["title"] as? String ?? ""
                 strType = "place"
            }
            
            if let strTabType = dict["tabType"] as? String, strTabType == "hasTag" {
                strType = "hasTag"
                strSearchUserData = dict["title"] as? String ?? ""
                if strSearchUserData.hasPrefix("#"){
                    strSearchUserData = strSearchUserData.replacingOccurrences(of: "#", with: "")
                }
            }
            
            if let strTabType = dict["tabType"] as? String, strTabType == "top" ||  strTabType == "people" {
               
                strType = "user"
                if let id = dict["tagId"] as? Int {
                    strSearchUserData = "\(id)"
                }
            }
        }
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let dicParam = ["feedType": "", // "" for all tab
            "search": "", // "" for all tab
            "page": self.pageNo,
            "limit": "20",
            "type": strType,
            "userId":strUserId,
            "findData": strSearchUserData,
            ] as [String : Any]
        self.dictParam = dicParam
        callWebserviceFor_getFeeds(dicParam: dicParam)
    }
    
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
            objWebserviceManager.StartIndicator()
        }
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        if self.sharedDict!["tabType"] as? String == "top"{
            parameters["peopleType"] = "top"
        }
        objWebserviceManager.requestPostForJson(strURL: WebURL.userFeed, params: parameters , success: { [weak self] response in
            print(response)
            self?.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            
            if self?.pageNo == 0 {
                self?.arrFeedsData.removeAll()
                self?.postCollection.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                
                if let totalCount = response["total"] as? Int{
                    self?.totalCount = totalCount
                }
                
                if let arrDict = response["AllUserFeeds"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        for dict in arrDict{
                            let obj = feeds.init(dict: dict)
                            self?.arrFeedsData.append(obj!)
                        }
                    }
                }
                
            }else{
                
                if strSucessStatus == "fail"{
                
                }else{
                  
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                    }
                }
            }
            
            self?.postCollection.reloadData()
            if self?.arrFeedsData.count==0{
                self?.lblNoResults.isHidden = false
            }else{
                self?.lblNoResults.isHidden = true
            }
            
        }) { [weak self] (error) in
            
            self?.postCollection.reloadData()
            if self?.arrFeedsData.count==0{
                self?.lblNoResults.isHidden = false
            }else{
                self?.lblNoResults.isHidden = true
            }
            
            self?.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
}

