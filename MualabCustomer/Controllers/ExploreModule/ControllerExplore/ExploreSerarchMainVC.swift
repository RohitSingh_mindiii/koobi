//
//  ExploreSerarchMainVC.swift
//  MualabCustomer
//
//  Created by Mac on 26/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
import SDWebImage
/*
 At ButtonBarPagerTabStripViewController set
 cell.label.tag = indexPath.row
*/

class ExploreSerarchMainVC: ButtonBarPagerTabStripViewController {
    
    var arrVC = [TopViewController]()
    
    @IBOutlet weak var searchView: ShadowView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    fileprivate var strSearchValue : String = ""
    
    var selectedTabValue : Int = 0
    var isKeyBoardOpen : Bool = true
    
    override func viewDidLoad() {
        self.configurePagerTabs()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText), name: NSNotification.Name(rawValue: "showSearchText"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(animated)
//    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        //SDImageCache.removeImage()
//        URLCache.shared.removeAllCachedResponses()
        
        self.txtSearch.becomeFirstResponder()
        self.btnSearch.isHidden = true
        if selectedTabValue == 1{
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshPeopleList"), object: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        //SDImageCache.removeImage()
        URLCache.shared.removeAllCachedResponses()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        self.arrVC.removeAll()
        
        if let objVC = sb.instantiateViewController(withIdentifier:"TopViewController") as? TopViewController{
            objVC.itemInfo = IndicatorInfo(title: "Top")
            objVC.tabType = "top"
            arrVC.append(objVC)
        }
        
        if let objVC = sb.instantiateViewController(withIdentifier:"TopViewController") as? TopViewController{
            objVC.itemInfo = IndicatorInfo(title: "People")
            objVC.tabType = "people"
            
            arrVC.append(objVC)
        }
        
        if let objVC = sb.instantiateViewController(withIdentifier:"TopViewController") as? TopViewController{
            objVC.itemInfo = IndicatorInfo(title: "Hashtag")
            objVC.tabType = "hasTag"
           
            arrVC.append(objVC)
        }
        
        if let objVC = sb.instantiateViewController(withIdentifier:"TopViewController") as? TopViewController{
            objVC.itemInfo = IndicatorInfo(title: "Service Tag")
            objVC.tabType = "serviceTag"
            
            //arrVC.append(objVC)
        }
        
        if let objVC = sb.instantiateViewController(withIdentifier:"TopViewController") as? TopViewController{
            objVC.itemInfo = IndicatorInfo(title: "Location")
            objVC.tabType = "place"
            arrVC.append(objVC)
        }
        
        return arrVC
    }
}

// MARK:- Custom Methods
extension ExploreSerarchMainVC {
    
}

// MARK:- Custom Pager Methods
extension ExploreSerarchMainVC {
    
    func configurePagerTabs(){
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.clear
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
        settings.style.buttonBarItemFont = UIFont(name: "Nunito-Medium", size:15) ?? UIFont.systemFont(ofSize: 14)
        settings.style.selectedBarHeight = 1.5
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
       settings.style.buttonBarItemsShouldFillAvailableWidth = true
        
        //settings.style.buttonBarLeftContentInset = 10
        //settings.style.buttonBarRightContentInset = 10
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
          
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
            if let tag = newCell?.label.tag {
               self.selectedTabValue = tag
                let dict = ["tag":tag];
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTabType"), object: dict)
            }
        }
        super.viewDidLoad()
    }
}

// MARK:- Button Actions
extension ExploreSerarchMainVC {
    
    @IBAction func btnBackAction(_ sender: Any){
        ////
        objAppShareData.isFromServiceTagBook = true
        ////
        objAppShareData.strExpSearchText = ""
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        btnSearch.isHidden = true
        txtSearch.becomeFirstResponder()
    }
}

// MARK: - UITextfield Delegate
extension ExploreSerarchMainVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            getLikesUsersData(0, andSearchText: "")
        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    @objc func showSearchText(){
        if (self.txtSearch.text?.count == 0){
            btnSearch.isHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString
        if (text.length == 0){
            btnSearch.isHidden = false
        }
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        
        getLikesUsersData(0, andSearchText: "")
        return true
    }
  
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            strSearchValue = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
           // getLikesUsersData(0, andSearchText: substring)
        }
    }
    
    @objc func reload() {
        getLikesUsersData(0, andSearchText: strSearchValue)
    }
}

// MARK:- Save data
extension ExploreSerarchMainVC{
    
    func getLikesUsersData(_ pageNo: Int, andSearchText strText: String) {
        
        var strSeletedTabName = ""
      
        switch (self.selectedTabValue){
            
            case 0 :
                
                strSeletedTabName = "top"
                break;
            case 1 :
               
                strSeletedTabName = "people"
                break;
            case 2 :
                
                strSeletedTabName = "hasTag"
                break;
            case 3 :
               
                //strSeletedTabName = "serviceTag"
                strSeletedTabName = "place"
                break;
            case 4 :
       
                strSeletedTabName = "place"
                break;
            default:
                break;
        }
        
        let strSearchText = strText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        objAppShareData.strExpSearchText = strSearchText.lowercased()
        let dicParam = [
                         "tabType": strSeletedTabName,
                         "search": strSearchText.lowercased()
                        ] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName.searchText), object: nil, userInfo: dicParam)
    }
}

