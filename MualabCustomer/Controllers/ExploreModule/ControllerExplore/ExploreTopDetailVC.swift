//
//  ExploreDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 03/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SDWebImage
import Kingfisher

class ExploreTopDetailVC : UIViewController {
    
    @IBOutlet weak var lblHeaderName: UILabel!
    @IBOutlet weak var lblNoResults: UIView!
    
    var sharedDict : [String : Any]?
    var dictParam:[String:Any] = [:]
    @IBOutlet weak var vwDataContainer: UIView!
    @IBOutlet weak var postCollection: UICollectionView!
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var arrFeedsData = [feeds]()
    
    let pageSize = 10 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.skyBlueNewTheam
        return refreshControl
    }()
}


extension ExploreTopDetailVC {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        loadFeedsWithPageCount(page: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadFeedsWithPageCount(page: 0)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.arrFeedsData.count>0{
            //DispatchQueue.main.async{
                self.postCollection.reloadData()
            //}
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK: - IBActions
extension ExploreTopDetailVC {
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
}

// MARK:- Custom Methods
extension ExploreTopDetailVC {
    
    func configureView(){
        
        self.postCollection.delegate = self
        self.postCollection.dataSource = self
        self.postCollection.addSubview(self.refreshControl)
        self.lblHeaderName.text = "Detail"
        
        if let dict = sharedDict {
            if let strTabType = dict["tabType"] as? String, strTabType == "hasTag" {
                if let str = dict["title"] as? String {
                    self.lblHeaderName.text = "#" + str
                }
            }else{
                if let str = dict["title"] as? String {
                    self.lblHeaderName.text = str
                }
            }
        }
    }
}

/*
// MARK: - collection view delegate and data source
extension ExploreTopDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeedsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if self.arrFeedsData.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                loadFeedsWithPageCount(page: nextPage)
            }
        }
        
        if let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
            indexPath) as? ExploreCollectionCell){
            
            cell.imgVwPost.layer.cornerRadius = 2
            cell.imgVwPost.layer.masksToBounds = true
            
//            cell.imgVwPost.layer.borderWidth = 0.5
//            cell.imgVwPost.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
            
            
            cell.imgVwPlay?.isHidden = true
            cell.imgVwPost.image = UIImage(named: "gallery_placeholder")
            
            let objFeeds = self.arrFeedsData[indexPath.row]
            
            if objFeeds.feedType == "video"{
                if objFeeds.arrFeed.count > 0 {
                    if objFeeds.arrFeed[0].videoThumb != "" {
                        
                        if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                            //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            
                            cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                        }
                    }
                    cell.imgVwPlay?.isHidden = false
                }
            }else if objFeeds.feedType == "image"{
                
                if objFeeds.arrFeed.count > 0 {
                    
                    if objFeeds.arrFeed[0].feedPost != "" {
                        
                        if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                            
                            //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                        }
                    }
                }
            }
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let objFeeds = self.arrFeedsData[indexPath.row]
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            
            objVC.objFeeds = objFeeds
            objAppShareData.isExploreGridToAllFeed = true
           objAppShareData.arrFeedsForArtistData.removeAll()
            objAppShareData.arrFeedsForArtistData = self.arrFeedsData
            objVC.arrFeedsLocal = self.arrFeedsData
            objVC.indexToScroll = indexPath.item

            navigationController?.pushViewController(objVC, animated: true)
        }
        /*
        let sb = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objDetail = sb.instantiateViewController(withIdentifier:"NewFeedDetailVC") as? NewFeedDetailVC{
            objAppShareData.arrFeedsForArtistData.removeAll()
            objAppShareData.arrFeedsForArtistData.append(objFeeds)
            self.navigationController?.pushViewController(objDetail, animated: true)
        }*/
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellDimension = CGFloat((collectionView.frame.size.width - 6)/3.0)
        return CGSize(width: cellDimension, height: cellDimension)
    }
}
*/

// MARK: - Webservices call

extension ExploreTopDetailVC {
    
    func loadFeedsWithPageCount(page: Int) {
        
        lastLoadedPage = page
        self.pageNo = page
        
        /*
         type - "user" for TOP and PEOPLE tab
              - "hasTag" for HASHTAG tab
              - "" for SERVICETAG tab is under development
              - "place" for LOCATION tab
        */
        
        /*
         place - placeName for LOCATION tab
         - "" for all other Tab
        */
        
        /*
         tagId - id for HASHTAG tab
         - "" for all other Tab
         */
        
        var strSearchUserData : String = ""
        var strType : String = ""
     
        
        if let dict = sharedDict {
            
            if let strTabType = dict["tabType"] as? String, strTabType == "place" {
                 strSearchUserData = dict["title"] as? String ?? ""
                 strType = "place"
            }
            
            if let strTabType = dict["tabType"] as? String, strTabType == "hasTag" {
                strType = "hasTag"
                strSearchUserData = dict["title"] as? String ?? ""
                if strSearchUserData.hasPrefix("#"){
                    strSearchUserData = strSearchUserData.replacingOccurrences(of: "#", with: "")
                }
            }
            
            if let strTabType = dict["tabType"] as? String, strTabType == "top" ||  strTabType == "people" {
               
                strType = "user"
                if let id = dict["tagId"] as? Int {
                    strSearchUserData = "\(id)"
                }
            }
        }
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let dicParam = ["feedType": "", // "" for all tab
            "search": "", // "" for all tab
            "page": self.pageNo,
            "limit": "10",
            "type": strType,
            "userId":strUserId,
            "findData": strSearchUserData,
            ] as [String : Any]
        self.dictParam = dicParam
//        var dicParam = [ "search": strSearchText,
//                         "page": self.pageNo,
//                         "limit": "15",
//                         "type": tabType ,
//                         "userId":strUserId ] as [String : Any]
        callWebserviceFor_getFeeds(dicParam: dicParam)
    }
    
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
            objWebserviceManager.StartIndicator()
        }
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        parameters["peopleType"] = "top"
        objWebserviceManager.requestPostForJson(strURL: WebURL.userFeed, params: parameters , success: { response in
            print(response)
            self.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            
            if self.pageNo == 0 {
                self.arrFeedsData.removeAll()
                self.postCollection.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                
                if let totalCount = response["total"] as? Int{
                    self.totalCount = totalCount
                }
                
                if let arrDict = response["AllUserFeeds"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        for dict in arrDict{
                            let obj = feeds.init(dict: dict)
//                            for objPhoto in obj!.arrPhotoInfo{
//                                for objTag in objPhoto.arrTags{
//                                    let objTagService = SubSubService.init(dict: [:])
//                                    //objTagService.serviceId =
//                                }
//                            }
                            

        //// For Category only shows with service tag
        for objPhoto in (obj?.arrPhotoInfo)!{
        for objTag in objPhoto.arrTags{
            if objTag.strTagType == "service"{
                let dict = objTag.metaData
                print(dict)
                //[AnyHashable("staffInCallPrice"): "25.2", AnyHashable("categoryId"): 21, AnyHashable("businessTypeId"): 2, AnyHashable("tagId"): 7, AnyHashable("isOutCall"): "0", AnyHashable("incallPrice"): "25.2", AnyHashable("staffOutCallPrice"): "50.3", AnyHashable("staffId"): 17, AnyHashable("outcallPrice"): "50.3", AnyHashable("artistId"): "7"]
                var catId = dict["categoryId"] as? Int ?? 0
                if catId == 0{
                   catId = Int(dict["categoryId"] as? String ?? "0")!
                }
                let objTagService = SubSubService.init(dict: [:])
                objTagService.serviceId = dict["businessTypeId"] as? Int ?? 0
                if let id =  dict["businessTypeId"] as? String {
                    objTagService.serviceId = Int(id) ?? 0
                }
                objTagService.subServiceId = dict["categoryId"] as? Int ?? 0
                if let id =  dict["categoryId"] as? String{
                    objTagService.subServiceId = Int(id) ?? 0
                }
                objTagService.subSubServiceId = dict["tagId"] as? Int ?? 0
                if let id =  dict["tagId"] as? String{
                    objTagService.subSubServiceId = Int(id) ?? 0
                }
                objTagService.staffIdForEdit = dict["staffId"] as? Int ?? 0
                if let id =  dict["staffId"] as? String {
                    objTagService.staffIdForEdit = Int(id) ?? 0
                }
                objTagService.artistId = dict["artistId"] as? Int ?? 0
                if let id =  dict["artistId"] as? String {
                    objTagService.artistId = Int(id) ?? 0
                }
                objTagService.strInCallPrice = dict["staffInCallPrice"] as? String ?? ""
                objTagService.strOutCallPrice = dict["staffOutCallPrice"] as? String ?? ""

                if let businessTypeName =  dict["businessTypeName"] as? String {
                    objTagService.serviceName = businessTypeName
                }
                if let categoryName =  dict["categoryName"] as? String {
                    objTagService.subServiceName = categoryName
                }
                if let title =  dict["title"] as? String {
                    objTagService.subSubServiceName = title
                }
                    obj?.arrTagedServices.append(objTagService)
                }
                }
            }
                        
                            if (obj?.arrTagedServices.count)!>0{
                               self.arrFeedsData.append(obj!)
                            }
                            //self.arrFeedsData.append(obj!)
                    }
                    }
                }
                
            }else{
                if strSucessStatus == "fail"{
                }else{
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
            DispatchQueue.main.async{
            self.postCollection.reloadData()
            }
            if self.arrFeedsData.count==0{
                self.lblNoResults.isHidden = false
            }else{
                self.lblNoResults.isHidden = true
            }
            
        }) { (error) in
            
            self.postCollection.reloadData()
            if self.arrFeedsData.count==0{
                self.lblNoResults.isHidden = false
            }else{
                self.lblNoResults.isHidden = true
            }
            
            self.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

// MARK: - Collection view delegate and data source
extension ExploreTopDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            if self.arrFeedsData.count>0{
                return CGSize(width: 0.0, height: 0.0)
            }else{
                return CGSize(width: 0.0, height: 0.0)
            }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeedsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
            if self.arrFeedsData.count < self.totalCount {
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                    loadFeedsWithPageCount(page: nextPage)
                }
            }
            
            if let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
                indexPath) as? ExploreCollectionCell){
                
                //cell.vwBg.layer.cornerRadius = 3
                //cell.vwBg.layer.masksToBounds = true
                
                let objFeeds = self.arrFeedsData[indexPath.row]
                cell.btnCategoryFromGrid.tag = indexPath.row
                cell.lblCategoryName.text = objFeeds.tagCategoryName
                cell.lblServiceName.text = objFeeds.tagServiceName
                
                ////
                cell.indexPath = indexPath
                //DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                cell.setDataInCollection(obj: objFeeds)
                //}
                ////
                
                if objFeeds.feedType == "video"{
                    if objFeeds.arrFeed.count > 0 {
                        if objFeeds.arrFeed[0].videoThumb != "" {
                            
                            if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                                
                                //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            }
                        }
                        cell.imgVwPlay?.isHidden = false
                    }
               
                }else if objFeeds.feedType == "image"{
                    if objFeeds.arrFeed.count > 0 {
                        if objFeeds.arrFeed[0].feedPost != "" {
                            if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                                //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                 cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                /*let processor = DownsamplingImageProcessor(size: cell.imgVwPost.frame.size)
                                    >> RoundCornerImageProcessor(cornerRadius: 0)
                                cell.imgVwPost.kf.indicatorType = .activity
                                cell.imgVwPost.kf.setImage(
                                    with: url,
                                    placeholder: UIImage(named: "gallery_placeholder"),
                                    options: [
                                        .processor(processor),
                                        .scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(0.7)),
                                        .cacheOriginalImage
                                    ])
                                {
                                    result in
                                    switch result {
                                    case .success(let value):
                                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                        
                                    case .failure(let error):
                                        //cell.imgVwPost.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                                        cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                        print("Job failed: \(error.localizedDescription)")
                                    }
                                }
                                */
                            }
                        }
                    }
                }
                return cell
            }else{
                return UICollectionViewCell()
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let objFeeds = self.arrFeedsData[indexPath.row]
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            
            objVC.objFeeds = objFeeds
            objVC.dictParam = self.dictParam
            objAppShareData.isExploreGridToAllFeed = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            objAppShareData.arrFeedsForArtistData = self.arrFeedsData
            objVC.arrFeedsLocal = self.arrFeedsData
            objVC.indexToScroll = indexPath.item
            
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let cellDimension = CGFloat((collectionView.frame.size.width - 2)/2)
            return CGSize(width: cellDimension, height: cellDimension+47)
    }
    
    @IBAction func btnServiceInListAction(_ sender: UIButton) {
        print(sender.superview!.tag)
        print(sender.tag)
        let objArtist = self.arrFeedsData[sender.superview!.tag]
        let objService = objArtist.arrTagedServices[sender.tag]
        
        objAppShareData.selectedOtherIdForProfile = objService.artistId
        var strIncallOrOutCall = ""
        if objService.strInCallPrice.count == 0 || objService.strInCallPrice == "0" || objService.strInCallPrice == "0.0"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        
        objAppShareData.isFromServiceTagBook = true
        objAppShareData.isBookingFromService = true
        objAppShareData.objServiceForEditBooking = objService
        
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}
