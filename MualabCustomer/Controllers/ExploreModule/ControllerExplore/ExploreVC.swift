//
//  ExploreVC.swift
//  MualabCustomer
//
//  Created by Mac on 17/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit
import Social
import Alamofire
import AlamofireImage
import FirebaseAuth
import TLPhotoPicker
import Photos
import HCSStarRatingView
import GooglePlaces
import Kingfisher

var arrCategoryForUpdatedName = [Service]()

class ExploreVC: UIViewController,AVPlayerViewControllerDelegate {
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var vwDataContainer: UIView!
    @IBOutlet weak var lblNoResults: UIView!
    @IBOutlet weak var imgProfileHeader: UIImageView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblRadiusSlider: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewProfilePic: UIView!
    @IBOutlet weak var viewBackButton: UIView!
    @IBOutlet weak var viewFeedList: UIView!
    @IBOutlet weak var tblFeeds: UITableView!
    @IBOutlet weak var imgBig: UIImageView!
    fileprivate var myId:Int = 0
    @IBOutlet weak var categoryCollection: UICollectionView!
    var headerView: ExploreBigImageHeader?
    @IBOutlet weak var postCollection: UICollectionView!
    @IBOutlet weak var collectionBigImage: UICollectionView!
    fileprivate var pageNo: Int = 1
    @IBOutlet weak var viewNoLocation: UIView!
    fileprivate var totalCount = 0
    fileprivate var scrollToIndex = 0
    fileprivate var arrFeedsData = [feeds]()
    fileprivate var arrCategory = [Service]()
    fileprivate var arrAllCategory = [Service]()
    fileprivate var tblView: UITableView?
    fileprivate var indexLastViewMoreView = 0
    var arrSelectCat : [Int] = []
   
    var arrExploreBusiness : [ExploreBusiness] = [ExploreBusiness]()
    var arrExploreCategory : [ExploreCategory] = [ExploreCategory]()
    var arrExploreService : [ExploreService] = [ExploreService]()
    var arrExploreSearchCategory : [ExploreCategory] = [ExploreCategory]()
    var arrExploreSearchService : [ExploreService] = [ExploreService]()
    var arrExploreTopCategory : [ExploreCategory] = [ExploreCategory]()
    var arrSelCatForLocal = [Int]()
    var arrSelServiceForLocal = [Int]()
    var arrCatSelectGlobal = [SubSubService]()
    //let pageSize = 20 // that's up to you, really
    let pageSize = 20 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    var isMilesApiCalling = false
    
    @IBOutlet weak var btnImages: UIButton!
    @IBOutlet weak var btnVideos: UIButton!
    
    //// Filter PopUp
    var strText = ""
    var selectedTable = -1
    var isOutCallSelected:Bool = false
    var isInCallSelected:Bool = false
    var strLatitude : String =  ""
    var strLongitude : String = ""
    var isGoForLocation : Bool = false
    var arrAllServices : [Service] = [Service]()
    var arrSubService : [SubService] = [SubService]()
    var arrFilterAllServices : [Service] = [Service]()
    var arrFilterSubService : [SubService] = [SubService]()
    
    @IBOutlet weak var imgVwInCall: UIImageView!
    @IBOutlet weak var imgVwOutCall: UIImageView!
    @IBOutlet weak var viewFilterPopUp: UIView!
    @IBOutlet weak var vwTableCointaner: UIView!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var txtTableSearch: UITextField!
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var tblBookingList: UITableView!
    @IBOutlet weak var viewBusinessTypeNew: UIView!
    @IBOutlet weak var viewServiceTypeNew: UIView!
    @IBOutlet weak var viewLocationNew: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var viewRadiusChoose: UIView!
    @IBOutlet weak var viewLabelRadiusMiles: UIView!
    @IBOutlet weak var viewSlideRadiusMile: UIView!
    @IBOutlet weak var lblMileDistance: UILabel!
    @IBOutlet weak var sliderRadius: customSlider!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    ////
    let imgCircleSelected : UIImage = UIImage.init(named: "active_check_box_icon")!
    let imgCircleUnselected : UIImage = UIImage.init(named: "inactive_check_box_icon")!
    
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.logout()
    }
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.viewNoLocation.isHidden = true
        //self.callWebserviceForGetArtistServices()
        objWebserviceManager.StartIndicator()
        self.addGesturesToMainView()
        self.lblLocation.text = objLocationManager.strAddress
        let lat = objLocationManager.strlatitude
        let long = objLocationManager.strlongitude
        self.strLatitude = lat ?? ""
        self.strLongitude = long ?? ""
        //loadFeedsWithPageCount(page: 0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        URLCache.shared.removeAllCachedResponses()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.vwTableCointaner.isHidden = true
//        SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        URLCache.shared.removeAllCachedResponses()
        
        ImageLoad()
        self.pageNo = 1
        if objAppShareData.isTabChange{
            if !isGoForLocation{
                if objAppShareData.isFromServiceTagBook{
                    objAppShareData.isFromServiceTagBook = false
                }else{
                    self.isInCallSelected = false
                    self.isOutCallSelected = false
                    self.setOutCallUI()
                    self.arrSelectCat.removeAll()
                    self.arrCatSelectGlobal.removeAll()
                    self.callWebserviceFor_getAllCategoriesNew()
                    self.viewFilterPopUp.isHidden = true
                    self.vwTableCointaner.isHidden = true
                    let btn = UIButton()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                      self.btnFilterResetAction(btn)
                    }
                }
            }else{
                isGoForLocation = false
            }
        }else{
            if !isGoForLocation{
                if objAppShareData.isFromServiceTagBook{
                    objAppShareData.isFromServiceTagBook = false
                }else{
                self.isInCallSelected = false
                self.isOutCallSelected = false
                self.setOutCallUI()
                self.arrSelectCat.removeAll()
                self.arrCatSelectGlobal.removeAll()
                self.viewFilterPopUp.isHidden = true
                self.vwTableCointaner.isHidden = true
                self.callWebserviceFor_getAllCategoriesNew()
                }
            }else{
                isGoForLocation = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
               self.loadFeedsWithPageCount(page: 1)
            }
        }
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadFeedsWithPageCount(page: 1)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK: - IBActions
extension ExploreVC {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if SDImageCache.shared().getSize() >= 8040260{
//            SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//            SDImageCache.shared().clearMemory()
//            SDImageCache.shared().clearDisk()
//            URLCache.shared.removeAllCachedResponses()
//        }
    }
    
    @IBAction func btnCategoryFromGridAction(_ sender: UIButton) {
        let obj = self.arrFeedsData[sender.tag]
        self.redirectionFromServiceTag(obj:obj)
    }
    
    func redirectionFromServiceTag(obj:feeds){
        print("redirectionFromServiceTag")
        objAppShareData.isFromServiceTag = true
        objAppShareData.selectedOtherIdForProfile = obj.tagArtistId
        let strInCallPrice = obj.tagStrIncallPrice
        let strOutCallPrice = obj.tagStrOutcallPrice
        var strIncallOrOutCall = ""
        if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        let objService = SubSubService.init(dict: [:])
        objService.artistId = objAppShareData.selectedOtherIdForProfile
        objService.serviceId = obj.tagBusinessTypeId
        objService.subServiceId = obj.tagCategoryId
        objService.subSubServiceId = obj.tagServiceId
        objService.staffIdForEdit = obj.tagStaffId
        
        objAppShareData.isFromServiceTagBook = true
        objAppShareData.isBookingFromService = true
        objAppShareData.objServiceForEditBooking = objService
        
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnOutCallAction(_ sender: UIButton){
        self.view.endEditing(true)
        if sender.tag == 0{
           isInCallSelected = !isInCallSelected
        }else{
            isOutCallSelected = !isOutCallSelected
        }
        setOutCallUI()
        ////
        self.arrExploreCategory.removeAll()
        self.arrExploreTopCategory.removeAll()
        self.arrExploreService.removeAll()
        self.arrSelCatForLocal.removeAll()
        self.arrSelServiceForLocal.removeAll()
        self.viewServiceTypeNew.isHidden = true
        self.viewBusinessTypeNew.isHidden = true
        self.lblBusinessType.text = "Service Category"
        self.lblServiceType.text = "Services"
        ////
        ////
        for objC in self.arrExploreTopCategory{
            objC.isSelected = false
            objC.isSelectLocal = false
        }
        self.arrSelCatForLocal.removeAll()
        self.arrSelServiceForLocal.removeAll()
        self.arrExploreService.removeAll()
        self.arrExploreBusiness.removeAll()
        self.arrExploreCategory.removeAll()
        self.arrFilterAllServices.removeAll()
        self.arrFilterAllServices = self.arrAllServices
        self.arrCatSelectGlobal.removeAll()
        self.arrSelectCat.removeAll()
        self.callWebserviceFor_getAllCategoriesNew()
        ////
    }
    @IBAction func btnSettingsAction(_ sender: UIButton) {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url, options: [:]
            , completionHandler: nil)
    }
    @IBAction func btnFilterAction(_ sender: Any) {
        self.tblBookingList.reloadData()
        if self.viewFilterPopUp.isHidden {
        }else{
            self.viewFilterPopUp.isHidden = true
            self.categoryCollection.reloadData()
            pageNo = 1
            self.loadFeedsWithPageCount(page: 1)
            return
        }
        let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
        if newArr.count==0{
        }else{
//            for obj in newArr{
//                self.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
//            }
        }
        var arrBusinessData = [String]()
        var arrServiceData = [String]()
        if self.arrExploreCategory.count > 0{
            self.arrExploreService.removeAll()
            for objArrAll in self.arrExploreCategory{
                if objArrAll.isSelected == false{
                }else{
                    objArrAll.isSelectLocal = true
                    objArrAll.isSelected = true
                    arrBusinessData.append(objArrAll.categoryName)
                    self.arrExploreService.append(contentsOf: objArrAll.arrService.clone())
                }
            }
            if arrBusinessData.count>0{
                self.viewBusinessTypeNew.isHidden = false
                self.lblBusinessType.text = arrBusinessData.joined(separator: ", ")
            }else{
                self.viewBusinessTypeNew.isHidden = true
                self.lblBusinessType.text = "Service Category"
            }
        }else{
            self.viewServiceTypeNew.isHidden = true
            self.viewBusinessTypeNew.isHidden = true
            self.lblBusinessType.text = "Service Category"
            self.lblServiceType.text = "Services"
        }
        
        if self.arrExploreService.count > 0{
//            for obj in self.arrExploreService{
//                if (self.arrSelServiceForLocal.contains(obj.serviceId)){
//                    obj.isSelected = true
//                }
//            }
            for objArrAll in self.arrExploreService{
                if objArrAll.isSelected == false && !(self.arrSelServiceForLocal.contains(objArrAll.serviceId)) {
                    objArrAll.isSelected = false
                }else{
                    objArrAll.isSelectLocal = true
                    objArrAll.isSelected = true
                    arrServiceData.append(objArrAll.serviceName)
                }
            }
            if arrServiceData.count>0{
                self.viewServiceTypeNew.isHidden = false
                self.lblServiceType.text = arrServiceData.joined(separator: ", ")
            }else{
                self.viewServiceTypeNew.isHidden = true
                self.lblServiceType.text = "Services"
            }
        }else{
            self.viewServiceTypeNew.isHidden = true
            self.lblServiceType.text = "Services"
        }
       
        let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        let thumbImage = textToImage(drawText: "", inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        self.sliderRadius.setThumbImage(thumbImage, for:.normal)
        self.viewFilterPopUp.isHidden = false
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.viewBackButton.isHidden = true
        self.viewFeedList.isHidden = true
        self.viewProfilePic.isHidden = false
        self.postCollection.reloadData()
    }
    @IBAction func btnBigImageAction(_ sender: Any) {
        if self.arrFeedsData.count>0{
            scrollToIndex = 0
            self.openFeedList()
        }
    }
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnLogoutAction(_ sender: Any) {
        self.view.endEditing(true)
        appDelegate.logout()
    }
    
    @IBAction func btnSelectDataType(toView sender: UIButton){
        self.view.endEditing(true)
        if sender.tag == 0 {
            
            if !btnImages.isSelected {
                changeButtonColors()
                self.btnImages.isSelected = true
                loadFeedsWithPageCount(page: 1)
            }
        }else if sender.tag == 1 {
            
            if !btnVideos.isSelected {
                changeButtonColors()
                self.btnVideos.isSelected = true
                loadFeedsWithPageCount(page: 1)
                
            }
        }
    }
    func setOutCallUI(){
        if isOutCallSelected{
            imgVwOutCall.image = imgCircleSelected
        }else{
            imgVwOutCall.image = imgCircleUnselected
        }
        if isInCallSelected{
            imgVwInCall.image = imgCircleSelected
        }else{
            imgVwInCall.image = imgCircleUnselected
        }
    }
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.isOtherSelectedForProfile = false
        objAppShareData.isFromServiceTagBook = true
        self.gotoProfileVC()
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        objAppShareData.strExpSearchText = ""
        arrCategoryForUpdatedName = self.arrCategory
        self.gotoExploreSerarchMainVC()
    }
}

// MARK: - Custom Methods
extension ExploreVC {
    
    func addGesturesToMainView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginTap.delegate = self
        socialLoginTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(socialLoginTap)
    }
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //if indexPath != indexPathOld{
            cellOld.btnSaveToFolder.isHidden = false
            cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.btnHiddenMoreOption.isHidden = true
            cellOld.setDefaultDesign()
        }
        //}
        ////
    }
    
    func ImageLoad(){
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfileHeader.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfileHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfileHeader.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfileHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        objAppShareData.isFromServiceTagBook = true
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
         objVC.hidesBottomBarWhenPushed = true
         navigationController?.pushViewController(objVC, animated: true)
         }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnFeedProfileAction(_ sender: UIButton){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objFeed = self.arrFeedsData[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":(objFeed.userInfo?.userName)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objFeed.userInfo?.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    func configureView(){
        //Get my Id
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            myId = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            myId = userInfo["_id"] as? Int ?? 0
        }
        self.viewBackButton.isHidden = true
        self.viewFeedList.isHidden = true
        self.viewProfilePic.isHidden = false
        self.postCollection.delegate = self
        self.postCollection.dataSource = self
        //self.postCollection.addSubview(self.refreshControl)
        changeButtonColors()
        self.btnImages.isSelected = true
        
        self.viewSlideRadiusMile.isHidden = false
        self.viewLabelRadiusMiles.isHidden = true
        
        self.sliderRadius.minimumValue = 1
        self.sliderRadius.maximumValue = 20
        self.sliderRadius.value = 5.0
        self.sliderRadius.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        let thumbImage = textToImage(drawText: "", inImage: #imageLiteral(resourceName: "green_gubbara_ico_slider") , atPoint: CGPoint.init(x:12,y:-2))
        self.sliderRadius.setThumbImage(thumbImage, for:.normal)
    }
    
    func changeButtonColors(){
        btnImages.isSelected = false
        btnVideos.isSelected = false
    }
    
    func gotoExploreSerarchMainVC(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExploreSerarchMainVC") as? ExploreSerarchMainVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
}

// MARK: - collection view delegate and data source
extension ExploreVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            self.headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ExploreBigImageHeader", for: indexPath) as? ExploreBigImageHeader
            
            if collectionView != categoryCollection && collectionView != self.headerView?.collectionBigImage && self.arrFeedsData.count>0{
                let objFeeds = self.arrFeedsData[0]
                if objFeeds.arrFeed.count > 0 {
                    if objFeeds.arrFeed[0].feedPost != "" {
                        if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                            //headerView?.imgBig.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            let processor = DownsamplingImageProcessor(size: (headerView?.imgBig.frame.size)!)
                                >> RoundCornerImageProcessor(cornerRadius: 0)
                            headerView?.imgBig.kf.indicatorType = .activity
                            headerView?.imgBig.kf.setImage(
                                with: url,
                                placeholder: UIImage(named: "gallery_placeholder"),
                                options: [
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.7)),
                                    .cacheOriginalImage
                                ])
                            {
                                result in
                                switch result {
                                case .success(let value):
                                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                    
                                case .failure(let error):
                                    self.headerView?.imgBig.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                    print("Job failed: \(error.localizedDescription)")
                                }
                            }
                            
                            self.headerView?.arrServices.removeAll()
                            let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
                            if newArr.count>0{
                               self.headerView?.arrServices = self.arrExploreCategory
                            }else{
                              self.headerView?.arrServices = self.arrExploreTopCategory
                            }
                            self.headerView?.collectionBigImage.reloadData()
                            //self.collectionBigImage.reloadData()
                        }
                    }
                }
            }
            return self.headerView!
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == categoryCollection{
            return CGSize(width: 0.0, height: 0.0)
        }else if collectionView == collectionBigImage{
            return CGSize(width: 0.0, height: 0.0)
        }else{
            if self.arrFeedsData.count>0{
                return CGSize(width: collectionView.frame.width, height: (collectionView.frame.width*3/4)-10)
                //return CGSize(width: 0.0, height: 0.0)
            }else{
                return CGSize(width: 0.0, height: 0.0)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollection{
            return self.arrExploreBusiness.count
        }else if collectionView == collectionBigImage{
            return self.arrFeedsData[0].arrTagedServices.count
        }else{
            return self.arrFeedsData.count-1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == categoryCollection{
            
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionCell", for:
                indexPath) as? CalenderCollectionCell)!
            
            let objCategory = self.arrExploreBusiness[indexPath.row]
            cell.lblTime.text = objCategory.businessName
            if objCategory.isSelected {
                cell.vwBg.layer.borderWidth = 0.0
                cell.vwBg.layer.borderColor = UIColor.black.cgColor
                cell.vwBg.backgroundColor = UIColor.init(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
                cell.lblTime.textColor = UIColor.white
                cell.vwBlurr.backgroundColor = UIColor.init(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
            }else{
                cell.vwBg.layer.borderWidth = 1.0
                cell.vwBg.layer.borderColor = UIColor.black.cgColor
                //cell.vwBg.backgroundColor = UIColor.init(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
                cell.vwBg.backgroundColor = UIColor.white
                cell.lblTime.textColor = UIColor.black
                cell.vwBlurr.backgroundColor = UIColor.black
            }
            return cell
        
        }else if collectionView == collectionBigImage{
            let cellIdentifier = "SubsubServicesCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
            let obj = self.arrFeedsData[0].arrTagedServices[indexPath.item]
            cell.btnService.superview?.tag = 0
            cell.btnService.tag = indexPath.row
            if obj.isSelected{
                cell.lblUserName.textColor = UIColor.black
            }else{
                cell.lblUserName.textColor = UIColor.white
            }
            cell.lblUserName.text = obj.subSubServiceName
            let a = cell.lblUserName.layer.frame.height
            cell.lblUserName.layer.cornerRadius = 11
            cell.lblUserName.layer.masksToBounds = true
            cell.lblBorder.layer.cornerRadius = 12
            cell.lblBorder.layer.masksToBounds = true
            return cell
            
        }else{
            
            if self.arrFeedsData.count < self.totalCount {
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                    loadFeedsWithPageCount(page: nextPage)
                }
            }
            
            if let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
                indexPath) as? ExploreCollectionCell){
                
                //cell.vwBg.dropShadow(color: .red, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
                //cell.vwBg.layer.cornerRadius = 3
                
                //cell.vwBg.layer.masksToBounds = true
                
                //                cell.imgVwPost.layer.borderWidth = 0.5
                //                cell.imgVwPost.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
                
                let objFeeds = self.arrFeedsData[indexPath.row+1]
                //let objFeeds = self.arrFeedsData[indexPath.row]
                cell.btnCategoryFromGrid.tag = indexPath.row
                cell.lblCategoryName.text = objFeeds.tagCategoryName
                cell.lblServiceName.text = objFeeds.tagServiceName
                
                ////
                cell.indexPath = indexPath
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    cell.setDataInCollection(obj: objFeeds)
                }
                ////
                
                if objFeeds.feedType == "video"{
                    if objFeeds.arrFeed.count > 0 {
                        if objFeeds.arrFeed[0].videoThumb != "" {
                            
                            if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                                //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                
                                cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            }
                        }
                        cell.imgVwPlay?.isHidden = false
                    }
                
                //}else if objFeeds.feedType == "image"{
                }else{
                   
                    if objFeeds.arrFeed.count > 0 {
                        
                        if objFeeds.arrFeed[0].feedPost != "" {
                            
                            if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                                //cell.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                //cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                let processor = DownsamplingImageProcessor(size: cell.imgVwPost.frame.size)
                                    >> RoundCornerImageProcessor(cornerRadius: 0)
                                cell.imgVwPost.kf.indicatorType = .activity
                                cell.imgVwPost.kf.setImage(
                                    with: url,
                                    placeholder: UIImage(named: "gallery_placeholder"),
                                    options: [
                                        .processor(processor),
                                        .scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(0.7)),
                                        .cacheOriginalImage
                                    ])
                                {
                                    result in
                                    switch result {
                                    case .success(let value):
                                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                        
                                    case .failure(let error):
                                        //cell.imgVwPost.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                                        cell.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                        print("Job failed: \(error.localizedDescription)")
                                    }
                                }
                                
                            }
                        }
                    }
                }
                return cell
            }else{
                return UICollectionViewCell()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == categoryCollection{
            self.arrExploreCategory.removeAll()
            self.arrExploreTopCategory.removeAll()
            self.arrExploreService.removeAll()
            self.arrSelCatForLocal.removeAll()
            self.arrSelServiceForLocal.removeAll()
            let objBusiness = self.arrExploreBusiness[indexPath.row]
            if objBusiness.isSelected{
                objBusiness.isSelected = false
            }else{
                objBusiness.isSelected = true
            }
            let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
            if newArr.count > 0{
                for obj in newArr{
                    self.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
                }
            }else{
                let arr = self.arrExploreBusiness.map { $0.arrCategory }
                print(arr)
                if arr.count>0{
                    for arrCat in arr{
                        self.arrExploreCategory.append(contentsOf: arrCat.clone())
                    }
                }
            }

            if self.arrFeedsData.count>0{
               self.postCollection.contentOffset.y = 0.0
            }
            self.categoryCollection.reloadData()
            pageNo = 1
            self.loadFeedsWithPageCount(page: 1)
        }else{
            scrollToIndex = indexPath.row+1
            self.openFeedList()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == categoryCollection{
            var cellWidth:CGFloat = 0
            var cellHeight:CGFloat = 36
            cellWidth = CGFloat(self.categoryCollection.frame.size.width-2)
            let obj = self.arrExploreBusiness[indexPath.row]
            let a = obj.businessName
            var sizeOfString = CGSize()
            if let font = UIFont(name: "Nunito-Bold", size: 15)
            {
                let finalDate = a
                let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
            }
            //cellWidth = sizeOfString.width+34
            cellWidth = sizeOfString.width+20
            //cellHeight = 40
            cellHeight = 32
            return CGSize(width: cellWidth, height: cellHeight)
            
        }else if collectionView == collectionBigImage{
            var cellWidth:CGFloat = 0
            var cellHeight:CGFloat = 25
            cellWidth = CGFloat(self.collectionBigImage.frame.size.width-4)
            let obj = self.arrFeedsData[0].arrTagedServices[indexPath.item]
            let a = obj.subSubServiceName
            
            var sizeOfString = CGSize()
            if let font = UIFont(name: "Nunito-Regular", size: 12)
            {
                let finalDate = a
                let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
            }
            cellWidth = sizeOfString.width+12
            cellHeight = 25
            
            return CGSize(width: cellWidth, height: cellHeight)
            
        }else{
            let cellDimension = CGFloat((collectionView.frame.size.width - 2)/2)
            //return CGSize(width: cellDimension, height: cellDimension+47)
            return CGSize(width: cellDimension, height: cellDimension)
        }
    }
}

// MARK: - Webservices call

extension ExploreVC {
    
    func openFeedList(){
        self.tblFeeds.reloadData()
        self.viewBackButton.isHidden = false
        self.viewFeedList.isHidden = false
        self.viewProfilePic.isHidden = true
        let indexPath = IndexPath.init(row: scrollToIndex, section: 0)
        self.tblFeeds.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    func loadFeedsWithPageCount(page: Int) {
        
        lastLoadedPage = page
        self.pageNo = page
        
        var strFeedType : String = "image"
        if self.btnVideos.isSelected{
            strFeedType = "video"
        }
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        /*
        var arrServiceTagId = [String]()
        var serviceTagId = ""
        for obj in arrCategory{
            if obj.isSelected{
                arrServiceTagId.append(String(obj.serviceId))
            }
        }
        if arrServiceTagId.count > 0{
            serviceTagId = arrServiceTagId.joined(separator: ",")
        }
        */
        var arrServiceTagId = [String]()
        var businessId = ""
        let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
        if newArr.count > 0{
            let ids = newArr.map { String($0.businessId) }
            print(ids)
            businessId = ids.joined(separator: ",")
        }
        
        /*
        var arrServiceTagIdNew = [String]()
        var serviceTagIdNew = ""
        if self.arrFeedsData.count>0{
            for obj in self.arrFeedsData[0].arrTagedServices{
                if obj.isSelected{
                    //arrServiceTagIdNew.append(String(obj.subSubServiceId))
                    if obj.subServiceId == 0{
                        arrServiceTagIdNew.append(String(obj.subSubServiceId))
                    }else{
                        arrServiceTagIdNew.append(String(obj.subServiceId))
                    }
                }
            }
        }
        if arrServiceTagIdNew.count > 0{
            serviceTagIdNew = arrServiceTagIdNew.joined(separator: ",")
        }
        */
        var arrServiceTagIdNew = [String]()
        var categoryId = ""
        var newArrNN = [ExploreCategory]()
        let new = self.arrExploreBusiness.filter(){ $0.isSelected == true }
        if new.count>0{
           newArrNN = self.arrExploreCategory.filter(){ $0.isSelected == true }
        }else{
          newArrNN = self.arrExploreTopCategory.filter(){ $0.isSelected == true }
        }
        if newArrNN.count > 0{
            let ids = newArrNN.map { String($0.categoryId) }
            print(ids)
            categoryId = ids.joined(separator: ",")
        }
        
        var artistServiceId = ""
        let newArrService = self.arrExploreService.filter(){ $0.isSelected == true }
        if newArrService.count > 0{
            let ids = newArrService.map { String($0.serviceId) }
            print(ids)
            artistServiceId = ids.joined(separator: ",")
        }
        
        var arr = [String]()
        var isOutCall = ""
        if self.isOutCallSelected{
            arr.append("1")
        }else{
            arr.append("0")
        }
        /*if self.isInCallSelected{
            arr.append("0")
        }else{
        }
        */
        isOutCall = arr.joined(separator: ",")
        
        let dicParam = ["feedType": strFeedType,
                        "search": "",
                        "isOutCall": isOutCall,
                        "page": self.pageNo,
                        "limit": "50",
                        "type": "explore",
                        "userId":strUserId,
                        "businessId":businessId,
                        "categoryId":categoryId,
                        "artistServiceId":artistServiceId,
                        "rating":Int(self.vwRating.value),
                        "radius":Int(self.sliderRadius.value),
                        "latitude":self.strLatitude,
                        "longitude":self.strLongitude
            ] as [String : Any]
        print(dicParam)
        callWebserviceFor_getFeeds(dicParam: dicParam,arrServiceTagId:arrServiceTagId)
    }
    
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any],arrServiceTagId:[String]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.viewNoLocation.isHidden = true
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.viewNoLocation.isHidden = true
        }else if (CLLocationManager.authorizationStatus() == .denied) {
            self.viewNoLocation.isHidden = false
            return
        } else {
        }
        if  !self.refreshControl.isRefreshing && self.pageNo == 1 {
            objWebserviceManager.StartIndicator()
        }
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.exploreFeeds, params: parameters , success: { [weak self] response in
            
            print(response)
            self?.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            self?.activity.stopAnimating()
            
            if self?.pageNo == 1 {
                self?.arrFeedsData.removeAll()
                self?.postCollection.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                if let totalCount = response["total"] as? Int{
                    self?.totalCount = totalCount + 1
                }
                if let arrDict = response["AllFeeds"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        for dictOld in arrDict{
                            let obj = feeds.init(dict: dictOld)
                            
                            ////
                            if self?.arrFeedsData.count == 0{
                                self?.arrExploreTopCategory.removeAll()
                                var arrTemp = [ExploreCategory]()
                                for objPhoto in (obj?.arrPhotoInfo)!{
                                for objTag in objPhoto.arrTags{
                                    if objTag.strTagType == "service"{
                                        let dict = objTag.metaData
                                        let objTagService = ExploreCategory.init(dict: [:])
                                        objTagService.businessId = dict["businessTypeId"] as? Int ?? 0
                                        if let id =  dict["businessTypeId"] as? String {
                                            objTagService.businessId = Int(id) ?? 0
                                        }
                                        objTagService.categoryId = dict["categoryId"] as? Int ?? 0
                                        if let id =  dict["categoryId"] as? String{
                                            objTagService.categoryId = Int(id) ?? 0
                                        }
                                            arrTemp.append(objTagService)
                                        }
                                        }
                                    }
                                for objCat in arrTemp{
                                    let newArrNN = self?.arrExploreBusiness.filter(){ $0.businessId == objCat.businessId }
                                    if newArrNN!.count > 0{
//                                        for objBusi in newArrNN!{
//                                            let newArrCat = objBusi.arrCategory.filter(){ $0.categoryId == objCat.categoryId }
//                                            self?.arrExploreTopCategory.append(contentsOf: newArrCat.clone())
//                                        }
                                        for objBusi in newArrNN!{
                                            for objCat in objBusi.arrCategory.clone(){
                                                let newArr = self?.arrExploreTopCategory.filter(){ $0.categoryId == objCat.categoryId }
                                                if newArr!.count>0{
                                                    let obj = newArr![0]
                                                    if !(self?.arrExploreTopCategory.contains(obj))!{
                                                       self?.arrExploreTopCategory.append(objCat)
                                                    }
                                                }else{
                                                    self?.arrExploreTopCategory.append(objCat)
                                                }
                                            }
                                            //self?.arrExploreTopCategory.append(contentsOf: objBusi.arrCategory.clone())
                                        }
                                    }
                                }
                                for obj in self!.arrExploreTopCategory{
                                    if (self?.arrSelCatForLocal.contains(obj.categoryId))!{
                                        obj.isSelected = true
                                        obj.isSelectLocal = true
                                    }
                                }
                            }
                            ////
                            if !(self?.arrFeedsData.contains(obj!))!{
                                self?.arrFeedsData.append(obj!)
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self?.headerView?.collectionBigImage.isHidden = false
                    for objCat in self!.arrExploreTopCategory{
                        
                    }
                    self?.categoryCollection.reloadData()
                }
                if (self?.arrFeedsData.count)!>0{
                    self?.tblFeeds.reloadData()
                }
            }else{
                if strSucessStatus == "fail"{
                }else{
                    if let msg = response["message"] as? String{
                       objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                    }
                }
            }
            self?.postCollection.reloadData()
            
            if self?.arrFeedsData.count==0{
                self?.lblNoResults.isHidden = false
            }else{
                self?.lblNoResults.isHidden = true
            }
            if (self?.arrFeedsData.count)!>0 && self?.pageNo == 1{
                self?.postCollection.contentOffset.y = 0.0
            }
        }) { [weak self] (error) in
            
            self?.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
            self?.lblNoResults.isHidden = false
        }
    }
    
    func callWebserviceFor_getAllCategoriesNew(){
        if !objServiceManager.isNetworkAvailable(){
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
         /*
         URL : https://dev.koobi.co.uk/api/get-buisnes-type-by-miles
         
         Method : POST
         
         Params :
         userId:3
         latitude:22.7196
         longitude:75.8577
         radius:
         
         Headers :
         Content-Type:application/x-www-form-urlencoded
         authToken:
         */
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        ////
         var arr = [String]()
               var isOutCall = ""
               if self.isOutCallSelected{
                   arr.append("1")
               }else{
                   arr.append("0")
               }
        isOutCall = arr.joined(separator: ",")
        ////
        
        let parameters : Dictionary = [
        "isOutCall": isOutCall,
        "userId" : strUserId,
        "latitude":self.strLatitude,
        "longitude":self.strLongitude,
        "radius":Int(self.sliderRadius.value)] as [String : Any]
        print(parameters)
        self.isMilesApiCalling = true
        objWebserviceManager.requestPostForJson(strURL: WebURL.get_buisnes_type_by_miles, params: parameters , success: { [weak self] response in
            print(response)
            self?.isMilesApiCalling = false
            self?.arrSelCatForLocal.removeAll()
            self?.arrSelServiceForLocal.removeAll()
            /*
             
             ["status": success, "Allcagtegories": <__NSArrayI 0x7ff6b40cc1e0>(
             {
                 "__v" = 0;
                 "_id" = 2;
                 "active_hash" = "$2a$10$AamnZ35oZDuLSM.12AeVROmKYmNddL4R8jSzFwSWffbgCAuSXO2Ga";
                 categories =     (
                             {
                         "__v" = 0;
                         "_id" = 21;
                         "active_hash" = hrkbiz;
                         "created_date" = "2019-09-19T07:45:00.000Z";
                         deleteStatus = 1;
                         "role_id" = 2;
                         serviceId = 2;
                         services =             (
                                             {
                                 "__v" = 0;
                                 "_id" = 7;
                                 artistId = 7;
                                 bookingCount = 6;
                                 completionTime = "00:10";
                                 crd = "2019-09-20T05:55:22.872Z";
                                 deleteStatus = 1;
                                 description = test;
                                 inCallPrice = "25.20";
                                 outCallPrice = "50.30";
                                 status = 1;
                                 title = "Hair Color";
                                 upd = "2019-09-20T05:55:22.872Z";
                             },
                                             {
                                 "__v" = 0;
                                 "_id" = 3;
                                 artistId = 7;
                                 bookingCount = 1;
                                 completionTime = "00:10";
                                 crd = "2019-09-18T09:58:28.441Z";
                                 deleteStatus = 1;
                                 description = Hello;
                                 inCallPrice = "0.0";
                                 outCallPrice = "30.0";
                                 status = 1;
                                 title = Hair;
                                 upd = "2019-09-18T09:58:28.441Z";
                             },
                                             {
                                 "__v" = 0;
                                 "_id" = 5;
                                 artistId = 7;
                                 bookingCount = 0;
                                 completionTime = "00:10";
                                 crd = "2019-09-20T05:55:22.872Z";
                                 deleteStatus = 1;
                                 description = yes;
                                 inCallPrice = "0.0";
                                 outCallPrice = "69.0";
                                 status = 1;
                                 title = "Blonde Hair";
                                 upd = "2019-09-20T05:55:22.872Z";
                             }
                         );
                         status = 1;
                         title = "Hair Cut & Maintain";
                         type = 0;
                         "updated_date" = "2019-09-19T07:45:00.000Z";
                     }
                 );
                 "created_date" = "2019-09-04T09:22:33.000Z";
                 deleteStatus = 1;
                 "role_id" = 1;
                 status = 1;
                 title = "Hair & Beauty Salon";
                 type = 0;
                 "updated_date" = "2019-09-04T09:22:33.000Z";
             },
             {
                 "__v" = 0;
                 "_id" = 1;
                 "active_hash" = "$2a$10$m.GmC7p9l7pAt.BjS3R7buV0zlFcHHy.7gtSsadpF4tEiV10Kw5q.";
                 categories =     (
                             {
                         "__v" = 0;
                         "_id" = 16;
                         "active_hash" = hrkbiz;
                         "created_date" = "2019-09-10T06:53:40.000Z";
                         deleteStatus = 1;
                         "role_id" = 2;
                         serviceId = 1;
                         services =             (
                                             {
                                 "__v" = 0;
                                 "_id" = 14;
                                 artistId = 17;
                                 bookingCount = 3;
                                 completionTime = "00:20";
                                 crd = "2019-09-26T12:26:14.330Z";
                                 deleteStatus = 1;
                                 description = "Hari cut";
                                 inCallPrice = "1.0";
                                 outCallPrice = "1.0";
                                 status = 1;
                                 title = "Hari Cut";
                                 upd = "2019-09-26T12:26:14.330Z";
                             }
                         );
                         status = 1;
                         title = "Barbecue Category";
                         type = 0;
                         "updated_date" = "2019-09-10T06:53:40.000Z";
                     }
                 );
                 "created_date" = "2019-09-04T09:22:07.000Z";
                 deleteStatus = 1;
                 "role_id" = 1;
                 status = 1;
                 title = "Barber Salon";
                 type = 0;
                 "updated_date" = "2019-09-04T09:22:07.000Z";
             }
             )
            */
            
            let strSucessStatus = response["status"] as? String
            self?.arrExploreBusiness.removeAll()
            self?.arrExploreCategory.removeAll()
            self?.headerView?.collectionBigImage.isHidden = true
            if strSucessStatus == k_success{
                if let arrDict = response["Allcagtegories"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        for dict in arrDict{
                            let obj = ExploreBusiness.init(dict:dict)
                            obj.isSelected = false
                            obj.isSelectLocal = false
                            self?.arrExploreBusiness.append(obj)
                            self?.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
                        }
                    }
//                    let arr = self?.arrExploreBusiness.map { $0.arrCategory }
//                    print(arr)
//                    if arr.count>0{
//                        for arrCat in arr{
//                            self.arrExploreCategory.append(contentsOf: arrCat.clone())
//                        }
//                    }
                    DispatchQueue.main.async {
                        self?.categoryCollection.reloadData()
                    }
                }
                self?.loadFeedsWithPageCount(page: 1)
            }else{
                objWebserviceManager.StopIndicator()
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                }
            }
        }) { [weak self] (error) in
            self?.isMilesApiCalling = false
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
    
    func callWebserviceFor_getAllCategories(){
        if !objServiceManager.isNetworkAvailable(){
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        /*
         URL : https://dev.koobi.co.uk/api/get-buisnes-type-by-miles
         
         Method : POST
         
         Params :
         userId:3
         latitude:22.7196
         longitude:75.8577
         radius:
         
         Headers :
         Content-Type:application/x-www-form-urlencoded
         authToken:
         */
        
        objWebserviceManager.requestGetForJson(strURL: WebURL.allExploreBusiness, params: [:] , success: { [weak self] response in
            print(response)
            let strSucessStatus = response["status"] as? String
            self?.arrAllCategory.removeAll()
            self?.headerView?.collectionBigImage.isHidden = true
            if strSucessStatus == k_success{
                if let arrDict = response["businessType"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        for dict in arrDict{
                            let obj = Service.init(dict: [:])
                            obj.serviceId = dict["_id"] as? Int ?? 0
                            obj.serviceName = dict["title"] as? String ?? ""
                            obj.isSelected = false
                            if let arr = dict["category"] as? [[String : Any]]{
                                for dict in arr{
                                    let objSubService = SubService.init(dict: dict)
                                    objSubService.serviceId = dict["serviceId"] as? Int ?? 0
                                    objSubService.subServiceId = dict["_id"] as? Int ?? 0
                                    objSubService.isSelected = false
                                    objSubService.subServiceName = dict["title"] as? String ?? ""
                                    obj.arrSubServices.append(objSubService)
                                }
                            }
                            self?.arrAllCategory.append(obj)
                        }
                    }
                    DispatchQueue.main.async {
                        self?.categoryCollection.reloadData()
                    }
                }
                ////
                self?.loadFeedsWithPageCount(page: 1)
                ////
            }else{
                objWebserviceManager.StopIndicator()
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                }
            }
        }) { [weak self] (error) in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
    
    func gotoExpPostDetailVC(objFeeds:feeds?){
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            objAppShareData.arrFeedsForArtistData.removeAll()
            if let objFeeds = objFeeds {
                objVC.objFeeds = objFeeds
                self.isGoForLocation = true
                objAppShareData.arrFeedsForArtistData.append(objFeeds)
                navigationController?.pushViewController(objVC, animated: true)
            }else{
                navigationController?.pushViewController(objVC, animated: false)
            }
        }
    }
}

// MARK:- UITableView Delegate and Datasource
extension ExploreVC : UITableViewDelegate,feedsTableCellDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == self.tblFeeds{
            return self.arrFeedsData.count
        }else if tableView == self.tblBookingList{
            if selectedTable == 1{
                if self.arrExploreSearchCategory.count > 0{
                   return self.arrExploreSearchCategory.count
                }else{
                   return self.arrExploreCategory.count
                }
                //return self.arrFilterAllServices.count
                
            } else if selectedTable == 2 {
                //return self.arrFilterSubService.count
                if self.arrExploreSearchService.count > 0{
                   return self.arrExploreSearchService.count
                }else{
                   return self.arrExploreService.count
                }
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    @objc func btnHiddenMoreOption(_ sender: UIButton)
    {
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnHiddenMoreOption.isHidden = true
        cell.setDefaultDesign()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        
        if tableView == tblFeeds{
            tblView = tableView
            let objFeeds = self.arrFeedsData[indexPath.row]
            
            var cellId = "ImageVideo"
            if objFeeds.feedType == "text"{
                cellId = "Text"
            }else{
                cellId = "ImageVideo"
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! feedsTableCell
            
            if objFeeds.isSave == 0{
                //cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                cell.btnSaveToFolder.setImage(UIImage.init(named: "inactive_book_mark_ico"), for: .normal)
            }else{
                // cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                cell.btnSaveToFolder.setImage(UIImage.init(named: "active_book_mark_ico"), for: .normal)
            }
            cell.setTagsAsHidden(true)
            let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
            if String(objFeeds.userId) == id {
                //cell.btnMore.isHidden = true
                cell.btnMore.isHidden = false
                cell.viewSeparatorViewMore.isHidden = false
            }else{
                cell.btnMore.isHidden = false
                //cell.viewSeparatorViewMore.isHidden = true
                cell.viewSeparatorViewMore.isHidden = false
            }
            cell.imgPlay?.isHidden = true
            cell.viewMore.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.scrollView.removeFromSuperview()
            //amit
            cell.tag = indexPath.row
            cell.indexPath = indexPath
            cell.delegate = self
            
            cell.btnHiddenMoreOption.tag = indexPath.row
            cell.btnHiddenMoreOption.addTarget(self, action: #selector(btnHiddenMoreOption(_:)), for: .touchUpInside)
            
            //amit
            if objFeeds.feedType == "video" || objFeeds.feedType == "image"{
                cell.setDefaultDesign()
            }
            
            if objFeeds.feedType == "video"{
                cell.pageControll.isHidden = true
                cell.pageControllView.isHidden = true;
                //let height = self.view.frame.size.width * 0.75
                let height = self.view.frame.size.width
                cell.heightOfImageRatio.constant = height
            }else if objFeeds.feedType == "image"{
                cell.pageControll.isHidden=false
                cell.pageControllView.isHidden = false;
                var height = self.view.frame.size.width * 0.75
                if objFeeds.feedImageRatio == "0"{
                    height = self.view.frame.size.width * 0.75
                }else if objFeeds.feedImageRatio == "1"{
                    height = self.view.frame.size.width
                }else{
                    height = self.view.frame.size.width*1.25
                }
                cell.heightOfImageRatio.constant = height
                
            }else{
                
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.superview?.tag = indexPath.section
            
//            if self.scrollToIndex == indexPath.row{
//              DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                cell.setTagsAsHidden(false)
//                }
//            }
            self.tableView(self.tblFeeds, willDisplay: cell , forRowAt: indexPath)
            return cell
            
        }else if tableView == self.tblBookingList{
            
            if let cellNew = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTableCell", for: indexPath) as? SubCategoriesTableCell{
               
                if selectedTable == 1{
                    var objServiceList = ExploreCategory.init(dict: [:])
                    if self.arrExploreSearchCategory.count > 0{
                        objServiceList = self.arrExploreSearchCategory[indexPath.row]
                    }else{
                        objServiceList = self.arrExploreCategory[indexPath.row]
                    }
                    
                    cellNew.lblName.text =  objServiceList.categoryName
                    if objServiceList.isSelected || objServiceList.isSelectLocal{
                        cellNew.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                    }else{
                        cellNew.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                    }
                } else if selectedTable == 2{
                    var objServiceList = ExploreService.init(dict: [:])
                    if self.arrExploreSearchService.count > 0{
                        objServiceList = self.arrExploreSearchService[indexPath.row]
                    }else{
                        objServiceList = self.arrExploreService[indexPath.row]
                    }
                    cellNew.lblName.text =  objServiceList.serviceName
                    if objServiceList.isSelected || objServiceList.isSelectLocal{
                        cellNew.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                    }else{
                        cellNew.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                    }
                }
                return cellNew
            }else{
                return UITableViewCell()
            }
        }else{
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell:
        UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
        /*
         let objSuggession = self.suggesionArray[indexPath.row]
         if suggesionType == ""{
         cell.lblTag.text = "#"+objSuggession.tag
         }else{
         cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
         cell.lblUserName.text = objSuggession.userName
         cell.imgProfile.image = UIImage.customImage.user
         if objSuggession.profileImage.count > 0 {
         cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
         }
         }
         */
    }
    @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        //self.viewFilterButton.isHidden = true
        //isNavigate = true
        let objFeed = self.arrFeedsData[(recognizer.view?.tag)!]
        addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
    }
    
    // MARK: - Filter PopUp
    @IBAction func ratingValueChanged(_ sender: HCSStarRatingView) {
        self.vwRating.value = sender.value
    }
    @IBAction func btnFilterCrossAction(_ sender: Any){
        self.view.endEditing(true)
        self.viewFilterPopUp.isHidden = true
        self.categoryCollection.reloadData()
        pageNo = 1
        self.loadFeedsWithPageCount(page: 1)
    }
    @IBAction func btnFilterResetAction(_ sender: Any){
        /*
        for obj in arrExploreBusiness{
            for objC in obj.arrCategory{
                objC.isSelected = false
                objC.isSelectLocal = false
            }
        }
        */
        for objC in self.arrExploreTopCategory{
            objC.isSelected = false
            objC.isSelectLocal = false
        }
        self.arrSelCatForLocal.removeAll()
        self.arrSelServiceForLocal.removeAll()
        self.arrExploreService.removeAll()
        self.arrExploreBusiness.removeAll()
        self.arrExploreCategory.removeAll()
        ////
        self.arrFilterAllServices.removeAll()
        self.arrFilterAllServices = self.arrAllServices
        ////
        self.arrCatSelectGlobal.removeAll()
        self.arrSelectCat.removeAll()
        self.view.endEditing(true)
        self.viewFilterPopUp.isHidden = true
        self.vwRating.value = 0.0
        self.sliderRadius.value = 5.0
        
        let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        let thumbImage = textToImage(drawText: strForThumbImage, inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        self.sliderRadius.setThumbImage(thumbImage, for:.normal)
        
        self.lblLocation.text = objLocationManager.strAddress
        self.strLatitude = objLocationManager.strlatitude ?? ""
        self.strLongitude = objLocationManager.strlongitude ?? ""
        self.lblBusinessType.text = ""
        self.lblServiceType.text = ""
        for obj in arrCategory{
            if obj.isSelected{
                obj.isSelected = false
                for objN in obj.arrSubServices{
                    if objN.isSelected{
                        objN.isSelected = false
                    }
                }
            }
        }
        //self.callWebserviceFor_getAllCategoriesNew()
//        let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
//        if newArr.count==0{
//           self.arrExploreCategory.removeAll()
//        }
        self.categoryCollection.reloadData()
        self.isOutCallSelected = false
        self.isInCallSelected = false
        self.setOutCallUI()
        pageNo = 1
        self.callWebserviceFor_getAllCategoriesNew()
        self.loadFeedsWithPageCount(page: 1)
    }
    @IBAction func btnFilterApplyAction(_ sender: Any){
        self.view.endEditing(true)
        self.viewFilterPopUp.isHidden = true
        self.categoryCollection.reloadData()
        pageNo = 1
        self.loadFeedsWithPageCount(page: 1)
    }
    @IBAction func btnLocationAction(_ sender: Any){
        self.view.endEditing(true)
        self.locationAuthorization()
    }
    func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name:"Nunito-Medium", size: 11)!
        var t = text
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        var rect = CGRect(origin: point, size: image.size)
        if text.count == 1{
            //rect = CGRect(origin: CGPoint(x:10,y:8), size: image.size)
            rect = CGRect(origin: CGPoint(x:18,y:8), size: image.size)
        }else if text.count == 2 {
            //rect = CGRect(origin: CGPoint(x:8,y:8), size: image.size)
            rect = CGRect(origin: CGPoint(x:15,y:8), size: image.size)
        }else if text.count == 3 {
            rect = CGRect(origin: CGPoint(x:13,y:8), size: image.size)
        }else{
            //rect = CGRect(origin: point, size: image.size)
            rect = CGRect(origin: CGPoint(x:4,y:8), size: image.size)
        }
        t.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    @IBAction func sliderValueChangedRadius(_ sender: UISlider) {
        
        ////
        self.arrExploreCategory.removeAll()
        self.arrExploreTopCategory.removeAll()
        self.arrExploreService.removeAll()
        self.arrSelCatForLocal.removeAll()
        self.arrSelServiceForLocal.removeAll()
        self.viewServiceTypeNew.isHidden = true
        self.viewBusinessTypeNew.isHidden = true
        self.lblBusinessType.text = "Service Category"
        self.lblServiceType.text = "Services"
        ////
        
        let aSlider = sender
        let a = Int(aSlider.value)
        print("radius change")
        ////
        if a == 1{
            self.sliderRadius.minimumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.sliderRadius.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else if a == 20{
            self.sliderRadius.minimumTrackTintColor = appColor
            self.sliderRadius.maximumTrackTintColor = appColor
        }else{
            self.sliderRadius.minimumTrackTintColor = appColor
            self.sliderRadius.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        ////
        
        let strForThumbImage = String(format: "%.0f", ceil(aSlider.value*100)/100)
        self.lblMileDistance.text = "1-"+String(strForThumbImage)+" Miles"
        
        let thumbImage = textToImage(drawText: "", inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        let thumbImageHigh = textToImage(drawText: strForThumbImage, inImage: #imageLiteral(resourceName: "gubbara_ico_PINNew") , atPoint: CGPoint.init(x:12,y:0))
        aSlider.setThumbImage(thumbImageHigh, for:.highlighted)
        aSlider.setThumbImage(thumbImage, for:.normal)
        aSlider.setThumbImage(thumbImage, for:.selected)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        aSlider.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        
        //// Api call for refresh business type & category for new location
        if !self.isMilesApiCalling{
           self.callWebserviceFor_getAllCategoriesNew()
        }
        ////
    }
    @IBAction func btnOpenRadiusSlider(_ sender: UIButton) {
        return
//        self.viewSlideRadiusMile.isHidden = false
//        self.viewLabelRadiusMiles.isHidden = true
//        self.sliderRadius.value = Float(objAppShareData.objRefineData.strDistance)!
//        let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
//        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
//        let thumbImage = textToImage(drawText: "", inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
//        self.sliderRadius.setThumbImage(thumbImage, for:.normal)
    }
    @IBAction func btnBusinessTypeAction(_ sender: Any){
        self.arrExploreSearchCategory.removeAll()
        self.strText = ""
        self.txtTableSearch.text = ""
        self.lblNoRecordFound.isHidden = true
        self.tblBookingList.isHidden = false
        //self.arrFilterAllServices = self.arrAllServices
        
        self.lblTableHeader.text = "Select Service Category"
        self.txtTableSearch.placeholder = "Search Service Category"
        
        //fromTableBusinessType = true
        selectedTable = 1
        if self.arrExploreCategory.count > 0{
            self.vwTableCointaner.isHidden = false
            self.tblBookingList.reloadData()
        }else{
            self.vwTableCointaner.isHidden = true
            objAppShareData.showAlert(withMessage: "No service category available!", type: alertType.bannerDark, on: self)
        }
    }
    
    func locationAuthorizationForAPI(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.viewNoLocation.isHidden = true
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.viewNoLocation.isHidden = true
        }else if (CLLocationManager.authorizationStatus() == .denied) {
            /*let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            */
            self.viewNoLocation.isHidden = false
            return
        } else if (CLLocationManager.authorizationStatus() == .notDetermined) {
            self.viewNoLocation.isHidden = false
            objLocationManager.getCurrentLocation()
        }else{
            self.viewNoLocation.isHidden = false
            objLocationManager.getCurrentLocation()
        }
    }
    func locationAuthorization(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.gotoGMSAutocompleteVC()
            self.viewNoLocation.isHidden = true
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.gotoGMSAutocompleteVC()
            self.viewNoLocation.isHidden = true
        }
        else if (CLLocationManager.authorizationStatus() == .denied) {
            /*let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            */
            self.viewNoLocation.isHidden = false
            return
        } else {
            //locManager.requestWhenInUseAuthorization()
            //self.webServiceCall_artistSearchWithDict(parameters: parameters)
            gotoGMSAutocompleteVC()
        }
    }
    @IBAction func btnServiceAction(_ sender: Any){
        self.arrExploreSearchService.removeAll()
        self.strText = ""
        self.txtTableSearch.text = ""
        self.lblNoRecordFound.isHidden = true
        self.tblBookingList.isHidden = false
        
        self.lblTableHeader.text = "Select Services"
        self.txtTableSearch.placeholder = "Search Services"
        
        //fromTableBusinessType = false
        selectedTable = 2
        self.arrExploreService.removeAll()
        if self.arrExploreCategory.count > 0{
            for obj in self.arrExploreCategory{
                if obj.isSelected{
                    self.arrExploreService.append(contentsOf: obj.arrService.clone())
                }else{
                }
            }
            for obj in self.arrExploreService{
                if (self.arrSelServiceForLocal.contains(obj.serviceId)){
                    obj.isSelected = true
                    obj.isSelectLocal = true
                }
            }
            if arrExploreService.count > 0{
                self.tblBookingList.reloadData()
                self.vwTableCointaner.isHidden = false
            }else{
                objAppShareData.showAlert(withMessage: "Please select service category", type: alertType.bannerDark, on: self)
            }
        }else{
            objAppShareData.showAlert(withMessage: "No category & service available", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnDoneTableAction(_ sender: Any){
        self.view.endEditing(true)
        self.vwTableCointaner.isHidden = true
        
        var arrBusinessData = [String]()
        var arrServiceData = [String]()
        
        if selectedTable == 1{
            
        if self.arrExploreCategory.count > 0{
            self.arrExploreService.removeAll()
            ////
            self.viewServiceTypeNew.isHidden = true
            self.lblServiceType.text = "Services"
            ////
            for objArrAll in self.arrExploreCategory{
                //if objArrAll.isSelectLocal == false && objArrAll.isSelected == false{
                if objArrAll.isSelectLocal == false {
                    objArrAll.isSelectLocal = false
                    objArrAll.isSelected = false
                    if self.arrSelCatForLocal.contains(objArrAll.categoryId){
                        let index = self.arrSelCatForLocal.firstIndex(of:objArrAll.categoryId)
                        self.arrSelCatForLocal.remove(at: index!)
                    }
                }else{
                    if !self.arrSelCatForLocal.contains(objArrAll.categoryId){
                        self.arrSelCatForLocal.append(objArrAll.categoryId)
                    }
                    //objArrAll.isSelectLocal = false
                    objArrAll.isSelectLocal = true
                    objArrAll.isSelected = true
                    ////
                    let newArr = self.arrExploreBusiness.filter(){ $0.businessId == objArrAll.businessId }
                    if newArr.count > 0{
                        let objBusiness = newArr[0]
                        objBusiness.isSelected = true
                    }
                    ////
                    /*let newArr = self.arrExploreBusiness.filter(){ $0.businessId == objArrAll.businessId }
                    if newArr.count > 0{
                        let objBusiness = newArr[0]
                        objBusiness.isSelected = true
                        ////
                        self.arrExploreCategory.removeAll()
                        let newArrBusi = self.arrExploreBusiness.filter(){ $0.isSelected == true }
                        if newArrBusi.count > 0{
                            for obj in newArrBusi{
                                self.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
                            }
                            for obj in self.arrExploreCategory{
                                if (self.arrSelCatForLocal.contains(obj.categoryId)){
                                    obj.isSelected = true
                                    obj.isSelectLocal = true
                                }
                            }
                        }
                        ////
                    }
                    arrBusinessData.append(objArrAll.categoryName)
                    self.arrExploreService.append(contentsOf: objArrAll.arrService.clone())
                    */
                }
            }
            
            ////
                self.arrExploreCategory.removeAll()
                let newArrBusi = self.arrExploreBusiness.filter(){ $0.isSelected == true }
                if newArrBusi.count > 0{
                    for obj in newArrBusi{
                        self.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
                    }
                    for obj in self.arrExploreCategory{
                        if (self.arrSelCatForLocal.contains(obj.categoryId)){
                            obj.isSelected = true
                            obj.isSelectLocal = true
                            arrBusinessData.append(obj.categoryName)
                            self.arrExploreService.append(contentsOf: obj.arrService.clone())
                        }else{
                            obj.isSelected = false
                            obj.isSelectLocal = false
                        }
                    }
                }
            ////
            
            
            if arrBusinessData.count>0{
                self.viewBusinessTypeNew.isHidden = false
                self.lblBusinessType.text = arrBusinessData.joined(separator: ", ")
            }else{
                self.viewBusinessTypeNew.isHidden = true
                self.lblBusinessType.text = "Service Category"
            }
        }else{
            self.viewServiceTypeNew.isHidden = true
            self.viewBusinessTypeNew.isHidden = true
            self.lblBusinessType.text = "Service Category"
            self.lblServiceType.text = "Services"
        }
        
        }else{
            
        if self.arrExploreService.count > 0{
            for objArrAll in self.arrExploreService{
                //if objArrAll.isSelectLocal == false && objArrAll.isSelected == false{
                if objArrAll.isSelectLocal == false{
                    objArrAll.isSelected = false
                    if self.arrSelServiceForLocal.contains(objArrAll.serviceId){
                        let index = self.arrSelServiceForLocal.firstIndex(of:objArrAll.serviceId)
                        self.arrSelServiceForLocal.remove(at: index!)
                    }
                }else{
                    if !self.arrSelServiceForLocal.contains(objArrAll.serviceId){
                       self.arrSelServiceForLocal.append(objArrAll.serviceId)
                    }
                    objArrAll.isSelectLocal = true
                    objArrAll.isSelected = true
                    arrServiceData.append(objArrAll.serviceName)
                }
            }
            if arrServiceData.count>0{
                self.viewServiceTypeNew.isHidden = false
                self.lblServiceType.text = arrServiceData.joined(separator: ", ")
            }else{
                self.viewServiceTypeNew.isHidden = true
                self.lblServiceType.text = "Services"
            }
        }else{
            self.viewServiceTypeNew.isHidden = true
            self.lblServiceType.text = "Services"
        }
        }
    }
    
    @IBAction func btnCancelTableAction(_ sender: Any){
        self.view.endEditing(true)
        self.vwTableCointaner.isHidden = true
        if self.arrExploreCategory.count > 0{
            for objArrAll in self.arrExploreCategory{
                if objArrAll.isSelected == false{
                    objArrAll.isSelectLocal = false
                }else{
                    objArrAll.isSelectLocal = true
                }
            }
        }
        
        if self.arrExploreService.count > 0{
            for objArrAll in self.arrExploreService{
                if objArrAll.isSelected == false{
                    objArrAll.isSelectLocal = false
                }else{
                    objArrAll.isSelectLocal = true
                }
            }
        }
    }
    
    // MARK: - Video player
    func addVideo(toVC url: URL) {
        // create an AVPlayer
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: url)
        controller.delegate = self
        controller.player?.play()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }
    @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
        self.view.endEditing(true)
        objAppShareData.isFromServiceTagBook = true
        let sb = UIStoryboard(name:"Feeds",bundle:Bundle.main)
        let objFeeds = self.arrFeedsData[(myLabel.view?.tag)!]
        let objLikeVc = sb.instantiateViewController(withIdentifier: "LikesListVC") as? LikesListVC
        objLikeVc?.objFeeds = objFeeds
        navigationController?.pushViewController(objLikeVc ?? UIViewController(), animated: true)
    }
    
    @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
        self.view.endEditing(true)
        return
        //self.viewFilterButton.isHidden = true
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (myLabel.view?.tag)!
        //isNavigate = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    /*
     // MARK: - Generate thumb from video
     func generateImage(_ URL: URL) ->Void {
     let asset = AVAsset.init(url: URL)
     let assetImageGenerator = AVAssetImageGenerator(asset: asset)
     
     assetImageGenerator.appliesPreferredTrackTransform=true;
     
     let maxSize = CGSize(width: 320, height: 180)
     assetImageGenerator.maximumSize = maxSize
     
     var time = asset.duration
     time.value = min(time.value, 2)
     
     do {
     let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
     self.imgFeed.image = UIImage.init(cgImage: imageRef)
     } catch {
     //print("error")
     }
     }*/
    
    @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
        
        let indexPath = IndexPath.init(row: (recognizer.view?.tag)!, section: 0)
        let cell = self.tblFeeds.cellForRow(at: indexPath) as! feedsTableCell
        cell.setTagsHidden(!(cell.tagsHidden))
        
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //cellOld.btnSaveToFolder.isHidden = false
            //cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.btnHiddenMoreOption.isHidden = true
            cellOld.setDefaultDesign()
        }
        /*
         //self.viewFilterButton.isHidden = true
         let objFeed = self.arrFeedsData[(recognizer.view?.tag)!]
         
         let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
         let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
         
         //isNavigate = true
         objShowImage.isTypeIsUrl = true
         objShowImage.arrFeedImages = objFeed.arrFeed
         objShowImage.objFeeds = objFeed
         
         //present(objShowImage, animated: true)// { _ in }
         */
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: feedsTableCell, forRowAt indexPath: IndexPath){
        
        if self.arrFeedsData.count>indexPath.row {
            cell.imgPlay?.isHidden = true
            let objFeeds = self.arrFeedsData[indexPath.row]
            
            if objFeeds.feedType == "video"{
                cell.pageControll.isHidden = true
                cell.pageControllView.isHidden = true
                cell.viewCollection.isHidden = true
                cell.imgFeeds?.isHidden = false
                cell.imgFeeds?.sd_setImage(with: URL.init(string: objFeeds.arrFeed[0].videoThumb)!, placeholderImage: UIImage(named: "gallery_placeholder"))
                
            }else if objFeeds.feedType == "image"{
                cell.pageControll.isHidden=false
                cell.pageControllView.isHidden = false
                cell.viewCollection.isHidden = false
                cell.imgFeeds?.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    cell.setPageControllMethodNew(objFeeds:objFeeds)
                    
                    if self.scrollToIndex == indexPath.row{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            cell.setTagsAsHidden(false)
                        }
                    }
                }
            }
            
            if objFeeds.feedType == "text"{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
                cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                    self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }else{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
                cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                    self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }
            
            cell.imgPlay?.tag = indexPath.row
            cell.imgPlay?.superview?.tag = indexPath.section
            cell.btnShare.tag = indexPath.row
            cell.btnShare.superview?.tag = indexPath.section
            cell.btnLike.tag = indexPath.row
            cell.btnLike.superview?.tag = indexPath.section
            cell.imgFeeds?.tag = indexPath.row
            cell.btnShowTag?.tag = indexPath.row
            
            cell.lblUserName.text = objFeeds.userInfo?.userName
            cell.lblCity.text = objFeeds.location
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            cell.lblCommentCount.text = "\(objFeeds.commentCount)"
            cell.lblTime.text = objFeeds.timeElapsed
            cell.imgProfile.image = UIImage.customImage.user
            cell.imgProfile.setImageFream()
            
            if let pImage = objFeeds.userInfo?.profileImage {
                
                if pImage.count > 0 {
                    //cell.imgProfile.af_setImage(withURL:URL(string:pImage)!)
                    //cell.imgProfile.af_setImage(withURL: URL(string:pImage)!, placeholderImage: UIImage(named: "gallery_placeholder"))
                    cell.imgProfile.sd_setImage(with: URL(string:pImage)!, placeholderImage: UIImage(named: "gallery_placeholder"))
                }
            }
            
            let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
            tapForLike.numberOfTapsRequired = 2
            
            
            if objFeeds.feedType == "video"{
                cell.viewShowTag.isHidden = true
                if objFeeds.arrFeed[0].videoThumb.count > 0 {
                    //cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                }
                cell.imgPlay?.isHidden = false
                
                let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                tapForPlayVideo.numberOfTapsRequired = 1
                cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                cell.imgPlay?.addGestureRecognizer(tapForLike)
                tapForPlayVideo.require(toFail:tapForLike)
                
            }else if objFeeds.feedType == "image"{
                cell.imgPlay?.isHidden = true
                var isTag = false
                for obj in objFeeds.arrPhotoInfo{
                    if obj.arrTags.count>0{
                        isTag = true
                        break
                    }
                }
                if isTag{
                    //cell.viewShowTag.isHidden = false
                    cell.viewShowTag.isHidden = true
                }else{
                    cell.viewShowTag.isHidden = true
                }
                
                cell.pageControllView.tag = indexPath.row
                cell.viewCollection.tag = indexPath.row
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                tap.numberOfTapsRequired = 1
//                cell.viewCollection.addGestureRecognizer(tap)
//                cell.viewCollection.addGestureRecognizer(tapForLike)
//                tap.require(toFail:tapForLike)
            }else{
                if objAppShareData.validateUrl(objFeeds.caption) {
                    cell.lblCaption.tag = indexPath.row
                }
            }
            
            if objFeeds.userInfo?._id == myId{
                cell.btnFollow.isHidden=true;
            }else{
                //cell.btnFollow.isHidden=false;
                cell.btnFollow.isHidden=true;
            }
            
            if objFeeds.isLike {
                cell.btnLike.isSelected = true
            }else {
                cell.btnLike.isSelected = false
            }
            cell.btnComment.tag = indexPath.row
            // cell.btnViewComment.tag = indexPath.row
            cell.lblLikeCount.tag = indexPath.row
            cell.lblCommentCount.tag = indexPath.row
            cell.lblCommentCount.isUserInteractionEnabled = true
            
            let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
            tapCommentCount.numberOfTapsRequired = 1
            cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
            if (cell.likes) != nil{
                cell.likes.tag = indexPath.row
            }
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            let myInt = objFeeds.likeCount
            
            if myInt > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true
                cell.lblLikeCount.addGestureRecognizer(tap2)
                cell.likes.addGestureRecognizer(tap1)
            }else {
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
            
            //Follow status
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.superview?.tag = indexPath.section
            
            if objFeeds.followerStatus {
                cell.btnFollow.setTitle("Following", for: .normal)
                cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnFollow.layer.borderWidth = 1
                cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.btnFollow.backgroundColor = UIColor.clear
            }else {
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                cell.btnFollow.layer.borderWidth = 0
            }
            //if objFeeds.feedType == "video" || objFeeds.feedType == "image"{
            cell.btnMore.tag = indexPath.row
            cell.btnSaveToFolder.tag = indexPath.row
            cell.btnReportThisPost.tag = indexPath.row
            cell.btnEditPost.tag = indexPath.row
            cell.btnDeletePost.tag = indexPath.row
            cell.btnEditPost.addTarget(self, action: #selector(btnEditPostAction(_:)), for: .touchUpInside)
            cell.btnDeletePost.addTarget(self, action: #selector(btnDeletePostAction(_:)), for: .touchUpInside)
            cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
            cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
            cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
            //}
            
            if objFeeds.feedType == "image"{
                if objFeeds.arrFeed.count > 1 {
                    cell.pageControll.isHidden = false
                    cell.pageControllView.isHidden = false
                }
            }
        }
    }
    
    @objc func btnMoreAction(_ sender: UIButton)
    {
        //self.viewFilterButton.isHidden = true
        let objData = self.arrFeedsData[sender.tag]
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        if cell.viewMore.isHidden == true{
            let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
            if String(objData.userId) == id {
                //cell.btnEditPost.isHidden = false
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = false
                cell.btnReportThisPost.isHidden = true
                cell.btnSaveToFolder.isHidden = false
                cell.viewMore.isHidden = false
                cell.btnHiddenMoreOption.isHidden = false
            }else{
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = true
                cell.btnSaveToFolder.isHidden = false
                cell.btnReportThisPost.isHidden = false
                cell.viewMore.isHidden = false
                cell.btnHiddenMoreOption.isHidden = false
            }
            //cell.btnSaveToFolder.isHidden = false
            //cell.viewMore.isHidden = false
        } else {
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.viewMore.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
        }
        
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            if indexPath != indexPathOld{
                cellOld.btnSaveToFolder.isHidden = false
                cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.btnHiddenMoreOption.isHidden = true
                cellOld.setDefaultDesign()
            }
        }
        indexLastViewMoreView = (sender as AnyObject).tag
        ////
        //cell?.setDefaultDesign()
    }
    
    @objc func btnSaveToFolder(_ sender: UIButton){
        let objData = self.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        if objData.isSave == 0{
            objAppShareData.isFromServiceTagBook = true
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                objVC.strFeedId = String(objData._id)
                objAppShareData.btnAddHiddenONMyFolder = false
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            self.webServiceForEditFolder(feedId:String(objData._id))
        }
    }
    
    func webServiceForEditFolder(feedId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "feedId" : feedId,
            "folderId":""] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    //self.viewWillAppear(true)
                    self.loadFeedsWithPageCount(page: 1)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    @objc func btnEditPostAction(_ sender: UIButton)
    {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @objc func btnDeletePostAction(_ sender: UIButton)
    {
        
        let objData = self.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.setDefaultDesign()
            var myIds = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myIds = String(dicUser["_id"] as? Int ?? 0)
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myIds = String(userInfo["_id"] as? Int ?? 0)
            }
            
            
            let param = ["feedType":objData.feedType, "id":String(objData._id), "userId":myIds]
            // objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
            self.deleteCommentAlert(param: param, indexPath: indexPath)
        }
    }
    
    func deleteCommentAlert(param:[String:Any],indexPath:IndexPath){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this feed?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            // self.totalCount  = self.totalCount  - 1
            //objAppShareData.arrFeedsData.remove(at: indexPath.row)
            //self.tblFeeds.reloadData()
            self.webServiceForDeletePost(param: param)
        }
        okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func webServiceForDeletePost(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteFeed, params: param  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: "Post deleted successfully", type: alertType.bannerDark, on: self)
                    self.loadFeedsWithPageCount(page: 1)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)  }}  }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        } }
    
    @objc func btnReportThisPost(_ sender: UIButton){
        //self.viewFilterButton.isHidden = true
        objAppShareData.isFromServiceTagBook = true
        let objData = self.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
            objVC.objModel = objData
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnSharePost(_ sender: Any) {
        objAppShareData.isFromServiceTagBook = true
        self.view.endEditing(true)
        let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        let index = (sender as AnyObject).tag
        let objFeeds = self.arrFeedsData[index!]
        
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
        objChooseType.objFeeds = objFeeds
        objChooseType.fromVoucher = false
        self.navigationController?.pushViewController(objChooseType, animated: true)
        
        return
        
        //self.shareLinkUsingActivity(withObject: objFeeds, andTableCell: cell!)
    }
    
    func shareLinkUsingActivity(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
        //let str = WebURL.BaseUrl+WebURL.feedDetails + "/" + String(objFeeds._id)
        let str = "http://koobi.co.uk:3000/"
        let url = NSURL(string:objFeeds.arrFeed[0].feedPost)
        let shareItems:Array = [url]
        
        /*
         let urlData = NSData(contentsOfURL: NSURL(string:"https://example.com/video.mov)")!)
         if ((urlData) != nil){
         print(urlData)
         let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
         let docDirectory = paths[0]
         let filePath = "\(docDirectory)/tmpVideo.mov"
         urlData?.writeToFile(filePath, atomically: true)
         // file saved
         
         let videoLink = NSURL(fileURLWithPath: filePath)
         
         let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
         let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
         activityVC.setValue("Video", forKey: "subject")
         
         //New Excluded Activities Code
         if #available(iOS 9.0, *) {
         activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList, UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypeOpenInIBooks, UIActivityTypePostToTencentWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToWeibo, UIActivityTypePrint]
         } else {
         // Fallback on earlier versions
         activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList, UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToTencentWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToWeibo, UIActivityTypePrint ]
         }
         self.presentViewController(activityVC, animated: true, completion: nil)
         }
         */
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        //self.viewFilterButton.isHidden = true
        
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        let index = (sender as AnyObject).tag
        let objFeeds = self.arrFeedsData[index!]
        //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
        if !objFeeds.isLike{
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
    }
    
    func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
            objWebserviceManager.StopIndicator()
        }){ error in
        }
    }
    
    func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
        
        if (objFeeds.isLike){
            objFeeds.isLike = false
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = false
                })
            })
            objFeeds.likeCount = objFeeds.likeCount - 1
        }else{
            objFeeds.isLike = true
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = true
                })
            })
            objFeeds.likeCount = objFeeds.likeCount + 1
        }
        cell.lblLikeCount.text = "\(objFeeds.likeCount)"
        if objFeeds.likeCount > 1{
            cell.lblLikeOrLikes.text = "Likes"
        }else{
            cell.lblLikeOrLikes.text = "Like"
        }
        
        //        // API Call
        //        objLocationManager.getCurrentAdd(success: { address in
        //
        //
        //        }) { error in
        //            print("address nor found")
        //        }
        
        var dicUser = [:] as! [String:Any]
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            dicUser = dict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            dicUser = dict
        }
        //let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! [String:Any]
        
        let dicParam  = ["feedId": objFeeds._id,
                         "userId":objFeeds.userInfo?._id ?? 0,
                         "likeById":self.myId,
                         "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? "18"),
                         "gender":dicUser["gender"] as? String ?? "male",
                         "city":objLocationManager.currentCLPlacemark?.locality ?? "Indore",
                         "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "MP",
                         "country":objLocationManager.currentCLPlacemark?.country ?? "India",
                         "type":"feed"]  as [String : Any]
        
        print("dicParam = \(dicParam)")
        
        self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        tap1.numberOfTapsRequired = 1
        tap2.numberOfTapsRequired = 1
        if objFeeds.likeCount > 0{
            cell.lblLikeCount.isUserInteractionEnabled = true
            cell.likes.isUserInteractionEnabled = true
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
        }else {
            cell.lblLikeCount.isUserInteractionEnabled = false
            cell.likes.isUserInteractionEnabled = false
        }
    }
    
    func showAnimatedLikeUnlikeOn(cell:feedsTableCell, objFeeds:feeds)->Void{
        let size = cell.imageVideoView.frame.size.width/3
        
        let heartImageView = UIImageView(frame: CGRect(x:0,y:0, width:size ,height:size))
        
        //heartImageView.layer.cornerRadius = heartImageView.frame.size.width/2
        //heartImageView.layer.masksToBounds = true;
        heartImageView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue:1.0, alpha: 0.5)
        heartImageView.center = CGPoint(x:cell.imageVideoView.center.x,y:cell.imageVideoView.center.y-size/2)
        heartImageView.image = UIImage.init(named:"ic_heart_outline")
        heartImageView.contentMode = .center
        heartImageView.transform = CGAffineTransform(scaleX:0, y:0)
        cell.imageVideoView.addSubview(heartImageView)
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            heartImageView.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.4, animations: {
                heartImageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            }, completion: { (_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.4, animations: {
                    heartImageView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                    heartImageView.removeFromSuperview()
                })
            })
        })
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if tableView == tblFeeds {
            
            if self.arrFeedsData.count > indexPath.row{
                
                let objFeeds = self.arrFeedsData[indexPath.row]
                
                if objFeeds.feedType == "text" {
                    
                    if let height = objFeeds.cellHeight{
                        return height
                    }else{
                        let height = getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                        objFeeds.cellHeight = height
                        return height
                    }
                    
                }else {
                    if objFeeds.caption.count > 0 {
                        if let height = objFeeds.cellHeight{
                            return height + 6
                        }else{
                            
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else{
                                height = self.view.frame.size.width * 1.25 + 112
                            }
                            
                            let newHeight = height + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 8), font: "Nunito-Regular", fontSize: 16.0).height
                            objFeeds.cellHeight = newHeight
                            
                            return height + 6
                        }
                        
                    }else{
                        
                        if let height = objFeeds.cellHeight{
                            return height
                        }else{
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else if objFeeds.feedImageRatio == "2"{
                                height = self.view.frame.size.width * 1.25 + 112
                            }
                            
                            objFeeds.cellHeight = height
                            return height
                        }
                        //return self.view.frame.size.width * 0.75 + 100
                    }
                }
            }
            
        }else if tableView == self.tblBookingList {
            return 45
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == self.tblBookingList{
            self.txtTableSearch.resignFirstResponder()
            let cell = (tblBookingList?.cellForRow(at: indexPath) as? SubCategoriesTableCell)!
            if selectedTable == 1{
                var objServiceList = ExploreCategory.init(dict: [:])
                if self.arrExploreSearchCategory.count > 0{
                    objServiceList = self.arrExploreSearchCategory[indexPath.row]
                }else{
                    objServiceList = self.arrExploreCategory[indexPath.row]
                }
                if objServiceList.isSelectLocal == true{
                    objServiceList.isSelectLocal = false
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                }else{
                    objServiceList.isSelectLocal = true
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                }
                
            } else if selectedTable == 2{
                var objServiceList = ExploreService.init(dict: [:])
                if self.arrExploreSearchService.count > 0{
                    objServiceList = self.arrExploreSearchService[indexPath.row]
                }else{
                    objServiceList = self.arrExploreService[indexPath.row]
                }
                if objServiceList.isSelectLocal == true{
                    objServiceList.isSelectLocal = false
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                }else{
                    objServiceList.isSelectLocal = true
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                }
            }
        }
    }
    
    func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
        
        let constraintSize = CGSize(width: width, height: .infinity)
        let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
        let attributes = [NSAttributedString.Key.font : font];
        let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
        let stringSize: CGSize = frame.size
        return stringSize
    }
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
        let objFeed = self.arrFeedsData[indexPath.row]
        if objFeed.feedType == "video"{
            //isNavigate = true
            self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            
        }else if objFeed.feedType == "image" {
            
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                
                //isNavigate = true
                objShowImage.isTypeIsUrl = true
                objShowImage.arrFeedImages = objFeed.arrFeed
                objShowImage.objFeeds = objFeed
                
                //present(objShowImage, animated: true)
            }
        }
    }
    @IBAction func btnLikeFeed(_ sender: Any){
        self.view.endEditing(true)
        //self.viewFilterButton.isHidden = true
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        let index = (sender as AnyObject).tag
        let objFeeds = self.arrFeedsData[index!]
        likeUnlikePost(cell: cell, objFeeds: objFeeds)
    }
    
    @IBAction func btnComment(onFeed sender: Any) {
        self.view.endEditing(true)
        objAppShareData.isFromServiceTagBook = true
        ////
        objAppShareData.arrFeedsData = self.arrFeedsData
        ////
        //return
        //self.viewFilterButton.isHidden = true
        let sb = UIStoryboard(name:"Feeds",bundle:Bundle.main)
        let objComments = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (sender as AnyObject).tag
        //isNavigate = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        let objFeed = self.arrFeedsData[indexPath.row]
        if !objFeed.isLike{
            likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
        }
    }
    
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        print("hhdhfhds")
    }
    
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
        print("tagPopover dict = %@",dict)
        self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            if self.myId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    
    func gotoExploreDetailVCWithSearchText(searchText:String, type:String){
        
        if type == "hashtag" {
            
            let dicParam = [
                "tabType" : "hasTag",
                "tagId": "",
                "title": searchText
                ] as [String : Any]
            
            let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                objVC.sharedDict = dicParam
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let dicParam = ["userName":searchText]
            
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": searchText
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
            }
        }
    }
}


// MARK:- GMS Map Methods
extension ExploreVC : GMSAutocompleteViewControllerDelegate{
    
    func gotoGMSAutocompleteVC(){
        isGoForLocation = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let add = place.formattedAddress{
            self.lblLocation.text = add
            self.viewRadiusChoose.isHidden = false
            self.viewSlideRadiusMile.isHidden = false
            self.viewLabelRadiusMiles.isHidden = true
        }else{
            self.lblLocation.text = "Location"
            self.viewRadiusChoose.isHidden = true
        }
        ////
        self.arrExploreCategory.removeAll()
        self.arrExploreTopCategory.removeAll()
        self.arrExploreService.removeAll()
        self.arrSelCatForLocal.removeAll()
        self.arrSelServiceForLocal.removeAll()
        self.viewServiceTypeNew.isHidden = true
        self.viewBusinessTypeNew.isHidden = true
        self.lblBusinessType.text = "Service Category"
        self.lblServiceType.text = "Services"
        ////
        if place.coordinate.latitude != 0 && place.coordinate.longitude != 0 {
            if strLatitude == "\(place.coordinate.latitude)" && strLongitude == "\(place.coordinate.longitude)"{
                strLatitude = "\(place.coordinate.latitude)"
                strLongitude = "\(place.coordinate.longitude)"
            }else{
                strLatitude = "\(place.coordinate.latitude)"
                strLongitude = "\(place.coordinate.longitude)"
                //// Api call for refresh business type & category for new location
                self.callWebserviceFor_getAllCategoriesNew()
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

// MARK: - Text field methods
extension ExploreVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtTableSearch{
            self.txtTableSearch.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.txtTableSearch.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //if fromTableBusinessType == true{
        if selectedTable == 1{
            self.arrExploreSearchCategory.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            
            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
            let filteredArray = self.arrExploreCategory.filter(){ $0.categoryName.localizedCaseInsensitiveContains(trimmedString) }
            //let filteredArray = self.arrAllServices.filter(){ $0.title.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            self.strText = trimmedString
            //self.strText = substring
            self.arrExploreSearchCategory = filteredArray
            if self.arrExploreSearchCategory.count == 0 && self.strText.count != 0{
                self.lblNoRecordFound.isHidden = false
                self.tblBookingList.isHidden = true
            }else{
                if strText.count == 0{
                    self.arrExploreSearchCategory = self.arrExploreCategory
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }else{
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }
            }
            self.tblBookingList.reloadData()
            return  true
       
        }else if selectedTable == 2{
            self.arrExploreSearchService.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
            let filteredArray = self.arrExploreService.filter(){ $0.serviceName.localizedCaseInsensitiveContains(trimmedString) }
            //let filteredArray = self.arrSubService.filter(){ $0.title.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            //self.strText = substring
            self.strText = trimmedString
            self.arrExploreSearchService = filteredArray
            if self.arrExploreSearchService.count == 0 && self.strText.count != 0{
                self.lblNoRecordFound.isHidden = false
                self.tblBookingList.isHidden = true
            }else{
                if strText.count == 0{
                    self.arrExploreSearchService = self.arrExploreService
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }else{
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }
            }
            self.tblBookingList.reloadData()
            return true
        }else{
            return true
        }
    }
    
    @IBAction func btnServiceInListAction(_ sender: UIButton) {
        //// For refresh last level services
        self.arrExploreService.removeAll()
        self.arrSelServiceForLocal.removeAll()
        ////
        
        var objCategory = ExploreCategory.init(dict: [:])
        let newArr = self.arrExploreBusiness.filter(){ $0.isSelected == true }
        if newArr.count>0{
           objCategory = self.arrExploreCategory[sender.tag]
        }else{
            objCategory = self.arrExploreTopCategory[sender.tag]
        }
        if objCategory.isSelected{
            objCategory.isSelected = false
            if self.arrSelCatForLocal.contains(objCategory.categoryId){
                let index = self.arrSelCatForLocal.firstIndex(of:objCategory.categoryId)
                self.arrSelCatForLocal.remove(at: index!)
            }
        }else{
            self.arrSelCatForLocal.append(objCategory.categoryId)
            objCategory.isSelected = true
            let newArr = self.arrExploreBusiness.filter(){ $0.businessId == objCategory.businessId }
            if newArr.count > 0{
                let objBusiness = newArr[0]
                objBusiness.isSelected = true
                
                ////
                self.arrExploreCategory.removeAll()
                let newArrBusi = self.arrExploreBusiness.filter(){ $0.isSelected == true }
                if newArrBusi.count > 0{
                    for obj in newArrBusi{
                        self.arrExploreCategory.append(contentsOf: obj.arrCategory.clone())
                    }
                    for obj in self.arrExploreCategory{
                        if (self.arrSelCatForLocal.contains(obj.categoryId)){
                            obj.isSelected = true
                            obj.isSelectLocal = true
                        }
                    }
                }
                ////
            }
        }
        pageNo = 1
        self.loadFeedsWithPageCount(page: 1)
    }
    
}
