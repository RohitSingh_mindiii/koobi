//
//  ExpPostDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 10/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class NewFeedDetailVC : UIViewController, AVPlayerViewControllerDelegate {
    
    var objFeeds : feeds?
    @IBOutlet weak var lblHeaderName: UILabel!
    fileprivate var indexLastViewMoreView = 0
    @IBOutlet weak var vwDataContainer: UIView!
    @IBOutlet weak var tblFeeds: UITableView!
    var arrFeedsLocal = [feeds]()
    fileprivate var tblView: UITableView?
    fileprivate var myId:Int = 0
    fileprivate var isVideo = false
    fileprivate var isNavigate = false
    fileprivate var mp4VideoURL: URL?
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
}

extension NewFeedDetailVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.addGesturesToMainView()
        self.objFeeds?._id = objAppShareData.arrFeedsForArtistData[0]._id
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        //// From did load to here
        if objAppShareData.isFromNotification {
            self.lblHeaderName.text = "Post"
            self.reloadFeedPostFromNotification()
        }else{
            self.reloadFeedPost(activity: false)
            return
           
            if let objFeeds = objFeeds, objAppShareData.arrFeedsForArtistData.count == 0 {
                objAppShareData.arrFeedsForArtistData.append(objFeeds)
            }
            
            if objAppShareData.arrFeedsForArtistData.count > 0{
                let objFeeds = objAppShareData.arrFeedsForArtistData[0]
                self.lblHeaderName.text = "Image"
                if objFeeds.feedType == "video"{
                    self.lblHeaderName.text = "Video"
                }
            }
            
            self.tblFeeds.reloadData()
            //self.reloadFeedPost(activity: false)
        }
        /////
        
        if objAppShareData.arrFeedsForArtistData.count == 0{
           objAppShareData.arrFeedsForArtistData = self.arrFeedsLocal
        }
        self.lblHeaderName.text = "Post"

//        if objAppShareData.isFromNotification {
//            self.lblHeaderName.text = "Post"
//            self.reloadFeedPostFromNotification()
//        }else{
//
//            if let objFeeds = objFeeds, objAppShareData.arrFeedsForArtistData.count == 0 {
//                objAppShareData.arrFeedsForArtistData.append(objFeeds)
//            }
//
//            if objAppShareData.arrFeedsForArtistData.count > 0{
//                let objFeeds = objAppShareData.arrFeedsForArtistData[0]
//                self.lblHeaderName.text = "Image"
//                if objFeeds.feedType == "video"{
//                    self.lblHeaderName.text = "Video"
//                }
//            }
//
//            self.tblFeeds.reloadData()
//            self.reloadFeedPost(activity: false)
//        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.reloadFeedPost(activity: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

// MARK:- Custom Methods
extension NewFeedDetailVC {
    
    func configureView(){
        
        self.tblFeeds.delegate = self
        self.tblFeeds.dataSource = self
        //self.tblFeeds.addSubview(self.refreshControl)
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                self.myId = userId
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                self.myId = userId
            }
        }
    }
    func addGesturesToMainView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginTap.delegate = self
        socialLoginTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(socialLoginTap)
    }
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //if indexPath != indexPathOld{
            cellOld.btnSaveToFolder.isHidden = false
            cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.setDefaultDesign()
        }
        //}
        ////
    }
}


// MARK: - IBActions
extension NewFeedDetailVC {
    
    @IBAction func btnBackAction(_ sender: Any){
        if objAppShareData.isFromNotification {
           objAppShareData.clearNotificationData()
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnComment(onFeed sender: Any) {
        let sb = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        
        if let objComments = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
            objComments.isOtherThanFeedScreen = true
            objComments.selectedIndex = (sender as AnyObject).tag
            isNavigate = true
            navigationController?.pushViewController(objComments, animated: true)
        }       
    }
    
    @IBAction func btnFollowAction(_ sender: Any){
        
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        let objUser = objAppShareData.arrFeedsForArtistData[indexPath.row]
        objUser.followerStatus = !objUser.followerStatus
        callWebservice(for_Follow: objUser, andCell: cell!)
    }
    
    @IBAction func btnLikeFeed(_ sender: Any){
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        //let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsForArtistData[0]
        likeUnlikePost(cell: cell, objFeeds: objFeeds)
    }
}

// MARK:- UITableView Delegate and Datasource
extension NewFeedDetailVC : UITableViewDelegate,feedsTableCellDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return objAppShareData.arrFeedsForArtistData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
            let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
            
            var cellId = "ImageVideo"
           
            if objFeeds.feedType == "text"{
                cellId = "Text"
            }else{
                cellId = "ImageVideo"
            }
        
            tblView = tableView
        
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? feedsTableCell {
                
                cell.scrollView.removeFromSuperview()
                //amit
                cell.tag = indexPath.row
                cell.indexPath = indexPath
                cell.delegate = self
                //amit

                if objFeeds.feedType == "video"{
                    cell.viewShowTag.isHidden = true
                    cell.pageControll.isHidden=true
                    cell.pageControllView.isHidden = true;
                    
                }else if objFeeds.feedType == "image"{
                    var isTag = false
                    for obj in objFeeds.arrPhotoInfo{
                        if obj.arrTags.count>0{
                            isTag = true
                            break
                        }
                    }
                    if isTag{
                        cell.viewShowTag.isHidden = false
                    }else{
                        cell.viewShowTag.isHidden = true
                    }
                    cell.pageControll.isHidden=false
                    cell.pageControllView.isHidden = false;
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                        cell.setPageControllMethod(objFeeds:objFeeds)
                    }
                }
                if objFeeds.userInfo?._id == myId{
                    //cell.viewSeparatorViewMore.isHidden = true
                    cell.viewSeparatorViewMore.isHidden = false
                }else{
                    cell.viewSeparatorViewMore.isHidden = false
                }
                if objFeeds.isSave == 0{
                    //cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "inactive_book_mark_ico"), for: .normal)
                }else{
                    // cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "active_book_mark_ico"), for: .normal)
                }
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                self.tableView(self.tblFeeds, willDisplay: cell, forRowAt: indexPath)
                
                return cell

            }else{
                return UITableViewCell()
            }
    }
    @objc func btnEditPostAction(_ sender: UIButton)
    {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @objc func btnDeletePostAction(_ sender: UIButton)
    {
        self.deleteCommentAlert(param: [:])
    }
    
    func deleteCommentAlert(param:[String:Any]){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this post?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func btnReportThisPost(_ sender: UIButton)
    {
        
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
            objVC.objModel = objData
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnSharePost(_ sender: Any) {
        self.view.endEditing(true)
        let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
        self.shareLinkUsingActivity(withObject: objFeeds, andTableCell: cell!)
    }
    func shareLinkUsingActivity(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
        let str = "http://koobi.co.uk:3000/"
        let url = NSURL(string:str)
        let shareItems:Array = [url]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func btnMoreAction(_ sender: UIButton)
    {
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        if cell.viewMore.isHidden == true{
            let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
            if String(objData.userId) == id {
                cell.btnReportThisPost.isHidden = true
                cell.btnSaveToFolder.isHidden = false
                //cell.viewMore.isHidden = true
                //cell.btnEditPost.isHidden = false
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = false
                cell.viewMore.isHidden = false
            }else{
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = true
                cell.btnSaveToFolder.isHidden = false
                cell.btnReportThisPost.isHidden = false
                cell.viewMore.isHidden = false
            }
            //cell.btnSaveToFolder.isHidden = false
            //cell.viewMore.isHidden = false
        } else {
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.viewMore.isHidden = true
            cell.setDefaultDesign()
        }
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            if indexPath != indexPathOld{
                cellOld.btnSaveToFolder.isHidden = false
                cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.setDefaultDesign()
            }
        }
        indexLastViewMoreView = (sender as AnyObject).tag
        ////
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //self.tableView(self.tblFeeds, willDisplay: cell as! feedsTableCell, forRowAt: indexPath)
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: feedsTableCell, forRowAt indexPath: IndexPath){
        
        if objAppShareData.arrFeedsForArtistData.count > indexPath.row {
            let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
            
            if objFeeds.feedType == "text"{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
            cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                    
                    self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }else{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                   self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }
            
            cell.imgPlay?.tag = indexPath.row
            cell.imgPlay?.superview?.tag = indexPath.section
            cell.btnShare.tag = indexPath.row
            cell.btnShare.superview?.tag = indexPath.section
            cell.btnLike.tag = indexPath.row
            cell.btnLike.superview?.tag = indexPath.section
            cell.imgFeeds?.tag = indexPath.row
            cell.btnMore.tag = indexPath.row
            cell.btnShowTag?.tag = indexPath.row
            cell.lblUserName.text = objFeeds.userInfo?.userName
            cell.lblCity.text = objFeeds.location
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            cell.lblCommentCount.text = "\(objFeeds.commentCount)"
            cell.lblTime.text = objFeeds.timeElapsed
            cell.imgProfile.image = UIImage.customImage.user
            cell.imgProfile.setImageFream()
            cell.viewMore.isHidden = true
            
            if let pImage = objFeeds.userInfo?.profileImage {
                
                if pImage.count > 0 {
                    //cell.imgProfile.af_setImage(withURL:URL(string:pImage)!)
                    cell.imgProfile.sd_setImage(with: URL(string:pImage)!, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
            
            let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
            tapForLike.numberOfTapsRequired = 2
            
            
            if objFeeds.feedType == "video"{
                
                if objFeeds.arrFeed[0].videoThumb.count > 0 {
                    //cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                    cell.imgFeeds?.sd_setImage(with: URL.init(string: objFeeds.arrFeed[0].videoThumb)!, placeholderImage: nil)
                }
                cell.imgPlay?.isHidden = false
                
                let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                tapForPlayVideo.numberOfTapsRequired = 1
                cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                cell.imgPlay?.addGestureRecognizer(tapForLike)
                tapForPlayVideo.require(toFail:tapForLike)
                
            }else if objFeeds.feedType == "image"{
                
                cell.imgPlay?.isHidden = true
                cell.pageControllView.tag = indexPath.row
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                tap.numberOfTapsRequired = 1
                cell.pageControllView.addGestureRecognizer(tap)
                cell.pageControllView.addGestureRecognizer(tapForLike)
                tap.require(toFail:tapForLike)
                
            }else{
                if objAppShareData.validateUrl(objFeeds.caption) {
                    cell.lblCaption.tag = indexPath.row
                }
            }
            
            if objFeeds.userInfo?._id == myId{
                cell.btnFollow.isHidden=true;
                //cell.btnMore.isHidden=true;
                cell.btnMore.isHidden=false;
                cell.viewSeparatorViewMore.isHidden = false
            }else{
                cell.btnFollow.isHidden=false;
                cell.btnMore.isHidden=false;
                cell.viewSeparatorViewMore.isHidden = true
            }
            
            if objFeeds.isLike {
                cell.btnLike.isSelected = true
            }else {
                cell.btnLike.isSelected = false
            }
            cell.btnComment.tag = indexPath.row
            //cell.btnViewComment.tag = indexPath.row
            cell.lblLikeCount.tag = indexPath.row
            cell.lblCommentCount.tag = indexPath.row
            cell.lblCommentCount.isUserInteractionEnabled = true
            
            let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
            tapCommentCount.numberOfTapsRequired = 1
            cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
            if (cell.likes) != nil{
                cell.likes.tag = indexPath.row
            }
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            let myInt = objFeeds.likeCount
            
            if myInt > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true
                cell.lblLikeCount.addGestureRecognizer(tap2)
                cell.likes.addGestureRecognizer(tap1)
            }else {
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
            
            //Follow status
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.superview?.tag = indexPath.section
            
            if objFeeds.followerStatus {
                
                cell.btnFollow.setTitle("Following", for: .normal)
                cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnFollow.layer.borderWidth = 1
                cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.btnFollow.backgroundColor = UIColor.clear
                
            }else {
                
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                cell.btnFollow.layer.borderWidth = 0
            }
            
            cell.btnMore.tag = indexPath.row
            cell.btnSaveToFolder.tag = indexPath.row
            cell.btnReportThisPost.tag = indexPath.row
            cell.btnEditPost.tag = indexPath.row
            cell.btnDeletePost.tag = indexPath.row
            cell.btnEditPost.addTarget(self, action: #selector(btnEditPostAction(_:)), for: .touchUpInside)
            cell.btnDeletePost.addTarget(self, action: #selector(btnDeletePostAction(_:)), for: .touchUpInside)
            cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
            cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
            cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
         
//            cell.lblUserName.text = objFeeds.userInfo?.userName
//
//            cell.lblCity.text = objFeeds.location
//            cell.imgPlay?.tag = indexPath.row
//            cell.imgPlay?.superview?.tag = indexPath.section
//
//            let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
//            tapForLike.numberOfTapsRequired = 2
//
//            cell.btnShare.tag = indexPath.row
//            cell.btnShare.superview?.tag = indexPath.section
//            cell.btnLike.tag = indexPath.row
//            cell.btnLike.superview?.tag = indexPath.section
//            cell.imgFeeds?.tag = indexPath.row
//
//            cell.lblCaption.text = objFeeds.caption
//
//            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
//            cell.lblCommentCount.text = "\(objFeeds.commentCount)"
//            cell.lblTime.text = objFeeds.timeElapsed
//            cell.imgProfile.image = UIImage.customImage.user
//            cell.imgProfile.setImageFream()
//
//            if let pImage = objFeeds.userInfo?.profileImage {
//                if pImage.count > 0 {
//                    cell.imgProfile.af_setImage(withURL:URL(string:pImage)!)
//                }
//            }
//
//            if objFeeds.feedType == "video"{
//
//                if objFeeds.arrFeed[0].videoThumb.count > 0 {
//
//                    cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
//                }
//                cell.imgPlay?.isHidden = false
//                //
//                let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
//                tapForPlayVideo.numberOfTapsRequired = 1
//                cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
//                cell.imgPlay?.addGestureRecognizer(tapForLike)
//                tapForPlayVideo.require(toFail:tapForLike)
//
//            }else if objFeeds.feedType == "image"{
//
//                cell.imgPlay?.isHidden = true
//                cell.pageControllView.tag = indexPath.row
//
////                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
////                tap.numberOfTapsRequired = 1
////                cell.pageControllView.addGestureRecognizer(tap)
////                cell.pageControllView.addGestureRecognizer(tapForLike)
////                tap.require(toFail:tapForLike)
//
//            }else{
//                if objAppShareData.validateUrl(objFeeds.caption) {
//                    cell.lblCaption.tag = indexPath.row
//                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tappedLabel))
//                    tap.numberOfTapsRequired = 1
//                }
//            }
//
//            if objFeeds.userInfo?._id == myId {
//                cell.btnFollow.isHidden=true;
//            }else{
//                cell.btnFollow.isHidden=false;
//            }
//
//            if objFeeds.isLike {
//                cell.btnLike.isSelected = true
//            }else {
//                cell.btnLike.isSelected = false
//            }
//
//            cell.btnComment.tag = indexPath.row
//            cell.lblLikeCount.tag = indexPath.row
//            cell.lblCommentCount.tag = indexPath.row
//            cell.lblCommentCount.isUserInteractionEnabled = true
//            let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
//            tapCommentCount.numberOfTapsRequired = 1
//            cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
//            if (cell.likes) != nil{
//                cell.likes.tag = indexPath.row
//            }
//            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
//            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
//            tap1.numberOfTapsRequired = 1
//            tap2.numberOfTapsRequired = 1
//
//            let myInt = objFeeds.likeCount
//
//            if myInt > 0{
//
//                cell.lblLikeCount.isUserInteractionEnabled = true
//                cell.likes.isUserInteractionEnabled = true
//                cell.lblLikeCount.addGestureRecognizer(tap2)
//                cell.likes.addGestureRecognizer(tap1)
//
//            }else {
//
//                cell.lblLikeCount.isUserInteractionEnabled = false
//                cell.likes.isUserInteractionEnabled = false
//            }
//
//            //Follow status
//            cell.btnFollow.tag = indexPath.row
//            cell.btnFollow.superview?.tag = indexPath.section
//
//            if objFeeds.followerStatus {
//
//                cell.btnFollow.setTitle("Following", for: .normal)
//                cell.btnFollow.setTitleColor(UIColor.theameColors.blueColor, for: .normal)
//                cell.btnFollow.layer.borderWidth = 1
//                cell.btnFollow.layer.borderColor = UIColor.theameColors.blueColor.cgColor
//                cell.btnFollow.backgroundColor = UIColor.clear
//
//            }else {
//
//                cell.btnFollow.setTitle("Follow", for: .normal)
//                cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
//                cell.btnFollow.backgroundColor = UIColor.theameColors.pinkColor
//                cell.btnFollow.layer.borderWidth = 0
//
//            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if objAppShareData.arrFeedsForArtistData.count>indexPath.row {

            let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]

            if objFeeds.feedType == "text" {
                
                return getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 116
            }else {
                
                if objFeeds.caption.count > 0 {
                    return (self.view.frame.size.width * 0.75) + 100 + 6 +  getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 16), font: "Nunito-Regular", fontSize: 13).height
                    
                }else{
                    return self.view.frame.size.width * 0.75 + 100
                }
            }
        }
    
        return 0;
    }
    
    func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
        
         let constraintSize = CGSize(width: width, height: .greatestFiniteMagnitude)
               let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
        let attributes = [NSAttributedString.Key.font : font];
               let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
               let stringSize: CGSize = frame.size
               return stringSize
    }
    
    @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
       
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        let objFeeds = objAppShareData.arrFeedsForArtistData[0]
        if !objFeeds.isLike{
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
    }
    
    @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
        let objFeed = objAppShareData.arrFeedsForArtistData[0]
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
        
        isNavigate = true
        objShowImage.isTypeIsUrl = true
        objShowImage.arrFeedImages = objFeed.arrFeed
        objShowImage.objFeeds = objFeed
        
        //present(objShowImage, animated: false)
    }
    
    @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
        isNavigate = true
        let objFeed = objAppShareData.arrFeedsForArtistData[0]
        addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
    }
    
    @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
        
        let objFeeds = objAppShareData.arrFeedsForArtistData[0]
        let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"LikesListVC") as? LikesListVC{
            objVC.objFeeds = objFeeds
            isNavigate = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
        
        let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
            
            objVC.selectedIndex = 0
            objVC.isOtherThanFeedScreen = true
            isNavigate = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc func showVideoImage() {
        if isVideo {
            addVideo(toVC: mp4VideoURL!)
        }
    }
    
    // MARK: - UITapGestures action methods
    @objc func tappedLabel(_ myLabel: UITapGestureRecognizer) {

    }
    
    func addVideo(toVC url: URL) {
        // create an AVPlayer
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: url)
        controller.delegate = self
        controller.player?.play()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)// { _ in }
    }
    
    func gotoExploreDetailVCWithSearchText(searchText:String, type:String){
        
        if type == "hashtag" {
            
            let dicParam = [
                "tabType" : "hasTag",
                "tagId": "",
                "title": searchText
                ] as [String : Any]
            
            let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                objVC.sharedDict = dicParam
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let dicParam = ["userName":searchText]
            
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": searchText
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
            }
        }
    }
    
    func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
        
        if (objFeeds.isLike){
            objFeeds.isLike = false
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = false
                })
            })
            objFeeds.likeCount = objFeeds.likeCount - 1
        }else{
            objFeeds.isLike = true
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = true
                })
            })
            objFeeds.likeCount = objFeeds.likeCount + 1
        }
        cell.lblLikeCount.text = "\(objFeeds.likeCount)"
        
        // API Call
        objLocationManager.getCurrentAdd(success: { address in
            //let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! [String:Any]
            var dicUser = [:] as! [String : Any]
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                dicUser = dict
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                dicUser = userInfo
            }
            
            let dicParam  = ["feedId": objFeeds._id,
                             "userId":objFeeds.userInfo?._id ?? 0,
                             "likeById":self.myId,
                             "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? ""),
                             "gender":dicUser["gender"] as? String ?? "male",
                             "city":address.locality ?? "",
                             "state":address.administrativeArea ?? "",
                             "country":address.country ?? "",
                             "type":"feed"]  as [String : Any]
            
            self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
            
        }) { error in
            
        }

        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        tap1.numberOfTapsRequired = 1
        tap2.numberOfTapsRequired = 1
        if objFeeds.likeCount > 0{
            cell.lblLikeCount.isUserInteractionEnabled = true
            cell.likes.isUserInteractionEnabled = true
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
        }else {
            cell.lblLikeCount.isUserInteractionEnabled = false
            cell.likes.isUserInteractionEnabled = false
        }
    }
}

// MARK: - Webservices call

extension NewFeedDetailVC {
    
    func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
            objWebserviceManager.StopIndicator()
        }){ error in

        }
    }
    
    func callWebservice(for_Follow objUser: feeds, andCell cell: feedsTableCell){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["followerId": objUser.userInfo?._id ?? 0,
                        "userId":myId]
        let followActivity = UIActivityIndicatorView()
        followActivity.tintColor = UIColor.white
        followActivity.color = UIColor.white
        followActivity.hidesWhenStopped = true
        followActivity.center = CGPoint(x: cell.btnFollow.frame.size.width / 2, y: cell.btnFollow.frame.size.height / 2)
        cell.btnFollow.addSubview(followActivity)
        followActivity.startAnimating()
        
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            followActivity.stopAnimating()
            followActivity.removeFromSuperview()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
            let strSucessStatus = response["status"] as? String ?? ""
            if strSucessStatus == k_success{
                if objUser.followerStatus {
                    cell.btnFollow.setTitle("Following", for: .normal)
                    cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                    cell.btnFollow.layer.borderWidth = 1
                    cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.btnFollow.backgroundColor = UIColor.clear
                }else {
                    cell.btnFollow.setTitle("Follow", for: .normal)
                    cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                    cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                    cell.btnFollow.layer.borderWidth = 0
                }
            }else{
                
                if let msg = response["message"] as? String{
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            }}) { error in
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

// MARK: - Webservices call
extension NewFeedDetailVC {
    
    func reloadFeedPostFromNotification() {
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        var strFeedId = ""
        if let userInfo = objAppShareData.notificationInfoDict {
            if let feedID  = userInfo["notifyId"] as? Int{
                strFeedId = "\(feedID)"
            }else{
                strFeedId = userInfo["notifyId"] as? String ?? ""
            }
        }
     
        let dicParam = [
            "feedId": strFeedId,
            "userId":strUserId] as [String : Any]
        
        callWebserviceFor_getFeeds(dicParam: dicParam , activity:true)
    }
    
    func reloadFeedPost(activity:Bool) {
       
//        guard objAppShareData.arrFeedsForArtistData.count > 0 else {
//            return
//        }
        //let objFeeds = objAppShareData.arrFeedsForArtistData[0]
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let dicParam = [
            "feedId": self.objFeeds?._id,
                        "userId":strUserId] as [String : Any]
        
        callWebserviceFor_getFeeds(dicParam: dicParam , activity:activity)
    }
    
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any],activity:Bool ){
        
        if !objServiceManager.isNetworkAvailable(){
           objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if  !self.refreshControl.isRefreshing {
            if activity{
                objWebserviceManager.StartIndicator()
            }
        }
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.feedDetails, params: parameters , success: { response in
            
            self.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                
                if let arrDict = response["feedDetail"] as? [[String:Any]]{
                    if arrDict.count > 0 {
                        objAppShareData.arrFeedsForArtistData.removeAll()
                        for dict in arrDict{
                            let obj = feeds.init(dict: dict)
                            objAppShareData.arrFeedsForArtistData.append(obj!)
                        }
                        ////
                        self.arrFeedsLocal = objAppShareData.arrFeedsForArtistData
                        //// New
                        self.tblFeeds.reloadData()
                        ////
                    }else{
                        if objAppShareData.isFromNotification {
                            objAppShareData.clearNotificationData()
                        }
                        self.navigationController?.popViewController(animated: false)
                    objAppShareData.showAlert(withMessage: "Post is no longer available", type: alertType.bannerDark,on: self)
                    }
                }
            }else{
                if strSucessStatus == "fail"{
                    
                }else{
                 
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
            
            self.tblFeeds.reloadData()
            
        }) { (error) in
            
            self.refreshControl.endRefreshing()
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

// MARK: - Notification Actions
extension NewFeedDetailVC {
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        if objFeed.feedType == "video"{
            
            isNavigate = true
            self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            
        }else if objFeed.feedType == "image" {
            
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                
                isNavigate = true
                objShowImage.isTypeIsUrl = true
                objShowImage.arrFeedImages = objFeed.arrFeed
                objShowImage.objFeeds = objFeed
                
                //present(objShowImage, animated: true)
            }
        }
    }
    
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        if !objFeed.isLike{
            likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
        }
    }
    
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
       
    }
    
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
        print("tagPopover dict = %@",dict)
        self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            if self.myId == tagId {
                isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                self.gotoProfileVC()
            }
        }
    }
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        objAppShareData.arrFeedsForArtistData.removeAll()
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnFeedProfileAction(_ sender: UIButton){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":(objFeed.userInfo?.userName)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objFeed.userInfo?.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    @objc func btnSaveToFolder(_ sender: UIButton){
        if objAppShareData.arrFeedsForArtistData.count == 0{
            return
        }
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        if objData.isSave == 0{
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                objVC.strFeedId = String(objData._id)
                objAppShareData.btnAddHiddenONMyFolder = false
                navigationController?.pushViewController(objVC, animated: true)
            }}else{
            self.webServiceForEditFolder(feedId:String(objData._id))
        }
    }
    
    func webServiceForEditFolder(feedId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "feedId" : feedId,
            "folderId":""] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    //  self.viewWillAppear(true)
                    self.objFeeds?.isSave = 0
                    self.reloadFeedPost(activity: false)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
}

