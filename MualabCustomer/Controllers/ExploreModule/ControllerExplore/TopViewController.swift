//
//  TopViewController.swift
//  MualabCustomer
//
//  Created by Mac on 03/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase
import SDWebImage

class TopViewController: UIViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var lblNoResults: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var objService = SubSubService.init(dict: [:])
    var itemInfo = IndicatorInfo(title: "View")
    var tabType : String = ""
    var previousSearchText : String = ""
    fileprivate var strMyId: String = ""
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var arrExpSearchTag = [ExpSearchTag]()
    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    let pageSize = 10 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchDataWithDictNew(_:)), name: NSNotification.Name(rawValue: notificationName.searchText), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshTabType(_:)), name: NSNotification.Name(rawValue: "refreshTabType"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshPeopleList), name: NSNotification.Name(rawValue: "refreshPeopleList"), object: nil)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.addSubview(refreshControl)
        ref = Database.database().reference()

        if self.tabType == "place"{
            loadDataWithPageCount(page: 1, strSearchText: objAppShareData.strExpSearchText)
        }else{
           loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        //SDImageCache.removeImage()
        URLCache.shared.removeAllCachedResponses()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        //SDImageCache.removeImage()
//        URLCache.shared.removeAllCachedResponses()
        
        if previousSearchText != objAppShareData.strExpSearchText  {
            loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
        }else if self.tabType == "people"{
            loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
        }
        previousSearchText = objAppShareData.strExpSearchText
        self.tblView.contentOffset.y = 0.0
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    @objc func refreshPeopleList(_ refreshControl: UIRefreshControl) {
        loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if self.tabType == "place"{
            loadDataWithPageCount(page: 1, strSearchText: objAppShareData.strExpSearchText)
        }else{
            loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
        }
    }
    
    // handle notification
    @objc func refreshTabType(_ notification: NSNotification) {
        if let dict = notification.userInfo {
            let tabType = dict["tabType"] as? Int ?? 0
            if tabType == 0{
              self.tabType = "top"
            }else if tabType == 1{
              self.tabType = "people"
            }else if tabType == 2{
              self.tabType = "hasTag"
            }else if tabType == 3{
              self.tabType = "place"
            }
        }
    }
    @objc func searchDataWithDictNew(_ notification: NSNotification) {
        
        if let dict = notification.userInfo {
            
            let strTabType = dict["tabType"] as? String ?? ""
            //if strTabType == self.tabType && {
            if strTabType == "place"{
                loadDataWithPageCount(page: 1, strSearchText: objAppShareData.strExpSearchText)
            }else{
                loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
            }
            //}else{
                //return from another tab
            //}
        }
    }
}

// MARK:- TableView Delegates
extension TopViewController  {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrExpSearchTag.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if self.arrExpSearchTag.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            //if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
            if (indexPath.item >= preloadIndex-5 && lastLoadedPage < nextPage) {
                loadDataWithPageCount(page: nextPage)
            }
        }
        
        if let cell : CellTop = tableView.dequeueReusableCell(withIdentifier: "CellTop")! as? CellTop{
            
            let objExpSearchTag = self.arrExpSearchTag[indexPath.row]
            
            cell.lblName.text = objExpSearchTag.title
            cell.lblPostDetails.text = objExpSearchTag.desc
            cell.imgVwProfile.image = UIImage(named: "cellBackground")
            cell.lblPostDetails.isHidden = false
            
            cell.btnProfilePic.tag = indexPath.row
            cell.btnProfilePic.superview?.tag = indexPath.section
            cell.btnFollow.isHidden = true
            cell.btnBook.isHidden = true
            if objExpSearchTag.tabType == "people"{
                if Int(self.strMyId) == objExpSearchTag.id{
                    cell.btnFollow.isHidden = true
                }else{
                    cell.btnFollow.isHidden = false
                }
                if objExpSearchTag.imageUrl != "" {
                    if let url = URL(string: objExpSearchTag.imageUrl){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                       cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }
                //Follow status
                cell.btnFollow.tag = indexPath.row
                cell.btnFollow.superview?.tag = indexPath.section
                
                if objExpSearchTag.followerStatus {
                    
                   cell.btnFollow.setTitle("Following", for: .normal)
                    cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                    cell.btnFollow.layer.borderWidth = 1
                    cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.btnFollow.backgroundColor = UIColor.clear
                    
                }else {
                    
                    cell.btnFollow.setTitle("Follow", for: .normal)
                    cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                    cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                    cell.btnFollow.layer.borderWidth = 0
                }
            }else if objExpSearchTag.tabType == "top" {
                if objExpSearchTag.imageUrl != "" {
                    if let url = URL(string: objExpSearchTag.imageUrl){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                       cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }
                cell.btnBook.tag = indexPath.row
                if Int(self.strMyId) == objExpSearchTag.id{
                    cell.btnBook.isHidden = true
                }else{
                    cell.btnBook.isHidden = false
                }
            }else if objExpSearchTag.tabType == "hasTag" {
                cell.lblName.text = "#" + objExpSearchTag.title
                cell.imgVwProfile.image = UIImage(named: "hagTag_ico")
            }else if objExpSearchTag.tabType == "place" {
                cell.lblPostDetails.isHidden = false
                cell.imgVwProfile.image = UIImage(named: "new_map_ico")
            }else{
                cell.imgVwProfile.image = UIImage(named: "cellBackground")
            }
            
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        self.parent?.view.endEditing(true)
        let objExpSearchTag = self.arrExpSearchTag[indexPath.row]
        
        if objExpSearchTag.tabType == "top"{
            self.view.isUserInteractionEnabled = false
            var strMyId = ""
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    strMyId = "\(userId)"
                }
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                if let id = userInfo["_id"] as? Int {
                    strMyId = String(id)
                }
            }
            let strOtherId = String(objExpSearchTag.id)
            var BlockRoom = ""
            BlockRoom = strOtherId+"_"+strMyId
            var miId = 0
            miId = Int(strMyId)!
            var oopId = 0
            oopId = Int(strOtherId) ?? 0
            if miId <= oopId{
                BlockRoom = strMyId+"_"+strOtherId
            }
        self.ref.child("blockUserArtist").child(BlockRoom).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
                //guard let strongSelf = self else { return }
                var blockedBy = ""
                if let dict = snapshot.value as? [String:Any]{
                    let strBlockedBy =  dict["blockedBy"] as? String ?? ""
                    switch strBlockedBy{
                    case strMyId:
                        blockedBy = "me"
                        
                    case strOtherId:
                        blockedBy =
                            "opponent"
                        
                    case "Both":
                        blockedBy = "both"
                        
                    default:
                        blockedBy = "none"
                        self.view.isUserInteractionEnabled = true
                    }
                }else {
                    self.view.isUserInteractionEnabled = true
                    blockedBy = "none"
                }
                if blockedBy == "opponent" || blockedBy == "both" {
                    objAppShareData.showAlert(withMessage: "You are blocked by this user! will no longer see their feed", type: alertType.bannerDark,on: self)
                    self.view.isUserInteractionEnabled = false
                }else{
                //self.gotToPostScreen(objExpSearchTag:objExpSearchTag)
                self.gotToTopPostScreen(objExpSearchTag:objExpSearchTag)
                    self.view.isUserInteractionEnabled = true
                }
            })
       
        }else if objExpSearchTag.tabType == "people"{
            let objExpSearchTag = self.arrExpSearchTag[indexPath.row]
            if objExpSearchTag.tabType == "people"{
                let section = 0
                let row = indexPath.row
                let indexPath = IndexPath(row: row, section: section)
                let objUser = arrExpSearchTag[indexPath.row]
                
                if !objServiceManager.isNetworkAvailable(){
                    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                    return
                }
                self.view.isUserInteractionEnabled = false
                let dicParam = ["userName":objUser.title]
                objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                    self.view.isUserInteractionEnabled = true
                    if response["status"] as? String ?? "" == "success"{
                        var strId = ""
                        var strType = ""
                        if let dictUser = response["userDetail"] as? [String : Any]{
                            let myId = dictUser["_id"] as? Int ?? 0
                            strId = String(myId)
                            strType = dictUser["userType"] as? String ?? ""
                        }
                        let dic = [
                            "tabType" : "people",
                            "tagId": strId,
                            "userType":strType,
                            "title": objUser.title
                            ] as [String : Any]
                        self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                    }
                }) { error in
                    self.view.isUserInteractionEnabled = true
                }
            }
        
        }else{
           self.gotToPostScreen(objExpSearchTag:objExpSearchTag)
        }
    }
    
    func gotToPostScreen(objExpSearchTag:ExpSearchTag){
        let dicParam = [
            "tabType": objExpSearchTag.tabType,
            "tagId": objExpSearchTag.id,
            "title": objExpSearchTag.title,
            "userType": objExpSearchTag.userType
            ] as [String : Any]
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
            objVC.sharedDict = dicParam
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotToTopPostScreen(objExpSearchTag:ExpSearchTag){
        let dicParam = [
            "tabType": objExpSearchTag.tabType,
            "tagId": objExpSearchTag.id,
            "title": objExpSearchTag.title,
            "userType": objExpSearchTag.userType
            ] as [String : Any]
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExploreTopDetailVC") as? ExploreTopDetailVC{
            objVC.sharedDict = dicParam
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSearchText"), object: nil, userInfo: nil)
        self.view.endEditing(true)
        self.parent?.view.endEditing(true)
        print(self.tblView.contentInset)
        self.tblView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 250.0, right: 0.0);
    }
    
    @IBAction func btnProfilePicAction(_ sender: UIButton){
        let objExpSearchTag = self.arrExpSearchTag[sender.tag]
        if objExpSearchTag.tabType == "top" || objExpSearchTag.tabType == "people"{
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objUser = arrExpSearchTag[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.title]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.title
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myUserId = 0
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    myUserId = userId
                }
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                if let userId = userInfo["_id"] as? Int {
                    myUserId = userId
                }
            }
            if myUserId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        //self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

// MARK: - Webservices call

extension TopViewController {
    
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        
        lastLoadedPage = page
        self.pageNo = page
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        self.strMyId = strUserId
        previousSearchText = strSearchText
        let dicParam = [ "search": strSearchText,
                         "page": self.pageNo,
                         "limit": "15",
                         "type": tabType ,
                         "userId":strUserId ] as [String : Any]
        callWebserviceFor_getData(dicParam: dicParam)
    }
    
    func callWebserviceFor_getData(dicParam: [AnyHashable: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
            self.activity.startAnimating()
            //objWebserviceManager.StartIndicator()
        }
        if self.tabType == "place" && !self.refreshControl.isRefreshing && self.pageNo == 1 {
            self.activity.startAnimating()
        }
        self.lblNoResults.isHidden = true
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.exploreSearch, params: parameters , success: { [weak self] response in
            print(response)
            self?.refreshControl.endRefreshing()
          
            self?.activity.stopAnimating()
            
            if self?.pageNo == 0 {
                self?.arrExpSearchTag.removeAll()
                self?.tblView.reloadData()
            }else if self?.pageNo == 1 && self?.tabType == "place"{
                self?.arrExpSearchTag.removeAll()
                self?.tblView.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                
                if let totalCount = response["totalCount"] as? Int{
                    self?.totalCount = totalCount
                }
                
                var arrTemp = [[String : Any]]()
                if response.keys.contains("topList"){
                    if let arrDict = response["topList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                }else if response.keys.contains("peopleList"){
                    if let arrDict = response["peopleList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("hasTagList"){
                    if let arrDict = response["hasTagList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("placeList"){
                    if let arrDict = response["placeList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("serviceTagList"){
                    if let arrDict = response["serviceTagList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                }
                
                if arrTemp.count > 0 {
                    
                    for dict in arrTemp{
                        
                        let objExpSearchTag = ExpSearchTag.init(dict: dict)
                        
                        switch (self?.tabType){
                            
                        case "top":
                            objExpSearchTag.type = 0;
                            objExpSearchTag.tabType = "top"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            
                            if Int(objExpSearchTag.postCount)!>1{
                              objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "posts";
                            }else{
                              objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "post";
                            }
                            break;
                            
                        case "people":
                            objExpSearchTag.type = 1;
                            objExpSearchTag.tabType = "people"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            if Int(objExpSearchTag.postCount)!>1{
                                objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "posts";
                            }else{
                                objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "post";
                            }
                            break;
                            
                        case "hasTag":
                            objExpSearchTag.type = 2;
                            objExpSearchTag.tabType = "hasTag"
                            objExpSearchTag.title = objExpSearchTag.tag;
                            if Int(objExpSearchTag.tagCount)!>1{
                               objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public posts";
                            }else{
                              objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                            }
                            break;
                            
                        case "serviceTag":
                            objExpSearchTag.type = 3;
                            objExpSearchTag.tabType = "serviceTag"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                            break;
                            
                        case "place":
                            objExpSearchTag.type = 4;
                            objExpSearchTag.tabType = "place"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            if Int(objExpSearchTag.placeCount)!>1{
                                objExpSearchTag.desc =  "\(objExpSearchTag.placeCount) " + "public posts";
                            }else{
                                objExpSearchTag.desc =  "\(objExpSearchTag.placeCount) " + "public post";
                            }
                            break;
                        default:
                            break;
                        }
                        
                        if objExpSearchTag.type == 4 {
                            if objExpSearchTag.title.count>0{
                                //if !(self?.arrExpSearchTag.contains(objExpSearchTag))!{
                                let filteredServiceArray = self?.arrExpSearchTag.filter(){ $0.id == objExpSearchTag.id }
                                if filteredServiceArray!.count == 0{
                                   self?.arrExpSearchTag.append(objExpSearchTag)
                                }
                            }
                        }else if objExpSearchTag.type == 0 {
                            if objExpSearchTag.postCount != "0"{
                                //if !(self?.arrExpSearchTag.contains(objExpSearchTag))!{
                                let filteredServiceArray = self?.arrExpSearchTag.filter(){ $0.id == objExpSearchTag.id }
                                if filteredServiceArray!.count == 0{
                                    self?.arrExpSearchTag.append(objExpSearchTag)
                                }
                            }
                        }else{
                            //if !(self?.arrExpSearchTag.contains(objExpSearchTag))!{
                            let filteredServiceArray = self?.arrExpSearchTag.filter(){ $0.id == objExpSearchTag.id }
                            if filteredServiceArray!.count == 0{
                                self?.arrExpSearchTag.append(objExpSearchTag)
                            }
                        }
                    }
                }
                
            }else{
                if strSucessStatus == "fail"{
                    
                }else{
                    
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                    }
                }
            }
            
            self?.tblView.reloadData()
            if self?.arrExpSearchTag.count==0{
                self?.lblNoResults.isHidden = false
            }else{
                self?.lblNoResults.isHidden = true
            }
            
        }) { [weak self] (error) in
            
            self?.refreshControl.endRefreshing()
            self?.activity.stopAnimating()
            
            self?.tblView.reloadData()
            if self?.arrExpSearchTag.count==0{
                self?.lblNoResults.isHidden = false
            }else{
                self?.lblNoResults.isHidden = true
            }
   
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
    
    @IBAction func btnFollowAction(_ sender: Any){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.endEditing(true)
        self.parent?.view.endEditing(true)
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = tblView?.cellForRow(at: indexPath) as? CellTop
        let objUser = self.arrExpSearchTag[indexPath.row]
        objUser.followerStatus = !objUser.followerStatus
        
        ////
        if objUser.followerStatus {
            cell!.btnFollow.setTitle("Following", for: .normal)
            cell!.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            cell!.btnFollow.layer.borderWidth = 1
            cell!.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell!.btnFollow.backgroundColor = UIColor.clear
        }else {
            cell!.btnFollow.setTitle("Follow", for: .normal)
            cell!.btnFollow.setTitleColor(UIColor.white, for: .normal)
            cell!.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            cell!.btnFollow.layer.borderWidth = 0
        }
        cell!.btnFollow.isUserInteractionEnabled = false
        ////
        
        callWebservice(for_Follow: objUser, andCell: cell!)
    }
    
    func callWebservice(for_Follow objUser: ExpSearchTag, andCell cell: CellTop){
//        if !objServiceManager.isNetworkAvailable(){
//            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//            return
//        }
        let dicParam = ["followerId": objUser.id,
                        "userId":self.strMyId] as [String : Any]
        /*let followActivity = UIActivityIndicatorView()
        followActivity.tintColor = UIColor.white
        followActivity.color = UIColor.white
        followActivity.hidesWhenStopped = true
        followActivity.center = CGPoint(x: cell.btnFollow.frame.size.width / 2, y: cell.btnFollow.frame.size.height / 2)
        cell.btnFollow.addSubview(followActivity)
        followActivity.startAnimating()
        */
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            /*followActivity.stopAnimating()
            followActivity.removeFromSuperview()
            */
            cell.btnFollow.isUserInteractionEnabled = true
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == k_success{
                    /*if objUser.followerStatus {
                      cell.btnFollow.setTitle("Following", for: .normal)
                        cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                        cell.btnFollow.layer.borderWidth = 1
                        cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                        cell.btnFollow.backgroundColor = UIColor.clear
                    }else {
                        cell.btnFollow.setTitle("Follow", for: .normal)
                        cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                        cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                        cell.btnFollow.layer.borderWidth = 0
                    }
                    */
                }else{
                    
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }}) { error in
                cell.btnFollow.isUserInteractionEnabled = true
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    @IBAction func btnBookAction(_ sender: UIButton){
        self.view.endEditing(true)
        let obj = self.arrExpSearchTag[sender.tag]
        objAppShareData.selectedOtherIdForProfile = obj.id
        
        ////
        var strIncallOrOutCall = objAppShareData.dictFilter["serviceType"] as? String ?? ""
        if strIncallOrOutCall == "1"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        self.objService.artistId = obj.id
        let strServiceId = objAppShareData.dictFilter["service"] as? String ?? "0"
        let strSubServiceId = objAppShareData.dictFilter["subservice"] as? String ?? "0"
        //var serviceId = 0
        //var subServiceId = 0
        if strServiceId.contains(","){
            let arr = strServiceId.components(separatedBy: ",")
            self.objService.serviceId = Int(arr[0]) ?? 0
        }else{
            self.objService.serviceId = Int(strServiceId) ?? 0
        }
        if strSubServiceId.contains(","){
            let arr = strSubServiceId.components(separatedBy: ",")
            self.objService.subServiceId = Int(arr[0]) ?? 0
        }else{
            self.objService.subServiceId = Int(strSubServiceId) ?? 0
        }
        let strDate = objAppShareData.dictFilter["dateForHold"] as? String ?? ""
        objAppShareData.objServiceForEditBooking.strDateForEdit = strDate
        objAppShareData.isBookingFromService = true
        ////
        
        if self.objService.serviceId == 0 {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objWebserviceManager.StartIndicator()
            let parameters : Dictionary = [
                "userId" : strMyId,
                "artistId" : obj.id
                ] as [String : Any]
            objWebserviceManager.requestPost(strURL: WebURL.getMostBookedService, params: parameters  , success: { response in
                let keyExists = response["responseCode"] != nil
                if keyExists {
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        let dictServices =  response["artistServices"] as? [String:Any] ?? [:]
                        self.objService.serviceId = dictServices["serviceId"] as? Int ?? 0
                        self.objService.subServiceId = dictServices["subserviceId"] as? Int ?? 0
                        self.objService.subSubServiceId = dictServices["_id"] as? Int ?? 0
                        objAppShareData.objServiceForEditBooking = self.objService
                        objWebserviceManager.StopIndicator()
                        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
                        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                            
                            ////
                            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
                            ////
                            objVC.hidesBottomBarWhenPushed = true
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                    }else{
                        objWebserviceManager.StopIndicator()
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
            }
        }else{
            objWebserviceManager.StopIndicator()
            objAppShareData.objServiceForEditBooking = self.objService
            let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
            if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                
                ////
                objVC.strInCallOrOutCallFromService = strIncallOrOutCall
                ////
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
}
