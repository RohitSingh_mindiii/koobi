//
//  ExpSearchTag.swift
//  MualabCustomer
//
//  Created by Mac on 05/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation
import UIKit

class ExpSearchTag : NSObject {

    var id : Int = 0
    var type : Int = 0
    var title : String = ""
    var desc : String = ""
    var tabType : String = ""
    
    var imageUrl : String = ""
    var uniTxt : String = ""
    var postCount : String = "0"
    var placeCount : String = "0"
    var fullName : String = ""
    
    var tag : String = ""
    var tagCount : String = "0"
    
    var userType : String = ""
    var strCreatUserStatus = false
    var followerStatus : Bool = false
    
    init(dict : [String : Any]){
        self.strCreatUserStatus = false
        if let id = dict["_id"] as? Int{
            self.id = id
        }
        if let followerStatus = dict["followerStatus"] as? Int{
            if followerStatus == 1{
                self.followerStatus = true
            }else{
                self.followerStatus = false
            }
        }
        if let firstName = dict["firstName"] as? String{
            self.fullName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.fullName =  self.fullName + lastName
        }
        if let userName = dict["userName"] as? String{
           self.uniTxt =  userName
        }else{
            if let location = dict["location"] as? String{
                self.uniTxt =  location
            }
        }
        if let imageUrl = dict["profileImage"] as? String{
            self.imageUrl = imageUrl
        }
        if let postCount = dict["postCount"] as? String{
            self.postCount = postCount
        }else if let postCount = dict["postCount"] as? Int{
            self.postCount = String(postCount)
        }
        
        if let tag = dict["tag"] as? String{
            self.tag = tag
        }
        if let tagCount = dict["tagCount"] as? String{
            self.tagCount = tagCount
        }else{
            if let tagCount = dict["tagCount"] as? Int{
                self.tagCount = "\(tagCount)"
            }
        }
        if let placeCount = dict["count"] as? String{
            self.placeCount = placeCount
        }else{
            if let placeCount = dict["count"] as? Int{
                self.placeCount = "\(placeCount)"
            }
        }
        if let userType = dict["userType"] as? String{
            self.userType = userType
        }
    }
    
}

