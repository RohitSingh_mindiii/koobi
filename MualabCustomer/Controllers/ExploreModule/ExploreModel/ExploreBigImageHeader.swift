//
//  TagInfoHeader.swift
//  SceneKey
//
//  Created by Mindiii on 2/13/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ExploreBigImageHeader: UICollectionReusableView {
    //var arrServices = [SubSubService]()
    var arrServices = [ExploreCategory]()
    @IBOutlet weak var viewBackground: customView!
    @IBOutlet weak var imgBig: UIImageView!
    @IBOutlet weak var collectionBigImage: UICollectionView!
    //var ExploreBigImageHeader: ExploreBigImageHeader?
    
    func setDataInCollection(obj:feeds){
//        arrServices.removeAll()
//        arrServices = obj.arrTagedServices
//    self.ExploreBigImageHeader?.collectionBigImage.reloadData()
    }
}

//MARK:- Collection view Delegate Methods
extension ExploreBigImageHeader:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrServices.count
    }
        
    // make a cell for each cell index path
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cellIdentifier = "SubsubServicesCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
            let obj = arrServices[indexPath.row]
            cell.btnService.superview?.tag = 0
            cell.btnService.tag = indexPath.row
            cell.lblUserName.textColor = UIColor.white
//            if obj.isSelected{
//                cell.lblUserName.backgroundColor = UIColor.theameColors.skyBlueNewTheam
//                cell.lblBorder.backgroundColor = UIColor.theameColors.skyBlueNewTheam
//            }else{
//                cell.lblUserName.backgroundColor = UIColor.init(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
//                cell.lblBorder.backgroundColor = UIColor.init(red: 159.0/255.0, green: 159.0/255.0, blue: 159.0/255.0, alpha: 1.0)
//            }
        cell.lblUserName.textColor = UIColor.black
        if obj.isSelected {
            cell.lblBorder.layer.borderWidth = 0.0
            cell.lblBorder.layer.borderColor = UIColor.black.cgColor
            cell.lblBorder.backgroundColor = UIColor.theameColors.skyBlueNewTheam
            cell.lblUserName.textColor = UIColor.white
            cell.lblUserName.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        }else{
            cell.lblBorder.layer.borderWidth = 0.5
            cell.lblBorder.layer.borderColor = UIColor.black.cgColor
            cell.lblBorder.backgroundColor = UIColor.white
            cell.lblUserName.textColor = UIColor.black
            cell.lblUserName.backgroundColor = UIColor.white
        }
            cell.lblUserName.text = obj.categoryName
            let a = cell.lblUserName.layer.frame.height
            cell.lblUserName.layer.cornerRadius = 11
            cell.lblUserName.layer.masksToBounds = true
            cell.lblBorder.layer.cornerRadius = 12
            cell.lblBorder.layer.masksToBounds = true
            return cell
        }
        
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            var cellWidth:CGFloat = 0
            var cellHeight:CGFloat = 25
            cellWidth = CGFloat(self.collectionBigImage.frame.size.width-4)
            let obj = arrServices[indexPath.row]
            let a = obj.categoryName
            
            var sizeOfString = CGSize()
            if let font = UIFont(name: "Nunito-Regular", size: 15)
            {
                let finalDate = a
                let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
            }
            cellWidth = sizeOfString.width+20
            cellHeight = 25
            
            return CGSize(width: cellWidth, height: cellHeight)
        }
}

