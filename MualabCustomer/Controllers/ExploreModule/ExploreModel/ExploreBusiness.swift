//
//  Service.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation

class ExploreBusiness : NSObject {
    var artistId : Int = 0
    var isSelectLocal : Bool = false
    var isSelected : Bool = false
	
    var businessId : Int = 0
    var businessName : String = ""
    
    var arrCategory : [ExploreCategory] = [ExploreCategory]()
    
    init(dict: [String : Any]){
        
        if let serviceId = dict["_id"] as? Int{
            self.businessId = serviceId
        }
        if let serviceName = dict["title"] as? String{
            self.businessName = serviceName
        }
        
        if let arr = dict["categories"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = ExploreCategory.init(dict: dict)
                self.arrCategory.append(objSubService)
            }
        }
        
        artistId = dict[""] as? Int ?? 0
    }
    
    /*
     {
         "__v" = 0;
         "_id" = 2;
         "active_hash" = "$2a$10$AamnZ35oZDuLSM.12AeVROmKYmNddL4R8jSzFwSWffbgCAuSXO2Ga";
         categories =     (
                     {
                 "__v" = 0;
                 "_id" = 21;
                 "active_hash" = hrkbiz;
                 "created_date" = "2019-09-19T07:45:00.000Z";
                 deleteStatus = 1;
                 "role_id" = 2;
                 serviceId = 2;
                 services =             (
                                     {
                         "__v" = 0;
                         "_id" = 7;
                         artistId = 7;
                         bookingCount = 6;
                         completionTime = "00:10";
                         crd = "2019-09-20T05:55:22.872Z";
                         deleteStatus = 1;
                         description = test;
                         inCallPrice = "25.20";
                         outCallPrice = "50.30";
                         status = 1;
                         title = "Hair Color";
                         upd = "2019-09-20T05:55:22.872Z";
                     },
                                     {
                         "__v" = 0;
                         "_id" = 3;
                         artistId = 7;
                         bookingCount = 1;
                         completionTime = "00:10";
                         crd = "2019-09-18T09:58:28.441Z";
                         deleteStatus = 1;
                         description = Hello;
                         inCallPrice = "0.0";
                         outCallPrice = "30.0";
                         status = 1;
                         title = Hair;
                         upd = "2019-09-18T09:58:28.441Z";
                     },
                                     {
                         "__v" = 0;
                         "_id" = 5;
                         artistId = 7;
                         bookingCount = 0;
                         completionTime = "00:10";
                         crd = "2019-09-20T05:55:22.872Z";
                         deleteStatus = 1;
                         description = yes;
                         inCallPrice = "0.0";
                         outCallPrice = "69.0";
                         status = 1;
                         title = "Blonde Hair";
                         upd = "2019-09-20T05:55:22.872Z";
                     }
                 );
                 status = 1;
                 title = "Hair Cut & Maintain";
                 type = 0;
                 "updated_date" = "2019-09-19T07:45:00.000Z";
             }
         );
         "created_date" = "2019-09-04T09:22:33.000Z";
         deleteStatus = 1;
         "role_id" = 1;
         status = 1;
         title = "Hair & Beauty Salon";
         type = 0;
         "updated_date" = "2019-09-04T09:22:33.000Z";
     }
     */
}

