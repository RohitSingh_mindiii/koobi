//
//  SubSubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class ExploreCategory : Copying, Equatable{

    static func == (lhs: ExploreCategory, rhs: ExploreCategory) -> Bool {
        lhs.categoryId = rhs.categoryId
        lhs.isSelected = rhs.isSelected
        return true
    }

    required init(original: ExploreCategory) {
         artistId = original.artistId
         isSelectLocal = original.isSelectLocal
         isSelected = original.isSelected
         businessId = original.businessId
         businessName = original.businessName
         categoryId = original.categoryId
         categoryName = original.categoryName
         arrService = original.arrService
    }
    
    var artistId : Int = 0
    var isSelectLocal : Bool = false
    var isSelected : Bool = false
    
    var businessId : Int = 0
    var businessName : String = ""
    
    var categoryId : Int = 0
    var categoryName : String = ""
    var arrService : [ExploreService] = [ExploreService]()

    init(dict : [String : Any]){
        businessId = dict["serviceId"] as? Int ?? 0
        
        categoryId = dict["_id"] as? Int ?? 0
        categoryName = dict["title"] as? String ?? ""
        
        if let arr = dict["services"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = ExploreService.init(dict: dict)
                self.arrService.append(objSubService)
            }
        }
        
        artistId = dict[""] as? Int ?? 0
        businessName = dict[""] as? String ?? ""
       
        /*
         {
             "__v" = 0;
             "_id" = 21;
             "active_hash" = hrkbiz;
             "created_date" = "2019-09-19T07:45:00.000Z";
             deleteStatus = 1;
             "role_id" = 2;
             serviceId = 2;
             services =             (
                                 {
                     "__v" = 0;
                     "_id" = 7;
                     artistId = 7;
                     bookingCount = 6;
                     completionTime = "00:10";
                     crd = "2019-09-20T05:55:22.872Z";
                     deleteStatus = 1;
                     description = test;
                     inCallPrice = "25.20";
                     outCallPrice = "50.30";
                     status = 1;
                     title = "Hair Color";
                     upd = "2019-09-20T05:55:22.872Z";
                 },
                                 {
                     "__v" = 0;
                     "_id" = 3;
                     artistId = 7;
                     bookingCount = 1;
                     completionTime = "00:10";
                     crd = "2019-09-18T09:58:28.441Z";
                     deleteStatus = 1;
                     description = Hello;
                     inCallPrice = "0.0";
                     outCallPrice = "30.0";
                     status = 1;
                     title = Hair;
                     upd = "2019-09-18T09:58:28.441Z";
                 },
                                 {
                     "__v" = 0;
                     "_id" = 5;
                     artistId = 7;
                     bookingCount = 0;
                     completionTime = "00:10";
                     crd = "2019-09-20T05:55:22.872Z";
                     deleteStatus = 1;
                     description = yes;
                     inCallPrice = "0.0";
                     outCallPrice = "69.0";
                     status = 1;
                     title = "Blonde Hair";
                     upd = "2019-09-20T05:55:22.872Z";
                 }
             );
             status = 1;
             title = "Hair Cut & Maintain";
             type = 0;
             "updated_date" = "2019-09-19T07:45:00.000Z";
         }
         */
    }
}


