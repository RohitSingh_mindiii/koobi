//
//  SubSubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class ExploreService : Copying {
       required init(original: ExploreService) {
            artistId = original.artistId
            isSelectLocal = original.isSelectLocal
            isSelected = original.isSelected
            businessId = original.businessId
            businessName = original.businessName
            categoryId = original.categoryId
            categoryName = original.categoryName
            serviceId = original.serviceId
            serviceName = original.serviceName
       }
       
       var artistId : Int = 0
       var isSelectLocal : Bool = false
       var isSelected : Bool = false
       
       var businessId : Int = 0
       var businessName : String = ""
       
       var categoryId : Int = 0
       var categoryName : String = ""
    
       var serviceId : Int = 0
       var serviceName : String = ""
    
    init(dict : [String : Any]){
        artistId = dict["artistId"] as? Int ?? 0
        serviceId = dict["_id"] as? Int ?? 0
        serviceName = dict["title"] as? String ?? ""
        
        businessId = dict["businessId"] as? Int ?? 0
        categoryId = dict["categoryId"] as? Int ?? 0
        businessName = dict[""] as? String ?? ""
        categoryName = dict[""] as? String ?? ""
    }
    
    /*
     {
         "__v" = 0;
         "_id" = 7;
         artistId = 7;
         bookingCount = 6;
         completionTime = "00:10";
         crd = "2019-09-20T05:55:22.872Z";
         deleteStatus = 1;
         description = test;
         inCallPrice = "25.20";
         outCallPrice = "50.30";
         status = 1;
         title = "Hair Color";
         upd = "2019-09-20T05:55:22.872Z";
     }
     */
}


