//
//  ExploreCollectionCell.swift
//  MualabCustomer
//
//  Created by Mac on 29/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ExploreCollectionCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var btnCategoryFromGrid: UIButton!
    @IBOutlet weak var imgVwPost: UIImageView!
    @IBOutlet weak var imgVwPlay : UIImageView?
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var Collection: UICollectionView!
    @IBOutlet weak var collectionView: UIView!
    var arrServices = [SubSubService]()
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.Collection.delegate = self
        //self.Collection.dataSource = self
    }
    func setDataInCollection(obj:feeds){
        arrServices.removeAll()
        arrServices = obj.arrTagedServices
        self.Collection.reloadData()
    }
}

//MARK:- Collection view Delegate Methods
extension ExploreCollectionCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if arrServices.count >= 3 {
//            return 2
//        }else{
//            return arrServices.count
//        }
        return arrServices.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        cell.btnService.superview?.tag = self.indexPath.row
        cell.btnService.tag = indexPath.row
        cell.lblUserName.textColor = UIColor.white
        cell.lblUserName.text = obj.subSubServiceName
        if indexPath.item == 0{
            print("new test")
            print(obj.subSubServiceName)
            print(cell.lblUserName.text)
        }
        //let a = cell.lblUserName.layer.frame.height
        cell.lblUserName.layer.cornerRadius = 11
        cell.lblUserName.layer.masksToBounds = true
        cell.lblBorder.layer.cornerRadius = 12
        cell.lblBorder.layer.masksToBounds = true

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 25
        cellWidth = CGFloat(self.collectionView.frame.size.width-4)
        let obj = arrServices[indexPath.row]
        let a = obj.subSubServiceName
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 14)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+12
        cellHeight = 25
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}
