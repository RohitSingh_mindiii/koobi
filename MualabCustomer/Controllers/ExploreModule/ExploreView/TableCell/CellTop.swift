//
//  CellTop.swift
//  MualabCustomer
//
//  Created by Mac on 03/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView



class CellTop: UITableViewCell {

    @IBOutlet weak var imgVwOuter: UIImageView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var imgCheckUnCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPostDetails : UILabel!

    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
