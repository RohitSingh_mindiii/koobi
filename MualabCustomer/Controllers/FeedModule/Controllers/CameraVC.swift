//
//  ViewController.swift
//  camera
//
//  Created by Natalia Terlecka on 10/10/14.
//  Copyright (c) 2014 imaginaryCloud. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import ImageIO
import MobileCoreServices
import Photos
import Alamofire

fileprivate enum CameraState {
    case ready, accessDenied, noDeviceFound, notDetermined
}

fileprivate enum CameraType {
    case Front
    case Back
}

fileprivate enum CameraFlashMode: Int {
    case off
    case on
    case auto
}

fileprivate enum CameraMode {
    case image
    case video
}

fileprivate enum CameraOutputQuality: Int {
    case low, medium, high
}

@objc protocol CameraVCDelegate {
    func recall()
}

class CameraVC: UIViewController {
    
    fileprivate var time : Int = 0
    fileprivate var timer : Timer?
    fileprivate var isRecordVideo = false
    fileprivate var arrVideoURL = [String]()
    fileprivate var arrThumbImage = [UIImage]()
    fileprivate var videoURL : URL?
    var timerNNN = Timer()
    fileprivate var sloatCount = 0
    
    fileprivate let imageOutput = AVCapturePhotoOutput()
    fileprivate var movieOutput = AVCaptureMovieFileOutput()
    fileprivate var captureSession = AVCaptureSession()
    fileprivate var currentDevice : AVCaptureDevice?
    
    fileprivate var cameraCheck = CameraType.Back
    fileprivate var flashMode = CameraFlashMode.on.rawValue
    fileprivate var cameraMode = CameraMode.image
    
    fileprivate var pivotPinchScale : CGFloat = 1.0
    
    @IBOutlet weak var cameraView:UIView!
    @IBOutlet weak var activityView:UIView!
    @IBOutlet weak var feedImageView:UIView!
    @IBOutlet weak var imgViewFeed:UIImageView!
    @IBOutlet weak var captureImageView:UIView!
    @IBOutlet weak var imgView:UIImageView!

    @IBOutlet var countDownLabel: UILabel!
    @IBOutlet weak var btnBack:UIButton!
    @IBOutlet weak var btnCapture:UIButton!
    @IBOutlet weak var btnFlash:UIButton!
    @IBOutlet weak var btnMode:UIButton!
    @IBOutlet weak var btnPlay:UIButton!
    @IBOutlet weak var btnPlayFeed:UIButton!
    @IBOutlet weak var btnSwipeCamera:UIButton!
    @IBOutlet weak var btnGallery:UIButton!
    var count = 0

    @IBOutlet weak var lblTimer:UILabel!

    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    weak var delegate:CameraVCDelegate?

}
//MARK: - View Hirarcey
extension CameraVC{
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.btnBack.transform = CGAffineTransform(rotationAngle:.pi)
        self.btnCapture.layer.borderWidth = 7.0
        self.btnCapture.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        ////
        self.feedImageView.isHidden = true
        ////
        self.checkCameraAuthorization { authorized in
            if authorized {
                // Proceed to set up and use the camera.
                self.createDeafultDeavice()
            } else {
                objAppShareData.showAlert(withMessage: "Enable camera access from settings", type: alertType.banner, on: self)
            }
        }
        //var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        
        if objAppShareData.isCameraForFeed {
            self.btnMode.isHidden = false
            self.btnGallery.isHidden = true
        }else{
            self.btnMode.isHidden = false
            self.btnGallery.isHidden = false
        }
        self.btnFlash.setImage(UIImage.customImage.flashOff, for: UIControl.State.selected)
        flashMode = CameraFlashMode.off.rawValue
        self.setFlashForVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSession.startRunning()
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession.stopRunning()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
        self.timerNNN.invalidate()
    }
    override var prefersStatusBarHidden: Bool { return true }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let anyTouch = touches.first
        self.focusTo(value: (anyTouch?.location(in: self.view))!)
    }
    
    override func didReceiveMemoryWarning() {
    }
    
}

//MARk: - Local Methods
fileprivate extension CameraVC{
    
    @objc func updateTimer() {
        if(count > 0) {
            count = count-1
            let str = String(count)
            self.btnCapture.setTitle(str, for: UIControl.State.normal)
        }
    }
    
    func addCameraToView() {
        DispatchQueue.main.async {
            let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            previewLayer.frame = self.view.layer.bounds
            self.cameraView.layer.addSublayer(previewLayer)
        }
    }
    
    func createDeafultDeavice() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
        
            self.initilizeFrontAndBackCamera()
            self.addCameraToView()
            self.addPinchZoomToCamera()
            if self.cameraMode == CameraMode.image{
                self.configurePhotoOutput()
            }else{
                self.configureVideoOutput()
            }
        }
    }
    
    func addPinchZoomToCamera() {
        // PINCH Gesture
        DispatchQueue.main.async {
            let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchZoom))
            self.cameraView.isUserInteractionEnabled = true
            self.cameraView.addGestureRecognizer(pinchGesture)
        }
    }
    
    // Zoom function to camera
    @objc func pinchZoom(gesture: UIPinchGestureRecognizer) {
        
        let device: AVCaptureDevice = self.currentDevice!
        do {
            try device.lockForConfiguration()
            switch gesture.state {
            case .began:
                self.pivotPinchScale = device.videoZoomFactor
            case .changed:
                var factor = self.pivotPinchScale * gesture.scale
                factor = max(1, min(factor,4.0))
                device.videoZoomFactor = factor
            default:
                break
            }
            device.unlockForConfiguration()
        } catch {
            // handle exception
        }
    }
    // Auto focus
    func focusTo(value : CGPoint) {
        if let device = self.currentDevice {
            do{
              try device.lockForConfiguration()
                if (device.isFocusPointOfInterestSupported){
                    device.focusPointOfInterest = value
                }
                if (device.isFocusModeSupported(.autoFocus)){
                    device.focusMode = .autoFocus
                }
                device.unlockForConfiguration()
            }catch{
                // handle exception error
            }
        }
    }
    
    // Initilize front and back camera
    func initilizeFrontAndBackCamera(){
        //1
        for input in self.captureSession.inputs{
            self.captureSession.removeInput(input)
        }
        
        if self.cameraCheck == CameraType.Front{
            
            if let camera = AVCaptureDevice.default(.builtInDualCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.front){
                self.currentDevice = camera
                if let input = try? AVCaptureDeviceInput.init(device:camera){
                    captureSession.addInput(input)
                }
            }else if let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.front){
                self.currentDevice = camera
                if let input = try? AVCaptureDeviceInput.init(device:camera){
                    captureSession.addInput(input)
                }
            }
        }else{
            if let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.back){
            self.currentDevice = camera
                if let input = try? AVCaptureDeviceInput.init(device:camera){
                    captureSession.addInput(input)
                }
            }
        }
        
        // Add audio input.
        do {
            let audioDevice = AVCaptureDevice.default(for: .audio)
            let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice!)
            if captureSession.canAddInput(audioDeviceInput) {
                captureSession.addInput(audioDeviceInput)
            } else {
                //"Could not add audio device input to the session"
            }
        } catch {
            //"Could not create audio device input: \(error)"
        }
        // Lock Configration
        do {
            try self.currentDevice?.lockForConfiguration()
            self.currentDevice?.unlockForConfiguration()
        } catch{
            //error.localizedDescription
        }
    }
    
}

//MARK: - IBAction
fileprivate extension CameraVC{
    
    @IBAction func btnBack(_ sender:Any){
        objAppShareData.isCameraForFeed = false
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        //kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal
        transition.subtype = CATransitionSubtype.fromRight
        //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.popViewController(animated: false)
        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: {
            self.delegate?.recall()
        })
        //self.delegate?.recall()
    }
    
    @IBAction func btnTakePicture(_ sender:Any){
        
        var isAuthorized = false
        self.checkCameraAuthorization { authorized in
            if authorized {
                isAuthorized = true
            } else {
                isAuthorized = false
            }
        }
        if !isAuthorized{
            objAppShareData.showAlert(withMessage: "Enable camera access from settings", type: alertType.banner, on: self)
            return
        }
        
        count = 0
        timerNNN.invalidate()
        
        if self.cameraMode == CameraMode.image{
            self.capturePhoto()
        }else{
            if isRecordVideo{
                DispatchQueue.main.async {
                    self.timer?.invalidate()
                    self.captureSession.stopRunning()
                    self.movieOutput.stopRecording()
                    self.activityView.isHidden = false
                }
                self.showControlls()
                self.btnCapture.setTitle("REC", for: UIControl.State.normal)
                self.btnCapture.setImage(UIImage(), for: UIControl.State.normal)
            }else{
                isRecordVideo = true
                self.captureVideo()
                self.hideControlls()
                
                /////
                count = 60
                timerNNN = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
//                if count == 0 {
//                   timer.invalidate()
//                }
                /////
                
                //self.btnCapture.setTitle("", for: UIControlState.normal)
                //self.btnCapture.setImage(UIImage(named:"stopRec"), for: UIControlState.normal)
            }
        }
    }
    
    @IBAction func btnSwipeCamera(_ sender:Any){
        var isAuthorized = false
        self.checkCameraAuthorization { authorized in
            if authorized {
                isAuthorized = true
            } else {
                isAuthorized = false
            }
        }
        if !isAuthorized{
            objAppShareData.showAlert(withMessage: "Enable camera access from settings", type: alertType.banner, on: self)
            return
        }
        if cameraCheck == CameraType.Front{
            cameraCheck = CameraType.Back
        }else{
            cameraCheck = CameraType.Front
        }
        self.initilizeFrontAndBackCamera()
    }
    
    @IBAction func btnSetFlashMode(){
        var isAuthorized = false
        self.checkCameraAuthorization { authorized in
            if authorized {
                isAuthorized = true
            } else {
                isAuthorized = false
            }
        }
        if !isAuthorized{
            objAppShareData.showAlert(withMessage: "Enable camera access from settings", type: alertType.banner, on: self)
            return
        }
        
        if (self.currentDevice?.isFlashAvailable)! {
    
            switch flashMode {
//            case CameraFlashMode.auto.rawValue:
//                flashMode = CameraFlashMode.off.rawValue
//                self.btnFlash.setImage(UIImage.customImage.flashOff, for: UIControlState.selected)
            
            case CameraFlashMode.on.rawValue:
//                flashMode = CameraFlashMode.auto.rawValue
//                self.btnFlash.setImage(UIImage.customImage.flashAuto, for: UIControlState.selected)
                
                flashMode = CameraFlashMode.off.rawValue
                self.btnFlash.setImage(UIImage.customImage.flashOff, for: UIControl.State.selected)
            
            case CameraFlashMode.off.rawValue:
                flashMode = CameraFlashMode.on.rawValue
                self.btnFlash.setImage(UIImage.customImage.flashOn, for: UIControl.State.selected)
            
            default:
               //"No Flash"
                break
            }
            setFlashForVideo()
        }
    }
    
    @IBAction func btnCloseCaptureImage(){
        self.feedImageView.isHidden = true
        self.captureImageView.isHidden = true
        time = 0
        self.lblTimer.text = "00:00"
        isRecordVideo = false
        self.showControlls()
        if self.cameraMode == CameraMode.image{
            self.configurePhotoOutput()
            self.btnMode.setImage(UIImage.customImage.video, for: UIControl.State.normal)
            self.lblTimer.isHidden = true
            self.btnCapture.setTitle("", for: UIControl.State.normal)
            self.btnCapture.setImage(UIImage(), for: UIControl.State.normal)
        }else{
            self.configureVideoOutput()
            self.btnMode.setImage(UIImage.customImage.camera, for: UIControl.State.normal)
            self.lblTimer.isHidden = false
            self.btnCapture.setTitle("REC", for: UIControl.State.normal)
            self.btnCapture.setImage(UIImage(), for: UIControl.State.normal)
        }
        captureSession.startRunning()
        
        if objAppShareData.isCameraForFeed {
            self.btnMode.isHidden = false
            self.btnGallery.isHidden = true
        }else{
            self.btnMode.isHidden = false
            self.btnGallery.isHidden = false
        }
    }
    
    @IBAction func addStory(_ sender:Any){
        //self.btnBack(0)
        if isRecordVideo{
            objActivity.startActivityIndicator()
            self.createSloatFromVideo(url:self.videoURL!)
        }else{
            if objAppShareData.isCameraForFeed{
                objAppShareData.imgForPostFeed = self.imgViewFeed.image!
                dismiss(animated: false) {
                }
            }else{
               callWebserviceForUploadStory()
            }
        }
    }
    
    @IBAction func changeMode(_ sender:Any){
            time = 0
            self.lblTimer.text = "00:00"
            isRecordVideo = false
            if cameraMode == CameraMode.image{
                cameraMode = CameraMode.video
                self.btnMode.setImage(UIImage.customImage.camera, for: UIControl.State.normal)
                self.lblTimer.isHidden = false
                self.btnCapture.setTitle("REC", for: UIControl.State.normal)
            }else{
                cameraMode = CameraMode.image
                self.btnMode.setImage(UIImage.customImage.video, for: UIControl.State.normal)
                self.lblTimer.isHidden = true
                self.btnCapture.setTitle("", for: UIControl.State.normal)
                self.btnCapture.setImage(UIImage(), for: UIControl.State.normal)
            }
            
            //Change Mode
            
            if self.cameraMode == CameraMode.image{
                self.configurePhotoOutput()
            }else{
                self.configureVideoOutput()
            }
    }
    
    @IBAction func playVideo(_ sender:Any){
        self.addVideo(toVC: self.videoURL!)
    }
    
    @IBAction func btnGallery(_ sender:Any){
        self.selectDataFromGallery()
    }
}

//MARK: - custom Methods
fileprivate extension CameraVC{
    
    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .authorized:
            //The user has previously granted access to the camera.
            completionHandler(true)
            
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(for:.video, completionHandler: { success in
                completionHandler(success)
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }

    func photoSetting() -> AVCapturePhotoSettings{
        let photoSettings = AVCapturePhotoSettings()
        photoSettings.isAutoStillImageStabilizationEnabled = true
        if (self.currentDevice?.isFlashAvailable) ?? false {
            photoSettings.flashMode = AVCaptureDevice.FlashMode(rawValue:flashMode) ?? .off
        }
        photoSettings.isHighResolutionPhotoEnabled = true
        return photoSettings
    }
    
    func configurePhotoOutput(){
        
        for output in self.captureSession.outputs{
            self.captureSession.removeOutput(output)
        }
        
        captureSession.beginConfiguration()
        // Create and configure the photo output.
        self.imageOutput.isHighResolutionCaptureEnabled = true
        self.imageOutput.isLivePhotoCaptureEnabled = self.imageOutput.isLivePhotoCaptureSupported
        self.imageOutput.setPreparedPhotoSettingsArray([AVCapturePhotoSettings.init(format: [AVVideoCodecKey:AVVideoCodecType.jpeg])], completionHandler: nil)
        // Make sure inputs and output can be added to session.
        if captureSession.canAddOutput(self.imageOutput) {
            captureSession.addOutput(self.imageOutput)
            self.captureSession.sessionPreset = .high
        }
        captureSession.commitConfiguration() //5
    }
    
    func configureVideoOutput(){
        
        for output in self.captureSession.outputs{
            self.captureSession.removeOutput(output)
        }
        
        captureSession.beginConfiguration()
        
        if (captureSession.canAddOutput(movieOutput) == true) {
            captureSession.addOutput(movieOutput)
            
            self.captureSession.sessionPreset = .hd1280x720
            
            if let connection = movieOutput.connection(with: .video) {
                if connection.isVideoStabilizationSupported {
                    connection.preferredVideoStabilizationMode = .auto
                }
            }
        }
        captureSession.commitConfiguration() //5
    }
    
    func hideControlls(){
        self.btnMode.isHidden = true
        self.btnSwipeCamera.isHidden = true
        self.btnGallery.isHidden = true
    }
    
    func showControlls(){
        if objAppShareData.isCameraForFeed {
            self.btnGallery.isHidden = true
        }else{
            self.btnGallery.isHidden = false
        }
        self.btnMode.isHidden = false
        self.btnSwipeCamera.isHidden = false
    }
    
    func setFlashForVideo(){
        if let device = self.currentDevice {
            do{
                try device.lockForConfiguration()
                
            if device.isTorchModeSupported(device.torchMode){
                switch flashMode {
                case CameraFlashMode.auto.rawValue:
                    device.torchMode = .auto
                case CameraFlashMode.on.rawValue:
                    device.torchMode = .on
                case CameraFlashMode.off.rawValue:
                    device.torchMode = .off
                default:
                    //"No Flash"
                    break
                
                    }
                }
                device.unlockForConfiguration()
            } catch{
                //error.localizedDescription)
            } // 4
        }
        
    }
    
    func clearOperationData(){
        for strUrl in self.arrVideoURL {
            self.deleteFile(filePath: URL(string: strUrl)! as NSURL)
        }
        if let url = self.videoURL{
            self.deleteFile(filePath: url as NSURL)
        }
    }
    
    func selectDataFromGallery(){
        let picker = UIImagePickerController()
        picker.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for:.photoLibrary) ?? [String]()
            picker.mediaTypes = ["public.movie","public.video","public.mpeg","public.avi","public.mpeg-4","public.image"]
            picker.videoQuality = .typeMedium
            picker.videoMaximumDuration = 60
            //picker.allowsEditing = true
            picker.allowsEditing = false
            picker.modalPresentationStyle = .fullScreen
            self.present(picker, animated: true)
        }
    }
}
extension CameraVC:AVCapturePhotoCaptureDelegate{
    
    func capturePhoto() {
        self.imageOutput.capturePhoto(with: self.photoSetting(), delegate: self)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        // capture image finished
        if let imageData = photo.fileDataRepresentation() {
            if let img = UIImage(data: imageData){
                
                 ////
                 if self.currentDevice?.position == AVCaptureDevice.Position.back {
                     //let image = UIImage(cgImage: img.cgImage!, scale: 1.0, orientation: .right)
                     let image = img.fixedOrientation()
                     self.imgView.image = image
                    let i = self.cropToBounds(image: self.imgView.image!, width: 1100, height: 1100)
                    self.imgViewFeed.image = i
                 }else if self.currentDevice?.position == AVCaptureDevice.Position.front {
                     let image = UIImage(cgImage: img.cgImage!, scale: 1.0, orientation: .leftMirrored)
                     self.imgView.image = image
                     let i = self.cropToBounds(image: self.imgView.image!, width: 1100, height: 1100)
                     self.imgViewFeed.image = i
                 }
                 ////
                
                if objAppShareData.isCameraForFeed{
                   self.feedImageView.isHidden = false
                }else{
                   self.captureImageView.isHidden = false
                }
                self.btnPlay.isHidden = true
                self.btnPlayFeed.isHidden = true
                self.captureSession.stopRunning()
            }
        }
    }
    
    func addVideo(toVC url: URL) {
        // create an AVPlayer
        let player = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = player
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: {() -> Void in
            controller.player?.play()
        })
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeImageNewForFeed(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth
        let newHeight = newWidth
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
}

extension CameraVC : AVCaptureFileOutputRecordingDelegate{
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        do {
            let imageData = try Data.init(contentsOf:outputFileURL)
            self.videoURL = outputFileURL
            self.imgView.image = objAppShareData.generateImage(fromURL: outputFileURL, withSize: CGSize(width: 320, height: 560))
            self.imgViewFeed.image = objAppShareData.generateImage(fromURL: outputFileURL, withSize: CGSize(width: self.view.frame.width, height: self.view.frame.width))
            if objAppShareData.isCameraForFeed {
               self.feedImageView.isHidden = false
            }else{
               self.captureImageView.isHidden = false
            }
            self.btnPlay.isHidden = false
            self.btnPlayFeed.isHidden = false
            self.arrVideoURL.removeAll()
            self.activityView.isHidden = true
        } catch {
            //print(error)
        }
    }
    
    func captureVideo(){
        self.setFlashForVideo()
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let fileURL = documentsDirectory2.appendingPathComponent("outPut.mov")
        self.deleteFile(filePath: fileURL! as NSURL)
        self.changeRecordingFormats()
        movieOutput.maxRecordedDuration = CMTime(seconds:60, preferredTimescale:24)
        movieOutput.startRecording(to: fileURL!, recordingDelegate: self)
        self.addTimer()
    }
    
    func changeRecordingFormats(){
            if let device = self.currentDevice {
                do{
                    try device.lockForConfiguration()
                    device.activeVideoMinFrameDuration = CMTimeMake(value: 1,timescale: 24)
                    device.activeVideoMaxFrameDuration = CMTimeMake(value: 1,timescale: 24)
                    device.unlockForConfiguration()
                } catch{
                    //print(error.localizedDescription)
                } // 4
            }
    }
}

//MARK: - Timer Methods
fileprivate extension CameraVC{
    func addTimer(){
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    @objc func update() {
        time = time + 1
        self.lblTimer.text = self.secondsToMinutesSeconds(seconds:time)
        ////
//        let str = String(count-1)
//        self.btnCapture.setTitle(str, for: UIControlState.normal)
        ////
        if time == 60{
            self.timer?.invalidate()
            self.activityView.isHidden = false
        }
    }
    
    func secondsToMinutesSeconds (seconds : Int) -> String {
        var timeString  = "00:00"
        
        let (min,sec) = ((seconds % 3600) / 60, (seconds % 3600) % 60)
        
        // time format
        if min < 10 {
            if sec < 10{
                timeString = "0\(min):0\(sec)"
            }else{
                timeString = "0\(min):\(sec)"
            }
        }else{
            if sec < 10{
                timeString = "\(min):0\(sec)"
            }else{
                timeString = "\(min):\(sec)"
            }
        }
        return timeString
    }
    
}
//MARK: - Webservices
fileprivate extension CameraVC{
    func callWebserviceForUploadStory(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        captureSession.stopRunning()
        
        
        var id = 0
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            id = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            id = userInfo["_id"] as? Int ?? 0
        }
        
        let dicParam = ["type":"image",
                        "videoThumb":"",
                        "userId":id] as [String : Any]
        var i = UIImage()
        
        if imgView.image!.size.width > 1000{
            i = self.resizeImage(image: imgView.image!, newWidth: 1000)
        }else{
            i = self.imgView.image!
        }
        
        let imgData = i.jpegData(compressionQuality: 0.2)
        
        objServiceManager.uploadMultipartData(strURL: WebURL.addMyStory, params: dicParam as [String : AnyObject] , imageData: imgData, fileName: "file.jpg", key: "myStory", mimeType: "image/jpg", success: { response in
            
            if response["status"] as? String ?? "" == "success"{
                self.btnBack(0)
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            print(error)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

    func callWebserviceForAddVideo() {
     
        var id = ""
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            let myId = dicUser["_id"] as? Int ?? 0
            id = "\(myId)"
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            let myId = userInfo["_id"] as? Int ?? 0
            id = "\(myId)"
        }
        
        let dicParam = ["type":"video",
                        "userId":id] as [String : Any]

        let url = WebURL.BaseUrl+WebURL.addMyStory
        var strAuth = ""
        if UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)==nil {
            strAuth=""
        }else{
            strAuth=UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
        }
        
        let headers = ["authtoken" : strAuth]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            for strURL in self.arrVideoURL{
                do {
                    let imageData = try Data.init(contentsOf: URL(string: strURL)!)
                    multipartFormData.append(imageData,
                                             withName:"myStory",
                                             fileName:"file.mp4",
                                             mimeType:"video/mp4")
                } catch {
                    //print(error)
                }
            }
           
            for image in self.arrThumbImage{
                if let imageData = image.jpegData(compressionQuality: 0.1) {
                    multipartFormData.append(imageData,
                                             withName:"videoThumb",
                                             fileName:"image.jpg",
                                             mimeType:"image/jpeg")
                }
            }
            
            for (key, value) in dicParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)!, withName: key)
            }},
                         to:url,
                         headers:headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { responseObject in
                                    objActivity.stopActivity()
                                    if responseObject.result.isSuccess {
                                        do {
                                            let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                            if dictionary["status"] as? String ?? "" == "success"{
                                                let msg = dictionary["message"] as? String ?? ""
                                                if msg == message.invalidToken{
                                                    objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on:self)
                                                    return
                                                }
                                                self.btnBack(0)
                                                self.clearOperationData()
                                            }else{
                                                if let msg = dictionary["message"] as? String{
                                                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                                                }
                                            }
                                        }catch{
                                            
                                        }
                                    }
                                    if responseObject.result.isFailure {
                                        let error : Error = responseObject.result.error!
                                        objWebserviceManager.StopIndicator()
                                        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                                        //print(error)
                                    }
                                }
                            case .failure(let encodingError):
                                //print(encodingError)
                                objWebserviceManager.StopIndicator()
                                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                            }
        })
    }


}

fileprivate extension CameraVC{
    
    //MARK: - Make sloat from video
    
    func createSloatFromVideo( url:URL){
        
        // off flash
        
        do{
            try self.currentDevice?.lockForConfiguration()
            if (self.currentDevice?.isTorchModeSupported((self.currentDevice?.torchMode)!))!{
               self.currentDevice?.torchMode = .off
            }
            self.currentDevice?.unlockForConfiguration()
        } catch{
            //print(error.localizedDescription)
        } // 4
        
        captureSession.stopRunning()
        let asset = AVAsset.init(url:url)
        let videoSec = CMTimeGetSeconds(asset.duration)
        let seconds = Int(videoSec)
        let (min,sec) = ((seconds % 3600) / 60, (seconds % 3600) % 60)
        if min > 0{
            var sT = 0.0
            var eT = 60.0
            sloatCount = 1
            for indx in 0..<1{
                let name = "renderedVideo\(indx).mp4"
                self.encodeVideo(videoURL: url, withFileName: name, startTime: sT, endTime: eT)
                sT = eT+1
                eT = sT+60
            }
//            if sec > 0{
//                sloatCount = min+1
//                let name = "renderedVideo\(min).mp4"
//                sT = Double(seconds+1 - sec)
//                eT = Double(seconds)
//                self.encodeVideo(videoURL: url, withFileName: name, startTime: sT, endTime: eT)
//            }
        }else{
            sloatCount = 1
            let name = "renderedVideo\(min).mp4"
            let sT = 0
            let eT = Double(seconds)
            self.encodeVideo(videoURL: url, withFileName: name, startTime: Double(sT), endTime: eT)
        }
    }
    
    //MARK: - Encode Video
    func encodeVideo(videoURL: URL , withFileName name:String, startTime:Double , endTime:Double)  {

        let avAsset = AVURLAsset.init(url: videoURL, options: nil)
        //Create Export session
        let
        exportSession = AVAssetExportSession(asset:avAsset, presetName: AVAssetExportPresetPassthrough)
        //Creating temp path to save the converted video
                
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4")?.absoluteString
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = documentsDirectory2.appendingPathComponent(name)
        deleteFile(filePath: filePath! as NSURL)
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: myDocumentPath!) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath!)
            }
            catch let error {
                //print(error)
            }
        }
        
        exportSession!.outputURL = filePath
        exportSession!.outputFileType = AVFileType.mp4
        exportSession!.shouldOptimizeForNetworkUse = true
        //Deside Time Range
        let sTimeRange = CMTime(seconds: startTime, preferredTimescale: 1000)
        let eTimeRange = CMTime(seconds:endTime, preferredTimescale: 1000)
        let timeRange  = CMTimeRange.init(start: sTimeRange, end: eTimeRange)
        exportSession?.timeRange = timeRange
        
        exportSession!.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession!.status {
                
            case .failed:
                break
                //print(exportSession?.error as Any)
            case .cancelled:
                  break
                //print("Export canceled")
            case .completed:
                //Video conversion finished
                
                //self.addVideo(toVC:(exportSession?.outputURL)!)
                DispatchQueue.main.async{
                    self.arrVideoURL.append(String(describing: (exportSession?.outputURL)!))
                    self.arrVideoURL = self.arrVideoURL.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                    
                    if self.arrVideoURL.count == self.sloatCount{
                        for strUrl in self.arrVideoURL {
                            let image = objAppShareData.generateImage(fromURL: URL(string:strUrl)!, withSize: CGSize(width: 420, height: 560))
                            self.arrThumbImage.append(image)
                        }
                        if self.arrThumbImage.count == self.sloatCount{
                            
                            ////
                            if objAppShareData.isCameraForFeed {
                                /*
                                let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
                                let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
                                objNewPost.isVideo = true
                                objNewPost.videoURL = URL.init(string: self.arrVideoURL[0])
                                objNewPost.imgFeed = self.arrThumbImage[0]
                                objNewPost.imgThumb = self.arrThumbImage[0]
                                */
                objAppShareData.imgForPostFeed = self.arrThumbImage[0]
                objAppShareData.videoUrlForPostFeed = URL.init(string: self.arrVideoURL[0])
                objAppShareData.isVideoPostFeed = true
                objActivity.stopActivity()
                self.dismiss(animated: false) {
                }
                ////
            }else{
                self.callWebserviceForAddVideo()
            }
           }
          }
         }
        default:
            break
        }
        })
    }
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    func presentVideoSegmentVC(){
        let objPlayVideoSegmentVC = storyboard?.instantiateViewController(withIdentifier: "playVideoSegmentVC") as! playVideoSegmentVC
        objPlayVideoSegmentVC.arrVideoURL = self.arrVideoURL
        objPlayVideoSegmentVC.currentIndex = 0
        objPlayVideoSegmentVC.modalPresentationStyle = .fullScreen
        self.present(objPlayVideoSegmentVC, animated: true, completion: nil)
    }
}

//MARK: - UIImagepickerDelegate and datasource
extension CameraVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let mediaType = info[UIImagePickerController.InfoKey.mediaType]
        
        if mediaType as? String ?? "" == "public.image"{
            let pickedImage = info[.originalImage]
            self.imgView.image = pickedImage as? UIImage
            self.captureImageView.isHidden = false
            self.btnPlay.isHidden = true
            self.btnPlayFeed.isHidden = true
            self.isRecordVideo = false
            self.captureSession.stopRunning()
        }else{
            self.isRecordVideo = true
            let outputFileURL = info[UIImagePickerController.InfoKey.mediaURL]
            self.videoURL = outputFileURL as? URL
            self.imgView.image = objAppShareData.generateImage(fromURL: self.videoURL!, withSize: CGSize(width: 320, height: 560))
            self.imgViewFeed.image = objAppShareData.generateImage(fromURL: self.videoURL!, withSize: CGSize(width: 320, height: 560))
            if objAppShareData.isCameraForFeed {
                self.feedImageView.isHidden = false
            }else{
                self.captureImageView.isHidden = false
            }
            self.btnPlay.isHidden = false
            self.btnPlayFeed.isHidden = false
            self.arrVideoURL.removeAll()
            self.activityView.isHidden = true
        }
        //self.userImage = pickedImage
        //imgProfile.image = pickedImage
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //  isFromImagePicker = true
        dismiss(animated: true)
    }
}
