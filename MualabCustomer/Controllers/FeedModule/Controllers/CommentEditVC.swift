//
//  CommentEditVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/10/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CommentEditVC: UIViewController ,UITextViewDelegate{
    
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var imgUser: UIImageView!
    var objFeeds: feeds?
    var objHoldIndex = commentUser(dict: ["":""])

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtComment.delegate = self
        self.addAccesorryToKeyBoard()

        self.txtComment.text = objHoldIndex?.comment
        if objHoldIndex?.profileImage != "" {
            if let url = URL(string: objHoldIndex?.profileImage ?? ""){
                
                //self.imgUser.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }

    }
    

    @IBAction func btnUpdateAction(_ sender: UIButton){
        txtComment.text = txtComment.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if txtComment.text == ""{
            objAppShareData.showAlert(withMessage: "Please enter some text", type: alertType.bannerDark,on: self)
        }else {

            var myId = 0
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myId = dict["_id"] as? Int ?? 0
            }
            
            let dicParam = ["feedId":"\(self.objFeeds?._id ?? 0)",
                                        "comment": self.txtComment.text ?? "",
                                        "userId":"\(myId)",
                                        "id":String(objHoldIndex?._id ?? 0),
                                        "postUserId":"\(self.objFeeds?.userInfo?._id ?? 0)",
                                        "type":"text",
                                        ] as [String : Any]
            callWebserviceFor_PostTextComment(dicParam:dicParam)
        }
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }

    @IBAction func btnBackAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    func callWebserviceFor_PostTextComment(dicParam:[String:Any]){
        objWebserviceManager.StartIndicator()

        objWebserviceManager.requestPostMultipartData(strURL: WebURL.editComment, params: dicParam, success: { response in
             objWebserviceManager.StopIndicator()
            let strSucessStatus = response["status"] as? String ?? ""
            if strSucessStatus == k_success{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    self.navigationController?.popViewController(animated: true)
                }
                print(response)
            } else{
                objWebserviceManager.StopIndicator()
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objWebserviceManager.StopIndicator()
             objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtComment.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtComment.endEditing(true)
        self.view.endEditing(true)
    }
}
