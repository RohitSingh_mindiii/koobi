//
//  FeedReportVC.swift
//  MualabCustomer
//
//  Created by Mac on 23/10/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import DropDown

class FeedReportVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
   
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    
    var strLink = ""
    var feedId = ""
    let dropDown = DropDown()
    @IBOutlet weak var lblLink: UILabel!
    var objModel = feeds(dict: [:])
    fileprivate var strMyChatId = ""
    fileprivate var arrReasons = [String]()

    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callWebserviceFor_getReportReasons()
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.txtSelectReason.delegate = self
        self.txtViewDescription.delegate = self
        ref = Database.database().reference()
        setuoDropDownAppearance()
        self.txtViewDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtViewDescription.tintColorDidChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblLink.text = self.strLink
        let a = self.objModel?._id ?? 0
        self.lblLink.text = WebURL.BaseUrl+WebURL.feedDetails + "/" + String(a)
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtViewDescription {
            if (text.count) != 0
            { searchString = self.txtViewDescription.text! + (text).uppercased()
                newLength = (self.txtViewDescription.text?.count)! + text.count - range.length
            }
            else {
                //searchString = (self.txtViewDescription.text as NSString?)?.substring(to: (self.txtViewDescription.text?.count)! - 1).uppercased()
            }
            if newLength <= 500{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtSelectReason {
            if (string.count ) != 0
            { searchString = self.txtSelectReason.text! + (string).uppercased()
                newLength = (self.txtSelectReason.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtSelectReason.text as NSString?)?.substring(to: (self.txtSelectReason.text?.count)! - 1).uppercased()
            }
            if newLength <= 50{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSelectReason{
            txtViewDescription.becomeFirstResponder()
        }
        return true
    }
    
    
    @IBAction func btnSubmitReportAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtSelectReason.text = self.txtSelectReason.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtViewDescription.text = self.txtViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if self.txtSelectReason.text?.count == 0{
            objAppShareData.showAlert(withMessage: "Please enter report reason", type: alertType.bannerDark,on: self)
        }else if self.txtViewDescription.text?.count == 0{
            objAppShareData.showAlert(withMessage: "Please enter report description", type: alertType.bannerDark,on: self)
        }else{
            self.reportAction()
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)
    }
    /*
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 45
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.cornerRadius = 2
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
    }
    */
   
    @IBAction func btnReasonAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let objGetData = self.arrReasons.map { $0 }
        if self.arrReasons.count == 0{
           objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.direction = .bottom
        dropDown.dataSource = objGetData
        dropDown.width = self.view.frame.size.width - 50
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtSelectReason.text = item
        }
        dropDown.show()
    }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let objGetData = self.arrReasons.map { $0 }
        if self.arrReasons.count == 0{
            objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        objAppShareData.arrTBottomSheetModal = objGetData
        
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Report Reason"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            let strText = strongSelf.arrReasons[Int(str) ?? 0]
            strongSelf.txtSelectReason.text = strText
        }
        self.present(scrollingNC, animated: false, completion: nil)
        
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Report Reason"
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            let strText = strongSelf.arrReasons[Int(str) ?? 0]
            strongSelf.txtSelectReason.text = strText
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    func reportAction(){
        
            let time = ServerValue.timestamp()
        

        let dict = ["feedId":self.objModel?._id ?? "",
                    "feedOwnerId":self.objModel?.userId ?? "",
                    "link":self.lblLink.text,
                    "myId":self.strMyChatId ,//
                    "reason":self.txtSelectReason.text ?? "",//
                    "description":self.txtViewDescription.text ?? "",//
                    "timeStamp":time] as [String : Any]
            print(dict)
        
            let id = String(self.objModel?._id ?? 0)
        //self.ref.child("feed_report").child(id).childByAutoId().setValue(dict)
        self.ref.child("feed_report").childByAutoId().setValue(dict)
        objAppShareData.showAlert(withMessage: "Report sent successfully", type: alertType.bannerDark,on: self)
            self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: - Webservices
    func callWebserviceFor_getReportReasons(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters = ["type" : "feed"] as [String:Any]
        objWebserviceManager.requestGetForJson(strURL: WebURL.reportReason, params: parameters , success: { response in
            print(response)
            objWebserviceManager.StopIndicator()
            self.arrReasons.removeAll()
            let strSucessStatus = response["status"] as? String
            let strMessage = response["message"] as? String
            if strSucessStatus == k_success{
                let arrReason = response["data"] as? [[String:Any]]
                for dict in arrReason!{
                    let strReason = dict["title"] as? String
                    self.arrReasons.append(strReason!)
                }
            }else{
                objAppShareData.showAlert(withMessage: strMessage!, type: alertType.bannerDark,on: self)
            }
        }) { (error) in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

