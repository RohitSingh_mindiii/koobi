//
//  HomeVC.swift
//  Mualab
//
//  Created by MINDIII on 10/24/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Accounts
import AVFoundation
import AVKit
import Social
import Alamofire
import AlamofireImage
import FirebaseAuth
import TLPhotoPicker
import Photos
import Firebase
import SDWebImage

let colorGray = UIColor(red: 148.0 / 255.0, green: 148.0 / 255.0, blue: 148.0 / 255.0, alpha: 1.0)
let colorBtnBG = UIColor(red: 0.9271422029, green: 0.1869596243, blue: 0.4465353489, alpha: 1)
let colorBtnBorder = UIColor(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
let defaults = UserDefaults.standard

class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, AVPlayerViewControllerDelegate,UITextFieldDelegate,ShowStoriesVCDelegate{
              
    // Reload user story data
    func recall() {
        self.callWebserviceFor_getStoryUsers()
    }
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    var fromTextField = false
    fileprivate var strStatusfeed:String? = ""
    fileprivate var strStatus:String? = ""
    fileprivate var strSearchValue : String = ""
    let cellIdText = "Text"
    let cellIdImageVideo = "ImageVideo"
    fileprivate var arrUsers = [storyUser]()
    fileprivate var arrFeedImages = [UIImage]()
    fileprivate var suggesionArray = [suggessions]()
    var arrPeopleTagSuggesionArray = [suggessions]()
    fileprivate var isVideo = false
    fileprivate var isFromImagePicker = false
    fileprivate var isMyStoryIsAdded = false
    fileprivate var isNavigate = false
    fileprivate var isViewPostShow = false
    fileprivate var Image: UIImage?
    fileprivate var mp4VideoURL: URL?
    fileprivate var pageNo: Int = 0
    fileprivate var myStoryCount: Int = 0
    fileprivate var lastSelectFeedIndex: Int = 0
    fileprivate var strType = ""
    fileprivate var suggesionType = ""
    fileprivate var videoData: Data?
    fileprivate var tblView: UITableView?
    fileprivate var fileSize:Float = 0.0
    fileprivate var pointToScroll : CGFloat = 0.0
    fileprivate var myId:Int = 0
    fileprivate var isLoading = false
    fileprivate var isBackNavigation = false
    fileprivate var totalCount = 0
    fileprivate var indexLastViewMoreView = 0
    fileprivate var dataToAddCount = 10
    
    fileprivate let defultDescription = "Something in your mind?"
    fileprivate var currentWordIndex = 0
    
    @IBOutlet weak var viewUploadLoader: UIView!
    @IBOutlet weak var btnTextPostVC: UIButton!
    @IBOutlet weak var imgProfileHeader: UIImageView!
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    @IBOutlet weak var lblNoResults: UIView!
    @IBOutlet weak var lblImageCount: UILabel!
    //
    @IBOutlet weak var imgDot1: UIImageView!
    @IBOutlet weak var imgDot2: UIImageView!
    @IBOutlet weak var imgFeed: UIImageView!
    //
    @IBOutlet weak var addPostView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var imageThumbView: UIView!
    @IBOutlet weak var suggesionView: UIView!
    //
    @IBOutlet weak var activityUpload: UIActivityIndicatorView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var userCollection: UICollectionView!
    
    @IBOutlet weak var lblBusinessInvitationCount: UILabel!
    @IBOutlet weak var viewBusinessInvitationCount: UIView!
    //
    @IBOutlet weak var tblFeeds: UITableView!
    @IBOutlet weak var tblSuggesion: UITableView!
    //
    @IBOutlet weak var btnImages: UIButton!
    @IBOutlet weak var btnVideos: UIButton!
    @IBOutlet weak var btnFeeds: UIButton!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    //@IBOutlet weak var btnBack: UIButton!
    //
    @IBOutlet weak var optionButtonStackView: UIStackView!
    //
    @IBOutlet weak var txtDescription: UITextField!
    //
    @IBOutlet weak var feedsTableHeight: NSLayoutConstraint!
    //sky
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    @IBOutlet weak var constraintsYNoResults: NSLayoutConstraint!
    
    @IBOutlet weak var viewFilterButton: UIView!
    
    @IBOutlet weak var hiddenPageView: UIView!
    
    //filter view outlets
    
    @IBOutlet weak var viewImagePost: UIView!
    @IBOutlet weak var viewFilterIcon: UIView!
    @IBOutlet weak var viewFavouriteStareIcon: UIView!
    
    @IBAction func btnFilterIconAction(_ sender: UIButton) {
        self.viewFilterButton.isHidden = true
        self.viewFilterButton.isHidden = !self.viewFilterButton.isHidden
    }
    @IBAction func btnFavouriteIconAction(_ sender: UIButton){
        self.viewFilterButton.isHidden = true
    }
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        if (gestureRecognizer is uigestur || gestureRecognizer is UIRotationGestureRecognizer) {
//            return true
//        } else {
//            return false
//        }
//    }
    func hiddenSecondPageViews(){
        self.view.endEditing(true)
        //UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.isViewPostShow = false
            self.btnTextPostVC.isHidden = false
            self.viewImagePost.isHidden = false
            
            self.txtDescription.placeholder = self.defultDescription
            //self.viewFavouriteStareIcon.isHidden = true
            self.imgDot2.backgroundColor = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            self.imgDot1.backgroundColor = UIColor.init(red: 135.0/255.0, green: 135.0/255.0, blue: 135.0/255.0, alpha: 1.0)
            self.view.layoutIfNeeded()
        //})
        
        /*
         self.txtDescription.placeholder = defultDescription
         self.btnTextPostVC.isHidden = false
         self.viewFilterIcon.isHidden = true
         self.viewFavouriteStareIcon.isHidden = true
         self.viewImagePost.isHidden = false
         */
    }
    
    func hiddenFirstPageViews(){
        self.view.endEditing(true)
        //UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.isViewPostShow = true
            self.btnTextPostVC.isHidden = true
            self.viewImagePost.isHidden = true
            self.txtDescription.placeholder = "Search"
            //self.viewFavouriteStareIcon.isHidden = true
            self.imgDot1.backgroundColor = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            self.imgDot2.backgroundColor = UIColor.init(red: 135.0/255.0, green: 135.0/255.0, blue: 135.0/255.0, alpha: 1.0)
            self.view.layoutIfNeeded()
        //})
        
        /*
         self.isViewPostShow = true
         self.viewImagePost.isHidden = true
         self.btnTextPostVC.isHidden = true
         self.txtDescription.placeholder = "Search"
         self.viewFavouriteStareIcon.isHidden = true
         self.imgDot1.backgroundColor = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
         self.imgDot2.backgroundColor = UIColor.init(red: 135.0/255.0, green: 135.0/255.0, blue: 135.0/255.0, alpha: 1.0)
         */
    }
    
    //fileprivate var refreshControl = UIRefreshControl()
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor //UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
    //MARK:- View Life Cyles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ImageLoad()
        self.tblFeeds.delaysContentTouches = false
        isFromImagePicker = false
        ref = Database.database().reference()
        self.manageBookingRequestCount()
        self.addTapGastureToFeedImage()
        self.addGesturesToView()
        self.observeKeyboard()
        self.txtDescription.delegate = self
        self.dataScrollView.decelerationRate = UIScrollView.DecelerationRate.normal
        self.tblFeeds.decelerationRate = UIScrollView.DecelerationRate.normal
        self.tblFeeds.estimatedRowHeight = 44
        
        //self.userCollection.addSubview(refreshControl)
        self.dataScrollView.addSubview(refreshControl)
        //self.dataScrollView.refreshControl = refreshControl
        //        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        //        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1) // UIColor.theameColors.pinkColor
        //NotificationCenter.default.addObserver(self, selector: #selector(self.refreshChatBadgeCount), name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshChatBadgeCount(Notification)
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(self.refreshStoryCollection), name: NSNotification.Name(rawValue: "refreshStoryCollection"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshStoryCollection"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshStoryCollection(Notification)
        }
        self.addGesturesToMainView()
        //Get my Id
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            myId = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            myId = userInfo["_id"] as? Int ?? 0
        }
        
        ref = Database.database().reference()
        _refHandle = ref.child("chatBadgeCount").child(String(myId)).observe(.value, with: { [weak self] (snapshot) in
            guard let strongSelf = self else {
                return
            }
            let dict = snapshot.value as? [String:Any]
            print(dict)
            if let count = dict?["count"] as? Int {
                //objChatShareData.chatBadgeCount = count
            }else if let count = dict?["count"] as? String {
                //objChatShareData.chatBadgeCount = Int(count) ?? 0
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        })
        //NotificationCenter.default.addObserver(self, selector: #selector(self.hideUploadLoader), name: NSNotification.Name(rawValue:"hideUploadLoader"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "hideUploadLoader"), object: nil, queue: nil) { [weak self](Notification) in
            self?.hideUploadLoader()
        }
    }
    
    @objc func refreshStoryCollection(_ objNotify: Notification){
        self.isMyStoryIsAdded = false
        self.callWebserviceFor_getStoryUsers()
    }
    
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            self.viewChatbadgeCount.isHidden = false
        }
    }
    func addGesturesToMainView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginTap.delegate = self
        socialLoginTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(socialLoginTap)
    }
    
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //if indexPath != indexPathOld{
            cellOld.btnSaveToFolder.isHidden = false
            cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.btnHiddenMoreOption.isHidden = true
            cellOld.setDefaultDesign()
        }
        //}
        ////
    }
    
    func ImageLoad(){
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfileHeader.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfileHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfileHeader.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfileHeader.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }
    }
    @objc func hideUploadLoader(){
        self.activityUpload.stopAnimating()
        self.viewUploadLoader.isHidden = true
        self.dataScrollView.contentOffset.y = 0.0
        self.tblFeeds.contentOffset.y = 0.0
        //self.reloadDataAfterPost()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        URLCache.shared.removeAllCachedResponses()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //NotificationCenter.default.addObserver(self, selector: #selector(self.resetFields), name: NSNotification.Name(rawValue: notificationName.postFeedSuccess), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: notificationName.postFeedSuccess), object: nil, queue: nil) { [weak self](Notification) in
            self?.resetFields()
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataAfterPost), name: NSNotification.Name(rawValue: notificationName.postUploaded), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: notificationName.postUploaded), object: nil, queue: nil) { [weak self](Notification) in
            self?.reloadDataAfterPost()
        }
        
        super.viewDidAppear(animated)
        addAccesorryToKeyBoard()
        UserDefaults.standard.setValue("no", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.setValue("yes", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
        if !isNavigate {
            isFromImagePicker = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
//        SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        URLCache.shared.removeAllCachedResponses()
        self.lblNoResults.isHidden = true
        self.dataScrollView.delegate = self
        self.callWebserviceFor_getStoryUsers()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        
        if objAppShareData.isFileIsUploding{
            self.lastSelectFeedIndex = 0
            self.activityUpload.startAnimating()
            self.viewUploadLoader.isHidden = false
            self.dataScrollView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
        }else{
            self.activityUpload.stopAnimating()
            self.viewUploadLoader.isHidden = true
        }
        
        self.txtDescription.text = ""
        strSearchValue = ""
        self.txtDescription.resignFirstResponder()
        self.activity.isHidden = true
        self.hiddenFirstPageViews()
        self.viewFilterButton.isHidden = true
        self.ImageLoad()
        //objLocationManager.getCurrentLocation()
        
        if objAppShareData.isFromNotification {
            
            if objAppShareData.notificationType == "feed" {
                self.gotoExpPostDetailVC(objFeeds:nil)
                return
            }else if objAppShareData.notificationType == "profile" {
                
                // "userType": user, "notifyId": 1
                
                var notifyId : Int?
                var strUserType: String?
                if let userInfo = objAppShareData.notificationInfoDict {
                    
                    if let notiId  = userInfo["notifyId"] as? Int{
                        notifyId = notiId
                    }else{
                        if let notiId  = userInfo["notifyId"] as? String{
                            notifyId = Int(notiId)
                        }
                    }
                    
                    if let userType = userInfo["userType"] as? String{
                        strUserType = userType
                    }
                }
                
                if let notifyId = notifyId, let strUserType = strUserType {
                    objAppShareData.selectedOtherIdForProfile  = notifyId
                    objAppShareData.isOtherSelectedForProfile = true
                    objAppShareData.selectedOtherTypeForProfile = strUserType
                    self.gotoProfileVC()
                    return
                }
                
            }else if objAppShareData.notificationType == "story" {
                
                if let userInfo = objAppShareData.notificationInfoDict {
                    
                    var notifyId : Int?
                    if let notiId  = userInfo["notifyId"] as? Int{
                        notifyId = notiId
                    }else{
                        if let notiId  = userInfo["notifyId"] as? String{
                            notifyId = Int(notiId)
                        }
                    }
                    
                    if let objUser = storyUser.init(dict: ["":""]) {
                        
                        if let notifyId = notifyId {
                            
                            objUser.profileImage = userInfo["urlImageString"] as? String ?? ""
                            objUser.userName = userInfo["userName"] as? String ?? ""
                            objUser._id = notifyId
                            
                            
                            if let objShowStories = storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as? ShowStoriesVC{
                                
                                objShowStories.objStoryUser = objUser
                                objShowStories.delegate = self
                                objShowStories.isMyStories = false
                                objShowStories.arrUsres.append(objUser)
                                objShowStories.selectedIndex = 0
                                objShowStories.myStoryCount = 0
                                objShowStories.modalPresentationStyle = .fullScreen
                                UIApplication.shared.keyWindow?.rootViewController?.present(objShowStories, animated: true, completion: nil)
                                
                            }
                            
                        }
                    }
                }
            }else if objAppShareData.notificationType == "chat" {
                let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: false)
                }
                
            }
            //objAppShareData.clearNotificationData()
        }
        
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            
            if self.view.frame.size.width == 320{
                self.optionButtonStackView.spacing = 16
            }else if self.view.frame.size.width == 375{
                self.optionButtonStackView.spacing = 32
            }else{
                self.optionButtonStackView.spacing = 40
            }
        }
        
        self.tblView?.reloadData()
        if !objAppShareData.isFileIsUploding{
            //self.tblFeeds?.reloadData()
        }
        
        self.suggesionView.isHidden = true
        self.dataScrollView.isScrollEnabled = true
        self.tblFeeds.isScrollEnabled = true
        var api = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            self.callWebserviceFor_getStoryUsers()
            //self.pointToScroll = self.addPostView.frame.size.height + 90.0 //+ (self.view.frame.size.height*0.17)
            self.pointToScroll = self.addPostView.frame.size.height + 130.0 //+ (self.view.frame.size.height*0.17)
            self.tblFeeds.isScrollEnabled = true
            if (!self.isFromImagePicker && !self.isNavigate) {
                self.feedsTableHeight.constant = self.dataScrollView.frame.size.height
                api = false
                if !objAppShareData.isFileIsUploding{
                    self.loadFeedsWithPage(page: 0, refresh: false)
                }
            }else if api{
                self.loadFeedsWithPage(page: self.pageNo, refresh: false)
                api = false
            }
            if (!self.isFromImagePicker && !self.isNavigate) {
                self.configureView()
            }
            self.isNavigate = false
            
        }
        
        if isFromImagePicker {
            //imageThumbView.isHidden = false
        }
        
        if objAppShareData.isSwitchToFeedTab {
            self.lastSelectFeedIndex = 0
            objAppShareData.isSwitchToFeedTab = false
            //self.reloadDataAfterPost()
        }
        if objAppShareData.isFromPostUpload{
            self.lastSelectFeedIndex = 0
            objAppShareData.isFromPostUpload = false
            self.dataScrollView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
        }
        
        if self.lastSelectFeedIndex>0{
            if self.lastSelectFeedIndex == objAppShareData.arrFeedsData.count-1{
                self.lastSelectFeedIndex = self.lastSelectFeedIndex-1
            }
            let ind = IndexPath.init(row: self.lastSelectFeedIndex, section: 0)
            self.tblFeeds.scrollToRow(at: ind, at: .top, animated: false)
        }
    }
    
    // MARK: - AVPlayerController Delegate
    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController){
    }
    
    // MARK: - Webservices
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any] , activity:Bool){
        if !objServiceManager.isNetworkAvailable(){
            self.refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            self.activity.isHidden = true
            return
        }
        
        if activity{
            self.activity.isHidden = true
            objWebserviceManager.StartIndicator()
        }
        /// Himanshu
        self.isLoading = true
        ////
       
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        print(parameters)
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.getAllFeeds, params: parameters , success: { [weak self]response in
            print(response)
            self?.isLoading = false
            ////
            objWebserviceManager.StopIndicator()
            ////
            self?.refreshControl.endRefreshing()
            self?.activity.isHidden = true
            if self?.pageNo==0{
                objAppShareData.arrFeedsData.removeAll()
                //self.tblFeeds.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            self?.strStatus = response["message"] as? String
            
            if strSucessStatus == k_success{
                
                self?.totalCount = response["total"] as? Int ?? 0
                let arrDict = response["AllFeeds"] as! [[String:Any]]
                self?.dataToAddCount = arrDict.count
                
                if arrDict.count > 0 {
                    for dict in arrDict{
                        let obj = feeds.init(dict: dict)
                        objAppShareData.arrFeedsData.append(obj!)
                    }
                    if objAppShareData.arrFeedsData.count > 0{
                        self?.updateUI()
                    }
                    self?.tblFeeds.isHidden = false
                }else{
                    self?.tblFeeds.isHidden = true
                    if objAppShareData.arrFeedsData.count==0{
                        self?.lblNoResults.isHidden = false
                    }
                }
            }else{
                
                self?.tblFeeds.reloadData()
                if strSucessStatus == "fail"{
                    if objAppShareData.arrFeedsData.count==0{
                        self?.lblNoResults.isHidden = false
                    }
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                    }
                }
            }
        }) { [weak self](error) in
            self?.isLoading = false
            self?.refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
    
    public func callWebserviceFor_getStoryUsers() -> Void{
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objWebserviceManager.StartIndicator()
        var parameters = [String:Any]()
        parameters = ["userId":myId]
        
        objWebserviceManager.requestPostWithOutIndicator(strURL: WebURL.getMyStoryUser, params: parameters,success: { response in
            if response["status"] as? String ?? "" == "success"{
                self.arrUsers.removeAll()
                let arr = response["myStoryList"] as! [[String:Any]]
                
                
                var id = 0
                if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                    id = dicUser["_id"] as? Int ?? 0
                }else{
                    let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                    id = userInfo["_id"] as? Int ?? 0
                }
                
                
                for dict in arr{
                    if dict["_id"] as? Int ?? 0 == id {
                        self.isMyStoryIsAdded = true
                        self.myStoryCount = dict["count"] as? Int ?? 0
                    }else{
                        let obj = storyUser.init(dict: dict)
                        self.arrUsers.append(obj!)
                    }
                }
                //                if arr.count == 0{
                //                    if  self.myStoryCount == 0 {
                //                    self.isMyStoryIsAdded = false
                //                    }
                //                }
                self.userCollection.reloadData()
            }else{
                self.userCollection.reloadData()
                //objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { (error) in
            self.userCollection.reloadData()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
            objWebserviceManager.StopIndicator()
        }){ error in
            //showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    func callWebservice(for_Follow objUser: feeds, andCell cell: feedsTableCell){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["followerId": objUser.userInfo?._id ?? 0,
                        "userId":myId]
        let followActivity = UIActivityIndicatorView()
        followActivity.tintColor = UIColor.white
        followActivity.color = UIColor.white
        followActivity.hidesWhenStopped = true
        followActivity.center = CGPoint(x: cell.btnFollow.frame.size.width / 2, y: cell.btnFollow.frame.size.height / 2)
        cell.btnFollow.addSubview(followActivity)
        followActivity.startAnimating()
        
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            
            followActivity.stopAnimating()
            followActivity.removeFromSuperview()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == k_success{
                    if objUser.followerStatus {
                        cell.btnFollow.setTitle("Following", for: .normal)
                        cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                        cell.btnFollow.layer.borderWidth = 1
                        cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                        cell.btnFollow.backgroundColor = UIColor.clear
                    }else {
                        cell.btnFollow.setTitle("Follow", for: .normal)
                        cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                        cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                        cell.btnFollow.layer.borderWidth = 0
                    }
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }}
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForSearchTags(dicParam: [String: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.requestPost(strURL: WebURL.tagSearch, params: dicParam, success: { response in
            self.suggesionArray.removeAll()
            
            let arr = response["allTags"] as! [[String:Any]]
            if arr.count>0{
                for dic in arr {
                    let obj = suggessions(dict: dic)
                    self.suggesionArray.append(obj!)
                }
                self.dataScrollView.isScrollEnabled = false
                self.tblSuggesion.reloadData()
                self.suggesionView.isHidden = false
                self.tblFeeds.isScrollEnabled = false
            }else{
                self.suggesionView.isHidden = true
                self.dataScrollView.isScrollEnabled = true
                self.tblFeeds.isScrollEnabled = true
            }
            
        }){ error in
            //showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    func updateUI() -> Void {
        if objAppShareData.arrFeedsData.count > 0{
            self.lblNoResults.isHidden = true
        }else{
            self.lblNoResults.isHidden = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.tblFeeds.reloadData()
        }
    }
    
    // MARK: - collection view delegate and data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrUsers.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for:
            indexPath) as? userCollectionCell)!
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            cell.imgProfile.setImageFream()
        }
        
        if indexPath.row == 0 {
            //// Crash
            var userInfo = [:] as! [String : Any]
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                userInfo = dict
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                userInfo = dict
            }
            
            let strProfile = userInfo["profileImage"] as? String ?? ""
            if strProfile != "" {
                //cell.imgProfile.af_setImage(withURL:URL(string: strProfile)!)
                cell.imgProfile.sd_setImage(with: URL(string: strProfile)!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }else{
                cell.imgProfile.image = UIImage.customImage.user
            }
            if isMyStoryIsAdded {
                cell.imgPlus.isHidden = true
                cell.lblUserName.text = "You"
            }else {
                cell.imgPlus.isHidden = false
                cell.lblUserName.text = "Your Story"
            }
        }else{
            let objUser = arrUsers[indexPath.row - 1]
            cell.imgPlus.isHidden = true
            if objUser.profileImage != "" {
                //cell.imgProfile.af_setImage(withURL:URL(string: objUser.profileImage)!)
                cell.imgProfile.sd_setImage(with: URL(string: objUser.profileImage)!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }else{
                cell.imgProfile.image = UIImage.customImage.user
            }
            cell.lblUserName.text = objUser.userName
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let objShowStories = storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as! ShowStoriesVC
        objShowStories.delegate = self
        //objShowStories.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        if (indexPath.row == 0){
            if (isMyStoryIsAdded){
                objShowStories.isMyStories = true
                objShowStories.arrUsres = arrUsers
                objShowStories.selectedIndex = -1
                objShowStories.myStoryCount = self.myStoryCount
                objShowStories.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController?.present(objShowStories, animated: true, completion: nil)
                
            }else{
                gotoCameraVC()
            }
        }else{
            
            objShowStories.isMyStories = false
            objShowStories.arrUsres = arrUsers
            objShowStories.selectedIndex = indexPath.row-1
            objShowStories.myStoryCount = self.myStoryCount
            //self.tabBarController?.present(objShowStories, animated: true, completion: nil)
            objShowStories.modalPresentationStyle = .fullScreen
            UIApplication.shared.keyWindow?.rootViewController?.present(objShowStories, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - textview delegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //        if (txtDescription.text == defultDescription) {
        //            txtDescription.text = ""
        //        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // self.postAction()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end editing")
    }
    
    func textViewDidChange(_ textView: UITextView){
        //        if let text = txtDescription.text, text.isEmpty{
        //            txtDescription.text = defultDescription
        //            txtDescription.resignFirstResponder()
        //       }
    }
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        fromTextField = true
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            
            getFeedsDataWith(0, andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            searchAutocompleteEntries(withSubstring: substring)
        }
        return true
    }
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            //fromTextField = false
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    @objc func reload() {
        self.activity.isHidden = false
        self.getFeedsDataWith(0, andSearchText: strSearchValue)
    }
    func getFeedsDataWith(_ page: Int, andSearchText strText: String = "") {
        var parameters = [String:Any]()
        //self.lastLoadedPage = page
        self.pageNo = page
        
        let strSearchText = strText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        strSearchValue = strSearchText
        
        //            parameters = [
        //                "latitude":strLat,
        //                "longitude":strLong,
        //                //"distance":"100",
        //                "page":self.pageNo,
        //                "limit":self.pageSize,
        //                "text": strSearchText.lowercased()
        //            ]
        //        parameters["userId"] = strUserId
        
        self.loadFeedsWithPage(page: self.pageNo, refresh: true)
    }
    
    /*
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     
     var substring: String = textField.text!
     substring = (substring as NSString).replacingCharacters(in: range, with: string)
     
     let wordsInSentence = substring.components(separatedBy: CharacterSet.whitespacesAndNewlines)
     
     var indexInSavedArray  = 0
     
     for str in wordsInSentence{
     // Get multiple ranges of all matched strings
     let subS = substring.ranges(of: str)
     
     if let selectedRange = textField.selectedTextRange {
     // Get current index text position
     let cursorPosition = textField.offset(from: textField.beginningOfDocument, to: selectedRange.start)
     
     var p = 0
     if string == ""{
     p = 1
     }else if string == "\n" || string == " "{
     p = -2
     }else{
     p = 0
     }
     
     for sub in subS{
     
     let lowR = sub.lowerBound.encodedOffset
     let upR = sub.upperBound.encodedOffset
     
     if cursorPosition >= lowR && (cursorPosition - p) <= upR{
     if str.hasPrefix("@"){
     self.currentWordIndex = indexInSavedArray
     self.suggesionType = "user"
     self.searchEntries(withSubstring: str, andType: "user")
     break
     }else if str.hasPrefix("#"){
     self.currentWordIndex = indexInSavedArray
     self.suggesionType = ""
     self.searchEntries(withSubstring: str, andType: "")
     break
     }else{
     self.suggesionView.isHidden = true
     self.dataScrollView.isScrollEnabled = true
     self.tblFeeds.isScrollEnabled = true
     self.suggesionArray.removeAll()
     self.tblSuggesion.reloadData()
     }
     }else{
     self.suggesionView.isHidden = true
     self.dataScrollView.isScrollEnabled = true
     self.tblFeeds.isScrollEnabled = true
     self.suggesionArray.removeAll()
     self.tblSuggesion.reloadData()
     }
     }
     }
     indexInSavedArray = indexInSavedArray + 1
     }
     return true
     }
     */
    
    // MARK: - scrollview delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        print("SDImageCache")
//        print(SDImageCache.shared().getSize())
//        if SDImageCache.shared().getSize() >= 10040260{
//            SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//            SDImageCache.shared().clearMemory()
//            SDImageCache.shared().clearDisk()
//            URLCache.shared.removeAllCachedResponses()
//        }
//        print(SDImageCache.shared().getSize())
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        //self.view.endEditing(true)
        //print(SDImageCache.shared().getSize())
        
//                if SDImageCache.shared().getSize() >= 8040260{
//                SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//                    SDImageCache.shared().clearMemory()
//                    SDImageCache.shared().clearDisk()
//                    URLCache.shared.removeAllCachedResponses()
//                }
        
        self.viewFilterButton.isHidden = true
        if scrollView != self.txtDescription && scrollView != self.tblSuggesion{
            
            if self.tblFeeds.contentOffset.y > pointToScroll{
                dataScrollView.contentOffset.y = pointToScroll
                print("return")
                return
            }
            if dataScrollView.contentOffset.y < pointToScroll{
                dataScrollView.contentOffset.y = scrollView.contentOffset.y
                print("if")
            }else if dataScrollView.contentOffset.y >= pointToScroll {
                dataScrollView.contentOffset.y = pointToScroll
                print("else")
            }
            
            if dataScrollView.contentOffset.y <= 20{
                dataScrollView.bounces = true
            }else{
                dataScrollView.bounces = false
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        self.viewFilterButton.isHidden = true
        //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnSelectDataType(toView sender: UIButton){
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        if sender.tag == 0 {
            loadImagesWithPage(page: 0, refresh: false)
        }else if sender.tag == 1 {
            loadVideosWithPage(page: 0, refresh: false)
        }else{
            loadFeedsWithPage(page: 0, refresh: false)
        }
    }
    
    @IBAction func btnPickVideo(_ sender: Any) {
        self.view.endEditing(true)
        if objAppShareData.isFileIsUploding{
            showAlertVC(title: kAlertTitle, message:"Uploading is already in progress", controller: self)
        }else{
            txtDescription.endEditing(true)
            isVideo = true
            
            self.isNavigate = true
            self.tabBarController?.selectedIndex =  2
            return
            //selectVideo()
        }
    }
    
    @IBAction func btnPickImage(_ sender: Any) {
        self.viewFilterButton.isHidden = true
        self.view.endEditing(true)
        if objAppShareData.isFileIsUploding{
            showAlertVC(title: kAlertTitle, message:"Uploading is already in progress", controller: self)
        }else{
            isVideo = false
            txtDescription.endEditing(true)
            self.isNavigate = true
            self.tabBarController?.selectedIndex =  2
            return
            //selectImage()
        }
    }
    
    @IBAction func btnShowVideoImage(_ sender: Any) {
        
        self.view.endEditing(true)
        txtDescription.endEditing(true)
        showVideoImage()
    }
    
    @IBAction func btnCloseZoomView(_ sender: Any) {
        
        if (sender as AnyObject).tag == 0 {
            // btnBack.isHidden = true
        }else {
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    @IBAction func btnPostAction(_ sender: Any){
        self.viewFilterButton.isHidden = true
        self.view.endEditing(true)
        self.postAction()
    }
    func postAction(){
        //if arrFeedImages.count>0 || mp4VideoURL != nil || (txtDescription.text)! != defultDescription && txtDescription.text!.count > 0{
        
        txtDescription.resignFirstResponder()
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
        
        objNewPost.isVideo = isVideo
        //objNewPost.imgFeed = imgFeed.image
        //objNewPost.imgThumb = imgFeed.image
        objNewPost.videoURL = mp4VideoURL
        
        if arrFeedImages.count>0{
            objNewPost.arrFeedImages = arrFeedImages
        }
        if txtDescription.text != defultDescription {
            objNewPost.strDescriptionText = txtDescription.text!
            objNewPost.arrPeopleTagSuggesionArray = self.arrPeopleTagSuggesionArray
        }
        isNavigate = true
        if objAppShareData.isFileIsUploding{
            showAlertVC(title: kAlertTitle, message:"Uploading is already in progress", controller: self)
        }else if fileSize>51.0{
            showAlertVC(title: kAlertTitle, message:"You cannot upload more then 50 MB's video", controller: self)
        }else{
            navigationController?.pushViewController(objNewPost, animated: true)
        }
        //        }else{
        //
        //            if arrFeedImages.count == 0 || (mp4VideoURL == nil) || txtDescription.text == defultDescription {
        //                objAppShareData.shakeViewField(self.txtDescription!)
        //            }
        //        }
    }
    
    @IBAction func btnShowTblImage(_ sender: Any) {
        // btnBack.isHidden = false
    }
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.logout()
    }
    @IBAction func btnShowTblVideo(_ sender: Any) {
        self.view.endEditing(true)
        isNavigate = true
        let index = (sender as AnyObject).tag
        self.lastSelectFeedIndex = index!
        let objFeeds = objAppShareData.arrFeedsData[index!]
        addVideo(toVC: URL(string: (objFeeds.arrFeed[0].feedPost))!)
    }
    
    @IBAction func btnSharePost(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        
        let objData = objAppShareData.arrFeedsData[sender.tag]
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
        objChooseType.objFeeds = objData
        objChooseType.fromVoucher = false
        self.navigationController?.pushViewController(objChooseType, animated: true)
        return
            
            self.lastSelectFeedIndex = (sender as AnyObject).tag
        let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        
        // let objFeeds: feeds? = objAppshare.arrFeeds[(sender as AnyObject).tag]
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        self.shareLinkUsingActivity(withObject: objFeeds, andTableCell: cell!)
        /*
         if (objFeeds.feedType == "video"){
         showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
         } else {
         showShareActionSheet(withObject: objFeeds, andTableCell: cell!)
         }
         */
    }
    
    @IBAction func btnLikeFeed(_ sender: Any){
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        likeUnlikePost(cell: cell, objFeeds: objFeeds)
    }
    
    @IBAction func btnComment(onFeed sender: Any) {
        self.view.endEditing(true)
        //return
        self.viewFilterButton.isHidden = true
        
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (sender as AnyObject).tag
        isNavigate = true
        self.lastSelectFeedIndex = (sender as AnyObject).tag
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
//        self.viewFilterButton.isHidden = true
//        isNavigate = true
//        objAppShareData.isOtherSelectedForProfile = false
//        self.gotoProfileVC()
        self.gotoNotificationVC()
    }
    
    func gotoNotificationVC(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"NotificationVC") as? NotificationVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnShowCameraAction(_ sender: Any) {
        self.view.endEditing(true)
        gotoCameraVC()
    }
    
    @IBAction func btnFollowAction(_ sender: Any){
        
        self.view.endEditing(true)
        //return
        self.viewFilterButton.isHidden = true
        
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        let objUser = objAppShareData.arrFeedsData[indexPath.row]
        objUser.followerStatus = !objUser.followerStatus
        callWebservice(for_Follow: objUser, andCell: cell!)
        
    }
    
    @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        //return
        self.viewFilterButton.isHidden = true
        
        let objFeeds = objAppShareData.arrFeedsData[(myLabel.view?.tag)!]
        let objLikeVc = storyboard?.instantiateViewController(withIdentifier: "LikesListVC") as? LikesListVC
        objLikeVc?.objFeeds = objFeeds
        isNavigate = true
        self.lastSelectFeedIndex = (myLabel.view?.tag)!
        navigationController?.pushViewController(objLikeVc ?? UIViewController(), animated: true)
    }
    
    @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        return
            self.viewFilterButton.isHidden = true
        
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (myLabel.view?.tag)!
        isNavigate = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @objc func showVideoImage() {
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        
        if isVideo {
            addVideo(toVC: mp4VideoURL!)
        }
    }
    
    @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        
        isNavigate = true
        let objFeed = objAppShareData.arrFeedsData[(recognizer.view?.tag)!]
        self.lastSelectFeedIndex = (recognizer.view?.tag)!
        addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
    }
    
    @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
        let indexPath = IndexPath.init(row: (recognizer.view?.tag)!, section: 0)
        let cell = self.tblFeeds.cellForRow(at: indexPath) as! feedsTableCell
        cell.setTagsHidden(!(cell.tagsHidden))
        
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //cellOld.btnSaveToFolder.isHidden = false
            //cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.btnHiddenMoreOption.isHidden = true
            cellOld.setDefaultDesign()
        }
        /*
         self.viewFilterButton.isHidden = true
         let sb = UIStoryboard(name: "Explore", bundle: Bundle.main)
         if let objDetail = sb.instantiateViewController(withIdentifier:"NewFeedDetailVC") as? NewFeedDetailVC{
         let objFeed = objAppShareData.arrFeedsData[(recognizer.view?.tag)!]
         objAppShareData.arrFeedsForArtistData.removeAll()
         objAppShareData.arrFeedsForArtistData.append(objFeed)
         objDetail.objFeeds = objFeed
         self.navigationController?.pushViewController(objDetail, animated: true)
         }
         */
        /*
         let objFeed = objAppShareData.arrFeedsData[(recognizer.view?.tag)!]
         let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
         let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
         isNavigate = true
         objShowImage.isTypeIsUrl = true
         objShowImage.arrFeedImages = objFeed.arrFeed
         objShowImage.objFeeds = objFeed
         present(objShowImage, animated: true)// { _ in }
         */
    }
    
    @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.viewFilterButton.isHidden = true
        
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
        if !objFeeds.isLike{
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
    }
    
    
    // MARK: - UIImagePicker controller delegate
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.viewFilterButton.isHidden = true
        
        isFromImagePicker = true
        //imageThumbView.isHidden = false
        
        if isVideo {
            mp4VideoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            generateImage(mp4VideoURL!)
            btnPlayVideo.isHidden = false
            lblImageCount.isHidden = true
            do {
                let video = try Data.init(contentsOf: mp4VideoURL!)
                fileSize = (Float(video.count) / 1024.0 / 1024.0)
            } catch {
                //print(error)
            }
            
        }else {
            
            arrFeedImages.removeAll()
            Image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            Image = self.resizeImage(image: Image!, newWidth: 1000)
            //imgFeed.image = Image
            arrFeedImages.append(Image!)
            btnPlayVideo.isHidden = true
        }
        picker.dismiss(animated: true) //{ _ in }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // isFromImagePicker = true
        dismiss(animated: true) //{ _ in }
    }
    
    // MARK: - Video player
    func addVideo(toVC url: URL) {
        // create an AVPlayer
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: url)
        controller.delegate = self
        controller.player?.play()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }
    
    // MARK: - Generate thumb from video
    func generateImage(_ URL: URL) ->Void {
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 320, height: 180)
        assetImageGenerator.maximumSize = maxSize
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            //self.imgFeed.image = UIImage.init(cgImage: imageRef)
        } catch {
            //print("error")
        }
    }
}

//MARK: - LOCAL METHODS
private extension HomeVC {
    
    func gotoProfileVC (){
        self.viewFilterButton.isHidden = true
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
         objVC.hidesBottomBarWhenPushed = true
         navigationController?.pushViewController(objVC, animated: true)
         }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func addTapGastureToFeedImage(){
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showVideoImage))
        //imgFeed.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func resetFields() {
        self.viewFilterButton.isHidden = true
        self.isFromImagePicker = false
        //txtDescription.text = defultDescription
        //imageThumbView.isHidden = true
        mp4VideoURL = nil
        Image = nil
        //imgFeed.image = nil
        self.arrFeedImages.removeAll()
    }
    
    @objc func reloadDataAfterPost() {
        self.viewFilterButton.isHidden = true
        loadFeedsWithPage(page:0, refresh: true)
//        if self.btnFeeds.isSelected == true{
//            loadFeedsWithPage(page:0, refresh: true)
//        }else if self.btnImages.isSelected{
//            loadImagesWithPage(page:0, refresh: false)
//        }else{
//            loadVideosWithPage(page:0, refresh: false)
//        }
        self.resetFields()
        
        ////
//        self.activityUpload.stopAnimating()
//        self.viewUploadLoader.isHidden = true
//        self.dataScrollView.contentOffset.y = 0.0
//        self.tblFeeds.contentOffset.y = 0.0
//        //self.reloadDataAfterPost()
        ////
    }
    
    
    
    func configureView(){
        self.viewFilterButton.isHidden = true
        
        addPostView.layer.borderColor = UIColor(red: 191.0 / 255.0, green: 191.0 / 255.0, blue: 191.0 / 255.0, alpha: 1.0).cgColor
        dataView.layer.borderColor = colorGray.cgColor
        // btnBack.isHidden = true
        
        btnVideos.isSelected = false
        btnFeeds.isSelected = true
        btnImages.isSelected = false
        
        //imageThumbView.isHidden = true
        
        // btnFeeds.layer.borderColor = colorGray.cgColor
        
        // txtDescription.text = defultDescription
        dataScrollView.contentOffset = CGPoint(x: 0, y: 0)
        constraintsYNoResults.constant = 0
    }
    
    func loadImagesWithPage(page: Int, refresh:Bool) {
        
        pageNo = page
        strType = "image"
        
        let dicParam = ["feedType": strType, "search": "", "page": pageNo, "limit": "100", "type": "newsFeed","userId":myId,"loginUserId": myId] as [String : Any]
        
        if !refresh {
            if !self.isLoading{
               callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }
        }else{
            if !self.isLoading{
               callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
    }
    
    func loadVideosWithPage(page: Int, refresh:Bool) {
        
        pageNo = page
        strType = "video"
        
        let dicParam = ["feedType": strType, "search": "", "page": pageNo, "limit": "100", "type": "newsFeed","userId":myId,"loginUserId": myId] as [String : Any]
        
        if !refresh {
            if !self.isLoading{
               callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }
        }else{
            if !self.isLoading{
               callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
    }
    
    func loadFeedsWithPage(page: Int, refresh:Bool) {
        pageNo = page
        strType = ""
        
        let dicParam = ["feedType": "","search": strSearchValue, "page": pageNo, "limit": "100","type": "newsFeed","userId":myId,"loginUserId": myId] as [String : Any]
        
        if !refresh {
            if !self.isLoading{
               callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }
        }else{
            if !self.isLoading{
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
    }
    
    func addPushAnimation(to view: UIView) {
        
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.layer.add(transition, forKey: nil)
    }
    
    func selectImage(){
        
        let selectImage = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                imagePicker.showsCameraControls = true
                //imagePicker.allowsEditing = YES;
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true) //{ _ in }
            }
        })
        let btn2 = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.openGallaryForMultipleImageSelection();
        })
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true) //{ _ in }
    }
    
    func selectVideo(){
        
        let selectImage = UIAlertController(title: "Select Video", message: nil, preferredStyle: .actionSheet)
        let picker = UIImagePickerController()
        picker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.allowsEditing = true
                picker.sourceType = .camera
                picker.mediaTypes = ["public.movie"]
                picker.videoQuality = .typeMedium
                picker.videoMaximumDuration = 60
                picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true) //{ _ in }
            }
        })
        let btn2 = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary) ?? [String]()
                picker.mediaTypes = ["public.movie","public.video","public.mpeg","public.avi","public.mpeg-4"]
                picker.videoQuality = .typeMedium
                picker.videoMaximumDuration = 60
                picker.allowsEditing = true
                picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true) //{ _ in }
            }
        })
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true) //{ _ in }
    }
    
    func addAccesorryToKeyBoard(){
        
        let keyboardDoneButtonView = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.done, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        //txtDescription.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func resignKeyBoard(){
        
        if let text = txtDescription.text, text.isEmpty{
            // txtDescription.text = defultDescription
        }
        txtDescription.endEditing(true)
    }
    
    func showShareActionSheet(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
    }
    
    func gotoCameraVC() {
        DispatchQueue.main.async(execute: {() -> Void in
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.push
            //kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal
            transition.subtype = CATransitionSubtype.fromLeft
            //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
            self.navigationController?.view.layer.add(transition, forKey: nil)
            let objCamera = self.storyboard?.instantiateViewController(withIdentifier: "CameraVC") as? CameraVC
            self.isNavigate = true
            self.navigationController?.pushViewController(objCamera ?? UIViewController(), animated: false)
        })
    }
    
    func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
        
        if (objFeeds.isLike){
            objFeeds.isLike = false
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = false
                })
            })
            objFeeds.likeCount = objFeeds.likeCount - 1
        }else{
            objFeeds.isLike = true
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = true
                })
            })
            objFeeds.likeCount = objFeeds.likeCount + 1
        }
        cell.lblLikeCount.text = "\(objFeeds.likeCount)"
        if objFeeds.likeCount > 1{
            cell.lblLikeOrLikes.text = "Likes"
        }else{
            cell.lblLikeOrLikes.text = "Like"
        }
        
        //        // API Call
        //        objLocationManager.getCurrentAdd(success: { address in
        //
        //
        //        }) { error in
        //            print("address nor found")
        //        }
        
        var dicUser = [:] as! [String:Any]
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            dicUser = dict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            dicUser = dict
        }
        //let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! [String:Any]
        
        let dicParam  = ["feedId": objFeeds._id,
                         "userId":objFeeds.userInfo?._id ?? 0,
                         "likeById":self.myId,
                         "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? "18"),
                         "gender":dicUser["gender"] as? String ?? "male",
                         "city":objLocationManager.currentCLPlacemark?.locality ?? "Indore",
                         "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "MP",
                         "country":objLocationManager.currentCLPlacemark?.country ?? "India",
                         "type":"feed"]  as [String : Any]
        
        print("dicParam = \(dicParam)")
        
        self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        tap1.numberOfTapsRequired = 1
        tap2.numberOfTapsRequired = 1
        if objFeeds.likeCount > 0{
            cell.lblLikeCount.isUserInteractionEnabled = true
            cell.likes.isUserInteractionEnabled = true
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
        }else {
            cell.lblLikeCount.isUserInteractionEnabled = false
            cell.likes.isUserInteractionEnabled = false
        }
    }
    
    func showAnimatedLikeUnlikeOn(cell:feedsTableCell, objFeeds:feeds)->Void{
        let size = cell.imageVideoView.frame.size.width/3
        
        let heartImageView = UIImageView(frame: CGRect(x:0,y:0, width:size ,height:size))
        
        heartImageView.layer.cornerRadius = heartImageView.frame.size.width/2
        heartImageView.layer.masksToBounds = true;
        heartImageView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue:1.0, alpha: 0.5)
        heartImageView.center = CGPoint(x:cell.imageVideoView.center.x,y:cell.imageVideoView.center.y-size/2)
        heartImageView.image = UIImage.init(named:"ic_heart_outline")
        heartImageView.contentMode = .center
        heartImageView.transform = CGAffineTransform(scaleX:0, y:0)
        cell.imageVideoView.addSubview(heartImageView)
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            heartImageView.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.4, animations: {
                heartImageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            }, completion: { (_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.4, animations: {
                    heartImageView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                    heartImageView.removeFromSuperview()
                })
            })
        })
    }
}

extension HomeVC : TLPhotosPickerViewControllerDelegate{
    
    fileprivate func openGallaryForMultipleImageSelection(){
        
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        viewController.configure = configureTlPhotoPickerVC()
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    fileprivate func configureTlPhotoPickerVC() -> TLPhotosPickerConfigure {
        var configure = TLPhotosPickerConfigure()
        configure.maxSelectedAssets=10
        configure.allowedLivePhotos=false;
        configure.autoPlay=false;
        configure.allowedVideo=false;
        configure.usedCameraButton=false
        configure.selectedColor = UIColor.theameColors.pinkColor
        return configure
    }
    
    //MARK: - TLPhotosPickerViewControllerDelegate
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        
        if withTLPHAssets.count > 0 {
            
            fileSize = 0.0
            isFromImagePicker = true
            //imageThumbView.isHidden = false
            // use selected order, fullresolution image
            //imgFeed.image = withTLPHAssets[0].fullResolutionImage;
            btnPlayVideo.isHidden = true
            // lblImageCount.isHidden = false
            lblImageCount.text = String(withTLPHAssets.count)
            arrFeedImages.removeAll()
            
            for assets in withTLPHAssets{
                let img = assets.fullResolutionImage
                if img!.size.width > 1000{
                    let i = self.resizeImage(image: img!, newWidth: 1000)
                    arrFeedImages.append(i)
                }else{
                    arrFeedImages.append(img!)
                }
            }
        }
    }
    
    func photoPickerDidCancel() {
        //isFromImagePicker = true
        // cancel
    }
    
    func dismissComplete() {
        // picker viewcontroller dismiss completion
    }
    
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        // exceed max selection
        showAlertVC(title: kAlertTitle, message: "You cannot select more then 10 images at a time", controller:picker)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

// MARK:- UITableView Delegate and Datasource
extension HomeVC : UITableViewDelegate,feedsTableCellDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == self.tblFeeds{
            //return objAppShareData.arrFeedsData.count
            if objAppShareData.arrFeedsData.count>0{
                return objAppShareData.arrFeedsData.count
            }else{
                return 0
            }
        }else if tableView == self.tblSuggesion{
            return self.suggesionArray.count
        }else{
            return 0
        }
    }
    private func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        print("other gesture should work")
        return true
    }
    @objc func btnHiddenMoreOption(_ sender: UIButton)
    {
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnHiddenMoreOption.isHidden = true
        cell.setDefaultDesign()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        
        if tableView == tblFeeds{
            
            //RUN pagination
            if objAppShareData.arrFeedsData.count < self.totalCount {
                //if indexPath.row >= objAppShareData.arrFeedsData.count - 4{
                if indexPath.row >= objAppShareData.arrFeedsData.count {
                    if !isLoading{
                        isLoading = true
                        pageNo = pageNo + 1
                        if self.btnFeeds.isSelected == true{
                            loadFeedsWithPage(page: pageNo, refresh: true)
                        }else if self.btnImages.isSelected{
                            loadImagesWithPage(page:pageNo, refresh: true)
                        }else{
                            loadVideosWithPage(page: pageNo, refresh: true)
                        }
                    }
                }
            }
            //
            
            tblView = tableView
            if objAppShareData.arrFeedsData.count>indexPath.row {
                
                let objFeeds = objAppShareData.arrFeedsData[indexPath.row]
                
                var cell = feedsTableCell()
                if objFeeds.feedType == "text"{
                    cell = tableView.dequeueReusableCell(withIdentifier: cellIdText, for: indexPath) as! feedsTableCell
                }else{
                    cell = tableView.dequeueReusableCell(withIdentifier: cellIdImageVideo, for: indexPath) as! feedsTableCell
                }
               
                if objFeeds.feedType == "image"{
                    var height = CGFloat(0.0)
                    if objFeeds.feedImageRatio == "0"{
                        height = self.view.frame.size.width * 0.75
                    }else if objFeeds.feedImageRatio == "1"{
                        height = self.view.frame.size.width
                    }else{
                        height = self.view.frame.size.width * 1.25
                    }
                    cell.heightOfImageRatio.constant = height
                
                }else if objFeeds.feedType == "video"{
                    let height = self.view.frame.size.width
                    cell.heightOfImageRatio.constant = height
                }
                
                if objFeeds.feedType == "video" || objFeeds.feedType == "image"{
                    cell.setDefaultDesign()
                }
                if objFeeds.isSave == 0{
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "inactive_book_mark_ico"), for: .normal)
                }else{
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "active_book_mark_ico"), for: .normal)
                }
                let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
                
                if String(objFeeds.userId) == id {
                    cell.btnFollow.isHidden=true;
                    cell.btnMore.isHidden = false
                    cell.viewSeparatorViewMore.isHidden = false
                }else{
                    cell.btnFollow.isHidden=true;
                    cell.btnMore.isHidden = false
                    cell.viewSeparatorViewMore.isHidden = true
                }
                
                cell.tag = indexPath.row
                cell.indexPath = indexPath
                cell.delegate = self
                cell.imgPlay?.isHidden = true
                cell.viewMore.isHidden = true
                cell.btnHiddenMoreOption.isHidden = true
                cell.btnSaveToFolder.isHidden = false
                cell.btnReportThisPost.isHidden = true
                //cell.scrollView.removeFromSuperview()
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                
                let url = URL(string: (objFeeds.userInfo?.profileImage)!)
                if  url != nil {
                    //cell.imgProfile.af_setImage(withURL: url!)
                    cell.imgProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                }else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
                }
                
                self.tableView(self.tblFeeds, willDisplay: cell , forRowAt: indexPath)
               
                return cell
            }
            
        }else if tableView == self.tblSuggesion{
            var cellId = ""
            if suggesionType == "user"{
                cellId = "mentions"
            }else{
                cellId = "hashTag"
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SuggessionTableCell
            
            self.tableView(self.tblSuggesion, willDisplay: cell , forRowAt: indexPath)
            
            return cell
        }
        return cell
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: feedsTableCell, forRowAt indexPath: IndexPath){
        
        if objAppShareData.arrFeedsData.count>indexPath.row {
            
            let objFeeds = objAppShareData.arrFeedsData[indexPath.row]
            if objFeeds.feedType == "video"{
                cell.pageControll.isHidden = true
                cell.pageControllView.isHidden = true
                cell.viewCollection.isHidden = true
                cell.imgFeeds?.isHidden = false
                if objFeeds.arrFeed.count > 0{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    cell.imgFeeds?.sd_setImage(with: URL.init(string: objFeeds.arrFeed[0].videoThumb)!, placeholderImage: UIImage(named: "gallery_placeholder"))
                    }
                }
                
            }else if objFeeds.feedType == "image"{
                cell.pageControll.isHidden=false
                cell.pageControllView.isHidden = false
                cell.viewCollection.isHidden = false
                cell.imgFeeds?.isHidden = true
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    cell.setPageControllMethodNew(objFeeds:objFeeds)
                }
                
            }else{
            }
            
            if objFeeds.feedType == "text"{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                    
                    self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }else{
//                let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                let font = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                    
                    self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    
                }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
            }
            
            cell.imgPlay?.tag = indexPath.row
            cell.imgPlay?.superview?.tag = indexPath.section
            cell.btnShare.tag = indexPath.row
            cell.btnShare.superview?.tag = indexPath.section
            cell.btnLike.tag = indexPath.row
            cell.btnLike.superview?.tag = indexPath.section
            cell.imgFeeds?.tag = indexPath.row
            cell.btnShowTag?.tag = indexPath.row
            cell.lblUserName.text = objFeeds.userInfo?.userName
            cell.lblCity.text = objFeeds.location
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            cell.lblCommentCount.text = "\(objFeeds.commentCount)"
            cell.lblTime.text = objFeeds.timeElapsed
            //cell.imgProfile.image = UIImage.customImage.user
            cell.imgProfile.setImageFream()

            //// All gestures
            let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
              tapForLike.numberOfTapsRequired = 2
              tapForLike.cancelsTouchesInView = false
            ////

            if objFeeds.feedType == "video"{
                cell.viewShowTag.isHidden = true
                cell.imgPlay?.isHidden = false

        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
        let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
        tapForPlayVideo.numberOfTapsRequired = 1
    cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
        cell.imgPlay?.addGestureRecognizer(tapForLike)
        tapForPlayVideo.require(toFail:tapForLike)
        //}

            let height = self.view.frame.size.width
            cell.heightOfImageRatio.constant = height

            }else if objFeeds.feedType == "image"{
                cell.imgPlay?.isHidden = true
                var isTag = false
                for obj in objFeeds.arrPhotoInfo{
                    if obj.arrTags.count>0{
                        isTag = true
                        break
                    }
                }
                if isTag{
                    cell.viewShowTag.isHidden = true
                }else{
                    cell.viewShowTag.isHidden = true
                }

                cell.pageControllView.tag = indexPath.row
                cell.viewCollection.tag = indexPath.row

        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
            tap.numberOfTapsRequired = 1
            tap.cancelsTouchesInView = false
            //cell.contentView.addGestureRecognizer(tap)
          //cell.contentView.addGestureRecognizer(tapForLike)
            //cell.viewCollection.addGestureRecognizer(tap)
        //cell.viewCollection.addGestureRecognizer(tapForLike)
            tap.require(toFail:tapForLike)
        //}

            }else{
                if objAppShareData.validateUrl(objFeeds.caption) {
                    cell.lblCaption.tag = indexPath.row
                }
            }
            
            if objFeeds.isLike {
                cell.btnLike.isSelected = true
            }else {
                cell.btnLike.isSelected = false
            }
            cell.btnComment.tag = indexPath.row
            cell.lblLikeCount.tag = indexPath.row
            cell.lblCommentCount.tag = indexPath.row
            cell.lblCommentCount.isUserInteractionEnabled = true
            
            if (cell.likes) != nil{
                cell.likes.tag = indexPath.row
            }

            /*
            let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
            tapCommentCount.numberOfTapsRequired = 1
        cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
             
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
            */
            
            let myInt = objFeeds.likeCount
            if myInt > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true

                if myInt > 1{
                    cell.lblLikeOrLikes.text = "Likes"
                }else{
                    cell.lblLikeOrLikes.text = "Like"
                }
            }else {
                cell.lblLikeOrLikes.text = "Like"
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
            
            let myComment = objFeeds.commentCount
            if myComment == 0{
                cell.lblCommentOrComments.text = "Comment"
            }else if myComment == 1{
                cell.lblCommentOrComments.text = "Comment"
            }else {
                cell.lblCommentOrComments.text = "Comments"
            }
            
            //Follow status
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.superview?.tag = indexPath.section
            
            if objFeeds.followerStatus {
                cell.btnFollow.setTitle("Following", for: .normal)
                cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnFollow.layer.borderWidth = 1
                cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.btnFollow.backgroundColor = UIColor.clear
            }else {
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                cell.btnFollow.layer.borderWidth = 0
            }
            
            cell.btnMore.tag = indexPath.row
            cell.btnSaveToFolder.tag = indexPath.row
            cell.btnReportThisPost.tag = indexPath.row
            cell.btnEditPost.tag = indexPath.row
            cell.btnDeletePost.tag = indexPath.row
            cell.btnHiddenMoreOption.tag = indexPath.row
            cell.btnHiddenMoreOption.addTarget(self, action: #selector(btnHiddenMoreOption(_:)), for: .touchUpInside)
            cell.btnEditPost.addTarget(self, action: #selector(btnEditPostAction(_:)), for: .touchUpInside)
            cell.btnDeletePost.addTarget(self, action: #selector(btnDeletePostAction(_:)), for: .touchUpInside)
            cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
            cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
            cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
            cell.layoutIfNeeded()
        }
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
        
        let objSuggession = self.suggesionArray[indexPath.row]
        if suggesionType == ""{
            cell.lblTag.text = "#"+objSuggession.tag
        }else{
            cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
            cell.lblUserName.text = objSuggession.userName
            cell.imgProfile.image = UIImage.customImage.user
            if objSuggession.profileImage.count > 0 {
                //cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
                cell.imgProfile.sd_setImage(with: URL(string:objSuggession.profileImage)!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }
    }
    
    func BG(_ block: @escaping ()->Void) {
        DispatchQueue.global(qos: .default).async(execute: block)
    }
    
    @objc func btnMoreAction(_ sender: UIButton)
    {
        self.viewFilterButton.isHidden = true
        let objData = objAppShareData.arrFeedsData[sender.tag]
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        if cell.viewMore.isHidden == true{
            let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
            if String(objData.userId) == id {
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = false
                cell.btnReportThisPost.isHidden = true
                cell.btnSaveToFolder.isHidden = true
                cell.viewMore.isHidden = false
            }else{
                cell.btnEditPost.isHidden = true
                cell.btnDeletePost.isHidden = true
                cell.btnSaveToFolder.isHidden = true
                cell.btnReportThisPost.isHidden = false
                cell.viewMore.isHidden = false
            }
            cell.btnSaveToFolder.isHidden = false
            cell.btnHiddenMoreOption.isHidden = false
            //cell.viewMore.isHidden = false
        } else {
            cell.btnHiddenMoreOption.isHidden = true
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.viewMore.isHidden = true
            cell.setDefaultDesign()
        }
        
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            if indexPath != indexPathOld{
                cellOld.btnSaveToFolder.isHidden = false
                cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.btnHiddenMoreOption.isHidden = true
                cellOld.setDefaultDesign()
            }
        }
        indexLastViewMoreView = (sender as AnyObject).tag
        ////
        
        //        let ind = indexPath.row-1
        //        if ind >= 0{
        //           let indexPathOld = IndexPath(row:ind, section: 0)
        //            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
        //                cellOld.setTagsHidden(true)
        //            }
        //        }
    }
    
    @objc func btnSaveToFolder(_ sender: UIButton){
        self.viewFilterButton.isHidden = true
        let objData = objAppShareData.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        self.lastSelectFeedIndex = sender.tag
        if objData.isSave == 0{
            //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                objVC.strFeedId = String(objData._id)
                objAppShareData.btnAddHiddenONMyFolder = false
                navigationController?.pushViewController(objVC, animated: true)
            }}else{
            self.webServiceForEditFolder(feedId:String(objData._id))
        }
    }
    
    
    func webServiceForEditFolder(feedId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "feedId" : feedId,
            "folderId":""] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    //self.viewWillAppear(true)
                    self.loadFeedsWithPage(page: self.pageNo, refresh: false)
                    
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    @objc func btnEditPostAction(_ sender: UIButton)
    {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @objc func btnDeletePostAction(_ sender: UIButton)
    {
        
        let objData = objAppShareData.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.setDefaultDesign()
            var myIds = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myIds = String(dicUser["_id"] as? Int ?? 0)
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myIds = String(userInfo["_id"] as? Int ?? 0)
            }
            
            
            let param = ["feedType":objData.feedType, "id":String(objData._id), "userId":myIds]
            // objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
            self.deleteCommentAlert(param: param, indexPath: indexPath)
        }
    }
    
    func deleteCommentAlert(param:[String:Any],indexPath:IndexPath){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this feed?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            // self.totalCount  = self.totalCount  - 1
            //objAppShareData.arrFeedsData.remove(at: indexPath.row)
            //self.tblFeeds.reloadData()
            self.webServiceForDeletePost(param: param)
        }
        okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func webServiceForDeletePost(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteFeed, params: param  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: "Post deleted successfully", type: alertType.bannerDark, on: self)
                    self.loadFeedsWithPage(page: 0, refresh: true)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)  }}  }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        } }
    
    
    @objc func btnReportThisPost(_ sender: UIButton)
    {
        self.viewFilterButton.isHidden = true
        self.lastSelectFeedIndex = sender.tag
        let objData = objAppShareData.arrFeedsData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
            objVC.objModel = objData
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if tableView == tblFeeds {
            
            if objAppShareData.arrFeedsData.count > indexPath.row{
                
                let objFeeds = objAppShareData.arrFeedsData[indexPath.row]
                
                if objFeeds.feedType == "text" {
                    
                    if let height = objFeeds.cellHeight{
                        return height
                    }else{
                        let height = getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                        objFeeds.cellHeight = height
                        return height
                    }
                    
                }else {
                    if objFeeds.caption.count > 0 {
                        if let height = objFeeds.cellHeight{
                            return height + 6
                        }else{
                            
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else{
                                height = self.view.frame.size.width * 1.25 + 112
                            }
                            
                            let newHeight = height + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 8), font: "Nunito-Regular", fontSize: 16.0).height
                            objFeeds.cellHeight = newHeight
                            
                            return height + 6
                        }
                        
                    }else{
                        
                        if let height = objFeeds.cellHeight{
                            return height
                        }else{
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else if objFeeds.feedImageRatio == "2"{
                                height = self.view.frame.size.width * 1.25 + 112
                            }
                            
                            
                            
                            objFeeds.cellHeight = height
                            return height
                        }
                        //return self.view.frame.size.width * 0.75 + 100
                    }
                }
            }
            
        }else if tableView == self.tblSuggesion {
            return 30
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if tableView == self.tblSuggesion{
            let objSuggession = self.suggesionArray[indexPath.row]
            
            var array = self.txtDescription.text?.components(separatedBy:CharacterSet.whitespacesAndNewlines)
            
            let indexWord = array![currentWordIndex]
            
            // Get multiple ranges of all matched strings
            let subS = self.txtDescription.text?.ranges(of: indexWord)
            
            if let selectedRange = self.txtDescription.selectedTextRange {
                // Get current index text position
                let cursorPosition = self.txtDescription.offset(from: self.txtDescription.beginningOfDocument, to: selectedRange.start)
                
                
                for sub in subS!{
                    
                    let lowR = sub.lowerBound.encodedOffset
                    let upR = sub.upperBound.encodedOffset
                    
                    if cursorPosition >= lowR && cursorPosition <= upR{
                        
                        if indexWord.hasPrefix("@"){
                            var pre = ""
                            for character in indexWord {
                                if character == "@"{
                                    pre = pre+"@"
                                }else{
                                    break
                                }
                            }
                            //self.txtDescription.text = self.txtDescription.text?.replacingCharacters(in: sub, with: pre+"\(objSuggession.userName) ")
                            self.arrPeopleTagSuggesionArray.append(objSuggession)
                            break
                        }else if indexWord.hasPrefix("#"){
                            var pre = ""
                            for character in indexWord {
                                if character == "#"{
                                    pre = pre+"#"
                                }else{
                                    break
                                }
                            }
                            // self.txtDescription.text = self.txtDescription.text?.replacingCharacters(in: sub, with: pre+"\(objSuggession.tag) ")
                            break
                        }else{
                            
                        }
                    }
                }
            }
            
            self.suggesionView.isHidden = true
            self.dataScrollView.isScrollEnabled = true
            self.tblFeeds.isScrollEnabled = true
            self.suggesionArray.removeAll()
            self.tblSuggesion.reloadData()
        }else{
            let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
                //cellOld.btnSaveToFolder.isHidden = false
                //cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.btnHiddenMoreOption.isHidden = true
                cellOld.setDefaultDesign()
            }
        }
    }
    
    func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
        
        let constraintSize = CGSize(width: width, height: .infinity)
        let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
        let attributes = [NSAttributedString.Key.font : font];
        let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
        let stringSize: CGSize = frame.size
        return stringSize
    }
}

//MARK: - Up down Swipe
fileprivate extension HomeVC{
    
    func addGesturesToView() -> Void {
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.upSwipe))
        swipeUp.direction = .up
        //self.dataScrollView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.downSwipe))
        swipeDown.direction = .down
        //self.view.addGestureRecognizer(swipeDown)
        //self.tblFeeds.addGestureRecognizer(swipeDown)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe))
        swipeRight.direction = .right
        self.hiddenPageView.addGestureRecognizer(swipeRight)
        
        let swipeRightNew = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipeNew))
        swipeRightNew.direction = .right
        self.addPostView.addGestureRecognizer(swipeRightNew)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe))
        swipeLeft.direction = .left
        self.hiddenPageView.addGestureRecognizer(swipeLeft)
        
        let swipeLeftNew = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipeNew))
        swipeLeftNew.direction = .left
        self.addPostView.addGestureRecognizer(swipeLeftNew)
    }
    
    @objc func leftSwipe() ->Void {
        if !self.isViewPostShow{
            return
        }
        print("left")
        self.hiddenSecondPageViews()
    }
    @objc func leftSwipeNew() ->Void {
        if !self.isViewPostShow{
            return
        }
        print("left")
        self.hiddenSecondPageViews()
    }
    @objc func rightSwipe() ->Void {
        if self.isViewPostShow{
            return
        }
        print("right")
        self.hiddenFirstPageViews()
    }
    @objc func rightSwipeNew() ->Void {
        if self.isViewPostShow{
            return
        }
        print("right")
        self.hiddenFirstPageViews()
    }
    
    @objc func upSwipe() ->Void{
        // move up
        if dataScrollView.contentOffset.y >= pointToScroll - 20{
            self.tblFeeds.isScrollEnabled = true
        }
    }
    
    @objc func downSwipe() ->Void{
        // move down
        if tblFeeds.contentOffset.y <= 12 {
            self.tblFeeds.contentOffset = CGPoint(x: 0, y: 0)
            self.dataScrollView.isScrollEnabled = true
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !objServiceManager.isNetworkAvailable(){
            refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        if self.btnFeeds.isSelected == true{
            loadFeedsWithPage(page:0, refresh: true)
        }else if self.btnImages.isSelected{
            loadImagesWithPage(page:0, refresh: true)
        }else{
            loadVideosWithPage(page:0, refresh: true)
        }
        self.callWebserviceFor_getStoryUsers()
    }
}

// MARK: - keyboard methods
fileprivate extension HomeVC{
    
    func observeKeyboard() {
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: .UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self](Notification) in
            self?.keyboardWillShow(Notification)
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { [weak self](Notification) in
            self?.keyboardWillHide(Notification)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        UIView.animate(withDuration: 0.4) {
            self.dataScrollView.contentOffset.y = self.userCollection.frame.size.height
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 0.4) {
            self.dataScrollView.contentOffset.y = 0
            self.suggesionView.isHidden = true
            self.dataScrollView.isScrollEnabled = true
            self.tblFeeds.isScrollEnabled = true
        }
    }
}

//MARK: - search tags and users
fileprivate extension HomeVC{
    
    func searchEntries(withSubstring substring: String, andType Type:String) {
        var searchStr = ""
        
        if Type == ""{
            searchStr = substring.replacingOccurrences(of: "#", with: "")
        }else{
            searchStr = substring.replacingOccurrences(of: "@", with: "")
        }
        
        //let searchStr = substring.dropFirst()
        if !(searchStr == "") {
            let dicParam = ["search":searchStr,
                            "type":Type] as [String : Any]
            self.callWebserviceForSearchTags(dicParam: dicParam)
        }
    }
    
    func gotoExploreDetailVCWithSearchText(searchText:String, type:String){
        
        if type == "hashtag" {
            
            let dicParam = [
                "tabType" : "hasTag",
                "tagId": "",
                "title": searchText
                ] as [String : Any]
            
            let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                objVC.sharedDict = dicParam
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let dicParam = ["userName":searchText]
            
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": searchText
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
            }
        }
    }
}

extension String {
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
}

// MARK: - Notification Actions

extension HomeVC {
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
        let objFeed = objAppShareData.arrFeedsData[indexPath.row]
        if objFeed.feedType == "video"{
            isNavigate = true
            self.lastSelectFeedIndex = indexPath.row
            self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            
        }else if objFeed.feedType == "image" {
            /*
             let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
             if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
             
             isNavigate = true
             objShowImage.isTypeIsUrl = true
             objShowImage.arrFeedImages = objFeed.arrFeed
             objShowImage.objFeeds = objFeed
             
             present(objShowImage, animated: true)
             }
             */
            let sb = UIStoryboard(name: "Explore", bundle: Bundle.main)
            if let objDetail = sb.instantiateViewController(withIdentifier:"NewFeedDetailVC") as? NewFeedDetailVC{
                self.navigationController?.pushViewController(objDetail, animated: true)
            }
        }
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        print("1234567")
    }
    
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        let objFeed = objAppShareData.arrFeedsData[indexPath.row]
        if !objFeed.isLike{
            likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
        }
    }
    
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        print("hhdhfhds")
    }
    
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
        print("tagPopover dict = %@",dict)
        self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            if self.myId == tagId {
                isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                //self.tabBarController?.selectedIndex = 4
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
}

extension HomeVC {
    
    func gotoExpPostDetailVC(objFeeds:feeds?){
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            
            objAppShareData.arrFeedsForArtistData.removeAll()
            if let objFeeds = objFeeds{
                objVC.objFeeds = objFeeds
                objAppShareData.arrFeedsForArtistData.append(objFeeds)
                navigationController?.pushViewController(objVC, animated: true)
            }else{
                navigationController?.pushViewController(objVC, animated: false)
            }
        }
    }
    
    func shareLinkUsingActivity(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
        //let str = WebURL.BaseUrl+WebURL.feedDetails + "/" + String(objFeeds._id)
        let str = "http://koobi.co.uk:3000/"
        let url = NSURL(string:str)
        let shareItems:Array = [url]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnFeedProfileAction(_ sender: UIButton){
        let section = 0
        self.lastSelectFeedIndex = (sender as AnyObject).tag
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objFeed = objAppShareData.arrFeedsData[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":(objFeed.userInfo?.userName)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objFeed.userInfo?.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    func manageBookingRequestCount(){
        
        var  idUser = 0
        self.viewBusinessInvitationCount.isHidden = true
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            idUser = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            idUser = userInfo["_id"] as? Int ?? 0
        }
        ref.child("socialBookingBadgeCount").child(String(idUser)).observe(.value, with: { (snapshot) in
            let dict = snapshot.value as? [String:Any]
            
            var totalCount = 0
            if let count = dict?["socialCount"] as? Int {
                totalCount = count+totalCount
            }else if let count = dict?["socialCount"] as? String {
                totalCount = (Int(count) ?? 0)+totalCount
            }
            if let count = dict?["bookingCount"] as? Int {
                totalCount = count+totalCount
            }else if let count = dict?["bookingCount"] as? String {
                totalCount = (Int(count) ?? 0)+totalCount
            }
            if totalCount == 0{
                self.lblBusinessInvitationCount.text = String(totalCount)
            }else{
                self.lblBusinessInvitationCount.text = String(totalCount)
                if totalCount >= 100{
                    self.lblBusinessInvitationCount.text = "99+"
                }
            }
            if totalCount > 0{
                self.viewBusinessInvitationCount.isHidden = false
            }else{
                self.viewBusinessInvitationCount.isHidden = true
            }
        })
        if totalCount > 0{
            self.viewBusinessInvitationCount.isHidden = false
        }else{
            self.viewBusinessInvitationCount.isHidden = true
        }
    }
    
}

