//
//  SaveFeedsVC.swift
//  MualabCustomer
//
//  Created by Mac on 25/10/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

import UIKit
import Accounts
import AVFoundation
import AVKit
import Social
import Alamofire
import AlamofireImage
import FirebaseAuth
import TLPhotoPicker
import Photos
//import HCSStarRatingView

class SaveFeedsVC: UIViewController,SWRevealViewControllerDelegate,UITableViewDelegate, UITableViewDataSource,AVPlayerViewControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate {

        @IBOutlet weak var indicators: UIActivityIndicatorView!
        fileprivate let objChatList = ChatHistoryData()
        @IBOutlet weak var topOfFilterView: NSLayoutConstraint!
        var strSearchText = ""
        @IBOutlet weak var collectionView: UICollectionView!
        @IBOutlet weak var heightTableView: NSLayoutConstraint!
         @IBOutlet weak var viewBlackLayer: UIView!
 
 
        
        var objArtistDetails = ArtistDetails(dict: ["":""])
        
        @IBOutlet weak var lblNoDataFound: UIView!
        @IBOutlet weak var lblHeader: UILabel!
        var strHeader = "Save Feeds"

        @IBOutlet weak var tblFeeds: UITableView!
        //feed image video outlate
        @IBOutlet weak var btnFeeds: UIButton!
        @IBOutlet weak var btnImage: UIButton!
        @IBOutlet weak var btnVideo: UIButton!
        
        ///akash sir code
        @IBOutlet weak var dataScrollView: UIScrollView!
        
        
        //Deependra Code for new design manage
        @IBOutlet weak var lblFilterType: UILabel!
        @IBOutlet weak var imgSearchIcon: UIImageView!
        @IBOutlet weak var viewTextField: UIView!
        @IBOutlet weak var txtSearchText: UITextField!
        @IBOutlet weak var btnTable: UIButton!
        
        @IBOutlet weak var viewMoreFilterOption: UIView!
        
        @IBOutlet weak var btnGride: UIButton!
        fileprivate var indexLastViewMoreView = 0
        var folderId = 0

        //fileprivate var refreshControl = UIRefreshControl()
        fileprivate var pageNo: Int = 0
        fileprivate var totalCount = 0
        fileprivate let pageSize = 20 // that's up to you, really
        fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
        fileprivate var gridType = ""
        
        fileprivate var lastLoadedPage = 0
        
    
        fileprivate var isVideo = false
        fileprivate var isFromImagePicker = false
        fileprivate var isMyStoryIsAdded = false
        fileprivate var isNavigate = false
        
        fileprivate var Image: UIImage?
         fileprivate var strType = ""
        fileprivate var suggesionType = ""
         fileprivate var tblView: UITableView?
          fileprivate var myId:Int = 0
    
        //fileprivate var arrUsers = [UserDetailArtistProfile]()
        
        fileprivate var objUserDetailArtistProfile = UserDetailArtistProfile(dict: ["":""])
        
        fileprivate var arrFeedImages = [UIImage]()
        fileprivate var suggesionArray = [suggessions]()
        
        fileprivate var startValue = 0
        fileprivate var limitValue = 10
        fileprivate var nextPages = 0
        fileprivate var int = 0
        fileprivate var tableNo = 0
        fileprivate var stackManage = true
        fileprivate var LoderManage = true
        fileprivate var viewTableType = true
        
        //MARK: - refreshControl
        lazy var refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
            refreshControl.tintColor = appColor
            return refreshControl
        }()
        
        //MARK: - System method
        override func viewDidLoad() {
            super.viewDidLoad()
            self.addGesturesToMainView()
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    myId = userId
                }
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                if let id = userInfo["_id"] as? Int {
                    myId = id
                }
            }
            self.indicators.stopAnimating()
            //let a = self.dataScrollView.layer.frame.height+295//self.topOfFilterView.constant
            //self.heightTableView.constant = a
            viewConfigure()
            self.viewBlackLayer.isHidden = true
            LoderManage = true
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.hideShowBlurViewForSlideMenu), name: NSNotification.Name(rawValue: "hideShowBlurView"), object: nil)
            LoderManage = true
            self.tblFeeds.isScrollEnabled = true
            self.collectionView.isScrollEnabled = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            //self.getUserProfileDetailsFromServer()
            refreshData(page: 0)
        }
        
        @objc func hideShowBlurViewForSlideMenu(_ objNotify: Notification) {
            print(objNotify)
            self.view.endEditing(true)
            let dict = objNotify.userInfo as! [String:Any]
            let position = dict["xPosition"] as! String
            if position == "0"{
                self.viewBlackLayer.isHidden = true
            }else if position == "1"{
                self.viewBlackLayer.isHidden = false
            }
        }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.indicators.stopAnimating()
            self.lblHeader.text = self.strHeader
            LoderManage = true
            self.viewBlackLayer.isHidden = true
            
            if DoneEditProfile == true{
                objAppShareData.showAlert(withMessage: "Profile updated successfully.", type: alertType.banner, on: self)
            }
 
            if revealViewController() != nil {
                revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            }
            
            self.searchTextDataFromTextfield()
            
            if DoneEditProfile == true{
                DoneEditProfile = false
                dismiss(animated: true, completion: nil)
            }
         }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override func viewDidDisappear(_ animated: Bool) {
            self.indicators.stopAnimating()
        }
        
        func hideRevealViewController(){
            if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            }
        }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Feed Type"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        objAppShareData.arrTBottomSheetModal = ["All","Photo","Video"]
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            if str == "0"{
                strongSelf.btnAllAction()
            }else if str == "1"{
                strongSelf.btnImageAction()
            }else if str == "2"{
                strongSelf.btnVideoAction()
            }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Feed Type"
        objAppShareData.arrTBottomSheetModal = ["All","Photo","Video"]
        tableVc.didSelectHandler = { [weak self] (str) in
            
            guard let strongSelf = self else {
                return
            }
            print(str)
            if str == "0"{
                strongSelf.btnAllAction()
            }else if str == "1"{
                strongSelf.btnImageAction()
            }else if str == "2"{
                strongSelf.btnVideoAction()
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    }
    
    //MARK: - custome method
    extension SaveFeedsVC{
        
        func btnAllAction(){
            self.view.endEditing(true)
            self.viewMoreFilterOption.isHidden = true
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            self.lblFilterType.text = "All"
            LoderManage = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            tableNo = 0
            strType = ""
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            loadFeedsWithPage(page:0, refresh: true)
        }
        
        func btnImageAction(){
            self.viewMoreFilterOption.isHidden = true
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            self.lblFilterType.text = "Photo"
            LoderManage = true
            tableNo = 1
            strType = "image"
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            loadImagesWithPage(page:0, refresh: true)
            
        }
        
        func btnVideoAction(){
            self.view.endEditing(true)
            
            self.viewMoreFilterOption.isHidden = true
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            self.lblFilterType.text = "Video"
            LoderManage = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            tableNo = 2
            strType = "video"
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            loadVideosWithPage(page:0, refresh: true)
        }
        
//        func updateUIAccordingToUserType() {
//
//            if objAppShareData.isOtherSelectedForProfile {
//
//                if objAppShareData.selectedOtherTypeForProfile == "artist"{
//
//                    self.vwChatIcon.isHidden = false
//                    self.vwProfileMenuIcon.isHidden = true
//                    self.vwFavoriteIcon.isHidden = false
//
//                }else{
//
//                    self.vwChatIcon.isHidden = false
//                    self.vwFavoriteIcon.isHidden = true
//                    self.vwProfileMenuIcon.isHidden = true
//                }
//
//            }else{
//
//                if revealViewController() != nil {
//
//                    revealViewController().rightViewRevealWidth = 265 //self.view.frame.size.width * 0.7
//                    menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
//
//                    self.revealViewController().delegate = self
//                    view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//                }
//
//            }
//        }
        
        
        func viewConfigure(){
            self.txtSearchText.delegate = self
            self.viewTextField.isHidden = true
            self.viewMoreFilterOption.isHidden = true
            self.collectionView.isHidden = true
            self.tblFeeds.isHidden = false
            self.viewTableType = true
            self.btnGride.setImage(#imageLiteral(resourceName: "inactive_listing_ico"), for: .normal)
            self.btnTable.setImage(#imageLiteral(resourceName: "menu_icon"), for: .normal)
            gridType = ""
            self.tblFeeds.delegate = self
            self.tblFeeds.dataSource = self
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            
            self.lblNoDataFound.isHidden = true
            
            
            stackManage = true
            tableNo = 0
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }
            
          //  self.updateUIAccordingToUserType()
            
            self.tblFeeds.addSubview(self.refreshControl)
            self.collectionView.addSubview(self.refreshControl)
            
        }
        
        func dataParsing(){
            
            let objDetail : UserDetailArtistProfile = objUserDetailArtistProfile
            
            objArtistDetails.businessName = objDetail.businessName
            objArtistDetails.businessType = objDetail.businessType
            objArtistDetails._id = Int(objDetail._id) ?? 0
            objArtistDetails.profileImage = objDetail.profileImage
            objArtistDetails.ratingCount = Double(Int(objDetail.ratingCount) ?? 0)
            objArtistDetails.userName = objDetail.userName
            
            objChatList.strOpponentName = objDetail.userName
            objChatList.strOpponentProfileImage = objDetail.profileImage
            
          }
        
        func underDevelopment(){
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
    }
    
    //MARK: - Button action extension
    fileprivate extension SaveFeedsVC{
        @IBAction func btnGrideAction(_ sender: UIButton) {
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            
            self.tblFeeds.isScrollEnabled = false
            self.collectionView.isScrollEnabled = true

            self.viewMoreFilterOption.isHidden = true
            
            self.pageNo = 0
            objAppShareData.arrFeedsForArtistData.removeAll()
            //self.collectionView.reloadData()
            gridType = "grid"
            self.collectionView.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            objWebserviceManager.StartIndicator()
            LoderManage = true
            self.viewTableType = false
            self.viewMoreFilterOption.isHidden = true
            self.collectionView.isHidden = false
            self.tblFeeds.isHidden = true
            self.searchTextDataFromTextfield()
            self.btnGride.setImage(#imageLiteral(resourceName: "active_listing_ico"), for: .normal)
            self.btnTable.setImage(#imageLiteral(resourceName: "inactive_menu_icon"), for: .normal)
            if  objAppShareData.arrFeedsForArtistData.count > 0{
                // self.collectionView.scrollToItem(at: IndexPath(item:0, section: 0), at: .top, animated: true)
            }
        }
        
        @IBAction func btnTableAction(_ sender: UIButton) {
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            
            self.pageNo = 0
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.tblFeeds.reloadData()
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            LoderManage = true
            gridType = ""
            self.collectionView.isScrollEnabled = false
            self.tblFeeds.isScrollEnabled = true

            self.viewMoreFilterOption.isHidden = true
            self.viewTableType = true
            self.viewMoreFilterOption.isHidden = true
            self.collectionView.isHidden = true
            self.tblFeeds.isHidden = false
            self.searchTextDataFromTextfield()
            self.btnGride.setImage(#imageLiteral(resourceName: "inactive_listing_ico"), for: .normal)
            self.btnTable.setImage(#imageLiteral(resourceName: "menu_icon"), for: .normal)
            if  objAppShareData.arrFeedsForArtistData.count > 0{
                // self.tblFeeds.scrollToRow(at: IndexPath(row:0, section: 0), at: .top, animated: true)
            }
        }
        
        
        @IBAction func btnManu(_ sender: UIButton) {
            self.view.endEditing(true)
            
            self.viewMoreFilterOption.isHidden = true
            
            self.viewBlackLayer.isHidden = false
        }
        
        @IBAction func btnMoreFilter(_ sender: UIButton) {
            self.view.endEditing(true)
            if viewMoreFilterOption.isHidden == false{
                viewMoreFilterOption.isHidden = true
            }else{
                viewMoreFilterOption.isHidden = false
            }
        }
        
       
        
        
        
        @IBAction func btnFavoriteAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            
            self.view.endEditing(true)
        }
        
        @IBAction func btnChatAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            
            guard objAppShareData.isOtherSelectedForProfile else {
                return
            }
            
            
            objChatList.strOpponentId = String(objAppShareData.selectedOtherIdForProfile)
            objChatList.strOpponentName = objArtistDetails.userName
            objChatList.strOpponentProfileImage = objArtistDetails.profileImage
            objChatShareData.fromChatVC = true
            
            let sb = UIStoryboard(name: "Chat", bundle: nil)
            if let detailVC = sb.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC {
                detailVC.objChatHistoryModel = objChatList
                detailVC.modalPresentationStyle = .fullScreen
                self.present(detailVC, animated: true, completion: nil)
            }
            
        }
        
       @IBAction func btnBackAction(_ sender: Any){
            objAppShareData.clearNotificationData()
            objAppShareData.arrFeedsForArtistData.removeAll()
            navigationController?.popViewController(animated: true)
        }
        
      
        
        @IBAction func btnAboutAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            
            self.underDevelopment()
        }
        
        @IBAction func btnCertificateAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            
            let objComments = storyboard?.instantiateViewController(withIdentifier:"CertificateVC") as? CertificateVC
            navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
        }
        
        //stack ForWard button action
        @IBAction func btnFollowersAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            let objComments = storyboard?.instantiateViewController(withIdentifier:"FollowerVC") as? FollowerVC
            navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
        }
        @IBAction func btnFollowingAction(_ sender: UIButton) {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.viewMoreFilterOption.isHidden = true
            
            let objComments = storyboard?.instantiateViewController(withIdentifier:"FollowingVC") as? FollowingVC
            navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
        }
        
        @IBAction func btnPostAction(_ sender: UIButton) {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.viewMoreFilterOption.isHidden = true
            
            objWebserviceManager.StartIndicator()
            
            if self.lblFilterType.text == "All"{
                strType = ""
                loadFeedsWithPage(page:0, refresh: true)
                
            }else if self.lblFilterType.text == "Photo"{
                strType = "image"
                loadImagesWithPage(page:0, refresh: true)
                
            }else if self.lblFilterType.text == "Video"{
                strType = "video"
                loadVideosWithPage(page:0, refresh: true)
                
            }else{
                strType = ""
            }
            
            tableNo = 0
            //strType = ""
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            //  loadFeedsWithPage(page:0, refresh: true)
        }
        
        @IBAction func btnSearchAction(_ sender: UIButton) {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.viewMoreFilterOption.isHidden = true
            
            if viewTextField.isHidden == true{
                self.viewTextField.isHidden = false
            }else{
                self.viewTextField.isHidden = true
            }
        }
        
        //feed,video,image Button action
        @IBAction func btnFeedsAction(_ sender: UIButton) {
            self.view.endEditing(true)
            self.viewMoreFilterOption.isHidden = true
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            self.lblFilterType.text = "All"
            LoderManage = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            //self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            tableNo = 0
            strType = ""
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            loadFeedsWithPage(page:0, refresh: true)
            
        }
        
        @IBAction func btnImageAction(_ sender: UIButton) {
            self.viewMoreFilterOption.isHidden = true
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            objAppShareData.arrFeedsForArtistData.removeAll()
            //self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            self.lblFilterType.text = "Photo"
            LoderManage = true
            tableNo = 1
            strType = "image"
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            loadImagesWithPage(page:0, refresh: true)
            
        }
        @IBAction func btnVideoAction(_ sender: UIButton) {
            self.view.endEditing(true)
            
            self.viewMoreFilterOption.isHidden = true
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.strSearchText = ""
            self.txtSearchText.text = ""
            self.LoderManage = true
            self.pageNo = 0
            
            self.collectionView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
            self.view.layoutIfNeeded()
            self.lblFilterType.text = "Video"
            LoderManage = true
            objAppShareData.arrFeedsForArtistData.removeAll()
            //self.collectionView.reloadData()
            self.tblFeeds.reloadData()
            tableNo = 2
            strType = "video"
            btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            loadVideosWithPage(page:0, refresh: true)
        }
        
    }
    
    //MARK: - api calling
    extension SaveFeedsVC{
        
        @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
            LoderManage = false
            self.refreshData(page: 0)
        }
        
        func refreshData(page:Int){
            
            self.lastLoadedPage = page
            self.pageNo = page
            
            if tableNo == 0   {
                loadFeedsWithPage(page:self.pageNo, refresh: true)
            }else if tableNo == 1 {
                loadImagesWithPage(page:self.pageNo, refresh: true)
            }else{
                loadVideosWithPage(page:self.pageNo, refresh: true)
            }
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            self.viewMoreFilterOption.isHidden = true
            let text = textField.text! as NSString
            
            if (text.length == 1)  && (string == "") {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                strSearchText = ""
                self.searchTextDataFromTextfield()
                
            }else{
                var substring: String = textField.text!
                substring = (substring as NSString).replacingCharacters(in: range, with: string)
                substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                strSearchText = substring
                // self.searchTextDataFromTextfield()
                self.searchAutocompleteEntries(withSubstring: substring)
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.txtSearchText.resignFirstResponder()
            return true
        }
        
        
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.searchTextDataFromTextfield()
            return true
        }
        
        // MARK: - searching operation
        func searchAutocompleteEntries(withSubstring substring: String) {
            if substring != "" {
                // to limit network activity, reload half a second after last key press.
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
            }
        }
        
        @objc func reload() {
            self.searchTextDataFromTextfield()
        }
        
        func searchTextDataFromTextfield(){
            objWebserviceManager.StartIndicator()
            if self.lblFilterType.text == "All"{
                strType = ""
                loadFeedsWithPage(page:0, refresh: true)
                
            }else if self.lblFilterType.text == "Photo"{
                strType = "image"
                loadImagesWithPage(page:0, refresh: true)
                
            }else if self.lblFilterType.text == "Video"{
                strType = "video"
                loadVideosWithPage(page:0, refresh: true)
            }
            
        }
        
        
        func loadImagesWithPage(page: Int, refresh:Bool) {
            
            var dicParam : [String : Any] = ["": ""]
            if objAppShareData.isOtherSelectedForProfile {
                self.pageNo = page
                
                dicParam = ["feedType": "image",
//                            "search": self.strSearchText,
//                            "page": pageNo,
//                            "grid":self.gridType,
//                            "limit": self.pageSize,
                            "userId":objAppShareData.selectedOtherIdForProfile,
                            "loginUserId": myId,
                            "folderId": self.folderId

                            ] as [String : Any]
                
            }else{
                
                dicParam = ["feedType": "image",
//                            "search": self.strSearchText,
//                            "grid":self.gridType,
//                            "page": pageNo,
//                            "limit": self.pageSize,
                            "userId":myId,
                            "loginUserId": myId,
                            "folderId": self.folderId

                            ] as [String : Any]
                
            }
            
            if !refresh {
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }else{
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
        
        
        func loadVideosWithPage(page: Int, refresh:Bool) {
            
            self.pageNo = page
            self.strType = "video"
            
            
            var dicParam : [String : Any] = ["": ""]
            if objAppShareData.isOtherSelectedForProfile {
                
                
                dicParam = ["feedType": "video",
//                            "search": self.strSearchText,
//                            "grid":self.gridType,
//
//                            "page": self.pageNo,
//                            "limit": self.pageSize,
                            "userId":objAppShareData.selectedOtherIdForProfile,
                            "loginUserId": myId,
                            "folderId": self.folderId
                            ] as [String : Any]
                
            }else{
                
                dicParam = ["feedType": "video",
//                            "search": self.strSearchText,
//                            "grid":self.gridType,
//
//                            "page": pageNo,
//                            "limit": self.pageSize,
                            "userId":myId,
                            "loginUserId": myId,
                            "folderId": self.folderId
                            ] as [String : Any]
                
            }
            
            if !refresh {
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }else{
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
        
        func loadFeedsWithPage(page: Int, refresh:Bool) {
            
            pageNo = page
            lastLoadedPage = page
            
            var dicParam : [String : Any] = ["": ""]
            if objAppShareData.isOtherSelectedForProfile {
                
                dicParam = ["feedType": "",
//                            "search": self.strSearchText,
//                            "page": pageNo,
//                            "grid":self.gridType,
//
//                            "limit": self.pageSize,
                            "userId":objAppShareData.selectedOtherIdForProfile,
                            "loginUserId": myId,
                            "folderId": self.folderId

                            ] as [String : Any]
                
            }else{
                
                dicParam = ["feedType": "",
//                            "search": self.strSearchText,
//                            "grid":self.gridType,
//
//                            "page": pageNo,
//                            "limit": self.pageSize,
                            "userId":myId,
                            "loginUserId": myId,
                            "folderId": self.folderId

                            ] as [String : Any]
                
            }
            
            print("himanshu api call times")
            if !refresh {
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
            }else{
                callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
            }
        }
        
        func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any] , activity:Bool){
            if !objServiceManager.isNetworkAvailable(){
                objWebserviceManager.StopIndicator()
                self.indicators.stopAnimating()
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            if strSearchText != ""{
                self.indicators.startAnimating()
            }else if LoderManage{
                objWebserviceManager.StartIndicator()
            }
            self.LoderManage = false
            
            var parameters = [String:Any]()
            parameters = dicParam as! [String : Any]
            print("parameters",parameters)
            objWebserviceManager.requestPostForJson(strURL: WebURL.getFolderFeed, params: parameters , success: { response in
                print(response)
                self.refreshControl.endRefreshing()
                objWebserviceManager.StopIndicator()
                if self.pageNo == 0 {
                    objAppShareData.arrFeedsForArtistData.removeAll()
                }
                
                let strSucessStatus = response["status"] as? String
                
                if strSucessStatus == k_success{
                    
                    self.totalCount = response["total"] as? Int ?? 0
                    if let arrDict = response["AllFeeds"] as? [[String:Any]] {
                        if arrDict.count > 0 {
                            for dict in arrDict{
                                let obj = feeds.init(dict: dict)
                                if self.viewTableType == true{
                                    if self.strType == "image"{
                                        if obj?.feedType == "image"{
                                            objAppShareData.arrFeedsForArtistData.append(obj!)
                                        }
                                    }else if self.strType == "video"{
                                        if obj?.feedType == "video"{
                                            objAppShareData.arrFeedsForArtistData.append(obj!)
                                        }
                                    }else{
                                        objAppShareData.arrFeedsForArtistData.append(obj!)
                                    }
                                }else{
                                    if obj?.feedType != "text"{
                                        if self.strType == "image"{
                                            if obj?.feedType == "image"{
                                                objAppShareData.arrFeedsForArtistData.append(obj!)
                                            }
                                        }else if self.strType == "video"{
                                            if obj?.feedType == "video"{
                                                objAppShareData.arrFeedsForArtistData.append(obj!)
                                            }
                                        }else{
                                            objAppShareData.arrFeedsForArtistData.append(obj!)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if self.viewTableType == true{
                        self.tblFeeds.reloadData()
                        if objAppShareData.fromLikeComment == false{
                            if  objAppShareData.arrFeedsForArtistData.count > 0{
                                //self.tblFeeds.scrollToRow(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                            }}
                      
                    }else{
                        //self.collectionView.reloadData()
                        if objAppShareData.fromLikeComment == false{
                        }
                        
                    }
                     objAppShareData.fromLikeComment = false
                    self.indicators.stopAnimating()

                 }else{
                    self.indicators.stopAnimating()
                    
                    if self.viewTableType == true{
                        self.tblFeeds.reloadData()
                        if  objAppShareData.arrFeedsForArtistData.count > 0{
                         }
                    }else{
                        ////self.collectionView.reloadData()
                        if  objAppShareData.arrFeedsForArtistData.count > 0{
                         }
                    }
                    objWebserviceManager.StopIndicator()
                    
                    if strSucessStatus == "fail"{
                        self.indicators.stopAnimating()
                        
                        if objAppShareData.arrFeedsForArtistData.count==0{
                            self.lblNoDataFound.isHidden = false
                            
                        }
                    }else{
                        self.indicators.stopAnimating()
                        self.indicators.stopAnimating()

                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                        
                    }
                    
                }
                self.updateUI()
                self.indicators.stopAnimating()
                
            }) { (error) in
                objWebserviceManager.StopIndicator()
                self.indicators.stopAnimating()
                
                self.refreshControl.endRefreshing()
                self.updateUI()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            } }
        
        
       
        
         func updateUI() -> Void {
            
            if objAppShareData.arrFeedsForArtistData.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                
                if self.viewTableType == true{
                    self.tblFeeds.reloadData()
                    if  objAppShareData.arrFeedsForArtistData.count > 0{
                     }
                }else{
                    //self.collectionView.reloadData()
                    if  objAppShareData.arrFeedsForArtistData.count > 0{
                     }
                }
                if self.pageNo == 0 {
                    self.tblFeeds.contentOffset.y = 0
                }
            }
        }
    }
    // MARK:- UITableView Delegate and Datasource
    extension SaveFeedsVC :UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return objAppShareData.arrFeedsForArtistData.count
        }
        
        @objc func cellAPIMethod(){
            self.indicators.startAnimating()
            self.LoderManage = false
            loadFeedsWithPage(page: self.nextPages , refresh: true)
        }
        func collectionView(_ collectionView: UICollectionView,cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
            if objAppShareData.arrFeedsForArtistData.count < self.totalCount {
                
                let nextPage: Int = Int(indexPath.item / pageSize) + 1 //1
                let preloadIndex = nextPage * pageSize - preloadMargin //15
                
                print(" indexPath.item ",indexPath.item)
                print(" preloadIndex ",preloadIndex)
                print(" lastLoadedPage ",lastLoadedPage)
                print(" nextPage ",nextPage)
                print(" pageSize ",pageSize)
                
                print(" naw ",(indexPath.item >= preloadIndex && lastLoadedPage < nextPage))
                
                if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage){
                    print(" naw ",(indexPath.item >= preloadIndex && lastLoadedPage < nextPage))
                    //LoderManage = true
                    if tableNo == 0   {
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                        self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                        nextPages = nextPage
                    }else if tableNo == 1 {
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                        self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                        nextPages = nextPage
                    }else{
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                        self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                        nextPages = nextPage
                    }
                }
            }
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
                indexPath) as? ExploreCollectionCell)
            cell?.imgVwPlay?.isHidden = true
            cell?.imgVwPost.image = UIImage(named: "gallery_placeholder")
            if let index = indexPath.row as? Int {
                let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                
                if objFeeds.feedType == "video"{
                    if objFeeds.arrFeed.count > 0 {
                        if objFeeds.arrFeed[0].videoThumb != "" {
                            cell?.imgVwPost.image = nil
                            if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                                
                                //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: ""))
                                cell?.imgVwPost.sd_setImage(with: url, placeholderImage:nil)
                            }
                        }
                        cell?.imgVwPlay?.isHidden = false
                        cell?.imgVwPlay?.tag = indexPath.row
                        cell?.imgVwPlay?.superview?.tag = indexPath.section
                        let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                        tapForPlayVideo.numberOfTapsRequired = 1
                        cell?.imgVwPlay?.addGestureRecognizer(tapForPlayVideo)
                    }
                }else if objFeeds.feedType == "image"{
                    cell?.imgVwPost.image = UIImage(named: "gallery_placeholder")
                    if objFeeds.arrFeed.count > 0 {
                        
                        if objFeeds.arrFeed[0].feedPost != "" {
                            
                            if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                                //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: ""))
                                cell?.imgVwPost.sd_setImage(with: url, placeholderImage: nil)
                            }
                        }
                    }
                }
            }
            return cell!
        }
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            if touch.view?.isDescendant(of: self.collectionView) == true {
                return false
            }
            return true
        }
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            var cellWidth:CGFloat = 0
            var cellHeight:CGFloat = 20
            cellWidth = CGFloat((self.collectionView.frame.size.width/3)-3)
            cellHeight = cellWidth
            return CGSize(width: cellWidth, height: cellHeight)
        }
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            if objFeed.feedType == "image"{
                 let sb: UIStoryboard = UIStoryboard(name: "Add", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC {
                    isNavigate = true
                    objVC.isTypeIsUrl = true
                    objVC.arrFeedImages = objFeed.arrFeed
                    objVC.objFeeds = objFeed
                    
                //present(objVC, animated: true)
                }}else if objFeed.feedType == "video"{
             }
         }
     }
    
    // MARK:- UITableView Delegate and Datasource
    extension SaveFeedsVC : feedsTableCellDelegate {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            
            if tableView == self.tblFeeds{
                return objAppShareData.arrFeedsForArtistData.count
            }else{
                return 0
            }
        }
        
        @objc func btnHiddenMoreOption(_ sender: UIButton)
        {
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            
            let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
            
            var cellId = "ImageVideo"
            
            if objFeeds.feedType == "text"{
                cellId = "Text"
            }else{
                cellId = "ImageVideo"
            }
            
            tblView = tableView
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? feedsTableCell {
                
                cell.scrollView.removeFromSuperview()
                //amit
                cell.tag = indexPath.row
                cell.indexPath = indexPath
                cell.delegate = self
                //amit
                
                cell.btnHiddenMoreOption.tag = indexPath.row
                cell.btnHiddenMoreOption.addTarget(self, action: #selector(btnHiddenMoreOption(_:)), for: .touchUpInside)
                if objFeeds.feedType == "video"{
                    cell.viewShowTag.isHidden = true
                    cell.pageControll.isHidden=true
                    cell.pageControllView.isHidden = true;
                    
                }else if objFeeds.feedType == "image"{
                    var isTag = false
                    for obj in objFeeds.arrPhotoInfo{
                        if obj.arrTags.count>0{
                            isTag = true
                            break
                        }
                    }
                    if isTag{
                        //cell.viewShowTag.isHidden = false
                        cell.viewShowTag.isHidden = true
                    }else{
                        cell.viewShowTag.isHidden = true
                    }
                    if objFeeds.isSave == 0{
                        //cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                        cell.btnSaveToFolder.setImage(UIImage.init(named: "inactive_book_mark_ico"), for: .normal)
                    }else{
                        // cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                        cell.btnSaveToFolder.setImage(UIImage.init(named: "active_book_mark_ico"), for: .normal)
                    }
                    cell.pageControll.isHidden=false
                    cell.pageControllView.isHidden = false;
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
//                        cell.setPageControllMethod(objFeeds:objFeeds)
//                    }
                }
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                cell.setPageControllMethodNew(objFeeds:objFeeds)
                self.tableView(self.tblFeeds, willDisplay: cell, forRowAt: indexPath)
                
                return cell
                
            }else{
                return UITableViewCell()
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            
            if tableView == tblFeeds {
                
                if objAppShareData.arrFeedsForArtistData.count > indexPath.row{
                    
                    let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                    
                    if objFeeds.feedType == "text" {
                        
                        if let height = objFeeds.cellHeight{
                            return height
                        }else{
                            let height = getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                            objFeeds.cellHeight = height
                            return height
                        }
                        
                    }else {
                        if objFeeds.caption.count > 0 {
                            if let height = objFeeds.cellHeight{
                                return height + 6
                            }else{
                                
                                var height = self.view.frame.size.width * 0.75 + 112
                                if objFeeds.feedImageRatio == "0"{
                                    height = self.view.frame.size.width * 0.75 + 112
                                }else if objFeeds.feedImageRatio == "1"{
                                    height = self.view.frame.size.width + 112
                                }else{
                                    height = self.view.frame.size.width * 1.25 + 112
                                }
                                
                                let newHeight = height + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 8), font: "Nunito-Regular", fontSize: 16.0).height
                                objFeeds.cellHeight = newHeight
                                
                                return height + 6
                            }
                            
                        }else{
                            
                            if let height = objFeeds.cellHeight{
                                return height
                            }else{
                                var height = self.view.frame.size.width * 0.75 + 112
                                if objFeeds.feedImageRatio == "0"{
                                    height = self.view.frame.size.width * 0.75 + 112
                                }else if objFeeds.feedImageRatio == "1"{
                                    height = self.view.frame.size.width + 112
                                }else if objFeeds.feedImageRatio == "2"{
                                    height = self.view.frame.size.width * 1.25 + 112
                                }
                                
                                objFeeds.cellHeight = height
                                return height
                            }
                            //return self.view.frame.size.width * 0.75 + 100
                        }
                    }
                }
                
            }
            return 0;
        }
        
        private func tableView(_ tableView: UITableView, willDisplay cell: feedsTableCell, forRowAt indexPath: IndexPath){
            
            if objAppShareData.arrFeedsForArtistData.count > indexPath.row {
                let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                
                if objFeeds.feedType == "text"{
//                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                    let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                    let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                    //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
                    cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                        
                        self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                        
                    }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                }else{
//                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                    let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                    let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                    cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                        self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                        
                    }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                }
                if objFeeds.isSave == 0{
                    //cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "inactive_book_mark_ico"), for: .normal)
                }else{
                    // cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(UIImage.init(named: "active_book_mark_ico"), for: .normal)
                }
                cell.imgPlay?.tag = indexPath.row
                cell.imgPlay?.superview?.tag = indexPath.section
                cell.btnShare.tag = indexPath.row
                cell.btnShare.superview?.tag = indexPath.section
                cell.btnLike.tag = indexPath.row
                cell.btnLike.superview?.tag = indexPath.section
                cell.imgFeeds?.tag = indexPath.row
                cell.btnMore.tag = indexPath.row
                cell.btnShowTag?.tag = indexPath.row
                cell.lblUserName.text = objFeeds.userInfo?.userName
                cell.lblCity.text = objFeeds.location
                cell.lblLikeCount.text = "\(objFeeds.likeCount)"
                cell.lblCommentCount.text = "\(objFeeds.commentCount)"
                cell.lblTime.text = objFeeds.timeElapsed
                cell.imgProfile.image = UIImage.customImage.user
                cell.imgProfile.setImageFream()
                cell.viewMore.isHidden = true
                cell.btnHiddenMoreOption.isHidden = true
                
                if let pImage = objFeeds.userInfo?.profileImage {
                    
                    if pImage.count > 0 {
                        //cell.imgProfile.af_setImage(withURL:URL(string:pImage)!)
                        cell.imgProfile.sd_setImage(with: URL(string:pImage)!, placeholderImage: nil)
                    }
                }
                
                let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
                tapForLike.numberOfTapsRequired = 2
                
                
                if objFeeds.feedType == "video"{
                    
                    if objFeeds.arrFeed[0].videoThumb.count > 0 {
                        //cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                        cell.imgFeeds?.sd_setImage(with: URL.init(string: objFeeds.arrFeed[0].videoThumb)!, placeholderImage: nil)
                    }
                    cell.imgPlay?.isHidden = false
                    
                    let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                    tapForPlayVideo.numberOfTapsRequired = 1
                    cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                    cell.imgPlay?.addGestureRecognizer(tapForLike)
                    tapForPlayVideo.require(toFail:tapForLike)
                    //let height = self.view.frame.size.width * 0.75
                    let height = self.view.frame.size.width
                    cell.heightOfImageRatio.constant = height
                }else if objFeeds.feedType == "image"{
                    
                    cell.imgPlay?.isHidden = true
                    cell.pageControllView.tag = indexPath.row
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                    tap.numberOfTapsRequired = 1
                    //cell.pageControllView.addGestureRecognizer(tap)
                    //cell.pageControllView.addGestureRecognizer(tapForLike)
//                    cell.viewCollection.addGestureRecognizer(tap)
//                    cell.viewCollection.addGestureRecognizer(tapForLike)
//                    tap.require(toFail:tapForLike)
                    var height = self.view.frame.size.width * 0.75
                    if objFeeds.feedImageRatio == "0"{
                        height = self.view.frame.size.width * 0.75
                    }else if objFeeds.feedImageRatio == "1"{
                        height = self.view.frame.size.width
                    }else{
                        height = self.view.frame.size.width*1.25
                    }
                    cell.heightOfImageRatio.constant = height
                }else{
                    if objAppShareData.validateUrl(objFeeds.caption) {
                        cell.lblCaption.tag = indexPath.row
                    }
                }
                
                if objFeeds.userInfo?._id == myId{
                    cell.btnFollow.isHidden=true;
                    //cell.btnMore.isHidden=true;
                    cell.btnMore.isHidden=false;
                    cell.viewSeparatorViewMore.isHidden = false
                }else{
                    cell.btnFollow.isHidden=false;
                    cell.btnMore.isHidden=false;
                    //cell.viewSeparatorViewMore.isHidden = true
                    cell.viewSeparatorViewMore.isHidden = false
                }
                
                if objFeeds.isLike {
                    cell.btnLike.isSelected = true
                }else {
                    cell.btnLike.isSelected = false
                }
                cell.btnComment.tag = indexPath.row
                //cell.btnViewComment.tag = indexPath.row
                cell.lblLikeCount.tag = indexPath.row
                cell.lblCommentCount.tag = indexPath.row
                cell.lblCommentCount.isUserInteractionEnabled = true
                
                let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
                tapCommentCount.numberOfTapsRequired = 1
                cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
                if (cell.likes) != nil{
                    cell.likes.tag = indexPath.row
                }
                let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                tap1.numberOfTapsRequired = 1
                tap2.numberOfTapsRequired = 1
                let myInt = objFeeds.likeCount
                
                if myInt > 0{
                    cell.lblLikeCount.isUserInteractionEnabled = true
                    cell.likes.isUserInteractionEnabled = true
                    cell.lblLikeCount.addGestureRecognizer(tap2)
                    cell.likes.addGestureRecognizer(tap1)
                }else {
                    cell.lblLikeCount.isUserInteractionEnabled = false
                    cell.likes.isUserInteractionEnabled = false
                }
                
                //Follow status
                cell.btnFollow.tag = indexPath.row
                cell.btnFollow.superview?.tag = indexPath.section
                
                if objFeeds.followerStatus {
                    
                    cell.btnFollow.setTitle("Following", for: .normal)
                    cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                    cell.btnFollow.layer.borderWidth = 1
                    cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.btnFollow.backgroundColor = UIColor.clear
                    
                }else {
                    
                    cell.btnFollow.setTitle("Follow", for: .normal)
                    cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                    cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                    cell.btnFollow.layer.borderWidth = 0
                }
                
                cell.btnMore.tag = indexPath.row
                cell.btnSaveToFolder.tag = indexPath.row
                cell.btnReportThisPost.tag = indexPath.row
                cell.btnEditPost.tag = indexPath.row
                cell.btnDeletePost.tag = indexPath.row
                cell.btnEditPost.addTarget(self, action: #selector(btnEditPostAction(_:)), for: .touchUpInside)
                cell.btnDeletePost.addTarget(self, action: #selector(btnDeletePostAction(_:)), for: .touchUpInside)
                cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
                cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
                cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
            }
        }
 
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        }
        
        
        /////////getSizeForText
        func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
            
            let constraintSize = CGSize(width: width, height: .infinity)
            let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
            let attributes = [NSAttributedString.Key.font : font];
            let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
            let stringSize: CGSize = frame.size
            return stringSize
        }
     }
    
    //MARK: -  like, comment, shear button tablesub method
    extension SaveFeedsVC{
        
        
        @objc func btnMoreAction(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
            if cell.viewMore.isHidden == true{
                
                let id = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
                if String(objData.userId) == id {
                    //cell.btnEditPost.isHidden = false
                    cell.btnEditPost.isHidden = true
                    cell.btnDeletePost.isHidden = false
                    cell.btnReportThisPost.isHidden = true
                }else{
                    cell.btnEditPost.isHidden = true
                    cell.btnDeletePost.isHidden = true
                    cell.btnReportThisPost.isHidden = false
                }
                cell.btnSaveToFolder.isHidden = false
                cell.viewMore.isHidden = false
                cell.btnHiddenMoreOption.isHidden = false
            } else {
                cell.btnSaveToFolder.isHidden = false
                cell.btnReportThisPost.isHidden = true
                cell.viewMore.isHidden = true
                cell.btnHiddenMoreOption.isHidden = true
                cell.setDefaultDesign()
            }
            //cell?.setDefaultDesign()
            
            ////
            let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
            //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
            
            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
                if indexPath != indexPathOld{
                    cellOld.btnSaveToFolder.isHidden = false
                    cellOld.btnReportThisPost.isHidden = true
                    cellOld.viewMore.isHidden = true
                    cellOld.btnHiddenMoreOption.isHidden = true
                    cellOld.setDefaultDesign()
                }
            }
            indexLastViewMoreView = (sender as AnyObject).tag
            ////
        }
        func addGesturesToMainView() {
            let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
            socialLoginTap.delegate = self
            socialLoginTap.cancelsTouchesInView = false
            self.view.addGestureRecognizer(socialLoginTap)
        }
        @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
            ////
            let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
            //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
            
            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
                //if indexPath != indexPathOld{
                cellOld.btnSaveToFolder.isHidden = false
                cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.btnHiddenMoreOption.isHidden = true
                cellOld.setDefaultDesign()
            }
            //}
            ////
        }
        @objc func btnSaveToFolder(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.setDefaultDesign()
            self.webServiceForEditFolder(feedId:String(objData._id))
            
        }
        
        
        func webServiceForEditFolder(feedId:String){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
            objWebserviceManager.StartIndicator()
            let parameters : Dictionary = [
                "feedId" : feedId,
                "folderId":self.folderId] as [String : Any]
            
            objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    objWebserviceManager.StopIndicator()
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.searchTextDataFromTextfield()
                    }else{
                        objWebserviceManager.StopIndicator()
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                        
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
                showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
            }
        }
        
        @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
            
            let objFeeds = objAppShareData.arrFeedsForArtistData[(myLabel.view?.tag)!]
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"LikesListVC") as? LikesListVC{
                objVC.objFeeds = objFeeds
                isNavigate = true
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
            let sender = recognizer.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
            let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
            let index = (sender as AnyObject).tag
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
            if !objFeeds.isLike{
                likeUnlikePost(cell: cell, objFeeds: objFeeds)
            }
        }
        
        @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
            
            
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
                
                objVC.selectedIndex = (myLabel.view?.tag)!
                objVC.isOtherThanFeedScreen = true
                isNavigate = true
                
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
            let indexPath = IndexPath.init(row: (recognizer.view?.tag)!, section: 0)
            let cell = self.tblFeeds.cellForRow(at: indexPath) as! feedsTableCell
            cell.setTagsHidden(!(cell.tagsHidden))
            
            let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
                //cellOld.btnSaveToFolder.isHidden = false
                //cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.btnHiddenMoreOption.isHidden = true
                cellOld.setDefaultDesign()
            }
            /*
            let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
            let sb: UIStoryboard = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC {
                isNavigate = true
                objVC.isTypeIsUrl = true
                objVC.arrFeedImages = objFeed.arrFeed
                objVC.objFeeds = objFeed
                //present(objVC, animated: true)// { _ in }
            }
            */
        }
        
        @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
            isNavigate = true
            let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
            if objFeed.arrFeed.count > 0 {
                addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            }
        }
        
        func addVideo(toVC url: URL) {
            let controller = AVPlayerViewController()
            controller.player = AVPlayer(url: url)
            controller.delegate = self
            controller.player?.play()
            present(controller, animated: true)// { _ in }
        }
    }
    
    //MARK: - click like,comment, shear button
    extension SaveFeedsVC{
        
        @IBAction func btnSharePost(_ sender: UIButton) {
            self.view.endEditing(true)
            
            let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
            
            // let objFeeds: feeds? = objAppshare.arrFeeds[(sender as AnyObject).tag]
            let index = (sender as AnyObject).tag
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            
            let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
            objChooseType.objFeeds = objFeeds
            objChooseType.fromVoucher = false
        self.navigationController?.pushViewController(objChooseType, animated: true)
            
            return
            
            //self.shareLinkUsingActivity(withObject: objFeeds, andTableCell: cell!)
           
            /*
             if (objFeeds.feedType == "video"){
             showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
             } else {
             showShareActionSheet(withObject: objFeeds, andTableCell: cell!)
             }
             */
        }
        
        func shareLinkUsingActivity(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
            //let str = WebURL.BaseUrl+WebURL.feedDetails + "/" + String(objFeeds._id)
            let str = "http://koobi.co.uk:3000/"
            let url = NSURL(string:str)
            let shareItems:Array = [url]
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
            self.present(activityViewController, animated: true, completion: nil)
        }
        func showShareActionSheet(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
            showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
        }
        
        @IBAction func btnComment(onFeed sender: Any) {
            
                    let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
                    if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
                        objVC.selectedIndex = (sender as AnyObject).tag
                        objVC.isOtherThanFeedScreen = true
                        isNavigate = true
                        navigationController?.pushViewController(objVC, animated: true)
                    }
            
        }
        
        @IBAction func btnLikeFeed(_ sender: Any){
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
            let index = (sender as AnyObject).tag
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
        
        
        
        func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
            
            if (objFeeds.isLike){
                objFeeds.isLike = false
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.2, animations: {() -> Void in
                        cell.btnLike.transform = CGAffineTransform.identity
                        cell.btnLike.isSelected = false
                    })
                })
                objFeeds.likeCount = objFeeds.likeCount - 1
            }else{
                objFeeds.isLike = true
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.2, animations: {() -> Void in
                        cell.btnLike.transform = CGAffineTransform.identity
                        cell.btnLike.isSelected = true
                    })
                })
                objFeeds.likeCount = objFeeds.likeCount + 1
            }
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            
            if objFeeds.likeCount > 1{
                cell.lblLikeOrLikes.text = "Likes"
            }else{
                cell.lblLikeOrLikes.text = "Like"
            }
            
            var dob = "2000-01-01"
            var gender = "male"
            
            if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                
                dob = decoded["dob"] as? String ?? "2000-01-01"
                gender = decoded["gender"] as? String ?? "male"
            }
 
            let dicParam  = ["feedId": objFeeds._id,
                             "userId":objFeeds.userInfo?._id ?? 0,
                             "likeById":self.myId,
                             "age":objAppShareData.getAge(from: dob),
                             "gender":gender,
                             "city":objLocationManager.currentCLPlacemark?.locality ?? "",
                             "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
                             "country":objLocationManager.currentCLPlacemark?.country ?? "",
                             "type":"feed"
                ]  as [String : Any]
            
            self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
            
            //   objAppshare.playButtonClickSound()
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            if objFeeds.likeCount > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true
                cell.lblLikeCount.addGestureRecognizer(tap2)
                cell.likes.addGestureRecognizer(tap1)
            }else {
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
        }
 
        func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            var parameters = [String:Any]()
            parameters = dicParam as! [String : Any]
            objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
                objWebserviceManager.StopIndicator()
            }){ error in
                objWebserviceManager.StopIndicator()
            }
        }
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            //RUN pagination
 
        }
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            self.view.endEditing(true)
            self.collectionView.isScrollEnabled = true
            self.tblFeeds.isScrollEnabled = true
            self.viewMoreFilterOption.isHidden = true

//            if objAppShareData.arrFeedsForArtistData.count <= 0{
//                //self.dataScrollView.isScrollEnabled = false
//                let a = self.dataScrollView.layer.frame.height + 295
//                self.heightTableView.constant = a//self.heightTableView.constant
//            }else if self.viewTableType == false {
//                print(self.dataScrollView.contentOffset.y)
//                print(self.collectionView.contentOffset.y)
//                if (self.collectionView.contentOffset.y == 0.0 || self.collectionView.contentOffset.y == 2.0) && (self.dataScrollView.contentOffset.y == 0.0){
//                    self.collectionView.isScrollEnabled = false
//                }else if (self.collectionView.contentOffset.y == 0.0 || self.collectionView.contentOffset.y == 2.0) && (self.dataScrollView.contentOffset.y == 295.0){
//                    self.collectionView.isScrollEnabled = true
//                }
//                let a = self.dataScrollView.layer.frame.height + 295
//                self.heightTableView.constant = a
//                print(self.heightTableView.constant)
//                //dataScrollView.contentOffset = tblFeed
//            }else{
//                //self.dataScrollView.isScrollEnabled = true
//                print(self.dataScrollView.contentOffset.y)
//                print(self.tblFeeds.contentOffset.y)
//                if (self.tblFeeds.contentOffset.y == 0.0) && (self.dataScrollView.contentOffset.y == 0.0){
//                    self.tblFeeds.isScrollEnabled = false
//                }else if (self.tblFeeds.contentOffset.y == 0.0) && (self.dataScrollView.contentOffset.y == 295.0){
//                    self.tblFeeds.isScrollEnabled = true
//                }
//                let a = self.dataScrollView.layer.frame.height + 295
//                self.heightTableView.constant = a
//                print(self.heightTableView.constant)
//             }
        }
 
        
    }
    
    // MARK: - Notification Actions
    extension SaveFeedsVC {
        
        func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
            
            let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            if objFeed.feedType == "video"{
                
                isNavigate = true
                self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
                
            }else if objFeed.feedType == "image" {
                
                let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
                if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                     isNavigate = true
                    objShowImage.isTypeIsUrl = true
                    objShowImage.arrFeedImages = objFeed.arrFeed
                    objShowImage.objFeeds = objFeed
                    
                    //present(objShowImage, animated: true)
                }
            }
        }
        
        func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
             let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            if !objFeed.isLike{
                likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
            }
         }
        
        func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
         }
        
        func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
            print("tagPopover dict = %@",dict)
        self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
        }
        
        func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
            
            var dictTemp : [AnyHashable : Any]?
            
            dictTemp = dict
            
            if let dict1 = dict as? [String:[String :Any]] {
                if let dict2 = dict1.first?.value {
                    dictTemp = dict2
                }
            }
            
            guard let dictFinal = dictTemp as? [String : Any] else { return }
            
            var strUserType: String?
            var tagId : Int?
            
            if let userType = dictFinal["userType"] as? String{
                strUserType = userType
                
                if let idTag = dictFinal["tagId"] as? Int{
                    tagId = idTag
                }else{
                    if let idTag = dictFinal["tagId"] as? String{
                        tagId = Int(idTag)
                    }
                }
                
                if self.myId == tagId {
                    isNavigate = true
                    objAppShareData.isOtherSelectedForProfile = false
                    self.gotoProfileVC()
                    return
                }
                
                if let strUserType = strUserType, let tagId =  tagId {
                     objAppShareData.selectedOtherIdForProfile  = tagId
                    objAppShareData.isOtherSelectedForProfile = true
                    objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                    self.gotoProfileVC()
                }
            }
        }
        
        func gotoProfileVC (){
             self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
            /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }*/
            if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        func gotoExploreDetailVCWithSearchText(searchText:String, type:String){
            
            if type == "hashtag" {
                
                let dicParam = [
                    "tabType" : "hasTag",
                    "tagId": "",
                    "title": searchText
                    ] as [String : Any]
                
                let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                    objVC.sharedDict = dicParam
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }else{
                self.view.endEditing(true)
                
                if !objServiceManager.isNetworkAvailable(){
                    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                    return
                }
                let dicParam = ["userName":searchText]
                
                objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                    if response["status"] as? String ?? "" == "success"{
                        var strId = ""
                        var strType = ""
                        if let dictUser = response["userDetail"] as? [String : Any]{
                            let myId = dictUser["_id"] as? Int ?? 0
                            strId = String(myId)
                            strType = dictUser["userType"] as? String ?? ""
                        }
                        let dic = [
                            "tabType" : "people",
                            "tagId": strId,
                            "userType":strType,
                            "title": searchText
                            ] as [String : Any]
                        self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                    }
                }) { error in
                }
            }
        }
        
        
        @IBAction func btnFeedProfileAction(_ sender: UIButton){
            let section = 0
            let row = (sender as AnyObject).tag
            let indexPath = IndexPath(row: row!, section: section)
            let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.view.isUserInteractionEnabled = false
            let dicParam = ["userName":(objFeed.userInfo?.userName)!]
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                self.view.isUserInteractionEnabled = true
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": objFeed.userInfo?.userName
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
                self.view.isUserInteractionEnabled = true
            }
        }
        
        @IBAction func btnFollowAction(_ sender: Any){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let section = 0
            let row = (sender as AnyObject).tag
            let indexPath = IndexPath(row: row!, section: section)
            let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
            let objUser = objAppShareData.arrFeedsForArtistData[indexPath.row]
            objUser.followerStatus = !objUser.followerStatus
            
            ////
            if objUser.followerStatus {
                cell!.btnFollow.setTitle("Following", for: .normal)
                cell!.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell!.btnFollow.layer.borderWidth = 1
                cell!.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell!.btnFollow.backgroundColor = UIColor.clear
            }else {
                cell!.btnFollow.setTitle("Follow", for: .normal)
                cell!.btnFollow.setTitleColor(UIColor.white, for: .normal)
                cell!.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                cell!.btnFollow.layer.borderWidth = 0
            }
            cell!.btnFollow.isUserInteractionEnabled = false
            ////
            callWebservice(for_Follow: objUser, andCell: cell!)
        }
        
        @objc func btnEditPostAction(_ sender: UIButton)
        {
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
        @objc func btnDeletePostAction(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
                cell.btnSaveToFolder.isHidden = true
                cell.btnReportThisPost.isHidden = true
                cell.setDefaultDesign()
                var myIds = ""
                if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                    myIds = String(dicUser["_id"] as? Int ?? 0)
                }else{
                    let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                    myIds = String(userInfo["_id"] as? Int ?? 0)
                }
                
                
                let param = ["feedType":objData.feedType, "id":String(objData._id), "userId":myIds]
                // objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
                self.deleteCommentAlert(param: param, indexPath: indexPath)
            }
        }
        
        func deleteCommentAlert(param:[String:Any],indexPath:IndexPath){
            // Create the alert controller
            let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this feed?", preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                UIAlertAction in
                // self.totalCount  = self.totalCount  - 1
                //objAppShareData.arrFeedsData.remove(at: indexPath.row)
                //self.tblFeeds.reloadData()
                self.webServiceForDeletePost(param: param)
            }
            okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        
        
        func webServiceForDeletePost(param:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
            objWebserviceManager.StartIndicator()
            
            objWebserviceManager.requestPost(strURL: WebURL.deleteFeed, params: param  , success: { response in
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    objWebserviceManager.StopIndicator()
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        //  self.viewWillAppear(true)
                        self.loadFeedsWithPage(page: 0, refresh: true)
                    }else{
                        objWebserviceManager.StopIndicator()
                        if let msg = response["message"] as? String{    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)  }}  }
            }){ error in
                objWebserviceManager.StopIndicator()
                showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
            } }
        
        @objc func btnReportThisPost(_ sender: UIButton)
        {
            
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.setDefaultDesign()
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
                objVC.objModel = objData
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        func callWebservice(for_Follow objUser: feeds, andCell cell: feedsTableCell){
//            if !objServiceManager.isNetworkAvailable(){
//                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//                return
//            }
            let dicParam = ["followerId": objUser.userInfo?._id ?? 0,
                            "userId":myId]
            /*let followActivity = UIActivityIndicatorView()
            followActivity.tintColor = UIColor.white
            followActivity.color = UIColor.white
            followActivity.hidesWhenStopped = true
            followActivity.center = CGPoint(x: cell.btnFollow.frame.size.width / 2, y: cell.btnFollow.frame.size.height / 2)
            cell.btnFollow.addSubview(followActivity)
            followActivity.startAnimating()
            */
            objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
                /*followActivity.stopAnimating()
                followActivity.removeFromSuperview()
                */
                cell.btnFollow.isUserInteractionEnabled = true
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strSucessStatus = response["status"] as? String ?? ""
                    if strSucessStatus == k_success{
                        /*if objUser.followerStatus {
                            cell.btnFollow.setTitle("Following", for: .normal)
                            cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                            cell.btnFollow.layer.borderWidth = 1
                            cell.btnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                            cell.btnFollow.backgroundColor = UIColor.clear
                        }else {
                            cell.btnFollow.setTitle("Follow", for: .normal)
                            cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                            cell.btnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                            cell.btnFollow.layer.borderWidth = 0
                        }
                        */
                        ////
                        self.loadFeedsWithPage(page: 0, refresh: true)
                        ////
                    }else{
                        
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }}) { error in
                    cell.btnFollow.isUserInteractionEnabled = true
                    objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
 }

