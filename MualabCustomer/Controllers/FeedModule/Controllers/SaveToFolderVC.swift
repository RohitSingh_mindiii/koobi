//
//  SaveToFolderVC.swift
//  MualabCustomer
//
//  Created by Mac on 12/10/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SaveToFolderVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
//autolayout outlet
    @IBOutlet weak var trailingAddButton: NSLayoutConstraint!
    @IBOutlet weak var topPostButton: NSLayoutConstraint!
    
    @IBOutlet weak var btnAddRed: UIButton!
     //other outlet
    @IBOutlet weak var tblSaveFolderList: UITableView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblNoDataFound: UIView!

//add folder view outlet
    @IBOutlet weak var viewAddFolderPopup: UIView!
    @IBOutlet weak var txtFolderName: UITextField!
    
    var arrTableData = [folderList]()
    var strFolderName = ""
    var strFolderId = ""
    var strFeedId = ""
    var editFolder = false

}
//MARK: - system method
extension SaveToFolderVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblSaveFolderList.delegate = self
        self.tblSaveFolderList.dataSource = self
        self.txtFolderName.delegate = self
        self.viewAddFolderPopup.isHidden = true
        self.tblSaveFolderList.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override func viewWillAppear(_ animated: Bool) {
        if objAppShareData.btnAddHiddenONMyFolder{
            self.btnAddRed.isHidden = true
        }else{
            self.btnAddRed.isHidden = false
        }
        self.callWebserviceForGerFolder()
    }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
     }

//MARK: - TableView method
 extension SaveToFolderVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrTableData.count > 0{
             self.lblNoDataFound.isHidden = true
             if self.strFolderName != ""{
                self.btnDone.isHidden = false
            }else{
                self.btnDone.isHidden = true
            }
         }else{
            self.btnDone.isHidden = true
             self.lblNoDataFound.isHidden = false
        }
         return arrTableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellSaveToFolder", for: indexPath) as? CellSaveToFolder{
            
            let obj = self.arrTableData[indexPath.row]

            cell.lblName.text = obj.folderName
            cell.imgChack.isHidden = true
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(btnSaveToFolderCheck(_:)), for: .touchUpInside)
            if cell.lblName.text! == self.strFolderName{
                cell.imgChack.isHidden = false
            }else{
                cell.imgChack.isHidden = true
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
            let obj = self.arrTableData[indexPath.row]
            self.webServiceForDeleteFolder(folderId: String(obj._id))
        })
        
        Delete.image = #imageLiteral(resourceName: "delete_feed")
        //Delete.backgroundColor =  #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        Delete.backgroundColor =  UIColor.init(red: 255.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0)
        //#colorLiteral(red: 255.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1)
        
        let edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
            let obj = self.arrTableData[indexPath.row]
            self.editFolder = true
            self.viewAddFolderPopup.isHidden = false
            self.txtFolderName.text = obj.folderName
            self.strFolderId = String(obj._id)

            //self.webServiceForEditFolder(folderId:  String(obj._id), folderName:  obj.folderName)
        })
        
        edit.image = #imageLiteral(resourceName: "edit_feed")
        edit.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [Delete,edit])
    }
    
}

//MARK: - textfield method
 extension SaveToFolderVC{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtFolderName.resignFirstResponder()
        return true
    }
}
    
//MARK: - button action method
fileprivate extension SaveToFolderVC{
    @objc func btnSaveToFolderCheck(_ sender: UIButton)
    {
        if self.arrTableData.count == 0{
           return
        }
        let objData = self.arrTableData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblSaveFolderList?.cellForRow(at: indexPath) as? CellSaveToFolder)!
        
        if self.strFeedId == ""{
            self.strFolderName = ""
            let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SaveFeedsVC") as? SaveFeedsVC{
                objVC.folderId = objData._id
                 objVC.strHeader = objData.folderName
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
        if cell.lblName.text! == self.strFolderName{
            self.strFolderName = ""
            self.strFolderId = ""
        }else{
            self.strFolderName = cell.lblName.text!
            self.strFolderId = String(objData._id)
        }
        self.tblSaveFolderList.reloadData()
        }
    }
@IBAction func btnCancelFolderCreate(_ sender: UIButton) {
    self.view.endEditing(true)
    self.txtFolderName.text = ""
    self.viewAddFolderPopup.isHidden = true
}

@IBAction func btnDoneFolderCreate(_ sender: UIButton) {
    self.view.endEditing(true)
    self.txtFolderName.text = self.txtFolderName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    if txtFolderName.text?.count == 0{
        objAppShareData.shakeTextField(txtFolderName)
    }else{
        
        for obj in self.arrTableData{
            if obj.folderName == self.txtFolderName.text{
                
            objAppShareData.showAlert(withMessage: "You can not create multiple folder with same name", type: alertType.bannerDark, on: self)
                return
            }
        }
        if editFolder{
            self.webServiceForEditFolder(folderId: strFolderId, folderName: self.txtFolderName.text ?? "")
        }else{
            self.webServiceForCreateFolder()
        }
        self.viewAddFolderPopup.isHidden = true
       
    }
    
}

@IBAction func btnAddFolder(_ sender: UIButton) {
    self.view.endEditing(true)
    editFolder = false
    self.txtFolderName.text = ""
    self.viewAddFolderPopup.isHidden = false
}

@IBAction func btnSaveFeed(_ sender: UIButton) {
    self.view.endEditing(true)
    if self.strFolderName == ""{
        objAppShareData.showAlert(withMessage: "Please select any folder", type: alertType.bannerDark, on: self)
    }else{
    self.webServiceForSaveFeedToFolder(folderId: self.strFolderId, feedId: self.strFeedId)
    }
}

    
@IBAction func btnBackAction(_ sender: UIButton) {
    self.view.endEditing(true)
    objAppShareData.btnAddHiddenONMyFolder = false
    self.navigationController?.popViewController(animated: true)
}
    
@IBAction func btnCancelVC(_ sender: UIButton) {
    self.view.endEditing(true)
}
}


// MARK: - WebService Call
extension SaveToFolderVC {
    
    func callWebserviceForGerFolder() {
        self.arrTableData.removeAll()
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()

        objWebserviceManager.requestGet(strURL: WebURL.getFolder, params: ["":""], success: { response in
            self.arrTableData.removeAll()
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)

            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    if let data =  response["data"] as? [[String:Any]]{
                        for objDict in data{
                            let obj = folderList.init(dict: objDict)
                            self.arrTableData.append(obj!)
                        }
                    }
                    self.tblSaveFolderList.reloadData()
                }else{
                    objWebserviceManager.StopIndicator()
                    self.tblSaveFolderList.reloadData()
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            self.tblSaveFolderList.reloadData()

            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    func webServiceForCreateFolder(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()

        
        let parameters : Dictionary = [
            "folderName" : self.txtFolderName.text ?? "",
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.createFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.callWebserviceForGerFolder()
                }else{
                    objWebserviceManager.StartIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            objWebserviceManager.StartIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    
    func webServiceForDeleteFolder(folderId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "folderId" : folderId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.callWebserviceForGerFolder()
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
        showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    
    func webServiceForEditFolder(folderId:String,folderName:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        self.strFolderName = ""
        let parameters : Dictionary = [
            "folderName" : folderName,
            "folderId":folderId] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.editFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.callWebserviceForGerFolder()
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }

                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    
    func webServiceForSaveFeedToFolder(folderId:String,feedId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "folderId":folderId,
            "feedId":feedId
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.saveToFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.strFeedId = ""
                    self.strFolderId = ""
                    self.strFolderName = ""
                    self.tblSaveFolderList.reloadData()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: "Saved to Folder successfully", type: alertType.bannerDark,on: self)
                    }
                }else{
                   objWebserviceManager.StopIndicator()
                    self.strFeedId = ""
                    self.strFolderId = ""
                    self.strFolderName = ""
                    self.tblSaveFolderList.reloadData()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            self.strFeedId = ""
            self.strFolderId = ""
            self.strFolderName = ""
            self.tblSaveFolderList.reloadData()
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
}
