//
//  ShowStoriesVC.swift
//  Mualab
//
//  Created by MINDIII on 10/24/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import Kingfisher

@objc protocol ShowStoriesVCDelegate {
    func recall()
}

class ShowStoriesVC: UIViewController,CameraVCDelegate {
    
    //Recall API
    func recall() {
        self.delegate?.recall()
    }
    
    var arrUsres = [storyUser]()
    var isMyStories = false
    var selectedIndex : Int = 0
    var myStoryCount : Int = 0
    
    var objStoryUser : storyUser?

    @IBOutlet weak var viewCommentAction: UIView!
    @IBOutlet weak var viewEditCommentAction: UIView!
    @IBOutlet weak var btnMoreOptions: UIButton!
    @IBOutlet weak var imgMoreOptions: UIImageView!

    fileprivate var avPlayer = AVPlayer()
    fileprivate let avPlayerController = AVPlayerViewController()
    fileprivate var arrStories = [stories]()
    fileprivate var progressTimer: Timer?
    fileprivate var currentIndex: Int = 0
    fileprivate var isClosed = false
    
    fileprivate var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    fileprivate var currentVideoSec : Float = 0.0
    fileprivate var isVideo = false
    
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var wholeDataView: UIView!
    @IBOutlet weak var lockView: UIView!
    
    @IBOutlet weak var progressStack: UIStackView!
    
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    @IBOutlet weak var tempImageView:UIImageView!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var imgView:UIImageView!

    //
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var btnAdd:UIButton!

    weak var delegate:ShowStoriesVCDelegate?

    fileprivate var prg: UIProgressView?
}
// MARK: - LocalMethods

fileprivate extension ShowStoriesVC{
    
    func saveData(_ dicResponse: [AnyHashable: Any]) {
       
        currentIndex = 0
        self.arrStories.removeAll()
        let arrData = dicResponse["allMyStory"] as! [[String:Any]]
        for dicData in arrData {
            let objStories = stories(dict:dicData)
            arrStories.append(objStories)
            if objStories.type == "image"{
                //tempImageView.af_setImage(withURL: URL(string:objStories.myStory)!)
                tempImageView.sd_setImage(with: URL(string:objStories.myStory)!, completed: nil)
            }else{
                if objStories.videoThumb.count>0{
                   //tempImageView.af_setImage(withURL: URL(string:objStories.videoThumb)!)
                    tempImageView.sd_setImage(with: URL(string:objStories.videoThumb)!, completed: nil)
                }
            }
        }
        ////
        self.makeProgressWith(count: arrStories.count)
        ////
        if  objAppShareData.isFromNotification  && arrStories.count > 0{
            self.makeProgressWith(count: arrStories.count)
        }
        
        if self.progressStack.arrangedSubviews.count > 0{
            self.startProgress(withProgressBar: self.progressStack.arrangedSubviews[self.currentIndex] as! UIProgressView)
        }else{
            //print("===============NUll============")
        }
    }
    
    
    
    func callWebserviceDeleteStory(dicParam: [String:Any]){
        
        self.activityIndicator.startAnimating()
      
        objWebserviceManager.requestPost(strURL:WebURL.deleteStory, params: dicParam , success: { response in
            //var strStatus:String? = ""
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                //strStatus = response["message"] as? String
                if strSucessStatus == k_success{
                    NotificationCenter.default.post(name: NSNotification.Name("refreshStoryCollection"), object: nil)

                    self.dismiss(animated: true, completion: nil)
                }else{
                }
            }}) { error in
                self.activityIndicator.stopAnimating()
        }
    }
    
    // MARK: - Webservices
func callWebservice(for_GetMyStories dicParam: [AnyHashable: Any]){
    
        self.activityIndicator.startAnimating()
        self.dataView.isUserInteractionEnabled = false
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL:WebURL.myStory, params: parameters , success: { response in
            self.btnMoreOptions.isUserInteractionEnabled = true
            //var strStatus:String? = ""
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
            let strSucessStatus = response["status"] as? String ?? ""
            //strStatus = response["message"] as? String
            if strSucessStatus == k_success{
                self.saveData(response)
            }else{
                self.lockView.isHidden = true
                self.dataView.isUserInteractionEnabled = true
                self.activityIndicator.stopAnimating()
                objAppShareData.clearNotificationData()
                self.dismiss(animated: true, completion: nil)
            }
            }}) { error in
            self.btnMoreOptions.isUserInteractionEnabled = true
            self.lockView.isHidden = true
            self.dataView.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
        }
    }
    
// MARK:- IBActions Buttons
    
    @IBAction func btnClose(_ sender: Any) {
        avPlayer.pause()
        objAppShareData.clearNotificationData()
        dismiss(animated: true) //{ _ in }
    }
    
    @IBAction func btnAdd(_ sender: Any){
        let objCamera = self.storyboard?.instantiateViewController(withIdentifier: "CameraVC") as? CameraVC
        objCamera?.delegate = self
        objCamera!.modalPresentationStyle = .fullScreen
        self.present(objCamera!, animated: false, completion: nil)
    }
    
// MARK: - Load Data
    func startProgress(withProgressBar progressView: UIProgressView) {
       
        if isClosed{
            self.progressTimer?.invalidate()
            for pView in progressStack.arrangedSubviews{
                pView.removeFromSuperview()
            }
            return
        }
        
        self.currentVideoSec = 0.0
        if arrStories.count>currentIndex{
        }else{
            return
        }
        let objStories = arrStories[currentIndex]
        
        activityIndicator.startAnimating()
        self.progressTimer?.invalidate()
        avPlayer.pause()
        self.imgView?.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        if objStories.type == "image"{
            isVideo = false
        let imageRequest = URLRequest(url:(URL(string: (objStories.myStory)))!, cachePolicy: .returnCacheDataElseLoad,timeoutInterval:0.0)
//        imgView?.af_setImage(withURLRequest: imageRequest, placeholderImage: nil, completion: { image in
//            if image.result.isSuccess{
//                self.self.lockView.isHidden = true
//                self.dataView.isUserInteractionEnabled = true
//                self.activityIndicator.stopAnimating()
//                self.imgView?.image = image.result.value
//                self.currentIndex = self.currentIndex + 1
//                DispatchQueue.main.async {
//                    progressView.progress=0.0
//                    self.prg = progressView
//                    self.currentVideoSec = 0.005
//                    self.progressTimer = Timer.scheduledTimer(timeInterval: TimeInterval(self.currentVideoSec), target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
//                }
//            }
//        })
            imgView.sd_setImage(with: (URL(string: (objStories.myStory)))!) { (image, error, cache, urls) in
                if (error != nil) {
                    self.imgView?.image = nil
                } else {
                    // Successful in loading image
                                    self.lockView.isHidden = true
                                    self.dataView.isUserInteractionEnabled = true
                                    self.activityIndicator.stopAnimating()
                                    self.imgView?.image = image
                                    self.currentIndex = self.currentIndex + 1
                                    DispatchQueue.main.async {
                                        progressView.progress=0.0
                                        self.prg = progressView
                                        self.currentVideoSec = 0.005
                                        self.progressTimer = Timer.scheduledTimer(timeInterval: TimeInterval(self.currentVideoSec), target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
                                    }
                }
            }
        //// Missing setImageWithURLRequest
        }else{
            isVideo = true
            lockView.isHidden = false
            self.dataView.isUserInteractionEnabled = false
            if objStories.videoThumb.count>0{
                //self.imgView.af_setImage(withURL: URL(string: objStories.videoThumb)!)
                self.imgView.sd_setImage(with: URL(string: objStories.videoThumb)!, completed: nil)
            }
            self.playVideoWith(videoURL: URL(string:objStories.myStory)!, progressView: progressView)
        }
    }
    
    @objc func updateProgressBar(){
        if isVideo{
            let newProgress: Float = (prg?.progress)! + 0.01
            prg?.setProgress(newProgress,animated: true)
        }else{
            let newProgress: Float = (prg?.progress)! + 0.001
            prg?.setProgress(newProgress, animated: true)
        }
        let Pgbar = Double((prg?.progress)!)
        if Pgbar >= 1.0 && currentIndex < arrStories.count{
            self.progressTimer?.invalidate()
            startProgress(withProgressBar: (progressStack.arrangedSubviews[currentIndex] as! UIProgressView))
        }else if Pgbar >= 1.0 && currentIndex == arrStories.count{
            self.progressTimer?.invalidate()
            leftSwipe()
        }
    }
}

//MARK: - View Hirarchy functions
extension ShowStoriesVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isClosed = false
        self.modalPresentationCapturesStatusBarAppearance = true
        self.viewCommentAction.isHidden = true
        prg = UIProgressView()
        addGesturesToView()
        if isMyStories {
            self.btnMoreOptions.isHidden = false
            self.imgMoreOptions.isHidden = false
            self.setYourData()
        }else {
            self.btnMoreOptions.isHidden = true
            self.imgMoreOptions.isHidden = true
            if objAppShareData.isFromNotification {
                
                if let objUser = self.objStoryUser {
    
                    if objUser.profileImage != ""{
                        //imgProfile.af_setImage(withURL: URL(string:objUser.profileImage)!)
                        imgProfile.sd_setImage(with: URL(string:objUser.profileImage)!, completed: nil)
                    }else{
                        imgProfile.image = UIImage.customImage.user
                    }
                    lblUserName.text = objUser.userName
                    callWebservice(for_GetMyStories:["userId":objUser._id])
                    self.btnAdd.isHidden = true
                    self.btnMoreOptions.isHidden = true
                    self.imgMoreOptions.isHidden = true
                }

            }else{
               loadStoriesWithSelectedIndex(index:selectedIndex)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgProfile.setImageFream()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isClosed = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async {
            self.avPlayer.pause()
        }
         self.progressTimer?.invalidate()
    }
}

private extension ShowStoriesVC{
    
    func playVideoWith(videoURL:URL , progressView:UIProgressView) -> Void{
        
        let sourceAsset = AVURLAsset(url:videoURL, options: nil)
        let duration: CMTime = sourceAsset.duration
        let sec = CMTimeGetSeconds(duration)
        self.currentVideoSec = Float(sec/100)
        let playerItem = CachingPlayerItem(url:videoURL)
        playerItem.delegate = self
        //avPlayer = AVPlayer(url:videoURL)
        avPlayer = AVPlayer(playerItem:playerItem)
        if #available(iOS 10.0,*) {
            avPlayer.automaticallyWaitsToMinimizeStalling = false
        } else {
            // Fallback on earlier versions
        }
        avPlayer.isMuted = false
        let avPlayerLayer = AVPlayerLayer(player:avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        avPlayerLayer.frame = (self.imgView?.frame)!
        self.imgView?.layer.addSublayer(avPlayerLayer)
        avPlayerController.player = avPlayer
        avPlayer.play()
        self.currentIndex = self.currentIndex + 1
        DispatchQueue.main.async {
            progressView.progress=0.0
            self.progressTimer?.invalidate()
            self.prg = progressView
            self.progressTimer = Timer.scheduledTimer(timeInterval:1.0,target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: false)
        }
    }
    
    func touchOnRight () -> Void{
        if currentIndex<arrStories.count{
            self.avPlayer.pause()
            self.prg?.setProgress(1.0, animated:false)
            self.progressTimer?.invalidate()
            startProgress(withProgressBar: progressStack.arrangedSubviews[currentIndex] as! UIProgressView)
        }else if currentIndex == arrStories.count{
            leftSwipe()
        }
    }

    func touchOnLeft() -> Void {
        if currentIndex > 1{
            self.avPlayer.pause()
            self.prg?.setProgress(0.0,animated:false)
            self.progressTimer?.invalidate()
            currentIndex = currentIndex - 2
            startProgress(withProgressBar: progressStack.arrangedSubviews[currentIndex] as! UIProgressView)
        }else{
            rightSwipe()
        }
    }
    
    func loadStoriesWithSelectedIndex(index:Int) ->Void{
       
        if arrUsres.count>0{
            let objUser = arrUsres[index]
            //self.makeProgressWith(count: objUser.storyCount)
            
            if objUser.profileImage != ""{
                //imgProfile.af_setImage(withURL: URL(string:objUser.profileImage)!)
                imgProfile.sd_setImage(with: URL(string:objUser.profileImage)!, completed: nil)
            }else{
                imgProfile.image = UIImage.customImage.user
            }
            lblUserName.text = objUser.userName
            callWebservice(for_GetMyStories:["userId":objUser._id])
            self.btnAdd.isHidden = true
            self.btnMoreOptions.isHidden = true
            self.imgMoreOptions.isHidden = true
        }else{
            //print("===============NUll============")
        }
    }
    
//MARK: - add Swipe gesture to view
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe))
        swipeRight.direction = .right
        self.lockView.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe))
        swipeLeft.direction = .left
        self.lockView.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeDown = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureRecognizerHandler(_:)))//UISwipeGestureRecognizer(target: self, action: #selector(self.downSwipe))
       // swipeDown.direction = .down
        
        self.wholeDataView.addGestureRecognizer(swipeDown)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnDataView))
        tapGesture.numberOfTapsRequired = 1
        self.dataView.addGestureRecognizer(tapGesture)
        
        let tapGestureCancle = UITapGestureRecognizer(target: self, action: #selector(self.tapOnDataViewForNoAction))
        tapGestureCancle.numberOfTapsRequired = 2
        
        let tapGestureCancle2 = UITapGestureRecognizer(target: self, action: #selector(self.tapOnDataViewForNoAction))
        tapGestureCancle2.numberOfTapsRequired = 3
        
        self.dataView.addGestureRecognizer(tapGestureCancle)
        self.dataView.addGestureRecognizer(tapGestureCancle2)

        tapGesture.require(toFail: tapGestureCancle)
        tapGesture.require(toFail: tapGestureCancle2)
        
        let longPressGesture =  UILongPressGestureRecognizer(target: self, action: #selector(self.holdDataView))
        longPressGesture.minimumPressDuration = 0.25
        self.dataView.addGestureRecognizer(longPressGesture)
        
        swipeDown.require(toFail: swipeLeft)
        swipeDown.require(toFail: swipeRight)

    }
    
    @objc func leftSwipe() ->Void {
        self.viewCommentAction.isHidden = true

         if selectedIndex < arrUsres.count-1{
            swipeAnimation(WithDirection:"right")
            selectedIndex = selectedIndex+1
            loadStoriesWithSelectedIndex(index:selectedIndex)
        }else if selectedIndex >= arrUsres.count-1{
            if !isClosed{
                objAppShareData.clearNotificationData()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func rightSwipe() ->Void{
        self.viewCommentAction.isHidden = true

        if selectedIndex > 0 {
            swipeAnimation(WithDirection:"left")
            selectedIndex = selectedIndex-1
            loadStoriesWithSelectedIndex(index:selectedIndex)
        }else if selectedIndex == 0{
            if self.myStoryCount > 0{
                swipeAnimation(WithDirection:"left")
                selectedIndex = -1
                setYourData()
            }
        }
    }
    @objc func downSwipe(_ sender : UISwipeGestureRecognizer) ->Void{
        avPlayer.pause()
        objAppShareData.clearNotificationData()
        dismiss(animated: true) {
            let obj = HomeVC()
            obj.callWebserviceFor_getStoryUsers()
        }
    }
    
    @objc func tapOnDataView(_ sender : UITapGestureRecognizer) ->Void{
        
        let tappedView = sender.view
        let location = sender.location(in:tappedView)
        
        if(location.x < self.dataView.frame.size.width/2-10){
            touchOnLeft()
        }else if(location.x > self.dataView.frame.size.width/2+10){
            self.touchOnRight()
        }else{
            self.touchOnRight()
        }
    }

    @objc func tapOnDataViewForNoAction(_ sender : UITapGestureRecognizer) ->Void{

    }

    @objc func holdDataView(_ sender : UILongPressGestureRecognizer) ->Void{
        if sender.state == UIGestureRecognizer.State.began{
            self.progressTimer?.invalidate()
            self.avPlayer.pause()
        self.prg?.setProgress((self.prg?.progress)!,animated:false)
        }else if sender.state == UIGestureRecognizer.State.ended{
            if isVideo{
               self.avPlayer.play()
            }
            self.progressTimer = Timer.scheduledTimer(timeInterval:TimeInterval(self.currentVideoSec),target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
        }
    }
    //
    func swipeAnimation(WithDirection direction:String) {
        
        DispatchQueue.main.async {
            self.imgView.image = nil
            self.progressTimer?.invalidate()
            self.self.avPlayer.pause()
        }
        //**//
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.duration = 0.7
        transition.type = CATransitionType(rawValue: "cube")
        if direction == "left"{
            transition.subtype = CATransitionSubtype.fromLeft
        }else{
            transition.subtype = CATransitionSubtype.fromRight
        }
        self.wholeDataView.layer.add(transition, forKey:nil)
        //**//
    }
    
    //////////Swipe Down to close view////////
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.wholeDataView?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.wholeDataView.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.wholeDataView.frame.size.width, height: self.wholeDataView.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 150 {
                objAppShareData.clearNotificationData()
                dismiss(animated: true, completion: {
                    self.delegate?.recall()
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.wholeDataView.frame = CGRect(x: 0, y: 0, width: self.wholeDataView.frame.size.width, height: self.wholeDataView.frame.size.height)
                })
            }
        }
    }
}

extension ShowStoriesVC: CachingPlayerItemDelegate {
    
    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data) {
        DispatchQueue.main.async {
            if !self.isClosed{
            self.activityIndicator.stopAnimating()
            self.lockView.isHidden = true
            self.dataView.isUserInteractionEnabled = true
            self.avPlayer.play()
            self.progressTimer?.invalidate()
                self.progressTimer = Timer.scheduledTimer(timeInterval:TimeInterval(self.currentVideoSec),target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
            }
        }
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, didDownloadBytesSoFar bytesDownloaded: Int, outOf bytesExpected: Int) {

    }
    
    func playerItemPlaybackStalled(_ playerItem: CachingPlayerItem) {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.dataView.isUserInteractionEnabled = false
            self.progressTimer?.invalidate()
            self.avPlayer.pause()
            self.prg?.setProgress((self.prg?.progress)!,animated:false)
        }
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, downloadingFailedWith error: Error) {
        //print(error)
    }
    
    func playerItemReadyToPlay(_ playerItem: CachingPlayerItem){

        if !isClosed{
        self.progressTimer?.invalidate()
        self.activityIndicator.stopAnimating()
        self.lockView.isHidden = true
        self.dataView.isUserInteractionEnabled = true
        self.avPlayer.play()
            self.progressTimer = Timer.scheduledTimer(timeInterval:TimeInterval(self.currentVideoSec),target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
        }
    }
}
//MARK : - new updates Methods
fileprivate extension ShowStoriesVC{
    
    func setYourData(){
        var userInfo = [:] as! [String:Any]
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            userInfo = dict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            userInfo = dict
        }
        //let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! [String:Any]
        let strProfile = userInfo["profileImage"] as? String ?? ""
        if strProfile != "" {
            //imgProfile.af_setImage(withURL: URL(string:strProfile)!)
            imgProfile.sd_setImage(with: URL(string:strProfile)!, completed: nil)
        }else{
            imgProfile.image = UIImage.customImage.user
        }
        lblUserName.text = "You"
        self.makeProgressWith(count: myStoryCount)
        callWebservice(for_GetMyStories: ["userId":""])
        self.btnAdd.isHidden = false
        self.btnMoreOptions.isUserInteractionEnabled = false
        self.btnMoreOptions.isHidden = false
        self.imgMoreOptions.isHidden = false
    }
    
    func makeProgressWith(count:Int){
        
        for pView in self.progressStack.arrangedSubviews{
            pView.removeFromSuperview()
        }
        
        for _ in 0..<count {
            let progressView = UIProgressView(progressViewStyle: .default)
            progressView.frame = CGRect(x: 0, y: 0, width: 0,height: 0)
            progressView.progressTintColor = appColor//UIColor.theameColors.pinkColor
            progressView.trackTintColor = colorGray
            progressView.progress = 0.0
            self.progressStack.addArrangedSubview(progressView)
        }
    }
    
}

//MARK : - Edit & Delete Section
fileprivate extension ShowStoriesVC{
   
    @IBAction func btnMoreOptionAction(_ sender: UIButton){
        self.viewEditCommentAction.isHidden = true
        self.viewCommentAction.isHidden = false
        self.progressTimer?.invalidate()
        self.avPlayer.pause()
    self.prg?.setProgress((self.prg?.progress)!,animated:false)
    }
    
    @IBAction func btnEditCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
//        let objReminder = self.objHoldIndex
//        let param = ["id":String(objReminder?._id ?? 0),"feedId":objReminder?.feedId]
//        objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
//        //self.arrComntList.remove(at: indexOfArray)
//        self.tblComntList.reloadData()
//        //self.webServiceForDeleteComment(param: param)
    }
    
    @IBAction func btnDeleteCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        var id = ""
        if currentIndex > 0{
            if self.arrStories.count >= currentIndex{
                id = String (self.arrStories[currentIndex-1]._id)
            }
        }else{
                id = String (self.arrStories[0]._id)
        }
        let params = ["id":id]
        deleteCommentAlert(param:params)
    }
    
    @IBAction func btnCommentActionPopUpHiddden(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        if isVideo{
            self.avPlayer.play()
        }
        self.progressTimer = Timer.scheduledTimer(timeInterval:TimeInterval(self.currentVideoSec),target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
    }
    
    func deleteCommentAlert(param:[String:Any]){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this story?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            //objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
            self.callWebserviceDeleteStory(dicParam: param)
        }
      okAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            if self.isVideo{
                self.avPlayer.play()
            }
            self.progressTimer = Timer.scheduledTimer(timeInterval:TimeInterval(self.currentVideoSec),target: self, selector: #selector(self.updateProgressBar), userInfo: nil, repeats: true)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
