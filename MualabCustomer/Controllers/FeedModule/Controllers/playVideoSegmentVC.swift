//
//  playVideoSegmentVC.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import AVKit

class playVideoSegmentVC: UIViewController {

    var arrVideoURL = [String]()
    var currentIndex = 0
    
    @IBOutlet weak var playerView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
}

fileprivate extension playVideoSegmentVC{
    @IBAction func btnNextPre(_ sender:UIButton){
        if sender.tag == 0{
            if currentIndex > 0{
                currentIndex = currentIndex - 1
            }
        }else{
            if currentIndex < self.arrVideoURL.count - 1{
                currentIndex = currentIndex + 1
            }
        }
        self.addVideo(toVC: self.arrVideoURL[currentIndex])
    }
    
    @IBAction func btnClose(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPlay(){
        self.addVideo(toVC: self.arrVideoURL[currentIndex])
    }
    
    
    func addVideo(toVC url: String) {
        // create an AVPlayer
        let imageData = try! Data.init(contentsOf:URL(string: url)!)
        let player = AVPlayer(url:URL(string: url)!)
        let controller = AVPlayerViewController()
        controller.player = player
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: {() -> Void in
            controller.player?.play()
        })
        
    }
    
}
