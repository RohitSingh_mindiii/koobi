//
//  PeopleTag.swift
//  MualabCustomer
//
//  Created by Mac on 17/07/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class PeopleTag {
    
    var x_axis : Double = 0
    var y_axis : Double = 0
    var unique_tag_id : String = ""
    var dictTagDetails : [String : Any]?
    
    init(dict : [String:Any]){
        
        if let unique_tag_id = dict["unique_tag_id"] as? String{
            self.unique_tag_id = unique_tag_id
        }
        
        if let x_axis = dict["x_axis"] as? Double{
            self.x_axis = x_axis
        }else{
            if let x_axis = dict["x_axis"] as? String{
                self.x_axis = Double(x_axis) ?? 0
            }
        }
        
        if let y_axis = dict["y_axis"] as? Double{
            self.y_axis = y_axis
        }else{
            if let y_axis = dict["y_axis"] as? String{
                self.y_axis = Double(y_axis) ?? 0
            }
        }
        
        if let dictTagDetails = dict["tagDetails"] as? [String : Any]{
            self.dictTagDetails = dictTagDetails
        }
    }    
}
