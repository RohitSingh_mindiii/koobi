//
//  suggessions.swift
//  MualabCustomer
//
//  Created by Mac on 10/04/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class suggessions {
    
    var _id : Int = 0
    var tag : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var profileImage : String = ""
    var userName : String = ""
    
    init?(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        if let tag = dict["tag"] as? String{
            self.tag = tag
        }
    }
}



class folderList {
    
    var _id : Int = 0
    var folderName : String = ""
  

    init?(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        if let firstName = dict["folderName"] as? String{
            self.folderName = firstName
        }
    }
}
