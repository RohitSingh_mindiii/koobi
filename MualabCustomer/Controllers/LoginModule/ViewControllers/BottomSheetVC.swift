//
//  ViewController.swift
//  Table View Koobi
//
//  Created by Traning-IOS on 31/10/19.
//  Copyright © 2019 Traning-IOS. All rights reserved.
//

import UIKit

class BottomSheetVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblFilterInbox: UITableView!
    @IBOutlet weak var viewOptions: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var viewSwipe: UIView!
    @IBOutlet weak var heightTblFilterInbox: NSLayoutConstraint!
    var callback : ((String) -> Void)?
    var strHeaderTile = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.tblFilterInbox.delegate = self
        self.tblFilterInbox.dataSource = self
        self.viewOptions.isHidden = false
        self.lblHeaderTitle.text = strHeaderTile
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        upSwipe.direction = .up
        downSwipe.direction = .down
        upSwipe.cancelsTouchesInView = false
        downSwipe.cancelsTouchesInView = false
        self.viewOptions.addGestureRecognizer(upSwipe)
        self.viewOptions.addGestureRecognizer(downSwipe)
        
//        let path = UIBezierPath(roundedRect:self.viewCorner.bounds,
//                                byRoundingCorners:[.topRight, .topLeft],
//                                cornerRadii: CGSize(width: 15, height:  20))
//        let maskLayer = CAShapeLayer()
//        maskLayer.path = path.cgPath
//        self.viewCorner.layer.mask = maskLayer
     
        self.tblFilterInbox.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let path = UIBezierPath(roundedRect:self.viewCorner.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 15, height:  20))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.viewCorner.layer.mask = maskLayer
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        /*if (sender.direction == .up) {
            print("Swipe Up")
            let labelPosition = CGPoint(x: self.viewOptions.frame.origin.x , y: self.viewOptions.frame.origin.y - 10)
            self.viewOptions.frame = CGRect(x: labelPosition.x, y: labelPosition.y, width: self.viewOptions.frame.size.width, height: self.viewOptions.frame.size.height)
        }
        
        if (sender.direction == .down) {
            print("Swipe Down")
            let labelPosition = CGPoint(x: self.viewOptions.frame.origin.x , y: self.viewOptions.frame.origin.y + 10)
            self.viewOptions.frame = CGRect(x: labelPosition.x, y: labelPosition.y, width: self.viewOptions.frame.size.width, height: self.viewOptions.frame.size.height)
        }
        */
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.heightTblFilterInbox.constant = self.tblFilterInbox.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objAppShareData.arrTBottomSheetModal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BottomSheetCell", for: indexPath) as! BottomSheetCell
        let obj = objAppShareData.arrTBottomSheetModal[indexPath.row]
        cell.lblOption.text = obj
        cell.btnOpenOptionPop.tag = indexPath.row
        cell.btnOpenOptionPop.addTarget(self, action: #selector(btnOpenOption(sender:)), for: UIControl.Event.touchUpInside)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }

    @objc private func btnOpenOption(sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        if let handler = self.callback{
            handler(String(sender.tag))
        }
    }
    
    @IBAction func btnCloseInboxFilter(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
