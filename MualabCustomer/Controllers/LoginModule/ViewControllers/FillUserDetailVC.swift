//
//  FillUserDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 23/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import CropViewController

class FillUserDetailVC: UIViewController {
    
    var imagePicker = UIImagePickerController()
    ////
    @IBOutlet weak var viewScreenShot : UIView!
    @IBOutlet weak var lblScreenShot : UILabel!
    ////
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var imgCam : UIImageView!
    @IBOutlet weak var profileBackView : UIView!
    @IBOutlet weak var imageChekBox: UIImageView!
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var btnMale : UIButton!
    @IBOutlet weak var btnFemale : UIButton!
    @IBOutlet weak var DOBPicker : UIDatePicker!
    var boolForTerms:Bool = false
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtDOB : UITextField!
    @IBOutlet weak var txtUserName : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    var dictSocialData : [String : AnyObject]!
    var isFromSocial = false
    fileprivate var userImage:UIImage?
    fileprivate var strDOBForServer: String?

    @IBOutlet weak var bottomConstraint : NSLayoutConstraint!

    var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
}

//MARK: - View Hirarchy
extension FillUserDetailVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGasturesToViews()
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
            self.btnMale.isSelected = true
            self.btnFemale.isSelected = false
        //DOBPicker.date = Date().addingTimeInterval(-365*12*24*60*60)
        DOBPicker.maximumDate = Date()//.addingTimeInterval(-365*12*24*60*60)
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        if (UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) == nil){
            let ad = Address.init(locality:"",address2:"", city:"", state: "", postalCode: "", country:"", placeName:"", fullAddress:"", latitude:"", longitude:"")
            let userInfo = User.init(address:ad)
            let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
            UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
            UserDefaults.standard.synchronize()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
            self.setAddress()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.setAddress()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension FillUserDetailVC{
    func configureView(){
        
         self.txtFirstName.delegate = self
         self.txtLastName.delegate = self
         self.txtUserName.delegate = self
         self.txtAddress.delegate = self

        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width/2
//        self.imgProfile.layer.borderWidth = 4.0
//        self.imgProfile.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
        self.profileBackView.layer.cornerRadius = self.profileBackView.frame.size.width/2
        
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        
        if self.isFromSocial{
           self.isFromSocial = false
            
           let strType = self.dictSocialData["socialType"] as? String
            if strType == "insta"{
                if let url = URL(string:(self.dictSocialData["profile_picture"] as? String)!){
//                    self.imgProfile.af_setImage(withURL: url, placeholderImage:UIImage.init(named: "cellBackground"), progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { response in
//                        DispatchQueue.main.async {
//                            if let image = response.result.value {
//                                self.imgProfile.image = image
//                                self.userImage = image
//                            }else{
//                                self.imgProfile.image = UIImage.init(named: "cellBackground")
//                            }
//                        }
//                    }

                    self.imgProfile.sd_setImage(with: url) { (image, error, cache, urls) in
                        if (error != nil) {
                            self.imgProfile.image = nil
                        } else {
                            // Successful in loading image
                            self.imgProfile.image = image
                            self.userImage = image
                        }
                    }
                }
                if let strFullName = self.dictSocialData["full_name"] as? String{
                    let arr = strFullName.components(separatedBy: " ")
                    if arr.count >= 2{
                       self.txtFirstName.text = arr[0]
                       self.txtLastName.text = arr[1]
                    }else{
                       self.txtFirstName.text = strFullName
                       self.txtLastName.text = ""
                    }
                }
                if let strUserName = self.dictSocialData["username"] as? String{
                    self.txtUserName.text = strUserName
                }
            }else{
                
           let strId = self.dictSocialData["id"] as? String
           let strPic = "https://graph.facebook.com/\(strId!)/picture?width=600&height=600"
           if let url = URL(string:strPic){
              //self.imgProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
//              self.imgProfile.af_setImage(withURL: url, placeholderImage:UIImage.init(named: "cellBackground"), progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { response in
//                   DispatchQueue.main.async {
//                    if let image = response.result.value {
//                        self.imgProfile.image = image
//                        self.userImage = image
//                    }else{
//                        self.imgProfile.image = UIImage.init(named: "cellBackground")
//                    }
//                }
//               }
            self.imgProfile.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    self.imgProfile.image = nil
                } else {
                    // Successful in loading image
                    self.imgProfile.image = image
                    self.userImage = image
                }
            }
           }
           if let strFirstName = self.dictSocialData["first_name"] as? String{
               self.txtFirstName.text = strFirstName
           }
            if let strLastName = self.dictSocialData["last_name"] as? String{
                self.txtLastName.text = strLastName
            }
          }
        }
        
        /////
        if self.imgProfile.image == UIImage.init(named: "cellBackground"){
           //cell.lblImg.text = self.txtUserName.text.getAcronyms(separator: "").uppercased();
        }
        /////
    }
    
    func saveUserData(){
        
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        if (self.dictSocialData != nil){
            userInfo.email = ""
            userInfo.strSocialId = self.dictSocialData["id"] as! String
            userInfo.strSocialType = self.dictSocialData["socialType"] as! String
        }
        userInfo.firstName = self.txtFirstName.text!
        userInfo.lastName = self.txtLastName.text!
        //userInfo.DOB = self.txtDOB.text!
        userInfo.DOB = strDOBForServer!
        userInfo.userName = self.txtUserName.text!.lowercased()
        userInfo.addresss = self.txtAddress.text!
        //Rohit
        if self.btnMale.isSelected{
            //userInfo.gender = "male"
            userInfo.gender = "1"
        }else{
           // userInfo.gender = "female"
            userInfo.gender = "2"
        }
                
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
        UserDefaults.standard.synchronize()
        
        self.callWebserviceForCheckUserName()
    }
    
    func GotoSubmitPasswordVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"SubmitPasswordVC") as! SubmitPasswordVC
        if let img = self.userImage{
            objVC.userImage = img
        }else{
            //objVC.userImage = self.imgProfile.image
        }
       
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func addGasturesToViews() {
        
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTapNew(gestureRecognizer:)))
        imgProfile.addGestureRecognizer(loginTap)

//        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTapNew(gestureRecognizer:)))
//        profileBackView.addGestureRecognizer(loginTap)
//
//        let imgCamTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTap(gestureRecognizer:)))
//        imgCam.addGestureRecognizer(imgCamTap)

    }
    
    func selectProfileImage() {
        let selectImage = UIAlertController(title: "Select Profile Image", message: nil, preferredStyle: .actionSheet)
        imagePicker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.selectImageFromCamera()
//            if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                self.imagePicker.sourceType = .camera
//                self.imagePicker.showsCameraControls = true
//                self.imagePicker.allowsEditing = true;
//                self.present(self.imagePicker, animated: true)
//            }
        })
        let btn2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoLibraryPermission()
//            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
//                self.imagePicker.sourceType = .photoLibrary
//                self.imagePicker.allowsEditing = true;
//                self.present(self.imagePicker, animated: true)
//            }
        })
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true)
    }
}

//MARK: - UITapGestureActions
fileprivate extension FillUserDetailVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func handleImgCamTap(gestureRecognizer: UIGestureRecognizer) {
        selectProfileImage()
    }
    @objc func handleImgCamTapNew(gestureRecognizer: UIGestureRecognizer) {
        selectProfileImage()
    }
}
//MARK: - IBActions

fileprivate extension FillUserDetailVC{
    
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        if boolForTerms == false {
            boolForTerms = true
            self.imageChekBox.image = #imageLiteral(resourceName: "check_box")
        } else {
            self.imageChekBox.image = #imageLiteral(resourceName: "uncheck_box")
            boolForTerms = false
        }
    }
    @IBAction func btnTermsAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        viewController.strComeFrom = "Terms & Conditions"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnImageAction(_ sender:Any){
       self.selectProfileImage()
    }
   
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    func GotoAddAdressVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
        objVC.isOutcallOption = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func setAddress(){
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        let addressComponent = userInfo.address
        
        if addressComponent.placeName.count > 0 && !addressComponent.placeName.contains("°"){
            //self.txtAddress.text = addressComponent.placeName
            self.txtAddress.text = addressComponent.fullAddress
            self.txtAddress.placeholder = "Business Address"
        }else if addressComponent.fullAddress.count>0{
            self.txtAddress.text = addressComponent.fullAddress
            self.txtAddress.placeholder = "Business Address"
            // self.resizeTextView(textView: self.txtAddress)
        }
    }
    
    @IBAction func btnAddBusinessAddress(){
        self.view.endEditing(true)
        self.locationAuthorization()
    }
    func makeDefaultUserImage(){
        //self.userImage
        self.lblScreenShot.text = self.txtUserName.text!.getAcronyms(separator: "").uppercased();
        self.viewScreenShot.clipsToBounds = true
        self.userImage = self.viewScreenShot.takeScreenshot()
        print("test")
    }
    
    @IBAction func btnContinue(_ sender:Any){
        
        var invalid = false
        var strMessage = ""
        
        txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtLastName.text = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtDOB.text = txtDOB.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtUserName.text = txtUserName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        txtAddress.text = txtAddress.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
//       if userImage == nil{
//           invalid = true
//           strMessage = "Please select or take profile image "
//       }else
        
        if self.txtFirstName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtFirstName)
        }else if self.txtLastName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtLastName)
        }else if self.txtUserName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtUserName)
        }else if self.txtDOB.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtDOB)
        }else if self.txtAddress.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtAddress)
        }else if !self.btnMale.isSelected && !self.btnFemale.isSelected {
            invalid = true
            strMessage = message.validation.genderSelection
        }else if (self.txtUserName.text?.count)! < 4{
            invalid = true
            strMessage = message.validation.userNameLength
        }else if self.txtUserName.text?.range(of:" ") != nil{
            invalid = true
            strMessage = message.validation.userNameSpace
        }else if (boolForTerms == false) {
            invalid=true
            strMessage = "Please accept terms and conditions"
        }
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.bannerDark, on: self)
    
        }else if !invalid{
            self.view.endEditing(true)
            if self.imgProfile.image == UIImage.init(named: "user_icon"){
                self.makeDefaultUserImage()
            }else{
            }
            self.saveUserData()
        }
    }
    
    @IBAction func btnSelectDOB(_ sender:Any){
        self.bottomConstraint.constant = 0
        if self.txtDOB.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" )
            self.DOBPicker.date = dataDate ?? Date()
        }else{
           //self.DOBPicker.date = Date()
        }
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        if sender.tag == 1 {
            self.txtDOB.text = objAppShareData.dateFormatToShow(forAPI: self.DOBPicker.date)
            strDOBForServer = objAppShareData.dateFormat(forAPI: self.DOBPicker.date)
        }
        if self.txtDOB.text != ""{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" )
         self.DOBPicker.date = dataDate ?? Date()
        }else{
        }
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnMaleFemaleSelection(_ sender:UIButton){
        if sender.tag == 0{
            self.btnMale.isSelected = true
            self.btnFemale.isSelected = false
        }else{
            self.btnMale.isSelected = false
            self.btnFemale.isSelected = true
        }
    }
}

//MARK: - UIImagePickerDelegate
extension FillUserDetailVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate,RSKImageCropViewControllerDelegate,RSKImageCropViewControllerDataSource,CropViewControllerDelegate{
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.navigationController?.popViewController(animated: true)
        let imgPosted = croppedImage
    }
    
//    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
//    }
    
    //: RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource
    // Returns a custom rect for the mask.
    /*
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
    var maskSize: CGSize
    // calculate the width based on the screen size - 30px
    let screenRect: CGRect = UIScreen.main.bounds
    let width: CGFloat = screenRect.size.width - 20
    // 4:3, height will be 75% of the width
    let height: CGFloat = width * 0.75
    if controller.isPortraitInterfaceOrientation() {
    maskSize = CGSize(width: width, height: height)
    }
    else {
    maskSize = CGSize(width: 355, height: 266.25)
    }
    let viewWidth: CGFloat = controller.view.frame.width
    let viewHeight: CGFloat = controller.view.frame.height
    let maskRect = CGRect(x: (viewWidth - maskSize.width) * 0.5, y: (viewHeight - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
    return maskRect
    }
    // Returns a custom rect in which the image can be moved.
    
    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
    // If the image is not rotated, then the movement rect coincides with the mask rect.
    return controller.maskRect
    }
    */
    
    // Returns a custom path for the mask.
//    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
//    return UIBezierPath(rect: controller.maskRect)
//    }
    
    // MARK: - RSKImageCropViewControllerDelegate
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
    //isImageCanceled = true
    self.navigationController?.popViewController(animated: true)
    }
    
//    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
//        self.navigationController?.popViewController(animated: true)
//        let imgPosted = croppedImage
//        //self.processSelectedImage(imgPosted: imgPosted)
//    }
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController?) -> CGRect {
        let aspectRatio = CGSize(width: 16.0, height: 16.0)
        
        let viewWidth = controller?.view.frame.width
        let viewHeight = controller?.view.frame.height
        
        var maskWidth: CGFloat
        if controller?.isPortraitInterfaceOrientation() != nil {
            maskWidth = viewWidth!
        } else {
            maskWidth = viewHeight!
        }
        
        var maskHeight: CGFloat
        repeat {
            maskHeight = maskWidth * aspectRatio.height / aspectRatio.width
            maskWidth -= 1.0
        } while maskHeight != floor(maskHeight)
        maskWidth += 1.0
        
        let maskSize = CGSize(width: maskWidth, height: maskHeight)
        
        let maskRect = CGRect(x: (viewWidth! - maskSize.width) * 0.5, y: (viewHeight! - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
        
        return maskRect
    }
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
        let rect: CGRect? = controller.maskRect
        let point1 = CGPoint(x: (rect?.minX)!, y: (rect?.maxY)!)
        let point2 = CGPoint(x: (rect?.maxX)!, y: (rect?.maxY)!)
        let point3 = CGPoint(x: (rect?.maxX)!, y: (rect?.minY)!)
        let point4 = CGPoint(x: (rect?.minX)!, y: (rect?.minY)!)
        
        let rectangle = UIBezierPath()
        rectangle.move(to: point1)
        rectangle.addLine(to: point2)
        rectangle.addLine(to: point3)
        rectangle.addLine(to: point4)
        rectangle.close()
       
        return rectangle
    }
    func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController?) -> CGRect {
        if controller?.rotationAngle == 0 {
            return controller?.maskRect ?? CGRect.zero
        } else {
            let maskRect: CGRect? = controller?.maskRect
            let rotationAngle = controller?.rotationAngle
            
            var movementRect = CGRect.zero
            
            movementRect.size.width = CGFloat((maskRect?.width)! * fabs(cos(rotationAngle!)) + (maskRect?.height)! * fabs(sin(rotationAngle!)))
            movementRect.size.height = CGFloat((maskRect?.height)! * fabs(cos(rotationAngle!)) + (maskRect?.width)! * fabs(sin(rotationAngle!)))
            
            movementRect.origin.x = (maskRect?.minX)! + ((maskRect?.width)! - movementRect.width) * 0.5
            movementRect.origin.y = (maskRect?.minY)!
            movementRect.origin.x = floor(movementRect.minX)
            movementRect.origin.y = floor(movementRect.minY)
            movementRect = movementRect.integral
            
            return movementRect
        }
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
        
            //let image: UIImage = pickedImage //Load an image
            let cropViewController = CropViewController.init(croppingStyle: .circular, image: pickedImage)
            cropViewController.delegate = self
            cropViewController.modalPresentationStyle = .fullScreen
            self.imagePicker.present(cropViewController, animated: true, completion: nil)
            //present(cropViewController, animated: true, completion: nil)
            //cropeImage(image: pickedImage)
        return
            
            let imageCropVC = RSKImageCropViewController(image: pickedImage, cropMode: .custom)
            imageCropVC.delegate = self
            imageCropVC.dataSource = self
            /*
             * This next section is needed because there is a top constraint that blocks the
             * cancel and choose buttons in the cropper from receiving touches properly.  We need
             * to remove the 'UIView-Encapsulated-Layout-Top" constraint.
             */

            
            imageCropVC.view.setNeedsUpdateConstraints()
            imageCropVC.view.layoutSubviews()
            
            var cons = imageCropVC.view.constraints
            var newConstraints = [AnyHashable]()
            for object: NSLayoutConstraint in cons {
                if object.identifier == nil || ((object.identifier as NSString?)?.range(of: "Layout-Height"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Width"))?.location != NSNotFound || ((object.identifier as NSString?)?.range(of: "Layout-Left"))?.location != NSNotFound {
                    newConstraints.append(object)
                }
            }
            imageCropVC.view.removeConstraints(imageCropVC.view.constraints)
            if let aConstraints = newConstraints as? [NSLayoutConstraint] {
                imageCropVC.view.addConstraints(aConstraints)
            }
            
            picker.dismiss(animated: false) {() -> Void in
            }
            //        self.barChart?.doubleTapToZoomEnabled = false
            //        self.barChart?.pinchZoomEnabled = false
            //        barChart.dragEnabled = false
            
            // push the cropper VC
            navigationController?.pushViewController(imageCropVC, animated: false)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //  isFromImagePicker = true
        dismiss(animated: true)
    }
}

//MARK: - UITextField Delegate
extension FillUserDetailVC:UITextFieldDelegate{
    
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{

        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if textField == self.txtFirstName || textField == self.txtLastName{
            if let range = string.rangeOfCharacter(from: NSCharacterSet.letters){
                return true
            } else if let range = string.rangeOfCharacter(from: NSCharacterSet.whitespaces){
                return true
            } else if string == "" {
                return true
            }else if string == "'" {
                return true
            }else if string == "."{
                return false
            }else{
                return false
            }
        }
        if textField == self.txtUserName && (string.rangeOfCharacter(from: NSCharacterSet.letters) != nil) && (self.txtUserName.text?.count == 20){
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == self.txtLastName{
            txtUserName.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: - Webservices
fileprivate extension FillUserDetailVC{
    
    func callWebserviceForCheckUserName(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let strUserName = self.txtUserName.text!.lowercased()
        let dicParam = ["user_name":strUserName]
        
        objServiceManager.requestPost(strURL: WebURL.checkUser, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                self.GotoSubmitPasswordVC()
            }else{
               
                objAppShareData.showAlert(withMessage: "That username is already being used", type: alertType.bannerDark, on: self)
                
            }
        }) { error in
          objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

extension FillUserDetailVC{
    func nameOfMonths() -> [Any] {
        return ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    }
}


extension FillUserDetailVC{
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                self.imagePicker.sourceType = .photoLibrary
              //  self.imagePicker.allowsEditing = true;
                //self.present(self.imagePicker, animated: true)
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true, completion: {
                    self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
                })
            }
            
            
            //self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized: break
                // as above
                case .denied, .restricted: break
                // as above
                case .notDetermined: break
                    // won't happen but still
                @unknown default: break
                    //
                }
            }
        }
    }
    
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
                //self.imagePicker.allowsEditing = false;
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true)
            }
            
        }
        
    }
}

extension FillUserDetailVC:ImageCropperDelegate{
//    func presentCropViewController (){
//        let image: UIImage = nil //Load an image
//        let cropViewController = CropViewController(image: image)
//        cropViewController.delegate = self
//        present(cropViewController, animated: true, completion: nil)
//    }
//
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        self.imgProfile.image = image
        self.userImage = image
        self.dismiss(animated: true, completion: nil)
    }
    
    func cropeImage(image: UIImage){
        let croper = ImageCropperViewController(image:image)
        croper.delegate = self
        croper.modalPresentationStyle = .fullScreen
        self.imagePicker.present(croper, animated: true, completion: nil)
    }
    
    func imageCropperDidFinishCroppingImage(cropprdImage: UIImage){
        self.imgProfile.image = cropprdImage
        self.userImage = cropprdImage
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropperDidCancel(){
        dismiss(animated: true, completion: nil)
    }
}


extension FillUserDetailVC {
    func locationAuthorization(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.GotoAddAdressVC()

        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.GotoAddAdressVC()

        }
        else if (CLLocationManager.authorizationStatus() == .denied) {
            let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        } else {
            //locManager.requestWhenInUseAuthorization()
            self.GotoAddAdressVC()

        }
    }
}
