//
//  ForgotPassVC.swift
//  MualabCustomer
//
//  Created by Mac on 13/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreTelephony

fileprivate class countryCod: NSObject {
    
    var countryName : String = ""
    var dialCode : String = ""
    var countryCode : String = ""
    
    init(countryName:String?, dialCode:String? , countryCode :String?) {
        self.countryName = countryName ?? ""
        self.dialCode = dialCode ?? ""
        self.countryCode = countryCode ?? ""
    }
}
class ForgotPassVC: UIViewController {
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var imgLogo : UIImageView!
    
    fileprivate var arrayCountryCode : [countryCod] = []
    fileprivate var arrToShow : [countryCod] = []
    
    fileprivate var countryCode: String = ""
    fileprivate var strLocalCCode: String = ""
    fileprivate var otp:String = ""
    @IBOutlet weak var lblPhone:UILabel!
    @IBOutlet weak var lblUsername:UILabel!
    @IBOutlet weak var viewUsername:UIView!
    @IBOutlet weak var viewPhone:UIView!

    @IBOutlet weak var lblCountryCode:UILabel!
    @IBOutlet weak var lblNoResults:UILabel!
    @IBOutlet weak var lblHintText:UILabel!

    @IBOutlet weak var txtUsername : UITextField!
    @IBOutlet weak var txtPhoneNumber : UITextField!
    
    
    @IBOutlet weak var countryCodeView : UIView!
    
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var tblCountryCode : UITableView!
    
    @IBOutlet weak var bottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var lblCountryTopConstraint : NSLayoutConstraint?
    
}
//MARK: - View Hirarchy
extension ForgotPassVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtUsername.delegate = self
        self.txtPhoneNumber.delegate = self
        
        getCountryCode()
        addGasturesToViews()
        observeKeyboard()
        customSearchBar()
        addAccessoryView()
        
//        if (UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) == nil){
//             let userInfo = User.init(userType:"customer")
//            let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
//            UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
//            UserDefaults.standard.synchronize()
//        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension ForgotPassVC{
    func phoneOption(){
        
        self.lblHintText.text = "Enter your phone number and we'll Send you a password reset link to get back into your account."
        self.viewPhone.isHidden = false
        self.viewUsername.isHidden = true
        self.lblPhone.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.lblUsername.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
    }
    
    func userNameOption(){
        self.lblHintText.text = "Enter your username or email and we'll Send you a link to get back into your account."

        self.viewPhone.isHidden = true
        self.viewUsername.isHidden = false
        self.lblPhone.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
        self.lblUsername.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func configureView(){
        self.userNameOption()
        self.countryCodeView.alpha = 0.0;
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.size.height/2
    }
    //Custom UISearchBar
    func customSearchBar() {
        searchBar.backgroundImage = UIImage()
        searchBar.searchBarStyle = .minimal
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.font = UIFont.init(name: "Nunito-Light", size: 17.0)
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        let clearButton = textFieldInsideSearchBar?.value(forKey: "_clearButton") as? UIButton
        let templateImage = UIImage.init(named:"clearButton")
        clearButton?.setImage(templateImage,for:.normal)
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor.white
    }
    //Get CountryCoads
    func getCountryCode() {
        countryCode = "+1"
        let carrier = CTTelephonyNetworkInfo().subscriberCellularProvider
        
        if (carrier?.isoCountryCode?.uppercased()) != nil{
            strLocalCCode = (carrier?.isoCountryCode?.uppercased())!
        }
        
        do {
            if let file = Bundle.main.url(forResource: "countrycode", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let dictData = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let object = dictData as? [String : Any] {
                    // json is a dictionary
                    let arr = object["country"] as! [AnyObject]
                    for dicCountry in arr{
                        let objCountry = countryCod.init(countryName: dicCountry["name"] as? String, dialCode: dicCountry["dial_code"] as? String , countryCode: dicCountry["code"] as? String)
                        self.arrayCountryCode.append(objCountry)
                        if (dicCountry["code"] as? String ?? "" == strLocalCCode) {
                            countryCode = (dicCountry["dial_code"] as? String)!
                            self.lblCountryCode.text = dicCountry["dial_code"] as? String
                        }
                    }
                    
                    self.arrToShow = self.arrayCountryCode.sorted(by: { $0.countryName < $1.countryName })
                    self.tblCountryCode.reloadData()
                } else if let object = dictData as? [Any] {
                    // json is an array
                } else {
                    //print("JSON is invalid")
                }
            } else {
                //print("no file")
            }
        } catch {
            //print(error.localizedDescription)
        }
    }
    
    func addGasturesToViews() {
        
        let countryCodeTap = UITapGestureRecognizer(target: self, action: #selector(handleCountryCodeTap(gestureRecognizer:)))
        lblCountryCode.addGestureRecognizer(countryCodeTap)
    }
    
    func GotoVerifyPhoneVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"VerifyPhoneNumberVC") as! VerifyPhoneNumberVC
        objVC.strOTP = self.otp
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - keyboard methods
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo
        let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
        let keyboardFrame: CGRect? = kbFrame?.cgRectValue
        let height: CGFloat? = keyboardFrame?.size.height
        bottomConstraint.constant = height!
        UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
        bottomConstraint.constant = 8
        UIView.animate(withDuration: animationDuration , animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    //
    func addAccessoryView() -> Void {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.7098039216, alpha: 1)
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView
    }
    //
    @objc func doneButtonTapped() {
        // do you stuff with done here
        self.txtPhoneNumber.resignFirstResponder()
    }
    
    
}
//MARK:- IBActions
fileprivate extension ForgotPassVC{
    @IBAction func btnPhoneOption(_ sender:Any){
        self.phoneOption()
    }
    @IBAction func btnUsernameOption(_ sender:Any){
        self.userNameOption()
    }
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignin(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinue(_ sender:Any){
        var invalid = false
        var strMessage = ""
        txtUsername.text = txtUsername.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtPhoneNumber.text = txtPhoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.viewUsername.isHidden == false{
            if self.txtUsername.text?.count == 0{
                objAppShareData.shakeTextField(self.txtUsername)
            }else{
                self.callWebserviceForForgotPassword()
            }
        }else{
            if self.txtPhoneNumber.text?.count == 0{
                objAppShareData.shakeTextField(self.txtPhoneNumber)
            }else{
                //self.callWebserviceForForgotPassword()
            }
        }
 
    }
    
    @IBAction func btnCloseCountryView(_ sender:Any){
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 0.0
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5) {
                self.countryCodeView.isHidden = true
            }
            self.view.endEditing(true)
        }
    }
}
//MARK: - UITapGesttureActions
fileprivate extension ForgotPassVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleCountryCodeTap(gestureRecognizer: UIGestureRecognizer) {
        self.countryCodeView.isHidden = false
        // searchBar.becomeFirstResponder()
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 1.0
        }
    }
}
//MARK: - UISearchBar Delegate
extension ForgotPassVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        searchAutocompleteEntries(withSubstring: searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        btnCloseCountryView(0)
    }
    func searchAutocompleteEntries(withSubstring substring: String) {
        var searchString = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let new = searchString.lowercased(with: Locale(identifier: "en"))
        if searchString.count > 0 {
            arrToShow = arrayCountryCode.filter({ $0.countryName.contains(searchString.uppercased()) || $0.countryName.contains(searchString.lowercased()) || $0.countryName.contains(searchString) || $0.dialCode.contains(searchString) || $0.countryCode.contains(searchString.uppercased()) || $0.countryName.contains(new) || $0.countryName.contains(new.capitalized);
            })
        }else {
            arrToShow = arrayCountryCode
        }
        //self.arrToShow = self.arrToShow.sorted(by: { $0.countryName < $1.countryName })
        self.tblCountryCode.reloadData()
        if arrToShow.count>0 {
            self.lblNoResults.isHidden = true
        }else{
            self.lblNoResults.isHidden = false
        }
    }
}

//MARK: - UITableView Delegate and Datasource
extension ForgotPassVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtUsername{
            self.txtPhoneNumber.becomeFirstResponder()
        }else if textField == self.txtPhoneNumber{
            self.txtPhoneNumber.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
//        if textField == self.txtPhoneNumber{
//            let r = range
//            if r.length == 0 && r.location == 0{
//                self.lblCountryTopConstraint?.constant = 0
//            }
//            if r.length == 1 && r.location == 0{
//                self.lblCountryTopConstraint?.constant = 0
//            }
//        }
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        return true
    }
}

//MARK: - UITableView Delegate and Datasource
extension ForgotPassVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CountryCodeCell") as! CountryCodeCell
        let objCountry = self.arrToShow[indexPath.row]
        cell.lblCountry?.text = objCountry.countryName
        cell.lblCode?.text = objCountry.dialCode
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let objCountry = self.arrToShow[indexPath.row]
        self.lblCountryCode.text = objCountry.dialCode
        self.countryCode = objCountry.dialCode
        btnCloseCountryView(0)
    }
}

//MARK: - Webservices
fileprivate extension ForgotPassVC{
    
    func callWebserviceForForgotPassword(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let strUserName = self.txtUsername.text!.lowercased()
        let dicParam = ["email":strUserName]
        
        objServiceManager.requestPost(strURL: WebURL.forgotPassword, params: dicParam, success: { response in
            objActivity.stopActivity()
            if response["status"] as? String ?? "" == "success"{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            self.navigationController?.popViewController(animated: true)
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForVerifyPhoneNumber(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let dicParam = ["countryCode":self.lblCountryCode.text!,
                        "contactNo":self.txtPhoneNumber.text!,
                        "socialId":"",
                        "email":""]
        
        objServiceManager.requestPost(strURL: WebURL.phonVerification, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                self.otp = String.init(describing: response["otp"]!)
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            //print(error)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
