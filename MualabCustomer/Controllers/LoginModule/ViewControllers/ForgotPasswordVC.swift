//
//  ForgotPasswordVC.swift
//  MualabCustomer
//
//  Created by Mac on 30/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController{
    
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var btnSendLink : UIButton!
}

//MARK: - View Hirarchy
extension ForgotPasswordVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEmail.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
        self.txtEmail.becomeFirstResponder()
    }
}

//MARK: - Local Methods
fileprivate extension ForgotPasswordVC{
    
    func configureView() {
        self.txtEmail.layer.cornerRadius = 4.0
       // self.txtEmail.layer.sublayerTransform=CATransform3DMakeTranslation(12, 0, 0);
        self.btnSendLink.layer.cornerRadius = self.btnSendLink.frame.size.height/2;
    }
}

//MARK: - IBActions
fileprivate extension ForgotPasswordVC{
    @IBAction func btnBack (_ sender:Any){
        self.dismiss(animated: true, completion: nil)
        self.view.endEditing(true)
    }
    
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.gotoLoginPage()
    }
    
    
    func backAction(){
        self.dismiss(animated: true, completion: nil)
        self.view.endEditing(true)
    }
    
    @IBAction func btnResetPassword(_ sender: UIButton) {
        self.view.endEditing(true)
        var invalid = false
        var strMessage = ""
        txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.txtEmail.text?.count == 0{
            objAppShareData.shakeTextField(self.txtEmail)
            invalid = true
        }else if !(objValidationManager.isValidateEmail(strEmail: self.txtEmail.text!)){
            strMessage = message.validation.email
            invalid = true
        }
        
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.bannerDark, on: self)
        }else if !invalid{
            self.view.endEditing(true)
            self.callWebServiceForgotPassword()
        }
    }
}

//MARK: - callWebServiceForgotPassword
extension ForgotPasswordVC{
    func callWebServiceForgotPassword(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dicParam = ["email":self.txtEmail.text!]
        
        objServiceManager.requestPost(strURL: webUrl.ForgotPassword, params: dicParam, success: { response in
            objActivity.stopActivity()
            if response["status"] as? String ?? "" == "success"{
             
                if let msg = response["message"] as? String{
                  objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // in half a second...
                    self.backAction()
                }
            }else{
         
                if let msg = response["message"] as? String{
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - UITextField Delegate
extension ForgotPasswordVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEmail{
            txtEmail.resignFirstResponder()
        }
        return true
    }
    
}

