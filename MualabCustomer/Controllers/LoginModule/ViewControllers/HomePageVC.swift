//
//  HomeVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 9/14/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class HomePageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackAction(_ sender:Any){
      appDelegate.logout()
    }

}
