//
//  ViewController.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import Fabric
import Crashlytics

class LoginVC: UIViewController {
    var iconClick : Bool!
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    var dictFaceBookData : [String : AnyObject]!
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var imgEye: UIImageView!
    @IBOutlet weak var btnLogin : UIButton!
    @IBOutlet weak var btnShowPassword : UIButton!
    @IBOutlet weak var btnFacebook : UIButton!
    @IBOutlet weak var btnInstagram : UIButton!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var socialLoginStack : UIStackView!
    @IBOutlet weak var lblSignUp : UILabel!
    @IBOutlet weak var viewWebView: UIView!
    @IBOutlet weak var loginWebView: UIWebView!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
}

//MARK: - View hirarchy
extension LoginVC : UIWebViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.loginWebView.isHidden = true
        //self.loginIndicator.isHidden = true
        self.viewWebView.isHidden = true
        loginWebView.delegate = self as! UIWebViewDelegate
        self.txtEmail.delegate = self
        addGesturesToView()
        iconClick = true
        self.btnFacebook.isUserInteractionEnabled = false
        self.btnInstagram.isUserInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let deadlineTime = DispatchTime.now() + .seconds(3)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            print("test")
            self.btnFacebook.isUserInteractionEnabled = true
            self.btnInstagram.isUserInteractionEnabled = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//        return
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
        objWebserviceManager.StopIndicator()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension LoginVC{
    
    func configureView(){
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height/2
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.size.height/2
        if self.txtPassword.text!.count > 0{
            self.btnShowPassword.isSelected = false
            self.txtPassword.isSecureTextEntry = true
        }
        objAppShareData.clearUserData()
    }
    
    func addGesturesToView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginStack.addGestureRecognizer(socialLoginTap)
        
        let signUpTap = UITapGestureRecognizer(target: self, action: #selector(handleSignUpTap(gestureRecognizer:)))
        lblSignUp.addGestureRecognizer(signUpTap)
    }
}

//MARK: - UITapGesttureActions
fileprivate extension LoginVC{
    
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        
        // let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        //  let objChooseType = sb.instantiateViewController(withIdentifier:"ChooseTypeVC") as! ChooseTypeVC
        // self.navigationController?.pushViewController(objChooseType, animated: true)
        
    }
    
    @objc func handleSignUpTap(gestureRecognizer: UIGestureRecognizer) {
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"EmailPhoneVC") as! EmailPhoneVC
        self.navigationController?.pushViewController(objChooseType, animated: true)
        
    }
}

//MARK: - IBActions
fileprivate extension LoginVC {
   
    @IBAction func btnSignUpAsServiceProvederAction(_ sender: Any) {
        self.view.endEditing(true)
        if let url = URL(string: "https://apps.apple.com/us/app/koobi-biz/id1360296200?ls=1"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnEyeAction(_ sender: Any) {
        if(iconClick == true) {
            self.txtPassword.isSecureTextEntry = false
            imgEye.image = UIImage.init(named: "open_eyes_ico")
            iconClick = false
        } else {
            self.txtPassword.isSecureTextEntry = true
            //
            imgEye.image = UIImage.init(named: "Close_eye_ico")
            iconClick = true
        }
    }
   
    @IBAction func btnLogin(_ sender:Any){
        //Crashlytics.sharedInstance().crash()

        self.view.endEditing(true)
        var invalid = false
        
        txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtPassword.text = txtPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if txtEmail.text?.count == 0{
            invalid = true
            objAppShareData.shakeTextField(txtEmail)
        }else if txtPassword.text?.count == 0{
            invalid = true
            objAppShareData.shakeTextField(txtPassword)
        }
        if invalid {
            
        }else {
            self.view.endEditing(true)
            self.callWebserviceForLogin()
        }
    }
    
    @IBAction func btnForgotPassword(_ sender:Any){
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ForgotPassVC") as! ForgotPassVC
        //objChooseType.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnShowPassword(_ sender:UIButton){
       // self.view.endEditing(true)

        if self.btnShowPassword.isSelected {
            self.btnShowPassword.isSelected = false
            self.txtPassword.isSecureTextEntry = true
        }else{
            self.btnShowPassword.isSelected = true
            self.txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func btnSocialLogin(_ sender:UIButton){
        self.view.endEditing(true)
        if sender.tag == 0{
           self.faceBookData()
        }else{
          
            self.loginWebView.isOpaque = false;
            //self.loginWebView.backgroundColor = UIColor.green
            //self.loginWebView.isHidden = false
            self.viewWebView.isHidden = false
            self.loginIndicator.isHidden = false
            self.loginWebView.delegate = self
            unSignedRequest()
 
            //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnCancelInstagramAction(_ sender:Any){
        self.viewWebView.isHidden = true
        if self.loginWebView.isLoading{
           self.loginWebView.stopLoading()
        }
    }
}

//Webservices
fileprivate extension LoginVC{
    
    func callWebserviceForLogin() {
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
    
        objActivity.startActivityIndicator()
        let strUserName = self.txtEmail.text!.lowercased()
        let dicParam = ["user_name":strUserName,
                        "password":self.txtPassword.text!,
                        //Rohit
            // "deviceType":"1",
            //"deviceToken":objAppShareData.firebaseToken,
            //"userType":"user",
            //"appType": "social",
            "firebase_token":objAppShareData.firebaseToken
        ]
        
    objServiceManager.requestPost(strURL: WebURL.login, params: dicParam, success: { response in
        print(response)
            if response["status"] as? String ?? "" == "success"{
                appDelegate.configureNotification()
                //UserInfo
                if let dictUser = response["users"] as? [String : Any]{
                    
                    let strToken = dictUser["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    //UserDefaults.standard.set(dictUser, forKey: UserDefaults.keys.userInfo)
                    
                    UserDefaults.standard.setValue(dictUser["notificationStatus"], forKey: UserDefaults.keys.notificationStatus)
                    UserDefaults.standard.setValue(dictUser["cardId"], forKey: UserDefaults.keys.stripeCardId)
                    UserDefaults.standard.set(dictUser["customerId"], forKey: UserDefaults.keys.stripeCustomerId)
                    ////
                    let data = NSKeyedArchiver.archivedData(withRootObject: dictUser)
                    UserDefaults.standard.set(data, forKey: UserDefaults.keys.userInfo)
                    ////
                    
                    UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                    
                    if let id = dictUser["_id"] as? String{
                        UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                    }else if let id = dictUser["_id"] as? Int{
                        UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                    }
                    UserDefaults.standard.set(dictUser["userName"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                    UserDefaults.standard.set(dictUser["profileImage"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                    
                    UserDefaults.standard.synchronize()
                    
                    strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                    
                    
                    self.writeUserDataToFireBase(dict: dictUser)
                    objAppShareData.selectedTab = 0
                    appDelegate.gotoTabBar(withAnitmation: true)
                     
                    
                }else{
                    objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                }
          }else{
                objActivity.stopActivity()

                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
            
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
   }
}

//MARK: - UITextField Delegate
extension LoginVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtEmail{
            self.txtPassword.becomeFirstResponder()
        }else if textField == self.txtPassword{
            self.txtPassword.resignFirstResponder()
        }
        return true
    }
 
}

//MARK: - FireBaseRegistration Method
extension LoginVC {
    
    func writeUserDataToFireBase(dict:[String : Any]){
        let calendarDate = ServerValue.timestamp()

        var id = 0
        if let ids = dict["_id"] as? Int{
            id = ids
        }else if let ids = dict["_id"] as? String{
            id = Int(ids)!
        }
        
        var img = dict["profileImage"] as? String ?? ""
        if (dict["profileImage"] as? String ?? "") != ""{
        }else{
            img = "http://koobi.co.uk:3000/uploads/default_user.png"
        }
        
        var strToken = ""
        if let str = dict["notificationStatus"] as? String{
            if str == "0"{
              strToken = ""
            }else{
                strToken = dict["firebaseToken"] as! String
            }
        }
        if let str = dict["notificationStatus"] as? Int{
            if str == 0{
               strToken = ""
            }else{
                strToken = dict["firebaseToken"] as! String
            }
        }
        let dict1 = ["firebaseToken":strToken,
                     "isOnline":checkForNULL(obj:1),
                     "lastActivity":checkForNULL(obj:calendarDate),
                     "profilePic":checkForNULL(obj:img),
                     "uId":checkForNULL(obj:id),
                     "userName":checkForNULL(obj:dict["userName"]) ]
        
        let ref = Database.database().reference()
        let myId:String = String(dict["_id"] as? Int ?? 0000000)
        ref.child("users").child(myId).setValue(dict1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            objWebserviceManager.StopIndicator()
        }
     }

    //MARK: - unSignedRequest
    func unSignedRequest () {
        //let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: ["https://api.instagram.com/oauth/authorize/","235f0bad46fb41a5af0c1309b99481e6","http://koobi.co.uk:8042", "likes+comments+relationships" ])
        //let authURL = "https://instagram.com/oauth/authorize/?client_id=235f0bad46fb41a5af0c1309b99481e6&redirect_uri=http://koobi.co.uk:8042&response_type=token&scope=basic+likes"
        let authURL = "https://instagram.com/oauth/authorize/?client_id=235f0bad46fb41a5af0c1309b99481e6&redirect_uri=http://koobi.co.uk:8042&response_type=token&scope=basic"
        //let authURL = "https://www.instagram.com/accounts/login/?client_id=235f0bad46fb41a5af0c1309b99481e6&redirect_uri=http://koobi.co.uk:8042&response_type=token&scope=likes+comments+relationships&DEBUG=True"
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.loadRequest(urlRequest)
    }
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix("http://koobi.co.uk:8042") {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        let userInfoURL = "https://api.instagram.com/v1/users/self/?access_token=" + authToken
        print("Instagram user info ==", userInfoURL)
        print("Instagram authentication token ==", authToken)
        let url = NSURL(string: userInfoURL)!
        //let paramString  = "client_id=\(INSTAGRAM_CLIENT_ID)&client_secret=\(INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\("http://koobi.co.uk:8042")&code=\(authToken)&scope=basic+public_content"
        let paramString  = "client_id=\(INSTAGRAM_CLIENT_ID)&client_secret=\(INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\("http://koobi.co.uk:8042")&code=\(authToken)&scope=basic"
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "GET"
        //request.httpBody = paramString.data(using: String.Encoding.utf8)!
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                        self.dictFaceBookData = jsonDataDict["data"] as! [String : AnyObject]
                        self.dictFaceBookData["socialType"] = "insta" as AnyObject
                        self.callWebserviceForCheckSocialLogin(strId: (self.dictFaceBookData["id"] as? String)!, strType: "insta")
                        
                        /*
                         Received data:
                         ["meta": {
                         code = 200;
                         }, "data": {
                         bio = "\"The people who are crazy enough to think that they can change the world, are the ones who do.\"\n ~ Steve Jobs";
                         counts =     {
                         "followed_by" = 3;
                         follows = 8;
                         media = 0;
                         };
                         "full_name" = "koobi biz";
                         id = 8641387277;
                         "is_business" = 0;
                         "profile_picture" = "https://instagram.fpku2-1.fna.fbcdn.net/vp/dc651f3c631c68d6e0789e2d7c8af2e5/5C8EBFF1/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg";
                         username = "koobi.co.uk";
                         website = "http://www.koobi.co.uk/";
                         }])
                        */
                        
                        let cookieStore = HTTPCookieStorage.shared
                        guard let cookies = cookieStore.cookies, cookies.count > 0 else { return }
                        
                        for cookie in cookies {
                            if cookie.domain == "www.instagram.com" || cookie.domain == "api.instagram.com" || cookie.domain == ".instagram.com"{
                                cookieStore.deleteCookie(cookie)
                            }
                        }
                    
                        if self.loginWebView.isLoading{
                           self.loginWebView.stopLoading()
                        }
                        self.viewWebView.isHidden = true
                        //self.loginWebView.isHidden = true
                        self.loginIndicator.isHidden = true
                    }
                }
            } catch let err as NSError {
                self.viewWebView.isHidden = true
                //self.loginWebView.isHidden = true
                self.loginIndicator.isHidden = true
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
    // MARK:- FaceBook Delegate
    func faceBookData (){
        objActivity.startActivityIndicator()
        let fbLoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (result?.isCancelled)!{
               objActivity.stopActivity()
            }
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")){
                        let strAccessToken = AccessToken.current!.tokenString
                        print("%@",strAccessToken as Any)
                        self.getFBUserData(token: strAccessToken)
                        fbLoginManager.logOut()
                    }
                }else{
                    objActivity.stopActivity()
                }
            }else{
                //objActivity.stopActivity()
            }
        }
    }
    
    func getFBUserData(token : String){
        //objWebServiceManager.StartIndicator()
        if((AccessToken.current) != nil){
            
            let token = AccessToken.current
            print("accessToken: " + String(describing: token))
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name,email, gender , first_name, last_name, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dictFaceBookData = result as! [String : AnyObject]
                    self.dictFaceBookData["socialType"] = "facebook" as AnyObject
                    print(result!)
                    self.callWebserviceForCheckSocialLogin(strId: (self.dictFaceBookData["id"] as? String)!, strType: "facebook")
                
                }else{
                    objActivity.stopActivity()
                }
            })
        }else{
            //objActivity.stopActivity()
        }
    }
    
    func callWebserviceForCheckSocialLogin(strId:String,strType:String) {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        print(objAppShareData.firebaseToken)
        let dicParam = ["socialId":strId,
                        "socialType":strType,
                    "deviceToken":objAppShareData.firebaseToken,
                    "firebaseToken":objAppShareData.firebaseToken
        ]
        
        objServiceManager.requestPost(strURL: WebURL.checkSocialLogin, params: dicParam, success: { response in
            objActivity.stopActivity()
            if response["status"] as? String ?? "" == "success"{
                if let dictUser = response["data"] as? [String : Any]{
                    
                    let strToken = dictUser["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    UserDefaults.standard.setValue(dictUser["notificationStatus"], forKey: UserDefaults.keys.notificationStatus)
                    UserDefaults.standard.set(dictUser["cardId"], forKey: UserDefaults.keys.stripeCardId)
                    UserDefaults.standard.set(dictUser["customerId"], forKey: UserDefaults.keys.stripeCustomerId)
                    ////
                    let data = NSKeyedArchiver.archivedData(withRootObject: dictUser)
                    UserDefaults.standard.set(data, forKey: UserDefaults.keys.userInfo)
                    ////
                    
                    UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                    if let id = dictUser["_id"] as? String{
                        UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                    }else if let id = dictUser["_id"] as? Int{
                        UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                    }
                    UserDefaults.standard.set(dictUser["userName"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                    UserDefaults.standard.set(dictUser["profileImage"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                    
                    UserDefaults.standard.synchronize()
                    
                    strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                   
                    self.writeUserDataToFireBase(dict: dictUser)
                    objAppShareData.selectedTab = 0
                    appDelegate.gotoTabBar(withAnitmation: true)
                    
                }else{
                    objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                }
            }else{
               if (response["status"] as? String ?? "" == "fail") && (response["message"] as? String ?? "" == "Not exist."){
                   self.GotoFillUserDetailVC()
                }
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func GotoFillUserDetailVC(){
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"FillUserDetailVC") as! FillUserDetailVC
        objVC.dictSocialData = self.dictFaceBookData
        objVC.isFromSocial = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}
