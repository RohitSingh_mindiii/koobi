//
//  ManageAccountVC.swift
//  MualabBusiness
//
//  Created by Mac on 18/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class ManageAccountVC: UIViewController {
    @IBOutlet weak var lblDescription : UILabel!
    var dictOfUserData = [String:Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        let username = dictOfUserData["userName"] as? String ?? ""
       self.changeString(userName: " @"+username)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackAction(_ sender:Any){
    self.navigationController?.popViewController(animated: true)
     }
    
    @IBAction func btnManageAction(_ sender:Any){
        
        if let dictUser = dictOfUserData as? [String : Any]{
            
            let strToken = dictUser["authToken"] as? String ?? ""
            UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
            UserDefaults.standard.set(dictUser, forKey: UserDefaults.keys.userInfo)
            UserDefaults.standard.set(true, forKey: UserDefaults.keys.isLoggedIn)
            
            if let id = dictUser["_id"] as? String{
                UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
            }else if let id = dictUser["_id"] as? Int{
                UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
            }
            UserDefaults.standard.set(dictUser["userName"] as? String ?? "", forKey:  UserDefaults.keys.myName)
            UserDefaults.standard.set(dictUser["profileImage"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
            
            UserDefaults.standard.synchronize()
            
            strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
            self.writeUserDataToFireBase(dict: dictUser)
        }
        appDelegate.gotoTabBar(withAnitmation: true)
    }
    
    @IBAction func btnCrearNewAction(_ sender:Any){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is  EmailPhoneVC{
                 _ = self.navigationController?.popToViewController(vc as! EmailPhoneVC, animated: true)
            }
        }
     }
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "You already have an existing biz account using this email and phone number! would you like to merge your existing biz profile"
        let b = userName
        let c = "with your social account or create a new account for your social profile?"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }

}

//MARK: - Webservices
fileprivate extension ManageAccountVC{
    
    func writeUserDataToFireBase(dict:[String : Any]){
        var id = 0
        if let ids = dict["_id"] as? Int{
            id = ids
        }else if let ids = dict["_id"] as? String{
            id = Int(ids)!
        }
        
        var img = dict["profileImage"] as? String ?? ""
        if (dict["profileImage"] as? String ?? "") != "" {
            
        }else{
            img = "http://koobi.co.uk:3000/uploads/default_user.png"
        }
        
        let calendarDate = ServerValue.timestamp()
        let dict1 = ["firebaseToken":checkForNULL(obj: dict["firebaseToken"]),
                     "isOnline":checkForNULL(obj:1),
                     "lastActivity":checkForNULL(obj:calendarDate),
                     "profilePic":checkForNULL(obj:img),
                     "uId":checkForNULL(obj:id),
                     "userName":checkForNULL(obj:dict["userName"]) ]
        
        let ref = Database.database().reference()
        let myId:String = String(dict["_id"] as? Int ?? 0000000)
        ref.child("users").child(myId).setValue(dict1)
        
        // self.gotuHomeVC()
    }
    
    /*
         func callWebServiceForgotPassword(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let authToken = dictOfUserData["authToken"] as? String ?? ""
            UserDefaults.standard.set(authToken, forKey: UserDefaults.keys.authToken)

            let dicParam = ["businessType":"independent",
                            "userType" : "artist", ]
            
            objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success: { response in
                objActivity.stopActivity()
                if response["status"] as! String == "success"{
                    let msg1 = response["message"] as? String ?? ""
                    let dictOfUserData2 = response["user"] as? [String:Any] ?? ["":""]
                    let busuinesstype = dictOfUserData2["businessType"] as? String ?? ""
                    UserDefaults.standard.setValue(busuinesstype,forKey: UserDefaults.keys.businessNameType)
                    let strToken = dictOfUserData2["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    UserDefaults.standard.set(dictOfUserData2, forKey: UserDefaults.keys.userInfoDic)
                    UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                    UserDefaults.standard.synchronize()
                    
                    
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictOfUserData2)
                    
                    var dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                        dict[UserDefaults.keys.isDocument] = "0"
                        UserDefaults.standard.set(dict, forKey: UserDefaults.keys.userInfoDic)
                        UserDefaults.standard.set(dict, forKey: UserDefaults.keys.businessInfoDic)
                        
                    
                   UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)
                    appDelegate.showSignUpProcess()
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
       */
 }
