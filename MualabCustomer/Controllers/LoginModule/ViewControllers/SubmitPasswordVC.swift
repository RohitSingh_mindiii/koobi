//
//  SubmitPasswordVC.swift
//  MualabBusiness
//
//  Created by Mac on 28/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class SubmitPasswordVC: UIViewController {
    
    var iconClick : Bool!
    var iconClickConfirm : Bool!
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    
    fileprivate var _refHandle: DatabaseHandle?
    
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var btnShowPassword : UIButton!
    @IBOutlet weak var imgEye: UIImageView!
    @IBOutlet weak var imgEyeConfirm: UIImageView!
    @IBOutlet weak var lblLogin:UILabel!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    var userImage : UIImage?
    
}
//MARK: - View Hirarchy
extension SubmitPasswordVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objWebserviceManager.StopIndicator()

        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        iconClick = true
        iconClickConfirm = true
        addGasturesToViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension SubmitPasswordVC{
    func configureView(){
        objWebserviceManager.StopIndicator()
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.size.height/2
      //  self.btnShowPassword.isHidden = true
    }
    
    func addGasturesToViews() {
        
//        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleLoginTap(gestureRecognizer:)))
//        lblLogin.addGestureRecognizer(loginTap)
        
        // let countryCodeTap = UITapGestureRecognizer(target: self, action: #selector(handleCountryCodeTap(gestureRecognizer:)))
        // lblCountryCode.addGestureRecognizer(countryCodeTap)
    }
    

}

//MARK: - UITapGesttureActions
fileprivate extension SubmitPasswordVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK: - IBActions
fileprivate extension SubmitPasswordVC{
    
    @IBAction func btnEyeAction(_ sender: Any) {
        if(iconClick == true) {
            self.txtPassword.isSecureTextEntry = false
            imgEye.image = UIImage.init(named: "open_eyes_ico")
            iconClick = false
        } else {
            self.txtPassword.isSecureTextEntry = true
            //
            imgEye.image = UIImage.init(named: "Close_eye_ico")
            iconClick = true
        }
    }
    
    @IBAction func btnEyeConfirmAction(_ sender: Any) {
        if(iconClickConfirm == true) {
            self.txtConfirmPassword.isSecureTextEntry = false
            imgEyeConfirm.image = UIImage.init(named: "open_eyes_ico")
            iconClickConfirm = false
        } else {
            self.txtConfirmPassword.isSecureTextEntry = true
            //
            imgEyeConfirm.image = UIImage.init(named: "Close_eye_ico")
            iconClickConfirm = true
        }
    }
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPassword(_ sender:UIButton){
        if self.btnShowPassword.isSelected {
            self.btnShowPassword.isSelected = false
            self.txtPassword.isSecureTextEntry = true
        }else{
            self.btnShowPassword.isSelected = true
            self.txtPassword.isSecureTextEntry = false
        }
    }
    @IBAction func btnContinue(_ sender:UIButton){
        var invalid = false
        var strMessage = ""
        
        txtPassword.text = txtPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtConfirmPassword.text = txtConfirmPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.txtPassword.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtPassword)
        }else if !(objValidationManager.isPasswordContainsCap(self.txtPassword.text!)) || !(objValidationManager.isPasswordContainsNum(self.txtPassword.text!)) || (self.txtPassword.text?.count)! < 8{
            invalid = true
            strMessage = "Use 8 or more characters including uppercase letters and numbers"
//message.validation.validPassword
        }else if self.txtConfirmPassword.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtConfirmPassword)
        }else if self.txtPassword.text! != self.txtConfirmPassword.text!{
            invalid = true
            strMessage = "Password and confirm password should be same"
        }

        if invalid && strMessage.count>0 {
            // objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.bannerDark, on: self)
           
        }else if !invalid{
            self.view.endEditing(true)
            //  self.saveUserData()
            self.callWebserviceForRegistration()
        }
    }
}
//MARK: - Webservices
fileprivate extension SubmitPasswordVC{
    
    func callWebserviceForRegistration(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        
        if userInfo.gender == "0" || userInfo.gender == ""{
            userInfo.gender = "male"
        }
        let strUserName = userInfo.userName.lowercased()
         //Rohit
        let dicParam = ["user_name":strUserName,
                        "first_name":userInfo.firstName,
                        "last_name":userInfo.lastName,
                        "email":userInfo.email,
                        "password":self.txtPassword.text!,
                        "country_code":userInfo.countryCode,
                        "contact_no":userInfo.contactNumber,
                       // "userType":"user",
                        //"deviceType":"1",
                        "gender":userInfo.gender,
                        "dob":userInfo.DOB,
                       // "deviceToken":objAppShareData.firebaseToken,
                        "firebase_token":objAppShareData.firebaseToken,
                        "social_id":userInfo.strSocialId,
                        //"socialType": userInfo.strSocialType,
                       
                        "address": userInfo.address.fullAddress,
                        //"address2": userInfo.address.fullAddress,
            
                        "city": userInfo.address.city,
                        "state":userInfo.address.state,
                        "country": userInfo.address.country,
                        "latitude": userInfo.address.latitude,
                        "longitude": userInfo.address.longitude,
                        ]

        print(dicParam)
        
      /*  profileImage: imageName,
        gender: fields.gender,
        dob: fields.dob,
         
        userType: fields.userType,
        socialId: fields.socialId,
        socialType: fields.socialType,
        deviceType: fields.deviceType,
        deviceToken: fields.deviceToken,
        chatId: fields.chatId,
        firebaseToken: fields.firebaseToken,
        OTP: otpStatus,
        otpVerified: 1,
        location: [fields.latitude,fields.longitude]
*/
        
        var imgData : Data?
        if let img = self.userImage{
            imgData = img.jpegData(compressionQuality: 0.1)
           //imgData = UIImageJPEGRepresentation(img, 1.0)
        }
        
        objServiceManager.uploadMultipartData(strURL: WebURL.registration, params: dicParam as [String : AnyObject] , imageData: imgData, fileName: "file.jpg", key: "profile_image", mimeType: "image/jpg", success: { response in
            
            print(response)
            
            if response["status"] as? String ?? "" == "success"{
                
                if let dictUser = response["users"] as? [String : Any]{
                    
                    let strToken = dictUser["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    UserDefaults.standard.set(dictUser, forKey: UserDefaults.keys.userInfo)
                    UserDefaults.standard.set(dictUser["customerId"], forKey: UserDefaults.keys.stripeCustomerId)
                    UserDefaults.standard.setValue(dictUser["cardId"], forKey: UserDefaults.keys.stripeCardId)
                    UserDefaults.standard.set(true, forKey: UserDefaults.keys.isLoggedIn)
                    UserDefaults.standard.setValue(1, forKey: UserDefaults.keys.notificationStatus)
                    if let id = dictUser["_id"] as? String{
                        UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                    }else if let id = dictUser["_id"] as? Int{
                        UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                    }
                    UserDefaults.standard.set(dictUser["userName"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                    UserDefaults.standard.set(dictUser["profileImage"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                    
                    UserDefaults.standard.synchronize()
                    
                    strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                    //Rohit
                    //self.writeUserDataToFireBase(dict: dictUser)
                }
                appDelegate.gotoTabBar(withAnitmation: true)
            }else{
                if let msg = response["message"] as? String{
                   objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            
           objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
//MARK: - UITextFieldDelegate
extension SubmitPasswordVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == txtPassword{
        txtConfirmPassword.becomeFirstResponder()
        }else if textField == txtConfirmPassword{
            txtConfirmPassword.resignFirstResponder()
        }
        return true
    }
}

//MARK: - FireBaseRegistration Method
extension SubmitPasswordVC {
    
    func writeUserDataToFireBase(dict:[String : Any]){
        var id = 0
        if let ids = dict["_id"] as? Int{
            id = ids
        }else if let ids = dict["_id"] as? String{
            id = Int(ids)!
        }

        var img = dict["profileImage"] as? String ?? ""
        if (dict["profileImage"] as? String ?? "") != "" {
            
        }else{
            img = "http://koobi.co.uk:3000/uploads/default_user.png"
        }

        let calendarDate = ServerValue.timestamp()
        let dict1 = ["firebaseToken":checkForNULL(obj: dict["firebaseToken"]),
                     "isOnline":checkForNULL(obj:1),
                     "lastActivity":checkForNULL(obj:calendarDate),
                     "profilePic":checkForNULL(obj:img),
                     "uId":checkForNULL(obj:id),
                     "userName":checkForNULL(obj:dict["userName"]) ]
        
        let ref = Database.database().reference()
        let myId:String = String(dict["_id"] as? Int ?? 0000000)
        ref.child("users").child(myId).setValue(dict1)
        
       // self.gotuHomeVC()
    }
    
    func gotuHomeVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"HomePageVC") as! HomePageVC
         self.navigationController?.pushViewController(objVC, animated: true)
    }
}
