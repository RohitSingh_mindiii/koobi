//
//  VerifyPhoneNumberVC.swift
//  MualabBusiness
//
//  Created by Mac on 23/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class VerifyPhoneNumberVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnResend : UIButton!
    @IBOutlet weak var viewSuper : UIView!

    var userAlreadyExist = ""
    var dictOfUserData = [String:Any]()
    @IBOutlet weak var imgLogo : UIImageView!

    var strOTP = ""
    fileprivate var  time:Int = 0
    fileprivate var timer:Timer?
    
    @IBOutlet weak var lbl1: UITextField!
    @IBOutlet weak var lbl2: UITextField!
    @IBOutlet weak var lbl3: UITextField!
    @IBOutlet weak var lbl4: UITextField!
}
//MARK: - View Hirarchy
extension VerifyPhoneNumberVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl1.delegate = self
        self.lbl2.delegate = self
        self.lbl3.delegate = self
        self.lbl4.delegate = self
        self.addGasturesToViews()
        objAppShareData.showAlert(withMessage: "Verification Code has been sent to your device.", type: alertType.bannerDark, on: self)
        if #available(iOS 12.0, *) {
            lbl1.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        // self.addTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func addGasturesToViews() {
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleLoginTap(gestureRecognizer:)))
        viewSuper.addGestureRecognizer(loginTap)
        
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
    }
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
}

//MARK: - textfieldDelegate method
extension VerifyPhoneNumberVC{
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        let text = textField.text! as NSString

        var searchString: String? = nil
        
        if textField == self.lbl1 {
            //view1.backgroundColor = UIColor.darkGray
            //txtFieldSelected = lbl1
            if string.count > 0 {
                //searchString = (lbl1.text! + (string)).uppercased()
                searchString = (string).uppercased()
            } else {
                //searchString = ((lbl1.text as? NSString)?.substring(to: (lbl1.text?.count)! - 1))?.uppercased()
            }
            if (searchString?.count ?? 0) >= 1 {
                //if(searchString.length == 1){
                if (searchString?.count ?? 0) == 1 {
                    lbl1.text = searchString
                }
                searchString = nil
                lbl2.becomeFirstResponder()
                //self.txt2.text = @"";
                return false
            } else {

            }
        } else if textField == self.lbl2 {
            //view1.backgroundColor = UIColor.darkGray
            //txtFieldSelected = lbl1
            if string.count > 0 {
                //searchString = (lbl2.text! + (string)).uppercased()
                searchString = (string).uppercased()

            } else {
                //searchString = ((lbl2.text as? NSString)?.substring(to: (lbl2.text?.count)! - 1))?.uppercased()
            }
            if (searchString?.count ?? 0) >= 1 {
                //if(searchString.length == 1){
                if (searchString?.count ?? 0) == 1 {
                    lbl2.text = searchString
                }
                searchString = nil
                lbl3.becomeFirstResponder()
                //self.txt2.text = @"";
                return false
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.lbl1.becomeFirstResponder()
                }
            }
        }else if textField == self.lbl3 {
            //view1.backgroundColor = UIColor.darkGray
            //txtFieldSelected = lbl1
            if string.count > 0 {
                //searchString = (lbl3.text! + (string)).uppercased()
                searchString = (string).uppercased()

            } else {
                //searchString = ((lbl3.text as? NSString)?.substring(to: (lbl3.text?.count)! - 1))?.uppercased()
            }
            if (searchString?.count ?? 0) >= 1 {
                //if(searchString.length == 1){
                if (searchString?.count ?? 0) == 1 {
                    lbl3.text = searchString
                }
                searchString = nil
                lbl4.becomeFirstResponder()
                //self.txt2.text = @"";
                return false
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.lbl2.becomeFirstResponder()
                }
            }
        }else if textField == self.lbl4 {
            //view1.backgroundColor = UIColor.darkGray
            //txtFieldSelected = lbl1
            if string.count > 0 {
               // searchString = (lbl4.text! + (string)).uppercased()
                searchString = (string).uppercased()
            } else {
                //searchString = ((lbl4.text as? NSString)?.substring(to: (lbl4.text?.count)! - 1))?.uppercased()
            }
            if (searchString?.count ?? 0) >= 1 {
                //if(searchString.length == 1){
                if (searchString?.count ?? 0) == 1 {
                    lbl4.text = searchString
                }
                searchString = nil
                lbl4.resignFirstResponder()
                //self.txt2.text = @"";
                return false
            } else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.lbl3.becomeFirstResponder()
                }
            }
        }
        return  true
    }

//
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.lbl4{
            self.lbl3.becomeFirstResponder()
        } else if textField == self.lbl3{
            self.lbl2.becomeFirstResponder()
        }else if textField == self.lbl2{
            self.lbl1.becomeFirstResponder()
        }else if textField == self.lbl1{
            self.lbl1.resignFirstResponder()
        }
        return true
    }
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
        self.lbl4.resignFirstResponder()
    }
}
    


//MARK: - Local Methods
fileprivate extension VerifyPhoneNumberVC{
    func configureView(){
        self.btnSubmit.layer.cornerRadius = self.btnSubmit.frame.size.height/2
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.size.height/2
        if self.view.frame.size.height == 480{
            self.imgLogo.isHidden = true
        }
        self.btnResend.isEnabled = true
        self.btnResend.setTitle("Resend Code", for: UIControl.State.normal)
    }
   
    
 
    
    func GotoFillUserDetailVC(){
        if userAlreadyExist == "YES"{
            let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            let objVC = sb.instantiateViewController(withIdentifier:"ManageAccountVC") as! ManageAccountVC
            objVC.dictOfUserData = dictOfUserData
        self.navigationController?.pushViewController(objVC, animated: true)
        }else{
            let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            let objVC = sb.instantiateViewController(withIdentifier:"FillUserDetailVC") as! FillUserDetailVC
            self.navigationController?.pushViewController(objVC, animated: true)
            objAppShareData.showAlert(withMessage: "Great work! You have successfully verified your Phone Number.", type: alertType.bannerDark, on: self)
        }
    }
    
    func addTimer(){
        time = 61
        self.btnResend.isEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    @objc func update() {
        time = time - 1
        if time == 60{
            self.btnResend.setTitle("01:00", for: UIControl.State.normal)
        }else{
            self.btnResend.setTitle("00:\(String(describing:time))", for: UIControl.State.normal)
        }
        if time == 0{
            self.btnResend.isEnabled = true
            self.btnResend.setTitle("Resend Code", for: UIControl.State.normal)
            self.timer?.invalidate()
        }
        // Something cool
    }
}

//MARK : - IBActions
fileprivate extension VerifyPhoneNumberVC{
    
    @IBAction func btnInsertNumbers(_ sender: Any){
        //insertNumber((sender as AnyObject).tag)
    }
    
    
    @IBAction func btnSubmit(_ sender:Any){
        
        //Rohit
        let insertedOtp = "\(self.lbl1.text!)\(self.lbl2.text!)\(self.lbl3.text!)\(self.lbl4.text!)"
        if self.lbl1.text == "" || self.lbl2.text == "" || self.lbl3.text == "" || self.lbl4.text == ""{
            objAppShareData.showAlert(withMessage: "Please enter OTP", type: alertType.bannerDark, on: self)
        }else if insertedOtp != ""{
            call_ws_VerifyOTP(strVerifyOTP: insertedOtp)
        }
        
        
        //        //GotoFillUserDetailVC()
        //        //return
        //        let insertedOtp = "\(self.lbl1.text!)\(self.lbl2.text!)\(self.lbl3.text!)\(self.lbl4.text!)"
        //        print(self.lbl1.text ?? "000")
        //        if self.lbl1.text! == "*"{
        //            objAppShareData.showAlert(withMessage: "Please enter OTP", type: alertType.bannerDark, on: self)
        //        }else if insertedOtp == self.strOTP{
        //            GotoFillUserDetailVC()
        //        }else{
        //
        //            objAppShareData.showAlert(withMessage: message.validation.invalidOTP, type: alertType.bannerDark, on: self)
        //            lbl4.text = ""
        //            lbl3.text = ""
        //            lbl2.text = ""
        //            lbl1.text = ""
        //        }
    }
    @IBAction func btnResendCode(_ sender:Any){
        lbl4.text = ""
        lbl3.text = ""
        lbl2.text = ""
        lbl1.text = ""
        callWebserviceForVerifyPhoneNumber()
    }
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: - Webservices
fileprivate extension VerifyPhoneNumberVC{
    
    func callWebserviceForVerifyPhoneNumber(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        let dicParam = ["country_code":userInfo.countryCode,
                        "contact_no":userInfo.contactNumber,
                       // "socialId":"",
                        "email":userInfo.email]
        print(dicParam)
        objServiceManager.requestPost(strURL: WebURL.phonVerification, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                //Rohit
               // self.strOTP = String.init(describing: response["otp"]!)
               
                //objAppShareData.showAlert(withMessage: "New OTP has been sent to you", type: alertType.bannerDark, on: self)
                objAppShareData.showAlert(withMessage: "A new Verification Code has been sent to your device.", type: alertType.bannerDark, on: self)
                //self.addTimer()
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
     //Rohit
        //Verify Inserted OTP
        func call_ws_VerifyOTP(strVerifyOTP: String){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            objActivity.startActivityIndicator()
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
            let dicParam = ["country_code":userInfo.countryCode,
                            "contact_no":userInfo.contactNumber,
                            "otp":strVerifyOTP]
            
            objWebserviceManager.requestPost(strURL: WebURL.verifyAuthOtp, params: dicParam, success: { (response) in
                objActivity.stopActivity()
                
                if response["status"] as? String ?? "" == "success"{
                    self.GotoFillUserDetailVC()
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                        self.lbl4.text = ""
                        self.lbl3.text = ""
                        self.lbl2.text = ""
                        self.lbl1.text = ""
                    }
                }
                
                
                
            }) { (Error) in
                print(Error)
            }
            
        }
        
    }
