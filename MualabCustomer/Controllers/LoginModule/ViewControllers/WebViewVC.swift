//
//  WebViewVC.swift
//  Habito
//
//  Created by Mindiii 2 on 21/12/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var lblHeaderText: UILabel!
    @IBOutlet weak var webViewOpen: UIWebView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    var strComeFrom = ""
}

// MARK: - View LifeCycle
extension WebViewVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        //if objAppShareData.isFromSlideMenuToProfile{
           //objAppShareData.isFromSlideMenuToProfile = false
           // self.btnBack.isHidden = true
           // self.imgBack.isHidden = true
           // self.btnMenu.isHidden = false
           // self.imgMenu.isHidden = false
       // }else{
           // self.btnBack.isHidden = false
           // self.imgBack.isHidden = false
           // self.btnMenu.isHidden = true
           // self.imgMenu.isHidden = true
       // }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        webViewOpen.delegate = self
        webViewOpen.backgroundColor = UIColor.clear
        configureView()
        objWebserviceManager.StartIndicator()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Local Func
extension WebViewVC{
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureView(){
        self.lblHeaderText.text = self.strComeFrom
        var strUrl = ""
        if self.strComeFrom == "Terms & Conditions" {
            strUrl = WebURL.termsURL
        }else{
            strUrl = WebURL.privacyURL
        }
            if strUrl == ""{
            }else{
                let url = NSURL (string:strUrl)!
            let requestObj = NSURLRequest(url: url as URL);
            webViewOpen.loadRequest(requestObj as URLRequest)
            }
    }
}

//MARK :- Button Action
extension WebViewVC{
    @IBAction func btnBackAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
}
extension WebViewVC{
   func webViewDidStartLoad(_ webView: UIWebView) {
    }
   func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
       objWebserviceManager.StopIndicator()
    }
   func webViewDidFinishLoad(_ webView: UIWebView){
        objWebserviceManager.StopIndicator()
    }
}

