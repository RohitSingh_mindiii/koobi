//
//  alertVC.swift
//  MualabBusiness
//
//  Created by Mac on 05/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

fileprivate enum type:String {
    case noNetwork,
    error,
    banner,
    bannerDark,
    sessionExpire
}

class alertVC: UIViewController {
    
    var alertMessage:String?
    var alertType:String?
    
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblNotiMessage:UILabel!
    @IBOutlet weak var lblBannerTitle:UILabel?
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var imgWarring:UIImageView!
    @IBOutlet weak var alertBigView:UIView!
    @IBOutlet weak var alertBannerView:UIView!
    
    @IBOutlet weak var btnOk:UIButton!
    
    @IBOutlet weak var alert1Top : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNotiMessage.text = alertMessage ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        objWebserviceManager.StopIndicator()
        UIApplication.shared.statusBarStyle = .lightContent
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        showAlert()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
fileprivate extension alertVC{
    func showAlert(){
        
        switch self.alertType {
        case type.noNetwork.rawValue?:do{
            
            self.alertBigView.isHidden = false
            //self.lblTitle.text = "No Network"
            self.lblTitle.text = "Wake up your connection!"
            //self.lblMessage.text = message.noNetwork
            self.lblMessage.text = "Your internet seems too slow to reach our server."
            self.imgWarring.image = UIImage.customImage.noNetwork
            self.imgWarring.image = UIImage.init(named:"network_tower_ico")
            self.btnOk.setTitle("Try again", for: UIControl.State.normal)
            }
            
        case type.error.rawValue?:do{
            
            self.alertBigView.isHidden = false
            //self.lblTitle.text = "Oops"
            self.lblTitle.text = "Temporary Maintenance"
            //self.lblMessage.text = message.error.wrong
            self.lblMessage.text = "The web server koobi.co.uk is currently undergoing some maintenance"
            //self.imgWarring.image = UIImage.customImage.error
            self.imgWarring.image = UIImage.init(named:"network_ico")
            self.btnOk.setTitle("Try again", for: UIControl.State.normal)
            }
            
        case type.banner.rawValue?:do{
            self.alertBigView.isHidden = true
            alert1Top.constant = 0
           // alertBannerView.backgroundColor = UIColor.theameColors.pinkColor.withAlphaComponent(0.8)
            alertBannerView.backgroundColor = #colorLiteral(red: 0.2784313725, green: 0.2509803922, blue: 0.2392156863, alpha: 0.85) // UIColor.lightGray.withAlphaComponent(0.8)

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                self.dismiss(animated: false, completion: nil)
            }
            }
        case type.bannerDark.rawValue?:do{
            self.alertBigView.isHidden = true
            alert1Top.constant = 0
            //alertBannerView.backgroundColor = UIColor.theameColors.darkColor.withAlphaComponent(0.8)
            alertBannerView.backgroundColor = #colorLiteral(red: 0.2784313725, green: 0.2509803922, blue: 0.2392156863, alpha: 0.85) // UIColor.darkGray.withAlphaComponent(0.8)

            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                self.dismiss(animated: false, completion: nil)
            }
            }
        case type.sessionExpire.rawValue?:do{
            
            self.alertBigView.isHidden = false
            self.lblTitle.text = "Session expired"
            self.lblMessage.text = message.sessionExpire
            self.imgWarring.image = UIImage.customImage.sessionExpire
            self.btnOk.setTitle("Ok", for: UIControl.State.normal)
            }
        default:
            do{
                self.alertBigView.isHidden = false
                self.lblTitle.text = "Oops"
                self.lblMessage.text = message.error.wrong
                self.imgWarring.image = UIImage.customImage.error
                self.btnOk.setTitle("Retry", for: UIControl.State.normal)
            }
        }
        
    }
}
fileprivate extension alertVC{
    @IBAction func btnRetry(_ sender:Any){
        self.dismiss(animated: false, completion: nil)
        if self.alertType == type.sessionExpire.rawValue{
            appDelegate.logout()
        }
    }
}

