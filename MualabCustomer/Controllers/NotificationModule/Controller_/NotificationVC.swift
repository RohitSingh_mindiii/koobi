//
//  BookingDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 04/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit
import Firebase
import SDWebImage

class NotificationVC: UIViewController {
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var attributes : [[NSAttributedString.Key : Any]] = {
        //        [[NSAttributedStringKey.foregroundColor:UIColor.black],
        //         [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15)]]
        [[NSAttributedString.Key.foregroundColor:UIColor.black],
         [NSAttributedString.Key.font: UIFont(name: "Nunito-SemiBold", size: 16)!]]
    }()
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    @IBOutlet weak var viewSocialbadgeCount: UIView!
    @IBOutlet weak var lblSocialbadgeCount: UILabel!
    @IBOutlet weak var viewBookingbadgeCount: UIView!
    @IBOutlet weak var lblBookingbadgeCount: UILabel!
    @IBOutlet weak var imgBooking: UIImageView!
    @IBOutlet weak var imgSocial: UIImageView!
    @IBOutlet weak var btnSocial: UIButton!
    @IBOutlet weak var btnBooking: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewPastBooking: UIView!
    @IBOutlet weak var viewFutureBooking: UIView!
    @IBOutlet weak var viewOptionDots: UIView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var tblPastBooking: UITableView!
    @IBOutlet weak var tblFutureBooking: UITableView!
    @IBOutlet weak var lblBusinessInvitationCount: UILabel!
    @IBOutlet weak var viewBusinessInvitationCount: UIView!

    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblNoRecord: UIView!
    
    var selectedIndexPath : IndexPath?
    var arrBookingNotificationData : [NotificationData] = []
    var arrSocialNotificationData : [NotificationData] = []
    var isLoading = false
    fileprivate var strUserId : String = ""
    fileprivate var isSocial = false
    fileprivate var isOnNotificationScreen = false
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var lastLoadedPage = 0
    
    fileprivate var pageNo2: Int = 0
    fileprivate var totalCount2 = 0
    fileprivate var lastLoadedPage2 = 0
    
    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    
    fileprivate let reviewTextLimit = 150
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    lazy var refreshControl2: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        self.addGesturesToView()
        self.manageBookingRequestCount()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshChatBadgeCount), name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshSocialBookingBadgeCount), name: NSNotification.Name(rawValue: "refreshSocialBookingBadgeCount"), object: nil)
        //self.getNotificationDataWith(0)
        
        segmentController.selectedSegmentIndex = 0
        self.viewPastBooking.isHidden = false
        self.viewFutureBooking.isHidden = true
        self.viewOptionDots.isHidden = true
        
        self.updateArtistInfo()
        self.isSocial = true
        self.showSocialTabSelected()
    }
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            //self.viewChatbadgeCount.isHidden = false
            self.viewChatbadgeCount.isHidden = true
        }
    }
    @objc func refreshSocialBookingBadgeCount(_ objNotify: Notification){
        if objAppShareData.badgeContSocialNotification == 0{
            self.viewSocialbadgeCount.isHidden = true
        }else{
            self.lblSocialbadgeCount.text = String(objAppShareData.badgeContSocialNotification)
            self.viewSocialbadgeCount.isHidden = false
        }
        if objAppShareData.badgeContBookingNotification == 0{
            self.viewBookingbadgeCount.isHidden = true
        }else{
            self.lblBookingbadgeCount.text = String(objAppShareData.badgeContBookingNotification)
            self.viewBookingbadgeCount.isHidden = false
        }
    }
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.logout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.isOnNotificationScreen = false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.isOnNotificationScreen = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        self.lblNoRecord.isHidden = true

        self.updateArtistInfo()

        self.getNotificationDataWith(0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        selectedIndexPath = nil
        self.getNotificationDataWith(0)
        //API Call
    }
}

//MARK: - custome method extension
extension NotificationVC {
    
    func configureView(){
        self.tblPastBooking.estimatedRowHeight = 50.0
        self.tblPastBooking.rowHeight = UITableView.automaticDimension
        self.tblFutureBooking.estimatedRowHeight = 50.0
        self.tblFutureBooking.rowHeight = UITableView.automaticDimension
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                self.strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = userInfo["_id"] as? Int {
                self.strUserId = "\(userId)"
            }
        }
        
        ref = Database.database().reference()
        _refHandle = ref.child("socialBookingBadgeCount").child(self.strUserId).observe(.value, with: { [weak self] (snapshot) in
            guard let strongSelf = self else {
                return
            }
            let dict = snapshot.value as? [String:Any]
            print(dict)
            if let count = dict?["socialCount"] as? Int {
                objAppShareData.badgeContSocialNotification = count
            }else if let count = dict?["socialCount"] as? String {
                objAppShareData.badgeContSocialNotification = Int(count) ?? 0
            }
            if let count = dict?["bookingCount"] as? Int {
                objAppShareData.badgeContBookingNotification = count
            }else if let count = dict?["bookingCount"] as? String {
                objAppShareData.badgeContBookingNotification = Int(count) ?? 0
            }
            if strongSelf.isOnNotificationScreen {
                if strongSelf.isSocial{
                    strongSelf.ref.child("socialBookingBadgeCount").child(strongSelf.strUserId).updateChildValues(["socialCount":0])
                    objAppShareData.badgeContSocialNotification = 0
                }else{
                    strongSelf.ref.child("socialBookingBadgeCount").child(strongSelf.strUserId).updateChildValues(["bookingCount":0])
                    objAppShareData.badgeContBookingNotification = 0
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshSocialBookingBadgeCount"), object: nil)
        })
        
        //        outerView.layer.cornerRadius = 10
        //        outerView.layer.borderColor =             UIColor.theameColors.pinkColor.cgColor
        //        outerView.layer.borderWidth = 1
        //        outerView.clipsToBounds = true
        
        self.tblPastBooking.delegate = self
        self.tblPastBooking.dataSource = self
        self.tblFutureBooking.delegate = self
        self.tblFutureBooking.dataSource = self
        
        self.tblPastBooking.addSubview(self.refreshControl)
        self.tblFutureBooking.addSubview(self.refreshControl2)
        
    }
    
    func updateArtistInfo(){
        self.imgProfile.image = UIImage(named: "cellBackground")
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            }
        }
    }
}

//MARK: - btn Actions
extension NotificationVC {
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    //    @IBAction func btnProfileAction(_ sender: Any) {
    //        self.view.endEditing(true)
    //        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    //    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.isOtherSelectedForProfile = false
        self.gotoProfileVC()
    }
    
    
    @IBAction func segmentButtonSocialOrBooking(_ sender: UIButton) {
        self.changeSegment(sender: sender.tag)
    }
    
    func changeSegment(sender:Int){
        if self.isLoading{
            return
        }
        
        if sender == 0 {
            if self.isSocial{
                return
            }
            self.viewOptionDots.isHidden = true
            showSocialTabSelected()
            isSocial = true
            //viewPastBooking.isHidden = false
            //viewFutureBooking.isHidden = true
            self.lblNoRecord.isHidden = true
            //if arrSocialNotificationData.count == 0{
            self.getNotificationDataWith(0)
            //}
            
        }else if sender == 1{
            if !isSocial{
                return
            }
            self.viewOptionDots.isHidden = false
            showBookingTabSelected()
            isSocial = false
            //viewPastBooking.isHidden = true
            //viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            //if arrFutureBookingData.count == 0{
            self.getNotificationDataWith(0)
            //}
        }
    }
    func showBookingTabSelected() {
        self.imgSocial.backgroundColor = UIColor.lightGray
        self.imgBooking.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnSocial.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnBooking.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.ref.child("socialBookingBadgeCount").child(self.strUserId).updateChildValues(["bookingCount":0])
    }
    
    func showSocialTabSelected() {
        self.imgBooking.backgroundColor = UIColor.lightGray
        self.imgSocial.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnSocial.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.btnBooking.setTitleColor(UIColor.lightGray, for: .normal)
        self.ref.child("socialBookingBadgeCount").child(self.strUserId).updateChildValues(["socialCount":0])
    }
    
    @IBAction func segmentPastFuture(_ sender: UISegmentedControl) {
        
        //        switch segmentController.selectedSegmentIndex {
        //        case 0:
        //            viewPastBooking.isHidden = false
        //            viewFutureBooking.isHidden = true
        //            self.lblNoRecord.isHidden = true
        //           // self.lblHeader.text = "Past Booking"
        //            if arrBookingNotificationData.count == 0{
        //                self.getNotificationDataWith(0)
        //            }
        //
        //        case 1:
        //            viewPastBooking.isHidden = true
        //            viewFutureBooking.isHidden = false
        //            self.lblNoRecord.isHidden = true
        //           // self.lblHeader.text = "Future Booking"
        //            if arrSocialNotificationData.count == 0{
        //                self.getNotificationDataWith(0)
        //            }
        //
        //        default:
        //            break;
        //        }
    }
}

// MARK: - table view Delegate and Datasource
extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isSocial{
            return arrSocialNotificationData.count
        }else{
            return arrBookingNotificationData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if self.isLoading{
            return UITableViewCell()
        }
        
        if tableView == tblPastBooking {
            if self.isLoading{
                return UITableViewCell()
            }
            
            if !self.isSocial{
                if self.arrBookingNotificationData.count < self.totalCount {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                        self.getNotificationDataWith(nextPage)
                    }
                }
            }else{
                if self.arrSocialNotificationData.count < self.totalCount2 {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                        self.getNotificationDataWith(nextPage)
                    }
                }
            }
            
        }else{
            if self.isLoading{
                return UITableViewCell()
            }
            if self.arrSocialNotificationData.count < self.totalCount2 {
                
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                    self.getNotificationDataWith(nextPage)
                }
            }
        }
        
        var obj : NotificationData = NotificationData.init(dict: ["":""])
        if self.isSocial{
            obj = arrSocialNotificationData[indexPath.row]
        }else{
            obj = arrBookingNotificationData[indexPath.row]
        }
        
        if obj.type == "booking" {
            
            if let cell : CellNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellNotificationDetail")! as? CellNotificationDetail {
                
                //if let cell : CellSocialNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellSocialNotificationDetail")! as? CellSocialNotificationDetail {
                
                cell.lblName.text = obj.userName
                //cell.lblMsg.attributedText = obj.messageAtt  //obj.message
                cell.lblMsg.text = obj.message
                cell.lblTime.text  = obj.date
                
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                        cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }else{
                    cell.imgVwProfile.image = UIImage(named: "cellBackground")
                }
                
                cell.lblTimeAgo.text = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblTimeAgo.isHidden = false
                }else{
                    let indexOldData = arrBookingNotificationData[indexPath.row-1].strTimeAgo
                    if indexOldData == obj.strTimeAgo{
                        cell.lblTimeAgo.isHidden = true
                    }else{
                        cell.lblTimeAgo.isHidden = false
                    }
                }
                
                cell.lblTimeAgo.text  = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblTimeAgo.isHidden = false
                }else{
                    if tableView == tblPastBooking{
                        let indexOldData = arrBookingNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblTimeAgo.isHidden = true
                            }else{
                                cell.lblTimeAgo.isHidden = false
                            }
                        }
                    }else{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblTimeAgo.isHidden = true
                            }else{
                                cell.lblTimeAgo.isHidden = false
                            }
                        }
                    }
                }
                cell.btnProfile.accessibilityHint = obj.userName
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            
            if let cell : CellSocialNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellSocialNotificationDetail")! as? CellSocialNotificationDetail {
                
                cell.lblName.text  = obj.userName
                //cell.lblMsg.attributedText = obj.messageAtt  //obj.message
                cell.lblMsg.text = obj.message
                cell.lblTime.text = obj.date
                
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                        cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }else{
                    cell.imgVwProfile.image = UIImage(named: "cellBackground")
                }
                
                cell.lblTimeAgo.text  = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblTimeAgo.isHidden = false
                }else{
                    if tableView == tblPastBooking{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblTimeAgo.isHidden = true
                            }else{
                                cell.lblTimeAgo.isHidden = false
                            }
                        }
                    }else{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblTimeAgo.isHidden = true
                            }else{
                                cell.lblTimeAgo.isHidden = false
                            }
                        }
                    }
                }
                
                cell.btnProfile.accessibilityHint = obj.userName
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                return cell
                
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if isLoading{
            return
        }
        var obj : NotificationData = NotificationData.init(dict: ["":""])
        if self.isSocial{
            obj = arrSocialNotificationData[indexPath.row]
        }else{
            obj = arrBookingNotificationData[indexPath.row]
        }
        
        if obj.notifincationType == 7 ||  obj.notifincationType == 16{
            var strMyId = ""
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    strMyId = "\(userId)"
                }
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                if let id = userInfo["_id"] as? Int {
                    strMyId = String(id)
                }
            }
            let strOtherId = String(obj.senderId!)
            var BlockRoom = ""
            BlockRoom = strOtherId+"_"+strMyId
            var miId = 0
            miId = Int(strMyId)!
            var oopId = 0
            oopId = Int(strOtherId) ?? 0
            if miId <= oopId{
                BlockRoom = strMyId+"_"+strOtherId
            }
            self.ref.child("blockUserArtist").child(BlockRoom).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
                //guard let strongSelf = self else { return }
                var blockedBy = ""
                if let dict = snapshot.value as? [String:Any]{
                    let strBlockedBy =  dict["blockedBy"] as? String ?? ""
                    switch strBlockedBy{
                    case strMyId:
                        blockedBy = "me"
                        
                    case strOtherId:
                        blockedBy =
                        "opponent"
                        
                    case "Both":
                        blockedBy = "both"
                        
                    default:
                        blockedBy = "none"
                    }
                }else {
                    blockedBy = "none"
                }
                if blockedBy == "opponent" || blockedBy == "both" {
                    objAppShareData.showAlert(withMessage: "You are blocked by this user! will no longer see their feed", type: alertType.bannerDark,on: self)
                }else{
                    self.redirectNotificationWith(obj: obj)
                }
            })
        }else{
            self.redirectNotificationWith(obj: obj)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//MARK: - custome method extension
extension NotificationVC {
    
    func getNotificationDataWith(_ page: Int) {
        
        var parameters = [String:Any]()
        
        //var strBookingType : String = ""
        
        if self.segmentController.selectedSegmentIndex == 0 {
            self.lastLoadedPage = page
            self.pageNo = page
            // strBookingType = "booking"
        }else{
            self.lastLoadedPage2 = page
            self.pageNo2 = page
            //strBookingType = "social"
        }
        
        parameters = [
            "userId":strUserId,
            "page":page,
            //"limit":self.pageSize,
            "limit":"5000",
            //"type": strBookingType
        ]
        self.webServiceCall_getBookingData(parameters: parameters)
    }
    
    
    func webServiceCall_getBookingData(parameters : [String:Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if self.segmentController.selectedSegmentIndex == 0 {
            
            if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                self.activity.startAnimating()
            }
            
        }else{
            
            if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                self.activity.startAnimating()
            }
        }
        isLoading = true
        self.lblNoRecord.isHidden = true
        objWebserviceManager.requestPost(strURL:  WebURL.getNotificationList, params: parameters as [String : AnyObject] , success: { response in
            print(response)
            objWebserviceManager.StopIndicator()
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            self.isLoading = false
            if self.segmentController.selectedSegmentIndex == 0 {
                
                if self.pageNo == 0 {
                    self.arrBookingNotificationData.removeAll()
                    self.arrSocialNotificationData.removeAll()
                    self.tblPastBooking.reloadData()
                }
            }else{
                
                if self.pageNo2 == 0 {
                 self.arrSocialNotificationData.removeAll()
                    self.tblFutureBooking.reloadData()
                }
            }
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                
                if strStatus == k_success{
                    
                    if self.segmentController.selectedSegmentIndex == 0 {
                        self.arrSocialNotificationData.removeAll()
                        self.arrBookingNotificationData.removeAll()
                        if let totalCount = response["total"] as? Int{
                            self.totalCount = totalCount
                        }
                        if let arr = response["notificationList"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = NotificationData.init(dict: dict)
                                //obj.date =  self.getFormattedBookingDateToShowFrom(strDate: obj.date)
                                obj.messageAtt = obj.message.highlightWordsIn(highlightedWords: obj.userName, attributes:  self.attributes)
                                
                                if self.isSocial{
                                    if obj.type == "social"{
                                        self.arrSocialNotificationData.append(obj)
                                    }
                                }else{
                                    if obj.type == "booking"{
                                        self.arrBookingNotificationData.append(obj)
                                    }
                                }
                            }
                        }
                        if self.isSocial{
                            self.totalCount =  self.arrSocialNotificationData.count
                        }else{
                            self.totalCount = self.arrBookingNotificationData.count
                        }
                    }else{
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount2 = totalCount
                        }
                        if let arr = response["notificationList"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = NotificationData.init(dict: dict)
                                //obj.date =  self.getFormattedBookingDateToShowFrom(strDate: obj.date)
                                obj.messageAtt = obj.message.highlightWordsIn(highlightedWords: obj.userName, attributes:  self.attributes)
                                self.arrSocialNotificationData.append(obj)
                            }
                        }
                    }
                }else{
                    
                    if strStatus == "fail"{
                        
                    }else{
                        
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }
            
            if self.segmentController.selectedSegmentIndex == 0 {
                self.tblPastBooking.reloadData()
                self.tblPastBooking.contentOffset.y = 0.0
                if self.isSocial{
                    if self.arrSocialNotificationData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else{
                    if self.arrBookingNotificationData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrSocialNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            
        }) { (error) in
            self.isLoading = false
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            
            if self.segmentController.selectedSegmentIndex == 0 {
                self.tblPastBooking.reloadData()
                if self.arrBookingNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrSocialNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - custome method extension
extension NotificationVC {
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        if strDate == "" {
            return ""
        }
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
    
    
    
}

extension NotificationVC : ShowStoriesVCDelegate {
    
    
    
    func redirectNotificationWith(obj : NotificationData) {
        
        var notifincationType : String = ""
        
        switch obj.notifincationType {
            
        case 1:
            //var body = 'sent a booking request.';
            //var title = 'Booking request';
            notifincationType = "booking" //Artist side
            break;
            
        case 2:
            //var body = 'accepted your booking request.'
            //var title = 'Booking accept';
            notifincationType = "booking" //->Checked
            break;
            
        case 3:
            //var body = 'rejected your booking request.';
            //var title = 'Booking reject';
            notifincationType = "booking" //->Checked
            break;
            
        case 4:
            //var body = 'cancelled your booking request.';
            //var title = 'Booking cancel';
            notifincationType = "booking" //->Checked
            break;
            
        case 5:
            //var body = 'completed your booking request.';
            //var title = 'Booking complete';
            notifincationType = "booking"
            break;
            
        case 6:
            //var body = 'given review for booking.';
            //var title = 'Booking Review';
            notifincationType = "booking"
            break;
            
        case 7:
            //var body = 'added a new post.';
            //var title = 'new post';
            notifincationType = "feed" //->Checked
            break;
            
        case 8:
            //var body = 'Payment has completed by';
            //var title = 'Payment';
            notifincationType = "booking" //Artist side //on back open payment history
            break;
            
        case 9:
            //var body = 'commented on your post.';
            //var title = 'Comment';
            notifincationType = "feed" //->Checked
            break;
            
        case 10:
            //var body = 'likes your post.';
            //var title = 'Post like';
            notifincationType = "feed" //->Checked
            break;
            
        case 11:
            //var body = 'likes your comment.';
            //var title = 'Comment like';
            notifincationType = "feed" //->Checked
            break;
            
        case 12:
            //var body = 'started following you.';
            //var title = 'Following';
            notifincationType = "profile" //->Checked
            break;
            
        case 13:
            //var body = 'added to their story.';
            //var title = 'Story';
            notifincationType = "story" // open story //->Checked
            break;
            
        case 14:
            //var body = 'added you as a favorite.';
            //var title = 'Story';
            obj.notifyId = obj.senderId
            notifincationType = "profile" //Artist side
            break;
            
        case 15:
            //var body = 'chat.';
            //var title = 'chat';
            notifincationType = "chat"
            break;
            
        case 16:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            notifincationType = "feed" //->Checked
            break;
        
        case 31:
            notifincationType = "booking"
            break;
        
        default:
            print("default : No condition matched")
        }
        
        
        //notificationType : "booking","feed","profile","chat","story"
        guard notifincationType != ""  else {
            return
        }
        
        
        if notifincationType == "booking" {
            
            let objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
            
            if let notifyId = obj.notifyId {
                objPastFutureBookingData._id = notifyId
                objPastFutureBookingData.userName = obj.userName
                objPastFutureBookingData.profileImage = obj.profileImage
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }
            
        }else if notifincationType == "feed" {
            objAppShareData.notificationInfoDict?.removeAll()
            if let notifyId = obj.notifyId {
                objAppShareData.isFromNotification = true
                let dict = ["notifyId":notifyId]
                objAppShareData.notificationInfoDict = dict
                self.gotoExpPostDetailVC(objFeeds:nil)
            }
            
        }else if notifincationType == "story" {
            
            objAppShareData.isFromNotification = true
            
            if let objUser = storyUser.init(dict: ["":""]) {
                
                if let notifyId = obj.senderId {
                    
                    objUser.profileImage = obj.profileImage
                    objUser.userName = obj.userName
                    objUser._id = notifyId
                    
                    let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
                    if let objShowStories = sb.instantiateViewController(withIdentifier: "ShowStoriesVC") as? ShowStoriesVC{
                        
                        objShowStories.objStoryUser = objUser
                        objShowStories.delegate = self
                        objShowStories.isMyStories = false
                        objShowStories.arrUsres.append(objUser)
                        objShowStories.selectedIndex = 0
                        objShowStories.myStoryCount = 0
                        
                        if let nav =  UIApplication.shared.keyWindow?.rootViewController{
                            objShowStories.modalPresentationStyle = .fullScreen
                            nav.present(objShowStories, animated: true, completion: nil)
                        }
                    }
                }
            }
        }else if notifincationType == "profile" {
            
            if let notifyId  = obj.notifyId {
                objAppShareData.selectedOtherIdForProfile  = notifyId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = obj.userType
                self.gotoProfileVC()
                return
            }
        }
        
    }
    
    func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingDetailVC") as? BookingDetailVC {
            //objVC.objPastFutureBookingData = obj
            objVC.strBookingId = String(obj._id)
            objVC.hidesBottomBarWhenPushed = true
            if objAppShareData.isFromNotification {
                navigationController?.pushViewController(objVC, animated: false)
            }else{
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func gotoExpPostDetailVC(objFeeds:feeds?){
        
        let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
            objAppShareData.arrFeedsForArtistData.removeAll()
            if let objFeeds = objFeeds {
                objVC.objFeeds = objFeeds
                objAppShareData.arrFeedsForArtistData.append(objFeeds)
                navigationController?.pushViewController(objVC, animated: true)
            }else{
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func recall() {
    }
    
    @IBAction func btnProfileListAction(_ sender: UIButton){
        //let section = 0
        //let row = (sender as AnyObject).tag
        //let indexPath = IndexPath(row: row!, section: section)
        //let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":(sender.accessibilityHint)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": sender.accessibilityHint ?? ""
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = 0
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myId = userInfo["_id"] as? Int ?? 0
            }
            if myId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func manageBookingRequestCount(){
        var  idUser = 0
        self.viewBusinessInvitationCount.isHidden = true
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            idUser = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            idUser = userInfo["_id"] as? Int ?? 0
        }
      var counts = 0
        ref.child("socialBookingBadgeCount").child(String(idUser)).observe(.value, with: { (snapshot) in
            let dict = snapshot.value as? [String:Any]
            if let count = dict?["businessInvitationCount"] as? Int {
                self.lblBusinessInvitationCount.text = String(count)
                counts = count
            }else if let count = dict?["businessInvitationCount"] as? String {
                self.lblBusinessInvitationCount.text = String(count)
                counts = Int(count) ?? 0
            }
            if counts > 0{
                //self.viewBusinessInvitationCount.isHidden = false
                self.viewBusinessInvitationCount.isHidden = true
            }else{
                self.viewBusinessInvitationCount.isHidden = true
            }
           
        })
        if counts > 0{
            //self.viewBusinessInvitationCount.isHidden = false
            self.viewBusinessInvitationCount.isHidden = true
        }else{
            self.viewBusinessInvitationCount.isHidden = true
        }
    }
}




//MARK: - Up down Swipe
fileprivate extension NotificationVC{
    func addGesturesToView() -> Void {
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRight))
        swipeRight.direction = .right
        self.viewPastBooking.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeft))
        swipeLeft.direction = .left
        self.viewPastBooking.addGestureRecognizer(swipeLeft)
        
    }
    
    @objc func leftToRight() ->Void {
        changeSegment(sender: 0)
    }
    @objc func rightToLeft() ->Void {
        changeSegment(sender: 1)
    }
    
    @IBAction func showScrollingNCForOptionDots() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Notifications"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        objAppShareData.arrTBottomSheetModal = ["Clear All Completed"]
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if str == "0"{
               strongSelf.clearAllCompletedNotifications()
            }
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Notifications"
       
        objAppShareData.arrTBottomSheetModal = ["Clear All Completed"]
      
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if str == "0"{
               strongSelf.clearAllCompletedNotifications()
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
    func clearAllCompletedNotifications(){
        self.webServiceCallForClearAllCompletedNotification()
    }
    
    func webServiceCallForClearAllCompletedNotification(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var userType = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let type = dict["userType"] as? String {
                userType = type
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let type = userInfo["userType"] as? String {
                userType = type
            }
        }
        var parameters = [String:Any]()
        parameters = [
            "userId":strUserId,
            "userType":userType,
        ]
        isLoading = true
        self.activity.startAnimating()
        objWebserviceManager.requestPost(strURL:  WebURL.notificationClear, params: parameters as [String : AnyObject] , success: { response in
            print(response)
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    if !self.isSocial{
                        self.getNotificationDataWith(0)
                    }else{
                        self.isLoading = false
                        self.activity.stopAnimating()
                    }
                }else{
                    self.isLoading = false
                    self.activity.stopAnimating()
                    if strStatus == "fail"{
                    }else{
                        if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }
        }) { (error) in
            self.isLoading = false
            self.activity.stopAnimating()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
