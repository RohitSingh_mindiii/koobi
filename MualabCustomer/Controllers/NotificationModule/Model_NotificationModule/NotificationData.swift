//
//  NotificationData.swift
//  MualabCustomer
//
//  Created by Mac on 12/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class NotificationData {
    
    var _id : Int = 0
    var senderId : Int?
    
    var firstName : String = ""
    var lastName : String = ""
    var message : String = ""
    var messageAtt : NSMutableAttributedString = NSMutableAttributedString(string:"")
    var notifincationType : Int = 0
    var profileImage : String = ""
    var readStatus : String = ""
    var userName : String = ""
    var date : String = ""
    var type : String = ""
    var notifyId : Int?
    var userType : String = ""
    var strTimeAgo : String = ""

    init(dict : [String : Any]){
        
        _id = dict["_id"] as? Int ?? 0
       // senderId = dict["senderId"] as? Int ?? 0
        firstName = dict["firstName"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        message = dict["message"] as? String ?? ""
        
        profileImage = dict["profileImage"] as? String ?? ""
        //readStatus = dict["readStatus"] as? Int ?? ""
        if let status = dict["readStatus"] as? Int{
            readStatus = String(status)
        }else if let status = dict["readStatus"] as? String{
            readStatus = status
        }
        
        let strDate = dict["crd"] as? String ?? ""
        let dateNew = objAppShareData.getDateFromCRD(strDate: strDate)
        strTimeAgo = objChatShareData.timeAgoSinceDateForNotificationList(date: dateNew as NSDate, numericDates: false)
        
        userName = dict["userName"] as? String ?? ""
        userType = dict["userType"] as? String ?? ""
        date = dict["timeElapsed"] as? String ?? ""
        type = dict["type"] as? String ?? ""
        
        if let notiId  = dict["notifyId"] as? Int{
            notifyId = notiId
        }else{
            if let notiId  = dict["notifyId"] as? String{
                notifyId = Int(notiId)
            }
        }
        
        if let notiId  = dict["senderId"] as? Int{
            senderId = notiId
        }else{
            if let notiId  = dict["senderId"] as? String{
                senderId = Int(notiId)
            }
        }
        
        if let notiType  = dict["notifincationType"] as? Int{
            notifincationType = notiType
        }else{
            if let notiType  = dict["notifincationType"] as? String{
                notifincationType = Int(notiType) ?? 0
            }
        }
    }
}


