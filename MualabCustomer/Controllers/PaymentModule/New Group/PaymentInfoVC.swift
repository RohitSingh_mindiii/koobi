//
//  CourierPaymentInfoVC.swift
//  SampleSwiftDemo
//
//  Created by MINDIII on 1/2/18.
//  Copyright © 2018 Amit. All rights reserved.
//

import UIKit

class PaymentInfoVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var objPastFutureBookingData : PastFutureBookingData = PastFutureBookingData.init(dict: ["":""])
    
    @IBOutlet weak var btnPayCard: UIButton!
    @IBOutlet weak var btnPayBank: UIButton!
    @IBOutlet weak var outerView: UIView!
    
    // TextField
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtAccountNumber: UITextField!
    @IBOutlet weak var txtRoutingNumber: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var viewMainForBank: UIView!
    @IBOutlet weak var viewMainForCard: UIView!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtSSNLast: UITextField!
    //View
    @IBOutlet weak var viewForDate: UIView!
    @IBOutlet weak var viewForDateCorner: UIView!
    
    // Picker
    @IBOutlet weak var datePicker: UIDatePicker!
    //Button
    @IBOutlet weak var btnAddCard: UIButton!
    // String
    var strBankPostid : String = ""
    var strRequestIdForPayment : String = ""
    var strSelectedPickerType = ""
    var strSelectedCityName = ""
    var strSelectedStateName = ""
    var CITYNAME = "CityName"
    var strDOB : String = ""
    var strChangeing : String = ""
    
    // Array
    var arrCityName = [AnyHashable]()
    var arrSortCityName = [AnyHashable]()
    @IBOutlet weak var picker_monthYear: UIPickerView!
    
   //// For Card
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var txtCardCVV: UITextField!
    
    var txtFieldSelected: PhoneNoTextField?
    @IBOutlet weak var txtCardNumber1: PhoneNoTextField!
    @IBOutlet weak var txtCardNumber2: PhoneNoTextField!
    @IBOutlet weak var txtCardNumber3: PhoneNoTextField!
    @IBOutlet weak var txtCardNumber4: PhoneNoTextField!
    
    var MONTH = 0
    var YEAR = 1
    var months = [Any]()
    var years = [Any]()
    var selectedSegmentIndex: Int = 0
    var minYear: Int = 0
    var maxYear: Int = 0
    var rowHeight: Int = 0
    @IBOutlet weak var viewDatMonth: UIView!
    @IBOutlet weak var constraintsPickerBottom: NSLayoutConstraint!
    var selectedMonthName = ""
    var selectedyearName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiUpdate()
        loadDefaultsParameters()
        addAccesorryToKeyBoard()
        addGesture()
        viewForDateCorner.layer.cornerRadius = 8
        viewForDateCorner.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewDatMonth.isHidden=true        
        let doublePrice = Double(objPastFutureBookingData.totalPrice) ?? 0
        let doubleStr = String(format: "%.2f", (doublePrice * 100)/100)
        let strTotalPrice = "Pay £" + doubleStr
        btnPayCard.setTitle(strTotalPrice, for: .normal)
        btnPayBank.setTitle(strTotalPrice, for: .normal)
    }
    
    // MARK:- IBActions Buttons
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        self.view.endEditing(true)
        if sender.selectedSegmentIndex == 0{
           sender.selectedSegmentIndex = 0
           self.selectedSegmentIndex = 0
           self.viewMainForCard.isHidden = false
           self.viewMainForBank.isHidden = true
            self.txtFirstName.text = ""
            self.txtPostalCode.text = ""
            self.txtSSNLast.text = ""
            self.txtRoutingNumber.text = ""
            self.txtAccountNumber.text = ""
        }else if sender.selectedSegmentIndex == 1{
            sender.selectedSegmentIndex = 1
            self.selectedSegmentIndex = 1
            self.viewMainForCard.isHidden = true
            self.viewMainForBank.isHidden = false
            self.txtCardNumber1.text = ""
            self.txtCardNumber2.text = ""
            self.txtCardNumber3.text = ""
            self.txtCardNumber4.text = ""
            self.txtExpiryDate.text = ""
            self.txtCardCVV.text = ""
        }
    }
   
    @IBAction func btnPayNowAction(_ sender: UIButton) {
        view.endEditing(true)
        if selectedSegmentIndex == 0{
            
            if (self.txtCardNumber1.text?.count)!  == 0  || (self.txtCardNumber2.text?.count)!  == 0  || (self.txtCardNumber3.text?.count)!  == 0  || (self.txtCardNumber4.text?.count)!  == 0 {
                
                
             objAppShareData.showAlert(withMessage: "Please fill all required fields", type: alertType.bannerDark,on: self)
                
            }else if (self.txtCardNumber1.text?.count)!<4 || (self.txtCardNumber2.text?.count)!<4 || (self.txtCardNumber3.text?.count)!<4 || (self.txtCardNumber4.text?.count)!<4{
              
                
                objAppShareData.showAlert(withMessage: "Incorrect card number", type: alertType.bannerDark,on: self)
                
            }else if (self.txtExpiryDate.text?.count)! == 0{
                
                objAppShareData.showAlert(withMessage: "Please enter expiry date", type: alertType.bannerDark,on: self)
            
            }else if (self.txtCardCVV.text?.count)! == 0 {
                
                objAppShareData.showAlert(withMessage: "Please fill all required fields", type: alertType.bannerDark,on: self)
               
            }else if (self.txtCardCVV.text?.count)! < 3 {
                
                 objAppShareData.showAlert(withMessage: "Incorrect CVV number", type: alertType.bannerDark,on: self)
                //objAlert.showAlert(message: "Incorrect CVV number", controller: self)
            }else{
                self.callWebServiceFor_Payment()
            }
        }else{
           if (self.txtFirstName.text?.count)! == 0 ||
             (self.txtAccountNumber.text?.count)! == 0 || (self.txtRoutingNumber.text?.count)! == 0 {
            
            //(self.txtSSNLast.text?.count)! == 0 || (self.txtPostalCode.text?.count)! == 0
            
               objAppShareData.showAlert(withMessage: "Please fill all required fields", type: alertType.bannerDark,on: self)
           
           }else{
              self.callWebServiceFor_Payment()
           }
        }
    }
    @IBAction func btnPickerDoneAction(_ sender: UIButton) {
        view.endEditing(true)
        self.viewDatMonth.isHidden = true
        if selectedMonthName.count > 0 && selectedyearName.count > 0{
            let str = "\(selectedMonthName)/\(selectedyearName)"
            txtExpiryDate.text = str
        }
    }
    func nameOfMonths() -> [Any] {
        return ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    }
    
    func nameOfYears() -> [Any] {
        var years = [AnyHashable]()
        for year in minYear...maxYear {
            let yearStr = "\(Int(year))"
            years.append(yearStr)
        }
        return years
    }
    func loadDefaultsParameters() {
        let components: DateComponents? = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        let year: Int? = components?.year
        minYear = year!
        maxYear = year! + 30
        rowHeight = 44
        months = nameOfMonths()
        years = nameOfYears()
        picker_monthYear.delegate = self
        picker_monthYear.dataSource = self
        let str = "\(Int(year!))"
        if (str.count ) > 2 {
            let strLastTwoDigits: String? = (str as? NSString)?.substring(from: (str.count ) - 2)
            selectedyearName = strLastTwoDigits!
        }
        selectedMonthName = "01"
    }
    @IBAction func btnPickerCancelAction(_ sender: UIButton) {
        view.endEditing(true)
        self.viewDatMonth.isHidden = true
    }
    @IBAction func btnForBack(_ sender: Any) {
        self.view .endEditing(true)
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnForAddBankAccount(_ sender: Any) {
        Validation()
    }
    @IBAction func btnExpiryDateAction(_ sender: Any) {
        self.view.endEditing(true)
        picker_monthYear.reloadAllComponents()
        self.viewDatMonth.isHidden = false
    }
    @IBAction func btnForDOB(_ sender: Any) {
        view.endEditing(true)
    }
    @IBAction func btnForDateDone(_ sender: Any){
        datePicker.datePickerMode = .date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate = dateFormatter.string(from: datePicker.date)
        // For Server
        dateFormatter.dateFormat = "yyyy-MM-dd"
        strDOB = dateFormatter.string(from: datePicker.date)
    }
    
    @IBAction func btnForDatePickerChangeValue(_ sender: Any)
    {
        let Cdate = Date()
        let Cformatter = DateFormatter()
        Cformatter.dateFormat = "dd-MM-yyyy"
        let result = Cformatter.string(from: Cdate)
        datePicker.datePickerMode = .date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        datePicker.maximumDate = dateFormatter.date(from: result)
    }
    
    @IBAction func btnCityAction(_ sender: Any) {
        strSelectedPickerType = CITYNAME
        showPicker()
    }
    
    // MARK:- Dropdown methods
    func showPicker(){
        view.endEditing(true)
        picker_monthYear.reloadAllComponents()
    }
    
    // MARK:- Picker View Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == MONTH {
            return months.count
        }
        else {
            return years.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if component == MONTH {
            let monthName: String = months[row] as? String ?? ""
            return monthName
        }
        else {
            let yearName: String = years[row] as? String ?? ""
            let str = "\(yearName)"
            return str
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == self.MONTH {
            selectedMonthName = months[row] as? String ?? ""
        }
        else {
            let str = "\(years[row])"
            if (str.count ) > 2 {
                let strLastTwoDigits: String! = (str as? NSString)?.substring(from: (str.count ) - 2)
                selectedyearName = strLastTwoDigits!
            }
        }
    }
    
    
    // MARK:- Local Methods
    
    func uiUpdate(){
        
        outerView.layer.cornerRadius = 10
        outerView.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
        outerView.layer.borderWidth = 1
        outerView.clipsToBounds = true
        
        self.txtCardNumber1.layer.cornerRadius = 4
        self.txtCardNumber1.layer.borderWidth = 1.0
        self.txtCardNumber1.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtCardNumber1.clipsToBounds = true
       
        self.txtCardNumber2.layer.cornerRadius = 4
        self.txtCardNumber2.layer.borderWidth = 1.0
        self.txtCardNumber2.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtCardNumber2.clipsToBounds = true
        
        self.txtCardNumber3.layer.cornerRadius = 4
        self.txtCardNumber3.layer.borderWidth = 1.0
        self.txtCardNumber3.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtCardNumber3.clipsToBounds = true
        
        self.txtCardNumber4.layer.cornerRadius = 4
        self.txtCardNumber4.layer.borderWidth = 1.0
        self.txtCardNumber4.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtCardNumber4.clipsToBounds = true
       
        self.txtExpiryDate.layer.cornerRadius = 4
        self.txtExpiryDate.layer.borderWidth = 1.0
        self.txtExpiryDate.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtExpiryDate.clipsToBounds = true
        
        self.txtCardCVV.layer.cornerRadius = 4
        self.txtCardCVV.layer.borderWidth = 1.0
        self.txtCardCVV.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtCardCVV.clipsToBounds = true
        
        self.txtAccountNumber.layer.cornerRadius = 4
        self.txtAccountNumber.layer.borderWidth = 1.0
        self.txtAccountNumber.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtAccountNumber.clipsToBounds = true
       
        self.txtRoutingNumber.layer.cornerRadius = 4
        self.txtRoutingNumber.layer.borderWidth = 1.0
        self.txtRoutingNumber.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColorr
        self.txtRoutingNumber.clipsToBounds = true
        
        self.txtSSNLast.layer.cornerRadius = 4
        self.txtSSNLast.layer.borderWidth = 1.0
        self.txtSSNLast.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtSSNLast.clipsToBounds = true
        
        self.txtPostalCode.layer.cornerRadius = 4
        self.txtPostalCode.layer.borderWidth = 1.0
        self.txtPostalCode.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtPostalCode.clipsToBounds = true
        
        self.txtFirstName.layer.cornerRadius = 4
        self.txtFirstName.layer.borderWidth = 1.0
        self.txtFirstName.layer.borderColor = UIColor.gray.cgColor //colorTheame.cgColor
        self.txtFirstName.clipsToBounds = true
        
        selectedSegmentIndex = 0
        self.viewMainForBank.isHidden = true
        self.viewMainForCard.isHidden = false
        arrCityName = [AnyHashable]()
        
//        let attr = NSDictionary(object: UIFont(name: "Lato-Regular", size: 17.0)!, forKey: NSAttributedStringKey.font as NSCopying)
//
//        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)
        
    }
    
    func addGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PaymentInfoVC.gestureHandlerMethod))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func gestureHandlerMethod() {
        view.endEditing(true)
    }
    
    func Validation(){
        
        if (txtFirstName.text?.isEmpty)!{
            
           // objAppShareData.showAlert(withMessage: "First name can't be empty", type: alertType.bannerDark,on: self)
           // objAlert.showAlert(message: "First name can't be empty" , title: "Alert", controller: self)
        }else if (txtAccountNumber.text?.isEmpty)!{
           
            objAppShareData.showAlert(withMessage: "Account number can't be empty", type: alertType.bannerDark,on: self)
           // objAlert.showAlert(message: "Account number can't be empty" , title: "Alert", controller: self)
        }else if (txtAccountNumber.text?.count)! < 9 || (txtAccountNumber.text?.count)! > 16
        {
            objAppShareData.showAlert(withMessage: "Please enter valid account number", type: alertType.bannerDark,on: self)
            
            //objAlert.showAlert(message: "Please enter valid account number" , title: "Alert", controller: self)
        }else if (txtPostalCode.text?.isEmpty)!{
            
            //objAppShareData.showAlert(withMessage: "Postal code can't be empty", type: alertType.bannerDark,on: self)
            
            //objAlert.showAlert(message: "Postal code can't be empty" , title: "Alert", controller: self)
        }else if (txtSSNLast.text?.isEmpty)!
        {
           // objAppShareData.showAlert(withMessage: "SSN_Last can't be empty", type: alertType.bannerDark,on: self)
            
            //objAlert.showAlert(message: "SSN_Last can't be empty" , title:  "Alert", controller: self)
        }else if (txtRoutingNumber.text?.isEmpty)!{
            
            objAppShareData.showAlert(withMessage: "Routing number can't be empty", type: alertType.bannerDark,on: self)
            //objAlert.showAlert(message: "Routing number can't be empty" , title: "Alert", controller: self)
        }else{
            //callWebServiceFor_GetAddCard()
        }
    }
    
    //Get Country Code -
    func getCityNames() {
        
        let fileURLProject = Bundle.main.path(forResource: "us-1", ofType: "json")
        var readStringProject = ""
        
        do {
            readStringProject = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)
            let countryData: NSData = readStringProject.data(using: String.Encoding.utf8)! as NSData
            let dictData: NSDictionary = try! JSONSerialization.jsonObject(with: countryData as Data, options: []) as! NSDictionary
            arrCityName = (dictData["city"] as? [Any])! as! [AnyHashable]
            for (index,element) in (arrCityName.enumerated()) {
            }
            
            let swiftArray = arrCityName as AnyObject as! [String]
            
            arrSortCityName = swiftArray.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
        } catch let error as NSError {
        }
    }
    
    
    // MARK:- TextFiled Delegate
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        var searchString: String? = nil
        var newLength:Int = 0
        if textField == txtCardNumber1
        {
            txtFieldSelected = txtCardNumber1
            if (string.count ) != 0 {
                searchString = txtCardNumber1.text! + (string).uppercased()
                newLength = (self.txtCardNumber1.text?.count)! + string.count - range.length
            }else {
                searchString = (txtCardNumber1.text as NSString?)?.substring(to: (txtCardNumber1.text?.count)! - 1).uppercased()
            }
            if (searchString?.count ?? 0) == 4 {
                txtCardNumber1.text = searchString
                searchString = nil
                txtCardNumber2.becomeFirstResponder()
                txtCardNumber2.text = ""
                return false
            }
            else {
                
            }
            
            return newLength<=4
            
        }
        else if textField == txtCardNumber2
        {
            txtFieldSelected = txtCardNumber2
            
            if (string.count ) != 0
                
            {
                searchString = txtCardNumber2.text! + (string).uppercased()
                newLength = (self.txtCardNumber2.text?.count)! + string.count - range.length
            }
            else {
                searchString = (txtCardNumber2.text as NSString?)?.substring(to: (txtCardNumber2.text?.count)! - 1).uppercased()
            }
            if searchString?.count == 4 {
                txtCardNumber2.text = searchString
                searchString = nil
                txtCardNumber3.becomeFirstResponder()
                txtCardNumber3.text = ""
                return false
            }
            else {
                
            }
            
            return newLength<=4
            
        }
        else  if textField == txtCardNumber3 {
            txtFieldSelected = txtCardNumber3
            if (string.count ) != 0{
                searchString = txtCardNumber3.text! + (string).uppercased()
                newLength = (self.txtCardNumber3.text?.count)! + string.count - range.length
            }
            else {
                searchString = (txtCardNumber3.text as NSString?)?.substring(to: (txtCardNumber3.text?.count)! - 1).uppercased()
            }
            if searchString?.count == 4 {
                txtCardNumber3.text = searchString
                searchString = nil
                txtCardNumber4.becomeFirstResponder()
                txtCardNumber4.text = ""
                return false
            }
            else {
            }
            return newLength<=4
        }
        else if textField == txtCardNumber4 {
            txtFieldSelected = txtCardNumber4
            if (string.count ) != 0{
                searchString = txtCardNumber4.text! + (string).uppercased()
                newLength = (self.txtCardNumber4.text?.count)! + string.count - range.length
            }
            else {
                searchString = (txtCardNumber4.text as NSString?)?.substring(to: (txtCardNumber4.text?.count)! - 1).uppercased()
            }
            if searchString?.count == 4 {
                txtCardNumber4.text = searchString
                searchString = nil
                txtCardNumber4.endEditing(true)
            }
            else {
            }
            return newLength<=4
        }
        else if textField == txtCardCVV {
            if (string.count ) != 0{
                searchString = txtCardCVV.text! + (string).uppercased()
                newLength = (self.txtCardCVV.text?.count)! + string.count - range.length
            }
            else {
                searchString = (txtCardCVV.text as NSString?)?.substring(to: (txtCardCVV.text?.count)! - 1).uppercased()
            }
            if searchString?.count == 3 {
                txtCardCVV.text = searchString
                searchString = nil
                txtCardCVV.endEditing(true)
            }
            else {
            }
            return newLength<=3
        }
        else {
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == txtExpiryDate {
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtExpiryDate {
            textField.resignFirstResponder()
            resignKeyBoard()
            picker_monthYear.reloadAllComponents()
            self.viewDatMonth.isHidden = false
        }
    }
    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        view.endEditing(true)
        self.viewDatMonth.isHidden = true
    }
    
    // MARK: - Webservices
    func callWebServiceFor_Payment() {
       // KRProgressHUD.show(withMessage: "Please wait...", completion: nil)
       
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objWebserviceManager.StartIndicator()
       
        var url = ""
        var param = [String:Any]()
        
        let strBookingId = "\(objPastFutureBookingData._id)"


        if self.selectedSegmentIndex == 0 {
           
            let strCardNumber = self.txtCardNumber1.text! + self.txtCardNumber2.text! + self.txtCardNumber3.text! + self.txtCardNumber4.text!
            
            url = "cardPayment"
            
            param = [ "cvv":self.txtCardCVV.text!,
                     "number":strCardNumber,
                     "exp_month":selectedMonthName,
                     "exp_year":selectedyearName,
                     "id":strBookingId
                   ] as [String : AnyObject]
        }else{
            
            url = "bankPayment"
          
            param = [   "holderName":self.txtFirstName.text!,
                        "routingnumber":self.txtRoutingNumber.text!,
                        "accountNo":self.txtAccountNumber.text!,
                        //"ssnLast":self.txtSSNLast.text! ,
                        //"postalcode":self.txtPostalCode.text!,
                        "id":strBookingId
                    ] as [String : AnyObject]
        }
        
        objServiceManager.requestGet(strURL: url, params: param , success: { (response) in
            
            objWebserviceManager.StopIndicator()
            let status = (response["status"] as? String)
            if status == "success"{
                
                self.showSuccessAlert()
                
            }else{
                
                var strMsg = ""
                if let msg = response["message"] as?  String {
                     strMsg = msg
                }else{
                    var strMsg = "You might enter some wrong info, please check below possibilities : "
                    if self.selectedSegmentIndex == 0{
                        strMsg = strMsg + "Card no., CVV, Expiry date"
                    }else{
                        strMsg = strMsg + "Account no., Routing no."
                    }
                }
                
                objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
            }
            
        }) { (error) in
           
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.callWebServiceFor_Payment()
            }else{
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    
    func showSuccessAlert() {
        
        let alert = UIAlertController(title: "Success", message: "Payment completed successfully..!", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callWebserviceForList"), object: nil)
            
            // self.gotoPaymentHistoryVC()
            self.navigationController?.popViewController(animated: true)
            //self.navigationController?.popToRootViewController(animated: true)
        })
        alert.addAction(yesButton)
        present(alert, animated: true) {() -> Void in }
    }
}

//MARK:- phonepad return keyboard
extension PaymentInfoVC{
   
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        txtAccountNumber.inputAccessoryView = keyboardDoneButtonView
        txtRoutingNumber.inputAccessoryView = keyboardDoneButtonView
        txtPostalCode.inputAccessoryView = keyboardDoneButtonView
        txtSSNLast.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func resignKeyBoard(){
        txtAccountNumber.endEditing(true)
        txtRoutingNumber.endEditing(true)
        txtPostalCode.endEditing(true)
        txtSSNLast.endEditing(true)
        txtCardNumber1.endEditing(true)
        txtCardNumber2.endEditing(true)
        txtCardNumber3.endEditing(true)
        txtCardNumber4.endEditing(true)
        txtCardCVV.endEditing(true)
        txtExpiryDate.endEditing(true)
    }
    
    func gotoPaymentHistoryVC(){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentHistoryVC") as? PaymentHistoryVC {
            objAppShareData.isFromPaymentScreen = true
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}
