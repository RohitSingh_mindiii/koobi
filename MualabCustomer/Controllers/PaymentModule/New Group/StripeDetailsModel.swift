//
//  CustomerMyPostRequestModelClass.swift
//  SampleSwiftDemo
//
//  Created by MINDIII on 1/4/18.
//  Copyright © 2018 Amit. All rights reserved.
//

import UIKit

class StripeDetailsModel: NSObject {
    
    var strlast4: String?
    var strbrand: String?
    var strexpMonth: Int
    var strexpYear: Int
    var strid: String?
    var strcustomer: String?
    var strclick: String?
    var strname = ""
    var strjobid = ""
    var strbidid = ""
    var strserviceProviderid = ""
    init(dict:Dictionary<String, Any>) {
        
        if let last4 = dict["last4"] as? String{
            strlast4 = last4
        }else{
            strlast4 = ""
        }
        
        if let brand = dict["brand"] as? String{
            strbrand = brand
        }else{
            strbrand = ""
        }
        
        if let expMonth = dict["exp_month"] as? Int{
            strexpMonth = expMonth
        }else{
            strexpMonth = 0
        }
        
        if let expYear = dict["exp_year"] as? Int{
            strexpYear = expYear
        }else{
            strexpYear = 0
        }
        
        
        if let id = dict["id"] as? String{
            strid = id
        }else{
            strid = ""
        }
        
        if let customer = dict["customer"] as? String{
            strcustomer = customer
        }else{
            strcustomer = ""
        }
        
        if let last4 = dict["click"] as? String{
            strclick = last4
        }else{
            strclick = "0"
        }
        if let name = dict["name"] as? String{
            strname = name
        
    }
        if let jobId = dict["jobId"] as? String{
            strjobid = jobId
            
        }
        if let bidId = dict["bidId"] as? String{
            strbidid = bidId
            
        }
       
        if let service_provider_id = dict["service_provider_id"] as? String{
            strserviceProviderid = service_provider_id
            
        }
        
    }
}
