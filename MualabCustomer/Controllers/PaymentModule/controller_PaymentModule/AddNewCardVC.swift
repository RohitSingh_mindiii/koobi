//
//  AddNewCardVC.swift
//  HourLabors
//
//  Created by Mindiii on 12/18/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Stripe
class AddNewCardVC: UIViewController,UIGestureRecognizerDelegate,UITextFieldDelegate {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var viewButton: customView!
    @IBOutlet weak var ViewAddCardTop: NSLayoutConstraint!
    @IBOutlet weak var viewTotalAmount: UIView!
    @IBOutlet weak var btnpay: UIButton!
    @IBOutlet weak var btnExpiryDate: UIButton!
    @IBOutlet weak var viewAddCard: UIView!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var pickerExpiryDate: UIPickerView!
    @IBOutlet weak var txtCVC: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var imgSave: UIImageView!
    @IBOutlet weak var btnSaveCard: UIButton!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var lblForHide: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblBigText: UILabel!
    var strBookingId: String?
    var isFromSlider: Bool = false
    var isForDetail: Bool = false
    var strDefaultCardId = ""
    var objCardDetail = StripeDetailsModel.init(dict: [:])
    @IBOutlet weak var viewSaveCard: UIView!
//    var isYearSelected = false
//    var isMonthSelected = false
//    var arrMonth = ["01","02","03","04","05","06","07","08","09","10","11","12"]
//    var arrYear = [Int]()
    var MONTH = 0
    var YEAR = 1
    var months = [Any]()
    var years = [Any]()
    var minYear: Int = 0
    var maxYear: Int = 0
    var selectedMonth = ""
    var selectedYear = ""
    var selectedMonthName = ""
    var selectedyearName = ""
    var strStripeCustomerId = ""
    var strSourceId = ""
    var isfrompay : Bool = false
    var strjobid = ""
    var strbidid = ""
    var strserviceProviderid = ""
    var strbidAmount = ""
    var arrAllCardList = [StripeDetailsModel]()
    var selectedIndex: Int = -1
    var strcardid: String?
    var strIdStrip = ""
    var stripeToken = ""
    var strSaveData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.strDefaultCardId = UserDefaults.standard.value(forKey: UserDefaults.keys.stripeCardId) as? String ?? ""
        self.lblHeader.text = "Add credit or debit card"
        self.lblBigText.text = "Please enter your credit card details for pay your bill."
        if self.isFromSlider{
            self.lblForHide.isHidden = true
            self.viewSaveCard.isHidden = true
            self.txtCardHolderName.isHidden = true
            self.btnpay.setTitle("Save Card", for: .normal)
            if isForDetail{
                self.lblHeader.text = "Card Detail"
                self.txtCardNumber.text = "xxxx xxxx xxxx " + objCardDetail.strlast4!
               self.txtExpiryDate.text = String(objCardDetail.strexpMonth) + " /" + String(objCardDetail.strexpYear)
                self.txtCVC.text = "***"
                self.txtCardNumber.isUserInteractionEnabled = false
                self.txtExpiryDate.isUserInteractionEnabled = false
                self.btnExpiryDate.isUserInteractionEnabled = false
                self.txtCVC.isUserInteractionEnabled = false
                self.btnpay.setTitle("Remove Card", for: .normal)
                self.lblBigText.text = "Your credit card details for pay your bill."
            }
        }else{
            self.lblHeader.text = "Payment Checkout"
            self.lblForHide.isHidden = false
            self.viewSaveCard.isHidden = false
            self.txtCardHolderName.isHidden = false
            self.btnpay.setTitle("Pay", for: .normal)
        }
        self.viewPicker.isHidden = true
        self.pickerExpiryDate.delegate = self
        self.pickerExpiryDate.dataSource = self
        self.txtExpiryDate.delegate = self
        self.txtCardHolderName.delegate = self
         self.txtCardNumber.delegate = self
         self.txtCVC.delegate = self
         loadDefaultsParameters()
       // self.fillDataInYearArray()
        //self.viewAddCard.LowsetViewShadowWithoutCornerRadius()
        let tap = UITapGestureRecognizer(target: self, action:  #selector(AddNewCardVC.gestureHandlerMethod))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.viewPicker.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.lblAmount.text = "$"+strbidAmount
        let doubleStr = String(format: "%.2f", Double(self.strbidAmount) ?? 0.0)
        //self.txtCardHolderName.text? = self.strbidAmount
        self.txtCardHolderName.text? = doubleStr
        if isfrompay == true {
            strSaveData = "2"
             self.ViewAddCardTop.constant = 0
             self.btnpay.isHidden = false
             //self.viewSaveCard.isHidden = false
             self.viewButton.isHidden = true
        }else{
            strSaveData = ""
            self.ViewAddCardTop.constant = -109
            //self.viewSaveCard.isHidden = true
            self.btnpay.isHidden = true
            self.viewButton.isHidden = false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.viewPicker.isHidden = true
        return true
    }
    
    @IBAction func btnCloseDatePicker(_ sender: Any) {
        self.viewPicker.isHidden = true
    }
    
    @IBAction func btnDone(_ sender: Any) {
//        if isYearSelected == false {
//            self.selectedYear = String(arrYear[0])
//        }else if isMonthSelected == false{
//            self.selectedMonth = self.arrMonth[0]
//        }else{
//        }
//        self.txtExpiryDate.text = "\(self.selectedMonth)/" + self.selectedYear
//
//
        self.view.endEditing(true)

        if selectedMonthName.count > 0 && selectedyearName.count > 0
            {
                let str = "\(selectedMonthName)/\(selectedyearName)"
                self.txtExpiryDate.text = str
            }
        self.viewPicker.isHidden = true
    }
   
    @objc func gestureHandlerMethod() {
        self.view.endEditing(true)
    }
    
    @IBAction func btnDExpireDate(_ sender: UIButton){
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        let calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let currentMonthInt = (calendar?.component(NSCalendar.Unit.month, from: Date()))!
        let currentYearInt = (calendar?.component(NSCalendar.Unit.year, from: Date()))!
        selectedMonthName = String(currentMonthInt)
        self.pickerExpiryDate.selectRow(currentMonthInt-1, inComponent: MONTH, animated: false)
        self.view.endEditing(true)
        self.viewPicker.isHidden = false
    }
    
    @IBAction func btnPay(_ sender: UIButton) {
       if self.btnpay.titleLabel?.text == "Remove Card"{
          self.showDeleteAlertMessage()
          //objAppShareData.showAlert(withMessage: "Under developement", type: alertType.bannerDark,on: self)
       }else{
          self.saveCartDetail()
       }
    }
    
    func showDeleteAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to delete this card?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
            
        }
        
        let action2 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.callWebServiceFor_DeleteCard()
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnSaveCard(_ sender: UIButton) {
        
        if btnSaveCard.isSelected
            {
                self.imgSave.image = #imageLiteral(resourceName: "inactive_select_ico")
                btnSaveCard.isSelected = false
                strSaveData = "2"
        }
        else
        {
            self.imgSave.image = #imageLiteral(resourceName: "active_select_ico")
            btnSaveCard.isSelected = true
            strSaveData = "1"
            
        }
    }
    
    func loadDefaultsParameters() {
        let components: DateComponents? = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        let year: Int? = components?.year
        minYear = year!
        maxYear = year! + 30

        months = nameOfMonths()
        years = nameOfYears()
      
        let str = "\(Int(year!))"
        if (str.count ) > 2 {
            let strLastTwoDigits: String = ((str as? NSString)?.substring(from: (str.count ) - 2))!
            selectedyearName = strLastTwoDigits
        }
        selectedMonthName = "01"
    }
    func nameOfMonths() -> [Any] {
        return ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    }
    
    func nameOfYears() -> [Any] {
        var years = [AnyHashable]()
        for year in minYear...maxYear {
            let yearStr = "\(Int(year))"
            years.append(yearStr)
        }
        return years
    }
    
}
    
//MARk:- extension for button action
extension AddNewCardVC{
    @IBAction func btnBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewCard(_ sender: Any) {
        self.saveCartDetail()
    }
    
}

//MARk:- extension for custom method
extension AddNewCardVC{
    func saveCartDetail(){
        let cardNo = self.txtCardNumber.text ?? "0"
        let cvcNo = self.txtCVC.text ?? "0"
//        if (self.txtCardHolderName.text?.isEmpty)!{
//            //objAppShareData.showAlert(message: "Please enter card holder name", title:"Alert", controller: self)
//            objAppShareData.showAlert(withMessage: "Please enter card holder name", type: alertType.bannerDark,on: self)
//        }else
        if(self.txtCardNumber.text?.isEmpty)!{
            //objAppShareData.showAlert(message: "Please enter card number", title:"Alert", controller: self)
            objAppShareData.showAlert(withMessage: "Please enter card number", type: alertType.bannerDark,on: self)
        }else if cardNo.count < 19{
            //objAppShareData.showAlert(message: "Please enter card number should be of 16 digit", title:"Alert", controller: self)
            objAppShareData.showAlert(withMessage: "Please enter card number should be of 16 digit", type: alertType.bannerDark,on: self)
        }else if(self.txtCVC.text?.isEmpty)!{
            //objAppShareData.showAlert(message: "Please enter CVC", title:"Alert", controller: self)
            objAppShareData.showAlert(withMessage: "Please enter CVC", type: alertType.bannerDark,on: self)
        }else if cvcNo.count < 3{
            //objAppShareData.showAlert(message: "Please enter CVC number should be of 3 digit", title:"Alert", controller: self)
            objAppShareData.showAlert(withMessage: "Please enter CVC number should be of 3 digit", type: alertType.bannerDark,on: self)
        }else if(self.txtExpiryDate.text?.isEmpty)!{
            //objAppShareData.showAlert(message: "Please select expiry date", title:"Alert", controller: self)
            objAppShareData.showAlert(withMessage: "Please select expiry date", type: alertType.bannerDark,on: self)
        }else{
            if self.btnpay.titleLabel?.text == "Pay"{
                print(strSaveData)
                isfrompay = true
                if strSaveData == "1"{
                    addCardAndMakePayment()
                }else if strSaveData == "2" {
                    createStripeTokenAndMakePayment()
                }else{
                    self.addCardAndMakePayment()
                }
            }else{
                isfrompay = false
                self.addCardAndMakePayment()
                //objAppShareData.showAlert(withMessage: "Under developement", type: alertType.bannerDark,on: self)
            }
        }
    }
    
//    func fillDataInYearArray(){
//        self.arrYear.removeAll()
//        let date = Date()
//        let calendar = Calendar.current
//        let components = calendar.dateComponents([.year, .month, .day], from: date)
//        let year =  components.year as? Int
//        for var i in year!..<year! + 30 {
//            self.arrYear.append(i)
//        }
//
//    }
    
    func addCardAndMakePayment(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
            SVProgressHUD.show()
        
        var cardNo = self.txtCardNumber.text ?? ""
        let arrCardNo = cardNo.components(separatedBy: "-")
        
        if arrCardNo.count == 4{
            cardNo = arrCardNo[0]+arrCardNo[1]+arrCardNo[2]+arrCardNo[3]
        }
        
        
            let stripCard = STPCardParams()
            let strCardNo = cardNo
            let expM = self.selectedMonthName//self.txtExpireMonth.text!
            let expY = self.selectedyearName//self.txtExpireYear.text!
            let expMonth = UInt(expM)
            let expYear = UInt(expY)
            stripCard.number = strCardNo
            stripCard.cvc = self.txtCVC.text//self.txtCvv.text!
            stripCard.expMonth = expMonth!
            stripCard.expYear = expYear!
        
            STPAPIClient.shared().createToken(withCard: stripCard) { (token: STPToken?, error: Error?) in
                guard let token = token, error == nil else {
                    // Present error to user...
                    
                    SVProgressHUD.dismiss()
                    //objAppShareData.showAlert(message: error as? String ?? "Your card details are not valid", title: "Alert", controller: self)
                    objAppShareData.showAlert(withMessage: "Your card details are not valid", type: alertType.bannerDark,on: self)
                    return
                }
                print(token.tokenId)
                self.addCardOnStripe(stripeToken: token.tokenId)
        }
    }
    
    func createStripeTokenAndMakePayment(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
            SVProgressHUD.show()
            
            let stripCard = STPCardParams()
        
        var cardNo = self.txtCardNumber.text ?? ""
        let arrCardNo = cardNo.components(separatedBy: "-")
        
        if arrCardNo.count == 4{
            cardNo = arrCardNo[0]+arrCardNo[1]+arrCardNo[2]+arrCardNo[3]
        }
        
          let strCardNo = cardNo
            
            let expM = self.selectedMonthName//self.txtExpireMonth.text!
            let expY = self.selectedyearName//self.txtExpireYear.text!
            let expMonth = UInt(expM)
            let expYear = UInt(expY)
            stripCard.number = strCardNo
            stripCard.cvc = self.txtCVC.text//self.txtCvv.text!
            stripCard.expMonth = expMonth!
            stripCard.expYear = expYear!
            stripCard.name = txtCardHolderName.text!
            
            STPAPIClient.shared().createToken(withCard: stripCard) { (token: STPToken?, error: Error?) in
                guard let token = token, error == nil else {
                    // Present error to user...
                    
                    SVProgressHUD.dismiss()
                    
                    //objAppShareData.showAlert(message: error as? String ?? "Your card details are not valid", title: "Alert", controller: self)
                    objAppShareData.showAlert(withMessage: "Your card details are not valid", type: alertType.bannerDark,on: self)
                    return
                }
                print(token.tokenId)
                self.call_Webservice_Payment(strSourceType: "", strCardId:token.tokenId)
        }
    }
    
    func addCardOnStripe(stripeToken:String){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
            SVProgressHUD.show()
            self.view.endEditing(true)
        let param = ["source":stripeToken] as [String : AnyObject]
        
          if let strStripeCustomerId = UserDefaults.standard.string(forKey:UserDefaults.keys.stripeCustomerId){
                self.strStripeCustomerId = strStripeCustomerId
          }
        
        let url = "https://api.stripe.com/v1/customers" + "/" + self.strStripeCustomerId + "/sources"
        
        objWebserviceManager.requestAddCardOnStripe(strURL: url, params: param, success: { (response) in
                print(response)
            
            if self.strDefaultCardId.count == 0{
                let cardId = response["id"] as? String ?? ""
                //UserDefaults.standard.set(cardId, forKey: UserDefaults.keys.stripeCardId)
                UserDefaults.standard.setValue(cardId, forKey: UserDefaults.keys.stripeCardId)
                UserDefaults.standard.synchronize()
                self.strDefaultCardId = cardId
            }
            
                if self.isfrompay == true {
                    let cardId = response["id"] as? String ?? ""
                     self.call_Webservice_Payment(strSourceType: "card", strCardId: cardId)
                }else{
                self.navigationController?.popViewController(animated: true)
                    objAppShareData.showAlert(withMessage: "Card added succesfully", type: alertType.bannerDark,on: self)
                    self.callWebServiceFor_MakeCardDefault(cardId: self.strDefaultCardId)
                    let alert = UIAlertController(title: "Alert", message: " Card added succesfully", preferredStyle: UIAlertController.Style.alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                    
                    }
                    alert.addAction(okAction)
                    //alert.show()
                    //Create the actions
                self.strSourceId = response["id"] as? String ?? ""
                self.arrAllCardList.removeAll()
                SVProgressHUD.dismiss()
                }
            }) { (error) in
                print(error)
                SVProgressHUD.dismiss()
                let alertView = UIAlertController(title:"Message" , message: "Failure", preferredStyle: .alert)
                alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertView, animated:true, completion:nil)
            }
    }
    
    func callWebServiceFor_MakeCardDefault(cardId:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["cardId":cardId]
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success: { response in
            objActivity.stopActivity()
            if response["status"] as! String == "success"{
                UserDefaults.standard.set(cardId, forKey: UserDefaults.keys.stripeCardId)
                UserDefaults.standard.synchronize()
                self.strDefaultCardId = cardId
            }else{
            }
        }) { error in
            objActivity.stopActivity()
        }
    }
}

//MARk:- extension for pickerview delegate
extension AddNewCardVC : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == MONTH {
            return months.count
        }else{
            return years.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var strYear: String?
        if component == MONTH {
            let monthName: String = months[row] as! String
            return monthName
        }
        else {
            let yearName: String = years[row] as! String
            let str = "\(yearName)"
            return str
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == self.MONTH {
            selectedMonthName = months[row] as! String
        }
        else {
            let str = "\(years[row])"
            if (str.count ) > 2 {
                let strLastTwoDigits: String! = (str as? NSString)?.substring(from: (str.count ) - 2)
                selectedyearName = strLastTwoDigits!
            }
        }
    }
    
}

extension AddNewCardVC{
    func call_Webservice_Payment(strSourceType:String ,strCardId:String) {
        self.view.endEditing(true)
        //        /api/cardPayment
        //        id : ('booking Id')
        //        token
        //        sourceType
        //        customerId
        var cardNo = self.txtCardNumber.text ?? ""
        let arrCardNo = cardNo.components(separatedBy: "-")
        
        if arrCardNo.count == 4{
            cardNo = arrCardNo[0]+arrCardNo[1]+arrCardNo[2]+arrCardNo[3]
        }
        
        let param = [
            "id":strBookingId!,
            "token":strCardId,
            "sourceType":strSourceType,
            "customerId":self.strStripeCustomerId,
            ] as [String : Any]
        
       print(param)
       objServiceManager.requestPostForJson(strURL: WebURL.cardPayment, params: param, success: { response in
            SVProgressHUD.dismiss()
           let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    
                    _ = SweetAlert().showAlert("Payment Successful !", subTitle: "Thank You! Your payment has been received.", style: AlertStyle.success, buttonTitle: "OK", buttonColor: UIColor.theameColors.skyBlueNewTheam, action: { (isClicked) in
                        objAppShareData.isFromPaymentDoneToAppointmentDetail = true
                        self.navigationController?.popViewController(animated: false)
                    })
                }
                //objAppShareData.showAlert(withMessage: "Payment done successfully", type: alertType.bannerDark,on: self)
                //self.Alertshow()
            }else{
                SVProgressHUD.showError(withStatus: message)
                print(status)
            }
          
        }) { (error) in
            print(error)
            let alertView = UIAlertController(title:"Message" , message: "Failure", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertView, animated:true, completion:nil)
        }
 
    }
    

func Alertshow()
{
    let alertController = UIAlertController(title: "Alert", message: "Payment succesfully done", preferredStyle: .alert)
    
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
        UIAlertAction in
//        let storyboard = UIStoryboard(name: "Customer", bundle: nil)
//        let reviewVC = storyboard.instantiateViewController(withIdentifier: "reviewVC") as! reviewVC
//        reviewVC.strjobid = self.strjobid
//        reviewVC.strserviceProviderid = self.strserviceProviderid
//        reviewVC.reviewHandler = {
//            self.dismiss(animated: false) {
//                self.navigationController?.popToRootViewController(animated: false)
//            }
//        }
//        reviewVC.modalPresentationStyle = .overCurrentContext
//        self.present(reviewVC, animated: false, completion: nil)
    }
    
    // Add the actions
    alertController.addAction(okAction)
    // Present the controller
    self.present(alertController, animated: true, completion: nil)
}

}

extension AddNewCardVC{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
            
            if textField == txtCardHolderName {
                txtCardNumber.becomeFirstResponder()
            }else if textField == txtCardNumber{
                txtExpiryDate.becomeFirstResponder()
            }else if textField == txtExpiryDate{
                txtCVC.becomeFirstResponder()
            }else if textField == txtCVC{
                txtCVC.resignFirstResponder()
}
        return true
}
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtCardNumber{
            let maxLength = 19
            let currentString: NSString = txtCardNumber.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if string != ""{
                if newString.length == 5{
                    self.txtCardNumber.text =  (self.txtCardNumber.text ?? "")+"-"
                }else if newString.length == 10{
                    self.txtCardNumber.text =   (self.txtCardNumber.text ?? "")+"-"
                }else if newString.length == 15{
                    self.txtCardNumber.text =   (self.txtCardNumber.text ?? "")+"-"
                }
            }
            
            
            return newString.length <= maxLength
        }else if textField == txtCVC{
            let maxLength = 3
            let currentString: NSString = txtCVC.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
         return true
        }
    }
    
    func callWebServiceFor_DeleteCard(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
            SVProgressHUD.show()
            self.view.endEditing(true)
            let param = ["":""]
        if let strStripeCustomerId = UserDefaults.standard.string(forKey:UserDefaults.keys.stripeCustomerId){
            self.strStripeCustomerId = strStripeCustomerId
        }
        
            let url = "https://api.stripe.com/v1/customers" + "/" + self.strStripeCustomerId + "/sources"
        let newUrl = url + "/" + self.objCardDetail.strid! 
        
        objWebserviceManager.requestDeleteCardFromStripe(strURL: newUrl, params: param as [String : Any], success: { (response) in
            self.navigationController?.popViewController(animated: true)
                objAppShareData.showAlert(withMessage: "Card deleted succesfully", type: alertType.bannerDark,on: self)
            }) { (error) in
                print(error)
                SVProgressHUD.dismiss()
            }
    }
}



