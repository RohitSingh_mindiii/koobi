//
//  PaymentHistoryVC.swift
//  MualabCustomer
//
//  Created by Mac on 23/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit

class PaymentHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
        @IBOutlet weak var outerView: UIView!
        @IBOutlet weak var lblHeader: UILabel!
        @IBOutlet weak var viewPastBooking: UIView!
        @IBOutlet weak var viewFutureBooking: UIView!
        @IBOutlet weak var segmentController: UISegmentedControl!
        @IBOutlet weak var tblPastBooking: UITableView!
        @IBOutlet weak var tblFutureBooking: UITableView!
        
        @IBOutlet weak var activity: UIActivityIndicatorView!
        @IBOutlet weak var lblNoRecord: UILabel!
        
        var selectedIndexPath : IndexPath?
        var arrPastBookingData : [PastFutureBookingData] = []
        var arrFutureBookingData : [PastFutureBookingData] = []
        
        fileprivate var strUserId : String = ""
        
        fileprivate var pageNo: Int = 0
        fileprivate var totalCount = 0
        fileprivate var lastLoadedPage = 0
        
        fileprivate var pageNo2: Int = 0
        fileprivate var totalCount2 = 0
        fileprivate var lastLoadedPage2 = 0
        
        fileprivate let pageSize = 20 // that's up to you, really
        fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
        fileprivate let reviewTextLimit = 150
        
        //MARK: - refreshControl
        lazy var refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(self.handleRefresh(_:)),
                                     for: UIControl.Event.valueChanged)
            refreshControl.tintColor = UIColor.theameColors.pinkColor
            return refreshControl
        }()
        
        lazy var refreshControl2: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(self.handleRefresh(_:)),
                                     for: UIControl.Event.valueChanged)
            refreshControl.tintColor = UIColor.theameColors.pinkColor
            return refreshControl
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureView()
            self.getBookingDataWith(0)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func viewWillAppear(_ animated: Bool) {
            selectedIndexPath = nil
            self.getBookingDataWith(0)
        }
        
        @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
            selectedIndexPath = nil
            self.getBookingDataWith(0)
            //API Call
        }
    }
    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        
        func configureView(){
            
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                if let userId = dict["_id"] as? Int {
                    self.strUserId = "\(userId)"
                }
            }
            
            outerView.layer.cornerRadius = 10
            outerView.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
            outerView.layer.borderWidth = 1
            outerView.clipsToBounds = true
            
            self.tblPastBooking.delegate = self
            self.tblPastBooking.dataSource = self
            self.tblFutureBooking.delegate = self
            self.tblFutureBooking.dataSource = self
            
            self.tblPastBooking.rowHeight = UITableView.automaticDimension;
            self.tblPastBooking.estimatedRowHeight = 77.0;
            
            self.tblPastBooking.addSubview(self.refreshControl)
            self.tblFutureBooking.addSubview(self.refreshControl2)
            
            self.lblNoRecord.isHidden = true
            
            segmentController.selectedSegmentIndex = 0
            self.lblHeader.text = "Payment History"
            self.viewPastBooking.isHidden = false
            self.viewFutureBooking.isHidden = true
        }
    }
    
    //MARK: - btn Actions
    extension PaymentHistoryVC {
        
        @IBAction func btnBackAction(_ sender: Any){
            
            if objAppShareData.isFromPaymentScreen {
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PastFutureBookingVC.self) {
                        let objVC : PastFutureBookingVC = controller as! PastFutureBookingVC
                        objAppShareData.isFromPaymentScreen = false
                        self.navigationController!.popToViewController(objVC, animated: true)
                        break
                    }
                }
                
            }else{
               self.navigationController?.popViewController(animated: true)
            }
            
        }
        
        @IBAction func btnProfileAction(_ sender: Any) {
            self.view.endEditing(true)
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
        
        @IBAction func segmentPastFuture(_ sender: UISegmentedControl) {
            
            switch segmentController.selectedSegmentIndex {
            case 0:
                
                viewPastBooking.isHidden = false
                viewFutureBooking.isHidden = true
                self.lblNoRecord.isHidden = true
                self.lblHeader.text = "Payment History"
                self.lblNoRecord.text  = "No Record Found"
                if arrPastBookingData.count == 0{
                    self.getBookingDataWith(0)
                }
                
            case 1:
                
                viewPastBooking.isHidden = true
                viewFutureBooking.isHidden = false
                self.lblHeader.text = "Referral Earnings"
                self.arrFutureBookingData.removeAll()
                self.tblFutureBooking.reloadData()
                self.lblNoRecord.text  = "Under Development"
                self.lblNoRecord.isHidden = false
                
            case 2:
                
                viewPastBooking.isHidden = true
                viewFutureBooking.isHidden = false
                self.lblHeader.text = "Discount Code"
                self.arrFutureBookingData.removeAll()
                self.tblFutureBooking.reloadData()
                self.lblNoRecord.text  = "Under Development"
                self.lblNoRecord.isHidden = false
                

            default:
                break;
            }
        }
    }
    
    // MARK: - table view Delegate and Datasource
    extension PaymentHistoryVC : CellPastFutureRatingDelegate, CellFutureBookingDelegate, CellPastFutureReviewDelegate , CellSmallPastFutureReviewDelegate, CellSmallPastFutureRatingDelegate {
        
        // MARK: - Tableview delegate methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tblPastBooking{
                return arrPastBookingData.count
            }else{
                return arrFutureBookingData.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            
            if tableView == tblPastBooking {
                
                if self.arrPastBookingData.count < self.totalCount {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                        self.getBookingDataWith(nextPage)
                    }
                }
                
            }else{
                
                if self.arrFutureBookingData.count < self.totalCount2 {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                        self.getBookingDataWith(nextPage)
                    }
                }
            }
            
            if tableView == tblPastBooking {
               
                let obj  : PastFutureBookingData = arrPastBookingData[indexPath.row]
                let cellIdentifier = "PaymentHistoryCell"
                
                if let cell : PaymentHistoryCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? PaymentHistoryCell {
                    
                    obj.indexPath = indexPath
                    cell.lblName.text = obj.userName
                    //cell.lblGender.text = obj.userName
                    cell.lblTime.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                    cell.lblPrice.text = "£" + obj.totalPrice
                    
                    if obj.arr_artistService.count > 0 {
                        cell.lblServiceName.text = "  " + obj.arr_artistService[0] + "  "
                        cell.lblServiceName.isHidden = false
                    }else{
                        cell.lblServiceName.text = "NA"
                        cell.lblServiceName.isHidden = true
                    }
                    
                    cell.lblServiceName.layer.cornerRadius = 9.0;
                    cell.lblServiceName.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
                    cell.lblServiceName.layer.borderWidth = 1.0
                    cell.lblServiceName.layer.masksToBounds = true
                    
                    let strImg = obj.profileImage
                    if strImg != "" {
                        if let url = URL(string: strImg){
                            //cell.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                            cell.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                        }
                    }
                    
                    if obj.paymentStatus == 1 {
                        cell.lblPaymentStatus.text = "Paid"
                        cell.lblPaymentStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                    }else{
                        cell.lblPaymentStatus.text = "Pending"
                        cell.lblPaymentStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
                    }
                    return cell
                    
                }else{
                    return UITableViewCell()
                }
                
            }else{
                
                let obj = arrFutureBookingData[indexPath.row]
                
                // bookStatus = 0: for pending , 1: for accept, 2: for reject or cancle, 3: for complete
                
                let cellIdentifier = "CellFutureBooking"
                if let cell : CellFutureBooking = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellFutureBooking {
                    
                    cell.delegate = self
                    cell.indexPath = indexPath
                    obj.indexPath = indexPath
                    
                    cell.lblName.text = obj.userName
                    cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                    cell.lblAmount.text = "£" + obj.totalPrice
                    
                    let strImg = obj.profileImage
                    if strImg != "" {
                        if let url = URL(string: strImg){
                            //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                            cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                        }
                    }
                    
                    if obj.arr_artistService.count > 0 {
                        cell.lblServiceCategory.text = obj.arr_artistService[0]
                    }else{
                        cell.lblServiceCategory.text = "NA"
                    }
                    
                    if obj.bookStatus == 0 {
                        
                        cell.btnRequest.setTitle("  A waiting confirmation  ", for: .normal)
                        cell.btnRequest.isHidden = false
                        
                    }else if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
                        
                        cell.btnRequest.setTitle("  Pay  ", for: .normal)
                        cell.btnRequest.isHidden = false                        
                    }else{
                        cell.btnRequest.isHidden = true
                    }
                    return cell
                }else{
                    return UITableViewCell()
                }
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            
            var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
            if tableView == tblPastBooking {
                objPastFutureBookingData = arrPastBookingData[indexPath.row]
            }else{
                objPastFutureBookingData = arrFutureBookingData[indexPath.row]
            }
            
            self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            
        }
        
    }
    
    // MARK: - table view Delegate
    extension PaymentHistoryVC {
        
        func btnDropDownTappedInRatingCell(at index:IndexPath){
            self.closeBigCell(at: index)
        }
        
        func btnDropDownTappedInSmallRatingCell(at index: IndexPath) {
            self.openBigCell(at: index)
        }
        
        func btnDropDownTappedInReviewCell(at index:IndexPath){
            self.closeBigCell(at: index)
        }
        
        func btnDropDownTappedInSmallReviewCell(at index: IndexPath) {
            self.openBigCell(at: index)
        }
        
        func btnSubmitTapped_ReviewCell(at index:IndexPath){
            
            // CellPastFutureReview
            
            if let cell : CellPastFutureReview = self.tblPastBooking.cellForRow(at: index) as? CellPastFutureReview {
                
                cell.txtViewComment.text = cell.txtViewComment.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                if cell.txtViewComment.text == "" {
                    objAppShareData.showAlert(withMessage: "Please enter some text", type: alertType.bannerDark, on: self)
                }else if cell.vwRating.value == 0 {
                    objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
                }else{
                    
                    cell.txtViewComment.endEditing(true)
                    let rating = "\(cell.vwRating.value)"
                    
                    // objAppShareData.showAlert(withMessage: "Submitted Successfully", type: alertType.bannerDark, on: self)
                    
                    let obj = arrPastBookingData[index.row]
                    
                    var parameters = [String:Any]()
                    
                    var strUserId : String = ""
                    if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                        
                        if let userId = dict["_id"] as? Int {
                            strUserId = "\(userId)"
                        }
                    }
                    
                    parameters = [
                        "artistId" : obj.artistId,
                        "userId" : strUserId,
                        "bookingId":obj._id,
                        "reviewByUser": cell.txtViewComment.text,
                        "reviewByArtist":"",
                        "rating": rating
                    ]
                    self.callWebserviceFor_addCommentAndRating(parameters : parameters, obj: obj)
                }
            }
            
        }
        
        func btnRebookTapped_ReviewCell(at index:IndexPath){
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
            self.closeBigCell(at: index)
        }
        
        func btnSendTapped(at index:IndexPath){
          
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
        
        func btnRequestTapped(at index:IndexPath){
            
            let obj : PastFutureBookingData = arrFutureBookingData[index.row]
            if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
                self.gotoPaymentInfoVC(obj : obj)
                
            }else if obj.bookStatus == 0 {
               // A waiting confirmation
                self.gotoAppoitmentBookingVC(obj: obj)
            }
        }
        
        func openBigCell(at index:IndexPath){
            
            // in fact, was this very row selected,
            // and the user is clicking to deselect it...
            // if you don't want "click a selected row to deselect"
            // then on't include this clause.
            if selectedIndexPath == index
            {
               
                selectedIndexPath = nil
                self.tblPastBooking.reloadRows(at: [index], with: .none)
                //tableView.deselectRowAtIndexPath(indexPath, animated:false)
                return
            }
            
            
            // in fact, was some other row selected??
            // user is changing to this row? if so, also deselect that row
            if selectedIndexPath != nil
            {
                let pleaseRedrawMe = selectedIndexPath!
                // (note that it will be drawn un-selected
                // since we're chaging the 'selectedIndexPath' global)
                selectedIndexPath = index
                self.tblPastBooking.reloadRows(
                    at: [pleaseRedrawMe, index],
                    with:UITableView.RowAnimation.none)
                return;
            }
            
            
            // no previous selection.
            // simply select that new one the user just touched.
            // note that you can not use Apple's willDeselectRowAtIndexPath
            // functions ... because they are freaky
            selectedIndexPath = index
            self.tblPastBooking.reloadRows(at: [index], with: .none)
        }
        
        func closeBigCell(at index:IndexPath){
            
            // in fact, was this very row selected,
            // and the user is clicking to deselect it...
            // if you don't want "click a selected row to deselect"
            // then on't include this clause.
            if selectedIndexPath == index
            {
                selectedIndexPath = nil
                self.tblPastBooking.reloadRows(at: [index], with: .none)
                //tableView.deselectRowAtIndexPath(indexPath, animated:false)
                return
            }
            
            // in fact, was some other row selected??
            // user is changing to this row? if so, also deselect that row
            if selectedIndexPath != nil
            {
                let pleaseRedrawMe = selectedIndexPath!
                // (note that it will be drawn un-selected
                // since we're chaging the 'selectedIndexPath' global)
                selectedIndexPath = nil
                self.tblPastBooking.reloadRows(
                    at: [pleaseRedrawMe, index],
                    with:UITableView.RowAnimation.none)
                return;
            }
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(at: [index], with: .none)
        }
        
    }
    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        
        
        func getBookingDataWith(_ page: Int) {
            
            var parameters = [String:Any]()
            
//            var strBookingType : String = ""
//            if self.segmentController.selectedSegmentIndex == 0 {
//                self.lastLoadedPage = page
//                self.pageNo = page
//                strBookingType = "past"
//            }else{
//                self.lastLoadedPage2 = page
//                self.pageNo2 = page
//                strBookingType = "future"
//            }
            
        
            
            parameters = [
                "userId" : strUserId,
                "userType" : "user",
                "page":page,
                "limit":self.pageSize,
                //"type": strBookingType
            ]
            
            self.webServiceCall_getBookingData(parameters: parameters)
        }
        
        
        func webServiceCall_getBookingData(parameters : [String:Any]){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            if self.segmentController.selectedSegmentIndex == 0 {
                
                if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                    self.activity.startAnimating()
                }
                
            }else{
                
                if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                    self.activity.startAnimating()
                }
            }
            
            self.lblNoRecord.isHidden = true
            objWebserviceManager.requestPost(strURL:  WebURL.paymentList, params: parameters as [String : AnyObject] , success: { response in
                
                self.activity.stopAnimating()
                self.refreshControl.endRefreshing()
                self.refreshControl2.endRefreshing()
                
                if self.segmentController.selectedSegmentIndex == 0 {
                    
                    if self.pageNo == 0 {
                        self.arrPastBookingData.removeAll()
                        self.tblPastBooking.reloadData()
                    }
                }else{
                    
                    if self.pageNo2 == 0 {
                        self.arrFutureBookingData.removeAll()
                        self.tblFutureBooking.reloadData()
                    }
                }
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    
                    if strStatus == k_success{
                        
                        if self.segmentController.selectedSegmentIndex == 0 {
                            
                            if let totalCount = response["total"] as? Int{
                                self.totalCount = totalCount
                            }
                            if let arr = response["paymentList"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    
                                    obj.userName = dict["artistName"] as? String ?? ""
                                    obj.profileImage = dict["artistProfileImage"] as? String ?? ""
                                    
                                    self.arrPastBookingData.append(obj)
                                }
                            }
                            
                        }else{
                            
                            if let totalCount = response["total"] as? Int{
                                self.totalCount2 = totalCount
                            }
                            if let arr = response["Booking"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    self.arrFutureBookingData.append(obj)
                                 }
                               }
                            }
                        
                    }else{
                        
                        if strStatus == "fail"{
                            
                        }else{
                           
                            if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                            }
                        }
                    }
                }
                
                if self.segmentController.selectedSegmentIndex == 0 {
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else{
                    self.tblFutureBooking.reloadData()
                    if self.arrFutureBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
                
            }) { (error) in
                
                self.activity.stopAnimating()
                self.refreshControl.endRefreshing()
                self.refreshControl2.endRefreshing()
                
                if self.segmentController.selectedSegmentIndex == 0 {
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else{
                    self.tblFutureBooking.reloadData()
                    if self.arrFutureBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceFor_addCommentAndRating(parameters : [String:Any] , obj : PastFutureBookingData ){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            self.activity.startAnimating()
            
            objWebserviceManager.requestPost(strURL: WebURL.bookingReviewRating, params: parameters  , success: { response in
                
                self.activity.stopAnimating()
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    
                    if strStatus == k_success{
                        obj.reviewStatus = 1
                        obj.isReviewAvailable = true
                        obj.reviewByUser = parameters["reviewByUser"] as? String ?? ""
                        if let rating = parameters["rating"] as? String {
                            obj.rating = Double(rating) ?? 0
                        }
                        
                        self.closeBigCell(at: obj.indexPath!)
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }else{
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }
                }
                
            }){ error in
                
                self.activity.stopAnimating()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        
        func getFormattedBookingDateToShowFrom(strDate : String) -> String{
            
            if strDate == ""{
                return ""
            }
            
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier:"en_US_POSIX") as Locale?
            formatter.dateFormat = "yyyy-MM-dd"
            if let date = formatter.date(from: strDate){
                formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
                
                let strDate = formatter.string(from: date)
                return strDate
            }
            
            return ""
        }
        
        func gotoPaymentInfoVC(obj : PastFutureBookingData) {
            
            self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"PaymentInfoVC") as? PaymentInfoVC {
                objVC.objPastFutureBookingData = obj
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
            
            self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"AppoitmentBookingVC") as? AppoitmentBookingVC {
                objVC.objPastFutureBookingData = obj
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
}
