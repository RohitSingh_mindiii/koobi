//
//  MyMenuVC.swift
//  Wedding
//
//  Created by mindiii on 11/16/17.
//  Copyright © 2017 mindiii. All rights reserved.
//

import UIKit
import Firebase


class MyMenuVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    // Fro Vendor
    var arrOptions:[String] = []
    var arrImgOptions:[UIImage] = []
    
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewForSide: UIView!
    @IBOutlet weak var tableSide: UITableView!
    @IBOutlet weak var btnBack: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.ConfigureView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshSlideMenuWhenBusinessInvitation), name: NSNotification.Name(rawValue: "refreshSlideMenuWhenBusinessInvitation"), object: nil)
    }
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.ConfigureView()
        self.updateArtistInfo()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func updateArtistInfo(){
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            }
            if let userName = dict["userName"] as? String {
               self.lblUsername.text = userName
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            }
            if let userName = userInfo["userName"] as? String {
                self.lblUsername.text = userName
            }
        }
    }
}

// MARK: - Custom Methods
extension MyMenuVC{
    @objc func refreshSlideMenuWhenBusinessInvitation(_ objNotify: Notification) {
        objAppShareData.strIsAnyInvitation = "1"
        ConfigureView()
    }
    func ConfigureView(){
        
        tableSide.delegate = self
        tableSide.dataSource = self
        
        //imgProfile.layer.cornerRadius = 53
        //imgProfile.layer.masksToBounds = true
        
        
        let img1 = #imageLiteral(resourceName: "user_ic")
        let img2 = #imageLiteral(resourceName: "chat_icon")
        let img3 = #imageLiteral(resourceName: "booking_icon")
        let img4 = #imageLiteral(resourceName: "id_card_icon")
        let img5 = #imageLiteral(resourceName: "star_icon")
        let img6 = #imageLiteral(resourceName: "payment_icon")
        let img7 = #imageLiteral(resourceName: "settings_icon")
        let img8 = #imageLiteral(resourceName: "info_ico_green")
        let img9 = #imageLiteral(resourceName: "logout_icon")
        let imgFolder = #imageLiteral(resourceName: "folder_icon")
        
        if objAppShareData.strIsAnyInvitation != "1"{
            arrOptions = ["Edit Profile",
                          "Inbox",
                          "My Bookings",
                          //"Business Invitation",
                          //"Rate this app",
                          "Payment Info",
                          "My Folders",
                "Settings",
                //"About Koobi",
                "Logout"]
            
            arrImgOptions = [img1,img2,
                             img3,
                             //img5,
                             img6,
                             imgFolder,img7,
                             //img8,
                             img9]
            tableSide.reloadData()
        }else{
            arrOptions = ["Edit Profile",
                          "Inbox",
                          "My Bookings",
                          "Business Invitation",
                          //"Rate this app",
                          "Payment Info",
                          "My Folders",
                "Settings",
                //"About Koobi",
                "Logout"]
            
            arrImgOptions = [img1,img2,img3,img4,
                             //img5,
                             img6,imgFolder,img7,
                             //img8,
                             img9]
            tableSide.reloadData()
        }
    }
    
    func ProfileVC(){
        
    }
}

// MARK: - Tableview delegate methods
extension MyMenuVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cellForSideIdentifier"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MyMenuCell{
            
            cell.lblOptions.text = arrOptions[indexPath.row]
            cell.imgOption.image = arrImgOptions[indexPath.row]
            
//            cell.viewOption.backgroundColor = UIColor.clear
//            cell.lblOptions.textColor = UIColor.white
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        //  "Edit Profile", "Inbox", "My Bookings","Payments", "Rate this app","Payment Info","About Koobi", "Logout"
        
        if let cell = tableView.cellForRow(at: indexPath) as? MyMenuCell {
            objAppShareData.isSlideMenuToProfileBack = true
            if cell.lblOptions.text == "Edit Profile" {
                self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            
            }else if cell.lblOptions.text == "Business Invitation"{
                let sb: UIStoryboard = UIStoryboard(name: "Invitation", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            
            }else if cell.lblOptions.text == "Inbox"{
                let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            
            }else if cell.lblOptions.text == "Settings"{
                let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SettingsVC") as? SettingsVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
                
            }else if cell.lblOptions.text == "My Bookings"{
               self.gotoBookingDetailModule()
            
            }else if cell.lblOptions.text == "Payments" {
               // self.gotoPaymentHistoryVC()
                //self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            
            }else if cell.lblOptions.text == "Rate this app" {
               // self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            
            }else if cell.lblOptions.text == "My Folders" {
                let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                     objAppShareData.btnAddHiddenONMyFolder = true
                     navigationController?.pushViewController(objVC, animated: true)
                }
            
            }else if cell.lblOptions.text == "Payment Info"{
                let sb = UIStoryboard(name:"PaymentModule",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"MakePaymentVC") as! MakePaymentVC
                objChooseType.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objChooseType, animated: true)
            
            }else if cell.lblOptions.text == "About Koobi"{
              //self.gotoTempVCWith(headerName : cell.lblOptions.text!)
                
            }else if cell.lblOptions.text == "Logout"{
                objWebserviceManager.StartIndicator()
                let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
                let calendarDate = ServerValue.timestamp()
                let dict = ["isOnline":0,
                            "lastActivity":calendarDate,
                            "firebaseToken":""
                           ] as [String : Any]
            Database.database().reference().child("users").child(myId).updateChildValues(dict)
                appDelegate.logout()
                UserDefaults.standard.set(false, forKey:  UserDefaults.keys.isLoggedIn)
                UserDefaults.standard.synchronize()
            }else{
            }
        }
    }
    
    func gotoBookingDetailModule(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PastFutureBookingVC") as? PastFutureBookingVC {
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoPaymentHistoryVC(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentHistoryVC") as? PaymentHistoryVC {
             objAppShareData.isFromPaymentScreen = false
             objVC.hidesBottomBarWhenPushed = true
             navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoTempVCWith(headerName : String){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"EditProfileVC") as? EditProfileVC {
           // objVC.headerName = headerName
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}






