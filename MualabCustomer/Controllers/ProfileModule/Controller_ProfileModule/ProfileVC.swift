//
//  ProfileVC.swift
//  MualabCustomer
//
//  Created by Mac on 04/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
//import SideMenu

class ProfileVC: UIViewController , SWRevealViewControllerDelegate {

 @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        if revealViewController() != nil {
           
            revealViewController().rightViewRevealWidth = 265 //self.view.frame.size.width * 0.7
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            
          //  self.revealViewController().delegate = self
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       
        if revealViewController() != nil {
            
            if (revealViewController().frontViewPosition == FrontViewPosition.right){
               
            }else{
                self.revealViewController().rightRevealToggle(animated: true)
            }
        }
    }
}


// MARK: - IBActions
extension ProfileVC{
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {

    }
}



