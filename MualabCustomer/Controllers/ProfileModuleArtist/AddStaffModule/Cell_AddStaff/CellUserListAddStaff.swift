//
//  CellUserListAddStaff.swift
//  MualabBusiness
//
//  Created by mac on 06/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellUserListAddStaff: UITableViewCell {

    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
