//
//  CellCertificateArtistProfile.swift
//  MualabBusiness
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellCertificateArtistProfile: UICollectionViewCell {

    @IBOutlet weak var viewCellBG: UIView!
    @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var lblCertificateStatus: UILabel!
    @IBOutlet weak var btnDeleteCertificate: UIButton!
    
}
