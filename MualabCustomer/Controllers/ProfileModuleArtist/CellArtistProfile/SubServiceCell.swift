//
//  SubSubServiceCell.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SubServiceCell: UITableViewCell{
    @IBOutlet weak var lblArtistName:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDuration:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var imgProfie:UIImageView!
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnDelete:UIButton!
    @IBOutlet weak var btnEdit2:UIButton!
    @IBOutlet weak var btnEdit3:UIButton!
    @IBOutlet weak var btnEdit4:UIButton!
    @IBOutlet weak var btnReport:UIButton!
    @IBOutlet weak var btnOnTheWay:UIButton!
    @IBOutlet weak var btnStartService:UIButton!
    @IBOutlet weak var btnEndServices:UIButton!
    @IBOutlet weak var viewStatus:UIView!
    @IBOutlet weak var lblStatusService:UILabel!
    var indexPath:IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


