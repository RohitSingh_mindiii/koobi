//
//  cellFollow.swift
//  MualabBusiness
//
//  Created by mac on 25/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class cellFollow: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnFollowUnFollow: UIButton!
    @IBOutlet weak var btnProfile: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
