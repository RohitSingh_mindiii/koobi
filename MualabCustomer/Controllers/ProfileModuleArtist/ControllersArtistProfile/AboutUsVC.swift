//
//  InvitationDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var lblDescription: UILabel!
    var strAboutUs = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
    
//MARK:- Custome Methods extension
extension AboutUsVC{
    @IBAction func btnBack(_ sender:Any){
    self.navigationController?.popViewController(animated: true)
    }
    
    func viewConfigure(){
        self.lblDescription.text = strAboutUs
        //self.changeString(userName: objCompanyList.businessName)
    }
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get started login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }
}

