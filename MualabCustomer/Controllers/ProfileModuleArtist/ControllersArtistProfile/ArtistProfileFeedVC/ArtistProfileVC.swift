//
//  ArtistProfileVC.swift
//  MualabBusiness
//
//  Created by mac on 23/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

import UIKit
import Accounts
import AVFoundation
import AVKit
import Social
//import KRProgressHUD
import Alamofire
import AlamofireImage
//import SwiftyJSON
import FirebaseAuth
import TLPhotoPicker
import Photos
import  HCSStarRatingView

let colorBtnBG = UIColor(red: 0.9271422029, green: 0.1869596243, blue: 0.4465353489, alpha: 1)
let colorBtnBorder = UIColor(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
let defaults = UserDefaults.standard

class ArtistProfileVC: UIViewController,UITableViewDelegate, UITableViewDataSource,AVPlayerViewControllerDelegate,UITextViewDelegate {
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var tblFeeds: UITableView!
    
    //profile Header detail Outlate
    @IBOutlet weak var lblArtistDistance: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblArtistEmail: UILabel!
    
    //profile image and rating, ratingcount detail Outlate
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var imgArtistProfile: UIImageView!
    @IBOutlet weak var imgCertifide: UIImageView!
    @IBOutlet weak var lblRatingCount: UILabel!
    
    @IBOutlet weak var imgBackWord: UIImageView!
    @IBOutlet weak var imgForword: UIImageView!
    
    //@IBOutlet weak var viewRating: HCSStarRatingView!
    
    //stack detail Outlate
    //backword
    @IBOutlet weak var stackBackword: UIStackView!
    @IBOutlet weak var lblServiceCount: UILabel!
    @IBOutlet weak var lblCertificateCount: UILabel!

    //forward
    @IBOutlet weak var stackForward: UIStackView!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblPostCount: UILabel!
    @IBOutlet weak var imgForwardSymbole: UIImageView!
    @IBOutlet weak var imgBackwardSymbole: UIImageView!
    
    //feed image video outlate
    @IBOutlet weak var btnFeeds: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    
    ///akash sir code
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    //fileprivate var refreshControl = UIRefreshControl()
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    fileprivate var lastLoadedPage = 0
    
    fileprivate var strStatusfeed:String? = ""
    fileprivate var strStatus:String? = ""
    
    fileprivate var isVideo = false
    fileprivate var isFromImagePicker = false
    fileprivate var isMyStoryIsAdded = false
    fileprivate var isNavigate = false
    
    fileprivate var Image: UIImage?
    fileprivate var mp4VideoURL: URL?
    fileprivate var myStoryCount: Int = 0
    fileprivate var strType = ""
    fileprivate var suggesionType = ""
    fileprivate var videoData: Data?
    fileprivate var tblView: UITableView?
    fileprivate var fileSize:Float = 0.0
    fileprivate var pointToScroll : CGFloat = 0.0
    fileprivate var myId:Int = 0
    fileprivate var dataToAddCount = 10

    fileprivate var arrUsers = [UserDetailArtistProfile]()
    fileprivate var arrFeedImages = [UIImage]()
    fileprivate var suggesionArray = [suggessions]()
    
    fileprivate var startValue = 0
    fileprivate var limitValue = 10
    
    fileprivate var int = 0
    fileprivate var tableNo = 0
    fileprivate var stackManage = true
    fileprivate var LoderManage = true

    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.theameColors.pinkColor
        return refreshControl
    }()

//MARK: - System method
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfigure()
        //refreshMethod()
        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoderManage = true
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        let id = userInfo.userId
        
         myId = Int(truncating: id)
        callWebserviceFor_UserDetail()
        refreshData(page: 0)
        //loadFeedsWithPage(page:0, refresh: false, strFeedType: feedType)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - custome method
extension ArtistProfileVC{
    func viewConfigure(){
        self.stackForward.isHidden = true
        self.stackBackword.isHidden = false
        self.tblFeeds.delegate = self
        self.tblFeeds.dataSource = self

        self.lblNoDataFound.isHidden = true
        
        self.imgBackWord.image = #imageLiteral(resourceName: "active_point_img")
        self.imgForword.image = #imageLiteral(resourceName: "inactive_point_img")
        stackManage = true
        tableNo = 0
        btnFeeds.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.1882352941, blue: 0.4470588235, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        
        self.imgForwardSymbole.image = #imageLiteral(resourceName: "Forword_ico")
        self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWordGray_ico")

        //Get my Id
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        let id = userInfo.userId
        myId = Int(truncating: id)
        self.tblFeeds.addSubview(self.refreshControl)
    }
    
   func dataParsing(){
    
        let objDetail = self.arrUsers[0]
    
        self.lblArtistName.text = objDetail.firstName+" "+objDetail.lastName//objDetail.businessName
        self.lblArtistEmail.text = "@"+objDetail.userName
        self.lblRatingCount.text = "("+objDetail.ratingCount+")"
        self.lblServiceCount.text = objDetail.serviceCount
        self.lblCertificateCount.text = objDetail.certificateCount
        self.lblFollowersCount.text = objDetail.followersCount
        self.lblFollowingCount.text = objDetail.followingCount
        self.lblArtistDistance.text = objDetail.radius + " Miles"
        self.lblPostCount.text = objDetail.postCount
    
        let str =  objDetail.ratingCount
            guard let n = NumberFormatter().number(from: str) else { return }
            self.viewRating.value = CGFloat(truncating: n)
    
            if objDetail.profileImage != "" {
                if let url = URL(string: (objDetail.profileImage)){
                    self.imgArtistProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
    
        if objDetail.certificateCount == "0"{
            imgCertifide.image = nil
        }
}
    
    func underDevelopment(){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
}

//MARK: - Button action extension
 fileprivate extension ArtistProfileVC{
    @IBAction func btnForwardAction(_ sender: UIButton) {
        if stackManage == true{
        self.imgForwardSymbole.image = #imageLiteral(resourceName: "forWordGray_ico")
        self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWord_ico")
        self.imgForword.image = #imageLiteral(resourceName: "active_point_img")
        self.imgBackWord.image = #imageLiteral(resourceName: "inactive_point_img")
        
        UIView.animate(withDuration: 0.4, animations: {
            self.stackBackword.isHidden = true
            self.stackForward.isHidden = false
        })
            stackManage = false
            
        }else{
            
        }
    
    }
    
    @IBAction func btnBackwardAction(_ sender: UIButton) {
        if stackManage == false{
        self.imgForwardSymbole.image = #imageLiteral(resourceName: "Forword_ico")
        self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWordGray_ico")
        self.imgBackWord.image = #imageLiteral(resourceName: "active_point_img")
        self.imgForword.image = #imageLiteral(resourceName: "inactive_point_img")
        
        UIView.animate(withDuration: 0.4, animations: {
            self.stackForward.isHidden = true
            self.stackBackword.isHidden = false
        })
            stackManage = true
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

//stack backWard button action
    @IBAction func btnServiceAction(_ sender: UIButton) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"ArtistServicesVC") as? ArtistServicesVC
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnAboutAction(_ sender: UIButton) {
        self.underDevelopment()
    }
    
    @IBAction func btnCertificateAction(_ sender: UIButton) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CertificateVC") as? CertificateVC
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
//stack ForWard button action
    @IBAction func btnFollowersAction(_ sender: UIButton) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"FollowerVC") as? FollowerVC
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    @IBAction func btnFollowingAction(_ sender: UIButton) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"FollowingVC") as? FollowingVC
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.underDevelopment()
    }


//feed,video,image Button action
    @IBAction func btnFeedsAction(_ sender: UIButton) {
        tableNo = 0
        strType = ""
        btnFeeds.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.1882352941, blue: 0.4470588235, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        loadFeedsWithPage(page:0, limitePage: 20, refresh: true, strFeedType: "")
        
    }
    
    @IBAction func btnImageAction(_ sender: UIButton) {
        tableNo = 1
        strType = "image"
        btnFeeds.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.1882352941, blue: 0.4470588235, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        loadImagesWithPage(page:0, limitePage: 20, refresh: true, strFeedType: "image")
        
    }
    @IBAction func btnVideoAction(_ sender: UIButton) {
        tableNo = 2
        strType = "video"
        btnFeeds.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.1882352941, blue: 0.4470588235, alpha: 1), for: .normal)
        loadVideosWithPage(page:0, limitePage: 20, refresh: true, strFeedType: "video")
       
    }
}

//MARK: - api calling
extension ArtistProfileVC{
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        LoderManage = false
        self.refreshData(page: 0)
    }
    
    func refreshData(page:Int){
        self.lastLoadedPage = page
        self.pageNo = page
             
        if tableNo == 0 {
            loadFeedsWithPage(page:self.lastLoadedPage, limitePage: self.pageSize, refresh: true, strFeedType: "")
        }else if tableNo == 1{
            loadImagesWithPage(page:self.lastLoadedPage, limitePage: self.pageSize, refresh: true, strFeedType: "image")
        }else{
            loadVideosWithPage(page:self.lastLoadedPage, limitePage: self.pageSize, refresh: true, strFeedType: "video")
        }
    }
    
    func loadImagesWithPage(page: Int, limitePage:Int, refresh:Bool,strFeedType:String) {

        let dicParam = ["feedType": strType, "search": "", "page": pageNo, "limit": limitePage,"userId":myId] as [String : Any]
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }
    
    
    func loadVideosWithPage(page: Int,limitePage:Int, refresh:Bool,strFeedType:String) {
        pageNo = page
        strType = strFeedType//"video"
        let dicParam = ["feedType": strType, "search": "", "page": pageNo, "limit": limitePage,"userId":myId] as [String : Any]
        
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }
    
    func loadFeedsWithPage(page: Int,limitePage:Int, refresh:Bool,strFeedType:String) {
        pageNo = page
        let dicParam = ["feedType": "","search": "", "page": pageNo, "limit": limitePage,"userId":myId] as [String : Any]
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }

    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any] , activity:Bool){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if LoderManage{
            objWebserviceManager.StartIndicator()
        }
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPostForJson(strURL: WebURL.profileFeed, params: parameters , success: { response in
            objWebserviceManager.StopIndicator()

            self.refreshControl.endRefreshing()
            if self.startValue  == 0{
                objAppShareData.arrFeedsData.removeAll()
                self.tblFeeds.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            print(response)
            self.strStatus = response["message"] as? String
            if strSucessStatus == k_success{
                self.totalCount = response["total"] as? Int ?? 0
                let arrDict = response["AllFeeds"] as! [[String:Any]]
                self.dataToAddCount = arrDict.count
                if arrDict.count > 0 {
                    for dict in arrDict{
                        let obj = feeds.init(dict: dict)
                        objAppShareData.arrFeedsData.append(obj!)
                    }
                    if objAppShareData.arrFeedsData.count > 0{  self.updateUI()  }
                }else{ if objAppShareData.arrFeedsData.count==0{ self.lblNoDataFound.isHidden = false } }
            }else{ if strSucessStatus == "fail"{ if objAppShareData.arrFeedsData.count==0{ self.lblNoDataFound.isHidden = false }
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }  }
        }) { (error) in
             objWebserviceManager.StopIndicator()
            self.refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        } }
    
    
    //call webservice for userDetail
    func callWebserviceFor_UserDetail(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return }
        objWebserviceManager.requestPostForJson(strURL: WebURL.getProfile, params: ["userId":myId], success: { (response) in
            objWebserviceManager.StopIndicator()
            
            print(response)
            if response["message"] as? String ?? "" == "ok" || response["message"] as? String ?? "" == k_success{ if let dict = response["userDetail"] as? [[String : Any]]{
                self.arrUsers.removeAll()
                for dicService in dict{
                    let obj1 = UserDetailArtistProfile.init(dict: dicService)
                    self.arrUsers.append(obj1!)
                }
                self.dataParsing()
                } }else{
                objAppShareData.showAlert(withMessage: response["message"] as! String, type: alertType.bannerDark,on: self) }
        }) { (error) in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func updateUI() -> Void {
        if objAppShareData.arrFeedsData.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.tblFeeds.reloadData()
            if self.pageNo == 0{
                self.tblFeeds.contentOffset.y = 0
            }
        }
    }
}

// MARK:- UITableView Delegate and Datasource
extension ArtistProfileVC {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == self.tblFeeds{
                return objAppShareData.arrFeedsData.count
        }else{
                return 0
        }
    }
    
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    let cell = UITableViewCell()
    
    if self.arrFeedImages.count < self.totalCount {
        
        let nextPage: Int = Int(indexPath.item / pageSize) + 1
        let preloadIndex = nextPage * pageSize - preloadMargin
        
        // trigger the preload when you reach a certain point AND prevent multiple loads and updates
        if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
            self.refreshData(page: nextPage)
        }
    }
    
    
    if tableView == tblFeeds{
     tblView = tableView
        if objAppShareData.arrFeedsData.count>indexPath.row {
            let objFeeds = objAppShareData.arrFeedsData[indexPath.row]
            var cellId = "ImageVideo"
            
            if objFeeds.feedType == "text"{
                cellId = "Text"
            }else{
                cellId = "ImageVideo"
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! feedsTableCell
            
            cell.lblUserName.text = objFeeds.userInfo?.userName
            cell.lblCity.text = objFeeds.location
            cell.lblTime.text = objFeeds.timeElapsed
            let url = URL(string: (objFeeds.userInfo?.profileImage)!)
            if  url != nil {
                cell.imgProfile.af_setImage(withURL: url!)
            }else{
                cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
            }
            cell.txtCapView.text = objFeeds.caption
            cell.lblCaption.text = objFeeds.caption
            //  if objFeeds.feedType == "image"{
            cell.scrollView.removeFromSuperview()
            //}
            if objFeeds.feedType == "video"{
                cell.pageControll.isHidden=true
                cell.pageControllView.isHidden = true;
            }else if objFeeds.feedType == "image"{
                cell.pageControll.isHidden=false
                cell.pageControllView.isHidden = false;
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            //DispatchQueue.global().async {
            cell.setPageControllMethod(arrFeed:objFeeds.arrFeed)
            //}
            }
            }else{
            }
            
            ///////code by deependra////////
            cell.imgPlay?.tag = indexPath.row
            cell.imgPlay?.superview?.tag = indexPath.section
            //
            let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
            tapForLike.numberOfTapsRequired = 2
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.superview?.tag = indexPath.section
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.superview?.tag = indexPath.section
            
            cell.imgFeeds?.tag = indexPath.row
            
            
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            cell.lblCommentCount.text = "\(objFeeds.commentCount)"
            cell.lblTime.text = objFeeds.timeElapsed
            cell.imgProfile.image = UIImage.customImage.user
            cell.imgProfile.setImageFream()

            if objFeeds.feedType == "video"{
                if objFeeds.arrFeed[0].videoThumb.count > 0 {
                    cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                }
                cell.imgPlay?.isHidden = false
             
                let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                tapForPlayVideo.numberOfTapsRequired = 1
                cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                cell.imgPlay?.addGestureRecognizer(tapForLike)
                tapForPlayVideo.require(toFail:tapForLike)
                
            }else if objFeeds.feedType == "image"{
                cell.imgPlay?.isHidden = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                tap.numberOfTapsRequired = 1
                cell.pageControllView.tag = indexPath.row
                cell.pageControllView.addGestureRecognizer(tap)
                //
                cell.pageControllView.addGestureRecognizer(tapForLike)
                tap.require(toFail:tapForLike)
            }else{
                if objAppShareData.validateUrl(objFeeds.caption) {
                    cell.lblCaption.tag = indexPath.row
                }
            }
            
            
            
            if objFeeds.isLike {
                cell.btnLike.isSelected = true
            }else {
                cell.btnLike.isSelected = false
            }
            cell.btnComment.tag = indexPath.row
            // cell.btnViewComment.tag = indexPath.row
            cell.lblLikeCount.tag = indexPath.row
            cell.lblCommentCount.tag = indexPath.row
            cell.lblCommentCount.isUserInteractionEnabled = true
            let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
            tapCommentCount.numberOfTapsRequired = 1
            cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
            
            if (cell.likes) != nil{
                cell.likes.tag = indexPath.row
            }
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            let myInt = objFeeds.likeCount
            
            if myInt > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true
                cell.lblLikeCount.addGestureRecognizer(tap2)
                cell.likes.addGestureRecognizer(tap1)
            }else {
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
            
            if objFeeds.userInfo?._id == myId{
                cell.btnFollow.isHidden=true;
            }else{
                cell.btnFollow.isHidden=false;
            }
            
            
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.superview?.tag = indexPath.section
            if objFeeds.followerStatus {
                cell.btnFollow.setTitle("Following", for: .normal)
                cell.btnFollow.setTitleColor(UIColor.theameColors.blueColor, for: .normal)
                cell.btnFollow.layer.borderWidth = 1
                cell.btnFollow.layer.borderColor = UIColor.theameColors.blueColor.cgColor
                cell.btnFollow.backgroundColor = UIColor.clear
            }else {
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                cell.btnFollow.backgroundColor = UIColor.theameColors.pinkColor
                cell.btnFollow.layer.borderWidth = 0
            }
            return cell
        }
        }else {
        return cell
        }
        return cell
    }
/////////UITableView, willDisplay cell
    private func tableView(_ tableView: UITableView, willDisplay cell: feedsTableCell, forRowAt indexPath: IndexPath){

    }

/////////UITableView, heightForRowAt indexPath
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblFeeds{
        if objAppShareData.arrFeedsData.count>indexPath.row {
        
            let objFeeds = objAppShareData.arrFeedsData[indexPath.row]
            
            if objFeeds.feedType == "text" {
               return getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Roboto-Regular", fontSize: 16.0).height + 112
            }else {
                if objFeeds.caption.count > 0 {
                   return (self.view.frame.size.width * 0.75) + 100 + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 16), font: "Roboto-Regular", fontSize: 13).height
                }else{
                   return self.view.frame.size.width * 0.75 + 100
                }
             }
            }
          }
        return 0;
    }
    
/////////UITableView, willDisplay cell: SuggessionTableCell
    private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
        let objSuggession = self.suggesionArray[indexPath.row]
        if suggesionType == ""{
            cell.lblTag.text = "#"+objSuggession.tag
        }else{
            cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
            cell.lblUserName.text = objSuggession.userName
            cell.imgProfile.image = UIImage.customImage.user
            if objSuggession.profileImage.count > 0 {
                cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
            }
        }
    }
    
    
 /////////getSizeForText
    func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
        let constraintSize = CGSize(width: width, height: .infinity)
        let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
        let attributes = [NSAttributedStringKey.font : font];
        let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
        let stringSize: CGSize = frame.size
    return stringSize
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableView(self.tblFeeds, willDisplay: cell as! feedsTableCell, forRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

//MARK: -  like, comment, shear button tablesub method
extension ArtistProfileVC{
    
    @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
        let objFeeds = objAppShareData.arrFeedsData[(myLabel.view?.tag)!]
        let objLikeVc = storyboard?.instantiateViewController(withIdentifier: "LikesListVC") as? LikesListVC
        objLikeVc?.objFeeds = objFeeds
        isNavigate = true
        navigationController?.pushViewController(objLikeVc ?? UIViewController(), animated: true)
    }
    
    @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
        if !objFeeds.isLike{
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
    }
    
    @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (myLabel.view?.tag)!
        isNavigate = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
        isNavigate = true
        let objFeed = objAppShareData.arrFeedsData[(recognizer.view?.tag)!]
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
        objShowImage.isTypeIsUrl = true
        objShowImage.arrFeedImages = objFeed.arrFeed
        present(objShowImage, animated: true)// { _ in }
    }
    
    @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
        isNavigate = true
        let objFeed = objAppShareData.arrFeedsData[(recognizer.view?.tag)!]
        addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
    }
    
    func addVideo(toVC url: URL) {
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: url)
        controller.delegate = self
        controller.player?.play()
        present(controller, animated: true)// { _ in }
    }
}

//MARK: - click like,comment, shear button
extension ArtistProfileVC{
    
    @IBAction func btnSharePost(_ sender: Any) {
        let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        // let objFeeds: feeds? = objAppshare.arrFeeds[(sender as AnyObject).tag]
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        if (objFeeds.feedType == "video"){
            showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
        } else {
            showShareActionSheet(withObject: objFeeds, andTableCell: cell!)
        }
    }
    
    func showShareActionSheet(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
        showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
    }
    
    @IBAction func btnComment(onFeed sender: Any) {
        let objComments = storyboard?.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC
        objComments?.selectedIndex = (sender as AnyObject).tag
        isNavigate = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnLikeFeed(_ sender: Any){
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsData[index!]
        likeUnlikePost(cell: cell, objFeeds: objFeeds)
    }
    

    
    func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
        if (objFeeds.isLike){
            objFeeds.isLike = false
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = false
                })
            })
            objFeeds.likeCount = objFeeds.likeCount - 1
        }else{
            objFeeds.isLike = true
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = true
                })
            })
            objFeeds.likeCount = objFeeds.likeCount + 1
        }
        cell.lblLikeCount.text = "\(objFeeds.likeCount)"
        // API Call
        objLocationManager.getCurrentAdd(success: { address in
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userData) as! [String:Any]
            var dob = ""
            var gender = ""
            var city = ""
            var state = ""
            var country = ""

            dob = decoded["dob"] as? String ?? "2000-01-01"
            gender = decoded["gender"] as? String ?? "male"
            city = decoded["city"] as? String ?? ""
            state = decoded["state"] as? String ?? ""
            country = decoded["country"] as? String ?? ""
            
            let dicParam  = ["feedId": objFeeds._id, "userId":objFeeds.userInfo?._id ?? 0, "likeById":self.myId, "age":objAppShareData.getAge(from: dob), "gender":"male", "city":city, "state":state, "country":country, "type":"feed"]  as [String : Any]
            
        self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
        }) { error in
            print("address nor found")
        }
        
        //   objAppshare.playButtonClickSound()
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        tap1.numberOfTapsRequired = 1
        tap2.numberOfTapsRequired = 1
        if objFeeds.likeCount > 0{
            cell.lblLikeCount.isUserInteractionEnabled = true
            cell.likes.isUserInteractionEnabled = true
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
        }else {
            cell.lblLikeCount.isUserInteractionEnabled = false
            cell.likes.isUserInteractionEnabled = false
        }
    }
    
    func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
            objWebserviceManager.StopIndicator()
        }){ error in
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           //dataScrollView.contentOffset = tblFeeds.contentOffset
    }
}
