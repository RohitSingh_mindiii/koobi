//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class ArtistServicesVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate var item = 0
    fileprivate var arrServices : [Service] = []
    fileprivate var arrSubServices1 : [SubService] = []
    fileprivate var arrSubSubServicesInCall : [SubSubService] = []
    fileprivate var selectedOtherIdForProfile = 0
    fileprivate var arrSubSubServicesOutCall : [SubSubService] = []

    var objArtistDetails = ArtistDetails(dict: ["":""])
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoDataFound: UIView!
    
    //user detail outlat
    
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    
    fileprivate var kOpenSectionTag: Int = 0
    fileprivate var arrCategories = [String]()
    fileprivate var arrSubCategories=[String]()
    fileprivate var expandedSectionHeaderNumber: Int = -1
    
    //// New service module
    let dropDown = DropDown()
    fileprivate var isBusinessType = true
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var btnBusinessType: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var imgBusinessTypeDown: UIImageView!
    @IBOutlet weak var imgCategoryDown: UIImageView!
    ////
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedOtherIdForProfile = objAppShareData.selectedOtherIdForProfile
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        
        self.lblNoDataFound.isHidden = true
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        //objLocationManager.getCurrentLocation()
        let nib = UINib(nibName: "CategoriesHeaderAddStaff", bundle: nil)
        self.tblBookingList.register(nib, forHeaderFooterViewReuseIdentifier: "CategoriesHeaderAddStaff")
        self.tblBookingList.reloadData()
        callWebserviceForGetServices()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //kOpenSectionTag = 0
        //item = 0
        //expandedSectionHeaderNumber = -1
        //self.tblBookingList.reloadData()
        self.lblNoDataFound.isHidden = true
        //callWebserviceForGetServices()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        self.lblNoDataFound.isHidden = true
        callWebserviceForGetServices()
    }
}

//MARK: - button extension
fileprivate extension ArtistServicesVC {
    
    func showAlertMessage(){
        
        if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
            /*
            let alertController = UIAlertController(title: "Alert", message: "You have to keep at least one staff service", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){
                UIAlertAction in
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is UsersAddServiceVC {
                        _ = self.navigationController?.popToViewController(vc as! UsersAddServiceVC, animated: true)
                    }
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
          */
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func showBookingSessionAlertMessage(){
        objWebserviceManager.StopIndicator()
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            objAppShareData.callWebserviceFor_deleteAllbooking()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}

fileprivate extension ArtistServicesVC{
    
    // MARK: - Expand / Collapse Methods
    
    /*
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
      
        let headerView = sender.view as!  CategoriesHeaderAddStaff
        let section    = headerView.tag
        headerView.lblName.textColor = UIColor.theameColors.pinkColor
        headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor

        headerView.imgDropDown.isHidden = true
        
        if (self.expandedSectionHeaderNumber == -1) {
            
            self.expandedSectionHeaderNumber = section
            kOpenSectionTag = section
            tableViewExpandSection(section, headerView: headerView)
            
        } else {
            
            if (self.expandedSectionHeaderNumber == section) {
                kOpenSectionTag = section
                tableViewCollapeSection(section, headerView: headerView)
            } else {
                let previousHeader = self.tblBookingList.headerView(forSection: kOpenSectionTag) as? CategoriesHeaderAddStaff
                tableViewCollapeSection(kOpenSectionTag,  headerView: previousHeader!)
                tableViewExpandSection(section, headerView: headerView)
                kOpenSectionTag = section
            }
        }
     }
    
    func tableViewCollapeSection(_ section: Int, headerView: CategoriesHeaderAddStaff ) {
        let imageView : UIImageView = headerView.imgDropDown
        let objService  = self.arrServices[section]
        let sectionData = objService.arrSubServices

        headerView.lblName.textColor = UIColor.theameColors.pinkColor
        headerView.imgDropDown.isHidden = true
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            headerView.lblName.textColor = UIColor.black
            headerView.lblBotumeLayer.backgroundColor = UIColor.clear
            
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, headerView: CategoriesHeaderAddStaff) {
        let objService  = self.arrSubServices1[section]
        let sectionData = objService.arrSubSubService
        
        let imageView : UIImageView = headerView.imgDropDown
        headerView.lblName.textColor = UIColor.theameColors.pinkColor
        headerView.imgDropDown.isHidden = true

        headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            
            headerView.lblName.textColor = UIColor.theameColors.pinkColor
            headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
    */
}

fileprivate extension ArtistServicesVC {
    
    func callWebserviceFor_deleteAllbooking(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let parameters : Dictionary = [
            "artistId" : objArtistDetails._id,
            "userId" : strUserId,
            ] as [String : Any]
        
        
        objWebserviceManager.requestPost(strURL: WebURL.addArtistCertificate, params: parameters  , success: { response in

            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                _ = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.arrSelectedService.removeAll()
                    self.navigationController?.popViewController(animated: true)
                }else{
                  
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


//MARK: - UITableview delegate
extension ArtistServicesVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count>0{
            return 2
        }else if (self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count==0) || (self.arrSubSubServicesInCall.count==0 && self.arrSubSubServicesOutCall.count>0){
            return 1
        }else{
           return 0
        }
        /*
        if self.arrSubServices1.count > 0 {
            tableView.backgroundView = nil
            return arrSubServices1.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = " No data Found "//"Retrieving data.\nPlease wait."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
            self.tblBookingList.backgroundView = messageLabel;
        }
        */
        return 0
    }
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && self.arrSubSubServicesInCall.count != 0{
           return self.arrSubSubServicesInCall.count
        }else if section == 0 && self.arrSubSubServicesInCall.count == 0{
           return self.arrSubSubServicesOutCall.count
        }else{
           return self.arrSubSubServicesOutCall.count
        }
            /*
            let objService = arrSubServices1[section]
            let arrayOfItems = objService.arrSubSubService
            self.arrSubSubServices2 = objService.arrSubSubService
            */
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        var subviewArray = Bundle.main.loadNibNamed("CategoriesHeaderAddStaff", owner: self, options: nil)
        let header = subviewArray?[0] as! CategoriesHeaderAddStaff
        //header.lblName.textColor = UIColor.theameColors.pinkColor
        header.lblName.textColor = UIColor.black
        //header.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
        //header.imgDropDown.isHidden = true
        //let objService = arrSubServices1[section]
        //header.lblName.text = objService.subServiceName
        if section == 0 && self.arrSubSubServicesInCall.count != 0{
            header.lblName.text = "In Call"
        }else if section == 0 && self.arrSubSubServicesInCall.count == 0{
            header.lblName.text = "Out Call"
        }else{
            header.lblName.text = "Out Call"
        }
        return header
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        var objSubService = SubSubService.init(dict: [:])
        if indexPath.section == 0 && self.arrSubSubServicesInCall.count != 0{
            objSubService = arrSubSubServicesInCall[indexPath.row]
            cell.lblPrice.text = String(objSubService.inCallPrice)
        }else if indexPath.section == 0 && self.arrSubSubServicesInCall.count == 0{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            let doubleStr = String(format: "%.2f", objSubService.outCallPrice)
            cell.lblPrice.text = String(doubleStr)
            //cell.lblPrice.text = String(objSubService.outCallPrice)
        }else{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            let doubleStr = String(format: "%.2f", objSubService.outCallPrice)
            //cell.lblPrice.text = String(objSubService.outCallPrice)
            cell.lblPrice.text = String(doubleStr)
        }
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        cell.lblName.text = objSubService.subSubServiceName
        if objSubService.isStaff == 0{
            cell.lblDuration.text = objSubService.completionTime
            if objSubService.completionTime.contains(":"){
                let arr = objSubService.completionTime.components(separatedBy: ":")
                if arr.count == 2{
                   let strHr = arr[0]
                   let strMin = arr[1]
                    if strHr != "00"{
                       cell.lblDuration.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                    }else{
                       cell.lblDuration.text = strMin + " " + "min"
                    }
                }
            }
           cell.lblDuration.isHidden = false
           cell.lblPrice.isHidden = false
        }else{
            //cell.lblDuration.text = objSubService.completionTime
            cell.lblDuration.isHidden = true
            cell.lblPrice.isHidden = true
        }
        ////
        cell.lblDuration.isHidden = true
        cell.lblPrice.isHidden = true
        ////
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var objSubService = SubSubService.init(dict: [:])
        let objDetail = storyboard?.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC
        if indexPath.section == 0 && self.arrSubSubServicesInCall.count != 0{
            objSubService = arrSubSubServicesInCall[indexPath.row]
            objDetail?.strInCallOrOutCall = "In Call"
        }else if indexPath.section == 0 && self.arrSubSubServicesInCall.count == 0{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            objDetail?.strInCallOrOutCall = "Out Call"
        }else{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            objDetail?.strInCallOrOutCall = "Out Call"
        }
        objAppShareData.selectedOtherIdForProfile = self.selectedOtherIdForProfile
        objDetail?.objService = objSubService
        navigationController?.pushViewController(objDetail ?? UIViewController(), animated: true)
    }
}

//MARK: - button extension
extension ArtistServicesVC {
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - callWebserviceForGetServices extension
fileprivate extension ArtistServicesVC{
    func callWebserviceForGetServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let dicParam = [
             "artistId":self.selectedOtherIdForProfile
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: dicParam, success: { response in
            print(response)
            self.arrServices.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    
                    objActivity.stopActivity()
                    self.saveData(dict:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dict:[String:Any]){
        
        if let dict = dict["artistServices"] as? [[String : Any]]{
            for dicService in dict{
                let obj = Service.init(dict: dicService)
                self.arrServices.append(obj)
            }
        }

        self.collectionView.reloadData()
        if arrServices.count > 0{
        var objMyData = self.arrServices[0]
            var isAnyService = false
            for objBusi in self.arrServices{
            for obj in objBusi.arrSubServices{
                if obj.arrInCall.count>0 || obj.arrOutCall.count>0{
                    isAnyService = true
                    break
                }
            }
                 if isAnyService{
                   objMyData = objBusi
                   break
                }
            }
       
        self.lblBusinessType.text = objMyData.serviceName
        self.arrSubServices1.removeAll()
        self.arrSubServices1 = objMyData.arrSubServices
        if self.arrSubServices1.count > 0{
            
            var objMyCat = self.arrSubServices1[0]
            
            //// New for no service found case
            for obj in objMyData.arrSubServices{
                if obj.arrInCall.count>0 || obj.arrOutCall.count>0{
                    objMyCat = obj
                    break
                }
            }
            ////
            
            self.lblCategory.text = objMyCat.subServiceName
            self.arrSubSubServicesInCall = objMyCat.arrInCall
            self.arrSubSubServicesOutCall = objMyCat.arrOutCall
        }
        self.tblBookingList.reloadData()
            if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                self.viewTable.isHidden = true
                self.lblNoDataFound.isHidden = false
            }else{
                self.viewTable.isHidden = false
                self.lblNoDataFound.isHidden = true
            }
            
            if self.arrServices.count == 1{
                self.imgBusinessTypeDown.isHidden = true
                self.btnBusinessType.isUserInteractionEnabled = false
            }else{
                self.imgBusinessTypeDown.isHidden = false
                self.btnBusinessType.isUserInteractionEnabled = true
            }
            if self.self.arrSubServices1.count == 1{
                self.imgCategoryDown.isHidden = true
                self.btnCategory.isUserInteractionEnabled = false
            }else{
                self.imgCategoryDown.isHidden = false
                self.btnCategory.isUserInteractionEnabled = true
            }
            if self.self.arrSubServices1.count == 1 || self.self.arrSubServices1.count == 0{
                if self.arrSubServices1.count == 0{
                    self.lblCategory.text = "No Category Available"
                }
                self.imgCategoryDown.isHidden = true
                self.btnCategory.isUserInteractionEnabled = false
            }else{
                self.imgCategoryDown.isHidden = false
                self.btnCategory.isUserInteractionEnabled = true
            }
        }else{
            self.lblNoDataFound.isHidden = false
            self.lblBusinessType.text = "No Business Available"
            self.lblCategory.text = "No Category Available"
            self.imgBusinessTypeDown.isHidden = true
            self.imgCategoryDown.isHidden = true
            self.btnCategory.isUserInteractionEnabled = false
            self.btnBusinessType.isUserInteractionEnabled = false
        }
        
    }
}

//MARK:- Collection view Delegate Methods
extension ArtistServicesVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrServices.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        if item == indexPath.row{
            cell.lblUserName.backgroundColor = #colorLiteral(red: 0.3042239547, green: 0.7356093526, blue: 0.1521026492, alpha: 1)
        }else{
            cell.lblUserName.backgroundColor = #colorLiteral(red: 0.9271422029, green: 0.1869596243, blue: 0.4465353489, alpha: 1)
        }
        cell.lblUserName.text = obj.serviceName
        cell.lblUserName.layer.cornerRadius = 15
        cell.lblUserName.layer.masksToBounds = true
        cell.btnService.tag = indexPath.row
        cell.btnService.addTarget(self, action:#selector(btnSelectService(sender:)) , for: .touchUpInside)
        self.tblBookingList.reloadData()
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 20
        cellWidth = CGFloat(self.collectionView.frame.size.width-2)
        
        let a = arrServices[indexPath.row].serviceName
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Roboto", size: 18.0)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+30
        cellHeight = 45
        if cellWidth <= 90 {
          cellWidth = 90
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    //ScrollView delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
    }
    
    @objc func btnSelectService(sender: UIButton!) {
        let objMyData  = self.arrServices[sender.tag]
        item = sender.tag
        self.collectionView.reloadData()
        self.arrSubServices1.removeAll()
        self.arrSubServices1 = objMyData.arrSubServices
        self.tblBookingList.reloadData()
    }
}

//MARK:- New Service Module
extension ArtistServicesVC{
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        if sender.tag == 0{
            isBusinessType = true
        }else{
            isBusinessType = false
        }
        
        var objGetData : [String] = []
        if isBusinessType{
            objGetData = self.arrServices.map { $0.serviceName }
            if self.arrServices.count == 0{
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
        }else{
            objGetData = self.arrSubServices1.map { $0.subServiceName }
            if self.arrSubServices1.count == 0{
                objAppShareData.showAlert(withMessage: "No category found", type: alertType.bannerDark,on: self)
                return
            }
        }
            self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
            self.dropDown.anchorView = sender // UIView or UIBarButtonItem
            self.dropDown.direction = .bottom
            self.dropDown.dataSource = objGetData
            self.dropDown.width = self.view.frame.size.width - 50
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
            if self.isBusinessType{
                self.arrSubSubServicesOutCall.removeAll()
                self.arrSubSubServicesInCall.removeAll()
                let obj = self.arrServices[index]
                self.arrSubServices1 = obj.arrSubServices
                if self.arrSubServices1.count>0{
                    let objMyCat = self.arrSubServices1[0]
                    self.arrSubSubServicesInCall = objMyCat.arrInCall
                    self.arrSubSubServicesOutCall = objMyCat.arrOutCall
                    self.lblCategory.text = objMyCat.subServiceName
                }else{
                   self.lblCategory.text = "No category available"
                }
                self.lblBusinessType.text = item
                self.tblBookingList.reloadData()
                if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                    self.viewTable.isHidden = true
                    self.lblNoDataFound.isHidden = false
                }else{
                    self.viewTable.isHidden = false
                    self.lblNoDataFound.isHidden = true
                }
                if self.arrSubServices1.count == 1 || self.arrSubServices1.count == 0{
                    self.imgCategoryDown.isHidden = true
                    self.btnCategory.isUserInteractionEnabled = false
                }else{
                    self.imgCategoryDown.isHidden = false
                    self.btnCategory.isUserInteractionEnabled = true
                }
            }else{
                self.item = index
                let objSub = self.arrSubServices1[self.item]
                self.arrSubSubServicesInCall = objSub.arrInCall
                self.arrSubSubServicesOutCall = objSub.arrOutCall
                self.lblCategory.text = item
                self.tblBookingList.reloadData()
                if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                    self.viewTable.isHidden = true
                    self.lblNoDataFound.isHidden = false
                }else{
                    self.viewTable.isHidden = false
                    self.lblNoDataFound.isHidden = true
                }
            }
            }
            self.dropDown.show()
        }
    
    @IBAction func showScrollingNC(sender:UIButton) {
        self.view.endEditing(true)
        if sender.tag == 0{
            isBusinessType = true
        }else{
            isBusinessType = false
        }
        var objGetData : [String] = []
        if isBusinessType{
            objGetData = self.arrServices.map { $0.serviceName }
            if self.arrServices.count == 0{
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
        }else{
            objGetData = self.arrSubServices1.map { $0.subServiceName }
            if self.arrSubServices1.count == 0{
                objAppShareData.showAlert(withMessage: "No category found", type: alertType.bannerDark,on: self)
                return
            }
        }
        objAppShareData.arrTBottomSheetModal = objGetData
        
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Select Option"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        if isBusinessType{
            scrollingNC.strHeaderTile = "Business Types"
        }else{
            scrollingNC.strHeaderTile = "Category Types"
        }
        
        scrollingNC.callback = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if strongSelf.isBusinessType{
                strongSelf.arrSubSubServicesOutCall.removeAll()
                strongSelf.arrSubSubServicesInCall.removeAll()
                let obj = strongSelf.arrServices[Int(str) ?? 0]
                strongSelf.arrSubServices1 = obj.arrSubServices
                if strongSelf.arrSubServices1.count>0{
                    let objMyCat = strongSelf.arrSubServices1[0]
                    strongSelf.arrSubSubServicesInCall = objMyCat.arrInCall
                    strongSelf.arrSubSubServicesOutCall = objMyCat.arrOutCall
                    strongSelf.lblCategory.text = objMyCat.subServiceName
                }else{
                    strongSelf.lblCategory.text = "No category available"
                }
                strongSelf.lblBusinessType.text = obj.serviceName
                strongSelf.tblBookingList.reloadData()
                if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                    strongSelf.viewTable.isHidden = true
                    strongSelf.lblNoDataFound.isHidden = false
                }else{
                    strongSelf.viewTable.isHidden = false
                    strongSelf.lblNoDataFound.isHidden = true
                }
                if strongSelf.arrSubServices1.count == 1 || strongSelf.arrSubServices1.count == 0{
                    strongSelf.imgCategoryDown.isHidden = true
                    strongSelf.btnCategory.isUserInteractionEnabled = false
                }else{
                    strongSelf.imgCategoryDown.isHidden = false
                    strongSelf.btnCategory.isUserInteractionEnabled = true
                }
            }else{
                strongSelf.item = Int(str) ?? 0
                let objSub = strongSelf.arrSubServices1[strongSelf.item]
                strongSelf.arrSubSubServicesInCall = objSub.arrInCall
                strongSelf.arrSubSubServicesOutCall = objSub.arrOutCall
                strongSelf.lblCategory.text = objSub.subServiceName
                strongSelf.tblBookingList.reloadData()
                if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                    strongSelf.viewTable.isHidden = true
                    strongSelf.lblNoDataFound.isHidden = false
                }else{
                    strongSelf.viewTable.isHidden = false
                    strongSelf.lblNoDataFound.isHidden = true
                }
            }
        }
        self.present(scrollingNC, animated: false, completion: nil)

        /*let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        if isBusinessType{
            tableVc.str = "Business Types"
        }else{
            tableVc.str = "Category Types"
        }
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            if strongSelf.isBusinessType{
                strongSelf.arrSubSubServicesOutCall.removeAll()
                strongSelf.arrSubSubServicesInCall.removeAll()
                let obj = strongSelf.arrServices[Int(str) ?? 0]
                strongSelf.arrSubServices1 = obj.arrSubServices
                if strongSelf.arrSubServices1.count>0{
                    let objMyCat = strongSelf.arrSubServices1[0]
                    strongSelf.arrSubSubServicesInCall = objMyCat.arrInCall
                    strongSelf.arrSubSubServicesOutCall = objMyCat.arrOutCall
                    strongSelf.lblCategory.text = objMyCat.subServiceName
                }else{
                    strongSelf.lblCategory.text = "No category available"
                }
                strongSelf.lblBusinessType.text = obj.serviceName
                strongSelf.tblBookingList.reloadData()
                if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                    strongSelf.viewTable.isHidden = true
                    strongSelf.lblNoDataFound.isHidden = false
                }else{
                    strongSelf.viewTable.isHidden = false
                    strongSelf.lblNoDataFound.isHidden = true
                }
                if strongSelf.arrSubServices1.count == 1 || strongSelf.arrSubServices1.count == 0{
                    strongSelf.imgCategoryDown.isHidden = true
                    strongSelf.btnCategory.isUserInteractionEnabled = false
                }else{
                    strongSelf.imgCategoryDown.isHidden = false
                    strongSelf.btnCategory.isUserInteractionEnabled = true
                }
            }else{
                strongSelf.item = Int(str) ?? 0
                let objSub = strongSelf.arrSubServices1[strongSelf.item]
                strongSelf.arrSubSubServicesInCall = objSub.arrInCall
                strongSelf.arrSubSubServicesOutCall = objSub.arrOutCall
                strongSelf.lblCategory.text = objSub.subServiceName
                strongSelf.tblBookingList.reloadData()
                if strongSelf.arrSubSubServicesOutCall.count == 0 && strongSelf.arrSubSubServicesInCall.count == 0{
                    strongSelf.viewTable.isHidden = true
                    strongSelf.lblNoDataFound.isHidden = false
                }else{
                    strongSelf.viewTable.isHidden = false
                    strongSelf.lblNoDataFound.isHidden = true
                }
            }
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
}
