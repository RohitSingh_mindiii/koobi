//
//  InvitationDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CertificateDetailVC: UIViewController {

    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var objCertificate = CertificateModel(dict: ["":""])
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCertificateImageZoom(_ sender:Any){
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
            var arr = [Any]()
            arr.append((objCertificate?.certificateImage)!)
            objShowImage.isTypeIsUrl = true
            objShowImage.arrFeedImages = arr
            //objShowImage.objFeeds = objFeed
            objShowImage.modalPresentationStyle = .fullScreen
            present(objShowImage, animated: true)
        }
    }
}
    
//MARK:- Custom Methods extension
extension CertificateDetailVC{
    func viewConfigure(){
        self.imgVwProfile.image = UIImage(named: "bg_group_placeholder")
        if objCertificate?.status == "0" {
            self.lblStatus.text = "Under Review"
        } else {
            self.lblStatus.text = "Verified"
        }
        self.lblTitle.text = objCertificate?.strTitle
        self.lblDescription.text  = objCertificate?.strDescription
        //self.changeString(userName: objCompanyList.businessName)
         if objCertificate?.certificateImage != "" {
            if let url = URL(string: (objCertificate?.certificateImage)!){
                //self.imgVwProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "gallery_placeholder"))
                self.imgVwProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }
    }
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get started login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }
}

