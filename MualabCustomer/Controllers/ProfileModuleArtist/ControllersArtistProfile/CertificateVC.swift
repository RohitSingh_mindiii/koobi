//
//  CertificateVC.swift
//  MualabBusiness
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CertificateVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate {
    
    fileprivate var fromCamera = false
    fileprivate var artistId = ""
    var newArtistId = ""
    var artistType = ""
    @IBOutlet weak var viewBtnAdd: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgCertificate: UIImageView!
   
    @IBOutlet weak var collecCertificate: UICollectionView!
    @IBOutlet weak var lblNoDataFound: UIView!
    
    var imagePicker = UIImagePickerController()
    fileprivate var arrCertificate = [CertificateModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DelegateCalling()
        self.preferredStatusBarStyle
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
       
//        let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.userData) as? [String:Any] ?? ["":""]
//        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
//        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
//        self.artistId = String(describing: userInfo.userId)
//        self.artistType = decoded1["userType"] as? String ?? ""
        
        if fromCamera {
           fromCamera = false
        }else{
            APICertificatelist()
        }
     }
    
    
        func APICertificatelist(){
           
//            let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.userData) as! [String:Any]
//            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
//            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
//            self.artistId = String(describing: userInfo.userId)
           
            self.callWebserviceForCertificateList(dict: ["artistId":self.newArtistId,"type":"artist"])
            
            /*
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                
                if let userId = dict["_id"] as? Int {
                    strUserId = "\(userId)"
                }
                
                if let type = dict["userType"] as? String {
                    strType = type
                    if type == "artist"{
                        self.viewBtnAdd.isHidden = false
                    }else{
                        self.viewBtnAdd.isHidden = true
                    }
                }
                
                callWebserviceForCertificateList(dict: ["artistId":self.newArtistId,
                     "type":"user",
                     ]
                )
            }
            */
    }
}

//MARK: - custome method
extension CertificateVC{
    func DelegateCalling(){
        imagePicker.delegate = self
        self.viewImage.isHidden = true
        self.collecCertificate.delegate = self
        self.collecCertificate.dataSource = self
        self.imgCertificate.layer.cornerRadius = 10
        self.imgCertificate.layer.masksToBounds = true
    }
    
    func indexMethod(){
        if self.arrCertificate.count == 0 {
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
   }
}

//MARK: - collection view delegate method
extension CertificateVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.indexMethod()
        return arrCertificate.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellIdentifier = "CellCertificateArtistProfile"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! CellCertificateArtistProfile
        let objStaff = arrCertificate[indexPath.row]
        cell.imgCertificate.image = nil

//        if objStaff.status == "0" {
//            cell.lblCertificateStatus.text = "Under Review"
//            cell.lblCertificateStatus.textColor = #colorLiteral(red: 0.9495775104, green: 0.6683173776, blue: 0.2074516714, alpha: 1)
//        } else {
//            cell.lblCertificateStatus.text = "Varified"
//            cell.lblCertificateStatus.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//        }

       // cell.viewCellBG.layer.cornerRadius = 7
        cell.viewCellBG.layer.masksToBounds = true
        let url = URL(string: objStaff.certificateImage)
        if url != nil{
            //cell.imgCertificate.af_setImage(withURL: url!)
            //cell.imgCertificate.af_setImage(withURL: url!, placeholderImage:#imageLiteral(resourceName: "gallery_placeholder"))
            cell.imgCertificate.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "gallery_placeholder"))
        }else{
            cell.imgCertificate.image = #imageLiteral(resourceName: "gallery_placeholder")
        }
        
        cell.btnDeleteCertificate.tag = indexPath.row
        cell.btnDeleteCertificate.isHidden = true
        cell.btnDeleteCertificate.addTarget(self, action:#selector(deleteCertificate(sender:)) , for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 0
        //cellWidth = CGFloat((self.collecCertificate.frame.size.width-10) / 2.0)
        cellWidth = CGFloat((self.collecCertificate.frame.size.width-3) / 3.0)
        //cellHeight = cellWidth*1.20
        cellHeight = cellWidth
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objCertificate = self.arrCertificate[indexPath.row]
        let objCertificateDetail = storyboard?.instantiateViewController(withIdentifier:"CertificateDetailVC") as? CertificateDetailVC
        objCertificateDetail?.objCertificate = objCertificate
        navigationController?.pushViewController(objCertificateDetail ?? UIViewController(), animated: true)
        
        /*
            // let isNavigate = true
            let objFeed = arrCertificate[indexPath.row]
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
             objShowImage.isTypeIsUrl = true
            // objShowImage.lblHeaderName.text = "Certificate"
            let obj = feedData(dict: ["feedPost":objFeed.certificateImage,"videoThumb":objFeed.certificateImage])
            var arr = [feedData]()
            arr.append(obj!)
        
            objShowImage.strHeaderName = "Certificate"
        
            objShowImage.arrFeedImages = arr
            self.fromCamera = true
            present(objShowImage, animated: true)// { _ in }
            */
     }
    
    //ScrollView delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.view.endEditing(true)
    }
}


//MARK:- Webservice Call
extension CertificateVC {
    
    func callWebserviceForCertificateList(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.getAllCertificate, params: dict  , success: { response in
            print(response)
            self.arrCertificate.removeAll()
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    self.collecCertificate.reloadData()
               
                }
            }
        }){ error in
            self.collecCertificate.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        if let arr = response["allCertificate"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CertificateModel.init(dict: dictArtistData)
                    self.arrCertificate.append(objArtistList!)
                }
            }
            self.collecCertificate.reloadData()
        }
    }
    
    
    func callWebserviceForImageAdd(data:Data){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let param = ["artistId":artistId]
        
        objServiceManager.uploadMultipartData(strURL: WebURL.addArtistCertificate, params:param as [String : AnyObject], imageData:data, fileName: "file.jpg", key: "certificateImage", mimeType: "image/*", success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.APICertificatelist()
                }else{
                    objWebserviceManager.StopIndicator()
                    
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            self.collecCertificate.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

    func callWebserviceForDeleteCertificate(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.deleteCertificate, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.APICertificatelist()
                }else{
                  
                    if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            self.collecCertificate.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
} }

//MARK: - button extension
extension CertificateVC{
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func deleteCertificate(sender: UIButton!){
        let objMyEventsData  = self.arrCertificate[sender.tag]
         let dict = ["artistId":objMyEventsData.artistId,"certificateId":objMyEventsData._id]
        DeleteView(dict1: dict)
    }

    func DeleteView(dict1: [String : Any]){
        let alertController = UIAlertController(title: "Alert", message: "Are you sure want to delete certificate", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.callWebserviceForDeleteCertificate(dict: dict1)
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
   
    }
    
    @IBAction func btnAddCertificate(_ sender: UIButton) {
        self.view .endEditing(true)
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.fromCamera = true
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.fromCamera = true
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnCancleCertificate(_ sender: UIButton) {
        self.viewImage.isHidden = true
        self.imgCertificate.image = nil
    }
    
    @IBAction func btnDoneCertificate(_ sender: UIButton) {
        self.viewImage.isHidden = true
        let img = self.imgCertificate.image?.fixedOrientation()
        let data1 =  objAppShareData.compressImage(image: img!) as Data?
        callWebserviceForImageAdd(data: data1!)
    }
}
//MARK: - UIImagePicker extension Methods
extension CertificateVC{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imgCertificate.image = image
            self.viewImage.isHidden = false

        }else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgCertificate.image = image
            self.viewImage.isHidden = false
        }else{
            //"Something went wrong" 
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    { dismiss(animated: true, completion: nil)
        
        
    }
    
    func openCamera() //to Access Camera
    {  if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.camera
        self.imagePicker.modalPresentationStyle = .fullScreen
        self .present(imagePicker, animated: true, completion: nil) }
    else { /*  openGallary() */ }
    }
    func openGallary() //to Access Gallery
    {    imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
        })
    }
}

