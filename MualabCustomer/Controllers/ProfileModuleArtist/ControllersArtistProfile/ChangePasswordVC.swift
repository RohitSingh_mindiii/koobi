//
//  BioVC.swift
//  SceneKey
//
//  Created by Apple on 23/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class ChangePasswordVC: UIViewController{

    @IBOutlet weak var btnOutletDone: UIButton!
    @IBOutlet weak var txtConfirmPW: UITextField!
    @IBOutlet weak var txtOldPW: UITextField!
    @IBOutlet weak var txtNewPW: UITextField!
    @IBOutlet weak var imgNewEye: UIImageView!
    @IBOutlet weak var imgConfrmEye: UIImageView!
    var iconNewClick : Bool!
    var iconConfirmClick : Bool!
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
}

// MARK: - View LifeCycle
extension ChangePasswordVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
        self.addGesture()
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK:- Custom Methods
extension ChangePasswordVC{
    
    func validation(){
        let strNewPw = self.txtNewPW.text!
        let strCnfrmPw = self.txtConfirmPW.text!
        var strMessage = ""
        if let text = txtOldPW.text, text.isEmpty{
        objAppShareData.shakeTextField(txtOldPW)
        }else if let text = txtNewPW.text, text.isEmpty{
        objAppShareData.shakeTextField(txtNewPW)
        }else if let text = txtConfirmPW.text, text.isEmpty{
            objAppShareData.shakeTextField(txtConfirmPW)
        }else if !(objValidationManager.isPasswordContainsCap(strNewPw)) || !(objValidationManager.isPasswordContainsNum(strNewPw)) || strNewPw.count < 8{
            strMessage = "Use 8 or more characters including uppercase letters and numbers"
             objAppShareData.showAlert(withMessage: strMessage , type: alertType.bannerDark,on: self)
        }else if strNewPw != strCnfrmPw{
            //strMessage = "Password and confirm password should be same"
            strMessage = "New password & confirm  password don't match"
            objAppShareData.showAlert(withMessage: strMessage , type: alertType.bannerDark,on: self)
        }else{
            self.webserviceCallForUpdatePassword()
        }
    }
    
    func updateUI(){
  
        iconNewClick = true
        iconConfirmClick = true
        
        self.btnOutletDone.layer.cornerRadius = self.btnOutletDone.frame.size.height/2
        
        let color: UIColor = UIColor.white
        txtOldPW.attributedPlaceholder = NSAttributedString(string: "Old Password", attributes: [NSAttributedString.Key.foregroundColor:color])
        txtNewPW.attributedPlaceholder = NSAttributedString(string: "New Password", attributes: [NSAttributedString.Key.foregroundColor:color])
        txtConfirmPW.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor:color])
    }
    
    func addGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
}

//MARK:- Call Webservice
extension ChangePasswordVC{
func webserviceCallForUpdatePassword() -> Void {
    objWebserviceManager.StartIndicator()
    var strId = ""
    if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
        if let userId = dict["_id"] as? Int {
            strId = "\(userId)"
        }
    }else{
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
        if let id = userInfo["_id"] as? Int {
            strId = String(id)
        }
    }
    var dict: [String: Any]? = nil
    dict = ["oldPassword" : self.txtOldPW.text!,
            "password" : self.txtNewPW.text!,
            "userId" : strId];
    objWebserviceManager.requestPost(strURL: WebURL.changepassword, params: dict!, success: { (response) in
        print(response)
        //objWebserviceManager.StopIndicator()
        let strStatus = response["status"] as! String
        let strMessage = response["message"] as! String
        if strStatus == "success" {
            //objAppShareData.showAlert(withMessage: "Password updated successfully. Please login again!" , type: alertType.bannerDark,on: self)
                let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
                let calendarDate = ServerValue.timestamp()
                let dict = ["isOnline":0,
                            "lastActivity":calendarDate,
                            "firebaseToken":""
                    ] as [String : Any]
                Database.database().reference().child("users").child(myId).updateChildValues(dict)
            
            self.showSessionAlertMessage()
        }else{
            objWebserviceManager.StopIndicator()
            showAlertVC(title: strMessage, message: "", controller: self)
        }
    }) { (error) in
        print(error)
        objWebserviceManager.StopIndicator()
        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
    }
    
     func showSessionAlertMessage(){
        objWebserviceManager.StopIndicator()
        let alertTitle = "Alert"
        let alertMessage = "Password updated successfully. Please login again to continue."
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            appDelegate.logout()
            UserDefaults.standard.set(false, forKey:  UserDefaults.keys.isLoggedIn)
            UserDefaults.standard.synchronize()
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK:- TextField Delegate
extension ChangePasswordVC : UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtOldPW {
           txtNewPW.becomeFirstResponder()
        }else if textField == txtNewPW {
           txtConfirmPW.becomeFirstResponder()
        }else{
           textField.resignFirstResponder()
        }
        return true
     }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        return true
    }
}

//MARK:- Button Actions
extension ChangePasswordVC{
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
        self.validation()
    }
    @IBAction func btnBioBackAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEyeNewAction(_ sender: Any) {
        if(iconNewClick == true) {
            txtNewPW.keyboardType = .default
            txtNewPW.isSecureTextEntry = false
            imgNewEye.image = UIImage.init(named: "open_eyes_ico")
            iconNewClick = false
        } else {
            txtNewPW.keyboardType = .default
            txtNewPW.isSecureTextEntry = true
            imgNewEye.image = UIImage.init(named: "Close_eye_ico")
            iconNewClick = true
        }
    }
    @IBAction func btnEyeConfirmAction(_ sender: Any) {
        if(iconConfirmClick == true) {
            txtConfirmPW.keyboardType = .default
            txtConfirmPW.isSecureTextEntry = false
            imgConfrmEye.image = UIImage.init(named: "open_eyes_ico")
            iconConfirmClick = false
        } else {
            txtConfirmPW.keyboardType = .default
            txtConfirmPW.isSecureTextEntry = true
            imgConfrmEye.image = UIImage.init(named: "Close_eye_ico")
            iconConfirmClick = true
        }
    }
}
