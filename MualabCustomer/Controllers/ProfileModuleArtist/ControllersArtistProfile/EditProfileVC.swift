//
//  EditProfileVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 9/24/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import CoreTelephony

fileprivate class countryCodeDataEdit: NSObject {
    
    var countryName : String = ""
    var dialCode : String = ""
    var countryCode : String = ""
    init(countryName:String?, dialCode:String? , countryCode :String?) {
        self.countryName = countryName ?? ""
        self.dialCode = dialCode ?? ""
        self.countryCode = countryCode ?? ""
    }
}


class EditProfileVC: UIViewController{
    var imagePicker = UIImagePickerController()
    var lastUserImage:UIImageView?
    var fromCamera = false
    @IBOutlet weak var imgPhoneVerifi: UIImageView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var imgCam : UIImageView!
    @IBOutlet weak var profileBackView : UIView!
    
    @IBOutlet weak var btnContactValidation: UIButton!
    
    
    @IBAction func btnContactValidationAction(_ sender: UIButton) {
        clickContinewAction()
    }
    
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var btnMale : UIButton!
    @IBOutlet weak var btnFemale : UIButton!
    
    @IBOutlet weak var DOBPicker : UIDatePicker!
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtDOB : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    @IBOutlet weak var txtUserName : UILabel!


    var dicFinalParam = ["":""]
    var imgData : Data?

    fileprivate var userImage:UIImage?
    fileprivate var strDOBForServer: String?
    
    @IBOutlet weak var viewDatePicker : UIView!
    /// for country
    
    fileprivate var arrayCountryCode : [countryCodeDataEdit] = []
    fileprivate var arrToShow : [countryCodeDataEdit] = []
    
    fileprivate var countryCode: String = ""
    fileprivate var strLocalCCode: String = ""
    fileprivate var otp:String = ""
    
    @IBOutlet weak var lblUsername:UILabel!
    @IBOutlet weak var lblCountryCode:UILabel!
    @IBOutlet weak var lblNoResults:UILabel!
    
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPhoneNumber : UITextField!
    
    
    @IBOutlet weak var countryCodeView : UIView!
    
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var tblCountryCode : UITableView!
    
    @IBOutlet weak var bottomConstraint1 : NSLayoutConstraint!
    @IBOutlet weak var lblCountryTopConstraint : NSLayoutConstraint?
       
}
//MARK: - View Hirarchy
extension EditProfileVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeKeyboard()
        getCountryCode()
        customSearchBar()
        addGasturesToViews()
        dataParsingFromUserDefault()
        addGesturesToView()
        self.searchBar.delegate = self
        self.viewDatePicker.isHidden = true
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.btnMale.isSelected = true
        self.btnFemale.isSelected = false
        self.imgPhoneVerifi.isHidden = true
        self.btnContactValidation.isHidden = true

        self.tblCountryCode.delegate = self
        self.tblCountryCode.dataSource = self
        //self.txtAddress.setValue(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), forKey: "_placeholderLabel.textColor")
        DOBPicker.date = Date().addingTimeInterval(-365*12*24*60*60)
        DOBPicker.maximumDate = Date()//.addingTimeInterval(-365*12*24*60*60)
        
        if (UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) == nil){
            let ad = Address.init(locality:"",address2:"", city:"", state: "", postalCode: "", country:"", placeName:"", fullAddress:"", latitude:"", longitude:"")
            
            let userInfo = User.init(address:ad)
            
            let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
            
            UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
            UserDefaults.standard.synchronize()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
            self.setAddress()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addGesturesToView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        imgPhoneVerifi.addGestureRecognizer(socialLoginTap)
    }
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.clickContinewAction()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if DoneEditProfile == true{
            DoneEditProfile = true
         self.navigationController?.popViewController(animated: false)
            DoneEditProfile = true
        }
        
        var dict = [:] as! [String : Any]
        if let newDict = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            dict = newDict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            dict = userInfo
        }
        let pn = dict[UserDefaults.MyData.contactNo] as? String ?? ""
        let cc = dict[UserDefaults.MyData.countryCode] as? String ?? ""
        if self.txtPhoneNumber.text == pn && cc == self.lblCountryCode.text {
            self.imgPhoneVerifi.isHidden = true
            self.btnContactValidation.isHidden = true
            objAppShareData.otpVerify = true
        }else{
            self.imgPhoneVerifi.isHidden = false
            self.btnContactValidation.isHidden = false
            objAppShareData.otpVerify = false
        }
        
        if fromCamera == false{
            dataParsingFromUserDefault()
        }else{
            fromCamera = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
        self.setAddress()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    func dataParsingFromUserDefault(){
        var dict = [:] as! [String:Any]
        if let newDict = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            dict = newDict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            dict = userInfo
        }
            
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: //#imageLiteral(resourceName: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
            lastUserImage = imgProfile
            self.txtFirstName.text = dict[UserDefaults.MyData.firstName] as? String ?? ""
            self.txtLastName.text = dict[UserDefaults.MyData.lastName] as? String ?? ""
            self.txtEmail.text = dict[UserDefaults.MyData.email] as? String ?? ""
            self.txtPhoneNumber.text = dict[UserDefaults.MyData.contactNo] as? String ?? ""
            self.lblCountryCode.text = dict[UserDefaults.MyData.countryCode] as? String ?? ""
            //self.txtAddress.text = dict[UserDefaults.MyData.address] as? String ?? ""
            self.txtAddress.text = dict["address2"] as? String ?? "" 
            self.txtUserName.text = dict[UserDefaults.MyData.userName] as? String ?? ""

            
            var newDate = dict[UserDefaults.MyData.dob] as? String ?? ""
            
            if newDate != ""{
                let array = newDate.split(separator: "-")
                if (array.count) >= 3{
                    self.txtDOB.text = array[2]+"/"+array[1]+"/"+array[0]
                }else{
                    self.txtDOB.text = newDate
                }
            }
            
            let gender = dict[UserDefaults.MyData.gender] as? String ?? ""
            if gender == "male"{
                self.btnMale.isSelected = true
                self.btnFemale.isSelected = false
            }else{
                self.btnMale.isSelected = false
                self.btnFemale.isSelected = true
            }
    }
    
    
    func parameterDictCreate(){
        var userId = ""
        var genger = "female"
        if self.btnMale.isSelected == true{
            genger = "male"
        }
        
        
        var newDate = self.txtDOB.text ?? ""
        if self.txtDOB.text != ""{
            let array = self.txtDOB.text?.split(separator: "/")
            if (array?.count)! >= 3{
                newDate = array![2]+"-"+array![1]+"-"+array![0]
            }else{
                newDate = self.txtDOB.text!
            }
        }
        
        
        
       dicFinalParam = ["userId":userId,
                        "firstName":self.txtFirstName.text ?? "",
                        "lastName":self.txtLastName.text ?? "",
                        "email":self.txtEmail.text ?? "",
                        "countryCode":self.lblCountryCode.text ?? "",
                        "contactNo":self.txtPhoneNumber.text ?? "",
                        "gender":genger,
                        "dob":newDate,
                        "address": self.txtAddress.text ?? "",
                        
                        "city": "",
                        "state":"",
                        "country": "",
                        "latitude": "",
                        "longitude": "",
                        ]
        
        
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            userId = String(dict[UserDefaults.MyData._id] as? Int ?? 0)
           
            dicFinalParam["userId"] = userId
//            dicFinalParam["countryCode"] = dict[UserDefaults.MyData.countryCode] as? Int ?? 0
//            dicFinalParam["contactNo"] = dict[UserDefaults.MyData.contactNo] as? Int ?? 0

            if self.txtAddress.text == dict[UserDefaults.MyData.address] as? String ?? ""{
                dicFinalParam["city"] = dict[UserDefaults.MyData.city] as? String ?? ""
                dicFinalParam["state"] = dict[UserDefaults.MyData.state] as? String ?? ""
                dicFinalParam["country"]  = dict[UserDefaults.MyData.country] as? String ?? ""
                dicFinalParam["latitude"] = dict[UserDefaults.MyData.latitude] as? String ?? ""
                dicFinalParam["longitude"] =  dict[UserDefaults.MyData.longitude] as? String ?? ""
            }else{
                
                if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.EditProfileAddress) as? Data{
                    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
                    let addressComponent = userInfo.address
                    
                    dicFinalParam["city"] = addressComponent.city
                    dicFinalParam["state"] = addressComponent.state
                    dicFinalParam["country"]  = addressComponent.country
                    dicFinalParam["latitude"] = addressComponent.latitude
                    dicFinalParam["longitude"] =  addressComponent.longitude
                }
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            userId = String(userInfo["_id"] as? Int ?? 0)
            dicFinalParam["userId"] = userId
            if let strLat = userInfo["latitude"] as? String{
               dicFinalParam["latitude"] = userInfo["latitude"] as? String
               dicFinalParam["longitude"] =  userInfo["longitude"] as? String
            }else{
//                let arr = userInfo["location"] as? [Any]
//                dicFinalParam["latitude"] = arr?[0] as
//                dicFinalParam["longitude"] = arr?[1]
            }
            
        }
//        if lastUserImage == self.imgProfile{
//
//        }else{
            if let img = self.userImage{
                imgData = img.jpegData(compressionQuality: 0.1)
            }
       // }
}
    
    
}
//MARK: - Local Methods
fileprivate extension EditProfileVC{
    func configureView(){
        
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtEmail.delegate = self
        self.txtPhoneNumber.delegate  = self
        
         self.txtAddress.delegate = self
        
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width/2
        //        self.imgProfile.layer.borderWidth = 4.0
        //        self.imgProfile.layer.borderColor = UIColor.theameColors.pinkColor.cgColor
        self.profileBackView.layer.cornerRadius = self.profileBackView.frame.size.width/2
        
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.viewDatePicker.isHidden = true
        
        self.countryCodeView.alpha = 0.0;
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
    }
    
    
    func addGasturesToViews() {
        
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTapNew(gestureRecognizer:)))
        imgProfile.addGestureRecognizer(loginTap)
        
        let countryCodeTap = UITapGestureRecognizer(target: self, action: #selector(handleCountryCodeTap(gestureRecognizer:)))
        lblCountryCode.addGestureRecognizer(countryCodeTap)
    }
    
    func selectProfileImage() {
        let selectImage = UIAlertController(title: "Select Profile Image", message: nil, preferredStyle: .actionSheet)
        imagePicker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.selectImageFromCamera()
        })
        
        let btn2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoLibraryPermission()
            
        })
        fromCamera = true
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true)
    }
}

//MARK: - UITapGestureActions
fileprivate extension EditProfileVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func handleImgCamTap(gestureRecognizer: UIGestureRecognizer) {
        selectProfileImage()
    }
    @objc func handleImgCamTapNew(gestureRecognizer: UIGestureRecognizer) {
        selectProfileImage()
    }
    @objc func handleCountryCodeTap(gestureRecognizer: UIGestureRecognizer) {
        self.countryCodeView.isHidden = false
        // searchBar.becomeFirstResponder()
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 1.0
        }
    }
    
}
//MARK: - IBActions

fileprivate extension EditProfileVC{
    
    @IBAction func btnImageAction(_ sender:Any){
        self.selectProfileImage()
    }
    
    @IBAction func btnBackAction(_ sender:Any){
        UserDefaults.standard.setValue(nil,forKey: UserDefaults.keys.EditProfileAddress)
        UserDefaults.standard.synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    func GotoVerifivcationVC(emailEdit:Bool){
        let sb = UIStoryboard(name:"ProfileModule",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"VerifyPhoneOTPVC") as! VerifyPhoneOTPVC
        objVC.dicFinalParam = dicFinalParam
        objVC.imgData = imgData
        objVC.strOTP = self.otp
        objVC.emailEdit = emailEdit

        fromCamera = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    func GotoAddAdressVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
        fromCamera = true

        objVC.isOutcallOption = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func setAddress(){
        if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? Data{
        let userInfonnn = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
        print(userInfonnn)
            
        /*
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! user
        let addressComponent = userInfo.address

        if addressComponent.placeName.count > 0 && !addressComponent.placeName.contains("°"){
            self.txtAddress.text = addressComponent.placeName
            self.txtAddress.placeholder = "Business Address"
        }else if addressComponent.fullAddress.count>0{
            self.txtAddress.text = addressComponent.fullAddress
            self.txtAddress.placeholder = "Business Address"
            //self.resizeTextView(textView: self.txtAddress)
        }
        */
            let strPlace = userInfonnn["address"] as! String
            let strFullAddress = userInfonnn["address2"] as! String

            if strPlace.count > 0 && !strPlace.contains("°"){
                //self.txtAddress.text = strPlace
                self.txtAddress.text = strFullAddress
                self.txtAddress.placeholder = "Business Address"
            }else if strFullAddress.count>0{
                self.txtAddress.text = strFullAddress
                self.txtAddress.placeholder = "Business Address"
                //self.resizeTextView(textView: self.txtAddress)
            }
        
        }else if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.EditProfileAddress) as? Data{
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
            let addressComponent = userInfo.address
            
            if addressComponent.placeName.count > 0 && !addressComponent.placeName.contains("°"){
                self.txtAddress.text = addressComponent.placeName
                self.txtAddress.placeholder = "Business Address"
            }else if addressComponent.fullAddress.count>0{
                self.txtAddress.text = addressComponent.fullAddress
                self.txtAddress.placeholder = "Business Address"
                // self.resizeTextView(textView: self.txtAddress)
            }
        }
    }
    @IBAction func btnAddBusinessAddress(){
        self.view.endEditing(true)
        self.locationAuthorization()
    }
    
    @IBAction func btnContinue(_ sender:Any){
        self.clickContinewAction()
    }
    func clickContinewAction(){
        var invalid = false
        var strMessage = ""
        
        txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtLastName.text = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtDOB.text = txtDOB.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        txtAddress.text = txtAddress.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtPhoneNumber.text = txtPhoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //       if userImage == nil{
        //           invalid = true
        //           strMessage = "Please select or take profile image "
        //       }else
        
        if self.txtFirstName.text?.count == 0{
            invalid = true
        objAppShareData.shakeViewField(self.txtFirstName)
        }else if self.txtLastName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtLastName)
        }else if self.txtEmail.text?.count == 0{
            objAppShareData.shakeTextField(self.txtEmail)
            invalid = true
        }else if self.txtPhoneNumber.text?.count == 0{
        objAppShareData.shakeTextField(self.txtPhoneNumber)
            invalid = true
        }else if !(objValidationManager.isValidateEmail(strEmail: self.txtEmail.text!)){
            strMessage = message.validation.email
            invalid = true
        }else if self.txtAddress.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtAddress)
        }else if self.txtDOB.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtDOB)
        }else if !self.btnMale.isSelected && !self.btnFemale.isSelected {
            invalid = true
            strMessage = message.validation.genderSelection
        }
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.bannerDark, on: self)
        }else if !invalid{
            self.view.endEditing(true)
            self.chackNumberValidation()
        }
    }
    
    func chackNumberValidation(){
        var dict = [:] as! [String:Any]
        if let newDict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            dict = newDict
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            dict = userInfo
        }
            let pn = dict[UserDefaults.MyData.contactNo] as? String ?? ""
            let cc = dict[UserDefaults.MyData.countryCode] as? String ?? ""
            let eml = dict[UserDefaults.MyData.email] as? String ?? ""

            self.parameterDictCreate()

            if self.txtPhoneNumber.text == pn && cc == self.lblCountryCode.text {
                self.imgPhoneVerifi.isHidden = true
                self.btnContactValidation.isHidden = true

                objAppShareData.otpVerify = true
                if self.txtEmail.text == eml{
                    self.callWebserviceForRegistration(logout:false,verifyScreen:false)
                }else{
                    self.callWebserviceForRegistration(logout:true,verifyScreen:false)
                }
            }else{
                self.imgPhoneVerifi.isHidden = false
                self.btnContactValidation.isHidden = false

                objAppShareData.otpVerify = false
                if self.txtEmail.text == eml{
                    self.callWebserviceForVerifyPhoneNumber(emailEdit:false)
                }else{
                    self.callWebserviceForVerifyPhoneNumber(emailEdit:true)
                }
        }
    }
    @IBAction func btnSelectDOB(_ sender:Any){
        self.viewDatePicker.isHidden = false
        self.view.endEditing(true)
        if self.txtDOB.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" as String)!
            self.DOBPicker.date = dataDate
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        if sender.tag == 1 {
            self.txtDOB.text = objAppShareData.dateFormatInDevideForm(forAPI: self.DOBPicker.date)
            strDOBForServer = objAppShareData.dateFormat(forAPI: self.DOBPicker.date)
        }
        
        if self.txtDOB.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" )!
            self.DOBPicker.date = dataDate
        }
       
        self.viewDatePicker.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnMaleFemaleSelection(_ sender:UIButton){
        if sender.tag == 0{
            self.btnMale.isSelected = true
            self.btnFemale.isSelected = false
        }else{
            self.btnMale.isSelected = false
            self.btnFemale.isSelected = true
        }
    }
    
    
    @IBAction func btnCloseCountryView(_ sender:Any){
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 0.0
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5) {
                self.countryCodeView.isHidden = true
            }
            self.view.endEditing(true)
        }
    }
}

//MARK: - UIImagePickerDelegate
extension EditProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            cropeImage(image: pickedImage)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //  isFromImagePicker = true
        dismiss(animated: true)
    }
}

//MARK: - UITextField Delegate
extension EditProfileVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if textField == self.txtFirstName || textField == self.txtLastName{
            if let range = string.rangeOfCharacter(from: NSCharacterSet.letters){
                return true
            } else if let range = string.rangeOfCharacter(from: NSCharacterSet.whitespaces){
                return true
            } else if string == "" {
                return true
            }else if string == "'" {
                return true
            }else if string == "."{
                return false
            }else{
                return false
            }
        }
        
        if textField == self.txtPhoneNumber || textField == self.lblCountryCode{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
            
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                let pn = dict[UserDefaults.MyData.contactNo] as? String ?? ""
                let cc = dict[UserDefaults.MyData.countryCode] as? String ?? ""
                if self.txtPhoneNumber.text == pn && cc == self.lblCountryCode.text {
                    self.imgPhoneVerifi.isHidden = true
                    self.btnContactValidation.isHidden = true

                    objAppShareData.otpVerify = true
                }else{
                    self.imgPhoneVerifi.isHidden = false
                    self.btnContactValidation.isHidden = false

                    objAppShareData.otpVerify = false
                }}
            }
        }
        
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == self.txtLastName{
            txtEmail.becomeFirstResponder()
        }else if textField == self.txtEmail{
            txtPhoneNumber.becomeFirstResponder()
        }else if textField == self.txtPhoneNumber{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        self.viewDatePicker.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}





extension EditProfileVC{
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                self.imagePicker.sourceType = .photoLibrary
                //  self.imagePicker.allowsEditing = true;
                //self.present(self.imagePicker, animated: true)
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true, completion: {
                    self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .red
                })
            }
            
            
        //self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized: break
                // as above
                case .denied, .restricted: break
                // as above
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
                //self.imagePicker.allowsEditing = false;
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true)
            }
        }
    }
}

extension EditProfileVC:ImageCropperDelegate{
    
    func cropeImage(image: UIImage){
        let croper = ImageCropperViewController(image:image)
        croper.delegate = self
        croper.modalPresentationStyle = .fullScreen
        self.imagePicker.present(croper, animated: true, completion: nil)
    }
    
    func imageCropperDidFinishCroppingImage(cropprdImage: UIImage){
        self.imgProfile.image = cropprdImage
        self.userImage = cropprdImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropperDidCancel(){
        dismiss(animated: true, completion: nil)
    }
}


extension EditProfileVC {
    func locationAuthorization(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.GotoAddAdressVC()
            
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.GotoAddAdressVC()
            
        }
        else if (CLLocationManager.authorizationStatus() == .denied) {
            let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        } else {
            //locManager.requestWhenInUseAuthorization()
            self.GotoAddAdressVC()
            
        }
    }
}


//MARK: - UITableView Delegate and Datasource
extension EditProfileVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CountryCodeCell") as! CountryCodeCell
        let objCountry = self.arrToShow[indexPath.row]
        cell.lblCountry?.text = objCountry.countryName
        cell.lblCode?.text = objCountry.dialCode
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let objCountry = self.arrToShow[indexPath.row]
        self.lblCountryCode.text = objCountry.dialCode
        self.countryCode = objCountry.dialCode
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            let pn = dict[UserDefaults.MyData.contactNo] as? String ?? ""
            let cc = dict[UserDefaults.MyData.countryCode] as? String ?? ""
            if self.txtPhoneNumber.text == pn && cc == self.lblCountryCode.text {
                self.imgPhoneVerifi.isHidden = true
                self.btnContactValidation.isHidden = true

                objAppShareData.otpVerify = true
            }else{
                self.imgPhoneVerifi.isHidden = false
                self.btnContactValidation.isHidden = false

                objAppShareData.otpVerify = false
            }}
        btnCloseCountryView(0)
    }
}

//MARK: - Local Methods
fileprivate extension EditProfileVC{
   
    //Custom UISearchBar
    func customSearchBar() {
        searchBar.backgroundImage = UIImage()
        searchBar.searchBarStyle = .minimal;
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.font = UIFont.init(name: "Nunito-Light", size: 17.0)
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        let clearButton = textFieldInsideSearchBar?.value(forKey: "_clearButton") as? UIButton
        let templateImage = UIImage.init(named:"clearButton")
        clearButton?.setImage(templateImage,for:.normal)
        
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor.white
    }
    //Get CountryCoads
    func getCountryCode() {
        countryCode = "+1"
        let carrier = CTTelephonyNetworkInfo().subscriberCellularProvider
        
        if (carrier?.isoCountryCode?.uppercased()) != nil{
            strLocalCCode = (carrier?.isoCountryCode?.uppercased())!
        }
        do {
            if let file = Bundle.main.url(forResource: "countrycode", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let dictData = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let object = dictData as? [String : Any] {
                    // json is a dictionary
                    let arr = object["country"] as! [AnyObject]
                    for dicCountry in arr{
                        let objCountry = countryCodeDataEdit.init(countryName: dicCountry["name"] as? String, dialCode: dicCountry["dial_code"] as? String , countryCode: dicCountry["code"] as? String)
                        self.arrayCountryCode.append(objCountry)
                        if (dicCountry["code"] as? String ?? "" == strLocalCCode) {
                            countryCode = (dicCountry["dial_code"] as? String)!
                            self.lblCountryCode.text = dicCountry["dial_code"] as? String
                        }
                    }
                    
                    self.arrToShow = self.arrayCountryCode.sorted(by: { $0.countryName < $1.countryName })
                    self.tblCountryCode.reloadData()
                } else if let object = dictData as? [Any] {
                    // json is an array
                } else {
                    //print("JSON is invalid")
                }
            } else {
                //print("no file")
            }
        } catch {
            //print(error.localizedDescription)
        }
    }
    
    
    func GotoVerifyPhoneVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"VerifyPhoneNumberVC") as! VerifyPhoneNumberVC
        objVC.strOTP = self.otp
        fromCamera = true

        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: - keyboard methods
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo
        let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
        let keyboardFrame: CGRect? = kbFrame?.cgRectValue
        let height: CGFloat? = keyboardFrame?.size.height
        bottomConstraint1.constant = height!
        UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
        bottomConstraint1.constant = 8
        UIView.animate(withDuration: animationDuration , animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    //
    func addAccessoryView() -> Void {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        
        doneButton.tintColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.7098039216, alpha: 1)
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView
    }
    //
    @objc func doneButtonTapped() {
        // do you stuff with done here
        self.txtPhoneNumber.resignFirstResponder()
    }
    
    
}


//MARK: - UISearchBar Delegate
extension EditProfileVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        searchAutocompleteEntries(withSubstring: searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        btnCloseCountryView(0)
    }
    func searchAutocompleteEntries(withSubstring substring: String) {
        let searchString = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let new = searchString.lowercased(with: Locale(identifier: "en"))
        if searchString.count > 0 {
            arrToShow = arrayCountryCode.filter({ $0.countryName.contains(searchString.uppercased()) || $0.countryName.contains(searchString.lowercased()) || $0.countryName.contains(searchString) || $0.dialCode.contains(searchString) || $0.countryCode.contains(searchString.uppercased()) || $0.countryName.contains(new) || $0.countryName.contains(new.capitalized);
            })
        }else {
            arrToShow = arrayCountryCode
        }
        self.arrToShow = self.arrToShow.sorted(by: { $0.countryName < $1.countryName })
        self.tblCountryCode.reloadData()
        if arrToShow.count>0 {
            self.lblNoResults.isHidden = true
        }else{
            self.lblNoResults.isHidden = false
        }
    }
}


//MARK: - Webservices
fileprivate extension EditProfileVC{
    
    func callWebserviceForVerifyPhoneNumber(emailEdit:Bool){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let dicParam = ["countryCode":self.lblCountryCode.text!,
                        "contactNo":self.txtPhoneNumber.text!,
                        "socialId":"",
                        "email":""]
        
        objServiceManager.requestPost(strURL: WebURL.phonVerification, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                self.otp = String.init(describing: response["otp"]!)
                self.callWebserviceForRegistration(logout: emailEdit, verifyScreen: true)
                //updateProfile API

            }else{
                
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            //print(error)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
//MARK: - Webservices
fileprivate extension EditProfileVC{
    
    func callWebserviceForRegistration(logout:Bool,verifyScreen:Bool){
        var dict = dicFinalParam
        if verifyScreen == true{
            if let dict1 : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
                 dict["countryCode"] = dict1[UserDefaults.MyData.countryCode] as? String ?? ""
                dict["contactNo"] = dict1[UserDefaults.MyData.contactNo] as? String ?? ""
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                dict["countryCode"] = userInfo[UserDefaults.MyData.countryCode] as? String ?? ""
                dict["contactNo"] = userInfo[UserDefaults.MyData.contactNo] as? String ?? ""
            }
        }else{
            dict = dicFinalParam
        }
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        
        print("dicFinalParam = ",dict)
        objServiceManager.uploadMultipartData(strURL: WebURL.profileUpdate, params: dict as [String : AnyObject] , imageData: imgData, fileName: "file.jpg", key: "profileImage", mimeType: "image/jpg", success: { response in
            print(response)
            if response["status"] as? String ?? "" == "success"{
                
                if let dictUser = response["users"] as? [String : Any]{
                    
                    let strToken = dictUser["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    //UserDefaults.standard.set(dictUser, forKey: UserDefaults.keys.userInfo)
                    
                    ////
                    let data = NSKeyedArchiver.archivedData(withRootObject: dictUser)
                    UserDefaults.standard.set(data, forKey: UserDefaults.keys.userInfo)
                    ////
                    
                    UserDefaults.standard.set(true, forKey: UserDefaults.keys.isLoggedIn)
                    
                    if let id = dictUser["_id"] as? String{
                        UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                    }else if let id = dictUser["_id"] as? Int{
                        UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                    }
                    UserDefaults.standard.set(dictUser["userName"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                    UserDefaults.standard.set(dictUser["profileImage"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                    
                    UserDefaults.standard.synchronize()
                    
                    strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                    //self.writeUserDataToFireBase(dict: dictUser)
                }
                //appDelegate.gotoTabBar(withAnitmation: true)
                
                if verifyScreen == true{
                    self.GotoVerifivcationVC(emailEdit:logout)
                }else{
                    DoneEditProfile = true
                    self.gotoBackVC(emailEdit:logout)
                }
            
            }else{
                
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func gotoBackVC(emailEdit:Bool){
        if emailEdit == true{
            appDelegate.logout()
        }else{
            DoneEditProfile = true

            self.navigationController?.popViewController(animated: true)
        }
    }
}
