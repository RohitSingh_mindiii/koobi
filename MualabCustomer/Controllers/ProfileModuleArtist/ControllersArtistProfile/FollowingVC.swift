//
//  FollowingVC.swift
//  MualabBusiness
//
//  Created by mac on 25/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SDWebImage

class FollowingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    @IBOutlet weak var indicators: UIActivityIndicatorView!

    @IBOutlet weak var tblFollower: UITableView!
    @IBOutlet weak var lblNoDataFound: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    fileprivate var arrFollower = [FollowerFollowingModel]()
    fileprivate var isOtherSelectedForProfile = false
    fileprivate var selectedOtherIdForProfile: Int = 0
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    let pageSize = 20 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    fileprivate var strSearchText = ""

    fileprivate var MyId = "0"

    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor//UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
//MARK: - system method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DelegateCalling()
        self.tblFollower.addSubview(self.refreshControl)
    
        ////
        if objAppShareData.isOtherSelectedForProfile {
           self.isOtherSelectedForProfile = true
           self.selectedOtherIdForProfile = objAppShareData.selectedOtherIdForProfile
        }
        ////
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.indicators.stopAnimating()
        
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            let myID = dicUser["_id"] as? Int ?? 0
            MyId = "\(myID)"
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                MyId = String(id)
            }
        }
        self.lblNoDataFound.isHidden = true
        loadDataWithPageCount(page: 0)
        
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
       
//        self.arrFollower.removeAll()
//        self.tblFollower.reloadData()
        loadDataWithPageCount(page: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString
        
        self.txtSearch.resignFirstResponder()
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        self.lblNoDataFound.isHidden = true

        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            strSearchText = ""
            self.indicators.startAnimating()
            self.loadDataWithPageCount(page: 0)
            
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            strSearchText = substring
            // self.searchTextDataFromTextfield()
            self.searchAutocompleteEntries(withSubstring: substring)
        }
        return true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.indicators.startAnimating()

        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.loadDataWithPageCount(page: 0)
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.indicators.startAnimating()

        self.loadDataWithPageCount(page: 0)
    }
}

//MARK: - Custome method extension
extension FollowingVC{
    func DelegateCalling(){
        self.tblFollower.delegate = self
        self.tblFollower.dataSource = self
        self.txtSearch.delegate = self
        self.indicators.stopAnimating()
    }

    func loadDataWithPageCount(page:Int){
       
        self.lastLoadedPage = page
        self.pageNo = page
        
        var userId = ""
//        if objAppShareData.isOtherSelectedForProfile {
//            userId = "\(objAppShareData.selectedOtherIdForProfile)"
//        }else{
//            userId = MyId
//        }
        
        if self.isOtherSelectedForProfile {
            userId = "\(self.selectedOtherIdForProfile)"
        }else{
            userId = MyId
        }
        
        let dicParam = ["userId": userId,
                        "userName":self.strSearchText,
                        "page": self.pageNo, "limit": self.pageSize, "loginUserId": MyId] as [String : Any]
        callWebserviceForCompanyList(dict: dicParam)
    }
}

//MARK: - Tableview delegate method extension
extension FollowingVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return arrFollower.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        
        if self.arrFollower.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                self.loadDataWithPageCount(page: nextPage)
            }
        }
                
        let cellIdentifier = "cellFollow"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? cellFollow{
            let objArr = self.arrFollower[indexPath.row]
            
            objArr.indexPath = indexPath
            
            let strImg = objArr.profileImage
            if strImg != "" {
                if let url = URL(string: strImg){
                    //cell.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
            
            
            cell.lblUserName.text = objArr.userName
            let folloStatus = objArr.followerStatus
          
//            if folloStatus == "0"{
//                cell.btnFollowUnFollow.setTitle("Follow", for: .normal)
//            }else{
//                cell.btnFollowUnFollow.setTitle("Unfollow", for: .normal)
//            }
            
            if MyId == objArr.userId{
                cell.btnFollowUnFollow.isHidden = true
            }else{
                cell.btnFollowUnFollow.isHidden = false
            }
            
            if folloStatus == "1" {
                
                cell.btnFollowUnFollow.setTitle("Following", for: .normal)
                cell.btnFollowUnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnFollowUnFollow.layer.borderWidth = 1
                cell.btnFollowUnFollow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.btnFollowUnFollow.backgroundColor = UIColor.clear
                
            }else {
                
                cell.btnFollowUnFollow.setTitle("Follow", for: .normal)
                cell.btnFollowUnFollow.setTitleColor(UIColor.white, for: .normal)
                cell.btnFollowUnFollow.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                cell.btnFollowUnFollow.layer.borderWidth = 0
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.superview?.tag = indexPath.section
            cell.btnFollowUnFollow.tag = indexPath.row
            cell.btnFollowUnFollow.addTarget(self, action:#selector(btnFollowerAction(sender:)) , for: .touchUpInside)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let objArr = self.arrFollower[indexPath.row]
    }
    
    
}

//MARK:- Webservice Call
extension FollowingVC {
    func callWebserviceForCompanyList(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            self.indicators.stopAnimating()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 && strSearchText == "" && self.txtSearch.text == ""{
            objActivity.startActivityIndicator()
        }
        
        objServiceManager.requestPost(strURL: WebURL.followingList, params: dict  , success: { response in
            self.indicators.stopAnimating()

            if self.pageNo == 0 {
                self.arrFollower.removeAll()
                self.tblFollower.reloadData()
            }
            
            self.refreshControl.endRefreshing()
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    //self.tblFollower.reloadData()
                   // let msg = response["message"] as? String ?? ""
                   // objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            
            self.tblFollower.reloadData()
            if self.arrFollower.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
        }){ error in
            self.tblFollower.reloadData()
            self.refreshControl.endRefreshing()
            objServiceManager.StopIndicator()
            self.indicators.stopAnimating()

            self.tblFollower.reloadData()
            if self.arrFollower.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        
        if let arr = response["followingList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = FollowerFollowingModel.init(dict: dictArtistData)
                    self.arrFollower.append(objArtistList)
                }
            }            
        }
        self.tblFollower.reloadData()
        if self.arrFollower.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
    }
    
    
     func callWebservice(objMyEventsData : FollowerFollowingModel, andCell cell: cellFollow){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        let followActivity = UIActivityIndicatorView()
        followActivity.tintColor = UIColor.white
        followActivity.color = UIColor.white
        followActivity.hidesWhenStopped = true
        followActivity.center = CGPoint(x: cell.btnFollowUnFollow.frame.size.width / 2, y: cell.btnFollowUnFollow.frame.size.height / 2)
        cell.btnFollowUnFollow.addSubview(followActivity)
        followActivity.startAnimating()
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        
        let dicParam = ["followerId":objMyEventsData.userId,
                        "userId":strUserId] as [String : Any]
        
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            followActivity.stopAnimating()
            followActivity.removeFromSuperview()
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
            let strSucessStatus = response["status"] as? String ?? ""
            if strSucessStatus == k_success{

                if objMyEventsData.followerStatus == "1" {
                    objMyEventsData.followerStatus = "0"
                }else{
                    objMyEventsData.followerStatus = "1"
                }
                
                self.tblFollower.reloadRows(at: [objMyEventsData.indexPath!], with: UITableView.RowAnimation.none)
                
               //self.loadDataWithPageCount(page: 0)
            
            }else{
                
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }}
        }) { error in
            self.indicators.stopAnimating()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - button extension
extension FollowingVC{
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFollower(_ sender: UIButton) {
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = self.tblFollower.cellForRow(at: indexPath) as? cellFollow
        let objMyEventsData : FollowerFollowingModel  = self.arrFollower[sender.tag]
        callWebservice(objMyEventsData: objMyEventsData, andCell: cell!)
    }
    
    @objc func btnFollowerAction(sender: UIButton!){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = self.tblFollower.cellForRow(at: indexPath) as? cellFollow
        let objMyEventsData : FollowerFollowingModel  = self.arrFollower[sender.tag]
        callWebservice(objMyEventsData: objMyEventsData, andCell: cell!)
    }
    @IBAction func btnProfileAction(_ sender: UIButton){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objUser = arrFollower[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
        /*
         let dic = [
         "tabType" : "people",
         "tagId": objUser.likeById,
         "userType":"user",
         "title": objUser.userName
         ] as [String : Any]
         self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
         */
    }
   
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        var dictTemp : [AnyHashable : Any]?
        dictTemp = dict
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let id = Int(self.MyId)
            if id == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

