//
//  InvitationDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ServiceDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var viewLabelStaff: UIView!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var tblStaff: UITableView!
    @IBOutlet weak var lblDescription: UILabel!
    fileprivate var arrInCallStaff : [StaffServiceNew] = []
    fileprivate var arrOutCallStaff : [StaffServiceNew] = []
    @IBOutlet weak var lblNoDataFound: UILabel!
    var objService = SubSubService.init(dict: [:])
    var strIdOtherUser = 0
    var strInCallOrOutCall = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.strIdOtherUser = objAppShareData.selectedOtherIdForProfile
        self.viewConfigure()
        self.callWebserviceForGetServices()
    }

    override func viewWillAppear(_ animated: Bool) {
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
    
//MARK:- Custome Methods extension
extension ServiceDetailVC{
    @IBAction func btnBack(_ sender:Any){
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBookNowAction(_ sender:Any){
        objAppShareData.isBookingFromService = true
        objAppShareData.selectedOtherIdForProfile = self.strIdOtherUser
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.hidesBottomBarWhenPushed = true
            objAppShareData.objServiceForEditBooking = self.objService
            objVC.strInCallOrOutCallFromService = self.strInCallOrOutCall
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    func viewConfigure(){
        self.viewTable.isHidden = true
        self.lblDescription.text = objService.subServiceDescription
        //self.changeString(userName: objCompanyList.businessName)
    }
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get started login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }
}

//MARK: - callWebserviceForGetServices extension
fileprivate extension ServiceDetailVC{
    func callWebserviceForGetServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let dicParam = [
        "businessId":self.strIdOtherUser,
        "artistServiceId":objService.subSubServiceId
            ] as [String : Any]

        objServiceManager.requestPostForJson(strURL: WebURL.serviceStaff, params: dicParam, success: { response in
            print(response)
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    self.saveData(dict:response)
                }else{
//                    let msg = response["message"] as? String ?? ""
//                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dict:[String:Any]){
        let dictService = dict["serviceInfo"] as! [String:Any]
        self.objService.serviceId = (dictService["serviceId"] as? Int)!
        self.objService.subServiceId = (dictService["subserviceId"] as? Int)!
        self.objService.artistId = (dictService["artistId"] as? Int)!
        
        let arrStaff = dict["staffInfo"] as! [[String:Any]]
        for dictStaff in arrStaff{
            let objStaff = StaffServiceNew.init(dict: dictStaff)
            let strType = dictStaff["bookingType"] as? String ?? ""
            if strType == "Incall"{
                self.arrInCallStaff.append(objStaff)
            }else if strType == "Outcall"{
                self.arrOutCallStaff.append(objStaff)
            }else if strType == "Both"{
                self.arrInCallStaff.append(objStaff)
                self.arrOutCallStaff.append(objStaff)
            }
        }
        
        if self.strInCallOrOutCall == "In Call" {
            if self.arrInCallStaff.count == 1{
               self.viewLabelStaff.isHidden = true
            }else{
               self.viewLabelStaff.isHidden = false
            }
        }else{
            if self.arrOutCallStaff.count == 1{
                self.viewLabelStaff.isHidden = true
            }else{
                self.viewLabelStaff.isHidden = false
            }
        }
            self.tblStaff.reloadData()
            if self.arrInCallStaff.count == 0 && self.arrOutCallStaff.count == 0{
                self.viewTable.isHidden = true
                self.lblNoDataFound.isHidden = true
            }else{
                self.viewTable.isHidden = false
                self.lblNoDataFound.isHidden = true
            }
        }
}

//MARK: - UITableview delegate
extension ServiceDetailVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        if self.arrInCallStaff.count>0 && self.arrOutCallStaff.count>0{
            return 2
        }else if (self.arrInCallStaff.count>0 && self.arrOutCallStaff.count==0) || (self.arrInCallStaff.count==0 && self.arrOutCallStaff.count>0){
            return 1
        }else{
            return 0
        }
        return 0
    }
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.strInCallOrOutCall == "In Call"{
           return self.arrInCallStaff.count
        }else{
           return self.arrOutCallStaff.count
        }
        /*
        if section == 0 && self.arrInCallStaff.count != 0{
            return self.arrInCallStaff.count
        }else if section == 0 && self.arrInCallStaff.count == 0{
            return self.arrOutCallStaff.count
        }else{
            return self.arrOutCallStaff.count
        }
        */
        /*
         let objService = arrSubServices1[section]
         let arrayOfItems = objService.arrSubSubService
         self.arrSubSubServices2 = objService.arrSubSubService
         */
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        var subviewArray = Bundle.main.loadNibNamed("CategoriesHeaderAddStaff", owner: self, options: nil)
        let header = subviewArray?[0] as! CategoriesHeaderAddStaff
        //header.lblName.textColor = UIColor.theameColors.pinkColor
        header.lblName.textColor = UIColor.black
        //header.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
        //header.imgDropDown.isHidden = true
        //let objService = arrSubServices1[section]
        //header.lblName.text = objService.subServiceName
        
        if self.strInCallOrOutCall == "In Call"{
            header.lblName.text = "In Call"
        }else{
            header.lblName.text = "Out Call"
        }
        /*
        if section == 0 && self.arrInCallStaff.count != 0{
            header.lblName.text = "In Call"
        }else if section == 0 && self.arrInCallStaff.count == 0{
            header.lblName.text = "Out Call"
        }else{
            header.lblName.text = "Out Call"
        }
        */
        return header
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        var objSubService = StaffServiceNew.init(dict: [:])
        if self.strInCallOrOutCall == "In Call"{
            objSubService = arrInCallStaff[indexPath.row]
            let doubleStr = String(format: "%.2f", objSubService.inCallPrice)
            cell.lblPrice.text = String(doubleStr)
        }else{
            objSubService = arrOutCallStaff[indexPath.row]
            let doubleStr = String(format: "%.2f", objSubService.outCallPrice)
            cell.lblPrice.text = String(doubleStr)
        }
        
        
        /*
        if indexPath.section == 0 && self.arrInCallStaff.count != 0{
            objSubService = arrInCallStaff[indexPath.row]
            cell.lblPrice.text = String(objSubService.inCallPrice)
        }else if indexPath.section == 0 && self.arrInCallStaff.count == 0{
            objSubService = arrOutCallStaff[indexPath.row]
            cell.lblPrice.text = String(objSubService.outCallPrice)
        }else{
            objSubService = arrOutCallStaff[indexPath.row]
            cell.lblPrice.text = String(objSubService.outCallPrice)
        }
        */
        
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        if objSubService.staffImage != "" {
            if let url = URL(string: objSubService.staffImage){
                //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                cell.imgProfie.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }else{
           cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
        }
        cell.lblName.text = objSubService.staffName
        cell.lblDuration.text = objSubService.completionTime
        if objSubService.completionTime.contains(":"){
           let arr = objSubService.completionTime.components(separatedBy: ":")
            if arr.count == 2{
                let strHr = arr[0]
                let strMin = arr[1]
                if strHr != "00"{
                    cell.lblDuration.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                }else{
                    cell.lblDuration.text = strMin + " " + "min"
                }
            }
            }
        cell.btnProfile.accessibilityHint = objSubService.staffName
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.superview?.tag = indexPath.section
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        var objSubService = SubSubService.init(dict: [:])
        if indexPath.section == 0 && self.arrSubSubServicesInCall.count != 0{
            objSubService = arrSubSubServicesInCall[indexPath.row]
        }else if indexPath.section == 0 && self.arrSubSubServicesInCall.count == 0{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
        }else{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
        }
        let objDetail = storyboard?.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC
        objDetail?.objService = objSubService
        navigationController?.pushViewController(objDetail ?? UIViewController(), animated: true)
         */
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton){
//        let section = (sender.superview?.tag)!
//        let row = (sender as AnyObject).tag
//        let indexPath = IndexPath(row: row!, section: section)
//        let objUser = arr[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["userName":(sender.accessibilityHint)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": (sender.accessibilityHint)!
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        var dictTemp : [AnyHashable : Any]?
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            var myId = 0
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
                myId = dicUser["_id"] as? Int ?? 0
            }else{
                let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                myId = userInfo["_id"] as? Int ?? 0
            }
            if myId == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = tagId
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}
