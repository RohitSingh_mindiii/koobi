//
//  MyMenuVC.swift
//  Wedding
//
//  Created by mindiii on 11/16/17.
//  Copyright © 2017 mindiii. All rights reserved.
//

import UIKit
import Firebase


class SettingsVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    var arrOptions:[String] = []
    var arrImgOptions:[UIImage] = []
    var notificationStatus = 0
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewForSide: UIView!
    @IBOutlet weak var tableSide: UITableView!
    @IBOutlet weak var btnBack: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ConfigureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }
}

// MARK: - Button Actions
extension SettingsVC{
    @IBAction func btnMuteAction(_ sender: UIButton) {
        self.changeMuteStatus()
    }
    
    @IBAction func btnBackAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - Custom Methods
extension SettingsVC{
    
    func ConfigureView(){
        
        if let str = UserDefaults.standard.value(forKey: UserDefaults.keys.notificationStatus) as? Int{
            self.notificationStatus = str
        }else if let str = UserDefaults.standard.value(forKey: UserDefaults.keys.notificationStatus) as? String{
            self.notificationStatus = Int(str)!
        }
        tableSide.delegate = self
        tableSide.dataSource = self
        
        let img1 = #imageLiteral(resourceName: "notification_ico_setting")
        let img2 = #imageLiteral(resourceName: "term_ico_setting")
        let img3 = #imageLiteral(resourceName: "helf_ico_setting")
        //let img4 = #imageLiteral(resourceName: "about_us_setting")
        let img5 = #imageLiteral(resourceName: "privacy_setting_ico")
        let img6 = #imageLiteral(resourceName: "lock_ico_setting")
        
        arrOptions = [    "Notification",
                          "Terms & Conditions",
                          //"Help & Support",
                          //"About Us",
                          "Privacy Policy",
                          "Reset Password",
                     ]
            
            arrImgOptions = [img1,img2,
                             //img3,
                             //img4,
                             img5,img6]
            tableSide.reloadData()
    }
    
    func changeMuteStatus(){
        
        let calendarDate = ServerValue.timestamp()
        var dict:[String:Any] = [:]
        if self.notificationStatus == 0{
            self.notificationStatus = 1
            dict = [    "isOnline":0,
                        "lastActivity":calendarDate,
                    "firebaseToken":objAppShareData.firebaseToken
                ]
        }else{
            self.notificationStatus = 0
            dict = ["isOnline":0,
                        "lastActivity":calendarDate,
                        "firebaseToken":""
                ]
        }
        let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    Database.database().reference().child("users").child(myId).updateChildValues(dict)
        self.tableSide.reloadData()
        self.callWebserviceFor_UpdateNotificationStatus()
        
    }
    
    func gotoBookingDetailModule(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PastFutureBookingVC") as? PastFutureBookingVC {
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoPaymentHistoryVC(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentHistoryVC") as? PaymentHistoryVC {
            objAppShareData.isFromPaymentScreen = false
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoTempVCWith(headerName : String){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"EditProfileVC") as? EditProfileVC {
            // objVC.headerName = headerName
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

// MARK: - Tableview delegate methods
extension SettingsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cellForSideIdentifier"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MyMenuCell{
            if indexPath.row == 0{
               cell.btnNotification.isHidden = false
            }else{
               cell.btnNotification.isHidden = true
            }
            if self.notificationStatus == 0{
                cell.btnNotification.setImage(#imageLiteral(resourceName: "ico_toggle_off"), for: .normal)
            }else{
                cell.btnNotification.setImage(#imageLiteral(resourceName: "ico_toggle_on"), for: .normal)
            }
            cell.lblOptions.text = arrOptions[indexPath.row]
            cell.imgOption.image = arrImgOptions[indexPath.row]
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if let cell = tableView.cellForRow(at: indexPath) as? MyMenuCell {
            
            if cell.lblOptions.text == "Reset Password" {
                let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ChangePasswordVC") as? ChangePasswordVC{
               navigationController?.pushViewController(objVC, animated: true)
                }
            }else if cell.lblOptions.text == "Terms & Conditions"{
                let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                viewController.strComeFrom = "Terms & Conditions"
                self.navigationController?.pushViewController(viewController, animated: true)
            }else if cell.lblOptions.text == "Privacy Policy"{
                let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                viewController.strComeFrom = "Privacy Policy"
                self.navigationController?.pushViewController(viewController, animated: true)
            }else if cell.lblOptions.text == "Business Invitation"{
                let sb: UIStoryboard = UIStoryboard(name: "Invitation", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC{
                navigationController?.pushViewController(objVC, animated: true)
                }
            }else if cell.lblOptions.text == "Inbox"{
                let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            
            }else if cell.lblOptions.text == "My Bookings"{
               self.gotoBookingDetailModule()
            
            }else if cell.lblOptions.text == "Payments" {
               // self.gotoPaymentHistoryVC()
                //self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            
            }else if cell.lblOptions.text == "Rate this app" {
               // self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            
            }else if cell.lblOptions.text == "My Folders" {
                let sb: UIStoryboard = UIStoryboard(name: "Feeds", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                    objAppShareData.btnAddHiddenONMyFolder = true
                     navigationController?.pushViewController(objVC, animated: true)
                }
            
            }else if cell.lblOptions.text == "Payment Info"{
                let sb = UIStoryboard(name:"PaymentModule",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"MakePaymentVC") as! MakePaymentVC
                self.navigationController?.pushViewController(objChooseType, animated: true)
            
            }else if cell.lblOptions.text == "About Koobi"{
              //self.gotoTempVCWith(headerName : cell.lblOptions.text!)
                
            }else if cell.lblOptions.text == "Logout"{
                let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
                let calendarDate = ServerValue.timestamp()
                let dict = ["isOnline":0,
                            "lastActivity":calendarDate,
                            "firebaseToken":""
                           ] as [String : Any]
            Database.database().reference().child("users").child(myId).updateChildValues(dict)
                appDelegate.logout()
                UserDefaults.standard.set(false, forKey:  UserDefaults.keys.isLoggedIn)
                UserDefaults.standard.synchronize()
            }else{
            }
        }
    }
    
}

// MARK: - Webservice calls
extension SettingsVC{

    func callWebserviceFor_UpdateNotificationStatus() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var token = ""
        if self.notificationStatus == 0{
            token = ""
        }else{
            token = objAppShareData.firebaseToken
        }
        let parameters : Dictionary = [
            "notificationStatus" : self.notificationStatus,
            "firebaseToken": token
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.updateRecord, params: parameters  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    UserDefaults.standard.setValue(self.notificationStatus, forKey: UserDefaults.keys.notificationStatus)
                    UserDefaults.standard.synchronize()
                }else{
                }
            }
        }){ error in
        }
    }
}

