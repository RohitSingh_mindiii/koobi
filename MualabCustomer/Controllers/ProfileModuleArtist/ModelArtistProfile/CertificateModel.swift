//
//  CertificateModel.swift
//  MualabBusiness
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CertificateModel: NSObject {
    var _id : String = "0"
    var __v: String = "0"
    var artistId: String = "0"
    var status: String = "0"
    
    var strTitle: String = ""
    var strDescription: String = ""
    var certificateImage:String = ""
    var crd: String = ""
    var upd: String = ""

    
    init?(dict : [String:Any]){
        
        if let id = dict["_id"] as? Int{
            self._id = String(id)
        }else if let id = dict["_id"] as? String{
            self._id = id
        }
        
        if let __v1 = dict["__v"] as? Int{
            self.__v = String(__v1)
        }else if let __v1 = dict["__v"] as? String{
            self.__v = __v1
        }
 
        if let artistId1 = dict["artistId"] as? Int{
            self.artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            self.artistId = artistId1
        }
        
        if let status1 = dict["status"] as? Int{
            self.status = String(status1)
        }else if let status1 = dict["status"] as? String{
            self.status = status1
        }
        
        if let certificateImage1 = dict["certificateImage"] as? String{
            self.certificateImage = certificateImage1
        }
        
        if let crd1 = dict["crd"] as? String{
            self.crd = crd1
        }
        
        if let title = dict["title"] as? String{
            self.strTitle = title
        }
        
        if let description = dict["description"] as? String{
            self.strDescription = description
        }
        
        if let upd1 = dict["upd"] as? String{
            self.upd = upd1
        }
    }
    
}
