//
//  FollowerFollowingModel.swift
//  MualabBusiness
//
//  Created by mac on 25/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

public class FollowerFollowingModel: NSObject {
 
        public var _id = ""
        public var followerId = ""
        public var userId = ""
    
        public var userName = ""
        public var firstName = ""
        public var lastName = ""
        public var profileImage = ""
        public var followerStatus = ""
    
        public var indexPath : IndexPath?
    
        init(dict: [String : Any]) {
           
            if let id = dict["_id"] as? String{
                _id = id
            }else if let id = dict["_id"] as? Int {
                _id = String(id)
            }
            
            if let followerId1 = dict["followerId"] as? String{
                followerId = followerId1
            }else if let followerId2 = dict["followerId"] as? Int {
                followerId = String(followerId2)
            }
            
            if let userId2 = dict["userId"] as? String{
                userId = userId2
            }else if let userId1 = dict["userId"] as? Int {
                userId = String(userId1)
            }
           
            if let userId2 = dict["followerStatus"] as? String{
                followerStatus = userId2
            }else if let userId1 = dict["followerStatus"] as? Int {
                followerStatus = String(userId1)
            }
            userName = dict["userName"] as? String ?? ""
            firstName = dict["firstName"] as? String ?? ""
            lastName = dict["lastName"] as? String ?? ""
            profileImage = dict["profileImage"] as? String ?? ""
           
    }
}
