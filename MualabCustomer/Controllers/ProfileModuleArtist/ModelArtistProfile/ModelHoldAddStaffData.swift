//
//  ModelHoldAddStaffData.swift
//  MualabBusiness
//
//  Created by mac on 24/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelHoldAddStaffData  {
    
    var artistId = ""
    var businessId = ""
    var arrStaffData = [ModelHoldAddStaffId]()
    
    init?(artistId:String,businessId:String,arrStaffData:[ModelHoldAddStaffId]) {
        self.artistId = artistId
        self.businessId = businessId
        self.arrStaffData = arrStaffData
    }
}

class ModelHoldAddStaff {
    
    var artistId = ""
    var _id = ""
    var artistServiceId = ""
    var businessId = ""
    var staffId = ""

    var serviceId = ""
    var subserviceId = ""
    var arrParseData = [ModelHoldAddStaffId]()
    var strArrParseDataIds = ""

    var serviceJosnArray = [[String: Any]]()
    var jsonObjectString = ""
}


class ModelHoldAddStaffId  {
    
    var artistId = ""
   // var _id = ""
    var artistServiceId = ""
    var businessId = ""
    
    var completionTime = ""
    var inCallPrice = ""
    var outCallPrice = ""
    
    var serviceId = ""
    var subserviceId = ""
    var title = ""
    init?(title:String,artistId:String,artistServiceId:String,businessId:String,completionTime:String,inCallPrice:String,serviceId:String,outCallPrice:String,subserviceId:String) {
        self.title = title

        self.artistId = artistId
        self.artistServiceId = artistServiceId
        self.businessId = businessId
        self.completionTime = completionTime
        self.inCallPrice = inCallPrice
        self.outCallPrice = outCallPrice
        self.serviceId = serviceId
        self.subserviceId = subserviceId
    }
}

