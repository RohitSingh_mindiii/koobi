//
//  SubSubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class StaffServiceNew {
    var artistId : Int = 0
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
    var subSubServiceName : String = ""
    var serviceDescription : String = ""
    
    var staffId : Int = 0
    var staffName : String = ""
    var staffImage : String = ""
    var staffJob : String = ""
    var completionTime : String = ""
    var inCallPrice : Double = 0.0
    var outCallPrice : Double = 0.0
    var isSelected = false

    init(dict : [String : Any]){
        staffId = dict["staffId"] as? Int ?? 0
        staffName = dict["staffName"] as? String ?? ""
        staffJob = dict["job"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
    }
}


