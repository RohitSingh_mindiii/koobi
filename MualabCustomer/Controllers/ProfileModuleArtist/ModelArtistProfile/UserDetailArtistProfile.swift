//
//  UserDetailArtistProfile.swift
//  MualabCustomer
//
//  Created by Mac on 13/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class UserDetailArtistProfile {
    
    var _id : String = ""
    var firstName: String = ""
    var lastName : String = ""
    var profileImage: String = ""
    var userName : String = ""
    var address : String = ""
    var address2 : String = ""
    var strIsInvitation : String = ""
    var bio : String = ""
    var buildingNumber : String = ""
    var businessName : String = ""
    var businessType: String = ""
    var businesspostalCode : String = ""
    var certificateCount : String = ""
    var contactNo : String = ""
    var countryCode : String = ""
    var dob : String = ""
    var email : String = ""
    var followersCount : String = ""
    var followerStatus : String = ""
    
    var favoriteStatus : String = ""
    var followingCount : String = ""
    var gender: String = ""
    var postCount : String = ""
    var ratingCount : String = ""
    var reviewCount : String = ""
    var serviceCount: String = ""
    var serviceType : String = ""
    var userType : String = ""
    var aboutUs : String = ""
    var radius : String = ""
    var isCertificateVerify : String = ""
    
    init(dict : [String:Any]) {
        
        if let radius = dict["radius"] as? Int{
            self.radius = String(radius)
        }else if let radius = dict["radius"] as? String{
            self.radius = radius
        }
        
        if let isCertificateVerify = dict["isCertificateVerify"] as? Int{
            self.isCertificateVerify = String(isCertificateVerify)
        }else if let isCertificateVerify = dict["isCertificateVerify"] as? String{
            self.isCertificateVerify = isCertificateVerify
        }
        
        if let isInvite = dict["isInvitation"] as? Int{
            self.strIsInvitation = String(isInvite)
        }else if let isInvite = dict["isInvitation"] as? String{
            self.strIsInvitation = isInvite
        }
        
        if let id = dict["_id"] as? Int{
            self._id = String(id)
        }else if let id1 = dict["_id"] as? String{
            self._id = id1
        }
        
        if let aboutUs = dict["bio"] as? String{
            self.aboutUs = aboutUs
        }
        
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        
        if let address1 = dict["address"] as? String{
            self.address = address1
        }
        
        if let addres = dict["address2"] as? String{
            self.address2 = addres
        }
        
        if let bio1 = dict["bio"] as? String{
            self.bio = bio1
        }
        
        if let businessNames = dict["businessName"] as? String{
            self.businessName = businessNames
        }
        
        if let businessTypes = dict["businessType"] as? String{
            self.businessType = businessTypes
        }
        
        if let genders = dict["gender"] as? String{
            self.gender = genders
        }
        
        if let emails = dict["email"] as? String{
            self.email = emails
        }
        
        if let userTypes = dict["userType"] as? String{
            self.userType = userTypes
        }
        if let serviceTypes = dict["serviceType"] as? String{
            self.serviceType = serviceTypes
        }
        
        if let buildingNumber1 = dict["buildingNumber"] as? Int{
            self.buildingNumber = String(buildingNumber1)
        }else if let buildingNumber2 = dict["buildingNumber"] as? String{
            self.buildingNumber = buildingNumber2
        }
        
        if let businesspostalCode1 = dict["businesspostalCode"] as? Int{
            self.businesspostalCode = String(businesspostalCode1)
        }else if let businesspostalCode2 = dict["businesspostalCode"] as? String{
            self.businesspostalCode = businesspostalCode2
        }
        
        if let certificateCount1 = dict["certificateCount"] as? Int{
            self.certificateCount = String(certificateCount1)
        }else if let certificateCount2 = dict["certificateCount"] as? String{
            self.certificateCount = certificateCount2
        }
        
        if let contactNo1 = dict["contactNo"] as? Int{
            self.contactNo = String(contactNo1)
        }else if let contactNo2 = dict["contactNo"] as? String{
            self.contactNo = contactNo2
        }
        
        if let countryCode1 = dict["countryCode"] as? Int{
            self.countryCode = String(countryCode1)
        }else if let countryCode2 = dict["countryCode"] as? String{
            self.countryCode = countryCode2
        }
        
        if let dob1 = dict["dob"] as? Int{
            self.dob = String(dob1)
        }else if let dob2 = dict["dob"] as? String{
            self.dob = dob2
        }
        
        if let followersCount1 = dict["followersCount"] as? Int{
            self.followersCount = String(followersCount1)
        }else if let followersCount2 = dict["followersCount"] as? String{
            self.followersCount = followersCount2
        }
        
        if let favoriteStatus = dict["favoriteStatus"] as? Int{
            self.favoriteStatus = String(favoriteStatus)
        }else if let favoriteStatus = dict["favoriteStatus"] as? String{
            self.favoriteStatus = favoriteStatus
        }
        
        if let followerStatus = dict["followerStatus"] as? Int{
            self.followerStatus = String(followerStatus)
        }else if let followerStatus = dict["followerStatus"] as? String{
            self.followerStatus = followerStatus
        }
        
        if let followingCount1 = dict["followingCount"] as? Int{
            self.followingCount = String(followingCount1)
        }else if let followingCount2 = dict["followingCount"] as? String{
            self.followingCount = followingCount2
        }
        
        if let postCount1 = dict["postCount"] as? Int{
            self.postCount = String(postCount1)
        }else if let postCount2 = dict["postCount"] as? String{
            self.postCount = postCount2
        }
        
        if let ratingCount1 = dict["ratingCount"] as? Int{
            self.ratingCount = String(ratingCount1)
        }else if let ratingCount2 = dict["ratingCount"] as? String{
            self.ratingCount = ratingCount2
        }else if let ratingCount3 = dict["ratingCount"] as? Double{
            self.ratingCount = String(ratingCount3)
        }
        
        if let reviewCount1 = dict["reviewCount"] as? Int{
            self.reviewCount = String(reviewCount1)
        }else if let reviewCount2 = dict["reviewCount"] as? String{
            self.reviewCount = reviewCount2
        }
        
        if let serviceCount1 = dict["serviceCount"] as? Int{
            self.serviceCount = String(serviceCount1)
        }else if let serviceCount2 = dict["serviceCount"] as? String{
            self.serviceCount = serviceCount2
        }
    }
}

/*
{
    "_id" = 10;
    address = "MINDIII Systems Pvt. Ltd., Brajeshwari Extension, Greater Brajeshwari, Indore, Madhya Pradesh, India";
    address2 = "";
    bio = "";
    buildingNumber = 11;
    businessName = "prish glamour";
    businessType = business;
    businesspostalCode = "";
    certificateCount = 2;
    contactNo = 7869116004;
    countryCode = "+91";
    dob = "2000-06-04";
    email = "arvind@gmail.com";
    favoriteStatus = 0;
    firstName = Prish;
    followerStatus = 0;
    followersCount = 0;
    followingCount = 0;
    gender = male;
    isCertificateVerify = 0;
    lastName = Patidar;
    postCount = 0;
    profileImage = "http://koobi.co.uk:5000/uploads/profile/1524476749062.jpg";
    radius = 5;
    ratingCount = 0;
    reviewCount = 0;
    serviceCount = 2;
    serviceType = 3;
    userName = prish;
    userType = artist;
}
*/
