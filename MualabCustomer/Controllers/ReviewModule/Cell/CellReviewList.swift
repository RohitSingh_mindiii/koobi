//
//  CellReviewList.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/5/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView

class CellReviewList: UITableViewCell {
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblReviewDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
