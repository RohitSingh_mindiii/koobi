//
//  ModelReview.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/6/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelReview: NSObject {
    var artistId = ""
    var artistRating = ""
    var artistRatingCrd = ""
    var reviewByArtist = ""
    var reviewByUser = ""
    var arrUserDetail = [UserDetail]()
    var arrArtistDetail = [UserDetail]()
    var _id = ""
    var userRating = ""
    var userRatingCrd = ""
    init(dict: [String : Any]) {
    
        if let status = dict["artistId"] as? String{
            artistId = status
        }else if let status = dict["artistId"] as? Int{
            artistId = String(status)
        }
        
        if let status = dict["rating"] as? String{
            artistRating = status
        }else if let status = dict["rating"] as? Int{
            artistRating = String(status)
        }
        
        if let status = dict["crd"] as? String{
            artistRatingCrd = status
        }else if let status = dict["crd"] as? Int{
            artistRatingCrd = String(status)
        }
        
        if let status = dict["review"] as? String{
            reviewByArtist = status
        }else if let status = dict["review"] as? Int{
            reviewByArtist = String(status)
        }
        
        if let status = dict["review"] as? String{
            reviewByUser = status
        }else if let status = dict["review"] as? Int{
            reviewByUser = String(status)
        }

        if let status = dict["_id"] as? String{
            _id = status
        }else if let status = dict["_id"] as? Int{
            _id = String(status)
        }
              ////
        
        if let status = dict["rating"] as? String{
            userRating = status
        }else if let status = dict["rating"] as? Int{
            userRating = String(status)
        }
        
        if let status = dict["crd"] as? String{
            userRatingCrd = status
        }else if let status = dict["crd"] as? Int{
            userRatingCrd = String(status)
        }
        
        if let array = dict["userDetail"] as? [[String:Any]]{
            for obj in array{
                let object = UserDetail.init(dict: obj)
                self.arrUserDetail.append(object)
            }
        }
        
        if let array = dict["artistDetail"] as? [[String:Any]]{
            for obj in array{
                let object = UserDetail.init(dict: obj)
                self.arrArtistDetail.append(object)
            }
        }
    }
}
