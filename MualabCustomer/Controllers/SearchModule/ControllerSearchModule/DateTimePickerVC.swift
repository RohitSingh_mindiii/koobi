//
//  DateTimePickerVC.swift
//  MualabCustomer
//
//  Created by Mac on 01/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol DateTimePickerVCDelegate {
    func finishDateSectionWithDate(forShow: String, forServer: String)
}

class DateTimePickerVC: UIViewController {
    
    weak var delegate: DateTimePickerVCDelegate?
    @IBOutlet weak var picker : UIDatePicker!
    @IBOutlet weak var bottomConstraint : NSLayoutConstraint!
    let dateFormator : DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.bottomConstraint.constant = 0
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureView(){
        picker.date = Date()
        picker.datePickerMode = .dateAndTime
        
        dateFormator.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormator.dateFormat = "dd-MM-yyyy hh:mm a"
        
        picker.minimumDate = Date()
        self.bottomConstraint.constant = -self.view.frame.size.height/2
    }
}

// MARK: - IBActions
extension DateTimePickerVC{
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        var strDOBForToShow = ""
        var strDOBForServer = ""
        
        if sender.tag == 1 {
            strDOBForToShow = dateFormator.string(from: self.picker.date)
            strDOBForServer = dateFormator.string(from: self.picker.date)
        }
     
        delegate?.finishDateSectionWithDate(forShow: strDOBForToShow, forServer: strDOBForServer)
    }
}
