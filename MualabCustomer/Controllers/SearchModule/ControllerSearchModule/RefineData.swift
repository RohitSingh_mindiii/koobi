//
//  RefineData.swift
//  MualabCustomer
//
//  Created by Mac on 01/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class RefineData {
    
    var isOutCallSelected : Bool = false
    var strLocationText : String = "Location"
    var strAllBusinessTypes = ""
    var strAllCategories = ""
    var strAllCertificates = ""
    var strLatitude = ""
    var strLongitude = ""
    var strDistance = ""
    var strPrice = ""
    var strMinPrice = ""
    var strRating = ""
    var strDateTime = "Date And Time"
    var arrAllServices : [ServiceList] = [ServiceList]()
    var arrAllCertificates = [Certificate]()

    var isPriceSelected : Bool  = false
    var isAscending  : Bool = true
}


