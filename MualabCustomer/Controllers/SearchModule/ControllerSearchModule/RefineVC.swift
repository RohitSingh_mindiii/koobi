//
//  RefineVC.swift
//  MualabCustomer
//
//  Created by Mac on 30/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import GooglePlaces
import AVFoundation
import HCSStarRatingView
import WWCalendarTimeSelector
import RangeSeekSlider

class RefineVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,WWCalendarTimeSelectorProtocol ,RangeSeekSliderDelegate{
    
    //deependra design outlet
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var viewDistance: UIView!
    @IBOutlet weak var viewAsenDecn: UIView!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    
    @IBOutlet weak var viewRadiusChoose: UIView!
    @IBOutlet weak var viewLabelRadiusMiles: UIView!
    @IBOutlet weak var viewSlideRadiusMile: UIView!
    @IBOutlet weak var lblMileDistance: UILabel!
    @IBOutlet weak var sliderRadius: customSlider!
    
    @IBOutlet weak var viewPriceChoose: UIStackView!
    @IBOutlet weak var viewLabelPriceEuro: UIView!
    @IBOutlet weak var viewSlidePriceEuro: UIView!
    @IBOutlet weak var lblPriceEuro: UILabel!
    @IBOutlet weak var sliderPrice: RangeSeekSlider!
    
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var txtTableSearch: UITextField!
    
    @IBOutlet weak var lblPriceMinValue: UILabel!
    @IBOutlet weak var lblPriceMaxValue: UILabel!
    
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var viewBusinessTypeNew: UIView!
    @IBOutlet weak var viewServiceTypeNew: UIView!
    @IBOutlet weak var viewLocationNew: UIView!
    @IBOutlet weak var viewDateTimeNew: UIView!
    
    //Certificate
    @IBOutlet weak var lblCertificate: UILabel!
    @IBOutlet weak var viewCertificateNew: UIView!
    
    //OutCall
    @IBOutlet weak var imgVwOutCall: UIImageView!
    @IBOutlet weak var lblOutCall: UILabel!
    @IBOutlet weak var lblRadiusSlider: UILabel!
    
    //table outlat
    @IBOutlet weak var vwTableCointaner: UIView!
    @IBOutlet weak var tblBookingList: UITableView!
    //@IBOutlet weak var tblHeightConst: NSLayoutConstraint!
    
    var kOpenSectionTag: Int = 0;
    fileprivate var arrCategories = [String]()
    fileprivate var arrSubCategories=[String]()
    fileprivate var expandedSectionHeaderNumber: Int = -1
    var sectionItems: Array<Any> = []
    var fromTableBusinessType = true
    var selectedTable = -1
    
    var strText = ""
    var isOutCallSelected:Bool = false
    let imgCircleSelected : UIImage = UIImage.init(named: "active_check_box_icon")!
    let imgCircleUnselected : UIImage = UIImage.init(named: "inactive_check_box_icon")!
    
    var timeSelector: WWCalendarTimeSelector!
    
    //Location
    @IBOutlet weak var lblLocation: UILabel!
    var strLatitude : String =  ""
    var strLongitude : String = ""
    var isGoForLocation : Bool =  false
    
    //DateTime
    @IBOutlet weak var lblDateTime: UILabel!
    
    //Price
    @IBOutlet weak var imgVwPrice: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    var isPriceSelected:Bool = false
    let imgPriceSelected : UIImage = UIImage.init(named: "coin_icon")!
    let imgPriceUnselected : UIImage = UIImage.init(named: "coin_icon")!
    
    //Distance
    @IBOutlet weak var imgVwDistance: UIImageView!
    @IBOutlet weak var lblDistance: UILabel!
    var isDistanceSelected:Bool = false
    let imgDistanceSelected : UIImage = UIImage.init(named: "active_route_ico")!
    let imgDistanceUnselected : UIImage = UIImage.init(named: "route_ico")!
    
    //Ascending/Descending
    @IBOutlet weak var imgVwAscending: UIImageView!
    @IBOutlet weak var imgVwDescending: UIImageView!
    
    @IBOutlet weak var lblAscending: UILabel!
    @IBOutlet weak var lblDescending: UILabel!
    
    let radioImgSelected : UIImage = UIImage.init(named: "active_check_box1")!
    let radioImgUnselected : UIImage = UIImage.init(named: "inactive_check_box1")!
    
    var isAscending:Bool = true
    var arrAllServices : [ServiceList] = [ServiceList]()
    var arrSubService : [SubServiceList] = [SubServiceList]()
    var arrFilterAllServices : [ServiceList] = [ServiceList]()
    var arrFilterSubService : [SubServiceList] = [SubServiceList]()
    
    var arrCertificates = [Certificate]()
    var arrFilteredCertificates = [Certificate]()
    
    func addGasturesToViews() {
        
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTapNew(gestureRecognizer:)))
        //vwTableCointaner.addGestureRecognizer(loginTap)
    }
    @objc func handleImgCamTapNew(gestureRecognizer: UIGestureRecognizer) {
        self.txtTableSearch.resignFirstResponder()
    }
}

// MARK:- View Life Cycle
extension RefineVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidesBottomBarWhenPushed = true;
        self.configureView()
        self.manageRangSlider()
        self.hiddenViewAlwais()
        self.hiddenViewDefault()
        self.addGasturesToViews()
        self.viewPriceChoose.isHidden = false
        self.viewSlidePriceEuro.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //objLocationManager.getCurrentLocation()
        self.vwTableCointaner.isHidden = true
        if !isGoForLocation{
            if objAppShareData.isSearchingUsingFilter{
                self.setPrefilledData()
            }else{
                self.vwRating.value = 0
                self.viewRadiusChoose.isHidden = false
                self.viewSlideRadiusMile.isHidden = false
                self.viewLabelRadiusMiles.isHidden = true
                self.sliderRadius.value = 5.0
                let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
                self.lblRadiusSlider.text = strForThumbImage //+ " miles"
                
                let thumbImage = textToImage(drawText: strForThumbImage, inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
                self.sliderRadius.setThumbImage(thumbImage, for:.normal)
                if self.arrAllServices.count == 0{
                    callWebserviceForGetArtistServices()
                }
                
                if self.arrCertificates.count == 0{
                    callWebservicCertificateList()
                }
                
            }
        }else{
            isGoForLocation = false
        }
        self.sliderRadius.setThumbImage(UIImage.init(named:"Dote_small"), for:.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) { //code
        self.lblPriceMinValue.text = "£" + String(format: "%.0f", self.sliderPrice.selectedMinValue)
        self.lblPriceMaxValue.text = "£" + String(format: "%.0f",self.sliderPrice.selectedMaxValue)
    }
    
}

// MARK: -  Text field methods
extension RefineVC{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtTableSearch{
            self.txtTableSearch.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.txtTableSearch.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //if fromTableBusinessType == true{
        if selectedTable == 1{
            self.arrFilterAllServices.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            
            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
            let filteredArray = self.arrAllServices.filter(){ $0.title.localizedCaseInsensitiveContains(trimmedString) }
            //let filteredArray = self.arrAllServices.filter(){ $0.title.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            self.strText = trimmedString
            //self.strText = substring
            self.arrFilterAllServices = filteredArray
            if self.arrFilterAllServices.count == 0 && self.strText.count != 0{
                self.lblNoRecordFound.isHidden = false
                self.tblBookingList.isHidden = true
            }else{
                if strText.count == 0{
                    self.arrFilterAllServices = self.arrAllServices
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }else{
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }
            }
            self.tblBookingList.reloadData()
            return  true
        }else if selectedTable == 2{
            self.arrFilterSubService.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
            let filteredArray = self.arrSubService.filter(){ $0.title.localizedCaseInsensitiveContains(trimmedString) }
            //let filteredArray = self.arrSubService.filter(){ $0.title.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            //self.strText = substring
            self.strText = trimmedString
            self.arrFilterSubService = filteredArray
            if self.arrFilterSubService.count == 0 && self.strText.count != 0{
                self.lblNoRecordFound.isHidden = false
                self.tblBookingList.isHidden = true
            }else{
                if strText.count == 0{
                    self.arrFilterSubService = self.arrSubService
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }else{
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }
            }
            self.tblBookingList.reloadData()
            return  true
        }
        else{
            self.arrFilteredCertificates.removeAll()
            let text = textField.text! as NSString
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            
            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
            let filteredArray = self.arrCertificates.filter(){ $0.title.localizedCaseInsensitiveContains(trimmedString) }
            //let filteredArray = self.arrAllServices.filter(){ $0.title.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            self.strText = trimmedString
            //self.strText = substring
            self.arrFilteredCertificates = filteredArray
            if self.arrFilteredCertificates.count == 0 && self.strText.count != 0{
                self.lblNoRecordFound.isHidden = false
                self.tblBookingList.isHidden = true
            }else{
                if strText.count == 0{
                    self.arrFilteredCertificates = self.arrCertificates
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }else{
                    self.lblNoRecordFound.isHidden = true
                    self.tblBookingList.isHidden = false
                }
            }
            self.tblBookingList.reloadData()
            return  true
        }
    }
}


// MARK: - Slidet button and method
extension RefineVC{
    @IBAction func ratingValueChanged(_ sender: HCSStarRatingView) {
        self.vwRating.value = sender.value
    }
    @IBAction func sliderValueChangedRadius(_ sender: UISlider) {
        let aSlider = sender
        let a = Int(aSlider.value)
        
        ////
        if a == 1{
            self.sliderRadius.minimumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.sliderRadius.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else if a == 20{
            self.sliderRadius.minimumTrackTintColor = appColor
            self.sliderRadius.maximumTrackTintColor = appColor
        }else{
            self.sliderRadius.minimumTrackTintColor = appColor
            self.sliderRadius.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        ////
        
        let strForThumbImage = String(format: "%.0f", ceil(aSlider.value*100)/100)
        self.lblMileDistance.text = "1-"+String(strForThumbImage)+" Miles"
        
        let thumbImage = textToImage(drawText: "", inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        let thumbImageHigh = textToImage(drawText: strForThumbImage, inImage: #imageLiteral(resourceName: "gubbara_ico_PINNew") , atPoint: CGPoint.init(x:12,y:0))
        aSlider.setThumbImage(thumbImageHigh, for:.highlighted)
        aSlider.setThumbImage(thumbImage, for:.normal)
        aSlider.setThumbImage(thumbImage, for:.selected)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        aSlider.tintColor = UIColor.black
        let radius = strForThumbImage
    }
    
    func setValueSlider(){
        if objAppShareData.objRefineData.strPrice.count==0{
            objAppShareData.objRefineData.strPrice = "1"
        }
        if objAppShareData.objRefineData.strMinPrice.count==0{
            objAppShareData.objRefineData.strMinPrice = "1"
        }
        if objAppShareData.objRefineData.strDistance.count==0{
            objAppShareData.objRefineData.strDistance = "0"
        }
        
        //// Himanshu
        self.sliderPrice.selectedMaxValue = CGFloat(Float(objAppShareData.objRefineData.strPrice)!)
        self.sliderPrice.selectedMinValue = CGFloat(Float(objAppShareData.objRefineData.strMinPrice)!)
        ////
        
        self.sliderRadius.value = Float(objAppShareData.objRefineData.strDistance)!
        let strForThumbImageNew = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
        self.lblRadiusSlider.text = strForThumbImageNew //+ " miles"
        let thumbImageNew = textToImage(drawText: strForThumbImageNew, inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        sliderRadius.setThumbImage(thumbImageNew, for:.normal)
        
        if objAppShareData.objRefineData.strDistance.count>0 && objAppShareData.objRefineData.strDistance != "0"{
            self.lblMileDistance.text = "1-" + objAppShareData.objRefineData.strDistance + " Miles"
        }else{
            self.lblMileDistance.text = "0-0 Miles"
        }
        
        if objAppShareData.objRefineData.strPrice.count>0 && objAppShareData.objRefineData.strPrice != "0"{
            self.lblPriceEuro.text = "£1-" + "£" +  objAppShareData.objRefineData.strPrice
            self.lblPriceEuro.text = "£" + objAppShareData.objRefineData.strMinPrice  + "-£" +  objAppShareData.objRefineData.strPrice
        }else{
            self.lblPriceEuro.text = "£1-£1"
        }
    }
    
    @IBAction func sliderValueChangedPrice(_ sender: RangeSeekSlider) {
        self.lblPriceMinValue.text = "£" + String(format: "%.0f", ceil(self.sliderPrice.selectedMinValue))
        self.lblPriceMaxValue.text = "£" + String(format: "%.0f", ceil(self.sliderPrice.selectedMaxValue))
        
    }
    
    func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name:"Nunito-Medium", size: 11)!
        var t = text
        //        if t == "51"{
        //            t = "50"
        //        }
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        var rect = CGRect(origin: point, size: image.size)
        if text.count == 1{
            //rect = CGRect(origin: CGPoint(x:10,y:8), size: image.size)
            rect = CGRect(origin: CGPoint(x:18,y:8), size: image.size)
        }else if text.count == 2 {
            //rect = CGRect(origin: CGPoint(x:8,y:8), size: image.size)
            rect = CGRect(origin: CGPoint(x:15,y:8), size: image.size)
        }else if text.count == 3 {
            //rect = CGRect(origin: CGPoint(x:6,y:8), size: image.size)
            rect = CGRect(origin: CGPoint(x:13,y:8), size: image.size)
        }else{
            rect = CGRect(origin: point, size: image.size)
        }
        t.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //extenstion may not containt stored properties so use in main block
    func getSelectedServiceIDs()-> (strSelectedMainServiceIDs: String, strSelectedSubServiceIDs: String){
        
        var strSelectedMainServiceIDs = ""
        var strSelectedSubServiceIDs = ""
        
        var arrTempAllServices = [String]()
        var arrTempSubServiceIDs = [String]()
        
        if self.arrFilterAllServices.count > 0 {
            for objServiceList in arrFilterAllServices{
                if objServiceList.arrSubService.count > 0{
                    for objSubServiceList in objServiceList.arrSubService{
                        if objSubServiceList.isSelected {
                            
                            let strSubServiceId = "\(objSubServiceList._id)"
                            if !arrTempSubServiceIDs.contains(strSubServiceId){
                                arrTempSubServiceIDs.append(strSubServiceId)
                            }
                            
                            
                            let strMainServiceId = "\(objServiceList._id)"
                            
                            if !arrTempAllServices.contains(strMainServiceId){
                                arrTempAllServices.append(strMainServiceId)
                            }
                        }else{
                            if objServiceList.isSelectLocal {
                                let strMainServiceId = "\(objServiceList._id)"
                                if !arrTempAllServices.contains(strMainServiceId){
                                    arrTempAllServices.append(strMainServiceId)
                                }
                            }
                        }
                    }
                }else{
                    if objServiceList.isSelectLocal {
                        let strMainServiceId = "\(objServiceList._id)"
                        if !arrTempAllServices.contains(strMainServiceId){
                            arrTempAllServices.append(strMainServiceId)
                        }
                    }
                }
            }
        }
        
        if arrTempAllServices.count > 0 {
            strSelectedMainServiceIDs = arrTempAllServices.joined(separator: ", ")
            strSelectedSubServiceIDs = arrTempSubServiceIDs.joined(separator: ", ")
        }
        return (strSelectedMainServiceIDs, strSelectedSubServiceIDs )
    }
    
    func getSelectedCertificatesId() -> String {
        var strSelectedCertificateIDs = ""
        var arrTempCertIds = [String]()
        if self.arrFilteredCertificates.count>0{
            for objCertificate in self.arrFilteredCertificates{
                if objCertificate.isSelectLocal {
                    let strId = "\(objCertificate._id)"
                    if !arrTempCertIds.contains(strId){
                        arrTempCertIds.append(strId)
                    }
                }
            }
            
            if arrTempCertIds.count>0{
                strSelectedCertificateIDs = arrTempCertIds.joined(separator: ", ")
            }
        }
        return strSelectedCertificateIDs
    }
}

// MARK: - IBActions
extension RefineVC{
    @IBAction func btnOpenRadiusSlider(_ sender: UIButton) {
        self.viewSlideRadiusMile.isHidden = false
        self.viewLabelRadiusMiles.isHidden = true
        self.sliderRadius.value = Float(objAppShareData.objRefineData.strDistance)!
        let strForThumbImage = String(format: "%.0f", ceil(self.sliderRadius.value*100)/100)
        self.lblRadiusSlider.text = strForThumbImage //+ " miles"
        let thumbImage = textToImage(drawText: strForThumbImage, inImage: #imageLiteral(resourceName: "Dote_small") , atPoint: CGPoint.init(x:12,y:-2))
        self.sliderRadius.setThumbImage(thumbImage, for:.normal)
    }
    @IBAction func btnOpenPriceSlider(_ sender: UIButton) {
        self.viewSlidePriceEuro.isHidden = false
        self.viewLabelPriceEuro.isHidden = true
        
        //// Himanshu
        self.sliderPrice.selectedMaxValue = CGFloat(Float(objAppShareData.objRefineData.strPrice)!)
        self.sliderPrice.selectedMinValue = CGFloat(Float(objAppShareData.objRefineData.strMinPrice)!)
        
        ////
    }
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHiddenKeyboardAction(_ sender: Any){
        self.txtTableSearch.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @IBAction func btnCancelTableAction(_ sender: Any){
        self.view.endEditing(true)
        self.vwTableCointaner.isHidden = true
        
        if self.arrAllServices.count > 0{
            for objArrAll in self.arrAllServices{
                if objArrAll.isSelect == false{
                    objArrAll.isSelectLocal = false
                    for obj in objArrAll.arrSubService{
                        obj.isSelectLocal = false
                        obj.isSelected = false
                    }
                }else{
                    objArrAll.isSelectLocal = true
                    for obj in objArrAll.arrSubService{
                        if obj.isSelected == false{
                            obj.isSelectLocal = false
                        }else{
                            obj.isSelectLocal = true
                        }
                    }
                }
            }
        }
        
        if self.arrFilterAllServices.count > 0{
            
            for objArrAll in self.arrFilterAllServices{
                
                if objArrAll.isSelect == false{
                    objArrAll.isSelectLocal = false
                    for obj in objArrAll.arrSubService{
                        obj.isSelectLocal = false
                        obj.isSelected = false
                    }
                }else{
                    objArrAll.isSelectLocal = true
                    for obj in objArrAll.arrSubService{
                        if obj.isSelected == false{
                            obj.isSelectLocal = false
                        }else{
                            obj.isSelectLocal = true
                        }
                    }
                }
            }
        }
        
        //Cetificates
        
        if self.arrCertificates.count > 0{
            for objArrAll in self.arrCertificates{
                if objArrAll.isSelected == false{
                    objArrAll.isSelectLocal = false
                }else{
                    objArrAll.isSelectLocal = true
                }
            }
        }
        
        
    }
    
    @IBAction func btnDoneTableAction(_ sender: Any){
        self.view.endEditing(true)
        self.vwTableCointaner.isHidden = true
        
        var arrBusinessData = [String]()
        var arrServiceData = [String]()
        var arrCertificateData = [String]()
        
        if self.arrAllServices.count > 0{
            
            for objArrAll in self.arrAllServices{
                
                if objArrAll.isSelectLocal == false{
                    objArrAll.isSelect = false
                    for obj in objArrAll.arrSubService{
                        obj.isSelectLocal = false
                        obj.isSelected = false
                    }
                }else{
                    objArrAll.isSelect = true
                    let name = objArrAll.title
                    arrBusinessData.append(name)
                    for obj in objArrAll.arrSubService{
                        if obj.isSelectLocal == false{
                            obj.isSelected = false
                        }else{
                            obj.isSelected = true
                            let titel = obj.title
                            arrServiceData.append(titel)
                        }
                    }
                }
            }
            if arrBusinessData.count>0{
                self.viewBusinessTypeNew.isHidden = false
                self.lblBusinessType.text = arrBusinessData.joined(separator: ", ")
            }else{
                self.viewBusinessTypeNew.isHidden = true
                //self.lblBusinessType.text = "Business Type"
                self.lblBusinessType.text = "Service Category"
            }
            
            if arrServiceData.count>0{
                self.viewServiceTypeNew.isHidden = false
                self.lblServiceType.text = arrServiceData.joined(separator: ", ")
            }else{
                self.viewServiceTypeNew.isHidden = true
                self.lblServiceType.text = "Services"
            }
        }else{
            self.viewServiceTypeNew.isHidden = true
            self.viewBusinessTypeNew.isHidden = true
            //self.lblBusinessType.text = "Business Type"
            self.lblBusinessType.text = "Service Category"
            self.lblServiceType.text = "Services"
        }
        
        if self.arrFilterAllServices.count > 0{
            
            for objArrAll in self.arrFilterAllServices{
                
                if objArrAll.isSelectLocal == false{
                    objArrAll.isSelect = false
                    for obj in objArrAll.arrSubService{
                        obj.isSelectLocal = false
                        obj.isSelected = false
                    }
                }else{
                    objArrAll.isSelect = true
                    let name = objArrAll.title
                    arrBusinessData.append(name)
                    for obj in objArrAll.arrSubService{
                        if obj.isSelectLocal == false{
                            obj.isSelected = false
                        }else{
                            obj.isSelected = true
                            let titel = obj.title
                            arrServiceData.append(titel)
                        }
                    }
                }
            }
        }
        //Cetificates
        if self.arrFilteredCertificates.count > 0{
            
            for objArrAll in self.arrFilteredCertificates{
                if objArrAll.isSelectLocal == false{
                    objArrAll.isSelected = false
                }else{
                    objArrAll.isSelected = true
                    let name = objArrAll.title
                    arrCertificateData.append(name)
                }
            }
            
            if arrCertificateData.count>0{
                self.viewCertificateNew.isHidden = false
                self.lblCertificate.text = arrCertificateData.joined(separator: ", ")
            }else{
                self.viewCertificateNew.isHidden = true
                self.lblCertificate.text = "Certificate"
            }
        }
        
    }
    
    @IBAction func btnProfileAction(_ sender: Any){
        self.viewPriceChoose.isHidden = false
        self.viewSlidePriceEuro.isHidden = false
        self.viewLabelPriceEuro.isHidden = false
    }
    
    @IBAction func btnOutCallAction(_ sender: UIButton){
        self.view.endEditing(true)
        isOutCallSelected = !isOutCallSelected
        setOutCallUI()
    }
    
    @IBAction func btnLocationAction(_ sender: Any){
        self.view.endEditing(true)
        locationAuthorization()
    }
    
    @IBAction func btnDateAndTimeAction(_ sender: Any){
        self.view.endEditing(true)
        self.showCalendar()
        //self.goToCalenderScreen()
        //gotoDateTimePickerVC()
    }
    func showCalendar() {
        self.timeSelector = WWCalendarTimeSelector.instantiate()
        self.timeSelector.delegate = self
        /*
         Any other options are to be set before presenting selector!
         */
        if (self.lblDateTime.text != "Date and Time") && ((self.lblDateTime.text?.count)!>0){
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatter.dateFormat = "dd/MM/yyyy hh:mm a"
            self.timeSelector.optionCurrentDate = formatter.date(from: self.lblDateTime.text!)!
        }
        timeSelector.modalPresentationStyle = .fullScreen
        present(self.timeSelector, animated: true, completion: nil)
    }
    
    @objc func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print(date)
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd/MM/yyyy hh:mm a"
        self.lblDateTime.text = formatter.string(from: date)
        self.viewDateTimeNew.isHidden = false
    }
    
    @IBAction func btnBusinessTypeAction(_ sender: Any){
        self.strText = ""
        self.txtTableSearch.text = ""
        self.lblNoRecordFound.isHidden = true
        self.tblBookingList.isHidden = false
        self.arrFilterAllServices = self.arrAllServices
        
        //self.lblTableHeader.text = "Select Business Type"
        //self.txtTableSearch.placeholder = "Search Business Type"
        self.lblTableHeader.text = "Select Service Category"
        self.txtTableSearch.placeholder = "Search Service Category"
        
        //fromTableBusinessType = true
        selectedTable = 1
        if arrAllServices.count > 0{
            self.vwTableCointaner.isHidden = false
            self.tblBookingList.reloadData()
        }else{
            self.vwTableCointaner.isHidden = true
            callWebserviceForGetArtistServices()
        }
    }
    
    
    
    @IBAction func btnServiceAction(_ sender: Any){
        self.strText = ""
        self.txtTableSearch.text = ""
        self.lblNoRecordFound.isHidden = true
        self.tblBookingList.isHidden = false
        
        self.lblTableHeader.text = "Select services"
        self.txtTableSearch.placeholder = "Search services"
        
        //fromTableBusinessType = false
        selectedTable = 2
        self.arrSubService.removeAll()
        self.arrFilterSubService.removeAll()
        if arrFilterAllServices.count > 0{
            self.arrSubService.removeAll()
            self.arrFilterSubService.removeAll()
            var isBusinessType = false
            for obj in self.arrFilterAllServices{
                if obj.isSelect{
                    isBusinessType = true
                    for newObj in obj.arrSubService{
                        self.arrSubService.append(newObj)
                        self.arrFilterSubService.append(newObj)
                    }
                }
            }
            
            //// Himanshu
            //            if self.arrFilterAllServices.count > 0{
            //                self.tblBookingList.reloadData()
            //                self.vwTableCointaner.isHidden = false
            //            }
            ////
            
            if arrSubService.count > 0{
                self.tblBookingList.reloadData()
                self.vwTableCointaner.isHidden = false
            }else{
                if isBusinessType{
                    objAppShareData.showAlert(withMessage: "No service available", type: alertType.bannerDark, on: self)
                }else{
                    objAppShareData.showAlert(withMessage: "Please select category", type: alertType.bannerDark, on: self)
                }
            }
        }else{
            //objAppShareData.showAlert(withMessage: "Please select business type", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnCertificatesAction(_ sender: Any){
        self.strText = ""
        self.txtTableSearch.text = ""
        self.lblNoRecordFound.isHidden = true
        self.tblBookingList.isHidden = false
        
        self.arrFilteredCertificates = self.arrCertificates
        self.lblTableHeader.text = "Select Qualifications"
        self.txtTableSearch.placeholder = "Search Qualifications"
        
        //fromTableBusinessType = true
        selectedTable = 3
        if arrCertificates.count > 0{
            self.vwTableCointaner.isHidden = false
            self.tblBookingList.reloadData()
        }else{
            self.vwTableCointaner.isHidden = true
            objAppShareData.showAlert(withMessage: "No qualifications added", type: alertType.bannerDark, on: self)
            //callWebservicCertificateList()
        }
    }
    
    @IBAction func btnPriceAction(_ sender: Any){
        isPriceSelected = !isPriceSelected
        self.viewPriceChoose.isHidden = false
        self.viewSlidePriceEuro.isHidden = false
        self.viewLabelPriceEuro.isHidden = true
    }
    
    @IBAction func btnDistanceAction(_ sender: Any){
        isPriceSelected = !isPriceSelected
    }
    
    @IBAction func btnAscendingAction(_ sender: Any){
        isAscending = true
        setBtnAscenDescendingImage()
    }
    
    @IBAction func btnDescendingAction(_ sender: Any){
        isAscending = false
        setBtnAscenDescendingImage()
    }
    @IBAction func btnQualificationAction(_ sender: Any){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnClearAllFilterFAction(_ sender: Any){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        objAppShareData.objRefineData = RefineData()
        objAppShareData.dictFilter = ["":""]
        objAppShareData.isSearchingUsingFilter = false
        self.hiddenViewDefault()
        self.resetAllDataToDefault()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000)) {
            objWebserviceManager.StopIndicator()
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyFilterAction(_ sender: Any){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        self.viewSlideRadiusMile.isHidden = true
        self.viewLabelRadiusMiles.isHidden = false
        self.viewSlidePriceEuro.isHidden = true
        self.viewLabelPriceEuro.isHidden = false
        
        self.createFinalDict()
        objAppShareData.isSearchingUsingFilter = true
        objAppShareData.isTabChange = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000)) {
            objWebserviceManager.StopIndicator()
        }
        navigationController?.popViewController(animated: true)
    }
}

// MARK:- Custom Methods
extension RefineVC{
    
    func configureView(){
        
        isAscending = true
        setBtnAscenDescendingImage()
        
        isOutCallSelected = false
        setOutCallUI()
        
        isPriceSelected = false
        
        self.sliderPrice.delegate = self
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.txtTableSearch.delegate = self
        
        let nib = UINib(nibName: "CategoriesHeader", bundle: nil)
        self.tblBookingList.register(nib, forHeaderFooterViewReuseIdentifier: "CategoriesHeader")
        self.lblLocation.text! = objAppShareData.objRefineData.strLocationText
        self.viewLocationNew.isHidden = false
        strLatitude = objAppShareData.objRefineData.strLatitude
        strLongitude = objAppShareData.objRefineData.strLongitude
        //self.tblHeightConst.constant = 0
    }
    
    func manageRangSlider(){
        //
        //    self.sliderPrice.delegate = self
        //    self.sliderRadius.delegate = self
        self.sliderRadius.minimumTrackTintColor = appColor
        self.sliderRadius.maximumTrackTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        self.sliderRadius.thumbTintColor = appColor
        //self.sliderRadius.minimumValue = 0
        self.sliderRadius.minimumValue = 1
        self.sliderRadius.maximumValue = 20
        
        self.sliderRadius.setThumbImage(#imageLiteral(resourceName: "gubbara_ico_PINNew") , for:.highlighted)
        self.sliderRadius.setThumbImage(#imageLiteral(resourceName: "gubbara_ico") , for:.selected)
        self.sliderRadius.setThumbImage(#imageLiteral(resourceName: "Dote_small") , for:.normal)
       
    }
    
    func setOutCallUI(){
        if isOutCallSelected{
            imgVwOutCall.image = imgCircleSelected
        }else{
            imgVwOutCall.image = imgCircleUnselected
        }
    }
    
    func hiddenViewAlwais(){
        self.viewDistance.isHidden = true
        self.viewAsenDecn.isHidden = true
    }
    
    func hiddenViewDefault(){
        self.viewRadiusChoose.isHidden = true
        self.viewSlideRadiusMile.isHidden = true
        self.viewLabelRadiusMiles.isHidden = true
        
        self.viewPriceChoose.isHidden = true
        self.viewLabelPriceEuro.isHidden = true
        self.viewSlidePriceEuro.isHidden = true
        self.setValueSlider()
    }
    
    func setBtnAscenDescendingImage(){
        
        if isAscending{
            imgVwAscending.image = radioImgSelected
            imgVwDescending.image = radioImgUnselected
            lblAscending.textColor = UIColor.theameColors.pinkColor
            lblDescending.textColor = UIColor.black
        }else{
            imgVwAscending.image = radioImgUnselected
            imgVwDescending.image = radioImgSelected
            lblAscending.textColor = UIColor.black
            lblDescending.textColor = UIColor.theameColors.pinkColor
        }
    }
}

// MARK:- Custom Methods
extension RefineVC : GMSAutocompleteViewControllerDelegate{
    
    func gotoGMSAutocompleteVC(){
        isGoForLocation = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let add = place.formattedAddress{
            self.lblLocation.text = add
            self.viewRadiusChoose.isHidden = false
            self.viewSlideRadiusMile.isHidden = false
            self.viewLabelRadiusMiles.isHidden = true
            //self.setValueSlider()
        }else{
            self.lblLocation.text = "Location"
            self.viewRadiusChoose.isHidden = true
        }
        if place.coordinate.latitude != 0 && place.coordinate.longitude != 0 {
            strLatitude = "\(place.coordinate.latitude)"
            strLongitude = "\(place.coordinate.longitude)"
        }
        
        if objAppShareData.isSearchingUsingFilter{
            //            objAppShareData.objRefineData.strLatitude = strLatitude
            //            objAppShareData.objRefineData.strLongitude = strLongitude
            //            objAppShareData.objRefineData.strLocationText = self.lblLocation.text!
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

// MARK:- Custom Methods
extension RefineVC : DateTimePickerVCDelegate{
    
    func gotoDateTimePickerVC(){
        
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        if let objVC = sb.instantiateViewController(withIdentifier:"DateTimePickerVC") as? DateTimePickerVC{
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.delegate = self
            objVC.modalPresentationStyle = .fullScreen
            present(objVC, animated: true, completion: nil)
        }
        
    }
    
    func finishDateSectionWithDate(forShow: String, forServer: String) {
        if forShow != "" {
            self.lblDateTime.text = forShow
            self.viewDateTimeNew.isHidden = false
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Expand / Collapse Methods
fileprivate extension RefineVC{
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! CategoriesHeader
        let section    = headerView.tag
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            kOpenSectionTag = section
            tableViewExpandSection(section, headerView: headerView)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                kOpenSectionTag = section
                tableViewCollapeSection(section, headerView: headerView)
            } else {
                let previousHeader = self.tblBookingList.headerView(forSection: kOpenSectionTag) as? CategoriesHeader
                tableViewCollapeSection(kOpenSectionTag,  headerView: previousHeader!)
                tableViewExpandSection(section, headerView: headerView)
                kOpenSectionTag = section
            }
        }
        // self.tblHeightConst.constant = self.tblBookingList.contentSize.height
    }
    
    func tableViewCollapeSection(_ section: Int, headerView: CategoriesHeader ) {
        
        let imageView : UIImageView = headerView.imgDropDown
        let objServiceList : ServiceList = self.arrAllServices[section]
        let sectionData = objServiceList.arrSubService
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            headerView.lblName.textColor = UIColor.black
            headerView.lblBotumeLayer.backgroundColor = UIColor.clear
            
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, headerView: CategoriesHeader) {
        
        let objServiceList : ServiceList = self.arrAllServices[section]
        let sectionData = objServiceList.arrSubService
        
        let imageView : UIImageView = headerView.imgDropDown
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            headerView.lblName.textColor = UIColor.theameColors.pinkColor
            headerView.lblBotumeLayer.backgroundColor = UIColor.theameColors.pinkColor
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblBookingList!.beginUpdates()
            self.tblBookingList!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblBookingList!.endUpdates()
        }
    }
}

//MARK: - UITableview delegate
extension RefineVC {
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if fromTableBusinessType == true{
        //            //return self.arrAllServices.count
        //            return self.arrFilterAllServices.count
        //        } else {
        //            //return self.arrSubService.count
        //            return self.arrFilterSubService.count
        //        }
        
        if selectedTable == 1{
            return self.arrFilterAllServices.count
        } else if selectedTable == 2 {
            return self.arrFilterSubService.count
        } else{
            return self.arrFilteredCertificates.count
        }
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTableCell", for: indexPath) as? SubCategoriesTableCell{
            //            cell.delegate = self
            //            cell.indexPath = indexPath
            //
            /*
             if fromTableBusinessType == true{
             let objServiceList =  self.arrFilterAllServices[indexPath.row]
             cell.lblName.text =  objServiceList.title
             if objServiceList.isSelectLocal{
             cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
             }else{
             cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
             }
             } else {
             let objServiceList =  self.arrFilterSubService[indexPath.row]
             cell.lblName.text =  objServiceList.title
             if objServiceList.isSelectLocal{
             cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
             }else{
             cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
             }
             }
             */
            
            if selectedTable == 1{
                let objServiceList =  self.arrFilterAllServices[indexPath.row]
                cell.lblName.text =  objServiceList.title
                if objServiceList.isSelectLocal{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                }else{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                }
            } else if selectedTable == 2{
                let objServiceList =  self.arrFilterSubService[indexPath.row]
                cell.lblName.text =  objServiceList.title
                if objServiceList.isSelectLocal{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                }else{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                }
            }
            else{
                let objServiceList =  self.arrFilteredCertificates[indexPath.row]
                cell.lblName.text =  objServiceList.title
                if objServiceList.isSelectLocal{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
                }else{
                    cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
                }
                
            }
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.txtTableSearch.resignFirstResponder()
        let cell = (tblBookingList?.cellForRow(at: indexPath) as? SubCategoriesTableCell)!
        
        /*
         if fromTableBusinessType == true{
         
         let objServiceList =  self.arrFilterAllServices[indexPath.row]
         if objServiceList.isSelectLocal == true{
         objServiceList.isSelectLocal = false
         cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
         }else{
         objServiceList.isSelectLocal = true
         cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
         }
         
         } else {
         let objServiceList =  self.arrFilterSubService[indexPath.row]
         if objServiceList.isSelectLocal == true{
         objServiceList.isSelectLocal = false
         cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
         }else{
         objServiceList.isSelectLocal = true
         cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
         }
         
         for obj in self.arrFilterAllServices{
         if obj.isSelect == true{
         for newObj in obj.arrSubService{
         if newObj._id == objServiceList._id{
         newObj.isSelectLocal = objServiceList.isSelectLocal
         }
         }
         }
         }
         
         }
         */
        if selectedTable == 1{
            let objServiceList =  self.arrFilterAllServices[indexPath.row]
            if objServiceList.isSelectLocal == true{
                objServiceList.isSelectLocal = false
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
            }else{
                objServiceList.isSelectLocal = true
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
            }
            
        } else if selectedTable == 2{
            let objServiceList =  self.arrFilterSubService[indexPath.row]
            if objServiceList.isSelectLocal == true{
                objServiceList.isSelectLocal = false
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
            }else{
                objServiceList.isSelectLocal = true
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
            }
            
            for obj in self.arrFilterAllServices{
                if obj.isSelect == true{
                    for newObj in obj.arrSubService{
                        if newObj._id == objServiceList._id{
                            newObj.isSelectLocal = objServiceList.isSelectLocal
                        }
                    }
                }
            }
            
        }else{
            let objServiceList =  self.arrFilteredCertificates[indexPath.row]
            if objServiceList.isSelectLocal == true{
                objServiceList.isSelectLocal = false
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "inactive_check_ico")
            }else{
                objServiceList.isSelectLocal = true
                cell.imgVwCheck?.image = #imageLiteral(resourceName: "ico_check")
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

//// MARK:- SubCategoriesTableCell Delegate
//extension RefineVC : SubCategoriesTableCellDelegate{
//    func btnCheckBoxTapped(at index: IndexPath) {
//        let objServiceList : ServiceList = self.arrAllServices[IndexPath.row]
//        objSubServiceList.isSelected = objServiceList.isSelect
//        self.tblBookingList.reloadRows(at: [index], with: .none)
//    }
//}

extension RefineVC {
    
    func callWebserviceForGetArtistServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        objWebserviceManager.requestGetForJson(strURL: WebURL.allExploreCategory, params: ["":""]  , success: { response in
            //objWebserviceManager.requestPost(strURL: WebURL.allCategory, params: ["":""]  , success: { response in
            print(response)
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebservicCertificateList(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objWebserviceManager.requestGet(strURL: WebURL.certificateList, params: ["":""]  , success: { response in
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    
                    self.arrCertificates.removeAll()
                    self.arrFilteredCertificates.removeAll()
                    
                    if let arr = response["data"] as? [[String : Any]]{
                        
                        for dict in arr{
                            let obj = Certificate.init(dict: dict)
                            self.arrCertificates.append(obj)
                            self.arrFilteredCertificates.append(obj)
                        }
                    }
                    objAppShareData.objRefineData.arrAllCertificates = self.arrCertificates
                    self.tblBookingList.reloadData()
                }else{
                    //objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

extension RefineVC {
    
    func parseResponce(response:[String : Any]){
        self.arrAllServices.removeAll()
        self.arrFilterAllServices.removeAll()
        //if let arr = response["serviceList"] as? [[String : Any]]{
        if let arr = response["data"] as? [[String : Any]]{
            for dict in arr{
                let obj = ServiceList.init(dictionary: dict)
                self.arrAllServices.append(obj)
                self.arrFilterAllServices.append(obj)
            }
        }
        objAppShareData.objRefineData.arrAllServices = self.arrAllServices
        self.tblBookingList.reloadData()
        //self.tblHeightConst.constant = self.tblBookingList.contentSize.height
    }
}

extension RefineVC {
    
    func getTimeFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "hh:mm a"
        let strTime = formatter.string(from: todayDate)
        
        return strTime
    }
    
    func getDayFromSelectedDate(strDate:String)-> String{
        
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        switch weekDay {
        case 1:
            return "6"//"Sun"
        case 2:
            return "0"//Mon"
        case 3:
            return "1"//Tue"
        case 4:
            return "2"//"Wed"
        case 5:
            return "3"//"Thu"
        case 6:
            return "4"//"Fri"
        case 7:
            return "5"//"Sat"
        default:
            //"Error fetching days"
            return "Day"
        }
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        
        // returns an integer from 1 - 7, with 1 being Sunday and 7 being Saturday
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
}

extension RefineVC {
    
    func clearSelectedServiceData(){
        for objServiceList in self.arrFilterAllServices{
            for objSubServiceList in objServiceList.arrSubService{
                objSubServiceList.isSelected = false
            }
        }
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
    }
    
    func resetAllDataToDefault(){
        
        isOutCallSelected = false
        setOutCallUI()
        
        self.lblLocation.text = "Location"
        strLatitude = ""
        strLongitude = ""
        
        self.lblDateTime.text = "Date and Time"
        self.viewDateTimeNew.isHidden = true
        //self.lblBusinessType.text = "Business Type"
        self.lblBusinessType.text = "Service Category"
        self.viewBusinessTypeNew.isHidden = true
        self.lblServiceType.text = "Services"
        self.viewServiceTypeNew.isHidden = true
        
        //Certificate
        self.lblCertificate.text = "Certificate"
        self.viewCertificateNew.isHidden = true
        
        self.clearSelectedServiceData()
        self.vwTableCointaner.isHidden = true
        objAppShareData.objRefineData.strLatitude = ""
        objAppShareData.objRefineData.strLongitude = ""
        objAppShareData.objRefineData.strLocationText = ""
        isPriceSelected = false
        
        isAscending = true
        setBtnAscenDescendingImage()
    }
    
    func createFinalDict(){
        
        //serviceType for "1" = OutCallSelected  or "" = default/Incall
        var serviceType = ""
        
        var BusinessType = ""
        var RadiusMiles = ""
        var PriceLimite = ""
        var Qualification = ""
        
        if isOutCallSelected {
            serviceType = "1"
        }
        
        //lat,long for location if not selected then set current location
        if strLatitude == "" &&  strLongitude == "" {
            strLatitude = objLocationManager.strlatitude ?? ""
            strLongitude = objLocationManager.strlongitude ?? ""
        }
        
        let strDateTime = self.lblDateTime.text
        let strDay = getDayFromSelectedDate(strDate: strDateTime!)
        let strTime = getTimeFromDate(strDate: strDateTime!)
        let tupleSelectedIDs = getSelectedServiceIDs()
        let strSelectedMainServiceIDs = tupleSelectedIDs.strSelectedMainServiceIDs
        let strSelectedSubServiceIDs = tupleSelectedIDs.strSelectedSubServiceIDs
        
        //Certificate
        let strSelectedCertificateIds = self.getSelectedCertificatesId()
        
        var strIsAscending = "" // 1 = desending , "" = defalt/Asending
        if isAscending {
            strIsAscending = ""
        }else{
            strIsAscending = "1"
        }
        
        var strIsPriceSelected  = ""// "price" for price , "" = defalt/distance
        if isPriceSelected {
            strIsPriceSelected = "price"
        }else{
            strIsPriceSelected = ""
        }
        
        let objRefineData = RefineData()
        
        objRefineData.isOutCallSelected  = isOutCallSelected
        objRefineData.strLocationText  = self.lblLocation.text!
        objRefineData.strLatitude = strLatitude
        objRefineData.strLongitude = strLongitude
        objRefineData.strRating = String(format: "%.0f",self.vwRating.value)
        objRefineData.strDistance = String(format: "%.0f", ceil(sliderRadius.value*100)/100)
        
        //// Himanshu
        objRefineData.strPrice = String(format: "%.0f", ceil(sliderPrice.selectedMaxValue*100)/100)
        objRefineData.strMinPrice = String(format: "%.0f", ceil(sliderPrice.selectedMinValue*100)/100)
        ////
        
        objRefineData.strDateTime = self.lblDateTime.text!
        objRefineData.isPriceSelected = isPriceSelected
        objRefineData.isAscending = isAscending
        objRefineData.arrAllServices = self.arrFilterAllServices
        
        //Certificate
        objRefineData.arrAllCertificates = self.arrFilteredCertificates
        
        //if self.lblBusinessType.text != "Business Type"{
        if self.lblBusinessType.text != "Service Category"{
            //objRefineData.strAllBusinessTypes = self.lblBusinessType.text!
            objRefineData.strAllBusinessTypes = self.lblBusinessType.text!
            //objRefineData.strAllCategories = self.lblBusinessType.text!
        }
        if self.lblServiceType.text != "Services"{
            objRefineData.strAllCategories = self.lblServiceType.text!
        }
        if self.lblCertificate.text != "Certificate"{
            objRefineData.strAllCertificates = self.lblCertificate.text!
        }
        
        objAppShareData.objRefineData = objRefineData
        objAppShareData.objRefineData.strLocationText = self.lblLocation.text!
        
        self.setValueSlider()
        
        let strDateTimeNN = objRefineData.strDateTime
        var strDateOnly = ""
        if strDateTimeNN.count > 0 && strDateTimeNN != "Date and Time"{
            let arr = strDateTimeNN.components(separatedBy: " ")
            let str = arr[0]
            let arrNew = str.components(separatedBy: "/")
            strDateOnly = arrNew[2] + "-" + arrNew[1] + "-" + arrNew[0]
        }
        
        let dict :[String : Any] = [
            "rating" : objRefineData.strRating,
            "minPrice" : objRefineData.strMinPrice,
            "maxPrice" : objRefineData.strPrice,
            "distance": objRefineData.strDistance,
            "serviceType" : serviceType, //serviceType for OutCallSelected "1" or "" =
            "latitude" : strLatitude,    //latitude for location
            "longitude" : strLongitude,   //longitude for location
            "day" : strDay,         //day for day if Monday than 0 and so on
            "time" : strTime,        //time for time "02:05 am"
            //"service" : strSelectedMainServiceIDs,//service for main service comma saprated ID "1,3"
            "service" : "",
            //"subservice" : strSelectedSubServiceIDs, //subservice for selected service comma saprated ID "1,3"
            "subservice" : strSelectedMainServiceIDs,
            "sortType" : strIsAscending, // 1 = desending , "" = defalt/Asending
            "page" : "0",
            "limit" : "10",
            "sortSearch" : strIsPriceSelected,
            "certificate":objAppShareData.objRefineData.strAllCertificates,
            "dateForHold":strDateOnly
        ]
        objAppShareData.dictFilter = dict
    }
    
    func setPrefilledData(){
        isOutCallSelected = objAppShareData.objRefineData.isOutCallSelected
        setOutCallUI()
        
        //self.lblLocation.text = objAppShareData.objRefineData.strLocationText
        //strLatitude = objAppShareData.objRefineData.strLatitude
        //strLongitude = objAppShareData.objRefineData.strLongitude
        
        if objAppShareData.objRefineData.strAllCategories.count>0 && objAppShareData.objRefineData.strAllCategories != "Business Type"{
            self.viewServiceTypeNew.isHidden = false
            self.lblServiceType.text = objAppShareData.objRefineData.strAllCategories
            self.lblBusinessType.text = objAppShareData.objRefineData.strAllBusinessTypes
            self.viewBusinessTypeNew.isHidden = false
        }
        if objAppShareData.objRefineData.strAllBusinessTypes.count>0{
            self.lblBusinessType.text = objAppShareData.objRefineData.strAllBusinessTypes
            self.viewBusinessTypeNew.isHidden = false
        }
        //Certificates
        if objAppShareData.objRefineData.strAllCertificates.count>0{
            self.lblCertificate.text = objAppShareData.objRefineData.strAllCertificates
            self.viewCertificateNew.isHidden = false
        }
        
        if objAppShareData.objRefineData.strDateTime.count>0 && objAppShareData.objRefineData.strDateTime != "Date and Time"{
           self.lblDateTime.text = objAppShareData.objRefineData.strDateTime
           self.viewDateTimeNew.isHidden = false
        }
        self.vwTableCointaner.isHidden = true
        self.arrAllServices = objAppShareData.objRefineData.arrAllServices
        self.arrFilterAllServices = self.arrAllServices
        
        //Certificates
        self.arrCertificates = objAppShareData.objRefineData.arrAllCertificates
        self.arrFilteredCertificates = self.arrCertificates
        
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        // self.tblHeightConst.constant = self.tblBookingList.contentSize.height
        
        isPriceSelected = objAppShareData.objRefineData.isPriceSelected
        
        isAscending = objAppShareData.objRefineData.isAscending
        setBtnAscenDescendingImage()
        
        // Manage range slider view
        self.viewRadiusChoose.isHidden = false
        self.viewSlideRadiusMile.isHidden = true
        self.viewLabelRadiusMiles.isHidden = false
        
        // Manage price slider view
        self.viewPriceChoose.isHidden = false
        self.viewSlidePriceEuro.isHidden = true
        self.viewLabelPriceEuro.isHidden = false
        
        self.setValueSlider()
        self.vwRating.value = CGFloat(Int(objAppShareData.objRefineData.strRating)!)
    }
}

extension RefineVC {
    func locationAuthorization(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            gotoGMSAutocompleteVC()
            
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            gotoGMSAutocompleteVC()
            
        }
        else if (CLLocationManager.authorizationStatus() == .denied) {
            let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        } else {
            //locManager.requestWhenInUseAuthorization()
            //self.webServiceCall_artistSearchWithDict(parameters: parameters)
            gotoGMSAutocompleteVC()
        }
    }
}
