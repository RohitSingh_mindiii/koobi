//
//  SearchBoardVC.swift
//  MualabCustomer
//
//  Created by Mac on 15/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import GooglePlaces
import Firebase
import SDWebImage

var viewHeightGloble = 0

class SearchBoardVC: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var dataScrollView: UIScrollView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tblSearchResult: UITableView!
    var fromTextField = false
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    @IBOutlet weak var imgVwFilter: UIImageView!
    @IBOutlet weak var imgVwFavorite: UIImageView!
    @IBOutlet weak var imgVwUser: UIImageView!
    @IBOutlet weak var lblNoRecord: UIView!
    @IBOutlet weak var viewNoLocation: UIView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    @IBOutlet weak var lblBusinessInvitationCount: UILabel!
    @IBOutlet weak var viewBusinessInvitationCount: UIView!

    var strLatitude : String = ""
    var isAddressChange : Bool = false
    var strLongitude : String = ""
    var strUserId : String = ""
    var arrData : [ArtistDetails] = []
    var isFavoriteSelected : Bool!
    var fromDidLoad = true
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    var objService = SubSubService.init(dict: [:])
    let pageSize = 20 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var strSearchValue : String = ""
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor //UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
}

// MARK:- View Life Cycle
extension SearchBoardVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNoRecord.isHidden = false
        if viewHeightGloble == 0{
            viewHeightGloble = Int(self.view.frame.height)
        }
        self.configureView()
        self.viewNoLocation.isHidden = true
        //Rohit
        //ref = Database.database().reference()
       // self.manageBookingRequestCount()
        //Rohit
       // self.GetChatHistoryFromFirebase()
        //NotificationCenter.default.addObserver(self, selector: #selector(self.refreshSearchBoardLocation), name: NSNotification.Name(rawValue: "refreshSearchBoardLocation"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshSearchBoardLocation"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshSearchBoardLocation(Notification)
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(self.refreshChatBadgeCount), name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshChatBadgeCount(Notification)
        }
       
        //let a = self.dataScrollView.layer.frame.height+120
        //self.heightTableView.constant = a
    }
    
    @objc func refreshSearchBoardLocation(_ objNotify: Notification) {
        print(objLocationManager.strAddress)
        objAppShareData.isTabChange = false
        self.strLongitude = ""
        self.strLatitude = ""
        if objServiceManager.isNetworkAvailable(){
            self.getAddressFromLatLon(pdblLatitude: objLocationManager.strlatitude ?? "22.705262" , withLongitude: objLocationManager.strlongitude ?? "75.909186")
        }
        objAppShareData.dictFilter = [:]
        objAppShareData.isSearchingUsingFilter = false
        self.lblNoRecord.isHidden = true
        //objLocationManager.getCurrentLocation()
        self.lblAddress.text = objLocationManager.strAddress
        self.tblSearchResult.setContentOffset(CGPoint.zero, animated: true)
        self.getArtistDataWith(0, andSearchText: self.txtSearch.text!)
    }
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            self.viewChatbadgeCount.isHidden = false
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.isAddressChange = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//    SDWebImageManager.shared().imageCache?.deleteOldFiles(completionBlock: nil)
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
//        URLCache.shared.removeAllCachedResponses()
       
        if objAppShareData.isFromNotification {
            if objAppShareData.notificationType == "booking" {
                self.gotoBookingDetailModule()
                return
            }else if objAppShareData.notificationType == "Staff invitation"{
                self.gotoInvitationList()
                return
            }
        }
        self.dataScrollView.contentOffset.y = 0.0
        self.tblSearchResult.contentOffset.y = 0.0
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if objAppShareData.isTabChange{
            self.btnFavorite.isSelected = !self.btnFavorite.isSelected
            self.isFavoriteSelected = false
            self.imgVwFavorite.image = #imageLiteral(resourceName: "inactive_yellow_star_ico")
        }
        
        if objAppShareData.isTabChange && !objAppShareData.isSearchingUsingFilter{
            objAppShareData.isTabChange = false
            self.strLongitude = ""
            self.strLatitude = ""
            if objServiceManager.isNetworkAvailable(){
                self.getAddressFromLatLon(pdblLatitude: objLocationManager.strlatitude ?? "22.705262" , withLongitude: objLocationManager.strlongitude ?? "75.909186")
            }
            objAppShareData.dictFilter = [:]
            objAppShareData.isSearchingUsingFilter = false
        }else{
            if objAppShareData.isTabChange{
                objAppShareData.isTabChange = false
                objAppShareData.isSearchingUsingFilter = false
                objAppShareData.objRefineData = RefineData()
            }
            if objAppShareData.objRefineData.strLatitude.count == 0{
                self.strLongitude = ""
                self.strLatitude = ""
                if objServiceManager.isNetworkAvailable(){
                    self.getAddressFromLatLon(pdblLatitude: objLocationManager.strlatitude ?? "22.705262" , withLongitude: objLocationManager.strlongitude ?? "75.909186")
                }
            }else{
                self.strLongitude = objAppShareData.objRefineData.strLongitude
                self.strLatitude = objAppShareData.objRefineData.strLatitude
                self.lblAddress.text = objAppShareData.objRefineData.strLocationText
            }
        }
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgVwUser.af_setImage(withURL: url, placeholderImage: //#imageLiteral(resourceName: "cellBackground"))
                        self.imgVwUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgVwUser.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgVwUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }
        
        self.lblNoRecord.isHidden = true
        self.tblSearchResult.setContentOffset(CGPoint.zero, animated: true)
        self.getArtistDataWith(0, andSearchText: self.txtSearch.text!)
        callWebserviceFor_deleteAllbooking()
        objAppShareData.stopTimerForHoldBookings()
        
        //self.dataScrollView.isScrollEnabled = true
        //self.tblSearchResult.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .de
    //    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !objServiceManager.isNetworkAvailable(){
            self.refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        fromTextField = true
        self.getArtistDataWith(0, andSearchText: self.txtSearch.text!)
    }
}

// MARK:- Custom Methods
extension SearchBoardVC {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        if self.dataScrollView.contentOffset.y == 0.0 && self.tblSearchResult.contentOffset.y == 0.0 {
        //            self.tblSearchResult.isScrollEnabled = false
        //        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        /*
         print(self.dataScrollView.contentOffset.y)
         print(self.tblSearchResult.contentOffset.y)
         if self.dataScrollView.contentOffset.y == 0.0 && self.tblSearchResult.contentOffset.y <= 5 {
         self.tblSearchResult.isScrollEnabled = false
         //self.dataScrollView.bounces = false
         //self.tblSearchResult.bounces = false
         }else if self.dataScrollView.contentOffset.y >= 130.0 && self.tblSearchResult.contentOffset.y <= 5{
         self.tblSearchResult.isScrollEnabled = true
         //self.dataScrollView.bounces = true
         //self.tblSearchResult.bounces = true
         }
         let a = self.dataScrollView.layer.frame.height + 130.0
         self.heightTableView.constant = a
         */
    }
    
    func configureView(){
        self.indicator.stopAnimating()
        self.dataScrollView.decelerationRate = UIScrollView.DecelerationRate.normal
        self.tblSearchResult.decelerationRate = UIScrollView.DecelerationRate.normal
        
        tblSearchResult.delegate = self
        tblSearchResult.dataSource = self
        //self.dataScrollView.refreshControl = refreshControl
        self.tblSearchResult.addSubview(self.refreshControl)
        
        fromDidLoad = true
        txtSearch.delegate = self
        
        self.isFavoriteSelected = false
        self.imgVwFavorite.image = UIImage.init(named: "inactive_yellow_star_ico")
        
        var strId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
                strId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
                strId = String(id)
            }
        }
        //Rohit
//        ref = Database.database().reference()
//        _refHandle = ref.child("chatBadgeCount").child(strId).observe(.value, with: { [weak self] (snapshot) in
//            guard let strongSelf = self else {
//                return
//            }
//            let dict = snapshot.value as? [String:Any]
//            print(dict)
//            if let count = dict?["count"] as? Int {
//                //objChatShareData.chatBadgeCount = count
//            }else if let count = dict?["count"] as? String {
//                //objChatShareData.chatBadgeCount = Int(count) ?? 0
//            }
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
//        })
        
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        if objLocationManager.strlatitude == "" {
            objLocationManager.strlatitude = UserDefaults.standard.value(forKey: UserDefaults.keys.myCurrentLat) as? String ?? ""
            objLocationManager.strlongitude = UserDefaults.standard.value(forKey: UserDefaults.keys.myCurrentLong) as? String ?? ""
        }
    }
}

// MARK:- TableView Delegates
extension SearchBoardVC : UITableViewDataSource, UITableViewDelegate,SearchBoardCellDelegate{
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if self.arrData.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                self.getArtistDataWith(nextPage)
            }
        }
        
        if let cell : SearchBoardCell = tableView.dequeueReusableCell(withIdentifier: "SearchBoardCell")! as? SearchBoardCell {
            
            cell.delegate = self
            cell.indexPath = indexPath
            var objArtistDetails : ArtistDetails?
            objArtistDetails = arrData[indexPath.row]
            
            cell.vwRating.isUserInteractionEnabled = false
            if let objArtistDetails = objArtistDetails {
                
                cell.imgVwFovorite.image = UIImage.init(named: "starFilter_ico")
                //#imageLiteral(resourceName: "ico_star_active")// UIImage.init(named: "active_ico")
                //cell.imgVwOuter.image = UIImage(named: "cellTransperent")
                cell.imgVwProfile.image = UIImage(named: "cellBackground")
                cell.lblName.text  = objArtistDetails.userName
                cell.vwRating.value = CGFloat(objArtistDetails.ratingCount)
                cell.lblReviewCount.text = "(\(objArtistDetails.reviewCount))"//String(objArtistDetails.reviewCount)
                if objArtistDetails.reviewCount > 999{
                    let a = objArtistDetails.reviewCount/1000
                    let b = (objArtistDetails.reviewCount-(a*1000))
                    if b > 99{
                        let c = b/100
                        cell.lblReviewCount.text = "("+String(a)+"."+String(c)+"k)"
                    }else{
                        cell.lblReviewCount.text = "("+String(a)+"k)"
                    }
                }
                let doubleStr = String(format: "%.2f", ceil(objArtistDetails.distance*100)/100)
                cell.lblDistance.text = doubleStr + " miles"
                cell.setDataInCollection(obj: objArtistDetails)
                
                if objArtistDetails.profileImage != "" {
                    cell.lblImg.isHidden = true
                    if let url = URL(string: objArtistDetails.profileImage){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                        //cell.imgVwProfile.sd_setImage(with: url, placeholderImage: nil)
                    }
                }else{
                    cell.imgVwProfile.image = nil
                    cell.lblImg.isHidden = false
                    cell.lblImg.text = objArtistDetails.userName.getAcronyms(separator: "").uppercased();
                }
                
                if self.isFavoriteSelected {
                    cell.imgVwFovorite.isHidden = false
                }else{
                    cell.imgVwFovorite.isHidden = true
                }
            }
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objArtistDetails : ArtistDetails = arrData[indexPath.row]
        objAppShareData.selectedOtherIdForProfile  = objArtistDetails._id
        objAppShareData.isOtherSelectedForProfile = true
        objAppShareData.selectedOtherTypeForProfile = "artist"
        // self.gotoProfileVC()
    }
    
    func btnBookTapped(at index:IndexPath){
        self.view.endEditing(true)
        let obj = self.arrData[index.row]
        objAppShareData.selectedOtherIdForProfile = obj._id
        
        ////
        var strIncallOrOutCall = objAppShareData.dictFilter["serviceType"] as? String ?? ""
        if strIncallOrOutCall == "1"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        self.objService.artistId = obj._id
        let strServiceId = objAppShareData.dictFilter["service"] as? String ?? "0"
        let strSubServiceId = objAppShareData.dictFilter["subservice"] as? String ?? "0"
        //var serviceId = 0
        //var subServiceId = 0
        if strServiceId.contains(","){
            let arr = strServiceId.components(separatedBy: ",")
            self.objService.serviceId = Int(arr[0]) ?? 0
        }else{
            self.objService.serviceId = Int(strServiceId) ?? 0
        }
        if strSubServiceId.contains(","){
            let arr = strSubServiceId.components(separatedBy: ",")
            self.objService.subServiceId = Int(arr[0]) ?? 0
        }else{
            self.objService.subServiceId = Int(strSubServiceId) ?? 0
        }
        let strDate = objAppShareData.dictFilter["dateForHold"] as? String ?? ""
        objAppShareData.objServiceForEditBooking.strDateForEdit = strDate
        objAppShareData.isBookingFromService = true
        ////
        
        if self.objService.serviceId == 0 {
            if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
             objWebserviceManager.StartIndicator()
            let parameters : Dictionary = [
                "userId" : strUserId,
                "artistId" : obj._id
                ] as [String : Any]
            objWebserviceManager.requestPost(strURL: WebURL.getMostBookedService, params: parameters  , success: { response in
                let keyExists = response["responseCode"] != nil
                if keyExists {
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        //                        "artistServices": {
                        //                            "_id": 2,
                        //                            "serviceId": 6,
                        //                            "subserviceId": 1,
                        //                            "bookingCount": 1
                        //                        }
                        let dictServices =  response["artistServices"] as? [String:Any] ?? [:]
                        self.objService.serviceId = dictServices["serviceId"] as? Int ?? 0
                        self.objService.subServiceId = dictServices["subserviceId"] as? Int ?? 0
                        self.objService.subSubServiceId = dictServices["_id"] as? Int ?? 0
                        objAppShareData.objServiceForEditBooking = self.objService
                        objWebserviceManager.StopIndicator()
                        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
                        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                            
                            ////
                            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
                            ////
                            objVC.hidesBottomBarWhenPushed = true
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                    }else{
                        objWebserviceManager.StopIndicator()
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
            }
        }else{
            objWebserviceManager.StopIndicator()
            objAppShareData.objServiceForEditBooking = self.objService
            let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
            if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                
                ////
                objVC.strInCallOrOutCallFromService = strIncallOrOutCall
                ////
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
        return
            
            objAppShareData.arrSelectedService.removeAll()
        
        var objArtistDetails : ArtistDetails?
        objArtistDetails = arrData[index.row]
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"BookingIndividualVC") as? BookingIndividualVC{
            
            objVC.objArtistDetails = objArtistDetails!
            objVC.isFromAddMoreService = false
            
            if objAppShareData.isSearchingUsingFilter{
                objVC.objArtistDetails.isOutCallSelected =  objAppShareData.objRefineData.isOutCallSelected
            }
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func btnProfileImageTapped(at index:IndexPath){
        let objArtistDetails : ArtistDetails = arrData[index.row]
        objAppShareData.selectedOtherIdForProfile  = objArtistDetails._id
        objAppShareData.isOtherSelectedForProfile = true
        objAppShareData.selectedOtherTypeForProfile = "artist"
        self.gotoProfileVC()
    }
}

//MARK: - btnActions
extension SearchBoardVC{
    
    @IBAction func btnServiceInListAction(_ sender: UIButton) {
        return
            print(sender.superview!.tag)
        print(sender.tag)
        let objArtist = self.arrData[sender.superview!.tag]
        let objService = objArtist.arrServices[sender.tag]
        
        objAppShareData.selectedOtherIdForProfile = objArtist._id
        var strIncallOrOutCall = objAppShareData.dictFilter["serviceType"] as? String ?? ""
        if strIncallOrOutCall == "1"{
            strIncallOrOutCall = "Out Call"
        }else{
            strIncallOrOutCall = "In Call"
        }
        self.objService.artistId = objArtist._id
        self.objService.serviceId = objService.serviceId
        self.objService.subServiceId = objService.subServiceIdTemp
        self.objService.subSubServiceId = objService.subSubServiceIdTemp
        objAppShareData.isBookingFromService = true
        objAppShareData.objServiceForEditBooking = self.objService
        
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
            objVC.strInCallOrOutCallFromService = strIncallOrOutCall
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnSettingsAction(_ sender: UIButton) {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url, options: [:]
            , completionHandler: nil)
    }
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnChangeAddressAction(_ sender: UIButton) {
        objAppShareData.isTabChange = false
        self.isAddressChange = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnFavoriteAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        sender.isSelected = !sender.isSelected
        if sender.isSelected  {
            self.isFavoriteSelected = true
            self.imgVwFavorite.image = UIImage.init(named: "starFilter_ico")
        }else{
            self.isFavoriteSelected = false
            self.imgVwFavorite.image = UIImage.init(named: "inactive_yellow_star_ico")
        }
        self.getArtistDataWith(0, andSearchText: self.txtSearch.text!)
    }
    
    @IBAction func btnFilterAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.objRefineData.strLatitude = self.strLatitude
        objAppShareData.objRefineData.strLongitude = self.strLongitude
        objAppShareData.objRefineData.strLocationText = self.lblAddress.text!
        
        let sb: UIStoryboard = UIStoryboard(name: "SearchBoard", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"RefineVC") as? RefineVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        //objAppShareData.isOtherSelectedForProfile = false
        //self.gotoProfileVC()
        self.gotoNotificationVC()
    }
    
    func gotoNotificationVC(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"NotificationVC") as? NotificationVC{
            objVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoProfileVC(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ProfileModule", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileOtherVC") as? ArtistProfileOtherVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
}

// MARK: - WebService Call
extension SearchBoardVC {
    
    func callWebserviceFor_deleteAllbooking() {
        
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        
        let parameters : Dictionary = [
            "userId" : strUserId,
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteUserBookService, params: parameters  , success: { [weak self] response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    objAppShareData.stopTimerForHoldBookings()
                }else{
                    
                }
            }
        }){ error in
            
        }
    }
    
    func webServiceCall_artistSearchWithDict(parameters : [String:Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            self.refreshControl.endRefreshing()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if fromDidLoad{
            fromDidLoad = false
            objWebserviceManager.StartIndicator()
        }else{
            
            if  !self.refreshControl.isRefreshing && self.pageNo == 0 && fromTextField == false{
                if self.txtSearch.text != ""{
                    self.indicator.startAnimating()
                }else{
                    objWebserviceManager.StartIndicator()
                }
            }else{
                fromTextField = false
            }
        }
        self.lblNoRecord.isHidden = true
        
        var strUrl = WebURL.artistSearch
        if self.isFavoriteSelected {
            strUrl = WebURL.favoriteList
        }
        
        objWebserviceManager.requestPost(strURL: strUrl, params: parameters as [String : AnyObject] , success: { [weak self] response in
            objWebserviceManager.StopIndicator()
            self?.indicator.stopAnimating()
            self?.refreshControl.endRefreshing()
            
            if self?.pageNo == 0 {
                self?.arrData.removeAll()
                self?.tblSearchResult.reloadData()
            }
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self!)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                //let strMsg = response["message"] as? String ?? kErrorMessage
                
                if strStatus == k_success{
                    
                    if let totalCount = response["totalCount"] as? Int{
                        self?.totalCount = totalCount
                    }
                    
                    if let arr = response["artistList"] as? [[String : Any]]{
                        for dict in arr{
                            let obj = ArtistDetails.init(dict: dict)
                            self?.arrData.append(obj)
                        }
                    }
                    if self?.arrData.count==0{
                        
                    }
                }else{
                    
                    if strStatus == "fail"{
                    }else{
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self!)
                        }
                    }
                }
            }
            
            self?.tblSearchResult.reloadData()
            if self?.arrData.count == 0 {
                self?.lblNoRecord.isHidden = false
            }else{
                self?.lblNoRecord.isHidden = true
            }
            
        }) { [weak self] (error) in
            self?.indicator.stopAnimating()
            
            objWebserviceManager.StopIndicator()
            self?.refreshControl.endRefreshing()
            
            self?.tblSearchResult.reloadData()
            if self?.arrData.count == 0 {
                self?.lblNoRecord.isHidden = false
            }else{
                self?.lblNoRecord.isHidden = true
            }
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self!)
        }
    }
}


// MARK: - UITextfield Delegate
extension SearchBoardVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        fromTextField = false
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            
            getArtistDataWith(0, andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            searchAutocompleteEntries(withSubstring: substring)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        fromTextField = false
        getArtistDataWith(0, andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            fromTextField = false
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith(0, andSearchText: strSearchValue)
    }
}


// MARK:- Save data
extension SearchBoardVC{
    
    func getArtistDataWith(_ page: Int, andSearchText strText: String = "") {
        
        var parameters = [String:Any]()
        
        self.lastLoadedPage = page
        self.pageNo = page
        
        let strSearchText = strText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if objAppShareData.isSearchingUsingFilter{
            self.imgVwFilter.image = UIImage.init(named: "filter_ico_filled")
            if objAppShareData.objRefineData.strLocationText == "Location"{
                self.getAddressFromLatLon(pdblLatitude: objLocationManager.strlatitude ?? "22.705262" , withLongitude: objLocationManager.strlongitude ?? "75.909186")
            }
            self.strLatitude = objAppShareData.objRefineData.strLatitude
            self.strLongitude = objAppShareData.objRefineData.strLongitude
            self.lblAddress.text = objAppShareData.objRefineData.strLocationText
            objAppShareData.dictFilter["latitude"] = self.strLatitude
            objAppShareData.dictFilter["longitude"] = self.strLongitude
            parameters = objAppShareData.dictFilter
            parameters["page"] = self.pageNo
            parameters["limit"] = self.pageSize
            parameters["text"] = strSearchText.lowercased()
            
        }else{
            self.imgVwFilter.image = UIImage.init(named: "filter_ico_Blue")
            var strLat = ""
            var strLong = ""
            if self.strLatitude.count > 0{
                strLat = self.strLatitude
                strLong = self.strLongitude
            }else{
                strLat = objLocationManager.strlatitude ?? "22.705262"
                strLong = objLocationManager.strlongitude ?? "75.909186"
            }
            parameters = [
                "latitude":strLat,
                "longitude":strLong,
                //"distance":"100",
                "page":self.pageNo,
                "limit":self.pageSize,
                "text": strSearchText.lowercased()
            ]
        }
        
        parameters["userId"] = strUserId
        //self.webServiceCall_artistSearchWithDict(parameters: parameters)
        locationAuthorization(parameters: parameters)
    }
}

// MARK:- Notification redirection
extension SearchBoardVC{
    
    func gotoInvitationList(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Invitation", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
    func gotoBookingDetailModule(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"PastFutureBookingVC") as? PastFutureBookingVC {
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
}

//
extension SearchBoardVC {
    func locationAuthorization(parameters: [String:Any]){
        self.refreshControl.endRefreshing()
        objWebserviceManager.StopIndicator()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            self.viewNoLocation.isHidden = true
            self.webServiceCall_artistSearchWithDict(parameters: parameters)
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            self.viewNoLocation.isHidden = true
            self.webServiceCall_artistSearchWithDict(parameters: parameters)
        }else if (CLLocationManager.authorizationStatus() == .denied) {
            /*self.lblNoRecord.isHidden = false
            self.arrData.removeAll()
            self.tblSearchResult.reloadData()
            let alert = UIAlertController(title: "Need location authorization", message: "The location permission was not authorized. Please enable it in Settings to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            let action = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(url, options: [:]
                    , completionHandler: nil)
            })
            action.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            */
            self.viewNoLocation.isHidden = false
            return
        } else if (CLLocationManager.authorizationStatus() == .notDetermined) {
            self.viewNoLocation.isHidden = false
            objLocationManager.getCurrentLocation()
        }else{
            self.viewNoLocation.isHidden = false
            objLocationManager.getCurrentLocation()
            //self.webServiceCall_artistSearchWithDict(parameters: parameters)
        }
    }
}

extension SearchBoardVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.strLatitude = String(place.coordinate.latitude)
        self.strLongitude = String(place.coordinate.longitude)
        self.lblAddress.text = place.formattedAddress
        
        //if objAppShareData.isSearchingUsingFilter{
        objAppShareData.objRefineData.strLatitude = self.strLatitude
        objAppShareData.objRefineData.strLongitude = self.strLongitude
        objAppShareData.objRefineData.strLocationText = place.formattedAddress!
        //}
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                //if pm = placemarks! as [CLPlacemark]
                if placemarks != nil{
                    if let pm = placemarks! as? [CLPlacemark]{
                        
                        if pm.count > 0 {
                            let pm = placemarks![0]
                            print(pm.country)
                            print(pm.locality)
                            print(pm.subLocality)
                            print(pm.thoroughfare)
                            print(pm.postalCode)
                            print(pm.subThoroughfare)
                            var addressString : String = ""
                            if pm.subLocality != nil {
                                addressString = addressString + pm.subLocality! + ", "
                            }
                            if pm.thoroughfare != nil {
                                addressString = addressString + pm.thoroughfare! + ", "
                            }
                            if pm.locality != nil {
                                addressString = addressString + pm.locality! + ", "
                            }
                            if pm.country != nil {
                                addressString = addressString + pm.country! + ", "
                            }
                            if pm.postalCode != nil {
                                addressString = addressString + pm.postalCode! + " "
                            }
                            
                            
                            print(addressString)
                            
                            self.lblAddress.text = addressString
                            
            objAppShareData.objRefineData.strLatitude = pdblLatitude
            objAppShareData.objRefineData.strLongitude = pdblLongitude
            objAppShareData.objRefineData.strLocationText = addressString
                        }
                    }
                }
        })
    }
    
    func GetChatHistoryFromFirebase() {
        _refHandle = self.ref.child("chat_history").child(strUserId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else {
                return
            }
            objChatShareData.chatBadgeCount = 0
            print(snapshot)
            
            if let arr = snapshot.value as? [Any]{
                for dicttt in arr {
                    if let dict = dicttt as? NSDictionary{
                        print("\n\ndicttt = \(dict)")
                        
                        let objChatList = ChatHistoryData()
                        
                        if let unreadMessage = dict["unreadMessage"] as? String {
                            var a = unreadMessage
                            if a == "" {
                                a = "0"
                            }
                            objChatList.strUnreadMessage = Int(a)!
                            objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + Int(a)!
                        }else if let unreadMessage = dict["unreadMessage"] as? Int{
                            objChatList.strUnreadMessage = unreadMessage
                            objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + unreadMessage
                        }
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(dictHistory: dict)
            }else{
                objChatShareData.chatBadgeCount = 0
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
            }
        })
    }
    
    func parseDataChatHistory(dictHistory:NSDictionary){
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                let objChatList = ChatHistoryData()
                if let unreadMessage = dict["unreadMessage"] as? String {
                    var a = unreadMessage
                    if a == "" {
                        a = "0"
                    }
                    objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + Int(a)!
                    objChatList.strUnreadMessage = Int(a)!
                }else if let unreadMessage = dict["unreadMessage"] as? Int{
                    objChatList.strUnreadMessage = unreadMessage
                    objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + unreadMessage
                    
                }
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
    }
    
    func manageBookingRequestCount(){
    
        var  idUser = 0
        self.viewBusinessInvitationCount.isHidden = true
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            idUser = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            idUser = userInfo["_id"] as? Int ?? 0
        }
        ref.child("socialBookingBadgeCount").child(String(idUser)).observe(.value, with: { (snapshot) in
            let dict = snapshot.value as? [String:Any]
          
            var totalCount = 0
            if let count = dict?["socialCount"] as? Int {
                totalCount = count+totalCount
            }else if let count = dict?["socialCount"] as? String {
                totalCount = (Int(count) ?? 0)+totalCount
            }
            if let count = dict?["bookingCount"] as? Int {
                totalCount = count+totalCount
            }else if let count = dict?["bookingCount"] as? String {
                totalCount = (Int(count) ?? 0)+totalCount
            }
            if totalCount == 0{
                self.lblBusinessInvitationCount.text = String(totalCount)
            }else{
                self.lblBusinessInvitationCount.text = String(totalCount)
                if totalCount >= 100{
                    self.lblBusinessInvitationCount.text = "99+"
                }
            }
            if totalCount > 0{
                self.viewBusinessInvitationCount.isHidden = false
            }else{
                self.viewBusinessInvitationCount.isHidden = true
            }
        })
        if totalCount > 0{
            self.viewBusinessInvitationCount.isHidden = false
        }else{
            self.viewBusinessInvitationCount.isHidden = true
        }
    }
}

