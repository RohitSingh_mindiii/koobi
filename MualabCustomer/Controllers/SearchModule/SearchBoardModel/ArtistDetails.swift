
//
//  ArtistList.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation

class ArtistDetails{
    
    var _id : Int = 0
    var businessType = ""
    var reviewCount : Int = 0
    var ratingCount : Double = 0
    var postCount : Int = 0
    var profileImage : String = ""
    var userName : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var aboutUs : String = ""
    var distance : Double = 0
    var bookingSetting : Int = 0
    var arrServiceNames : [String] = [String]()
    var arrStaffInfo: [StaffInfo] = [StaffInfo]()
    var serviceType = ""//"serviceType": 1 = inCall, 2 = outCall, 3 = both
    var arrServices : [Service] = [Service]() 
    var strServicesToShow = ""
    var businessName = ""
    var responce : [String : Any] = [String : Any]()
    var isOutCallSelected : Bool = false
    var inCallpreprationTime : String = ""
    var outCallpreprationTime : String = ""
    var address : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var isFromEdit : Bool = false
    var radius : Double = 0
    var bankStatus : Int = 0
    var payOption : Int = 0
    var isAlreadyBooked : Int = 0

    var userBookingAddress : String = ""
    var userBookingAddressLatitude : String = ""
    var userBookingAddressLongitude : String = ""
    var isAddressInArtistRange : Bool = false
    
    var userBookingSelectedAddress : String = ""
    var userBookingSelectedAddressLatitude : String = ""
    var userBookingSelectedAddressLongitude : String = ""
    
    
    init(dict : [String : Any]){
        
        if let id = dict["_id"] as? Int{
            _id = id
        }else if let id = dict["_id"] as? String{
            self._id = Int(id) ?? 0
        }
        
        if let bankStatus = dict["bankStatus"] as? Int{
            self.bankStatus = bankStatus
        }else if let bankStatus = dict["bankStatus"] as? String{
            self.bankStatus = Int(bankStatus) ?? 0
        }
        
        if let serviceType = dict["serviceType"] as? Int{
            self.serviceType = String(serviceType)
        }else if let serviceType = dict["serviceType"] as? String{
            self.serviceType = serviceType
        }
        
        //1 : card , 2 : cash , 3 both
        if let serviceType = dict["payOption"] as? Int{
            self.payOption = serviceType
        }else if let serviceType = dict["payOption"] as? String{
            self.payOption = Int(serviceType)!
        }
        
        // Manual = 1, Auto=0;
        if let serviceType = dict["bookingSetting"] as? Int{
            self.bookingSetting = serviceType
        }else if let serviceType = dict["bookingSetting"] as? String{
            self.bookingSetting = Int(serviceType)!
        }
        
        if let reviewCount = dict["reviewCount"] as? Int{
            self.reviewCount = reviewCount
        }else if let reviewCount = dict["reviewCount"] as? String{
            self.reviewCount = Int(reviewCount) ?? 0
        }
        
//        if let ratingCount = dict["ratingCount"] as? Int{
//            self.ratingCount = ratingCount
//        }else if let ratingCount = dict["ratingCount"] as? String{
//            self.ratingCount = Int(ratingCount) ?? 0
//        }
        if let ratingCount = dict["ratingCount"] as? Double{
            self.ratingCount = ratingCount
        }else if let ratingCount = dict["ratingCount"] as? String{
            self.ratingCount = Double(ratingCount) ?? 0.0
        }
        if let aboutUs = dict["bio"] as? String{
            self.aboutUs = aboutUs
        }
        if let postCount = dict["postCount"] as? Int{
            self.postCount = postCount
        }
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        if let address = dict["address"] as? String{
            self.address = address
        }
        
        if let latitude = dict["latitude"] as? String{
            self.latitude = latitude
        }
        if let longitude = dict["longitude"] as? String{
            self.longitude = longitude
        }
        
        if let inCallpreprationTime = dict["inCallpreprationTime"] as? String{
            self.inCallpreprationTime = inCallpreprationTime
        }
        if let outCallpreprationTime = dict["outCallpreprationTime"] as? String{
            self.outCallpreprationTime = outCallpreprationTime
        }
        
        if let serviceType = dict["serviceType"] as? Int{
            self.serviceType = "\(serviceType)"
        }
        if let businessType = dict["businessType"] as? String{
            self.businessType = businessType
        }
        if let businessName = dict["businessName"] as? String{
            self.businessName = businessName
        }
        if let distance = dict["distance"] as? Double{
            self.distance = distance
        }
        if let radius = dict["radius"] as? String{
            self.radius = Double(radius) ?? 0
        }

//        if let arr = dict["service"] as? [[String : Any]]{
//
//            for dictService in arr{
//                let strName = dictService["title"] as? String ?? ""
//                if strName != ""{
//                   self.arrServiceNames.append(strName)
//                }
//            }
        if let arr = dict["subcate"] as? [[String : Any]]{
            for dictService in arr{
                let strName = dictService["subServiceName"] as? String ?? ""
                if strName != ""{
                    self.arrServiceNames.append(strName)
                }
            }
            if self.arrServiceNames.count > 0{
                if self.arrServiceNames.count >= 2{
                    let arrTemp = self.arrServiceNames[0...1]
                    self.strServicesToShow = arrTemp.joined(separator: ", ")
                }else{
                    self.strServicesToShow = arrServiceNames.joined(separator: ", ")
                }
            }
        }
        
        if let arr = dict["allService"] as? [[String : Any]]{
            for dict in arr{
                let obj = Service.init(dict: dict)
                obj.subServiceIdTemp = dict["subserviceId"] as? Int ?? 0
                obj.subSubServiceIdTemp = dict["_id"] as? Int ?? 0
                self.arrServices.append(obj)
            }
        }
        if let arr = dict["service"] as? [[String : Any]]{
            for dict in arr{
                let obj = Service.init(dict: dict)
                obj.subServiceIdTemp = dict["subserviceId"] as? Int ?? 0
                obj.subSubServiceIdTemp = dict["_id"] as? Int ?? 0
                self.arrServices.append(obj)
            }
        }
        
        if let arr = dict["staffInfo"] as? [[String : Any]]{
            for dict in arr{
                let obj = StaffInfo.init(dict: dict)
                self.arrStaffInfo.append(obj)
            }
        }
    }
}


public class ArtistList {
    public var userName : String = ""
    public var profileImage : String = ""
    public var _id : Int = 0
    
    init(dict: [String : Any]) {
        userName = dict ["userName"] as? String ?? ""
        profileImage = dict ["profileImage"] as? String ?? ""
        _id = dict ["_id"] as? Int ?? 0
   }
}
