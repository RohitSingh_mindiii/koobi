//
//  Service.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation

class Service : NSObject {
    var isSelectLocal : Bool = false
    var isSelected : Bool = false
	var serviceId : Int = 0
    var serviceName : String = ""
    var arrSubServices : [SubService] = [SubService]()
	var subServiceIdTemp : Int = 0
    var subSubServiceIdTemp : Int = 0
    
    init(dict: [String : Any]){
        
        if let serviceId = dict["serviceId"] as? Int{
            self.serviceId = serviceId
        }
        if let serviceName = dict["serviceName"] as? String{
            self.serviceName = serviceName
        }
        
        if let arr = dict["subServies"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = SubService.init(dict: dict)
                objSubService.serviceId = self.serviceId
                objSubService.serviceName = self.serviceName
                self.arrSubServices.append(objSubService)
            }
        }
    }
}

