
//
//  Service.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//


import Foundation



public class ServiceList {
    var serviceIdTemp : Int = 0
    var _id : Int = 0
    var title : String = ""
    var isSelect : Bool = false
    var isSelectLocal : Bool = false

    var arrSubService : [SubServiceList] = [SubServiceList]()
    
    required public init(dictionary: [String : Any]) {
        isSelect = false
        isSelectLocal = false

        _id = dictionary["_id"] as? Int ?? 0
        title = dictionary["title"] as? String ?? ""
        serviceIdTemp = dictionary["serviceId"] as? Int ?? 0
        if let arr = dictionary["subService"] as? [[String : Any]]{
            for dict in arr{
                let obj = SubServiceList.init(dict: dict)
                self.arrSubService.append(obj)
            }
        }
        if let arr = dictionary["artistservices"] as? [[String : Any]]{
            for dict in arr{
                let obj = SubServiceList.init(dict: dict)
                self.arrSubService.append(obj)
            }
        }
    }    
}
