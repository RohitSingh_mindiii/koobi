//
//  StaffInfo.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class StaffInfo {
    
    var holiday : Int = 0
    var staffId : Int = 0
    var job : String = ""
    var mediaAccess : String = ""
    var staffImage : String = ""
    var staffName : String = ""
    var arrStaffService : [StaffService] = []
    var arrStaffServiceId : [Int] = []
    
    var selectedPrice : Double = 0
    var selectedCompletionTime : String = ""
    
    init(dict : [String : Any]){
        
        if let valholiday = dict["holiday"] as? Int {
            holiday = valholiday
        }else{
            if let valholiday = dict["holiday"] as? String {
                holiday = Int (valholiday) ?? 0
            }
        }
        
        staffId = dict["staffId"] as? Int ?? 0
        job = dict["job"] as? String ?? ""
        mediaAccess = dict["mediaAccess"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        staffName = dict["staffName"] as? String ?? ""
        
        if let arr = dict["staffService"] as? [[String : Any]]{
            for dict in arr{
                let objStaffService = StaffService.init(dict: dict)
                arrStaffService.append(objStaffService)
            }
        }
        
        if let arr = dict["staffServiceId"] as? [String]{
            for str in arr{
                let val = Int (str) ?? 0
                arrStaffServiceId.append(val)
            }
        }
    }
}

class StaffService {
    
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
   
    var businessId : Int = 0   // staff owner id (Boss)
    var deleteStatus : Int = 0 // 1 = service exist , 0 = service deleted
    var artistId : Int = 0
    var staffId : Int = 0
    var status : Int = 0 // 1 = service is active, 0 = inactive
    
    var serviceName : String = ""
    var subServiceName : String = ""
    
    
    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var subSubServiceName : String = ""
    var completionTime : String = ""
    var isSelected = false
    
    init(dict : [String : Any]){
        
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subserviceId"] as? Int ?? 0
        subSubServiceId = dict["artistServiceId"] as? Int ?? 0
        
        businessId = dict["businessId"] as? Int ?? 0
        deleteStatus = dict["deleteStatus"] as? Int ?? 0
        artistId = dict["artistId"] as? Int ?? 0
        staffId = dict["staffId"] as? Int ?? 0
        status = dict["status"] as? Int ?? 0
       
        subSubServiceName = dict["title"] as? String ?? ""
        completionTime = dict["completionTime"] as? String ?? ""
        
        //inCallPrice = dict["inCallPrice"] as? Double ?? 0
        //outCallPrice = dict["outCallPrice"] as? Double ?? 0
        
        if let val = dict["inCallPrice"] as? Double {
            inCallPrice = val
        }else{
            if let val = dict["inCallPrice"] as? String {
                inCallPrice = Double(val) ?? 0
            }
        }
        if let val = dict["outCallPrice"] as? Double {
            outCallPrice = val
        }else{
            if let val = dict["outCallPrice"] as? String {
                outCallPrice = Double(val) ?? 0
            }
        }
    }
}


/*
{
    holiday = 2;
    job = Moderate;
    mediaAccess = Editor;
    staffId = 13;
    staffImage = "http://koobi.co.uk:3000/uploads/profile/1524478277525.jpg";
    staffName = vinod;
 
    staffHours =                 (
        {
            day = 0;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 1;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 2;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 3;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 4;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 5;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    },
        {
            day = 6;
            endTime = "07:00 PM";
            startTime = "10:00 AM";
    }
    );

    staffService =                 (
        {
            "__v" = 0;
            "_id" = 58;
            artistId = 13;
            artistServiceId = 9;
            businessId = 10;
            completionTime = "00:50";
            crd = "2018-05-22T13:14:29.796Z";
            deleteStatus = 1;
            inCallPrice = "44.0";
            outCallPrice = 0;
            serviceId = 1;
            staffId = 13;
            status = 1;
            subserviceId = 2;
            title = vip;
            upd = "2018-05-22T13:14:29.796Z";
    },
        {
            "__v" = 0;
            "_id" = 59;
            artistId = 13;
            artistServiceId = 8;
            businessId = 10;
            completionTime = "01:20";
            crd = "2018-05-22T13:14:29.796Z";
            deleteStatus = 1;
            inCallPrice = "11.0";
            outCallPrice = "22.0";
            serviceId = 1;
            staffId = 13;
            status = 1;
            subserviceId = 2;
            title = Normal;
            upd = "2018-05-22T13:14:29.796Z";
    }
    );
    staffServiceId =                 (
        9,
        8
    );
},


*/

//Old
//
//class StaffInfo {
//
//    var _id : Int = 0
//    var profileImage : String = ""
//    var userName : String = ""
//    var serviceName : String = ""
//
//
//    init(dict : [String : Any]){
//
//        _id = dict["_id"] as? Int ?? 0
//        profileImage = dict["profileImage"] as? String ?? ""
//        userName = dict["userName"] as? String ?? ""
//        serviceName = dict["serviceName"] as? String ?? ""
//
//    }
//}
//

