//
//  SubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class SubService {
    var isSelectLocal : Bool = false
    var isSelected : Bool = false
    var serviceId : Int = 0
    var serviceName : String = ""
    
    var subServiceId : Int = 0
    var subServiceName : String = ""

    //var arrSubSubService : [SubSubService] = [SubSubService]()
    var arrInCall : [SubSubService] = [SubSubService]()
    var arrOutCall : [SubSubService] = [SubSubService]()
    var arrTotalService : [SubSubService] = [SubSubService]()
    
    init(dict : [String : Any]){
        
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subServiceName = dict["subServiceName"] as? String ?? ""
        
        if let arr = dict["artistservices"] as? [[String : Any]]{
            for dict in arr{
                let objSubSubService = SubSubService.init(dict: dict)
                let strType = dict["bookingType"] as? String ?? ""
                if strType == "Incall"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                    self.arrInCall.append(objSubSubService)
                }else if strType == "Outcall"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                self.arrOutCall.append(objSubSubService)
                }else if strType == "Both"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                    self.arrInCall.append(objSubSubService)
                    self.arrOutCall.append(objSubSubService)
                }
            }
        }
                
    }
}

