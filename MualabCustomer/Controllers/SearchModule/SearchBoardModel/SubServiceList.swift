//
//  Service.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//


import Foundation

public class SubServiceList {
    
     var _id : Int = 0
	 var image : String = ""
	 var serviceId : Int = 0
	 var title : String = ""
     var isSelected: Bool = false
    var isSelectLocal : Bool = false

    init(dict : [String : Any]){
        isSelectLocal = false
        isSelected = false

        _id = dict["_id"] as? Int ?? 0
        image = dict["image"] as? String ?? ""
        serviceId = dict["serviceId"] as? Int ?? 0
        title = dict["title"] as? String ?? ""
    }
}


public class Certificate {
    var _id : Int = 0
    var artisteId : Int = 0
    var title : String = ""
    var isSelected: Bool = false
    var isSelectLocal : Bool = false
    
    init(dict : [String : Any]){
        isSelectLocal = false
        isSelected = false
        
        _id = dict["_id"] as? Int ?? 0
        artisteId = dict["artistId"] as? Int ?? 0
        title = dict["title"] as? String ?? ""
    }
}
