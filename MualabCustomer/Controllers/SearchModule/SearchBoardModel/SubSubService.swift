//
//  SubSubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class SubSubService : NSObject{
    
    var artistId : Int = 0
    var serviceId : Int = 0
    var serviceName : String = ""
    
    var subServiceId : Int = 0
    var subServiceName : String = ""
    var subServiceDescription : String = ""

    var subSubServiceId : Int = 0
    var subSubServiceName : String = ""
    var isStaff : Int = 0
    var isInCallStaff : Int = 0
    var isOutCallStaff : Int = 0

    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var completionTime : String = ""
    var isSelected = false
    
    ////Optional Properties
    var staffIdForEdit : Int = 0
    var strDateForEdit : String = ""
    var strSlotForEdit : String = ""
    var strInCallPrice : String = ""
    var strOutCallPrice : String = ""
    ////
    
    init(dict : [String : Any]){
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subSubServiceId = dict["_id"] as? Int ?? 0
        subSubServiceName = dict["title"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
        subServiceDescription = dict["description"] as? String ?? ""
        isStaff = dict["isStaff"] as? Int ?? 0
        isInCallStaff = dict["incallStaff"] as? Int ?? 0
        isOutCallStaff = dict["outcallStaff"] as? Int ?? 0
    }
}


