//
//  SearchBoardCell.swift
//  MualabCustomer
//
//  Created by Mac on 15/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView

@objc protocol SearchBoardCellDelegate{
    func btnBookTapped(at index:IndexPath)
    func btnProfileImageTapped(at index:IndexPath)
}

class SearchBoardCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var imgVwOuter: UIImageView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblImg: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    //@IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var Collection: UICollectionView!

    @IBOutlet weak var btnBook: UIButton!
    
    @IBOutlet weak var imgVwFovorite: UIImageView!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    
    @IBOutlet weak var collectionView: UIView!
    var arrServices = [String]()
    weak var delegate:SearchBoardCellDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Collection.delegate = self
        self.Collection.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnBookAction(_ sender: Any) {
        self.delegate?.btnBookTapped(at: indexPath)
    }
    
    @IBAction func btnProfileImageAction(_ sender: Any) {
        self.delegate?.btnProfileImageTapped(at: indexPath)
    }
    
    func setDataInCollection(obj:ArtistDetails){
        arrServices.removeAll()
        arrServices = obj.arrServiceNames
        self.Collection.reloadData()
    }
}


//MARK:- Collection view Delegate Methods
extension SearchBoardCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrServices.count >= 3 {
            return 2
        }else{
            return arrServices.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = arrServices[indexPath.row]
        //cell.lblUserName.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        cell.btnService.superview?.tag = self.indexPath.row
        cell.btnService.tag = indexPath.row
        cell.lblUserName.text = obj
        let a = cell.lblUserName.layer.frame.height
        cell.lblUserName.layer.cornerRadius = 9
        cell.lblUserName.layer.masksToBounds = true
        cell.lblBorder.layer.cornerRadius = 10
        cell.lblBorder.layer.masksToBounds = true
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 22
        cellWidth = CGFloat(self.collectionView.frame.size.width-2)
        
        let a = arrServices[indexPath.row]
        
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 14)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+6
        cellHeight = 22
 
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
