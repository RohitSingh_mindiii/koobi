//
//  CellMyContact.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/2/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class CellMyContact: UITableViewCell {

        @IBOutlet weak var imgProfile: UIImageView!
        @IBOutlet weak var lblName: UILabel!
        @IBOutlet weak var lblContact: UILabel!

        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
            override func setSelected(_ selected: Bool, animated: Bool) {
                super.setSelected(selected, animated: animated)

                // Configure the view for the selected state
            }

}
