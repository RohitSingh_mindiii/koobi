//
//  MyStringItemSource.swift
//  
//
//  Created by Mindiii on 5/29/18.
//

import UIKit
let objMyStringItemSource = MyStringItemSource()
class MyStringItemSource: NSObject, UIActivityItemSource {
    var data = ""
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        
        if activityType!.rawValue == UIActivity.ActivityType.message.rawValue {
            return objMyStringItemSource.data
        }
        else if activityType!.rawValue == UIActivity.ActivityType.mail.rawValue
        {
            return objMyStringItemSource.data
        }
        else if activityType!.rawValue == UIActivity.ActivityType.postToTwitter.rawValue {
            
            return objMyStringItemSource.data
        }
        else if activityType!.rawValue == UIActivity.ActivityType.postToFacebook.rawValue {
            
            return objMyStringItemSource.data
            
        }
        else if activityType!.rawValue ==  "net.whatsapp.WhatsApp.ShareExtension" {
            
            return objMyStringItemSource.data
            
        }
        else
        {
            return objMyStringItemSource.data
        }
        return nil
    }
 
    

    @objc func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
   @objc func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
    if activityType == UIActivity.ActivityType.message.rawValue {
            return "String for message" as AnyObject
    } else if activityType == UIActivity.ActivityType.mail.rawValue
        {
            return "String for mail" as AnyObject
    } else if activityType == UIActivity.ActivityType.postToTwitter.rawValue {
            return "String for twitter" as AnyObject
    } else if activityType == UIActivity.ActivityType.postToFacebook.rawValue {
            return "String for facebook" as AnyObject
        }
        return nil
    }
 
    
}
