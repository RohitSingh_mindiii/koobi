//
//  ShareVoucherCodeVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 4/29/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import ContactsUI

class ShareVoucherCodeVC: UIViewController,CNContactPickerDelegate {

    
    @IBOutlet weak var viewVoucherPopUp: UIView!
    @IBOutlet weak var lblVoucherLink: UILabel!
    @IBOutlet weak var txtVoucherLink: UITextView!

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    var fromVoucher = true
    var objFeeds = feeds(dict: ["":""])
    
    var objData = VoucherModel(dict: ["":""])
    override func viewDidLoad() {
        super.viewDidLoad()
        var linkText = WebURL.Deeplinking

        self.viewVoucherPopUp.isHidden = true
        if self.fromVoucher == true{
            self.txtVoucherLink.isHidden = true
            self.lblVoucherLink.isHidden = false
            self.lblHeader.text = "Share Voucher Code"
            self.lblTitle.text = "Send this voucher link to people you want to message with on Koobi Social. "
            linkText = WebURL.Deeplinking+"allvoucherList/"+String( objData!._id)
        }else{
            self.txtVoucherLink.isHidden = false
            self.lblVoucherLink.isHidden = true
            self.lblHeader.text = "Share Post"
            self.lblTitle.text = "Send this post to your friends & family"
            linkText = WebURL.Deeplinking+"feedDetails/"+String( objData!._id)

        }

        if self.fromVoucher == true{
            self.lblVoucherLink.text  = linkText//"Check this Out"
        }else{
            var b = ""
            if (objFeeds?.arrFeed.count)!>0{
                b = String(objFeeds?.arrFeed[0].feedPost ?? "")
            }else{
            }
            let c =  String(objFeeds?.caption ?? "")
            var d  = c+"\n"+b
            if c==""{
                 //d = b
                 d = ""
            }
            var f = d+"\n"+linkText
            if d == ""{
               f = linkText
            }
            self.txtVoucherLink.text  = f
        }
        self.passDataInLable()
    }
    
    @IBAction func btnShearLink(_ sender: UIButton) {
        self.view.endEditing(true)
        var deeplink = WebURL.Deeplinking
        if self.fromVoucher == true{
            deeplink = deeplink+"allvoucherList/"
        }else{
            deeplink = deeplink+"feedDetails/"
        }
        
       // self.lblVoucherLink.text = deeplink+String( objData!._id)
        self.viewVoucherPopUp.isHidden = false
    }
    
    @IBAction func btnCloseLinkShare(_ sender: UIButton) {
        self.viewVoucherPopUp.isHidden = true
    }
    
    
    func passDataInLable(){
        
        let objData = objFeeds
        
        var firstActivityItem = "Hello, Check Out this post on Koobi "
        if objData?.caption != ""{
            firstActivityItem = firstActivityItem+"\n"+String(objData?.caption ?? "")
        }
        
        var deeplink = WebURL.Deeplinking+"feedDetail/"
        
        var urlString = ""
        let a = objData?.arrFeed.count
        if a ?? 0  > 0 {
            deeplink = deeplink+String( objData?._id ?? 0)
            urlString = (objData?.arrFeed[0].feedPost ?? "")
        }
     
        // If you want to put an image
        let aa = firstActivityItem+"\n\n"+urlString+"\n\n"+deeplink
        //self.txtVoucherLink.text = firstActivityItem+"\n\n"+urlString+"\n\n"+deeplink
        self.txtVoucherLink.text = firstActivityItem+"\n\n"+deeplink
    }
    
    
    @IBAction func btnShearLinkOption(_ sender: UIButton) {
        self.viewVoucherPopUp.isHidden = true
        if fromVoucher == false{
            
            let objData = objFeeds
            
            var firstActivityItem = "Hello, Check Out this post on Koobi "
            if objData?.caption != ""{
                firstActivityItem = firstActivityItem+"        "+String(objData?.caption ?? "")
            }
            
            var deeplink = WebURL.Deeplinking+"feedDetail/"
            
            var urlString = ""
            let a = objData?.arrFeed.count
            if a ?? 0  > 0 {
                deeplink = deeplink+String( objData?._id ?? 0)
                urlString = (objData?.arrFeed[0].feedPost ?? "")
            }
          
            // If you want to put an image
            //let aa = firstActivityItem+"\n\n"+urlString+"\n\n"+deeplink
            let aa = firstActivityItem+"\n\n"+deeplink
            
            objMyStringItemSource.data = aa
            
            let activityViewController = UIActivityViewController(activityItems: [MyStringItemSource()] as [AnyObject], applicationActivities: nil)
            activityViewController.setValue("Koobi", forKey: "Subject")
            if let window = (UIApplication.shared.delegate as? AppDelegate)?.window
            {  window.rootViewController?.present(activityViewController, animated: true, completion: nil)  }
            
        }else{
            
            var deeplink = WebURL.Deeplinking+"allvoucherList/"
            deeplink = deeplink+String( objData!._id)
            let appLink = "Enter the Voucher Code during the checkout process on our app Hurry, it doesn't get better than this! Tap "+"\n"+deeplink+"\n"+" to use my voucher code"
            objMyStringItemSource.data = appLink
            
            let activityViewController = UIActivityViewController(activityItems: [MyStringItemSource()] as [AnyObject], applicationActivities: nil)
            activityViewController.setValue("Koobi", forKey: "Subject")
            if let window = (UIApplication.shared.delegate as? AppDelegate)?.window
            {  window.rootViewController?.present(activityViewController, animated: true, completion: nil)  }
            
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOpenContactList(_ sender: UIButton) {
        
        
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"MyContactVC") as? MyContactVC{
            objVC.hidesBottomBarWhenPushed = true
            
            if fromVoucher == true{
                var deeplink = WebURL.Deeplinking+"allvoucherList/"
                deeplink = deeplink+String( objData!._id)
                let appLink = "Enter the Voucher Code during the checkout process on our app Hurry, it doesn't get better than this! Tap "+deeplink+" to use my voucher code"
                objVC.shareText = appLink
                
            }else{
                let objData = objFeeds
                var urlString = ""
                if objData?.feedType != "text" &&  (objData?.arrFeed.count ?? 0) > 0{
                    urlString =  objData?.arrFeed[0].feedPost ?? ""
                }
                let id = String(objData?._id ?? 0)
                let firstActivityItem = "Hello, Check Out this post on Koobi "+"\n"+urlString+"\n"+String(objData?.caption ?? "")
                let deeplink = WebURL.Deeplinking+"feedDetail/"+id
                objVC.shareText = firstActivityItem+"\n"+deeplink
            }
            self.navigationController?.pushViewController(objVC, animated: true)
        }

    }
    
 
    
}
