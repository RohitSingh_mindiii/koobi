//
//  ModelAddContactList.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelAddContactList: NSObject {

     public var contactNo = ""
    public var name = ""
    public var type = ""

    
    init(dict: [String : Any]) {
        contactNo = String(dict["contactNo"] as? Int ?? 0)
        if let _id1 = dict["contactNo"] as? String{
            contactNo = _id1
        }
        name = dict["name"] as? String ?? ""
        type = dict["type"] as? String ?? ""
    }
}
