//
//  TabBarController.swift
//  MualabCustomer
//
//  Created by Mac on 16/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class TabBarController: UITabBarController {
    
    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    
    let SEPARATOR_WIDTH = 1.0
    let blueColor = UIColor(red: 119.0 / 255.0, green: 175.0 / 255.0, blue: 58.0 / 255.0, alpha: 1).cgColor
    var selectedTabbarIndex:Int = 0
    
    let kDefaultImg1 = UIImage.init(named: "inactive_leaderbord_ico")
    let kDefaultImg2 = UIImage.init(named: "inactive_feeds_ico")
    let kDefaultImg3 = UIImage.init(named: "inactive_add_ico")
    let kDefaultImg4 = UIImage.init(named: "inactive_search_ico")
    let kDefaultImg5 = UIImage.init(named: "inactive_notifications_ico")
    
    
    let kSelectedImg1 = UIImage.init(named: "active_leaderboard_ico")
    let kSelectedImg2 = UIImage.init(named: "active_feeds_ico")
    let kSelectedImg3 = UIImage.init(named: "active_add_ico")
    let kSelectedImg4 = UIImage.init(named: "active_search_ico")
    let kSelectedImg5 = UIImage.init(named: "active_notifications_ico")
    
    let kTitleForTab1 = ""
    let kTitleForTab2 = ""
    let kTitleForTab3 = ""
    let kTitleForTab4 = ""
    let kTitleForTab5 = ""
    
    let kNameStoryBoard1 = "SearchBoard"
    let kNameStoryBoard2 = "Feed"
    let kNameStoryBoard3 = "AddFeed"
    let kNameStoryBoard4 = "Explore"
    let kNameStoryBoard5 = "Notification"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objAppShareData.isTabChange = true
        addTopBorderOnTabBar()
        for vc in self.viewControllers! {
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.manageBookingRequestCount()
        self.NotificationCount()
        
        //addSeparatorsOnTabBar()
        //addUnderlinInTabBarItem()
        // self.addTabBarItems()
        //self.setTitleAndIconOFTabBar()
        // UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0, -2)
        
        //Assign self for delegate for that ViewController can respond to UITabBarControllerDelegate methods
        self.delegate = self
        ////
        self.selectedIndex = objAppShareData.selectedTab
        self.tabBar.isTranslucent = false
        ////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if objAppShareData.isFromNotification {
            //notificationType : "booking","feed","payment","profile"
            if objAppShareData.notificationType == "feed" || objAppShareData.notificationType == "profile" || objAppShareData.notificationType == "story" || objAppShareData.notificationType == "chat"  {
                //self.selectedIndex = 1
                self.selectedIndex = 3
            }else if objAppShareData.notificationType == "booking" {
                self.selectedIndex = 0
            }
        }else{
            self.selectedIndex = objAppShareData.selectedTab
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

//MARK: UITabBarControllerDelegate method
extension TabBarController : UITabBarControllerDelegate{
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if objAppShareData.selectedTab != tabBarController.selectedIndex{
            objAppShareData.isTabChange = true
        }
        if tabBarController.selectedIndex != 2 {
            objAppShareData.selectedTab = tabBarController.selectedIndex
        }
    }
}

//MARK: Customize UITabBarController method
extension TabBarController{
    
    func NotificationCount(){
        /*
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strMyId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strMyId = String(id)
            }
        }
        ref = Database.database().reference()
    ref.child("socialBookingBadgeCount").child(strMyId).observe(.value, with: { [weak self] (snapshot) in
                guard let strongSelf = self else {
                    return
                }
                let dict = snapshot.value as? [String:Any]
                print(dict)
                var totalCount = 0
                if let count = dict?["socialCount"] as? Int {
                    totalCount = count+totalCount
                }else if let count = dict?["socialCount"] as? String {
                    totalCount = (Int(count) ?? 0)+totalCount
                }
                if let count = dict?["bookingCount"] as? Int {
                    totalCount = count+totalCount
                }else if let count = dict?["bookingCount"] as? String {
                    totalCount = (Int(count) ?? 0)+totalCount
                }
                if totalCount == 0{
                    self?.tabBar.items?[4].badgeColor = UIColor.clear
                    self?.tabBar.items?[4].badgeValue =  "0"
                }else{
                    self?.tabBar.items?[4].badgeColor = UIColor.red
                    self?.tabBar.items?[4].badgeValue =  String(totalCount)
                    if  totalCount >= 100{
                        self?.tabBar.items?[4].badgeValue = "99+"
                    }
                }
            })
    */
    }
    
    func addTabBarItems(){
        
        //        let storyBoard1 : UIStoryboard = UIStoryboard.init(name: kNameStoryBoard1, bundle: nil)
        //        var searchBoardVC = storyBoard1.instantiateViewController(withIdentifier: "SearchBoardVC") as! SearchBoardVC
        //        let nav1 = UINavigationController(rootViewController: searchBoardVC)
        //        let storyBoard2 : UIStoryboard = UIStoryboard.init(name: kNameStoryBoard2, bundle: nil)
        //        var feedVc = storyBoard1.instantiateViewController(withIdentifier: "FeedVc") as! FeedVc
        //        let nav2 = UINavigationController(rootViewController: feedVc)
        //
        
        /*
         
         // Create Tab one
         let tabOne = TabOneViewController()
         let tabBarItem1 = UITabBarItem(title: "Tab 1", image: UIImage(named: "defaultImage.png"), selectedImage: UIImage(named: "selectedImage.png"))
         tabOne.tabBarItem = tabBarItem1
         
         
         // Create Tab two
         let tabTwo = TabTwoViewController()
         let tabBarItem2 = UITabBarItem(title: "Tab 2", image: UIImage(named: "defaultImage2.png"), selectedImage: UIImage(named: "selectedImage2.png"))
         tabTwo.tabBarItem = tabBarItem1
         self.viewControllers = [tabOne, tabTwo]
         
         */
        
    }
    
    func setTitleAndIconOFTabBar() {
        
        let tabBar: UITabBar? = self.tabBar
        let tabBarItem1 = tabBar?.items?[0]
        //let tabBarItem2 = tabBar?.items?[1]
        let tabBarItem2 = tabBar?.items?[3]
        let tabBarItem3 = tabBar?.items?[2]
        //let tabBarItem4 = tabBar?.items?[3]
        let tabBarItem4 = tabBar?.items?[1]
        let tabBarItem5 = tabBar?.items?[4]
        
        
        tabBarItem1?.title = kTitleForTab1
        tabBarItem1?.selectedImage = kSelectedImg1?.withRenderingMode(.alwaysOriginal)
        tabBarItem1?.image = kDefaultImg1?.withRenderingMode(.alwaysOriginal)
        
        tabBarItem2?.title = kTitleForTab2
        tabBarItem2?.selectedImage = kSelectedImg2?.withRenderingMode(.alwaysOriginal)
        tabBarItem2?.image = kDefaultImg2?.withRenderingMode(.alwaysOriginal)
        
        tabBarItem3?.title = kTitleForTab3
        tabBarItem3?.selectedImage = kSelectedImg3?.withRenderingMode(.alwaysOriginal)
        tabBarItem3?.image = kDefaultImg3?.withRenderingMode(.alwaysOriginal)
        
        tabBarItem4?.title = kTitleForTab4
        tabBarItem4?.selectedImage = kSelectedImg4?.withRenderingMode(.alwaysOriginal)
        tabBarItem4?.image = kDefaultImg4?.withRenderingMode(.alwaysOriginal)
        
        tabBarItem5?.title = kTitleForTab5
        tabBarItem5?.selectedImage = kSelectedImg5?.withRenderingMode(.alwaysOriginal)
        tabBarItem5?.image = kDefaultImg5?.withRenderingMode(.alwaysOriginal)
    }
    
    func addTopBorderOnTabBar(){
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 0.5)
        // use for the border.
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        tabBar.layer.addSublayer(topBorder)
    }
    
    func addSeparatorsOnTabBar(){
        
        let itemWidth = floor(self.tabBar.frame.size.width / CGFloat(self.tabBar.items!.count))
        
        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height))
        
        for i in 0..<(tabBar.items?.count)! - 1
        {
            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(SEPARATOR_WIDTH / 2), y: 10, width: CGFloat(SEPARATOR_WIDTH), height: self.tabBar.frame.size.height-20))
            separator.backgroundColor = UIColor(red: 98.0 / 255.0, green: 98.0 / 255.0, blue: 98.0 / 255.0, alpha: 1)
            bgView.addSubview(separator)
            
        }
        
        UIGraphicsBeginImageContext(bgView.bounds.size)
        bgView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UITabBar.appearance().backgroundImage = tabBarBackground
        
    }
    
    func addUnderlinInTabBarItem() {
        
        let tabBar: UITabBar? = self.tabBar
        let divide = 5.0
        
        let view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
        let border = UIImageView(frame: CGRect(x: view.frame.origin.x + 5, y: view.frame.size.height - 6, width: self.view.frame.size.width / CGFloat(divide) - 10, height: 5))
        border.backgroundColor = UIColor(red: 119.0 / 255.0, green: 175.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        
        view.addSubview(border)
        
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //  UITabBar.appearance().backgroundImage = tabBarBackground
        self.tabBar.tintColor = UIColor(red: 119.0 / 255.0, green: 175.0 / 255.0, blue: 58.0 / 255.0, alpha: 1)
        
        //bottom line
        tabBar?.selectionIndicatorImage = tabBarBackground
    }
    
    func manageBookingRequestCount(){
        var  idUser = 0
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? [String:Any] {
            idUser = dicUser["_id"] as? Int ?? 0
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            idUser = userInfo["_id"] as? Int ?? 0
        }
        var counts = 0
        ref = Database.database().reference()
        ref.child("socialBookingBadgeCount").child(String(idUser)).observe(.value, with: { (snapshot) in
            let dict = snapshot.value as? [String:Any]
            if let count = dict?["businessInvitationCount"] as? Int {
                //self.lblBusinessInvitationCount.text = String(count)
                self.tabBar.items?[4].badgeValue =  String(count)
                counts = count
            }else if let count = dict?["businessInvitationCount"] as? String {
                //self.lblBusinessInvitationCount.text = String(count)
                self.tabBar.items?[4].badgeValue =  String(count)
                counts = Int(count) ?? 0
            }
            
            if counts == 0{
                self.tabBar.items?[4].badgeColor = UIColor.clear
                self.tabBar.items?[4].badgeValue =  "0"
            }else{
                self.tabBar.items?[4].badgeColor = UIColor.red
                self.tabBar.items?[4].badgeValue =  String(counts)
                if  counts >= 100{
                    self.tabBar.items?[4].badgeValue = "99+"
                }
            }
        })
    }
}

