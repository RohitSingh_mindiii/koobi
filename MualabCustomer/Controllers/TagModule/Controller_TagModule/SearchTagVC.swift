//
//  SearchTagVC.swift
//  MualabCustomer
//
//  Created by Mac on 06/07/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol SearchTagVCDelegate {
    func finishSectionOnSearchTagVCWithData(objExpSearchTag: ExpSearchTag?)
}

class SearchTagVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    weak var delegate: SearchTagVCDelegate?
    
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var lblNoResults: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    
    fileprivate var strSearchValue : String = ""
    
    var tabType : String = "people"
    var previousSearchText : String = ""
    
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var arrExpSearchTag = [ExpSearchTag]()
    
    let pageSize = 10 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtSearch.delegate = self
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataWithPageCount(page: 0, strSearchText: "")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadDataWithPageCount(page: 0, strSearchText: objAppShareData.strExpSearchText)
    }
}

// MARK:- Btn Action
extension SearchTagVC  {
    
    @IBAction func btnCancelAction(_ sender: Any) {
         //dismiss(animated: true, completion: nil)
         delegate?.finishSectionOnSearchTagVCWithData(objExpSearchTag: nil)
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        btnSearch.isHidden = true
        txtSearch.becomeFirstResponder()
    }
}

// MARK:- TableView Delegates
extension SearchTagVC  {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrExpSearchTag.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if self.arrExpSearchTag.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                loadDataWithPageCount(page: nextPage)
            }
        }
        
        if let cell : CellTop = tableView.dequeueReusableCell(withIdentifier: "CellTop")! as? CellTop{
            
            let objExpSearchTag = self.arrExpSearchTag[indexPath.row]
            
            cell.lblName.text = objExpSearchTag.title
            cell.lblPostDetails.text = objExpSearchTag.desc
            cell.imgVwProfile.image = UIImage(named: "cellBackground")
            cell.lblPostDetails.isHidden = false
            
            if objExpSearchTag.tabType == "top" || objExpSearchTag.tabType == "people"{
                
                if objExpSearchTag.imageUrl != "" {
                    if let url = URL(string: objExpSearchTag.imageUrl){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                        cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }
                
            }else if objExpSearchTag.tabType == "hasTag" {
                cell.lblName.text = "#" + objExpSearchTag.title
                cell.imgVwProfile.image = UIImage(named: "hagTag_ico")
            }else if objExpSearchTag.tabType == "place" {
                cell.lblPostDetails.isHidden = true
                cell.imgVwProfile.image = UIImage(named: "new_map_ico")
            }else{
                cell.imgVwProfile.image = UIImage(named: "cellBackground")
            }
            
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objExpSearchTag = self.arrExpSearchTag[indexPath.row]
        
//        let dicParam = [
//            "tabType": objExpSearchTag.tabType,
//            "tagId": objExpSearchTag.id,
//            "title": objExpSearchTag.title
//            ] as [String : Any]
        
         delegate?.finishSectionOnSearchTagVCWithData(objExpSearchTag: objExpSearchTag)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        self.parent?.view.endEditing(true)
    }
}

// MARK: - Webservices call

extension SearchTagVC {
    
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        
        lastLoadedPage = page
        self.pageNo = page
        
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        previousSearchText = strSearchText
        let dicParam = [ "search": strSearchText,
                         "page": self.pageNo,
                         "limit": "10",
                         "type": "people",
                         "userId":strUserId ] as [String : Any]
        
        callWebserviceFor_getData(dicParam: dicParam)
    }
    
    func callWebserviceFor_getData(dicParam: [AnyHashable: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
            self.activity.startAnimating()
            // objWebserviceManager.StartIndicator()
        }
        self.lblNoResults.isHidden = true
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.exploreSearch, params: parameters , success: { response in
            
            self.refreshControl.endRefreshing()
            
            self.activity.stopAnimating()
            
            if self.pageNo == 0 {
                self.arrExpSearchTag.removeAll()
                self.tblView.reloadData()
                if self.arrExpSearchTag.count > 0{
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tblView.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                
                if let totalCount = response["totalCount"] as? Int{
                    self.totalCount = totalCount
                }
                
                var arrTemp = [[String : Any]]()
                if response.keys.contains("topList"){
                    if let arrDict = response["topList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                }else if response.keys.contains("peopleList"){
                    if let arrDict = response["peopleList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("hasTagList"){
                    if let arrDict = response["hasTagList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("placeList"){
                    if let arrDict = response["placeList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                    
                }else if response.keys.contains("serviceTagList"){
                    if let arrDict = response["serviceTagList"] as? [[String:Any]]{
                        arrTemp = arrDict
                    }
                }
                
                
                if arrTemp.count > 0 {
                    
                    for dict in arrTemp{
                        
                        let objExpSearchTag = ExpSearchTag.init(dict: dict)
                        
                        switch (self.tabType){
                            
                        case "top":
                            objExpSearchTag.type = 0;
                            objExpSearchTag.tabType = "top"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "post";
                            break;
                            
                        case "people":
                            objExpSearchTag.type = 1;
                            objExpSearchTag.tabType = "people"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            objExpSearchTag.desc = "\(objExpSearchTag.postCount) " + "post";
                            break;
                            
                        case "hasTag":
                            objExpSearchTag.type = 2;
                            objExpSearchTag.tabType = "hasTag"
                            objExpSearchTag.title = objExpSearchTag.tag;
                            objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                            break;
                            
                        case "serviceTag":
                            objExpSearchTag.type = 3;
                            objExpSearchTag.tabType = "serviceTag"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                            break;
                            
                        case "place":
                            objExpSearchTag.type = 4;
                            objExpSearchTag.tabType = "place"
                            objExpSearchTag.title = objExpSearchTag.uniTxt;
                            objExpSearchTag.desc = "\(objExpSearchTag.postCount) " + "public post";
                            break;
                        default:
                            break;
                        }
                        self.arrExpSearchTag.append(objExpSearchTag)
                    }
                }
                
            }else{
                if strSucessStatus == "fail"{
                    
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
            
            self.tblView.reloadData()
            if self.arrExpSearchTag.count > 0{
                let indexPath = IndexPath.init(row: 0, section: 0)
                self.tblView.scrollToRow(at: indexPath, at: .top, animated: false)
            }
            if self.arrExpSearchTag.count==0{
                self.lblNoResults.isHidden = false
            }else{
                self.lblNoResults.isHidden = true
            }
            
        }) { (error) in
            
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            
            self.tblView.reloadData()
            if self.arrExpSearchTag.count > 0{
                let indexPath = IndexPath.init(row: 0, section: 0)
                self.tblView.scrollToRow(at: indexPath, at: .top, animated: false)
            }
            if self.arrExpSearchTag.count==0{
                self.lblNoResults.isHidden = false
            }else{
                self.lblNoResults.isHidden = true
            }
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

// MARK: - UITextfield Delegate
extension SearchTagVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            //getLikesUsersData(0, andSearchText: "")
            loadDataWithPageCount(page: 0, strSearchText: "")
        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString
        if (text.length == 0){
            btnSearch.isHidden = false
        }
        textField.resignFirstResponder()
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        //getLikesUsersData(0, andSearchText: "")
        loadDataWithPageCount(page: 0, strSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        //getLikesUsersData(0, andSearchText: strSearchValue)
        loadDataWithPageCount(page: 0, strSearchText: strSearchValue)
    }
}


