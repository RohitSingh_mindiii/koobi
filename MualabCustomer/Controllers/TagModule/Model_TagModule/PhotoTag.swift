
//
//  PhotoTag.swift
//  DemoImageTagSwift
//
//  Created by Mac on 10/07/18.
//  Copyright © 2018 Amit. All rights reserved.
//

import Foundation

class PhotoInfo: NSObject {

    var image: UIImage?
    var arrTags: [TagInfo] = []
    var arrTagPopovers: [EBTagPopover] = []
    var objEBPhotoView : EBPhotoView?
    
    init(photoInfo: [AnyHashable : Any]) {
        super.init()
        self.image = photoInfo["imageFile"] as? UIImage
        if let arr = photoInfo["tags"] as? [TagInfo]{
             self.arrTags = arr
        }
    }
}

class TagInfo: NSObject, EBPhotoTagProtocol {
    
    var tagPosition = CGPoint.zero
    var attributedText: NSAttributedString?
    var text = ""
    var strTagType = ""
    var metaData: [AnyHashable : Any] = [:]
    
    init(tagInfo: [AnyHashable : Any]) {
        
            super.init()
            let tagPositionValue = tagInfo["tagPosition"] as? NSValue
            self.tagPosition = tagPositionValue?.cgPointValue ?? CGPoint.zero
            self.text = tagInfo["tagText"] as? String ?? ""
            attributedText = tagInfo["attributedTagText"] as? NSAttributedString
            if let anInfo = tagInfo["metaData"] as? [AnyHashable : Any] {
                self.metaData = anInfo
            }
    }
    
    class func initWithServerData(dict : [String : Any], dictDetail : [String : Any]) -> TagInfo {
       
        let objTagInfo = TagInfo.init(tagInfo: ["" : ""])
        
        if let unique_tag_id = dict["unique_tag_id"] as? String{
            objTagInfo.text = unique_tag_id
        }
        
//        if let dictTagDetails = dict["tagDetails"] as? [String : Any]{
//            objTagInfo.metaData = dictTagDetails
//        }
        objTagInfo.metaData = dictDetail
      
        var x_axis : Double = 0
        var y_axis : Double = 0
        
        if let x = dict["x_axis"] as? Double{
              x_axis = x
        }else{
            if let x = dict["x_axis"] as? String{
                x_axis = Double(x) ?? 0
            }
        }
        
        if let y = dict["y_axis"] as? Double{
            y_axis = y
        }else{
            if let y = dict["y_axis"] as? String{
                y_axis = Double(y) ?? 0
            }
        }
        objTagInfo.tagPosition.x =  CGFloat(x_axis/100)
        objTagInfo.tagPosition.y =  CGFloat(y_axis/100)
        
        return objTagInfo
    }
    
    func normalizedPosition() -> CGPoint {
//        if tagPosition.y <= 0.05{
//           tagPosition.y = 0.05
//        }
//        if tagPosition.x <= 0.05{
//            tagPosition.x = 0.05-0.2
//        }
//        if tagPosition.x >= 0.92{
//            tagPosition.x = 0.92-0.1
//        }
//        if tagPosition.y >= 0.95{
//            tagPosition.y = 0.95
//        }
        return tagPosition
    }
    
    func attributedTagText() -> NSAttributedString? {
        return attributedText
    }
    
    func tagText() -> String? {
        return text
    }
    
    func metaInfo() -> [AnyHashable : Any]? {
        return metaData
    }
}
