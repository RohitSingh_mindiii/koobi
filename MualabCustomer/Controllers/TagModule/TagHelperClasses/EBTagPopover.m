////
////  Created by Eddy Borja.
////  Copyright (c) 2014 Eddy Borja. All rights reserved.
///*
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// */
//
//
//#import "EBTagPopover.h"
//#import "EBPhotoPagesNotifications.h"
//#import <QuartzCore/QuartzCore.h>
//
//@interface EBTagPopover ()
//@property (weak) UIView *contentView;
//@property (weak) UITextField *tagTextField;
//@property (assign, getter = isCanceled) BOOL canceled;
//
//@end
//
//#pragma mark - EBTagPopover
//
//@implementation EBTagPopover
//
//- (id)init {
//
//    self = [super init];
//    if (self) {
//        [self initialize];
//    }
//    return self;
//}
//
//- (id)initWithDelegate:(id<EBTagPopoverDelegate>)delegate {
//
//    self = [super init];
//    if (self) {
//        NSAssert([(NSObject *)delegate conformsToProtocol:@protocol(EBTagPopoverDelegate)],
//                 @"A tag popover's delegate must conform to the EBTagPopoverDelegate protocol.");
//        [self initialize];
//        [self setDelegate:delegate];
//    }
//    return self;
//}
//
//- (id)initWithTag:(id<EBPhotoTagProtocol>)aTag
//{
//    self = [super init];
//    if(self){
//        NSAssert([(NSObject *)aTag conformsToProtocol:@protocol(EBPhotoTagProtocol)],
//                 @"A tag's data source must conform to EBPhotoTagProtocol.");
//            [self initialize];
//            [self setDataSource:aTag];
//            [self setText:self.dataSource.tagText];
//    }
//    return self;
//}
//
//- (void)initialize
//{
//    [self loadContentView];
//    [self loadGestureRecognizers];
//
//    CGSize tagInsets = CGSizeMake(-7, -6);
//    CGRect tagBounds = CGRectInset(self.contentView.bounds, tagInsets.width, tagInsets.height);
//    tagBounds.size.height += 10.0f;
//    tagBounds.origin.x = 0;
//    tagBounds.origin.y = 0;
//
//    [self setFrame:tagBounds];
//
//    [self setMinimumTextFieldSize:CGSizeMake(25, 14)];
//    [self setMinimumTextFieldSizeWhileEditing:CGSizeMake(54, 14)];
//    [self setMaximumTextLength:40];
//
//    [self setNormalizedArrowOffset:CGPointMake(0.0, 0.02)];
//
//    [self setOpaque:NO];
//    [self.contentView setFrame:CGRectOffset(self.contentView.frame,
//                                            -(tagInsets.width),
//                                            -(tagInsets.height)+10)];
//
//    [self beginObservations];
//}
//
//- (void)dealloc {
//
//    [self stopObservations];
//}
//
//#pragma mark -
//
//- (void)loadContentView
//{
//    UIView *contentView = [self newContentView];
//    [self addSubview:contentView];
//    [self setContentView:contentView];
//}
//
//- (void)loadGestureRecognizers
//{
//    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc]
//                                                initWithTarget:self
//                                                        action:@selector(didRecognizeSingleTap:)];
//    [singleTapGesture setNumberOfTapsRequired:1];
//
//    [self addGestureRecognizer:singleTapGesture];
//}
//
//- (UIView *)newContentView
//{
//    NSString *placeholderText = NSLocalizedString(@"New Tag",
//                                                  @"Appears as placeholder text before a user enters text for a photo tag.");
//    UIFont *textFieldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
//    CGSize tagSize = [placeholderText sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:12]}];
//    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, tagSize.width, tagSize.height)];
//    [textField setFont:textFieldFont];
//    [textField setBackgroundColor:[UIColor clearColor]];
//    if ([self.strTagType isEqualToString:@"service"]){
//       [textField setTextColor:[UIColor clearColor]];
//    }else{
//       [textField setTextColor:[UIColor whiteColor]];
//    }
//    [textField setPlaceholder:placeholderText];
//    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
//    [textField setKeyboardAppearance:UIKeyboardAppearanceAlert];
//    [textField setTextAlignment:NSTextAlignmentCenter];
//    [textField setReturnKeyType:UIReturnKeyDone];
//    [textField setEnablesReturnKeyAutomatically:YES];
//    [textField setDelegate:self];
//    [textField setUserInteractionEnabled:NO];
//
//    [self setTagTextField:textField];
//    return textField;
//}
//
//
//#pragma mark - Notifications
//
//- (void)beginObservations
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(tagTextFieldDidChangeWithNotification:)
//                                                 name:UITextFieldTextDidChangeNotification
//                                               object:nil];
//
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didReceiveCancelNotification:)
//                                                name:EBPhotoPagesControllerDidCancelTaggingNotification object:nil];
//}
//
//
//- (void)stopObservations
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//
//#pragma mark -
//
//- (void)tagTextFieldDidChangeWithNotification:(NSNotification *)aNotification
//{
//    //resize, reposition
//    if(aNotification.object == self.tagTextField){
//        [self resizeTextField];
//    }
//}
//
//
//- (NSString *)text
//{
//    return self.tagTextField.text;
//}
//
//- (void)setText:(NSString *)text
//{
//    [self.tagTextField setText:text];
//    [self resizeTextField];
//}
//
//- (void)setDelegate:(id<EBTagPopoverDelegate>)aDelegate
//{
//    NSAssert([aDelegate conformsToProtocol:@protocol(EBTagPopoverDelegate)],
//             @"EBTagPopover delegates must conform to EBTagPopoverDelegate protocol.");
//    _delegate = aDelegate;
//}
//
//
//- (void)presentPopoverFromPoint:(CGPoint)point
//                         inView:(UIView *)view
//                       animated:(BOOL)animated
//{
//    [self presentPopoverFromPoint:point
//                           inRect:view.frame
//                           inView:view
//         permittedArrowDirections:UIPopoverArrowDirectionUp
//                         animated:animated];
//}
//
//
//
//- (void)presentPopoverFromPoint:(CGPoint)point
//                         inView:(UIView *)view
//       permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
//                       animated:(BOOL)animated
//{
//    [self presentPopoverFromPoint:point
//                           inRect:view.frame
//                           inView:view
//         permittedArrowDirections:arrowDirections
//                         animated:animated];
//}
//
//- (void)presentPopoverFromPoint:(CGPoint)point
//                         inRect:(CGRect)rect
//                         inView:(UIView *)view
//       permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
//                       animated:(BOOL)animated;
//{
//    //[self setCenter:point];
//
//    [view addSubview:self];
//
//    CGPoint difference = CGPointMake(0,//(newCenter.x - point.x)/self.frame.size.width,
//                                     0.5);
//
//    [self.layer setAnchorPoint:CGPointMake(0.5-difference.x,0.5-difference.y)];
//
//    [self setCenter:point];
//
//    CGFloat tagMaximumX = CGRectGetMaxX(self.frame);
//    CGFloat tagMinimumX = CGRectGetMinX(self.frame);
//    CGFloat tagMaximumY = CGRectGetMaxY(self.frame);
//    CGFloat tagMinimumY = CGRectGetMinY(self.frame);
//
//    CGRect tagBoundary = CGRectInset(view.frame, 5, 5);
//    CGFloat boundsMinimumX = CGRectGetMinX(tagBoundary);
//    CGFloat boundsMaximumX = CGRectGetMaxX(tagBoundary);
//    CGFloat boundsMinimumY = CGRectGetMinY(tagBoundary);
//    CGFloat boundsMaximumY = CGRectGetMaxY(tagBoundary);
//
//    CGFloat xOffset = ((MIN(0, tagMinimumX - boundsMinimumX) + MAX(0, tagMaximumX - boundsMaximumX))/1.0);
//    CGFloat yOffset = ((MIN(0, tagMinimumY - boundsMinimumY) + MAX(0, tagMaximumY - boundsMaximumY))/1.0);
//
//
//    CGPoint newCenter = CGPointMake(point.x - xOffset,
//                                    point.y - yOffset);
//
//    [self setCenter:newCenter];
//
//
///*
//    CGRect newFrame = self.frame;
//    newFrame.origin.x = point.x;
//    newFrame.origin.y = point.y;
//    //[self setFrame:newFrame];
//
//    [self setTransform:CGAffineTransformMakeScale(0.3, 0.3)];
//    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//        [self setTransform:CGAffineTransformMakeScale(1,1)];
//    }completion:nil];
//*/
//
//}
//
//
//- (void)drawRect:(CGRect)fullRect
//{
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    float radius = 4.0f;
//    float arrowHeight =  10.0f; //this is how far the arrow extends from the rect
//    float arrowWidth = 16.0;
//
//    if ([self.strTagType isEqualToString:@"service"]){
//    }
//
//    fullRect = CGRectInset(fullRect, 1, 1);
//
//    CGRect containerRect = CGRectMake(fullRect.origin.x,
//                      fullRect.origin.y+arrowHeight,
//                      fullRect.size.width,
//                      fullRect.size.height-arrowHeight);
//
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//
//    CGMutablePathRef tagPath = CGPathCreateMutable();
//
//    //the starting point, top left corner
//    CGPathMoveToPoint(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect));
//
//    /*
//     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(arrowWidth*0.5), CGRectGetMinY(containerRect));
//     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(fullRect));
//     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+(arrowWidth*0.5), CGRectGetMinY(containerRect));
//     //CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(containerRect));
//    */
//
//    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
//    if (([self.strLeftOrRight isEqualToString:@"left"]) || (self.normalizedArrowPoint.x <= 0.1)){
//        if (([str isEqualToString:@"down"]) ||
//            (self.normalizedArrowPoint.y >= 0.98)){
////        if (([str isEqualToString:@"down"]) ||
////                (self.normalizedArrowPoint.y >= 0.87)){
//            //draw the arrow
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth), CGRectGetMinY(containerRect));//+20
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth*0.5), CGRectGetMinY(fullRect));//+10
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));//0
//
//        }else{
//            //draw the arrow
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(self.tagTextField.frame.size.width*0.5)+(arrowWidth*0.5), CGRectGetMinY(fullRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5+(arrowWidth), CGRectGetMinY(containerRect));
//        }
//    }else if (([self.strLeftOrRight isEqualToString:@"right"]) || (self.normalizedArrowPoint.x >= 0.9)){
//       if (([str isEqualToString:@"down"]) || (self.normalizedArrowPoint.y >= 0.98)){
////        if (([str isEqualToString:@"down"]) || (self.normalizedArrowPoint.y >= 0.87)){
//            //draw the arrow
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(self.tagTextField.frame.size.width*0.5)+(arrowWidth*0.5), CGRectGetMinY(fullRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5+(arrowWidth), CGRectGetMinY(containerRect));
//        }else{
//            //draw the arrow
//           CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth), CGRectGetMinY(containerRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth*0.5), CGRectGetMinY(fullRect));
//            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
//        }
//    }else{
//        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(arrowWidth*0.5), CGRectGetMinY(containerRect));
//        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(fullRect));
//        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+(arrowWidth*0.5), CGRectGetMinY(containerRect));
//        //CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(containerRect));
//    }
//
//    //top right corner
//    CGPathAddArc(tagPath, NULL, CGRectGetMaxX(containerRect) - radius, CGRectGetMinY(containerRect) + radius, radius, 3 * (float)M_PI / 2, 0, 0);
//
//    //bottom right corner
//    CGPathAddArc(tagPath, NULL, CGRectGetMaxX(containerRect) - radius, CGRectGetMaxY(containerRect) - radius, radius, 0, (float)M_PI / 2, 0);
//
//    //bottom left corner
//    CGPathAddArc(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMaxY(containerRect) - radius, radius, (float)M_PI / 2, (float)M_PI, 0);
//
//    //top left corner, the ending point
//    CGPathAddArc(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect) + radius, radius, (float)M_PI, 3 * (float)M_PI / 2, 0);
//
//    //CGPathAddArc(tagPath.x, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect) + radius, radius, (float)M_PI, 3 * (float)M_PI / 2, 0);
//
//    //we are done
//    CGPathCloseSubpath(tagPath);
//
//    CGContextAddPath(context, tagPath);
//    //CGContextSetShadowWithColor(context, CGSizeMake(0, 2), 1.5, [[UIColor colorWithRed:0 green:0 blue:20/255.0 alpha:0.35] CGColor]);
//    CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:0.11 alpha:0.75] CGColor]);
//
//    if ([self.strTagType isEqualToString:@"service"]){
//        CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:1.0 alpha:0.0] CGColor]);
//        [self.tagTextField setTextColor:[UIColor clearColor]];
//    }else{
//        [self.tagTextField setTextColor:[UIColor whiteColor]];
//    }
//
//    CGContextFillPath(context);
//
//    //Draw stroke
//    CGContextAddPath(context, tagPath);
//
//    if ([self.strTagType isEqualToString:@"service"]){
//        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRed:230.0/255.0 green:55.0/255.0 blue:100.0/255.0 alpha:0.0] CGColor]);
//    }else{
//        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithWhite:0.85 alpha:1] CGColor]);
//    }
//
//    CGContextSetLineWidth(context, 1);
//    CGContextSetLineJoin(context, kCGLineJoinRound);
//    CGContextStrokePath(context);
//
//    //CGContextAddPath(context, tagPath);
//
//    //CGPathRelease(arrowPath);
//    CGPathRelease(tagPath);
//    CGColorSpaceRelease(colorSpace);
//
//    [self makeCustomViewForServiceTag];
//    [self resizeTextField];
//    /*
//    if ([self.strTagType isEqualToString:@"service"]){
//        //[self resizeTextField];
//        [self makeCustomViewForServiceTag];
//        [self resizeTextField];
//    }else{
//        if ([self.strTagType isEqualToString:@"people"]){
//            NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
//            if ([str isEqualToString:@"down"]){
//                self.tagTextField.transform = CGAffineTransformMakeRotation(M_PI);
//                self.transform = CGAffineTransformMakeRotation(M_PI);
//                [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//            }
//        }
//    }*/
//}
//
//- (void)repositionInRect:(CGRect)rect
//{
//    [self.layer setAnchorPoint:CGPointMake(0.5,0)];
//    CGPoint popoverPoint = CGPointMake(rect.origin.x, rect.origin.y);
//    popoverPoint.x += rect.size.width * (self.normalizedArrowPoint.x + self.normalizedArrowOffset.x);
//    popoverPoint.y += rect.size.height * (self.normalizedArrowPoint.y + self.normalizedArrowOffset.y);
//
//    [self setCenter:popoverPoint];
//
//    CGFloat rightX = self.frame.origin.x+self.frame.size.width;
//    CGFloat leftXClip = MAX(rect.origin.x - self.frame.origin.x, 0);
//    CGFloat rightXClip = MIN((rect.origin.x+rect.size.width)-rightX, 0);
//
//    CGRect newFrame = self.frame;
//    newFrame.origin.x += leftXClip;
//    newFrame.origin.x += rightXClip;
//
//    [self setFrame:newFrame];
//
//}
//
//#pragma mark - Event Hooks
//
//- (void)didRecognizeSingleTap:(UITapGestureRecognizer *)tapGesture
//{
//    if(self.isFirstResponder == NO){
//        [self.delegate tagPopover:self didReceiveSingleTap:tapGesture];
//    }
//}
//
//- (void)didReceiveCancelNotification:(NSNotification *)aNotification
//{
//    if(self.isFirstResponder){
//        [self setCanceled:YES];
//        [self resignFirstResponder];
//        [self removeFromSuperview];
//    }
//}
//
//#pragma mark - UITextField Delegate
//
//
//- (BOOL)textField:(UITextField *)textField
//    shouldChangeCharactersInRange:(NSRange)range
//                replacementString:(NSString *)string {
//    BOOL result = NO;
//
//    if(textField == self.tagTextField){
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if((!self.maximumTextLength) || (newLength <= self.maximumTextLength)){
//            result = YES;
//        }
//    }
//
//    return result;
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if(textField == self.tagTextField){
//        [textField setTextAlignment:NSTextAlignmentLeft];
//        [self resizeTextField];
//    }
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if(textField == self.tagTextField){
//        [textField setTextAlignment:NSTextAlignmentCenter];
//        [self resizeTextField];
//        if([self isCanceled] == NO){
//            [self.delegate tagPopoverDidEndEditing:self];
//        }
//        [self resignFirstResponder];
//    }
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    if(textField == self.tagTextField){
//        [self resignFirstResponder];
//    }
//    return YES;
//}
//
//- (BOOL)becomeFirstResponder
//{
//    [self.tagTextField setUserInteractionEnabled:YES];
//    if([self.tagTextField canBecomeFirstResponder]){
//        [self.tagTextField becomeFirstResponder];
//        [self resizeTextField];
//        return YES;
//    }
//
//    [self.tagTextField setUserInteractionEnabled:NO];
//    return NO;
//}
//
//- (BOOL)isFirstResponder
//{
//    return self.tagTextField.isFirstResponder;
//}
//
//- (BOOL)resignFirstResponder
//{
//    [self.tagTextField setUserInteractionEnabled:NO];
//    return self.tagTextField.resignFirstResponder;
//}
//
//# pragma mark -
//
//- (void)resizeTextField
//{
//    CGSize newTagSize = CGSizeZero;
//    if(self.tagTextField.text && ![self.tagTextField.text isEqualToString:@""]){
//        newTagSize = [self.tagTextField.text sizeWithAttributes:@{NSFontAttributeName: self.tagTextField.font}];
//    } else if (self.tagTextField.placeholder && ![self.tagTextField.placeholder isEqualToString:@""]){
//        newTagSize = [self.tagTextField.text sizeWithAttributes:@{NSFontAttributeName: self.tagTextField.font}];
//    }
//
//    if(self.tagTextField.isFirstResponder){
//        //This gives some extra room for the cursor.
//        newTagSize.width += 3;
//    }
//
//    CGRect newTextFieldFrame = self.tagTextField.frame;
//    CGSize minimumSize = self.tagTextField.isFirstResponder ? self.minimumTextFieldSizeWhileEditing :
//                                                              self.minimumTextFieldSize;
//
//    newTextFieldFrame.size.width = MAX(newTagSize.width, minimumSize.width);
//    newTextFieldFrame.size.height = MAX(newTagSize.height, minimumSize.height);
//    if ([self.strTagType isEqualToString:@"service"]){
//        newTextFieldFrame.size.width = 130;
//        newTextFieldFrame.size.height = 60;
//    }
//    [self.tagTextField setFrame:newTextFieldFrame];
//
//    CGSize tagInsets = CGSizeMake(-7, -6);
//    CGRect tagBounds = CGRectInset(self.tagTextField.bounds, tagInsets.width, tagInsets.height);
//    tagBounds.size.height += 10.0f;
//    tagBounds.origin.x = 0;
//    tagBounds.origin.y = 0;
//
//    CGPoint originalCenter = self.center;
//    [self setFrame:tagBounds];
//    [self setCenter:originalCenter];
//
//    if ([self.strTagType isEqualToString:@"people"]){
//        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
//       if ([str isEqualToString:@"down"] || self.normalizedArrowPoint.y>0.98){
////        if ([str isEqualToString:@"down"] || self.normalizedArrowPoint.y>0.87){
//            self.tagTextField.transform = CGAffineTransformMakeRotation(M_PI);
//            self.transform = CGAffineTransformMakeRotation(M_PI);
//            [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//        }
//    }
//
//    [self setNeedsDisplay];
//}
//
//-(void) makeCustomViewForServiceTag {
//
//    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(-35, 0, 135, 64)];
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height)];
//
//    if ([self.strTagType isEqualToString:@"service"]){
//        if ([self.strTopOrBottom isEqualToString:@"top"]){
//
//            if ([self.strLeftOrRight isEqualToString:@"left"]){
//                imgView.image = [UIImage imageNamed:@"upper_arrow_left"];
//                newView.frame = CGRectMake(-5, -newView.frame.origin.y-5, 135, 64);
//            }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//                imgView.image = [UIImage imageNamed:@"upper_arrow_right"];
//                newView.frame = CGRectMake(10, -newView.frame.origin.y-10, 135, 64);
//            }else{
//                imgView.image = [UIImage imageNamed:@"upper_arrow_center"];
//                newView.frame = CGRectMake(-7, -newView.frame.origin.y, 135, 64);
//            }
//
//        }else if ([self.strTopOrBottom isEqualToString:@"bottom"]){
//            if ([self.strLeftOrRight isEqualToString:@"left"]){
//                imgView.image = [UIImage imageNamed:@"down_arrow_left"];
//                newView.frame = CGRectMake(-0, -newView.frame.size.height-0, 135, 64);
//            }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//                imgView.image = [UIImage imageNamed:@"down_arrow_right"];
//                newView.frame = CGRectMake(10, -newView.frame.size.height-0, 135, 64);
//            }else{
//                imgView.image = [UIImage imageNamed:@"down_arrow_center"];
//                newView.frame = CGRectMake(-7, -newView.frame.size.height-0, 135, 64);
//            }
//        }
//    }
//    /*
//    if ([self.strTagType isEqualToString:@"service"]){
//       if ([self.strTopOrBottom isEqualToString:@"top"]){
//           if ([self.strLeftOrRight isEqualToString:@"left"]){
//               imgView.image = [UIImage imageNamed:@"upper_arrow_left"];
//               newView.frame = CGRectMake(-28, -newView.frame.origin.y, 135, 64);
//           }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//               imgView.image = [UIImage imageNamed:@"upper_arrow_right"];
//               newView.frame = CGRectMake(38, -newView.frame.origin.y, 135, 64);
//           }else{
//              imgView.image = [UIImage imageNamed:@"upper_arrow_center"];
//               newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
//           }
//
//       }else if ([self.strTopOrBottom isEqualToString:@"bottom"]){
//           if ([self.strLeftOrRight isEqualToString:@"left"]){
//               imgView.image = [UIImage imageNamed:@"down_arrow_left"];
//               newView.frame = CGRectMake(-28, -newView.frame.size.height+36, 135, 64);
//           }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//               imgView.image = [UIImage imageNamed:@"down_arrow_right"];
//               newView.frame = CGRectMake(38, -newView.frame.size.height+36, 135, 64);
//           }else{
//              imgView.image = [UIImage imageNamed:@"down_arrow_center"];
//               newView.frame = CGRectMake(0, -newView.frame.size.height+36, 135, 64);
//           }
//       }
//   }
//     */
//
//    imgView.layer.masksToBounds=YES;
//    imgView.contentMode = UIViewContentModeScaleToFill;
//
//    UIImageView *imgViewArrow = [[UIImageView alloc] initWithFrame:CGRectMake(imgView.frame.size.width-28, (imgView.frame.size.height/2)-10, 20, 20)];
//    imgViewArrow.image = [UIImage imageNamed:@"forWordGray_ico"];
//
//    UIImageView *imgViewLine = [[UIImageView alloc] initWithFrame:CGRectMake(imgView.frame.size.width-36, (imgView.frame.size.height/2)-10, 1, 28)];
//    [imgViewLine setCenter:imgViewArrow.center];
//    imgViewLine.frame = CGRectMake(imgView.frame.size.width-36, imgViewLine.frame.origin.y, 1, 28);
//    [imgViewLine setBackgroundColor:[UIColor lightGrayColor]];
//
//    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(10,11, imgView.frame.size.width-48, 25)];
//    lblName.text = self.dataSource.tagText;
//    [lblName setFont:[UIFont fontWithName:@"Nunito-Medium" size:13.0]];
//    [lblName setTextColor:[UIColor blackColor]];
//
//    UILabel *lblPrice = [[UILabel alloc]initWithFrame:CGRectMake(10,32, imgView.frame.size.width-48, 20)];
//    if ([self.strPrice length]>0){
//       lblPrice.text = [@"£" stringByAppendingString:self.strPrice];
//    }else{
//        lblPrice.text = @"£0.0";
//    }
//    [lblPrice setFont:[UIFont fontWithName:@"Nunito-Medium" size:13.0]];
//    [lblPrice setTextColor:[UIColor darkGrayColor]];
//    lblPrice.numberOfLines = 1;
//
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 13, newView.frame.size.width-10, newView.frame.size.height-26)];
//    btn.backgroundColor = [UIColor clearColor];
//    [btn addTarget:self
//                     action:@selector(touchButtonServiceTag)
//                     forControlEvents:UIControlEventTouchUpInside];
//    //[btn setCenter:newView.center];
//    [newView addSubview:imgView];
//    [newView addSubview:imgViewArrow];
//    [newView addSubview:imgViewLine];
//    [newView addSubview:lblName];
//    [newView addSubview:lblPrice];
//    [newView addSubview:btn];
//    if ([self.strTagType isEqualToString:@"service"]){
//       [self addSubview:newView];
//    }else{
//        if ([self.strTagType isEqualToString:@"people"]){
//            NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
//            if ([str isEqualToString:@"down"]){
//                self.tagTextField.transform = CGAffineTransformMakeRotation(M_PI);
//                self.transform = CGAffineTransformMakeRotation(M_PI);
//                [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//            }
//        }
//    }
//}
//
//-(void) touchButtonServiceTag {
//    NSLog(@"%@", self.dataSource.metaInfo);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"redirectionFromServiceTag" object:nil userInfo:self.dataSource.metaInfo];
//}
//
//@end
//
//


//
//  Created by Eddy Borja.
//  Copyright (c) 2014 Eddy Borja. All rights reserved.
/*
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "EBTagPopover.h"
#import "EBPhotoPagesNotifications.h"
#import <QuartzCore/QuartzCore.h>

@interface EBTagPopover ()
@property (weak) UIView *contentView;
@property (weak) UITextField *tagTextField;
@property (assign, getter = isCanceled) BOOL canceled;

@end

#pragma mark - EBTagPopover

@implementation EBTagPopover

- (id)init {
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithDelegate:(id<EBTagPopoverDelegate>)delegate {
    
    self = [super init];
    if (self) {
        NSAssert([(NSObject *)delegate conformsToProtocol:@protocol(EBTagPopoverDelegate)],
                 @"A tag popover's delegate must conform to the EBTagPopoverDelegate protocol.");
        [self initialize];
        [self setDelegate:delegate];
    }
    return self;
}

- (id)initWithTag:(id<EBPhotoTagProtocol>)aTag
{
    self = [super init];
    if(self){
        NSAssert([(NSObject *)aTag conformsToProtocol:@protocol(EBPhotoTagProtocol)],
                 @"A tag's data source must conform to EBPhotoTagProtocol.");
        //        if (self.tagPosition.y){
        //            if (self.tagPosition.y>1.0){
        //                [[NSUserDefaults standardUserDefaults]setValue:@"down" forKey:@"tagPosition"];
        //                [[NSUserDefaults standardUserDefaults] synchronize];
        //            }else{
        //                [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
        //                [[NSUserDefaults standardUserDefaults] synchronize];
        //            }
        //        }
        [self initialize];
        [self setDataSource:aTag];
        [self setText:self.dataSource.tagText];
    }
    return self;
}

- (void)initialize
{
    [self loadContentView];
    [self loadGestureRecognizers];
    
    CGSize tagInsets = CGSizeMake(-7, -6);
    CGRect tagBounds = CGRectInset(self.contentView.bounds, tagInsets.width, tagInsets.height);
    tagBounds.size.height += 10.0f;
    tagBounds.origin.x = 0;
    tagBounds.origin.y = 0;
    
    [self setFrame:tagBounds];
    
    //[self setMinimumTextFieldSize:CGSizeMake(25, 14)];
    //[self setMinimumTextFieldSizeWhileEditing:CGSizeMake(54, 14)];
    [self setMinimumTextFieldSize:CGSizeMake(25, 20)];
    [self setMinimumTextFieldSizeWhileEditing:CGSizeMake(54, 20)];
    [self setMaximumTextLength:40];
    
    [self setNormalizedArrowOffset:CGPointMake(0.0, 0.02)];
    
    [self setOpaque:NO];
    [self.contentView setFrame:CGRectOffset(self.contentView.frame,
                                            -(tagInsets.width),
                                            -(tagInsets.height)+10)];
   
    [self beginObservations];
}

- (void)dealloc {
    [self stopObservations];
}

#pragma mark -

- (void)loadContentView
{
    UIView *contentView = [self newContentView];
    [self addSubview:contentView];
    [self setContentView:contentView];
}

- (void)loadGestureRecognizers
{
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc]
                                                initWithTarget:self
                                                action:@selector(didRecognizeSingleTap:)];
    [singleTapGesture setNumberOfTapsRequired:1];
    
    [self addGestureRecognizer:singleTapGesture];
}

- (UIView *)newContentView
{
    NSString *placeholderText = NSLocalizedString(@"New Tag", @"Appears as placeholder text before a user enters text for a photo tag.");
    //UIFont *textFieldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
    //CGSize tagSize = [placeholderText sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:12]}];
    UIFont *textFieldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    CGSize tagSize = [placeholderText sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:14]}];
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, tagSize.width, tagSize.height)];
    [textField setFont:textFieldFont];
    [textField setBackgroundColor:[UIColor clearColor]];
    if ([self.strTagType isEqualToString:@"service"]){
        [textField setTextColor:[UIColor clearColor]];
    }else{
        [textField setTextColor:[UIColor whiteColor]];
    }
    [textField setPlaceholder:placeholderText];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setKeyboardAppearance:UIKeyboardAppearanceAlert];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setReturnKeyType:UIReturnKeyDone];
    [textField setEnablesReturnKeyAutomatically:YES];
    [textField setDelegate:self];
    [textField setUserInteractionEnabled:NO];
    
    [self setTagTextField:textField];
    return textField;
}

#pragma mark - Notifications

- (void)beginObservations
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tagTextFieldDidChangeWithNotification:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveCancelNotification:)
                                                 name:EBPhotoPagesControllerDidCancelTaggingNotification object:nil];
}


- (void)stopObservations
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (void)tagTextFieldDidChangeWithNotification:(NSNotification *)aNotification
{
    //resize, reposition
    if(aNotification.object == self.tagTextField){
        [self resizeTextField];
    }
}


- (NSString *)text
{
    return self.tagTextField.text;
}

- (void)setText:(NSString *)text
{
    [self.tagTextField setText:text];
    [self resizeTextField];
}

- (void)setDelegate:(id<EBTagPopoverDelegate>)aDelegate
{
    NSAssert([aDelegate conformsToProtocol:@protocol(EBTagPopoverDelegate)],
             @"EBTagPopover delegates must conform to EBTagPopoverDelegate protocol.");
    _delegate = aDelegate;
}


- (void)presentPopoverFromPoint:(CGPoint)point
                         inView:(UIView *)view
                       animated:(BOOL)animated
{
    [self presentPopoverFromPoint:point
                           inRect:view.frame
                           inView:view
         permittedArrowDirections:UIPopoverArrowDirectionUp
                         animated:animated];
}



- (void)presentPopoverFromPoint:(CGPoint)point
                         inView:(UIView *)view
       permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                       animated:(BOOL)animated
{
    [self presentPopoverFromPoint:point
                           inRect:view.frame
                           inView:view
         permittedArrowDirections:arrowDirections
                         animated:animated];
}

- (void)presentPopoverFromPoint:(CGPoint)point
                         inRect:(CGRect)rect
                         inView:(UIView *)view
       permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                       animated:(BOOL)animated;
{
    //[self setCenter:point];
    
    [view addSubview:self];
    
    CGPoint difference = CGPointMake(0,//(newCenter.x - point.x)/self.frame.size.width,
                                     0.5);
    
    [self.layer setAnchorPoint:CGPointMake(0.5-difference.x,0.5-difference.y)];
    
    [self setCenter:point];
    
    CGFloat tagMaximumX = CGRectGetMaxX(self.frame);
    CGFloat tagMinimumX = CGRectGetMinX(self.frame);
    CGFloat tagMaximumY = CGRectGetMaxY(self.frame);
    CGFloat tagMinimumY = CGRectGetMinY(self.frame);
    
    CGRect tagBoundary = CGRectInset(view.frame, 5, 5);
    CGFloat boundsMinimumX = CGRectGetMinX(tagBoundary);
    CGFloat boundsMaximumX = CGRectGetMaxX(tagBoundary);
    CGFloat boundsMinimumY = CGRectGetMinY(tagBoundary);
    CGFloat boundsMaximumY = CGRectGetMaxY(tagBoundary);
    
    CGFloat xOffset = ((MIN(0, tagMinimumX - boundsMinimumX) + MAX(0, tagMaximumX - boundsMaximumX))/1.0);
    CGFloat yOffset = ((MIN(0, tagMinimumY - boundsMinimumY) + MAX(0, tagMaximumY - boundsMaximumY))/1.0);
    
    
    CGPoint newCenter = CGPointMake(point.x - xOffset, point.y - yOffset);
    
    [self setCenter:newCenter];
    
    
    /*
     CGRect newFrame = self.frame;
     newFrame.origin.x = point.x;
     newFrame.origin.y = point.y;
     //[self setFrame:newFrame];
     
     [self setTransform:CGAffineTransformMakeScale(0.3, 0.3)];
     [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
     [self setTransform:CGAffineTransformMakeScale(1,1)];
     }completion:nil];
     */
    
}


- (void)drawRect:(CGRect)fullRect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    float radius = 4.0f;
    float arrowHeight =  10.0f; //this is how far the arrow extends from the rect
    float arrowWidth = 16.0;
    
    if ([self.strTagType isEqualToString:@"service"]){
    }
    
    fullRect = CGRectInset(fullRect, 1, 1);
    
    CGRect containerRect = CGRectMake(fullRect.origin.x,
                                      fullRect.origin.y+arrowHeight,
                                      fullRect.size.width,
                                      fullRect.size.height-arrowHeight);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGMutablePathRef tagPath = CGPathCreateMutable();
    
    //the starting point, top left corner
    CGPathMoveToPoint(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect));
    
    /*
     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(arrowWidth*0.5), CGRectGetMinY(containerRect));
     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(fullRect));
     CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+(arrowWidth*0.5), CGRectGetMinY(containerRect));
     //CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(containerRect));
     */
    
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
    if (([self.strLeftOrRight isEqualToString:@"left"]) || (self.normalizedArrowPoint.x <= 0.1)){
        if (([str isEqualToString:@"down"]) || (self.normalizedArrowPoint.y >= 0.98)){
            //draw the arrow
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth), CGRectGetMinY(containerRect));//+20
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth*0.5), CGRectGetMinY(fullRect));//+10
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));//0
            
        }else{
            //draw the arrow
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(self.tagTextField.frame.size.width*0.5)+(arrowWidth*0.5), CGRectGetMinY(fullRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5+(arrowWidth), CGRectGetMinY(containerRect));
        }
    }else if (([self.strLeftOrRight isEqualToString:@"right"]) || (self.normalizedArrowPoint.x >= 0.9)){
        if (([str isEqualToString:@"down"]) || (self.normalizedArrowPoint.y >= 0.98)){
            //draw the arrow
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(self.tagTextField.frame.size.width*0.5)+(arrowWidth*0.5), CGRectGetMinY(fullRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-self.tagTextField.frame.size.width*0.5+(arrowWidth), CGRectGetMinY(containerRect));
        }else{
            //draw the arrow
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth), CGRectGetMinY(containerRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5-(arrowWidth*0.5), CGRectGetMinY(fullRect));
            CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+self.tagTextField.frame.size.width*0.5, CGRectGetMinY(containerRect));
        }
    }else{
        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)-(arrowWidth*0.5), CGRectGetMinY(containerRect));
        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(fullRect));
        CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect)+(arrowWidth*0.5), CGRectGetMinY(containerRect));
        //CGPathAddLineToPoint(tagPath, NULL, CGRectGetMidX(containerRect), CGRectGetMinY(containerRect));
    }
    
    //top right corner
    CGPathAddArc(tagPath, NULL, CGRectGetMaxX(containerRect) - radius, CGRectGetMinY(containerRect) +radius, radius, 3 * (float)M_PI / 2, 0, 0);
    
    //bottom right corner
    CGPathAddArc(tagPath, NULL, CGRectGetMaxX(containerRect) - radius, CGRectGetMaxY(containerRect) - radius, radius, 0, (float)M_PI / 2, 0);
    
    //bottom left corner
    CGPathAddArc(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMaxY(containerRect) - radius, radius, (float)M_PI / 2, (float)M_PI, 0);
    
    //top left corner, the ending point
    CGPathAddArc(tagPath, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect) + radius, radius, (float)M_PI, 3 * (float)M_PI / 2, 0);
    
    //CGPathAddArc(tagPath.x, NULL, CGRectGetMinX(containerRect) + radius, CGRectGetMinY(containerRect) + radius, radius, (float)M_PI, 3 * (float)M_PI / 2, 0);
    
    //we are done
    CGPathCloseSubpath(tagPath);
    
    CGContextAddPath(context, tagPath);
    //CGContextSetShadowWithColor(context, CGSizeMake(0, 2), 1.5, [[UIColor colorWithRed:0 green:0 blue:20/255.0 alpha:0.35] CGColor]);
    CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:0.11 alpha:0.75] CGColor]);
    
    if ([self.strTagType isEqualToString:@"service"]){
        CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:1.0 alpha:0.0] CGColor]);
        [self.tagTextField setTextColor:[UIColor clearColor]];
    }else{
        [self.tagTextField setTextColor:[UIColor whiteColor]];
    }
    
    CGContextFillPath(context);
    
    //Draw stroke
    CGContextAddPath(context, tagPath);
    
    if ([self.strTagType isEqualToString:@"service"]){
        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithRed:230.0/255.0 green:55.0/255.0 blue:100.0/255.0 alpha:0.0] CGColor]);
    }else{
        CGContextSetStrokeColorWithColor(context, [[UIColor colorWithWhite:0.85 alpha:1] CGColor]);
    }
    
    CGContextSetLineWidth(context, 1);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextStrokePath(context);
    
    //CGContextAddPath(context, tagPath);
    
    //CGPathRelease(arrowPath);
    CGPathRelease(tagPath);
    CGColorSpaceRelease(colorSpace);
    
    [self makeCustomViewForServiceTag];
    [self resizeTextField];
    
}

- (void)repositionInRect:(CGRect)rect
{
        [self.layer setAnchorPoint:CGPointMake(0.5,0)];
        CGPoint popoverPoint = CGPointMake(rect.origin.x, rect.origin.y);
        popoverPoint.x += rect.size.width * (self.normalizedArrowPoint.x + self.normalizedArrowOffset.x);
        popoverPoint.y += rect.size.height * (self.normalizedArrowPoint.y + self.normalizedArrowOffset.y);
    
    /*////
    if (self.normalizedArrowPoint.y > 0.93){
//        NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:@"tagPosition"];
//        self.strTopOrBottom = str;
//        if ([self.strTopOrBottom isEqualToString:@"bottom"]){
            popoverPoint.y = popoverPoint.y+35;
        //}
    }else{
      popoverPoint.y = popoverPoint.y-10;
    }
    
    if (self.normalizedArrowPoint.x < 0.15){
        popoverPoint.x = popoverPoint.x + 20;
    }else if (self.normalizedArrowPoint.x > 0.85){
        popoverPoint.x = popoverPoint.x - 20;
    }
    ////*/
    
    ////
    if (self.normalizedArrowPoint.y > 0.93){
        popoverPoint.y = popoverPoint.y+35;
    }else if (self.normalizedArrowPoint.y < 0.03){
        popoverPoint.y = popoverPoint.y;
    }else{
        popoverPoint.y = popoverPoint.y-10;
    }
    if (![self.strTagType isEqualToString:@"service"]){
        if (self.normalizedArrowPoint.x < 0.15){
            popoverPoint.x = popoverPoint.x + 20;
        }else if (self.normalizedArrowPoint.x > 0.85){
            popoverPoint.x = popoverPoint.x - 20;
        }
    }
    ////
    
        [self setCenter:popoverPoint];

        CGFloat rightX = self.frame.origin.x+self.frame.size.width;
        CGFloat leftXClip = MAX(rect.origin.x - self.frame.origin.x, 0);
        CGFloat rightXClip = MIN((rect.origin.x+rect.size.width)-rightX, 0);

        CGRect newFrame = self.frame;
        newFrame.origin.x += leftXClip;
        newFrame.origin.x += rightXClip;

        [self setFrame:newFrame];
    
   /*
    [self.layer setAnchorPoint:CGPointMake(0.5,0)];
    CGPoint popoverPoint = CGPointMake(rect.origin.x, rect.origin.y);
    popoverPoint.x += rect.size.width * (self.normalizedArrowPoint.x + self.normalizedArrowOffset.x);
    popoverPoint.y += rect.size.height * (self.normalizedArrowPoint.y + self.normalizedArrowOffset.y);
    if ([self.strTagType isEqualToString:@"service"]){
        
        popoverPoint.y = popoverPoint.y;
        NSLog(@"y position :: %f", self.normalizedArrowPoint.y);
        if (self.normalizedArrowPoint.y > 0.90){
            if ([self.strTopOrBottom isEqualToString:@"bottom"]){
                popoverPoint.y = popoverPoint.y-35;
            }else{
            }
        }else{
//            if (([self.fromAddTag isEqualToString:@"YES"]) && ([self.strTopOrBottom isEqualToString:@"top"])){
//            }else{
                if (self.normalizedArrowPoint.x > 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
                    popoverPoint.x = popoverPoint.x+8;
                    popoverPoint.y = popoverPoint.y-3;
                    if (self.normalizedArrowPoint.x > 0.9) {
                        ////Himanshu
                        popoverPoint.x = popoverPoint.x-55;
                        popoverPoint.y = popoverPoint.y+8;
                    }
                }else if  (self.normalizedArrowPoint.x < 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
                    // popoverPoint.x = popoverPoint.x-150;
                    
                    popoverPoint.y = popoverPoint.y-5;
                }
            //}
        }
        
    }else{
        
        popoverPoint.y = popoverPoint.y;
        NSLog(@"strTopOrBottom :: %f", self.normalizedArrowPoint.y);
        if (self.normalizedArrowPoint.y > 0.90){
            if ([self.strTopOrBottom isEqualToString:@"top"]){
                popoverPoint.y = popoverPoint.y-50;
            }else{
            }
        }else{
//            if (([self.fromAddTag isEqualToString:@"YES"]) && ([self.strTopOrBottom isEqualToString:@"top"])){
//            }else{
                if (self.normalizedArrowPoint.x > 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
                    popoverPoint.x = popoverPoint.x+8;
                    popoverPoint.y = popoverPoint.y-3;
                }else if  (self.normalizedArrowPoint.x < 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
                    popoverPoint.x = popoverPoint.x-5;
                    popoverPoint.y = popoverPoint.y-3;
                }
            //}
        }
        
    }
    [self setCenter:popoverPoint];
    
    CGFloat rightX = self.frame.origin.x+self.frame.size.width;
    CGFloat leftXClip = MAX(rect.origin.x - self.frame.origin.x, 0);
    CGFloat rightXClip = MIN((rect.origin.x+rect.size.width)-rightX, 0);
    
    CGRect newFrame = self.frame;
    newFrame.origin.x += leftXClip;
    if ([self.strTagType isEqualToString:@"service"]){
//        if (([self.fromAddTag isEqualToString:@"YES"]) && ([self.strTopOrBottom isEqualToString:@"top"])){
//            if  (self.normalizedArrowPoint.x < 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
//                if  (self.normalizedArrowPoint.x < 0.4) {
//                    newFrame.origin.x  =  newFrame.origin.x-8;
//                }}
//        }else{
            if  (self.normalizedArrowPoint.x < 0.5) {//&& ([self.strTopOrBottom isEqualToString:@"top"])){
                //   newFrame.origin.x  =  newFrame.origin.x+3;
                if  (self.normalizedArrowPoint.x < 0.4) {
                    newFrame.origin.x  =  newFrame.origin.x-24;
                }else{
                    newFrame.origin.x  =  newFrame.origin.x+3;
                }
            }
        //}
        
    }else{
        
        NSLog(@"strTopOrBottom :: %@", self.strTopOrBottom);
        NSLog(@"strLeftOrRight :: %@", self.strLeftOrRight);
        
        if ([self.strTopOrBottom isEqualToString:@"top"]){
            if ([self.strLeftOrRight isEqualToString:@"left"]){
                newFrame.origin.x += rightXClip-10;
            }else if ([self.strLeftOrRight isEqualToString:@"right"]){
                ////Himanshu
                //newFrame.origin.x += rightXClip+50;
                if ([self.strIsForAddTag isEqualToString:@"add"]){
                    //newFrame.origin.x += rightXClip-48;
                }else{
                    newFrame.origin.x += rightXClip+24;
                }
            }else{
            }
        }else{
            //Himanshu
            if (self.normalizedArrowPoint.x > 0.9){
                newFrame.origin.x += rightXClip+10;
            }
        }
        //  newFrame.origin.x += rightXClip-10;
    }
    
    [self setFrame:newFrame];
    */
}

#pragma mark - Event Hooks

- (void)didRecognizeSingleTap:(UITapGestureRecognizer *)tapGesture
{
    if(self.isFirstResponder == NO){
        [self.delegate tagPopover:self didReceiveSingleTap:tapGesture];
    }
}

- (void)didReceiveCancelNotification:(NSNotification *)aNotification
{
    if(self.isFirstResponder){
        [self setCanceled:YES];
        [self resignFirstResponder];
        [self removeFromSuperview];
    }
}

#pragma mark - UITextField Delegate


- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    BOOL result = NO;
    
    if(textField == self.tagTextField){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if((!self.maximumTextLength) || (newLength <= self.maximumTextLength)){
            result = YES;
        }
    }
    return result;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.tagTextField){
        [textField setTextAlignment:NSTextAlignmentLeft];
        [self resizeTextField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.tagTextField){
        [textField setTextAlignment:NSTextAlignmentCenter];
        [self resizeTextField];
        if([self isCanceled] == NO){
            [self.delegate tagPopoverDidEndEditing:self];
        }
        [self resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.tagTextField){
        [self resignFirstResponder];
    }
    return YES;
}

- (BOOL)becomeFirstResponder
{
    [self.tagTextField setUserInteractionEnabled:YES];
    if([self.tagTextField canBecomeFirstResponder]){
        [self.tagTextField becomeFirstResponder];
        [self resizeTextField];
        return YES;
    }
    
    [self.tagTextField setUserInteractionEnabled:NO];
    return NO;
}

- (BOOL)isFirstResponder
{
    return self.tagTextField.isFirstResponder;
}

- (BOOL)resignFirstResponder
{
    [self.tagTextField setUserInteractionEnabled:NO];
    return self.tagTextField.resignFirstResponder;
}

# pragma mark -

- (void)resizeTextField
{
    CGSize newTagSize = CGSizeZero;
    if(self.tagTextField.text && ![self.tagTextField.text isEqualToString:@""]){
        newTagSize = [self.tagTextField.text sizeWithAttributes:@{NSFontAttributeName: self.tagTextField.font}];
    } else if (self.tagTextField.placeholder && ![self.tagTextField.placeholder isEqualToString:@""]){
        newTagSize = [self.tagTextField.text sizeWithAttributes:@{NSFontAttributeName: self.tagTextField.font}];
    }
    
    if(self.tagTextField.isFirstResponder){
        //This gives some extra room for the cursor.
        newTagSize.width += 3;
    }
    
    CGRect newTextFieldFrame = self.tagTextField.frame;
    CGSize minimumSize = self.tagTextField.isFirstResponder ? self.minimumTextFieldSizeWhileEditing :
    self.minimumTextFieldSize;
    
    newTextFieldFrame.size.width = MAX(newTagSize.width, minimumSize.width);
    newTextFieldFrame.size.height = MAX(newTagSize.height, minimumSize.height);
    if ([self.strTagType isEqualToString:@"service"]){
        newTextFieldFrame.size.width = 130;
        newTextFieldFrame.size.height = 60;
    }
    [self.tagTextField setFrame:newTextFieldFrame];
    
    CGSize tagInsets = CGSizeMake(-7, -6);
    CGRect tagBounds = CGRectInset(self.tagTextField.bounds, tagInsets.width, tagInsets.height);
    tagBounds.size.height += 10.0f;
    tagBounds.origin.x = 0;
    tagBounds.origin.y = 0;
    
    CGPoint originalCenter = self.center;
    [self setFrame:tagBounds];
    [self setCenter:originalCenter];
    
    if ([self.strTagType isEqualToString:@"people"]){
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
        if ([str isEqualToString:@"down"] || self.normalizedArrowPoint.y>0.98){
            self.tagTextField.transform = CGAffineTransformMakeRotation(M_PI);
            self.transform = CGAffineTransformMakeRotation(M_PI);
            [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    [self setNeedsDisplay];
}

-(void) makeCustomViewForServiceTag {
    
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(-35, 0, 135, 64)];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height)];
    NSLog(@"%@", self.dataSource.tagText);
    
//
//    if ([self.fromAddTag  isEqualToString:@"YES"]){
//
//        if ([self.strTagType isEqualToString:@"service"]){
//            if ([self.strTopOrBottom isEqualToString:@"top"]){
//                if ([self.strLeftOrRight isEqualToString:@"left"]){
//                    imgView.image = [UIImage imageNamed:@"upper_arrow_left"];
//                    newView.frame = CGRectMake(-5, -newView.frame.origin.y, 135, 64);
//                }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//                    imgView.image = [UIImage imageNamed:@"upper_arrow_right"];
//                    newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
//                }else{
//                    imgView.image = [UIImage imageNamed:@"upper_arrow_center"];
//                    newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
//                }
//
//            }else if ([self.strTopOrBottom isEqualToString:@"bottom"]){
//                if ([self.strLeftOrRight isEqualToString:@"left"]){
//                    imgView.image = [UIImage imageNamed:@"down_arrow_left"];
//                    newView.frame = CGRectMake(-28, -newView.frame.size.height-36, 135, 64);
//                }else if ([self.strLeftOrRight isEqualToString:@"right"]){
//                    imgView.image = [UIImage imageNamed:@"down_arrow_right"];
//                    newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
//                }else{
//                    imgView.image = [UIImage imageNamed:@"down_arrow_center"];
//                    newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
//                }
//            }
//        }
//    }else{
    
    if ([self.strTagType isEqualToString:@"service"]){
        if ([self.strTopOrBottom isEqualToString:@"top"] && self.normalizedArrowPoint.y < 0.85){
            if ([self.strLeftOrRight isEqualToString:@"left"] || self.normalizedArrowPoint.x < 0.15){
                imgView.image = [UIImage imageNamed:@"upper_arrow_left"];
                newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
            }else if ([self.strLeftOrRight isEqualToString:@"right"] || self.normalizedArrowPoint.x > 0.85){
                imgView.image = [UIImage imageNamed:@"upper_arrow_right"];
                newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
            }else{
                imgView.image = [UIImage imageNamed:@"upper_arrow_center"];
                newView.frame = CGRectMake(0, -newView.frame.origin.y, 135, 64);
            }
            
        }else if ([self.strTopOrBottom isEqualToString:@"bottom"] || self.normalizedArrowPoint.y > 0.85){
            if ([self.strLeftOrRight isEqualToString:@"left"] || self.normalizedArrowPoint.x < 0.15){
                imgView.image = [UIImage imageNamed:@"down_arrow_left"];
                //newView.frame = CGRectMake(-28, -newView.frame.size.height-36, 135, 64);
                if (self.normalizedArrowPoint.y > 0.95){
                    newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
                }else if (self.normalizedArrowPoint.y > 0.85){
                    newView.frame = CGRectMake(0, -newView.frame.size.height, 135, 64);
                }else {
                    newView.frame = CGRectMake(0, 0, 135, 64);
                }
                if (self.normalizedArrowPoint.x < 0.1){
                    newView.frame = CGRectMake(0, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }else if (self.normalizedArrowPoint.x < 0.15){
                    newView.frame = CGRectMake(15, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }else if (self.normalizedArrowPoint.x < 0.20){
                    newView.frame = CGRectMake(25, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }else{
                    newView.frame = CGRectMake(newView.frame.origin.x, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }
            }else if ([self.strLeftOrRight isEqualToString:@"right"] || self.normalizedArrowPoint.x > 0.85){
                imgView.image = [UIImage imageNamed:@"down_arrow_right"];
                //newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
                if (self.normalizedArrowPoint.y > 0.95){
                    newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
                }else if (self.normalizedArrowPoint.y > 0.85){
                    newView.frame = CGRectMake(0, -newView.frame.size.height, 135, 64);
                }else {
                    newView.frame = CGRectMake(0, 0, 135, 64);
                }
            }else{
                imgView.image = [UIImage imageNamed:@"down_arrow_center"];
                //newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
                if (self.normalizedArrowPoint.y > 0.95){
                    newView.frame = CGRectMake(0, -newView.frame.size.height-36, 135, 64);
                }else if (self.normalizedArrowPoint.y > 0.85){
                    newView.frame = CGRectMake(0, -newView.frame.size.height, 135, 64);
                }else{
                    newView.frame = CGRectMake(0, 0, 135, 64);
                }
            }
        }
        if (self.normalizedArrowPoint.x < 0.03){
            newView.frame = CGRectMake(0, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
        }else if (self.normalizedArrowPoint.x < 0.05){
            newView.frame = CGRectMake(7, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
        }else if (self.normalizedArrowPoint.x < 0.099){
            newView.frame = CGRectMake(13, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
        }else if (self.normalizedArrowPoint.x < 0.15){
            newView.frame = CGRectMake(28, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
        }else{
            if (self.normalizedArrowPoint.x > 0.95){
                newView.frame = CGRectMake(newView.frame.origin.x+10, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
            }else if (self.normalizedArrowPoint.x > 0.85){
                if (self.normalizedArrowPoint.x > 0.85 && self.normalizedArrowPoint.x < 0.91){
                    newView.frame = CGRectMake(newView.frame.origin.x-(self.normalizedArrowPoint.x*22), newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }else{
                    newView.frame = CGRectMake(newView.frame.origin.x-(self.normalizedArrowPoint.x*10), newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
                }
            }else if (self.normalizedArrowPoint.x > 0.7 && self.normalizedArrowPoint.x < 0.95){
                newView.frame = CGRectMake(newView.frame.origin.x+(self.normalizedArrowPoint.x*20), newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
            }else{
                newView.frame = CGRectMake(newView.frame.origin.x+5, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
            }
        }
    }
    //}
    
    imgView.layer.masksToBounds=YES;
    imgView.contentMode = UIViewContentModeScaleToFill;
    
    UIImageView *imgViewArrow = [[UIImageView alloc] initWithFrame:CGRectMake(imgView.frame.size.width-28, (imgView.frame.size.height/2)-10, 20, 20)];
    imgViewArrow.image = [UIImage imageNamed:@"forWordGray_ico"];
    
    UIImageView *imgViewLine = [[UIImageView alloc] initWithFrame:CGRectMake(imgView.frame.size.width-36, (imgView.frame.size.height/2)-10, 1, 28)];
    [imgViewLine setCenter:imgViewArrow.center];
    imgViewLine.frame = CGRectMake(imgView.frame.size.width-36, imgViewLine.frame.origin.y, 1, 28);
    [imgViewLine setBackgroundColor:[UIColor lightGrayColor]];
    
    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(10,11, imgView.frame.size.width-48, 25)];
    lblName.text = self.dataSource.tagText;
    [lblName setFont:[UIFont fontWithName:@"Nunito-Regular" size:15.0]];
    [lblName setTextColor:[UIColor blackColor]];
    
    UILabel *lblPrice = [[UILabel alloc]initWithFrame:CGRectMake(10,32, imgView.frame.size.width-48, 20)];
    if ([self.strPrice length]>0){
        lblPrice.text = [@"£" stringByAppendingString:self.strPrice];
    }else{
        lblPrice.text = @"£0.0";
    }
    [lblPrice setFont:[UIFont fontWithName:@"Nunito-Regular" size:15.0]];
    [lblPrice setTextColor:[UIColor darkGrayColor]];
    lblPrice.numberOfLines = 1;
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 13, newView.frame.size.width-10, newView.frame.size.height-26)];
    //UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, newView.frame.size.width-10, newView.frame.size.height)];
    btn.backgroundColor = [UIColor clearColor];
    [btn addTarget:self
            action:@selector(touchButtonServiceTag)
  forControlEvents:UIControlEventTouchUpInside];
    //[btn setCenter:newView.center];
    [newView addSubview:imgView];
    [newView addSubview:imgViewArrow];
    [newView addSubview:imgViewLine];
    [newView addSubview:lblName];
    [newView addSubview:lblPrice];
    [newView addSubview:btn];
    if ([self.strTagType isEqualToString:@"service"]){
            [self addSubview:newView];
    }else{
        if ([self.strTagType isEqualToString:@"people"]){
            NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"tagPosition"];
            //if ([str isEqualToString:@"down"]){
            if (([str isEqualToString:@"down"]) || (self.normalizedArrowPoint.y > 0.93)){
                self.tagTextField.transform = CGAffineTransformMakeRotation(M_PI);
                self.transform = CGAffineTransformMakeRotation(M_PI);
                [[NSUserDefaults standardUserDefaults]setValue:@"top" forKey:@"tagPosition"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
}

-(void) touchButtonServiceTag {
    NSLog(@"%@", self.dataSource.metaInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"redirectionFromServiceTag" object:nil userInfo:self.dataSource.metaInfo];
}

@end
