//
//  CustomExtension.swift
//  Mualab
//
//  Created by MINDIII on 11/1/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class CustomExtension: NSObject {

}
extension UIImageView
{
    
    func setImageProperty() {
        layer.backgroundColor = UIColor.clear.cgColor
        layer.borderWidth = 1.5
        //self.layer.borderColor=[commenColorRed CGColor];
    }
    
    func setImageFream() {
        layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
    }
    
}
extension String
{
    public func getAcronyms(separator: String = "") -> String
    {
        let acronyms = self.components(separatedBy: " ").map({ String($0.first ?? " ")}).joined(separator: separator);
        return acronyms;
    }
}

extension UIView
{
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func setViewShadow(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height + 1))
        //   self.backgroundColor     = [UIColor whiteColor];
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
    
    func setShadow3D(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 2, y: 2, width: frame.size.width - 2, height: frame.size.height - 2))
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

//Protocal that copyable class should conform
protocol Copying {
    init(original: Self)
}
//Concrete class extension
extension Copying {
    func copy() -> Self {
        return Self.init(original: self)
    }
}
//Array extension for elements conforms the Copying protocol
extension Array where Element: Copying {
    func clone() -> Array {
        var copiedArray = Array<Element>()
        for element in self {
            copiedArray.append(element.copy())
        }
        return copiedArray
    }
}
