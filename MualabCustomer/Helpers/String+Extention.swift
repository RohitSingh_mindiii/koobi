//
//  String+Extention.swift
//  MualabCustomer
//
//  Created by Mac on 28/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation


extension String {
    
    func highlightWordsIn(highlightedWords: String, attributes: [[NSAttributedString.Key: Any]]) -> NSMutableAttributedString {
        
        let result = NSMutableAttributedString(string: self)
        
        let strSentence = self.lowercased()
        let strWord = highlightedWords.lowercased()
        
        if strSentence.contains(strWord) {
            
            let range =  (strSentence as NSString).range(of: strWord)
            for attribute in attributes {
                result.addAttributes(attribute, range: range)
            }
        }
        
        return result
    }
}
