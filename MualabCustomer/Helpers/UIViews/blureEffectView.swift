//
//  blureEffectView.swift
//  Mualab
//
//  Created by Mac on 27/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class blureEffectView: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let blurEffect = UIBlurEffect(style:.light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.bounds
        self.addSubview(blurredEffectView)
    }
}
