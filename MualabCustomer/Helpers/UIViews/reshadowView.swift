//
//  shadowView.swift
//  Mualab
//
//  Created by Mac on 02/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class reshadowView: UIView {

    
    override func draw(_ rect: CGRect) {
        // Drawing code
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height + 1))
        //   self.backgroundColor     = [UIColor whiteColor];
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.6
        layer.shadowPath = shadowPath.cgPath
    }
 }
