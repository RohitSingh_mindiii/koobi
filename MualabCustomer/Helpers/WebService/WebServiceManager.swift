//
//  WebServiceClass.swift
//  Link
//
//  Created by MINDIII on 10/3/17.
//  Copyright © 2017 MINDIII. All rights reserved.

// MARK: - Required things
//  install Alamofire with cocoapods * pod 'Alamofire'
//  install SwiftyJSON with cocoapods * pod 'SwiftyJSON'


import UIKit
import Alamofire
import SVProgressHUD

var strAuthToken : String = ""
let objServiceManager = WebServiceManager.sharedObject()
let objActivity = activityIndicator()

class WebServiceManager: NSObject {
    
    //MARK: - Shared object
    fileprivate var window = UIApplication.shared.keyWindow
    
    private static var sharedNetworkManager: WebServiceManager = {
        let networkManager = WebServiceManager()
        return networkManager
    }()
    // MARK: - Accessors
    class func sharedObject() -> WebServiceManager {
        return sharedNetworkManager
    }
    
    private let manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        //configuration.timeoutIntervalForRequest = 10
        //configuration.timeoutIntervalForResource = 20
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    func showAlert(message: String = "", title: String , controller: UIWindow) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let subView = alertController.view.subviews.first!
            let alertContentView = subView.subviews.first!
            alertContentView.backgroundColor = UIColor.gray
            alertContentView.layer.cornerRadius = 20
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
    
    public func requestPost(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
        //let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        print("headers = \(headers)")
        print("\nstrURL = \(url)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.retrier = OAuth2Handler()
    
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
           
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                   
                    // Session Expire
                    let dict = dictionary as! Dictionary<String, Any>
                    if dict["status"] as? String ?? "" != "success"{
                       
                        if let msg = dict["message"] as? String{
                            
                            if msg == message.invalidToken{
                                objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                return
                            }
                        }
                        
                    }
                    print("\n response = \(dictionary)")
                    success(dictionary as! Dictionary<String, Any>)
                    //self.StopIndicator()
                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
                self.StopIndicator()
            }
        }
    }
    
    public func requestGet(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl + strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
        //let headers = ["authtoken" : strAuthToken]
        //  "Content-Type":"Application/json"]
        
        manager.retrier = OAuth2Handler()
        print("headers = \(headers)")
        print("\nstrURL = \(url)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            //self.StopIndicator()
            
            if responseObject.result.isSuccess {
                //                let resJson = JSON(responseObject.result.value!)
                //                success(resJson)
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    // Session Expire
                    let dict = dictionary as! Dictionary<String, Any>
                    if dict["status"] as? String ?? "" != "success"{
                        
                        if let msg = dict["message"] as? String{
                            if msg == message.invalidToken{
                                self.StopIndicator()
                                objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                return
                            }
                        }
                    }
                    print("\n response = \(dictionary)")
                    success(dictionary as! Dictionary<String, Any>)
                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    public func uploadMultipartData(strURL:String, params : [String : AnyObject]?, imageData:Data?, fileName:String, key:String, mimeType:String, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
       
        if !NetworkReachabilityManager()!.isReachable{
            self.StopIndicator()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            self.showAlert(message: "", title: message.noNetwork , controller: window!)
            
            return
        }
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
         print("headers = \(headers)")
        //let headers = ["authtoken" : strAuthToken]
        //  "Content-Type":"Application/json"]
        manager.retrier = OAuth2Handler()
        
        manager.upload(multipartFormData:{ multipartFormData in
            if let data = imageData{
                multipartFormData.append(data,
                                         withName:key,
                                         fileName:fileName,
                                         mimeType:mimeType)
            }
            for (key, value) in params! {
                let strValue = "\(value)"
                multipartFormData.append(strValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }},
                       usingThreshold:UInt64.init(),
                       to:url,
                       method:.post,
                       headers:headers,
                       encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { responseObject in
                                self.StopIndicator()
                                if responseObject.result.isSuccess {
                                    //                                        let resJson = JSON(responseObject.result.value!)
                                    //                                        success(resJson)
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        // Session Expire
                                        let dict = dictionary as! Dictionary<String, Any>
                                        if dict["status"] as? String ?? "" != "success"{
                                            
                                            if let msg = dict["message"] as? String{
                                                if msg == message.invalidToken{
                                                    objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                                    return
                                                }
                                            }
                                           
                                        }
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                    }
                                }
                                if responseObject.result.isFailure {
                                    let error : Error = responseObject.result.error!
                                    failure(error)
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                            self.StopIndicator()
                            failure(encodingError)
                        }
        })
    }
    
    public func requestPostMultipartData(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        if !NetworkReachabilityManager()!.isReachable{
            self.StopIndicator()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            self.showAlert(message: "", title: message.noNetwork ,controller: window!)
            return
        }
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
         print("headers = \(headers)")
       // let headers = ["authtoken" : strAuthToken,
       //                "Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        
        manager.upload(multipartFormData:{ multipartFormData in
            
            for (key, value) in params {
                let strValue = "\(value)"
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }},
                       // usingThreshold:UInt64.init(),
            to:url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { responseObject in
                        //self.StopIndicator()
                        if responseObject.result.isSuccess {
                            //                                        let resJson = JSON(responseObject.result.value!)
                            //                                        success(resJson)
                            do {
                                let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                // Session Expire
                                let dict = dictionary as! Dictionary<String, Any>
                                if dict["status"] as? String ?? "" != "success"{
                                    
                                    if let msg = dict["message"] as? String{
                                        if msg == message.invalidToken{
                                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                            return
                                        }
                                    }
                                }
                                success(dictionary as! Dictionary<String, Any>)
                            }catch{
                            }
                        }
                        if responseObject.result.isFailure {
                            let error : Error = responseObject.result.error!
                            failure(error)
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    self.StopIndicator()
                    failure(encodingError)
                }
        })
    }
    //MARK : -  Return Json Methods
    public func requestPostForJson(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
        //let headers = ["authtoken" : strAuthToken]
        print("headers = \(headers)")
        print("\nstrURL = \(url)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        //"Content-Type":"multipart/form-data"]
        manager.retrier = OAuth2Handler()
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            //self.StopIndicator()
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                                
                if dict!["status"] as? String ?? "" != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
                //print("\nResponce = \(dict!)")
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    // Return Json Methods
    public func requestGetForJson(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
       // let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        
        manager.request(url, method: .get, parameters: params, headers: headers).responseJSON { responseObject in
            //self.StopIndicator()
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                if dict!["status"] as? String ?? "" != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    //MARK: - mathod without indicators
    public func requestPostWithOutIndicator(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"2","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
       // let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                if dict!["status"] as? String ?? "" != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    //MARK: - Convert String to Dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isNetworkAvailable() -> Bool{
        if !NetworkReachabilityManager()!.isReachable{
            return false
        }else{
            return true
        }
    }
    
    func StartIndicator(){
         DispatchQueue.main.async {
            objActivity.startActivityIndicator()
        }
    }
    
    func StopIndicator(){
        DispatchQueue.main.async {
            objActivity.stopActivity()
        }
    }
}

//MARK :- retrier

class OAuth2Handler:RequestRetrier {
    
    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        /*if let response = request.task?.response as? HTTPURLResponse{
        if response.statusCode == 401 {
            completion(true, 1.0) // retry after 1 second
        } else if response.statusCode == -1005 {
            completion(true, 1.0) // retry after 1 second
        } else {
            completion(false, 0.0) // don't retry
            }
        }*/
        
        if error.localizedDescription.count > 0 && request.retryCount < 2{
            
            if error.localizedDescription == "The request timed out."{
                completion(false, 0.0) // don't retry
            }else{
                completion(true, 1.0)  // retry after 1 second
            }
            
        }else{
            completion(false, 0.0) // don't retry
        }
    }
}

//MARK:- extension for stripe payment method
extension WebServiceManager{
    public func requestAddCardOnStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" : stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Delete card
    public func requestDeleteCardFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" :  stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .delete, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Get all card List
    public func requestGetCardsFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" : stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseJSON { responseObject in
            
            // print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    SVProgressHUD.dismiss()
                }catch{
                    SVProgressHUD.dismiss()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                SVProgressHUD.dismiss()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}
