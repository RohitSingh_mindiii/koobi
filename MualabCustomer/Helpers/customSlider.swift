//
//  customSlider.swift
//  MualabBusiness
//
//  Created by Mac on 17/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
let appColor = #colorLiteral(red: 0.1058823529, green: 0.8274509804, blue: 0.7882352941, alpha: 1)

class customSlider: UISlider {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //self.setThumbImage(UIImage(named:"redius_point_ico"), for:.normal)
       // self.setThumbImage(#imageLiteral(resourceName: "active_check_box_icon"), for: .normal)
        let img = #imageLiteral(resourceName: "blue_img") //UIImage(named:"radius_img")
        let size = CGSize.init(width: rect.size.width-4,height:6)
        let resizedImage = reImage(image: img,newSize: size).resizableImage(withCapInsets: .zero)
        //self.setMaximumTrackImage(resizedImage,for:.normal)
        
        let img2 = #imageLiteral(resourceName: "blue_img")// UIImage(named:"pink_radius_img")
        
        let resizedImage2 = reImage(image: img2,newSize: size).resizableImage(withCapInsets: .zero)
        //self.setMinimumTrackImage(resizedImage2,for:.normal)
    }
    
    /// custom slider track height
    var trackHeight: CGFloat = 6
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        // Use properly calculated rect
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
 
    func resizeImage(image: UIImage, newSize: CGSize) -> UIImage {
        
        // Actually do the resizing to the rect using the ImageContext stuff
        let rect = CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in:rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func reImage(image: UIImage, newSize: CGSize) -> UIImage {
        // Guard newSize is different
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in:CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

class customSlider2: UISlider {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //   self.setThumbImage(UIImage(named:"grayGubbara_ico"), for:.normal)
        self.setThumbImage(#imageLiteral(resourceName: "grayGubbara_ico"), for: .normal)
        self.minimumTrackTintColor  = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
        self.maximumTrackTintColor  = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        let img = UIImage(named:"radius_img")
        let size = CGSize.init(width: rect.size.width-4,height:8)
        let resizedImage = reImage(image: img!,newSize: size).resizableImage(withCapInsets: .zero)
        // self.setMaximumTrackImage(resizedImage,for:.normal)
        
        let img2 = UIImage(named:"pink_radius_img")
        
        let resizedImage2 = reImage(image: img2!,newSize: size).resizableImage(withCapInsets: .zero)
        //self.setMinimumTrackImage(resizedImage2,for:.normal)
    }
    
    /// custom slider track height
    var trackHeight: CGFloat = 8
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        // Use properly calculated rect
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
    
    func resizeImage(image: UIImage, newSize: CGSize) -> UIImage {
        // Actually do the resizing to the rect using the ImageContext stuff
        let rect = CGRect.init(x: 0, y: 15, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in:rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func reImage(image: UIImage, newSize: CGSize) -> UIImage {
        // Guard newSize is different
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in:CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
