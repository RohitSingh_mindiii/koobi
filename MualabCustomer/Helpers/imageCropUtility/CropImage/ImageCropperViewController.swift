
import UIKit
func IS_IPAD() -> Bool {
    return (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
}

func iOSVersin() -> Float {
    let strVersion = UIDevice.current.systemVersion as NSString
    return strVersion.floatValue
}

let croper_width  = UIScreen.main.bounds.size.width - kMaskViewBorderWidth*2
let croper_height  = 3/4 * croper_width

let WIDTH_RATIO_MULTIPLIER = UIScreen.main.bounds.size.width///320
let HEIGHT_RATIO_MULTIPLIER = UIScreen.main.bounds.size.height/640

@objc protocol ImageCropperDelegate{
    func imageCropperDidFinishCroppingImage(cropprdImage: UIImage)
    func imageCropperDidCancel()
}

@objc class ImageCropperViewController: UIViewController {
    
    var sourceImage: UIImage!
    var cropPhotoView: BVCropPhotoView!
    var delegate: ImageCropperDelegate?
    
    init(image: UIImage) {
        super.init(nibName: nil, bundle: nil)
        self.sourceImage = image
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        //        UIApplication.shared.setStatusBarHidden(false, with:UIStatusBarAnimation.none)
        //        var navigationBar: UINavigationBar?
        //
        //        navigationBar = UINavigationBar(frame: CGRect(x: 0.0, y: 20.0, width: self.view.frame.size.width, height: 64))
        //
        //        navigationBar?.barStyle = UIBarStyle.black
        //        navigationBar?.isTranslucent = true
        //
        //
        //        let aNavigationItem: UINavigationItem = UINavigationItem()
        //        let lblTitle: UILabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 40))
        //        lblTitle.text = ""
        //        lblTitle.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        //        lblTitle.backgroundColor = UIColor.clear
        //        lblTitle.textColor = UIColor.white
        //        lblTitle.textAlignment = NSTextAlignment.center
        //        aNavigationItem.titleView = lblTitle
        //
        //        let doneButton: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ImageCropperViewController.finishCropping))
        //        aNavigationItem.rightBarButtonItem = doneButton
        //        aNavigationItem.rightBarButtonItem?.tintColor = UIColor.white
        //
        //
        //        let cancelButton: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ImageCropperViewController.cancelCropping))
        //        aNavigationItem.leftBarButtonItem = cancelButton
        //        aNavigationItem.leftBarButtonItem?.tintColor = UIColor.white
        //        navigationBar?.setItems([aNavigationItem], animated: true)
        //navigationBar?.setItems(NSArray(object: aNavigationItem) as [AnyObject], animated: true)
        
        //ssss
        let navigationView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 64))
        navigationView.backgroundColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.7098039216, alpha: 1)//UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
        
        let btnH : CGFloat = 30.0
        let btnCancel = UIButton(frame: CGRect(x: 10.0, y: (navigationView.frame.size.height - btnH)/2+15, width: 20, height: 20))
        btnCancel.setImage(#imageLiteral(resourceName: "back_ico_white"), for: .normal)
        //btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.white, for: .normal)
        
        btnCancel.addTarget(self, action: #selector(ImageCropperViewController.cancelCropping), for: .touchUpInside)
        
        let lblCropImage = UILabel(frame: CGRect(x: (((navigationView.frame.size.width)/2)-100),y: (navigationView.frame.size.height - btnH)/2+10, width: 200, height: btnH))
        lblCropImage.font = UIFont(name: "Nunito-Bold", size: 20)
        lblCropImage.text = ""//"Crop Image"
        lblCropImage.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let lblCrop = UILabel(frame: CGRect(x: self.view.frame.size.width - 60,y: (navigationView.frame.size.height - btnH)/2+12, width: 70, height: btnH))
        lblCrop.font = UIFont(name: "Nunito-Bold", size: 17)
        lblCrop.text = "Crop"
        lblCrop.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let btnDone = UIButton(frame: CGRect(x: self.view.frame.size.width - 70,y: (navigationView.frame.size.height - btnH)/2+10, width: 70, height: btnH))
//        btnDone.setTitle("CROP", for: .normal)
//        btnDone.setTitleColor(.white, for: .normal)
        
        btnDone.addTarget(self, action: #selector(ImageCropperViewController.finishCropping), for: .touchUpInside)
        navigationView.addSubview(btnCancel)
        navigationView.addSubview(btnDone)
        navigationView.addSubview(lblCrop)
        navigationView.addSubview(lblCropImage)
        
        self.cropPhotoView = BVCropPhotoView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        //        let adjustedW = CGFloat(croper_width) * WIDTH_RATIO_MULTIPLIER//CGFloat(croper_width) * WIDTH_RATIO_MULTIPLIER
        //
        //        var adjustedH = CGFloat(croper_height) * HEIGHT_RATIO_MULTIPLIER//CGFloat(croper_height) * HEIGHT_RATIO_MULTIPLIER
        
        let adjustedW = WIDTH_RATIO_MULTIPLIER
        var adjustedH = WIDTH_RATIO_MULTIPLIER //* 0.75
        
        if(croper_width == croper_height){
            adjustedH = adjustedW//*3/4
        }
        cropPhotoView.cropSize = CGSize(width: adjustedW, height: adjustedH)
        
        
        //        if (IS_IPAD()){
        //            cropPhotoView.overlayImage = UIImage(named: "ipad_crop.png")
        //            cropPhotoView.cropSize = CGSizeMake(400, 400)
        //        }
        //        else{
        //            var overlayImageName = "crop-overlay.png"
        //            if(SCREEN_WIDTH == 375){
        //               overlayImageName =  "crop_overlay_bg750"
        //            }
        //            else if(SCREEN_WIDTH == 414){
        //                overlayImageName = "crop_overlay_bg1242"
        //            }
        //            else{
        //                overlayImageName = "crop-overlay.png"
        //            }
        //
        //            cropPhotoView.overlayImage = UIImage(named: overlayImageName)
        //            cropPhotoView.cropSize = CGSizeMake(320, 298)
        //        }
        
        cropPhotoView.sourceImage = self.sourceImage
        cropPhotoView.requestFor = 1;
        cropPhotoView.backgroundColor = UIColor.black
        self.view.addSubview(cropPhotoView)
        self.view.backgroundColor = UIColor.black
        self.view.addSubview(navigationView)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // UIApplication.shared.statusBarStyle = .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cancelCropping(){
        cropPhotoView.isHidden = true
        delegate?.imageCropperDidCancel()
    }
    
    @objc func finishCropping(){
        cropPhotoView.isHidden = true
        let croppedImage: UIImage = cropPhotoView.croppedImage()
        delegate?.imageCropperDidFinishCroppingImage(cropprdImage: croppedImage)
    }
    
}

