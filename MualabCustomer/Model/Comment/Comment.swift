//
//  Comment.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class Comment: NSObject {
    
    var strComment = ""
    var strProfileImage = ""
    var strCommentId = ""
    var strUserId = ""
    var strUserName = ""
    var likesCount: Int?
    var strTime = ""
    var isLike = false

}
