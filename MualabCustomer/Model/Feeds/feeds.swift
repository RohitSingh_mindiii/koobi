    //
//  feeds.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class feeds: NSObject {
    
    var cellHeight : CGFloat?
    var feedImageRatio = "0"
    
    var tagCategoryName:String = ""
    var tagServiceName: String = ""
    var tagCategoryId:Int = 0
    var tagServiceId: Int = 0
    var tagBusinessTypeId: Int = 0
    var tagStaffId: Int = 0
    var tagArtistId: Int = 0
    var tagStrIncallPrice: String = ""
    var tagStrOutcallPrice: String = ""
   
    var _id : Int = 0
    var caption: String = ""
    var commentCount: Int = 0
    var feedType:String = ""
    var location: String = ""
    var likeCount: Int = 0
    var arrFeed : [feedData] = []
    var arrTagedServices : [SubSubService] = []
    var timeElapsed: String = ""
    var userInfo : storyUser?
    var isLike : Bool = false
    var followerStatus : Bool = false
    var arrPhotoInfo = [PhotoInfo]()
    var arrOfPeopleTagArray = [[PeopleTag]]()
    var isSave = 0
    var userId = 0
    
    
    init?(dict : [String:Any]){
        
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        if let userIds = dict["userId"] as? Int{
            self.userId = userIds
        }
        if let id = dict["isSave"] as? Int{
            self.isSave = id
        }
        if let likeCount = dict["likeCount"] as? Int{
            self.likeCount = likeCount
        }
        
        if let commentCount = dict["commentCount"] as? Int{
            self.commentCount = commentCount
        }
        
        if let arr = dict["feedData"] as? [[String:Any]]{
            for dic in arr{
                let obj = feedData(dict:dic)
                self.arrFeed.append(obj!)
            }
        }
        
        if let feedType = dict["feedType"] as? String{
            self.feedType = feedType
        }
        
        if let caption = dict["caption"] as? String{
            self.caption = caption
        }
        
        if let location = dict["location"] as? String{
            self.location = location
        }
        
        if let timeElapsed = dict["timeElapsed"] as? String{
            self.timeElapsed = timeElapsed
        }
        
        if let isLike = dict["isLike"] as? Bool{
            self.isLike = isLike
        }
        
        if let feedImageRatio1 = dict["feedImageRatio"] as? Int{
            self.feedImageRatio = String(feedImageRatio1)
        }else if let feedImageRatio1 = dict["feedImageRatio"] as? String{
            self.feedImageRatio = feedImageRatio1
        }
        
        if let arrUser = dict["userInfo"] as? NSArray{
            if let userDic = arrUser[0] as? [String:Any]{
                self.userInfo = storyUser.init(dict: userDic)
            }
        }
       
        if let followerStatus = dict["followerStatus"] as? Int{
            if followerStatus == 1{
                 self.followerStatus = true
            }else{
                self.followerStatus = false
            }
        }else if let followerStatus = dict["followerStatus"] as? String{
            if followerStatus == "1"{
                self.followerStatus = true
            }else{
                self.followerStatus = false
            }
        }
        
        //self.feedType = "image"
        if self.feedType == "image"{
            
            guard self.arrFeed.count > 0 else{return}
            
            var arrOfTagInfoArray = [[TagInfo]]()
            var arrOfServiceTagInfoArray = [[TagInfo]]()
            
            if let arrOfarrTags = dict["peopleTag"] as? NSArray{
                for arrTags in arrOfarrTags {
                    var arrTemp = [TagInfo]()
                    if let arrTags = arrTags as? [[String : Any]] {
                        for dict in arrTags {
                            let objTagInfo = TagInfo.initWithServerData(dict: dict,dictDetail:dict["tagDetails"] as! [String:Any])
                            objTagInfo.strTagType = "people"
                            arrTemp.append(objTagInfo)
                        }
                    }
                    arrOfTagInfoArray.append(arrTemp)
                }
            }
            
            /*

                 serviceTag =     (
                             {
                         arstistServiceTagDetail =             {
                             artistServiceID = 7;
                             buisnessTypeID =                 (
                                 2
                             );
                             buisnessTypeName =                 (
                                 "Hair & Beauty Salon"
                             );
                             categoryID =                 (
                                 21
                             );
                             categoryName =                 (
                                 "Hair Cut & Maintain"
                             );
                             completionTime = "00:10";
                             description = test;
                             inCallPrice = "25.20";
                             outCallPrice = "50.30";
                             title = "Hair Color";
                         };
                         artistDetail =             {
                             firstName = HRKBIZ;
                             lastName = hrk;
                         };
                         staffDetail =             {
                         };
                         staffServiceTagDetail =             {
                             buisnessTypeID =                 (
                             );
                             buisnessTypeName =                 (
                             );
                             categoryID =                 (
                             );
                             categoryName =                 (
                             );
                             description =                 (
                             );
                             title =                 (
                             );
                         };
                         tag =             {
                             index = 0;
                             tagDetails =                 {
                                 artistId = 7;
                                 artistServiseTagId = 7;
                                 staffId = 0;
                                 staffServiseTagId = 0;
                                 tabType = service;
                                 tagId = 7;
                             };
                             "unique_tag_id" = "Hair Color";
                             "x_axis" = "31.15942028985507";
                             "y_axis" = "62.40554813289149";
                         };
                     },
                             {
                         arstistServiceTagDetail =             {
                             buisnessTypeID =                 (
                             );
                             buisnessTypeName =                 (
                             );
                             categoryID =                 (
                             );
                             categoryName =                 (
                             );
                         };
                         artistDetail =             {
                         };
                         staffDetail =             {
                             firstName = Shiva;
                             lastName = Hrk;
                         };
                         staffServiceTagDetail =             {
                             buisnessTypeID =                 (
                                 2
                             );
                             buisnessTypeName =                 (
                                 "Hair & Beauty Salon"
                             );
                             categoryID =                 (
                                 21
                             );
                             categoryName =                 (
                                 "Hair Cut & Maintain"
                             );
                             completionTime = "00:10";
                             description =                 (
                                 "test group of the day"
                             );
                             inCallPrice = "25.0";
                             outCallPrice = "35.0";
                             staffServiceID = 19;
                             title =                 (
                                 "Blonde Hair 01"
                             );
                         };
                         tag =             {
                             index = 0;
                             tagDetails =                 {
                                 artistId = 0;
                                 artistServiseTagId = 0;
                                 staffId = 27;
                                 staffServiseTagId = 4;
                                 tabType = service;
                                 tagId = 19;
                             };
                             "unique_tag_id" = "Blonde Hair 01";
                             "x_axis" = "51.69082125603865";
                             "y_axis" = "42.75000361956884";
                         };
                     }
                 )
            */

            if let arrOfarrTags = dict["serviceTag"] as? NSArray{
                var indexGlobal = 0
                var index = 0
                var arrNewTemp = [TagInfo]()
                for arrTags in arrOfarrTags {
                    if let arrTags = arrTags as? [String : Any] {
                        //for dict in arrTags {
                        
                        //// For crash handle
                        let dictArtistServiceTagDetailNN = arrTags["arstistServiceTagDetail"] as? [String:Any]
                        let dictStaffServiceTagDetailNN = arrTags["staffServiceTagDetail"] as? [String:Any]
                        let buisnessTypeIDNN = dictArtistServiceTagDetailNN!["buisnessTypeID"] as? [Int]
                        let buisnessTypeIDStaffNN = dictStaffServiceTagDetailNN!["buisnessTypeID"] as? [Int]
                        if buisnessTypeIDNN!.count == 0 && buisnessTypeIDStaffNN!.count == 0{
                            break
                        }else{
                        }
                        ////
                       
                        let dictTag = arrTags["tag"] as? [String:Any]
                        let ind = dictTag!["index"] as? Int ?? 0
                        index = ind
                        
                        ////
                        let dictDetail = dictTag!["tagDetails"] as? [String:Any]
                        //let dictArtistDetail = arrTags["artistDetail"] as? [String:Any]
                        //let dictStaffDetail = arrTags["staffDetail"] as? [String:Any]
                        let dictArtistServiceTagDetail = arrTags["arstistServiceTagDetail"] as? [String:Any]
                        let dictStaffServiceTagDetail = arrTags["staffServiceTagDetail"] as? [String:Any]
                        let buisnessTypeID = dictArtistServiceTagDetail!["buisnessTypeID"] as? [Int]
                        var dictForServiceDetail = dictStaffServiceTagDetail
                        var staffId = 0
                        if buisnessTypeID!.count == 0{
                            dictForServiceDetail = dictStaffServiceTagDetail
                            //serviceId = dictForServiceDetail!["staffServiceID"] as? Int ?? 0
                            staffId = dictDetail!["staffId"] as? Int ?? 0
                        }else{
                            dictForServiceDetail = dictArtistServiceTagDetail
                            //serviceId = dictForServiceDetail!["artistServiceID"] as? Int ?? 0
                            staffId = dictDetail!["artistId"] as? Int ?? 0
                        }
                        let idB = dictForServiceDetail!["buisnessTypeID"] as? [Int]
                        let businessTypeIdN = idB![0]
                        let idC = dictForServiceDetail!["categoryID"] as? [Int]
                        let catTypeIdN = idC![0]
                        
                        let bName = dictForServiceDetail!["buisnessTypeName"] as? [String]
                        let businessTypeNameN = bName![0]
                        let cName = dictForServiceDetail!["categoryName"] as? [String]
                        let catTypeNameN = cName![0]
                        var serviceNameN = ""
                        if let tagName = dictForServiceDetail!["title"] as? [String]{
                            serviceNameN = tagName[0]
                        }
                        if serviceNameN.count == 0{
                           serviceNameN = dictForServiceDetail!["title"] as? String ?? ""
                        }
                        
                        let isOutCall = dictDetail!["isOutCall"] as? String ?? "0"
                        index = ind

                        let dictTagDetail = ["artistId":String(dictDetail!["artistIdFeed"] as? Int ?? 0),
                                             "incallPrice":dictForServiceDetail!["inCallPrice"] as? String ?? "0.0",
                            "outcallPrice":dictForServiceDetail!["outCallPrice"] as? String ?? "0.0",
                            "tagId":dictDetail!["tagId"] as? Int ?? 0,
                            "businessTypeId":businessTypeIdN,
                            "categoryId":catTypeIdN,
                            "staffId":staffId,
                            "staffInCallPrice":dictForServiceDetail!["inCallPrice"] as? String ?? "0.0",
                            "staffOutCallPrice":dictForServiceDetail!["outCallPrice"] as? String ?? "0.0",
                            "isOutCall":isOutCall,
                            "businessTypeName":businessTypeNameN,
                            "categoryName":catTypeNameN,
                            "title":serviceNameN
                            ] as [String : Any]
                        print(dictTagDetail)
                        ////
                        
                        let objTagInfo = TagInfo.initWithServerData(dict: (arrTags["tag"] as? [String:Any])!, dictDetail:dictTagDetail)
                        objTagInfo.strTagType = "service"
                        if index == indexGlobal{
                            arrNewTemp.append(objTagInfo)
                        }else{
                            indexGlobal = index
                            arrOfServiceTagInfoArray.append(arrNewTemp)
                            arrNewTemp.removeAll()
                            arrNewTemp.append(objTagInfo)
                        }
                        //}
                    }
                }
                if arrOfServiceTagInfoArray.count == 0{
                   arrOfServiceTagInfoArray.append(arrNewTemp)
                }
            }
            
            print(arrOfServiceTagInfoArray.count)
            
            //guard self.arrFeed.count == arrOfTagInfoArray.count else{return}
        
            for index in 0..<self.arrFeed.count {
                let objPhotoInfo = PhotoInfo.init(photoInfo: ["":""])
                if arrOfTagInfoArray.count > index {
                    objPhotoInfo.arrTags = arrOfTagInfoArray[index]
                    if arrOfServiceTagInfoArray.count > index{
                        let arrService = arrOfServiceTagInfoArray[index]
                        objPhotoInfo.arrTags.append(contentsOf: arrService)
                    }
                }else{
                    if arrOfServiceTagInfoArray.count > index{
                        let arrService = arrOfServiceTagInfoArray[index]
                        objPhotoInfo.arrTags.append(contentsOf: arrService)
                    }
                }
                self.arrPhotoInfo.append(objPhotoInfo)
            }
        }
        
       /*
        if let arrOfarrTags = dict["peopleTag"] as? NSArray{
            for arrTags in arrOfarrTags {
                var arrTemp = [PeopleTag]()
                if let arrTags = arrTags as? [[String : Any]] {
                    for dict in arrTags {
                        let objPeopleTag = PeopleTag.init(dict: dict)
                        arrTemp.append(objPeopleTag)
                    }
                }
                self.arrOfPeopleTagArray.append(arrTemp)
            }
         }
        */
    }
}

class feedData {
    
    var feedPost : String = ""
    var videoThumb: String = ""
    
    init?(dict : [String:Any]){
        
        if let feedPost = dict["feedPost"] as? String{
            self.feedPost = feedPost
        }
        if let videoThumb = dict["videoThumb"] as? String{
            self.videoThumb = videoThumb
        }
    }
}

