//
//  stories.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class stories{
    
    var _id : Int = 0;
    var myStory:String = ""
    var type:String = "";
    var videoThumb:String = "";
    
    init(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        if let myStory = dict["myStory"] as? String{
            self.myStory = myStory
        }
        if let type = dict["type"] as? String{
            self.type = type
        }
        if let videoThumb = dict["videoThumb"] as? String{
            self.videoThumb = videoThumb
        }
    }
}
