
//

import Foundation

struct UserInfo : Codable {
    
	let userId : Int?
	let loc : [Int]?
	let __v : Int?
	let upd : String?
	let crd : String?
	let isDocument : Int?
	let outCallpreprationTime : String?
	let inCallpreprationTime : String?
	let serviceType : Int?
	let radius : String?
	let status : Int?
	let bankStatus : Int?
	let bio : String?
	let reviewCount : Int?
	let postCount : Int?
	let certificateCount : Int?
	let serviceCount : Int?
	let followingCount : Int?
	let followersCount : Int?
	let firebaseToken : String?
	let chatId : Int?
	let mailVerified : Int?
	let oTP : String?
	let otpVerified : String?
	let longitude : Int?
	let latitude : Int?
	let authToken : String?
	let deviceToken : Int?
	let deviceType : Int?
	let socialType : String?
	let socialId : String?
	let userType : String?
	let contactNo : Int?
	let countryCode : Int?
	let country : String?
	let state : String?
	let city : String?
	let address : String?
	let dob : String?
	let gender : String?
	let password : String?
	let email : String?
	let profileImage : String?
	let businessType : String?
	let buildingNumber : String?
	let businesspostalCode : String?
	let businessName : String?
	let userName : String?
	let lastName : String?
	let firstName : String?

	enum CodingKeys: String, CodingKey {

		case userId = "_id"
		case loc = "loc"
		case __v = "__v"
		case upd = "upd"
		case crd = "crd"
		case isDocument = "isDocument"
		case outCallpreprationTime = "outCallpreprationTime"
		case inCallpreprationTime = "inCallpreprationTime"
		case serviceType = "serviceType"
		case radius = "radius"
		case status = "status"
		case bankStatus = "bankStatus"
		case bio = "bio"
		case reviewCount = "reviewCount"
		case postCount = "postCount"
		case certificateCount = "certificateCount"
		case serviceCount = "serviceCount"
		case followingCount = "followingCount"
		case followersCount = "followersCount"
		case firebaseToken = "firebaseToken"
		case chatId = "chatId"
		case mailVerified = "mailVerified"
		case oTP = "OTP"
		case otpVerified = "otpVerified"
		case longitude = "longitude"
		case latitude = "latitude"
		case authToken = "authToken"
		case deviceToken = "deviceToken"
		case deviceType = "deviceType"
		case socialType = "socialType"
		case socialId = "socialId"
		case userType = "userType"
		case contactNo = "contactNo"
		case countryCode = "countryCode"
		case country = "country"
		case state = "state"
		case city = "city"
		case address = "address"
		case dob = "dob"
		case gender = "gender"
		case password = "password"
		case email = "email"
		case profileImage = "profileImage"
		case businessType = "businessType"
		case buildingNumber = "buildingNumber"
		case businesspostalCode = "businesspostalCode"
		case businessName = "businessName"
		case userName = "userName"
		case lastName = "lastName"
		case firstName = "firstName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userId = try values.decodeIfPresent(Int.self, forKey: .userId)
		loc = try values.decodeIfPresent([Int].self, forKey: .loc)
		__v = try values.decodeIfPresent(Int.self, forKey: .__v)
		upd = try values.decodeIfPresent(String.self, forKey: .upd)
		crd = try values.decodeIfPresent(String.self, forKey: .crd)
		isDocument = try values.decodeIfPresent(Int.self, forKey: .isDocument)
		outCallpreprationTime = try values.decodeIfPresent(String.self, forKey: .outCallpreprationTime)
		inCallpreprationTime = try values.decodeIfPresent(String.self, forKey: .inCallpreprationTime)
		serviceType = try values.decodeIfPresent(Int.self, forKey: .serviceType)
		radius = try values.decodeIfPresent(String.self, forKey: .radius)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		bankStatus = try values.decodeIfPresent(Int.self, forKey: .bankStatus)
		bio = try values.decodeIfPresent(String.self, forKey: .bio)
		reviewCount = try values.decodeIfPresent(Int.self, forKey: .reviewCount)
		postCount = try values.decodeIfPresent(Int.self, forKey: .postCount)
		certificateCount = try values.decodeIfPresent(Int.self, forKey: .certificateCount)
		serviceCount = try values.decodeIfPresent(Int.self, forKey: .serviceCount)
		followingCount = try values.decodeIfPresent(Int.self, forKey: .followingCount)
		followersCount = try values.decodeIfPresent(Int.self, forKey: .followersCount)
		firebaseToken = try values.decodeIfPresent(String.self, forKey: .firebaseToken)
		chatId = try values.decodeIfPresent(Int.self, forKey: .chatId)
		mailVerified = try values.decodeIfPresent(Int.self, forKey: .mailVerified)
        oTP = try values.decodeIfPresent(String.self, forKey: .oTP)
		otpVerified = try values.decodeIfPresent(String.self, forKey: .otpVerified)
		longitude = try values.decodeIfPresent(Int.self, forKey: .longitude)
		latitude = try values.decodeIfPresent(Int.self, forKey: .latitude)
		authToken = try values.decodeIfPresent(String.self, forKey: .authToken)
		deviceToken = try values.decodeIfPresent(Int.self, forKey: .deviceToken)
		deviceType = try values.decodeIfPresent(Int.self, forKey: .deviceType)
		socialType = try values.decodeIfPresent(String.self, forKey: .socialType)
		socialId = try values.decodeIfPresent(String.self, forKey: .socialId)
		userType = try values.decodeIfPresent(String.self, forKey: .userType)
		contactNo = try values.decodeIfPresent(Int.self, forKey: .contactNo)
		countryCode = try values.decodeIfPresent(Int.self, forKey: .countryCode)
		country = try values.decodeIfPresent(String.self, forKey: .country)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		dob = try values.decodeIfPresent(String.self, forKey: .dob)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
		businessType = try values.decodeIfPresent(String.self, forKey: .businessType)
		buildingNumber = try values.decodeIfPresent(String.self, forKey: .buildingNumber)
		businesspostalCode = try values.decodeIfPresent(String.self, forKey: .businesspostalCode)
		businessName = try values.decodeIfPresent(String.self, forKey: .businessName)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
	}

}
