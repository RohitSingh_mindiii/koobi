//
//  user.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class user: NSObject {
    
    var strUserName:String!
    var strUserId:String!
    var strEmail:String!
    var strPassword:String!
    var strProfileImage:String!
    var strContactNo:String!
    var strCountryCode:String!
    var strUserType:String!
    var strSocialId:String!
    var strChatId:String!
    var strFirebaseToken:String!
    var strTime:String!
    var strPhoneNo:String!
    var strCode:String!
    var strUserComment:String!
    var strCommentId = ""
    var commentsLikeCount:Int!
    var isFollowing:Bool = false
    var isFollower:Bool = false
    var isLikeComment:Bool = false

}

class storyUser {
    
    var _id : Int = 0
    var storyCount:Int = 1
    var firstName : String = ""
    var lastName : String = ""
    var profileImage : String = ""
    var userName : String = ""
    var followerStatus : Bool = false
    
    init?(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }        
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        if let storyCount = dict["count"] as? Int{
            self.storyCount = storyCount
        }
        
    }
}

class likeUser {
    
    var _id : Int = 0
    var likeById : Int = 0
    var firstName : String = ""
    var lastName : String = ""
    var profileImage : String = ""
    var userName : String = ""
    var followerStatus : Bool = false
    
    init?(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        if let likeById = dict["likeById"] as? Int{
            self.likeById = likeById
        }
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        if let followerStatus = dict["followerStatus"] as? Bool{
            self.followerStatus = followerStatus
        }
    }
}

class commentUser {
    
    var _id : Int = 0
    var commentById : Int = 0
    var firstName : String = ""
    var lastName : String = ""
    var profileImage : String = ""
    var userName : String = ""
    var commentLikeCount:Int = 0
    var comment : String = ""
    var timeElapsed: String = ""
    var likeStatus:Bool = false
    var type : String = ""
    var feedId : String = ""
   
    init?(dict : [String:Any]) {
        if let id = dict["_id"] as? Int{
            self._id = id
        }
        
        feedId = dict["feedId"] as? String ?? ""
        if let id = dict["feedId"] as? Int{
            self.feedId = String(id)
        }
        if let commentById = dict["commentById"] as? Int{
            self.commentById = commentById
        }
        
        if let commentLikeCount = dict["commentLikeCount"] as? Int{
            self.commentLikeCount = commentLikeCount
        }
        
        if let firstName = dict["firstName"] as? String{
            self.firstName = firstName
        }
        
        if let lastName = dict["lastName"] as? String{
            self.lastName = lastName
        }
        
        if let profileImage = dict["profileImage"] as? String{
            self.profileImage = profileImage
        }
        
        if let userName = dict["userName"] as? String{
            self.userName = userName
        }
        
        if let comment = dict["comment"] as? String{
            self.comment = comment
        }
        
        if let type = dict["type"] as? String{
            self.type = type
        }
        
        if let timeElapsed = dict["timeElapsed"] as? String{
            self.timeElapsed = timeElapsed
        }
        if let likeStatus = dict["isLike"] as? Bool{
            self.likeStatus = likeStatus
        }
        
        if self.type == "text"{
            self.comment = self.comment.removingPercentEncoding!
        }
    }
}
