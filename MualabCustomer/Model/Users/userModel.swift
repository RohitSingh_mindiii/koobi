//
//  userModel.swift
//  MualabBusiness
//
//  Created by Mac on 10/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation


class User : NSObject,NSCoding {
    
    var address = Address(locality:"",address2:"", city:"", state: "", postalCode: "", country:"", placeName:"", fullAddress:"", latitude: "", longitude:"")
    
    var type:String = "0"
    var email:String = "0"
    var countryCode:String = "0"
    var contactNumber:String = "0"
    var firstName:String = "0"
    var lastName:String = "0"
    var DOB:String = "0"
    var strSocialId:String = ""
    var strSocialType:String = ""
    var userName:String = "0"
    var gender:String = "0"
    var addresss:String = "0"

    
    required init(address:Address) {
        self.address = address
        super.init()
    }
    
    
    required init(userType:String) {
        self.type = userType
        super.init()
    }
    
    // NSCodeing
    required init?(coder aDecoder: NSCoder) {
        self.type = aDecoder.decodeObject(forKey: Keys.user.type) as? String ?? ""
        self.email = aDecoder.decodeObject(forKey: Keys.user.email) as? String ?? ""
        self.countryCode = aDecoder.decodeObject(forKey: Keys.user.countryCode) as? String ?? ""
        self.contactNumber = aDecoder.decodeObject(forKey: Keys.user.contactNumber) as? String ?? ""
        self.firstName = aDecoder.decodeObject(forKey: Keys.user.firstName) as? String ?? ""
        self.lastName = aDecoder.decodeObject(forKey: Keys.user.lastName) as? String ?? ""
        self.userName = aDecoder.decodeObject(forKey: Keys.user.userName) as? String ?? ""
        self.DOB = aDecoder.decodeObject(forKey: Keys.user.DOB) as? String ?? ""
        self.gender = aDecoder.decodeObject(forKey: Keys.user.gender) as? String ?? ""
        self.addresss = aDecoder.decodeObject(forKey: Keys.user.address) as? String ?? ""
        self.address = (aDecoder.decodeObject(forKey: Keys.user.address) as? Address)!
        
        self.strSocialId = aDecoder.decodeObject(forKey: Keys.user.socialId) as? String ?? ""
        self.strSocialType = aDecoder.decodeObject(forKey: Keys.user.socialType) as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.strSocialId, forKey: Keys.user.socialId)
        aCoder.encode(self.strSocialType, forKey: Keys.user.socialType)
        aCoder.encode(self.type, forKey: Keys.user.type)
        aCoder.encode(self.email, forKey: Keys.user.email)
        aCoder.encode(self.countryCode, forKey:Keys.user.countryCode)
        aCoder.encode(self.contactNumber, forKey:Keys.user.contactNumber)
        aCoder.encode(self.firstName, forKey: Keys.user.firstName)
        aCoder.encode(self.lastName, forKey: Keys.user.lastName)
        aCoder.encode(self.DOB, forKey:Keys.user.DOB)
        aCoder.encode(self.userName, forKey:Keys.user.userName)
        aCoder.encode(self.gender, forKey:Keys.user.gender)
        aCoder.encode(self.address, forKey:Keys.user.address)
        aCoder.encode(self.address, forKey: Keys.user.address)
    }
    
    
    
    
    
    
}

class Address : NSObject,NSCoding  {
    
    var locality : String = ""
    var address2 : String = ""
    var city: String = ""
    var state: String = ""
    var postalCode: String = ""
    var country : String = ""
    var placeName : String = ""
    var fullAddress : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var buildingNumber:String = ""

    
    
    required init(locality : String,address2:String,city:String,state:String,postalCode:String,country:String,placeName:String,fullAddress:String,latitude:String,longitude:String) {
        
        self.locality = locality
        self.address2 = address2
        self.city = city
        self.state = state
        self.postalCode = postalCode
        self.country = country
        self.placeName = placeName
        self.fullAddress = fullAddress
        self.latitude = latitude
        self.longitude = longitude
        super.init()
    }
    
    // NSCodeing
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.locality, forKey: Keys.user.addressComponent.locality)
        aCoder.encode(self.address2, forKey: Keys.user.addressComponent.address2)
        aCoder.encode(self.city, forKey: Keys.user.addressComponent.city)
        aCoder.encode(self.state, forKey: Keys.user.addressComponent.state)
        aCoder.encode(self.postalCode, forKey: Keys.user.addressComponent.postalCode)
        aCoder.encode(self.country, forKey: Keys.user.addressComponent.country)
        aCoder.encode(self.placeName, forKey: Keys.user.addressComponent.placeName)
        aCoder.encode(self.fullAddress, forKey: Keys.user.addressComponent.fullAddress)
        aCoder.encode(self.latitude, forKey: Keys.user.addressComponent.latitude)
        aCoder.encode(self.longitude, forKey: Keys.user.addressComponent.longitude)
        aCoder.encode(self.buildingNumber, forKey: Keys.user.addressComponent.buildingNumber)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.locality = aDecoder.decodeObject(forKey: Keys.user.addressComponent.locality) as? String ?? ""
        self.address2 = aDecoder.decodeObject(forKey: Keys.user.addressComponent.address2) as? String ?? ""
        self.city = aDecoder.decodeObject(forKey: Keys.user.addressComponent.city) as? String ?? ""
        self.state = aDecoder.decodeObject(forKey: Keys.user.addressComponent.state) as? String ?? ""
        self.postalCode = aDecoder.decodeObject(forKey: Keys.user.addressComponent.postalCode) as? String ?? ""
        self.country = aDecoder.decodeObject(forKey: Keys.user.addressComponent.country) as? String ?? ""
        self.placeName = aDecoder.decodeObject(forKey: Keys.user.addressComponent.placeName) as? String ?? ""
        self.fullAddress = aDecoder.decodeObject(forKey: Keys.user.addressComponent.fullAddress) as? String ?? ""
        self.latitude = aDecoder.decodeObject(forKey: Keys.user.addressComponent.latitude) as? String ?? ""
        self.longitude = aDecoder.decodeObject(forKey: Keys.user.addressComponent.longitude) as? String ?? ""
        self.buildingNumber = aDecoder.decodeObject(forKey: Keys.user.addressComponent.buildingNumber) as? String ?? ""
    }
}
