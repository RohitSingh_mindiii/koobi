//
//  AppShareData.swift
//  MualabBusiness
//
//  Created by Mac on 27/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import Foundation
import UIKit
import AVKit

let objAppShareData = AppShareData.sharedObject()
var slideMenuXPosition:Double = 0.0

class AppShareData {
    var imgForPostFeed:UIImage?
    var videoUrlForPostFeed:URL?
    var isVideoPostFeed:Bool = false
    var isBackToSearchBoardFromBookingDetail:Bool = false
    var isFromCaptionFeedToBack:Bool = false
    var isExploreGridToAllFeed:Bool = false
    var isImageCrop:Bool = false
    var isFromDidFinish:Bool = false
    var isSlideMenuToProfileBack:Bool = false
    var isFromPostUpload = false
    var isFromServiceTagBook = false
    var isCameraForFeed = false
    var arrTBottomSheetModal = [String]()
    var isFromServiceTag = false
    var isFromChatToProfile = false
    var isFromComment = false
    var fromAcceptInvitation = false
    var fromLikeComment = false
    var isFromPaymentDoneToAppointmentDetail = false
    var arrTagePeopleNameIds = [[String:Any]]()
    var arrTagePeopleNameData = [ExpSearchTag]()
    var strIsAnyInvitation = ""
    var badgeContSocialNotification = 0
    var badgeContBookingNotification = 0
    var otpVerify = true
    var onTrackView = false
    var objServiceForEditBooking = SubSubService.init(dict: [:])
    var isOutCallServiceSelectedAlready = false
    var isBookingFromService = false
    var isFromNotification = false
    var notificationInfoDict : [String : Any]?
    var notificationType : String = ""
    var shouldReviewPopUpOpen = false
    let objModelEditTimeSloat = ModelEditTimeSloat()
    var isOtherSelectedForProfile : Bool = false
    var selectedOtherIdForProfile : Int = 0
    var selectedOtherTypeForProfile : String = ""
    var strVoucherCode : String = ""
    
    var isUserSelectedForProfile : Bool = false
    var selectedUserIdForProfile : Int = 0
    
    var isForBookingOutCallAddress : Bool = false
    var strBookingOutCallAddress : String = ""
    var latitudeBookingOutCallAddress : String = ""
    var longitudeBookingOutCallAddress : String = ""
    
    var strArtistID : String = ""
    let objModelHoldAddStaff = ModelHoldAddStaff()
    
    var deviceToken : String = "abcd"
    var firebaseToken : String = "abcd"
    
    var objAppdelegate:AppDelegate = AppDelegate()
    var objUser:user = user()
    var strCurruntCountry:String!
    var strDeviceToken:String!
    var strFCMToken:String!
    var currentVCName:String!
    var isDeleteChat = false
    var isFileIsUploding = false
    var isCloseAddFeed = false
    //    var strAddressForSearchBoard:String!
    //    var strLatitudeForSearchBoard:String!
    //    var strLongitudeForSearchBoard:String!
    var backView: UIView?
    var arrFeedsData = [feeds]()
    var arrFeedsForArtistData = [feeds]()
    var selectedTab = Int()
    var btnAddHiddenONMyFolder = false
    var isTabChange = false
    var isSearchingUsingFilter = false
    var dictFilter : [String : Any] = [String : Any]()
    var objRefineData : RefineData = RefineData()
    var arrSelectedService : [BookingServices] = [BookingServices]()
    var objModelServicesList = ModelServicesList(dict: [:])
    var isFromEditBooking = false
    var isFromPaymentScreen = false
    var arrAddedStaffServices = [ModelServicesList]()
    var isFromConfirmBooking = false
    var timer: Timer?
    var strExpSearchText : String = ""
    var isSwitchToFeedTab = false
    var objEditServiceVC = EditServiceVC()
    
    var manageNavigation : Bool = false
    let objModelFinalSubCategory = ModelFinelSubCategory()
    var param = ["serviceId":"",
                 "bookingType":"",
                 "title":"",
                 "description":"",
                 "incallPrice":"",
                 "outCallPrice":"",
                 "completionTime":""]
    
    //MARK: - Shared object
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
}

//MARK: - Class functions
extension AppShareData{
    
    func validateUrl(_ candidate: String) -> Bool {
        //  Converted with Swiftify v1.0.6488 - https://objectivec2swift.com/
        if candidate.hasPrefix("http://") {
            return true
        }
        else if candidate.hasPrefix("https://") {
            return true
        }
        else if candidate.hasPrefix("www") {
            return true
        }
        else {
            return false
        }
    }
    
    func shakeTextField(_ txtField: UITextField) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = CGPoint(x:txtField.center.x - 7,y:txtField.center.y)
        animation.toValue = CGPoint(x:txtField.center.x + 7, y:txtField.center.y)
        txtField.layer.add(animation,forKey: "position")
    }
    
    func shakeViewField(_ view: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = CGPoint(x:view.center.x - 7,y:view.center.y)
        animation.toValue = CGPoint(x:view.center.x + 7, y:view.center.y)
        view.layer.add(animation,forKey: "position")
    }
    //MARK: - Date time format methods
    
    func dateFormat(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    func dateFormatInDevideForm(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd/MM/yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    func dateFormatToShow(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd-MM-yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func getAge(from date:String) -> String{
        var age = ""
        let dateFormator = DateFormatter()
        if date.contains("-"){
            dateFormator.dateFormat = "yyyy-MM-dd"
        }else{
            dateFormator.dateFormat = "dd/MM/yyyy"
        }
        dateFormator.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let firstDate = dateFormator.date(from: date)
        let calendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate!)
        let date2 = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.year], from: date1, to: date2)
        age = "\(components.year ?? 21)"
        return age
    }
    
    func compressImage(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 681.0
        let maxWidth : CGFloat = 384.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }
    
    func clearUserData(){
        UserDefaults.standard.setValue(nil,forKey: UserDefaults.keys.userInfo)
        UserDefaults.standard.synchronize()
    }
    //MARK: - Image thumb with size
    func generateImage(fromURL URL: URL, withSize size:CGSize) -> UIImage {
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = size
        assetImageGenerator.maximumSize = maxSize
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
            
        } catch {
            //print(error)
        }
        return UIImage()
    }
}
//MARK: - Commen Alert
extension AppShareData{
    
    //    func showAlert(withMessage message:String , on Controller:UIViewController) {
    //        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
    //        let objVC = sb.instantiateViewController(withIdentifier:"alertVC") as! alertVC
    //        objVC.modalPresentationStyle = .overFullScreen
    //        objVC.alertMessage = message
    //        Controller.present(objVC, animated: false, completion: nil)
    //    }
    
    //MARK:- Hide view with animation
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 1.0, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func showAlert(withMessage message:String ,type : String, on Controller:UIViewController) {
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"alertVC") as! alertVC
        objVC.modalPresentationStyle = .overFullScreen
        objVC.alertMessage = message
        objVC.alertType = type
        Controller.present(objVC, animated: false, completion: nil)
    }
    func getDateFromCRD(strDate:String)-> Date{
        let formatter  = DateFormatter()
        // "2019-04-25T05:17:30+00:00"
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let todayDate = formatter.date(from: strDate)
        //  let formatedDate: String = formatter.string(from: todayDate ?? Date())
        return todayDate ?? Date()
    }
    func showDeafultAlert(withTitle title:String, message:String, on Controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let subView = alertController.view.subviews.first!
        let alertContentView = subView.subviews.first!
        alertContentView.backgroundColor = UIColor.gray
        alertContentView.layer.cornerRadius = 20
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        OKAction.setValue(UIColor.theameColors.skyBlueNewTheam, forKey: "titleTextColor")
        alertController.addAction(OKAction)
        Controller.present(alertController, animated: true, completion: nil)
    }
    
    
    func callWebserviceFor_deleteAllbooking() {
        
        //var isServiceDeleted : Bool = false
        if !objServiceManager.isNetworkAvailable(){
            //objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        //objActivity.startActivityIndicator()
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        
        let parameters : Dictionary = [
            "userId" : strUserId,
            ] as [String : Any]
        
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteUserBookService, params: parameters  , success: { response in
            
            //objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                // sessionExpireAlertVC(controller: self)
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                // arvind , why used above line
                if strStatus == k_success{
                    self.stopTimerForHoldBookings()
                    self.objAppdelegate.clearData()
                    self.objAppdelegate.gotoTabBar(withAnitmation: false)
                }else{
                    
                }
            }
        }){ error in
            
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.callWebserviceFor_deleteAllbooking()
            }else{
                
            }
            //objWebserviceManager.StopIndicator()
            //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func startTimerForHoldBookings(){
        let bookingHoldingTimeInMinute : Int = 5
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute , value: bookingHoldingTimeInMinute, to: Date())
        UserDefaults.standard.set(date, forKey: UserDefaults.keys.lastBookingTime)
        UserDefaults.standard.synchronize()
        self.startTimer()
    }
    
    func stopTimerForHoldBookings(){
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.lastBookingTime)
        UserDefaults.standard.synchronize()
        self.stopTimer()
    }
    
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.checkBookingHoldTime), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc func checkBookingHoldTime(){
        
        if let lastBookingTime = UserDefaults.standard.object(forKey: UserDefaults.keys.lastBookingTime) as? Date{
            if Date() >= lastBookingTime   {
                //call API
                //self.callWebserviceFor_deleteAllbooking()
                
                // post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil, userInfo: nil)
                // `default` is now a property, not a method call
                
            }else{
            }
        }else{
            self.stopTimerForHoldBookings()
        }
    }
    
    func clearNotificationData(){
        self.isFromNotification =  false
        self.notificationInfoDict = nil
        self.notificationType = ""
    }
    
    func getDayFromSelectedDate(strDate:String)-> String{
        var Day = "Day"
        if strDate == "6"{
            Day = "Sunday"//"Sun"
        }else if strDate == "0"{
            Day = "Monday"//Mon"
        }else if strDate == "1"{
            Day = "Tuesday"//Tue"
        }else if strDate == "2"{
            Day = "Wednesday"//"Wed"
        }else if strDate == "3"{
            Day = "Thursday"//"Thu"
        }else if strDate == "4"{
            Day = "Friday"//"Fri"
        }else if strDate == "5"{
            Day = "Saturday"//"Sat"
        }else{
            Day = "Day"
        }
        return Day
    }
    
    func dateFormatInFormYou(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func crtToDateString(crd: String) -> String {
        let newDate = crd
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: newDate)
        let formattedTime: String = dateFormatter.string(from: date ?? Date())
        return formattedTime
    }
}



