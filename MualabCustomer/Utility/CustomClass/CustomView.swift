//
//  CustomView.swift
//  MualabCustomer
//
//  Created by Mac on 09/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

import UIKit

class customView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.7 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        //layer.cornerRadius = 0.5 * bounds.size.width
        //clipsToBounds = true
    }
}
