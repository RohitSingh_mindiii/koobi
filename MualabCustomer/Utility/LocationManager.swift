//
//  LocationManager.swift
//  MualabCustomer
//
//  Created by Mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreLocation

let objLocationManager = LocationManager.sharedObject()

class LocationManager: NSObject, CLLocationManagerDelegate{
    
    //Location
    var locationManager : CLLocationManager = CLLocationManager()
    var seenError : Bool = false
    var locationStatus : NSString = "Not Started"
    var strlatitude : String?
    var strlongitude : String?
    var strAddress : String = ""
    var strCurrentPlaceName: String = ""
    var currentLocation : CLLocation?
    var currentCLPlacemark : CLPlacemark?

    //Location
    
    //MARK: - Shared object
    private static var sharedManager: LocationManager = {
        let manager = LocationManager()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> LocationManager {
        return sharedManager
    }
}

//MARK: - Class functions
extension LocationManager {
    
    func getCurrentLocation(){
        //if locationManager == nil{
           locationManager = CLLocationManager()
        //}
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 100
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation();
        }
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
        if CLLocationManager.authorizationStatus() != .authorizedAlways     {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func getCurrentAdd(success:@escaping(CLPlacemark) ->Void, failure:@escaping (Error) ->Void) {
        
        let geoCoder = CLGeocoder()
        
        if let loc = self.currentLocation{
            geoCoder.reverseGeocodeLocation(loc , completionHandler:
                {(placemarks, error) in
                    
                    if (error != nil){
                        //print("reverse geodcode fail: \(error!.localizedDescription)")
                        failure(error!)
                    }
                    if let pm = placemarks{
                        if pm.count > 0 {
                            let pm1 = placemarks![0]
                            success(pm1)
                        }
                    }
            })
            }
    }
    
    func getAddressOfCurrentLocation(){
        
        objLocationManager.getCurrentAdd(success: { pm in
            
            self.currentCLPlacemark = pm
            
            var addressString : String = ""
            if pm.subLocality != nil {
                addressString = addressString + pm.subLocality! + ", "
            }
            if pm.thoroughfare != nil {
                addressString = addressString + pm.thoroughfare! + ", "
            }
            if pm.locality != nil {
                addressString = addressString + pm.locality! + ", "
            }
            if pm.country != nil {
                addressString = addressString + pm.country! + ", "
            }
            if pm.postalCode != nil {
                addressString = addressString + pm.postalCode! + " "
            }
            if addressString != ""{
               self.strAddress = addressString
               UserDefaults.standard.set(self.strAddress, forKey: UserDefaults.keys.myCurrentAdd)
               UserDefaults.standard.synchronize()
            }
            
            if pm.name!.count > 0{
                self.strCurrentPlaceName = pm.name!
            }else if pm.locality!.count > 0{
               self.strCurrentPlaceName = pm.locality!
               if pm.country!.count > 0{
                  self.strCurrentPlaceName = self.strCurrentPlaceName + ", " + pm.country!
               }
            }else{
                var addressString : String = ""
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if addressString != ""{
                    self.strCurrentPlaceName = addressString
                }
            }
            
        }) { error in
            
        }
    }
    
}

extension LocationManager {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("error:: \(error.localizedDescription)")
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
        
        var shouldIAllow = false
        
        switch status {
            
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Status not determined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
        // NotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
        
        if (shouldIAllow == true) {
            NSLog("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(locationStatus)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            let locationArray = locations as NSArray
            if locationArray.count > 0{
                if let locationObj = locationArray.lastObject as? CLLocation{
                    let coord = locationObj.coordinate
                    strlatitude = coord.latitude.description
                    strlongitude = coord.longitude.description
                   
                    UserDefaults.standard.set(strlatitude, forKey: UserDefaults.keys.myCurrentLat)
                   
                    UserDefaults.standard.set(strlongitude, forKey: UserDefaults.keys.myCurrentLong)
                    UserDefaults.standard.synchronize()
                    
                    self.currentLocation = locationObj
                    if !objAppShareData.isSearchingUsingFilter{
                        print("notification times")
                    let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
                    if isLoggedIn {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshSearchBoardLocation"), object: nil)
                    }
                    }
                    self.getAddressOfCurrentLocation()
                    //locationManager.stopUpdatingLocation()
                }
            }
            
        }
    }
}
