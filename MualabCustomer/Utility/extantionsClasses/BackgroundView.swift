//
//  BackgroundView.swift
//  MualabCustomer
//
//  Created by Mac on 15/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class BackgroundView: UIView {

    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.backgroundColor = UIColor.theameColors.backgroundColor
    }
 

}
