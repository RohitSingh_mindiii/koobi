//
//  customColor.swift
//  MualabBusiness
//
//  Created by Mac on 25/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UIColor
extension UIColor{
    enum theameColors {
        static let pinkColor = UIColor.init(red: 248.0/255.0, green: 50.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        
        static let darkColor = UIColor.init(red: 76.0/255.0, green: 53.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        
        static let blueColor = UIColor.init(red: 63.0/255.0, green: 142.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        
        static let backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        static let greenCalendar = UIColor.init(red: 0.0/255.0, green: 187.0/255.0, blue: 39.0/255.0, alpha: 1.0)
        
        static let skyBlueNewTheam = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    }
}

//MARK: - Userdeafults
extension UserDefaults{
    enum keys {
        static let userInfo = "userInfo"
        static let EditProfileAddress = "EditProfileAddress"
        static let stripeCustomerId = "stripeCustomerId"
        static let stripeCardId = "stripeCardId"
        static let authToken = "authToken"
        static let isLoggedIn = "isLoggedIn"
        static let notificationStatus = "notificationStatus"
        static let lastBookingTime = "lastBookingTime"
        static let myCurrentAdd = "myCurrentAdd"
        static let myCurrentLat = "myCurrentLat"
        static let myCurrentLong = "myCurrentLong"
        static let mainServiceType = "mainServiceType"
        
        static let myId = "myId"
        static let myName = "myName"
        static let myImage = "myImage"
    }
    
    enum MyData{
        static let OTP = "OTP"
        static let __v = "_v"
        static let _id = "_id"
        static let address = "address"
        static let address2 = "address2"
        static let authToken = "authToken"
        static let bankStatus = "bankStatus"
        static let bio = "bio"
        static let buildingNumber = "buildingNumber"
        static let businessName = "businessName"
        static let businessType = "businessType"
        static let businesspostalCode = "businesspostalCode"
        static let certificateCount = "certificateCount"
        static let chatId = "chatId"
        static let city = "city"
        static let contactNo = "contactNo"
        static let country = "country"
        static let countryCode = "countryCode"
        static let crd = "crd"
        static let deviceToken = "deviceToken"
        static let deviceType = "deviceType"
        static let dob = "dob"
        static let email = "email"
        static let firebaseToken = "firebaseToken"
        static let firstName = "firstName"
        static let followersCount = "followersCount"
        static let followingCount = "followingCount"
        static let gender = "gender"
        static let inCallpreprationTime = "inCallpreprationTime"
        static let isDocument = "isDocument"
        static let lastName = "lastName"
        static let latitude = "latitude"
        static let location =  "location"
        static let longitude = "longitude"
        static let mailVerified = "mailVerified"
        static let otpVerified = "otpVerified"
        static let outCallpreprationTime = "outCallpreprationTime"
        static let password = "password"
        static let postCount = "postCount"
        static let profileImage = "profileImage"
        static let radius = "radius"
        static let ratingCount = "ratingCount"
        static let reviewCount = "reviewCount"
        static let serviceCount = "serviceCount"
        static let serviceType = "serviceType"
        static let socialId = "socialId"
        static let socialType = "socialType"
        static let state = "state"
        static let status = "status"
        static let upd = "upd"
        static let userName = "userName"
        static let userType = "userType"
    }
    
    
    
}


//MARK: - validation message
struct message {
    static let sessionExpire = "Your session has been expired. Please re-login to renew your session."
    static let invalidToken = "Invalid Auth Token"
    static let noNetwork = "Please check your network connection"
    enum validation{
        static let HouseNumber = "Please enter house number."
        static let City = "Please enter city name."
        static let Locality = "Please enter location."
        static let PostalCode = "Please enter postal code."
 
 
        static let required = "Please enter required field."
        static let email = "Please enter a valid email address."
        static let invalidOTP = "Please enter valid OTP."
        static let genderSelection = "Please select gender."
        static let userNameLength = "Username must be at least 4 Characters Long."
        static let userNameSpace = "User name cannot contain blank spaces."
        static let validPassword = "Use at least 8 character, include both one uppercase letter and one number."
        static let passwordLength = "Passwords must be at least 6 characters long."
    }
    enum error {
        static let title = "Error"
        static let wrong = "Something went wrong, please try after some time."
    }
}

//MARK: - Alert Type
struct alertType {
    static let noNetwork = "noNetwork"
    static let error = "error"
    static let banner = "banner"
    static let bannerDark = "bannerDark"
    static let sessionExpire = "sessionExpire"
}
//Notification names
struct notificationName {
    static let postFeedSuccess = "postFeedSuccess"
    static let postUploaded = "postUploaded"
    static let searchText = "searchText"
}
//MARK: - UIImage
extension UIImage{
    enum customImage {
        static let error = UIImage(named:"worried-emoji")
        static let noNetwork = UIImage(named:"wifi-2")
        static let sessionExpire = UIImage(named:"sessionExpire")
        static let flashOn = UIImage(named:"flash_on")
        static let flashOff = UIImage(named:"flash_off")
        static let flashAuto = UIImage(named:"flash_automatic")
        static let camera = UIImage(named:"ico_camera")
        static let video = UIImage(named:"icoVideoRec")
        static let user = UIImage(named:"cellBackground")
    }
}

//MARK: - Keys
struct Keys {
    enum user {
        static let socialId = "socialId"
        static let socialType = "socialType"
        static let address = "address"
        static let type = "userType"
        static let email = "email"
        static let countryCode = "countryCode"
        static let contactNumber = "contactNumber"
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let businessName = "businessName"
        static let userName = "userName"
        static let DOB = "dob"
        static let gender = "gender"
        static let buildingNumber = "buildingNumber"

        enum addressComponent {
            static let fullAddress = "fullAddress"
            static let locality = "locality"
            static let address2 = "address2"
            static let city = "city"
            static let state = "state"
            static let postalCode = "postalCode"
            static let country = "country"
            static let placeName = "placeName"
            static let latitude = "latitude"
            static let longitude = "longitude"
            static let buildingNumber = "buildingNumber"

        }
    }
}


