//
//  WebserviceConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/25/17.
//  Copyright © 2017 MINDIII. All rights reserved.


import Foundation

let objWebserviceManager : WebServiceManager = WebServiceManager.sharedObject()

let stripeKey = "Bearer sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt" //// Live Koobi
let KUrlStripeId : String = "pk_live_PfDNm4HgHKXuIK4yVCrr4ye500EJ4Cbm22" //// Live Koobi

//let stripeKey = "Bearer sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4" //// Dev Koobi
//let KUrlStripeId : String = "pk_test_2mCH7F6JmjI04AyuBpS29VpK00Srv2nBF7" //// Dev Koobi

/*
strip live
    pk_live_PfDNm4HgHKXuIK4yVCrr4ye500EJ4Cbm22
    sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt
strip test
    pk_test_2mCH7F6JmjI04AyuBpS29VpK00Srv2nBF7
    sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4
*/

class webUrl {
    
    //static let BaseURL =  "http://3.17.192.198:3000/api/"  //>>Live
    //static let BaseURL =  "https://dev.koobi.co.uk/api/"  //>>Dev
    
    static let Login = "userLogin"
    static let ForgotPassword = "forgotPassword"
    static let Registration = "userRegistration"
    static let UserDataVerification = "userDataVerification"
    static let AddFeed =  "user/addFeeds"
    static let AddMyStory = "user/addMyStory"
    static let GetAllFeed = "user/getAllFeeds"
    static let GetUsers = "user/userList"
    static let LikeUnlikePost = "user/likes"
    static let GetLikeList = "user/likeList"
    static let GetCommnetList = "user/commentList"
    static let AddComment = "user/addComment"
    static let FollowUnfollowUser = "user/followUnfollow"
    static let LikeUnlikeComment = "user/commentLike"
    static let GetMyStories = "user/getMyStory"
    static let postcode = "https://api.postcodes.io/postcodes/"
}

//MARK : - Webservices
struct WebURL {
    
    ///*
    //static let BaseUrl =  "https://koobi.co.uk/api/"  //>>Live
    //Rohit
    static let BaseUrl =  "http://192.168.1.215:3001/api/"  //>>Live
    static let termsURL =  "https://koobi.co.uk/terms.pdf"
    static let privacyURL =  "https://koobi.co.uk/privacy_policy.pdf"
    //*/
    
    /*
    static let BaseUrl =  "https://dev.koobi.co.uk/api/"  //>>Dev
    static let termsURL =  "https://dev.koobi.co.uk/terms.pdf"
    static let privacyURL =  "https://dev.koobi.co.uk/privacy_policy.pdf"
    */
    
    static let reportURL = "http://koobi.co.uk:5000/"
    static let postcode = "https://api.postcodes.io/postcodes/"
    static let registration = "auth/registration"
    static let login = "auth/user-login"
    static let checkSocialLogin = "auth/check-social-login"
    static let forgotPassword = "forgotPassword"
    //Rohit
    //static let phonVerification = "phonVerification"
    static let verifyAuthOtp = "auth/verify-otp"
    static let phonVerification = "auth/send-auth-otp"
    static let checkUser = "auth/check-user"
    
    static let artistSearch = "artistSearch"
    static let favoriteList = "favoriteList"   
    static let addMyStory = "addMyStory"
    static let profileByUserName = "profileByUserName"

    static let artistDetail = "artistDetailNew"
    static let artistTimeSlot = "artistTimeSlotNew"
    
    static let getMyStoryUser = "getMyStoryUser"
    static let myStory = "myStory"
    static let ivitationList = "companyInfo"
    static let invitationUpdate = "invitationUpdate"
    static let serviceStaff = "serviceStaff"
    static let allCategory = "allCategory"
    static let certificateList = "certificateList"

    static let addFeed = "addFeed"
    static let getAllFeeds = "getAllFeeds"
    static let reportReason = "reportReason"
    static let like = "like"
    static let deleteAllBookService = "deleteAllBookService"
    static let likeList = "likeList"
    static let commentList = "commentList"
    static let addComment = "addComment"
    static let followFollowing = "followFollowing"
    static let commentLike = "commentLike"
    static let deleteBookService = "deleteBookService"
    static let deleteUserBookService = "deleteUserBookService"
    static let getMostBookedService = "getMostBookedService"
    static let exploreSearch = "exploreSearch"
    static let userFeed = "userFeed"
    static let tagSearch = "tagSearch"
    static let feedDetails = "feedDetails"
    static let userBooking = "userBooking"
    static let getNotificationList = "getNotificationList"
    
    static let addArtistService = "addArtistService"
    static let getAllCertificate = "getAllCertificate"
    static let addArtistCertificate = "addArtistCertificate"
    static let deleteCertificate = "deleteCertificate"
    static let getProfile = "getProfile"
    static let profileFeed = "profileFeed"
    static let followingList = "followingList"
    static let followerList = "followerList"
    static let artistService = "artistService"
    static let addFavorite = "addFavorite"
    static let bookingDetails = "bookingDetails"
    static let changeStaff = "changeStaff"
    
    
    static let paymentList = "paymentList"
    static let staffServices = "staffServices"

    //
    static let profileUpdate = "profileUpdate"
    
    static let createFolder = "createFolder"
    static let getFolder = "getFolder"
    static let editFolder = "editFolder"
    static let deleteFolder = "deleteFolder"
    static let saveToFolder = "saveToFolder"
    static let getFolderFeed = "getFolderFeed"
    static let removeToFolder = "removeToFolder"
    static let deleteComment = "deleteComment"

    //// Booking
    static let bookArtist = "bookArtist"
    static let getBookedServices = "getBookedServices"
    static let confirmBooking = "confirmBooking"
    static let voucherList = "voucherList"
    static let applyVoucher = "applyVoucher"
    static let bookingDetail = "bookingDetail"
    static let userBookingHistory = "userBookingHistory"
    static let bookingAction = "bookingAction"
    static let bookingReviewRating = "bookingReviewRating"
    static let cardPayment: String = "cardPayment"
    static let getRatingReview: String = "getRatingReview"
    static let bookingReport: String = "bookingReport"
    static let adminCommision: String = "adminCommision"
    static let exploreFeeds: String = "exploreFeeds"
    static let allExploreCategory: String = "allExploreCategory"
    static let allExploreBusiness: String = "allExploreBusiness"
    
    static let editComment: String = "editComment"
    ////
    static let deleteStory: String = "deleteStory"
    static let deleteFeed: String = "deleteFeed"
    static let userReport: String = "userReport"
    static let updateRecord: String = "updateRecord"
    static let changepassword: String = "changepassword"
    static let notificationClear: String = "notificationClear"
   
    static let get_buisnes_type_by_miles: String = "get-buisnes-type-by-miles"
    static var Deeplinking =  "http://koobi.co.uk/"
}

let k_success = "success"


