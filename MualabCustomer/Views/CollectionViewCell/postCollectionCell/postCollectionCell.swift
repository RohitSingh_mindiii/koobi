//
//  postCollectionCell.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Photos

class postCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var imgThumbVideo: UIImageView!
    @IBOutlet weak var imgSelected: UIImageView!
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    var imgICloud : UIImage?
}

extension postCollectionCell{
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Bind Photo Library
    func showLibraryData(withAssets asset:PHAsset,withPreviewState state:Bool, andSelectState selectState:Bool) -> Void {
        self.imgThumb.image = getAssetThumbnail(asset: asset)
        if state{
            selectedView.isHidden = false
        }else{
            selectedView.isHidden = true
        }
        if selectState{
            imgSelected.isHidden = false
        }else{
            imgSelected.isHidden = true
        }
        switch asset.mediaType {
        case .image:
            self.lblTime.text = ""

        case .video:
            let duration = asset.duration
            var time = duration
            if time >= 60 {
                time = duration / 60
            }else{
                time = duration / 100
            }
            self.lblTime.text = String(format: "%.2f",time)
        default:
           break
        }
    }
}

private extension postCollectionCell {
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        //option.resizeMode = PHImageRequestOptionsResizeMode.exact
        //option.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic
        let dimension = self.frame.size.width+20
        //let dimension = 400
        manager.requestImage(for: asset, targetSize: CGSize(width:dimension, height:dimension), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
            if result != nil{
                thumbnail = result!
                self.imgICloud = result!
            }
        })
//        let dimensionImgICloud = 400
//        manager.requestImage(for: asset, targetSize: CGSize(width:dimensionImgICloud, height:dimensionImgICloud), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
//            if result != nil{
//                self.imgICloud = result!
//            }
//        })
        
         /*
         {
         let manager = PHImageManager.default()
         let option = PHImageRequestOptions()
         var thumbnail = UIImage()
         option.isSynchronous = true
         let dimension = self.frame.size.width+20
         manager.requestImage(for: asset, targetSize: CGSize(width:dimension, height:dimension), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
         if result != nil{
         thumbnail = result!
         }
         })
         return thumbnail
         }
         */
        return thumbnail
    }
}
