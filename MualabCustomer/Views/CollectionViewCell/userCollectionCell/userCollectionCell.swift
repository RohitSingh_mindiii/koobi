//
//  userCollectionCell.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class userCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgPlus: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTime: UILabel!
}
