//
//  gradiantBG.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class gradiantBG: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = self.bounds
        let color2 = UIColor.init(red: 248.0/255.0, green: 50.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        
        let color3 = UIColor.init(red: 63.0/255.0, green: 142.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        gradientLayer.colors = [color2.cgColor,color3.cgColor]
        // gradientLayer.locations = [0.0,1.0,2.0]
        gradientLayer.startPoint = CGPoint.init(x: 0.20, y: 0.20)
        gradientLayer.endPoint = CGPoint.init(x: 0.70, y: 1.0)
            //self.layer.insertSublayer(gradientLayer,at:0)
        self.layer.addSublayer(gradientLayer)
    }

}
