//
//  feedCollectionCell.swift
//  MualabCustomer
//
//  Created by Mac on 04/10/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

@objc protocol feedCollectionDelegate {
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsCollectionCell: feedCollectionCell )
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any])
}

class feedCollectionCell: UICollectionViewCell,UIScrollViewDelegate, EBTagPopoverDelegate, EBPhotoViewDelegate {
    
    private(set) var tagsHidden = true
    var objFeeds:feeds?
    var indexPath : IndexPath!
    //var delegate  : feedsTableCellDelegate!
    weak var delegate1  : feedCollectionDelegate!
   
    //
    @IBOutlet weak var imgFeeds: UIImageView?
    @IBOutlet weak var imgPlay : UIImageView?
    //
    @IBOutlet weak var pageControllView: UIView!
    //
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var imageVideoView: UIView!
    
    
    let scrollView = UIScrollView()
    
    //    override func setSelected(_ selected: Bool, animated: Bool) {
    //        super.setSelected(selected, animated: animated)
    //    }
    
    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.imgFeeds!.image = nil
    }
    func setPageControllMethod(objFeeds:feeds){
        
        self.objFeeds = objFeeds
        let arrFeed = objFeeds.arrFeed
        
        let scrollWidth = self.imageVideoView.frame.size.width
        let scrollHeight = self.imageVideoView.frame.size.height
        
        self.scrollView.delegate = self as! UIScrollViewDelegate
        self.scrollView.frame = CGRect(x: 0, y:0, width: scrollWidth, height: scrollHeight)
        self.scrollView.contentSize = CGSize(width:scrollWidth * CGFloat(arrFeed.count), height:scrollHeight)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator=false
        self.scrollView.bounces = false
        
        self.pageControll.hidesForSinglePage = true
        self.pageControll.numberOfPages = arrFeed.count
        self.pageControll.currentPage = 0
        self.pageControll.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        
        for index in  0..<arrFeed.count {
            
            //amit
            let viewOuter = UIView.init(frame: CGRect(x: CGFloat(index) * scrollWidth, y: 0, width: scrollWidth, height: scrollHeight))
            viewOuter.backgroundColor = UIColor.white
            
            let scrollVwEBPhotoView =   EBPhotoView.init(frame: CGRect(x: 0, y: 0, width: scrollWidth, height: scrollHeight))
            
            scrollVwEBPhotoView.tag = index
            scrollVwEBPhotoView.myDelegate = self
            
            scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFill
            scrollVwEBPhotoView.imageView.clipsToBounds = true
            scrollVwEBPhotoView.adjustsContentModeForImageSize = false
            
            scrollVwEBPhotoView.maximumZoomScale = 1.0
            scrollVwEBPhotoView.minimumZoomScale = 1.0
            
            if arrFeed[index].feedPost.count>0{
                
                //scrollVwEBPhotoView.imageView.af_setImage(withURL:URL(string:arrFeed[index].feedPost)!)
                
                if let urlImage = URL(string:arrFeed[index].feedPost){
                    //scrollVwEBPhotoView.imageView.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                    scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                }else{
                    scrollVwEBPhotoView.imageView.image = UIImage(named:"gallery_placeholder")
                }
            }
            
            viewOuter.addSubview(scrollVwEBPhotoView)
            self.scrollView.addSubview(viewOuter)
            
            if objFeeds.arrPhotoInfo.count > index{
                let objPhotoInfo = objFeeds.arrPhotoInfo[index]
                objPhotoInfo.objEBPhotoView = scrollVwEBPhotoView
            }
        }
        
        self.pageControllView.addSubview(self.scrollView)
        //self.pageControll.layoutIfNeeded()
        
        //self.setTagOnImages(objFeeds:objFeeds)
    }
    
    
    func setTagOnImages(objFeeds:feeds){
        
        guard objFeeds.arrPhotoInfo.count > 0 else{return}
        
        for objPhotoInfo in objFeeds.arrPhotoInfo {
            
            if objPhotoInfo.arrTags.count > 0  {
                
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrTags.enumerated() {
                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                    }
                }
                
                self.tagsHidden = true
                objPhotoInfo.arrTagPopovers = arrTagPopovers
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                    }
                }
            }
        }
    }
    
    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width
        pageControll.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
    }
}

// MARK: - Tag Hide/Show
extension feedCollectionCell {
    
    func setTagsHidden(_ tagsHidden: Bool) {
        tagsHidden ? hideTags() : showTags()
    }
    
    func showTags() {
        setTagsAsHidden(false)
    }
    
    func hideTags() {
        setTagsAsHidden(true)
    }
    
    func setTagsAsHidden(_ hidden: Bool) {
        
        guard let objFeeds = self.objFeeds,  objFeeds.arrPhotoInfo.count > 0   else{return}
        tagsHidden = hidden
        let alpha: CGFloat = hidden ? 0 : 1
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            
            for objPhotoInfo in objFeeds.arrPhotoInfo {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
            
        }) { finished in
            
            for objPhotoInfo in objFeeds.arrPhotoInfo{
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
        }
    }
}


// MARK: - Notification Actions
extension feedCollectionCell {
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        delegate1.didReceiveSingleTapAt(indexPath: indexPath, feedsCollectionCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveDoubleTap doubleTap: UITapGestureRecognizer!) {
        // delegate.didReceiveDoubleTapAt(indexPath: indexPath, feedCollectionCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveLongPress longPress: UILongPressGestureRecognizer!) {
        //delegate.didReceiveLondPressAt(indexPath: indexPath, feedCollectionCell: self)
        //self.setTagsHidden(!tagsHidden)
    }
    
    //tag delegate
    func tagPopoverDidEndEditing(_ tagPopover: EBTagPopover!) {
        
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        
        guard let objFeeds = self.objFeeds, pageControll.currentPage < objFeeds.arrPhotoInfo.count else {
            return
        }
        
        let objPhotoInfo = objFeeds.arrPhotoInfo[pageControll.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            
            if tagPopover.text() == objTagPopover.text() {
                let objTagInfo = objPhotoInfo.arrTags[index]
                let dict = objTagInfo.metaData
                delegate1.tagPopoverDidReceiveSingleTapWithInfo(dict: dict)
            }
        }
    }
    
}

class newFeedCollCell: UICollectionViewCell {
    @IBOutlet weak var btnShowTag: UIButton!
    @IBOutlet weak var viewShowTag: UIView!
    @IBOutlet weak var imgNewFeed : EEZoomableImageView!
    @IBOutlet weak var scrollVwEBPhotoView: EBPhotoView!
    
    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.imgNewFeed.image = nil
    }
}

