//
//  feedsTableCell.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import AttributedTextView
import EEZoomableImageView
import Kingfisher
import SDWebImage

@objc protocol feedsTableCellDelegate {
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any])
}

class feedsTableCell: UITableViewCell,UIScrollViewDelegate, EBTagPopoverDelegate, EBPhotoViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var viewCollection: UICollectionView!
    
    @IBOutlet weak var imgFeeds: EEZoomableImageView!
    //    {
    //        didSet {
    //            imgFeeds.minZoomScale = 0.5
    //            imgFeeds.maxZoomScale = 10.0
    //            imgFeeds.resetAnimationDuration = 0.5
    //        }
    //    }
    
    private(set) var tagsHidden = true
    var objFeeds:feeds?
    var placeholderImage:UIImage?
    var indexPath : IndexPath!
    weak var delegate : feedsTableCellDelegate?
    //more option view
    @IBOutlet weak var viewSeparatorViewMore: UIView!
    @IBOutlet weak var viewShowTag: UIView!
    @IBOutlet weak var btnShowTag: UIButton!
    @IBOutlet weak var btnEditPost: UIButton!
    @IBOutlet weak var btnDeletePost: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var btnSaveToFolder: UIButton!
    @IBOutlet weak var btnReportThisPost: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnHiddenMoreOption: UIButton!
    //
    @IBOutlet weak var heightOfImageRatio: NSLayoutConstraint!
    @IBOutlet weak var imgProfile: UIImageView!
    //@IBOutlet weak var imgFeeds: UIImageView?
    @IBOutlet weak var imgPlay : UIImageView?
    //
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    // @IBOutlet weak var btnViewComment: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    //
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var lblLikeOrLikes: UILabel!
    @IBOutlet weak var lblCommentOrComments: UILabel!
    
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCaption: UILabel!
    //
    @IBOutlet weak var imageVideoView: UIView!
    @IBOutlet weak var pageControllView: UIView!
    //
    @IBOutlet weak var txtCapView: AttrTextView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    let scrollView = UIScrollView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.viewCollection.delegate = self
        //self.viewCollection.dataSource = self
        self.txtCapView.textContainerInset = UIEdgeInsets.zero
    }
    override func prepareForReuse() {
        //        super.prepareForReuse()
        //        self.imgProfile.image = nil
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnShowTagAction( sender: UIButton){
        self.setTagsHidden(!tagsHidden)
    }
    
    func setPageControllMethod(objFeeds:feeds){
        
        self.objFeeds = objFeeds
        let arrFeed = objFeeds.arrFeed
        
        let scrollWidth = self.imageVideoView.frame.size.width
        let scrollHeight = self.imageVideoView.frame.size.height
        
        self.scrollView.delegate = self
        self.scrollView.frame = CGRect(x: 0, y:0, width: scrollWidth, height: scrollHeight)
        self.scrollView.contentSize = CGSize(width:scrollWidth * CGFloat(arrFeed.count), height:scrollHeight)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator=false
        self.scrollView.bounces = false
        
        self.pageControll.hidesForSinglePage = true
        self.pageControll.numberOfPages = arrFeed.count
        self.pageControll.currentPage = 0
        self.pageControll.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        
        for index in  0..<arrFeed.count {
            
            let viewOuter = UIView.init(frame: CGRect(x: CGFloat(index) * scrollWidth, y: 0, width: scrollWidth, height: scrollHeight))
            viewOuter.backgroundColor = UIColor.white
            
            let scrollVwEBPhotoView =   EBPhotoView.init(frame: CGRect(x: 0, y: 0, width: scrollWidth, height: scrollHeight))
            
            scrollVwEBPhotoView.tag = index
            scrollVwEBPhotoView.myDelegate = self
            
            scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFill
            scrollVwEBPhotoView.imageView.clipsToBounds = true
            scrollVwEBPhotoView.adjustsContentModeForImageSize = false
            
            scrollVwEBPhotoView.maximumZoomScale = 1.0
            scrollVwEBPhotoView.minimumZoomScale = 1.0
            
            if arrFeed[index].feedPost.count>0{
                
                if let urlImage = URL(string:arrFeed[index].feedPost){
                    //scrollVwEBPhotoView.imageView.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                    scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                }else{
                    
                }
            }
            
            viewOuter.addSubview(scrollVwEBPhotoView)
            self.scrollView.addSubview(viewOuter)
            
            if objFeeds.arrPhotoInfo.count > index{
                let objPhotoInfo = objFeeds.arrPhotoInfo[index]
                objPhotoInfo.objEBPhotoView = scrollVwEBPhotoView
            }
            
            
            if objFeeds.arrFeed.count > 1{
                self.pageControll.isHidden = false
                self.pageControllView.isHidden = false
            }
        }
        
        self.pageControllView.addSubview(self.scrollView)
        if objFeeds.arrFeed.count > 1{
            self.pageControll.isHidden = false
            self.pageControllView.isHidden = false
        }
        self.setTagOnImages(objFeeds:objFeeds)
    }
    
    func setDefaultDesign(){
        self.viewMore.isHidden = true
    }
    
    func setPageControllMethodNew(objFeeds:feeds){
        self.objFeeds = objFeeds
        if (self.pageControll != nil){
            self.pageControll.isHidden = false
            self.pageControll.hidesForSinglePage = true
            self.pageControll.numberOfPages = self.objFeeds!.arrFeed.count
            self.pageControll.currentPage = 0
            self.pageControll.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }
        if (self.viewCollection != nil){
            //if self.objFeeds!.feedType == "image"{
            self.viewCollection.delegate = self
            self.viewCollection.dataSource = self
            self.viewCollection.reloadData()
            //}
            if self.objFeeds!.arrFeed.count == 1{
                self.viewCollection.isScrollEnabled = false
                self.viewCollection.bounces = false
            }else{
                self.viewCollection.bounces = true
                self.viewCollection.isScrollEnabled = true
            }
            //self.setTagOnImages(objFeeds:self.objFeeds!)
        }
        
        let myInt = objFeeds.likeCount
        if myInt > 0{
            if myInt > 1{
                self.lblLikeOrLikes.text = "Likes"
            }else{
                self.lblLikeOrLikes.text = "Like"
            }
        }else {
            self.lblLikeOrLikes.text = "Like"
        }
        
        let myComment = objFeeds.commentCount
        if myComment == 0{
            self.lblCommentOrComments.text = "Comment"
        }else if myComment == 1{
            self.lblCommentOrComments.text = "Comment"
        }else {
            self.lblCommentOrComments.text = "Comments"
        }
        
        if self.objFeeds!.arrFeed.count > 1{
            self.pageControll.isHidden = false
            self.pageControllView.isHidden = false
        }
    }
    
    func BG(_ block: @escaping ()->Void) {
        DispatchQueue.global(qos: .userInitiated).async(execute: block)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.objFeeds != nil){
            return self.objFeeds!.arrFeed.count
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.viewCollection.frame.width, height: self.viewCollection.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.setTagsHidden(!tagsHidden)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "newFeedCollCell", for:
            indexPath) as? newFeedCollCell)!
        cell.btnShowTag.tag = indexPath.item
        cell.scrollVwEBPhotoView.tag = indexPath.item
        cell.scrollVwEBPhotoView.myDelegate = self
        
        if objFeeds!.arrPhotoInfo.count>0{
            if self.objFeeds?.arrPhotoInfo.count ?? 0 > indexPath.item{
                let obj = objFeeds!.arrPhotoInfo[indexPath.item]
                if obj.arrTags.count>0{
                    cell.viewShowTag.isHidden = false
                }else{
                    cell.viewShowTag.isHidden = true
                }
            }
        }else{
            cell.viewShowTag.isHidden = true
        }
    
        cell.scrollVwEBPhotoView.adjustsContentModeForImageSize = false
        
        cell.scrollVwEBPhotoView.maximumZoomScale = 1.0
        cell.scrollVwEBPhotoView.minimumZoomScale = 1.0
        
        if self.objFeeds?.arrPhotoInfo.count ?? 0 > indexPath.item{
            let objPhotoInfo = self.objFeeds?.arrPhotoInfo[indexPath.item]
            objPhotoInfo?.objEBPhotoView = cell.scrollVwEBPhotoView
        }else{
        }
        if self.objFeeds?.feedType == "video"{
            cell.scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
            if self.objFeeds?.arrPhotoInfo.count ?? 0 > indexPath.item{
                let urlImage = URL(string:(self.objFeeds?.arrFeed[indexPath.item].videoThumb)!)!
            }
            
        }else{
            
            if self.objFeeds!.arrPhotoInfo.count > 1{
                if indexPath.item == 0{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    cell.scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    cell.scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }else{
                if self.objFeeds?.feedImageRatio == "0"{
                    placeholderImage = UIImage.init(named:"gallery_placeholder")!
                    cell.scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else if self.objFeeds?.feedImageRatio == "1"{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    cell.scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_45")!
                    cell.scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }
            ////
            if (self.objFeeds?.arrFeed[indexPath.item].feedPost.count)!>0{
                
                if self.objFeeds?.arrPhotoInfo.count ?? 0 > indexPath.item{
                    if var urlImage = URL(string:(self.objFeeds?.arrFeed[indexPath.item].feedPost)!){
                        //cell.scrollVwEBPhotoView.imageView.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                        if self.objFeeds?.feedType == "video"{
                            urlImage = URL(string:(self.objFeeds?.arrFeed[indexPath.item].videoThumb)!)!
                        }
                        //cell.scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "gallery_placeholder"))
                        let processor = DownsamplingImageProcessor(size: cell.scrollVwEBPhotoView.imageView.frame.size)
                         >> RoundCornerImageProcessor(cornerRadius: 0)
                         cell.scrollVwEBPhotoView.imageView.kf.indicatorType = .activity
                         cell.scrollVwEBPhotoView.imageView.kf.setImage(
                         with: urlImage,
                         placeholder: placeholderImage,
                         options: [
                         .processor(processor),
                         .scaleFactor(UIScreen.main.scale),
                         .transition(.fade(0.7)),
                         .cacheOriginalImage
                         ])
                         {
                         result in
                         switch result {
                         case .success(let value):
                         print("Task done for: \(value.source.url?.absoluteString ?? "")")
                         
                         case .failure(let error):
                         //cell.scrollVwEBPhotoView.imageView.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                            cell.scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage: self.placeholderImage)
                         print("Job failed: \(error.localizedDescription)")
                         }
                         }
                         
                    }
                }
            }
        }
        //cell.scrollVwEBPhotoView.clipsToBounds = true
        //cell.contentView.addSubview(scrollVwEBPhotoView)
        
        //        if self.objFeeds?.arrPhotoInfo.count ?? 0 > indexPath.item{
        //            let objPhotoInfo = self.objFeeds?.arrPhotoInfo[indexPath.item]
        //            objPhotoInfo?.objEBPhotoView = cell.scrollVwEBPhotoView
        //        }
        
        ////
        if (self.objFeeds?.arrPhotoInfo.count)!>indexPath.item{
            let objPhotoInfo = self.objFeeds?.arrPhotoInfo[indexPath.item]
            for popover in  (objPhotoInfo?.objEBPhotoView!.subviews)!{
                if popover.isKind(of: EBTagPopover.self) {
                    popover.removeFromSuperview()
                }
            }
            if (objPhotoInfo?.arrTags.count)! > 0 {
                //BG(){
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.setTagOnImagesNew(objFeeds: self.objFeeds!, index:indexPath.item)
//                }
            }else{
            }
        }
        ////
        return cell
    }
    
    func setTagOnImages(objFeeds:feeds){
        
        guard objFeeds.arrPhotoInfo.count > 0 else{return}
        
        for objPhotoInfo in objFeeds.arrPhotoInfo {
            
            if objPhotoInfo.arrTags.count > 0  {
                
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrTags.enumerated() {
                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    
                    if objTagInfo.strTagType == "service"{
                        //tagPopover?.backgroundColor = UIColor.green
                        //tagPopover?.setViewShadow(20.0)
                        tagPopover?.strTagType = "service"
                        //incallPrice outcallPrice
                        
                        //let strInCallPrice = objTagInfo.metaData["incallPrice"] as? String ?? ""
                        //let strOutCallPrice = objTagInfo.metaData["outcallPrice"] as? String ?? ""
                        
                        //// Himanshu for new staff tag
                        let strInCallPrice = objTagInfo.metaData["staffInCallPrice"] as? String ?? ""
                        let strOutCallPrice = objTagInfo.metaData["staffOutCallPrice"] as? String ?? ""
                        ////
                        
                        if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                            tagPopover?.strPrice = strOutCallPrice
                        }else{
                            tagPopover?.strPrice = strInCallPrice
                        }
                    }else{
                        tagPopover?.strTagType = "people"
                    }
                    if objTagInfo.strTagType == "service"{
                        if objTagInfo.tagPosition.y>0.75{
                            tagPopover?.strTopOrBottom = "bottom"
                        }else{
                            tagPopover?.strTopOrBottom = "top"
                        }
                    }else{
                        if objTagInfo.tagPosition.y>0.9{
                            tagPopover?.strTopOrBottom = "bottom"
                        }else{
                            tagPopover?.strTopOrBottom = "top"
                        }
                    }
                    if objTagInfo.tagPosition.x>=0.87{
                        tagPopover?.strLeftOrRight = "right"
                    }else{
                        if objTagInfo.tagPosition.x<=0.05{
                            tagPopover?.strLeftOrRight = "left"
                        }else{
                            tagPopover?.strLeftOrRight = ""
                        }
                    }
                    
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                    }
                }
                
                self.tagsHidden = true
                objPhotoInfo.arrTagPopovers = arrTagPopovers
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                    }
                }
            }
        }
    }
    
    func setTagOnImagesNew(objFeeds:feeds, index:Int){
        
        //guard objFeeds.arrPhotoInfo.count > 0 else{return}
        
        //for objPhotoInfo in objFeeds.arrPhotoInfo {
        if objFeeds.arrPhotoInfo.count > index{
            let objPhotoInfo = objFeeds.arrPhotoInfo[index]
            if objPhotoInfo.arrTags.count > 0  {
                
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrTags.enumerated() {
                    
                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    
                    //BG(){
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    //tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.strIsForAddTag = ""
                    if objTagInfo.strTagType == "service"{
                        //tagPopover?.backgroundColor = UIColor.green
                        //tagPopover?.setViewShadow(20.0)
                        tagPopover?.strTagType = "service"
                        //incallPrice outcallPrice
                        
                        //let strInCallPrice = objTagInfo.metaData["incallPrice"] as? String ?? ""
                        //let strOutCallPrice = objTagInfo.metaData["outcallPrice"] as? String ?? ""
                        
                        //// Himanshu for new staff tag
                        let strInCallPrice = objTagInfo.metaData["staffInCallPrice"] as? String ?? ""
                        let strOutCallPrice = objTagInfo.metaData["staffOutCallPrice"] as? String ?? ""
                        ////
                        
                        /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                            tagPopover?.strPrice = strOutCallPrice
                        }else{
                            tagPopover?.strPrice = strInCallPrice
                        }
                        */
                        let isOutcall = objTagInfo.metaData["isOutCall"] as? String ?? ""
                        if isOutcall == "0"{
                            tagPopover?.strPrice = strInCallPrice
                        }else{
                            tagPopover?.strPrice = strOutCallPrice
                        }
                    }else{
                        tagPopover?.strTagType = "people"
                    }
                    
                    if objTagInfo.strTagType == "service"{
                        if objTagInfo.tagPosition.y>0.80{
                            tagPopover?.strTopOrBottom = "bottom"
                        }else{
                            tagPopover?.strTopOrBottom = "top"
                        }
                    }else{
                        if objTagInfo.tagPosition.y>0.9{
                            //if objTagInfo.tagPosition.y>0.8{
                            tagPopover?.strTopOrBottom = "bottom"
                        }else{
                            tagPopover?.strTopOrBottom = "top"
                        }
                    }
                    
                    if objTagInfo.tagPosition.x>=0.87{
                        //if objTagInfo.tagPosition.x>=0.8{
                        tagPopover?.strLeftOrRight = "right"
                    }else{
                        if objTagInfo.tagPosition.x<=0.05{
                            tagPopover?.strLeftOrRight = "left"
                        }else{
                            tagPopover?.strLeftOrRight = ""
                        }
                    }
                    
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                        objPhotoInfo.arrTagPopovers = arrTagPopovers
                    }
                    //}
                }
                //self.tagsHidden = true
                // objPhotoInfo.arrTagPopovers = arrTagPopovers
                
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                    }
                }
                ////
                //objPhotoInfo.objEBPhotoView?.clipsToBounds = true
                ////
            }
        }
    }
    
    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width
        pageControll.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
        //tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
    }
}

// MARK: - Tag Hide/Show
extension feedsTableCell {
    
    func setTagsHidden(_ tagsHidden: Bool) {
        tagsHidden ? hideTags() : showTags()
    }
    
    func showTags() {
        setTagsAsHidden(false)
    }
    
    func hideTags() {
        setTagsAsHidden(true)
    }
    
    func setTagsAsHidden(_ hidden: Bool) {
        
        guard let objFeeds = self.objFeeds,  objFeeds.arrPhotoInfo.count > 0   else{return}
        tagsHidden = hidden
        let alpha: CGFloat = hidden ? 0 : 1
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            
            for objPhotoInfo in objFeeds.arrPhotoInfo {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
            
        }) { finished in
            
            for objPhotoInfo in objFeeds.arrPhotoInfo{
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
        }
    }
}


// MARK: - Notification Actions
extension feedsTableCell {
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        //delegate?.didReceiveSingleTapAt(indexPath: indexPath, feedsTableCell: self)
        self.setTagsHidden(!tagsHidden)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveDoubleTap doubleTap: UITapGestureRecognizer!) {
        delegate?.didReceiveDoubleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveLongPress longPress: UILongPressGestureRecognizer!) {
        //delegate.didReceiveLondPressAt(indexPath: indexPath, feedsTableCell: self)
        self.setTagsHidden(!tagsHidden)
    }
    
    //tag delegate
    func tagPopoverDidEndEditing(_ tagPopover: EBTagPopover!) {
        
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        
        guard let objFeeds = self.objFeeds, pageControll.currentPage < objFeeds.arrPhotoInfo.count else {
            return
        }
        
        let objPhotoInfo = objFeeds.arrPhotoInfo[pageControll.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            
            if tagPopover.text() == objTagPopover.text() {
                let objTagInfo = objPhotoInfo.arrTags[index]
                let dict = objTagInfo.metaData
                delegate?.tagPopoverDidReceiveSingleTapWithInfo(dict: dict)
            }
        }
    }
    
    
}


