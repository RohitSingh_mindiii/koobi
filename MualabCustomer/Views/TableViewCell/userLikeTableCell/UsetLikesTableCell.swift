//
//  UsetLikesTableCell.swift
//  MualabCustomer
//
//  Created by mac on 24/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class UsetLikesTableCell: UITableViewCell {

    // like table outlat connection
   
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var ViewImgBG: UIView!
    @IBOutlet weak var imgProfileLiker: UIImageView!
    @IBOutlet weak var lblNameLiker: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfileLiker.contentMode = .scaleAspectFill
        self.btnFollow.layer.borderWidth = 1
        self.btnFollow.layer.masksToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
