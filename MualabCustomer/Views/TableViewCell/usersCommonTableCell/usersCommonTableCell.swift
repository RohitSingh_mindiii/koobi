//
//  usersCommonTableCell.swift
//  Mualab
//
//  Created by MINDIII on 10/30/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class usersCommonTableCell: UITableViewCell {
    //
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgComment: UIImageView!
    //
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    //
    @IBOutlet weak var containerView: UIView!
    //
    @IBOutlet weak var btnLike: UIButton?
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    //
   
    @IBOutlet weak var tblComments: UITableView!
    
    var arrComntList = [commentUser]()

    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.contentMode = .scaleAspectFill
        self.btnLike?.imageView?.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func scaleImage(_ recognizer: UIPinchGestureRecognizer) {
        recognizer.view?.transform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
        recognizer.scale = 1
    }

}

// MARK: - table view Delegate and Datasource
extension usersCommonTableCell{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComntList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let objUser = arrComntList[indexPath.row]
        var cellId = "CommentCell"
        if objUser.type == "text"{
            cellId = "CommentCell"
        }else{
            cellId = "CommentCellImage"
        }
        
        let cell = tblComments.dequeueReusableCell(withIdentifier: cellId) as! usersCommonTableCell
        cell.tblComments.reloadData()
        cell.containerView?.layer.borderColor = colorGray.cgColor
        cell.lblUserName.text = objUser.userName
        
        if objUser.type == "text"{
            cell.lblComment.text = objUser.comment
        }else{
            cell.imgComment.image = UIImage(named: "gallery_placeholder");
            //cell.imgComment.af_setImage(withURL:URL(string:objUser.comment)! )
            cell.imgComment.sd_setImage(with: URL(string:objUser.comment)!, placeholderImage: UIImage(named:"gallery_placeholder"))
        }
        cell.lblTime.text = objUser.timeElapsed
        cell.btnLike?.tag = indexPath.row
        cell.btnLike?.superview?.tag = indexPath.section
        cell.imgProfile.image = UIImage.customImage.user
        
        //        if objUser.profileImage.count>0{
        //            cell.imgProfile.af_setImage(withURL: URL(string: objUser.profileImage)!)
        //        }
        if objUser.profileImage != "" {
            if let url = URL(string: objUser.profileImage){
                //cell.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                cell.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named:"cellBackground"))
            }
        }
        
        if objUser.likeStatus {
            cell.btnLike?.isSelected = true
            cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .selected)
        }else {
            cell.btnLike?.isSelected = false
            cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .normal)
        }
//        if self.arrComntList.count < totalCount {
//            if indexPath.row == arrComntList.count - (arrComntList.count - 5){
//                page = page + 1
//                getCommentList(page)
//            }
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let objUser = arrComntList[indexPath.row]
        if objUser.type == "text" {
            return UITableView.automaticDimension
        }else {
            return self.tblComments.frame.size.width * 0.75
        }
    }
}
